<?php

/**
 * Class AUTHORIZATION
 * @package Helpers\AUTH_TOKEN
 * @property HELPER
 * @author Ricardo Daniel Carrada Peña <rdcarrada@gmail.com>
 */
class AUTH_TOKEN
{
    /**
     * Method para validar el tiempo de vida del token
     * @param $token
     * @return bool|object
     */
    public static function validateTimestamp($token)
    {
        $CI =& get_instance();
        $CI->config->load('jwt');
        $CI->load->helper('date');
        $token = self::validateToken($token);
        if ($token != false && (now() - $token->timestamp < ($CI->config->item('token_timeout') * 60))) {
            return $token;
        }
        return FALSE;
    }

    /**
     * Method para validar Token
     * @param $token
     * @return object
     */
    public static function validateToken($token)
    {
        $CI =& get_instance();
        $CI->config->load('jwt');
        return JWT::decode($token, $CI->config->item('jwt_key'));
    }

    /**
     * Method para generar un token
     * @param $data
     * @return string
     */
    public static function generateToken($data)
    {
        $CI =& get_instance();
        $CI->config->load('jwt');
        return JWT::encode($data, $CI->config->item('jwt_key'));
    }
}