<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config['jwt_key'] = '';
/*Generated token will expire in 1 minute for sample code
* Increase this value as per requirement for production
*/
$config['token_timeout'] = TIMEOUT_TOKEN;

