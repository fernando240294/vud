<?php
require(APPPATH . '/libraries/REST_Controller.php');

/**
 * Class Users
 * Class
 * http_code = 200 SUCCESS
 * http_code = 404 ERROR
 */
class Auth extends REST_Controller
{

    public function __construct($config = 'rest')
    {
        parent::__construct($config);

        $this->load->helper('Jwt');
        $this->load->helper('Auth_token');
        $this->load->model('Auth_model');
        $this->load->helper('date');

        $this->lang->load('rest_controller_auth', LANG, FALSE, TRUE, __DIR__ . '/application/');
        $this->lang->load('errors_api', LANG, FALSE, TRUE, __DIR__ . '/application/');
    }

    /**
     * Method para validar el API KEY otorgado por SIAP
     * En caso que el APIKEY exista, el servicio generará un token con el cual podra realizar consultas a los demas servicios actualizacion de git

     * Aqui se agrega un nuevasdasdasdasdo comentario debido a que nada mas se estan haciendo pruebas para poder generar cambios en git
     * Aqui se agrega un nuevo comenasdasdasslllllllllllllllltario debido a que nada mas se estan haciendo pruebas para poder generar cambios en git

     Se dejan los comentarios de ambos proyectos.
     */
    public function generateToken_get()
    {
        $tokenData = array();
        $apiKey = trim($this->get('apiKey'));
        $http_response = REST_Controller::HTTP_OK;

        if (!empty($apiKey)) {
            $result_model = $this->Auth_model->validateApiKey($apiKey);

            if (!empty($result_model)) {
                $tokenData['id'] = $result_model[0]['id'];
                $tokenData['timestamp'] = now();
                $output['status'] = TRUE;
                $output['token'] = AUTH_TOKEN::generateToken($tokenData);
            } else {
                $output['status'] = FALSE;
                $output['code'] = 101;
                $http_response = REST_Controller::HTTP_BAD_REQUEST;
                $output['error'] = $this->lang->line('error_101');
            }
        } else {
            $output['status'] = FALSE;
            $output['code'] = 2;
            $http_response = REST_Controller::HTTP_BAD_REQUEST;
            $output['error'] = sprintf($this->lang->line('error_2'), "apiKey");
        }

        $this->set_response(["response" => $output], $http_response);

    }


}
