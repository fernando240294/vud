<?php

/**
 * Class Server
 */
class Server extends REST_Controller
{

    public function __construct($config = 'rest')
    {
        parent::__construct($config);

        $this->lang->load('server', LANG, FALSE, TRUE, __DIR__ . '/application/');
        $this->lang->load('errors_api', LANG, FALSE, TRUE, __DIR__ . '/application/');
    }


    /**
     * INDEX()
     */
    function index()
    {
        $data_response['status'] = FALSE;
        $data_response['code'] = 109;
        $data_response['error'] = $this->lang->line('error_109');
        $http_response = REST_Controller::HTTP_BAD_REQUEST;

        $this->response(['response' => $data_response], $http_response);

    }


}
