<?php

if (!defined('BASEPATH'))
    exit('Acceso Denegado');

class Mdl_users extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function vud_users_record(){
        $query = "SELECT * from vud_users_record();";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

     public function vud_role(){
        $query = "SELECT * FROM vud_role();";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_insert_user($data){
        $query = "SELECT vud_insert_user('$data[user_name]',
                                        $data[employee_number],
                                        '$data[description]',
                                        $data[role],
                                        $data[user_register],
                                        '$data[user_password]');";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }


    public function vud_update_user($data){
        $query = "SELECT vud_update_user($data[id_user],
                                        '$data[user_name]',
                                        $data[employee_number],
                                        '$data[description]',
                                        $data[role],
                                        $data[user_register],
                                        '$data[user_password]');";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_delete_user($id_user){
        $query = "SELECT vud_delete_user($id_user)";
        $result = $this->db->query($query);
        if($result->num_rows()>0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_users_permisions($id_user){
        $query = "SELECT * FROM vud_users_permisions($id_user);";
        $result = $this->db->query($query);
        if($result->num_rows()>0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_insert_user_permission($data,$permission){
        $query ="DELETE FROM permission where id_user = ".$data['id_user_permission'].";";
        $result = $this->db->query($query);
        
                $data['_read_permission']= "NULL";
                $data['_write_permission']= "NULL";
                $data['_delete_permission']= "NULL";
                $data['_update_permission']= "NULL";

        foreach ($permission as $module => $value){
            foreach ($value as $key => $permiso){
                if ($permiso == "R"){
                    $data['_read_permission']= 1;
                }
                if ($permiso == "W"){
                    $data['_write_permission']= 1;
                }
                if ($permiso == "D"){
                    $data['_delete_permission']= 1;
                }
                if ($permiso == "U"){
                    $data['_update_permission']= 1;
                }

            }
            $query = "SELECT * FROM vud_insert_user_permission(".$data['id_user_permission'].",    
                ".$module.",
                ".$data['_read_permission'].",
                ".$data['_write_permission'].",
                ".$data['_delete_permission'].",
                ".$data['_update_permission'].")";
                $result = $this->db->query($query);

                $data['_read_permission']= "NULL";
                $data['_write_permission']= "NULL";
                $data['_delete_permission']= "NULL";
                $data['_update_permission']= "NULL";
        }
        if($result->num_rows()>0)
            return $result->result_array();
        else
            return false;
    }


}

?>