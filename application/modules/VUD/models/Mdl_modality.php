<?php

if (!defined('BASEPATH'))
    exit('Acceso Denegado');

class Mdl_modality extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function vud_modality_record(){
        $query = "SELECT * FROM vud_modality_record();";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_create_modality($data,$resquirements){
        $query = "SELECT * FROM vud_create_modality($data[modality_code],
                                                    '$data[modality_description]',
                                                    '$data[modality_legal_foundament]',
                                                    $data[modality_transact],
                                                    $data[modality_days],
                                                    '$data[type_days_modality]')";                           
        $result = $this->db->query($query);
        $code_modality = $result->result_array();
        foreach ($resquirements as $key => $value) {
            $query = "SELECT * FROM vud_relation_modality_requirement(".$code_modality[0]['code_modality'].",".$value.")";
            $result = $this->db->query($query);
        }
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_update_modality($data,$resquirements){
        $query = "SELECT * FROM vud_update_modality($data[udpate_id_modality],
                                                    '$data[update_modality_description]',
                                                    '$data[update_modality_legal_foundament]',
                                                     $data[update_modality_days],
                                                     '$data[update_type_days_modality]')";                           
        $result = $this->db->query($query);
        if ($resquirements) {
            $query = "SELECT vud_eliminate_relation_modality($data[udpate_code_modality])";
            $result = $this->db->query($query);
            foreach ($resquirements as $key => $value) {
            $query = "SELECT * FROM vud_relation_modality_requirement($data[udpate_code_modality],".$value.")";
            $result = $this->db->query($query);
            }    
        }
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_delete_modality($id_modality){
        $query = "SELECT vud_delete_modality(".$id_modality.");";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_record_transact(){
        $query = "SELECT * from vud_record_transact();";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_record_requirement(){
        $query = "SELECT * FROM vud_record_requirement();";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_get_modality_requirements($_code_transact){
        $query = "SELECT * FROM vud_get_modality_requirements(".$_code_transact.");";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }


}

?>