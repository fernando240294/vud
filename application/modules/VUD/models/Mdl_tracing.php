<?php

if (!defined('BASEPATH'))
    exit('Acceso Denegado');

class Mdl_tracing extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    public function vud_transact_record(){
        $query = "SELECT * from vud_transact_record();";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_data_vulnerable_group(){
        $query = "select * from vud_data_vulnerable_group();";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_area_record(){
        $query = "SELECT * from vud_area_record();";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }


    

    public function vud_status_list(){
        $query = "SELECT * FROM vud_status_list();";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_transaction_data($code_transact){
        $query = "SELECT * FROM vud_transact_record_modality($code_transact);";
        $result  = $this->db->query($query);
        if($result->num_rows() > 0){
            $result = $result->result_array();
            $result['transact_status'] = false;
            return $result;
        }else{
            $query = "SELECT * FROM vud_transact_requeriment($code_transact);";
            $result  = $this->db->query($query);
            if ($result->num_rows() > 0){
                $result = $result->result_array();
                $result['transact_status'] = true;            
                return $result;    
            }else{
                return false;
            }
        }
    }

    public function vud_modality_data($code_modality){
        $query = "SELECT * FROM vud_modality_data($code_modality);";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }




    public function vud_request_record(){
        $query = "SELECT * FROM vud_request_record();";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_filter_record($filter){
        $query = "SELECT 
                        a.id,
                        a.folio,
                        a.id_user,
                        d.id as id_status,
                        d.description as description_status,
                        to_char(a.date_creation,'dd-mm-yyyy' ) as date_creation,
                        to_char(a.date_commitment,'dd-mm-yyyy' ) as date_commitment,
                        b.code,
                        b.description,
                        c.description as modality_desc,
                        e.id as id_turn,
                        f.id as id_dictum,
                        a.last_status,
                        a.settlement_number,
                        a.id_address,
                        a.id_interested,
                        f.folio_dictum
                    FROM 
                        public.request as a
                    left join 
                        public.cat_transact as b
                    on
                        a.id_transact = b.code 
                    left join 
                        public.cat_modality as c
                    on 
                        a.id_modality = c.code

                    left join 
                        public.cat_status as d
                    on
                        d.id = a.id_status
                    left join 
                        public.tracing_turn as e
                    on
                        e.id_request = a.id
                    left join
                        public.tracing_dictum as f
                    on
                        f.id_request = a.id
                    left join 
                        public.interested as g
                    on 
                        g.id = a.id_interested
                    WHERE
                        flag = 1
                    ".$filter."
                    order by id desc;";

        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_create_turn_request($data){
        $query = "SELECT * FROM vud_create_turn_request($data[id_request_turn],
                                                        '$data[request_turn_date]',
                                                        $data[request_turn_area],
                                                        '$data[request_turn_observations]',
                                                        $data[id_status_turn])";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }




    public function vud_create_dictum_request($data){
        $query = "SELECT * FROM vud_create_dictum_request('$data[request_dictum_recibe_date]',
                                                          '$data[request_dictum_office]',
                                                           $data[request_dictum_answer],
                                                           $data[request_dictum_citizen],
                                                           $data[id_request_dictum]);";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_update_citizen_dictum($data){
        $query = "SELECT * FROM vud_update_citizen_dictum($data[id_request_dictum],
                                                          $data[request_dictum_citizen]);";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_view_data_request($id_request){
        $query = "SELECT * FROM vud_view_data_request(".$id_request.");";
        $result = $this->db->query($query);
        if ($result->num_rows()>0)
            return $result->result_array(); 
        else
            return false;
    }

    public function vud_prevent_request($data){
        if($data['id_prevent'] == 1 ){
            //se previene la solicitud
            $query = "SELECT * FROM vud_update_prevention($data[id_request],
                                                         '$data[request_prevent_date]',
                                                         $data[prevent_citizen_date]);";
            $result = $this->db->query($query);    
        }else{
            // se subsana la informacion de la solicitud
            $query = "SELECT * FROM vud_update_correct($data[id_request],
                                                      '$data[request_prevent_date]');";
            $result = $this->db->query($query);    
        }
        if ($result->num_rows()>0)
            return $result->result_array(); 
        else
            return false;
    }

    public function vud_update_status_request($data){
        $query = "SELECT * FROM vud_update_status_request($data[id_request],
                                                          $data[status_request],
                                                          $data[last_status],
                                                          '$data[request_cancelation_desc]');";
        $result = $this->db->query($query);  
        if ($result)
            return $result; 
        else
            return false;
    }

    public function vud_activate_request($data){
        $query = "SELECT * FROM vud_activate_request($data[id_request],
                                                          $data[status_request],
                                                          $data[last_status]);";
        $result = $this->db->query($query);  
        if ($result)
            return $result; 
        else
            return false;
    }

    public function vud_create_settlement($data){
        $query = "SELECT * FROM vud_create_settlement( $data[id_request_settlement],
                                                      '$data[request_settlement_date]',
                                                      '$data[request_settlement_number]');";
        $result = $this->db->query($query);
        if ($result)
            return $result; 
        else
            return false;
    }

    public function vud_update_request_info($data){
        $query = "SELECT * FROM vud_update_request_info(".$data['id_request'].");";
        
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }


    public function vud_record_request_requirement($id_request){
        $query = "SELECT * FROM vud_record_request_requirement(".$id_request.");"; 
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_get_request_requirement($code,$type_request){
        $query = "SELECT * FROM vud_get_request_requirement(".$code.",".$type_request.");"; 
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }


    public function vud_update_request($request,$interested,$address,$requeriments){
        $query = "SELECT * FROM vud_update_data_address( $address[update_id_address],
                                                        '$address[update_codigoPostal]',
                                                        '$address[colonia]',
                                                        '$address[update_ext_number]',
                                                        '$address[update_int_number]',
                                                        '$address[update_street]',
                                                        '$address[delegacion]')";

        $this->db->query($query);
        $query = "SELECT * FROM vud_update_data_interested( $interested[update_id_interested],
                                                            '$interested[name_update]',
                                                            '$interested[update_laste_name]',
                                                            '$interested[update_e_mail]',
                                                            '$interested[update_phone]',
                                                            $interested[tipo_de_persona]);";
        $this->db->query($query);
        $query = "SELECT * FROM vud_update_data_request($request[update_id_request],
                                                        '$request[observations_update]',
                                                        $request[update_status],
                                                        $request[transact_code_edit],
                                                        $request[edit_group_vulnerable],
                                                        '$request[update_turn]',
                                                        '$request[update_type_work]',
                                                        $request[medio_por_el_cual_solicita],
                                                        $request[tipo_de_documento],
                                                        $request[modality_code_edit]);";
        $result = $this->db->query($query);
            if(count($requeriments) > 0){
                foreach($requeriments as $key => $value){
                $query ="SELECT * FROM vud_update_request_requeriment(".$request['update_id_request'].",
                                                                      ".$value.");"; 
                    $result = $this->db->query($query);
                }
            }            
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;

    }
    
}

?>