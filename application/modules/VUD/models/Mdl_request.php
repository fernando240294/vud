<?php

if (!defined('BASEPATH'))
    exit('Acceso Denegado');

class Mdl_request extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function vud_transact_record(){
        $query = "SELECT * from vud_transact_record();";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

     public function vud_role(){
        $query = "SELECT * FROM vud_role();";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_transaction_data($code_transact){
        $query = "SELECT * FROM vud_transact_record_modality($code_transact);";
        $result  = $this->db->query($query);
        if($result->num_rows() > 0){
            $result = $result->result_array();
            $result['transact_status'] = false;
            return $result;
        }else{
            $query = "SELECT * FROM vud_transact_requeriment($code_transact);";
            $result  = $this->db->query($query);
            if ($result->num_rows() > 0){
                $result = $result->result_array();
                $result['transact_status'] = true;            
                return $result;    
            }else{
                return false;
            }
        }

    }

    public function vud_modality_data($code_transact){
        $query = "SELECT * FROM vud_modality_data($code_transact);";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_data_vulnerable_group(){
        $query = "select * from vud_data_vulnerable_group();";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_transact_days($request){
        $query = "select * from vud_transact_days(".$request['tramite'].");";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_modality_days($request){
        $query = "select * from vud_modality_days(".$request['modalidad'].");";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_get_last_folio(){
        $query = "SELECT * FROM vud_get_last_folio();";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_request_data_save($address,$interested,$request,$requisitos){
        $query  ="SELECT * from vud_request_address('$address[codigoPostal]',
                                                    '$address[colonia]',
                                                    '$address[noExterior]',
                                                    '$address[noInterior]',
                                                    '$address[calle]',
                                                    '$address[delegacion]')";
        $result = $this->db->query($query);
        $result = $result->result_array();
        $address['id_request_address'] = $result[0]['vud_request_address'];

        $query  = "SELECT * FROM vud_request_interested('$interested[nombre]',
                                                        '$interested[lastname]',
                                                        '$interested[email]',
                                                        '$interested[telefono]',
                                                        $interested[tipoDePersona])";
        $result = $this->db->query($query);
        $result = $result->result_array();
        $address['id_request_interested'] = $result[0]['vud_request_interested'];

        $query  = "SELECT * FROM vud_request_data($request[user_register],
                                                        '$request[folio]',
                                                        '$request[observaciones]',
                                                        $request[id_status],
                                                        $address[id_request_address],
                                                        $address[id_request_interested],
                                                        $request[tramite],
                                                        $request[grupoVulnerable],
                                                        '$request[giro]',
                                                        '$request[tipoDeObra]',
                                                        $request[medioDeSolicitud],
                                                        $request[cartaPoderFormato],
                                                        '$request[FechaComprpomiso]',
                                                        $request[modalidad])";
        $result = $this->db->query($query);
        $result = $result->result_array();
        //if (count($requisitos)> 0) {
            foreach($requisitos as $key => $value){
                $query ="SELECT * FROM vud_create_request_requeriment('".$result[0]['vud_request_data']."',
                                                                       ".$value.");"; 
                $this->db->query($query);
            }

        //}

        if (count($result) > 0)
            return $result;
        else
            return false;
    }




}

?>