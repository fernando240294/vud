<?php

if (!defined('BASEPATH'))
    exit('Acceso Denegado');

class Mdl_transact extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    public function vud_record_transact(){
        $query = "SELECT * from vud_record_transact();";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_insert_transact($data,$requirements){
        $query = "SELECT * FROM vud_insert_transact($data[transact_code],
                                            '$data[transact_description]',
                                            '$data[transact_foundament]',
                                             $data[transact_days],
                                            '$data[type_days_transact]');";
        $result = $this->db->query($query);
        $id_code = $result->result_array();
            if($requirements){
                foreach ($requirements as $key => $value) {
                    $query = "SELECT vud_relation_transaction_requirement(".$id_code[0]['_id_code'].",".$value.")";
                    $result = $this->db->query($query);
                }
            }

        if($result->num_rows()>0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_update_transact($data,$requeriments){
        $query = "SELECT vud_update_transact($data[id_update_transact],
                                            '$data[update_transact_description]',
                                            '$data[update_transact_foundament]',
                                             $data[update_transact_days],
                                            '$data[update_type_days_transact]');";
        $result = $this->db->query($query);
        if (count($requeriments) > 0){
            $query = "SELECT vud_eliminate_relation_transact($data[code_update_transact]);";
            $result = $this->db->query($query);
            foreach ($requeriments as $key => $value){
                $query ="SELECT vud_relation_transaction_requirement($data[code_update_transact],".$value.")";
                $result = $this->db->query($query);
            }
        }
        if ($result->num_rows()>0) 
            return $result->result_array();
        else
            return false;
    }

    public function vud_delete_transact($id_transact){
        $query = "SELECT vud_delete_transact($id_transact);";
        $result = $this->db->query($query);
        if ($result->num_rows()>0)
            return $result->result_array();
        else 
            return false;
    }

    public function vud_get_transact_requirements($_code_transact){
        $query = "SELECT * FROM vud_get_transact_requirements($_code_transact);";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_record_requirement(){
        $query = "SELECT * FROM vud_record_requirement();";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

}

?>