<?php

if (!defined('BASEPATH'))
    exit('Acceso Denegado');

class Mdl_report extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function vud_status_list(){
        $query = "SELECT * FROM vud_status_list();";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_record_area_revisora(){
        $query = "SELECT * FROM vud_record_area_revisora();";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_record_vulnerable_group(){
        $query = "SELECT * FROM vud_record_vulnerable_group();";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_record_delegation(){
        $query = "SELECT 
                    distinct(a.delegation) as delegation
                FROM
                    public.address AS a 
                ORDER BY a.delegation ASC;";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_record_report(){
        $query = "SELECT * FROM vud_record_report()";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_year_report($year){
        $query = "SELECT * FROM vud_year_report('".$year."')";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    

    public function vud_report_filter($column,$filter){
        $query = "SELECT 
                        a.folio,
                        b.code as Clave_Tramite,
                        b.description as Tramite,
                        c.code as Clave_Modalidad,
                        c.description as Modalidad,
                        to_char(a.date_creation,'dd-mm-yyyy' ) as fecha_creacion,
                        to_char(a.date_commitment,'dd-mm-yyyy') as fecha_compromiso
                        ".$column."
                    FROM
                        public.request as a
                    left join
                        public.cat_transact as b
                    on
                        a.id_transact = b.code
                    left join 
                        public.cat_modality as c
                    on 
                        a.id_modality = c.code
                    left join 
                        public.address as d
                    on
                        d.id = a.id_address
                    left join
                        public.tracing_turn as e
                    on 
                        e.id_request = a.id
                    left join
                        public.tracing_dictum as f
                    on 
                        f.id_request = a.id
                    left join
                        public.interested as g
                    on g.id = a.id_interested
                    left join
                        public.cat_status h
                    on 
                        h.id = a.id_status
                    left join
                        public.vulnerable_group i
                    on 
                        i.id = a.id_vulnerable_group 
                    left join 
                        public.cat_area as j
                    on 
                        j.id = e.review_area

                    WHERE a.flag = 1
                    ".$filter."
                    order by a.id asc;";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_get_report(){
        $query="SELECT
                    a.folio,
                    b.code,
                    b.description,
                    c.code,
                    c.description,
                    to_char(a.date_creation,'dd-mm-yyyy' ),
                    to_char(a.date_commitment,'dd-mm-yyyy' )
                FROM
                    public.request as a
                left join
                    public.cat_transact as b
                on
                    a.id_transact = b.code
                left join 
                    public.cat_modality as c
                on 
                    a.id_modality = c.code
                order by a.id desc";

        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;

    }

}

?>