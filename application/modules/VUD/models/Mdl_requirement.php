<?php

if (!defined('BASEPATH'))
    exit('Acceso Denegado');

class Mdl_requirement extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    public function vud_record_requirement(){
        $query = "SELECT * FROM vud_record_requirement();";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_create_requirement($requirement){

        foreach ($requirement as $key => $value){
            $query = "SELECT * FROM vud_create_requirement('".$value."');";
            $result = $this->db->query($query);
        }

        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }

    public function vud_update_requirement($data){
        $query  = "SELECT * FROM vud_update_requirement($data[id_update_requirement],
                                                        '$data[update_requirement_description]')";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;

    }

    public function vud_delete_requirement($id_requirement){
        $query = "SELECT * FROM vud_delete_requirement($id_requirement);";
        $result  = $this->db->query($query);
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return false;
    }
    
}

?>