<div id="main_menu">
    <div class="container-fluid">
        <h1 class="text-center">Reportes</h1>
        <section id="filtro">
            <div class="container">
                    <form id="form_filter_report" name="form_filter_report" method="post" target="_blank" action="<?=ROOT_URL?>VUD/Ctr_report/CreateReport">
                <!--<form id="form_filter_report" name="form_filter_report" onsubmit="return report_create();">-->
                <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="report_ejecicio">Ejercicio:</label>
                                <input type="number" name="report_ejecicio" id="report_ejecicio" class="form-control" placeholder="año" aria-describedby="">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label for="report_delegacion">Delegación</label>
                            <select class="form-control" id="report_delegacion" name="report_delegacion">
                                <option value="">-Selecciona-</option>
                                <?=$delegation;?>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <label for="report_estatus">Estatus</label>
                            <select class="form-control" id="report_estatus" name="report_estatus">
                                <option value="">-Selecciona-</option>
                                <?=$status?>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <label for="report_area_revisorsa">Área revisora</label>
                            <select class="form-control" id="report_area_revisorsa" name="report_area_revisorsa">
                                <option value="">-Selecciona-</option>
                                <?=$area?>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <label for="report_resp_dictamen">Respuesta de dictamen</label>
                            <select class="form-control" id="report_resp_dictamen" name="report_resp_dictamen">
                                <option value="">-Selecciona-</option>
                                <option value="1">Autorizada</option>
                                <option value="2">Rechazada</option>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <label for="report_tipo_persona">Tipo de persona</label>
                            <select class="form-control" id="report_tipo_persona" name="report_tipo_persona">
                                <option value="">-Selecciona-</option>
                                <option value="2">Fisica</option>
                                <option value="1">Moral</option>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <label for="report_grupo_vulnerable">Grupo vulnerable</label>
                            <select class="form-control" id="report_grupo_vulnerable" name="report_grupo_vulnerable">
                                <option value="">-Selecciona-</option>
                                <?=$grupo_vulnerable;?>
                            </select>
                        </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="report_fecha_inicio">Fecha inicio</label>
                                    <input type="date" name="report_fecha_inicio" id="report_fecha_inicio" class="form-control" placeholder="RFC" aria-describedby="">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="report_fecha_final">Fecha fin</label>
                                    <input type="date" name="report_fecha_final" id="report_fecha_final" class="form-control" placeholder="Teléfono" aria-describedby="">
                                </div>
                            </div>
                        <div class="container-fluid">
                            <button type="submit" class="btn offset-md-4 btn-outline-success" >Generar reporte</button>
                            <button type="button" id="clean_filter" class="btn offset-md-1 btn-outline-danger">Limpiar filtros</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>            
    <br>
</div>


<div class="container-fluid" id="year_report" style="display: none">
    <form id="form_filter_year_report" name="form_filter_year_report" method="post" target="_blank" action="<?=ROOT_URL?>VUD/Ctr_report/ReportYear">
            <div class="container-fluid">
                <h1 class="text-center">Reporte general</h1>
                <div class="col-md-4 offset-md-4">
                    <div class="form-group ">
                        <label for="report_fecha_final" class="offset-md-6">Año:</label>
                        <input type="number" name="report_full_year" id="report_full_year" class="form-control" aria-describedby="">
                    </div>
                </div>
            <button type="submit" class="btn col-md-4 offset-md-4 btn-outline-primary" >Generar</button>
        </div>
        </form>
</div>