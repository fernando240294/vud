<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>VUD-GAM</title>

    <link rel="stylesheet" href="<?=URL_TEMPLATE?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=URL_TEMPLATE?>css/style.css">
    <link rel="stylesheet" href="<?=URL_TEMPLATE?>css/processing.css">
    <link rel="stylesheet" href="<?=URL_TEMPLATE?>fonts/css/font-awesome.css">
    <script src="<?=URL_TEMPLATE?>js/jquery.js"></script>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</head>

<style>
    body{
      background-image: url("<?=URL_ASSETS?>img/del_background2.jpg");
      background-size: 100% 100%;
      background-attachment: fixed;
    }
    
    div.transbox {
      margin: 30px;
      background-color: #ffffff;
      border: 1px solid black;
      opacity: 0.0;
      filter: alpha(opacity=60); /* For IE8 and earlier */
    }

    div.transparent label{
      color: #ffffff;
    }

</style>
<body class="fixed-nav sticky-footer">


  <div class="container" style="padding-top: 400px">
    <div class="card card-login mx-auto mt-5 transbox">
   
    </div>
       <div class="card-body transparent">
        <form onsubmit="return validarUser();">
          <div class="form-group">
            <label for="exampleInputEmail1">Usuario</label>
            <input class="form-control" id="login_username" type="text" aria-describedby="emailHelp" placeholder="Ingresar Usuario">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Contraseña</label>
            <input class="form-control" id="login_password" type="password" placeholder="Contraseña">
          </div>
          <button class="btn btn-primary btn-block">Entrar</button>
        </form>
        <div class="form-group" id="error_login">

        </div>
      </div>
  </div>