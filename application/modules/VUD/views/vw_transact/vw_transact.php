  <div class="content-wrapper">
  	<div class="container-fluid">
          <div class="card-body">
          <?= $transact ?>
          </div>
  	    <!-- Add user Modal-->
        <div class="modal fade " id="add_transact" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar Trámite</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                  <form id="form_transact_create" name="form_transact_create" onsubmit="return transact_create();">
                    <div class="form-group">
                      <div class="form-row">
                        <div class="col-md-6">
                          <div class="form-label-group">
                            <label for="firstName">Clave</label>
                            <input type="number" id="transact_code" name="transact_code" class="form-control" required>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-label-group">
                            <label for="lastName">Descripción</label>
                            <input type="text" id="transact_description" name="transact_description" class="form-control" required>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-label-group">
                            <label for="transact_days">Días de respuesta</label>
                            <input data-toggle="tooltip" data-placement="top" title="En caso de ser inmediato asignar 0" type="text" id="transact_days" name="transact_days" class="form-control" required>
                          </div>
                        </div>

                      <div class="col-md-6">
                        <label for="type_days_transact">Tipos de días</label>
                          <select class="form-control" id="type_days_transact" name="type_days_transact" required>
                            <option value="">Seleccione</option>
                            <option value="0" >Inmediato</option>
                            <option value="N">Naturales</option>
                            <option value="H">Hábiles</option>
                          </select>
                        </div>

                      </div>
                    </div>
                    <div class="form-group">
                      <div class="form-label-group">
                        <label for="inputEmail">Fundamento Jurídico</label>
                        <input type="text" id="transact_foundament" name="transact_foundament" class="form-control" required>
                      </div>
                    </div>
              
                    <h5>Requisitos</h5> 
                    <div class="container-fluid check-requirements-add"> 

                      <?=$select_requirement?>
                    </div>

                    <div class="modal-footer">
                      <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                      <button class="btn btn-success" type="submit" >Agregar</button>
                    </div>
                  </form>
              </div>
              
            </div>
          </div>
        </div>



          	    <!-- Add user Modal-->
        <div class="modal fade " id="vud_update_transact" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar Tramite</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                  <form id="form_transact_update" name="form_transact_update" onsubmit="return transact_update();">
                    <div class="form-group">
                      <div class="form-row">
                        <div class="col-md-6">
                          <div class="form-label-group">
                            <label for="firstName">Clave</label>
                            <input type="number" id="update_transact_code" name="update_transact_code" class="form-control">
                          </div>
                        </div>
                        
                        <div class="col-md-6">
                          <div class="form-label-group">
                            <label for="lastName">Descripción</label>
                            <input type="text" id="update_transact_description" name="update_transact_description" class="form-control">
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-label-group">
                            <label for="update_transact_days">Días de respuesta</label>
                            <input data-toggle="tooltip" data-placement="top" title="En caso de ser inmediato asignar 0" type="text" id="update_transact_days" name="update_transact_days" class="form-control" required>
                          </div>
                        </div>


                      <div class="col-md-6">
                        <label for="update_type_days_transact">Tipos de días</label>
                          <select class="form-control" id="update_type_days_transact" name="update_type_days_transact" required>
                            <option value="">Seleccione</option>
                            <option value="0" >Inmediato</option>
                            <option value="N">Naturales</option>
                            <option value="H">Hábiles</option>
                          </select>
                        </div>


                      </div>
                    </div>
                    <div class="form-group">
                      <div class="form-label-group">
                        <label for="inputEmail">Fundamento Jurídico</label>
                        <input type="text" id="update_transact_foundament" name="update_transact_foundament" class="form-control">
                      </div>
                    </div>

                    <h5 class="text-update">Requisitos</h5> 
                    <div class="container-fluid check-requirements"> 
                      
                    </div>

                    <input type="hidden" id="id_update_transact" name="id_update_transact">
                    <input type="hidden" id="code_update_transact" name="code_update_transact">
                    <div class="modal-footer">
                      <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                      <button class="btn btn-success" type="submit" >Editar</button>
                    </div>
                  </form>
              </div>
              
            </div>
          </div>
        </div>



  	<footer class="sticky-footer">
    </footer>
    
  </div>