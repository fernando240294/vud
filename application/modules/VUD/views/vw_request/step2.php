 <!-- Contenido  -->
<div class="container">

    <section id="datos_del_interesado">
        <h2 class="text-center">Datos del interesado</h2>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="nombre">Nombre(s) / Razón social *</label>
                        <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre(s)*" aria-describedby="">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="apelidoPaterno">Apellido paterno</label>
                        <input type="text" name="apelidoPaterno" id="apelidoPaterno" class="form-control" placeholder="Apellido Paterno*" aria-describedby="">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="apellidoMaterno">Apellido materno</label>
                        <input type="text" name="apellidoMaterno" id="apellidoMaterno" class="form-control" placeholder="Apellido Materno*" aria-describedby="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label for="identificacionOficial">Identificación oficial</label>
                    <select class="form-control" id="identificacionOficial" name="identificacionOficial">
                        <option value="">--Identificación oficial--</option>
                        <option value="1"> Si</option>
                        <option value="0"> No</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="sexo">Sexo</label>
                    <select class="form-control" id="sexo" name="sexo">
                        <option value="">--Sexo--</option>
                        <option value="1"> Masculino </option>
                        <option value="2"> Femenino </option>
                        <option value="3"> No especificado </option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="grupoVulnerable">Grupo vulnerable *</label>
                    <select class="form-control" id="grupoVulnerable" name="grupoVulnerable">
                        <option value="">--Grupo vulnerable--</option>
                        
                    </select>
                </div>

            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="rfc">RFC</label>
                        <input type="text" name="rfc" id="rfc" class="form-control" placeholder="RFC" aria-describedby="">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="telefono">Teléfono*</label>
                        <input type="number" name="telefono" id="telefono" class="form-control" placeholder="Teléfono" aria-describedby="">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="email">Correo electrónico</label>
                        <input type="email" name="email" id="email" class="form-control" placeholder="Correo electrónico" aria-describedby="">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="direccion">
        <h2 class="text-center">Dirección</h2>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="codigoPostal">Código postal *</label>
                        <input type="number" name="codigoPostal" id="codigoPostal" class="form-control" placeholder="Código Postal" aria-describedby="">
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="form-group" id="coloniaGroup">
                        <label for="colonia">Colonia *</label>
                        <input type="text" name="colonia" id="colonia" class="form-control" placeholder="Colonia" aria-describedby="">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group" id="delegacionGroup">
                        <label for="delegacion">Delegación *</label>
                        <input type="text" name="delegacion" id="delegacion" class="form-control" placeholder="Delegación" aria-describedby="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="calle">Calle*</label>
                        <input type="text" name="calle" id="calle" class="form-control" placeholder="Calle" aria-describedby="">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="noExterior">Número exterior</label>
                        <input type="text" name="noExterior" id="noExterior" class="form-control" placeholder="Número exterior" aria-describedby="">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="noInterior">Número interior</label>
                        <input type="text" name="noInterior" id="noInterior" class="form-control" placeholder="Número interior" aria-describedby="">
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section id="datos_del_establecimiento">
        <h2 class="text-center">Datos del establecimiento</h2>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="giro">Giro o uso</label>
                        <input type="text" name="giro" id="giro" class="form-control" placeholder="Giro o Uso" aria-describedby="">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="tipoDeObra">Tipo de obra</label>
                        <input type="text" name="tipoDeObra" id="tipoDeObra" class="form-control" placeholder="Tipo de obra" aria-describedby="">
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="terminos">
                    <p>Autorizado en términos de los artículos 41 y 42 de la Ley de Procedimiento Administrativo del Distrito
                        Federal</p>

                        <label class="input-vud" for="cartaPoder">
                            <input type="radio" value="1" id="cartaPoder" name="tipo_de_documento">Carta Poder
                        </label>
                        
                        <label class="input-vud" for="formato">
                            <input type="radio" value="2" id="formato" name="tipo_de_documento">Formato
                        </label>
                </div>
            </div>
            <div class="row">
                <div class="col offset-md-4">
                    <fieldset>
                        <h4>Medio por el cual solicita</h4>
                        
                        <label class="input-vud offset-md-1" for="solicitado_en_linea" id >
                            <input type="radio" value="1" id="solicitado_en_linea" name="medio_por_el_cual_solicita">En línea
                        </label>
                        
                        <label class="input-vud" for="solicitado_presencialmente">
                            <input type="radio" value="2" id="solicitado_presencialmente" name="medio_por_el_cual_solicita">Presencial
                        </label>
                    </fieldset>
                </div>
                

            </div>
    
        </div>
    </section>




</div>



<style>
        *{
            margin: 0;
            padding: 0;
        }
         .modal-header{
            background-color: #787A7C;
            
        }
        #barraLateral{
            background-color: #B3BAC3;
            height: 100%;
            width: 8%;
            top: 0;
            left: 0;
            position: absolute;
            margin-top: -1px;
        }
        
        #previsualizacion{
            margin-left: 12%; 
         
        }
        
        
        .grid-container{
            display: grid; 
            grid-template-columns: 200px auto;
            grid-gap: 10px 1px;
            padding-bottom: 15px;
            padding-top: 15px;
            
        }
        .titulo{
            font-weight: 600;
            font-size: 18px;
        }
    
    </style>

    <!-- MOodal -->

 <!-- Button trigger modal -->
 <div class="container" style="padding-bottom: 50px"><button type="button" class="btn btn-outline-primary btn-lg offset-md-5" data-toggle="modal" data-target="#revision_formulario" id="crearModal" >
   Revisar
 </button>
 </div>
 
 <!-- Modal -->
 <div class="modal fade" id="revision_formulario" data-backdrop="static" data-keyboard="false"  tabindex="-1" role="dialog" aria-labelledby="datos_para_enviar" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close delete-info-modal" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
                 <h4 class="modal-title" id="datos_para_enviar"></h4>
             </div>
             <div class="modal-body">
                    <div id="barraLateral"></div>
                    
                    <div id="previsualizacion">
                        <h2>Datos de la solicitud del trámite</h2>
                       <div id="datos_de_la_solicitud_modal">
                               
                       </div>
                        <h2>Datos del interesado</h2>
                        <div id="datos_del_interesado_modal">
                            
                        </div>
                        <h2>Dirección</h2>
                        <div id="direccion_modal">
                            
                        </div>
                        <h2>Datos del establecimiento</h2>
                        <div id="datos_del_establecimiento_modal">
                            
                            
                        </div>
                    </div>
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-secondary delete-info-modal" id="delete-info-modal" data-dismiss="modal">Cancelar</button>
                 <button type="button" class="btn btn-primary" id="guardarTramite">Guardar</button>
             </div>
         </div>
     </div>
 </div>
