<h1 class="text-center">Requisitos del trámite</h1>

        <!-- Campos: tipo de persona ; Trámite ; Modalidad -->
        <div class="row">
            <div class="col-md-2">
                <h4 class="text-center">Persona *</h4>
                <br>
                <div align="center">
                <fieldset id="tipo_de_persona">

                    <label for="tipo_moral">
                        <input class="type_person_check" type="radio" id="tipo_moral" value="1" name="tipo_de_persona"> Moral
                    </label>

                    <label for="tipo_fisica">
                        <input class="type_person_check" type="radio" id="tipo_fisica" value="2" name="tipo_de_persona"> Física
                    </label>
                </fieldset>
                </div>
            </div>
            <div class="col-md-5 text-center">
                <h4 class="text-center"> Trámite *</h4>
                <div class="form-group" id="buscador">
                    <label for="search"></label>
                    <input value="" type="text" name="search" id="search" class="form-control" placeholder="Busque su trámite" aria-describedby="">
                </div>
            </div>
            <div class="col-md-5 ">
                <h4 class="text-center">Modalidad *</h4>
                <br>
                <div class="form-label-group">
                    <select class="form-control" id="modalidades" name="modalidad" data-modalidad-seleccionada="0">
                        <option value="">--Modalidad--</option>

                    </select>
                </div>

            </div>

        </div>

        <!-- Elemento para el buscador -->
        <div id="resultado_de_la_busqueda"></div>

        <!-- Requisitos del trámite, observaciones y fundamento jurídico -->
        <div class="row">
            <div class="col">
                <section>
                    <h2 class="text-center">Requisitos del trámite *</h2>
        <div>
            <label for="all">
                <input class="all" type="checkbox" id="mark_all" value=""> Marcar Todos
            </label>
        </div>
                    <div class="contenedor-requisitos">
                        <ul id="requisitos_del_tramite">
                          
                        </ul>
                    </div>
                </section>
            </div>
            <div class="col">

                <section id="observaciones">
                    <h2 class="text-center">Observaciones</h2>
                    <div class="form-group">
                        <label for="comment"></label>
                        <textarea class="form-control" rows="5" name="observaciones" id="comentarios"></textarea>
                    </div>
                </section>

                <section>
                    <div id="fundamentoJuridico" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header" role="tab" id="contenidoFundamentoJuridico">
                                <h5 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#fundamentoJuridico" href="#contenedor_fundamento_juridico" aria-expanded="true" aria-controls="contenedor_fundamento_juridico">
                                        Fundamento Jurídico
                                    </a>
                                </h5>
                            </div>
                            <div id="contenedor_fundamento_juridico" class="collapse in" role="tabpanel" aria-labelledby="contenidoFundamentoJuridico">
                                <div class="card-body" id="contenedor-de-fundamento-juridico">

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2 offset-md-4">
                <h4 class="text-center">Ingreso *</h4>
                <br>
                <fieldset id="tipo_de_persona">

                    <label for="completo">
                        <input class="entry_check" type="radio" id="completo" value="1" name="entry_check"> Completa
                    </label>

                    <label for="art_49">
                        <input class="entry_check" type="radio" id="art_49" value="2" name="entry_check"> Art. 49
                    </label>
                </fieldset>
            </div>
        </div>