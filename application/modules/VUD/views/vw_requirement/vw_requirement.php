  <div class="content-wrapper">
  	<div class="container-fluid">
        <div class="card-body">
        <?= $requirement ?>
        </div>

  	    <!-- Add user Modal-->
        <div class="modal fade " id="add_transact" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar Requisitos</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                  <form id="form_requirement_create" name="form_requirement_create" onsubmit="return requirement_create();">
                    <div class="form-group">
                      <div class="form-row" id="requirements_area">
                      </div>
                    </div>
                    
                    <div>
                      
                    </div>

                    <div class="modal-footer">
                      <button class="btn btn-success" type="button" id="add_modality" >+</button>
                      <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                      <button class="btn btn-success" type="submit" >Agregar</button>
                    </div>
                  </form>
              </div>
              
            </div>
          </div>
        </div>



        <!-- Add user Modal-->
        <div class="modal fade " id="vud_update_requirement" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar Requisito</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                  <form id="form_requirement_update" name="form_requirement_update" onsubmit="return requirement_update();">
                    <div class="form-group">
                      <div class="form-row">
                        <div class="col-md-6">
                          <div class="form-label-group">
                            <label for="lastName">Descripción</label>
                            <input type="text" id="update_requirement_description" name="update_requirement_description" class="form-control">
                          </div>
                        </div>
                      </div>
                    </div>
                    <input type="hidden" id="id_update_requirement" name="id_update_requirement">
                    <div class="modal-footer">
                      <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                      <button class="btn btn-success" type="submit" >Editar</button>
                    </div>
                  </form>
              </div>
              
            </div>
          </div>
        </div>



  	<footer class="sticky-footer">
    </footer>
    
  </div>