  <div class="content-wrapper">
  	<div class="container-fluid">
        <div class="card-body">
        <?= $modality ?>
        </div>

  	    <!-- Add Modality Modal-->
        <div class="modal fade " id="add_transact" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar Modalidad</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                  <form id="form_modality_create" name="form_modality_create" onsubmit="return modality_create();">
                    <div class="form-group">
                      <div class="form-row" id="requirements_area">
                        <div class="col-md-12">
                          <div class="form-label-group">
                            <label for="role">Trámite de la modalidad a crear</label>
                              <select class="form-control" id="modality_transact" name="modality_transact" required>
                                <option value="">--Selecciona el Trámite--</option>
                                <?=$select_transact ?>
                              </select>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-label-group">
                            <label for="lastName">Clave</label>
                            <input type="number" id="modality_code" name="modality_code" class="form-control" required>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-label-group">
                            <label for="lastName">Descripción</label>
                            <input type="text" id="modality_description" name="modality_description" class="form-control" required>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-label-group">
                            <label for="modality_days">Días de respuesta</label>
                            <input data-toggle="tooltip" data-placement="top" title="En caso de ser inmediato asignar 0" type="text" id="modality_days" name="modality_days" class="form-control" required>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <label for="type_days_modality">Tipos de días</label>
                            <select class="form-control" id="type_days_modality" name="type_days_modality" required>
                              <option value="">Seleccione</option>
                              <option value="0" >Inmediato</option>
                              <option value="N">Naturales</option>
                              <option value="H">Habíles</option>
                            </select>
                          </div>

                        <div class="col-md-12">
                          <div class="form-label-group">
                            <label for="modality_legal_foundament">Fundamento Jurídico</label>
                            <textarea type="text" id="modality_legal_foundament" name="modality_legal_foundament" class="form-control" required></textarea>
                          </div>
                        </div>

                        <h5>Requisitos</h5> 
                        <div class="container-fluid check-requirements"> 
                          <?=$select_requirement?>
                        </div>

                      </div>
                      </div>
                    <div class="modal-footer">
                      <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                      <button class="btn btn-success" type="submit" >Agregar</button>
                    </div>
                  </form>
              </div>
              
            </div>
          </div>
        </div>



          <!-- Update Modality Modal-->
          <div class="modal fade " id="vud_update_transact" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Actualizar Modalidad</h5>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                    <form id="form_modality_update" name="form_modality_update" onsubmit="return modality_update();">
                      <div class="form-group">
                        <div class="form-row" id="requirements_area">
                          <div class="col-md-12">
                            <div class="form-label-group">
                              <label for="role">Trámite de la modalidad a crear</label>
                                <select class="form-control" id="update_modality_transact" name="update_modality_transact">
                                  <?=$select_transact ?>
                                </select>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-label-group">
                              <label for="lastName">Clave</label>
                              <input type="text" id="update_modality_code" name="update_modality_code" class="form-control">
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-label-group">
                              <label for="lastName">Descripción</label>
                              <input type="text" id="update_modality_description" name="update_modality_description" class="form-control">
                            </div>
                          </div>

                          <div class="col-md-6">
                          <div class="form-label-group">
                            <label for="update_transact_days">Días de respuesta</label>
                            <input data-toggle="tooltip" data-placement="top" title="En caso de ser inmediato asignar 0" type="text" id="update_modality_days" name="update_modality_days" class="form-control" required>
                          </div>
                        </div>


                      <div class="col-md-6">
                        <label for="update_type_days_transact">Tipos de días</label>
                          <select class="form-control" id="update_type_days_modality" name="update_type_days_modality" required>
                            <option value="">Seleccione</option>
                            <option value="0" >Inmediato</option>
                            <option value="N">Naturales</option>
                            <option value="H">Hábiles</option>
                          </select>
                        </div>

                          <div class="col-md-12">
                            <div class="form-label-group">
                              <label for="modality_legal_foundament">Fundamento Jurídico</label>
                              <textarea type="text" id="update_modality_legal_foundament" name="update_modality_legal_foundament" class="form-control"></textarea>
                            </div>
                          </div>

                          <br>
                          <h5>Requisitos</h5> 
                          <div class="container-fluid check-requirements">                        
                            <?=$select_requirement?>
                          </div>

                          <input type="hidden" id="udpate_id_modality" name="udpate_id_modality">
                          <input type="hidden" id="udpate_code_modality" name="udpate_code_modality">

                        </div>
                        </div>
                      <div class="modal-footer">
                        <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-success" type="submit" >Actualizar </button>
                      </div>
                    </form>
                </div>
              
                </div>
              </div>
            </div>

      	<footer class="sticky-footer">
        </footer>
        
      </div>