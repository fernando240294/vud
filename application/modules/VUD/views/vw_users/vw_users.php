  <div class="content-wrapper">
    <div class="container-fluid">
      <?= $users ?>
        <!-- Add user Modal-->
        <div class="modal fade " id="add_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar Usuario</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                  <form id="form_users_create" name="form_users_create_name" onsubmit="return users_create();">
                    <div class="form-group">
                      <div class="form-row">
                        <div class="col-md-6">
                          <div class="form-label-group">
                            <label for="firstName">Nombre de Usuario</label>
                            <input type="text" id="user_name" name="user_name" class="form-control" required>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-label-group">
                            <label for="lastName">Número de Empleado</label>
                            <input type="number" id="employee_number" name="employee_number" class="form-control" required>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="form-label-group">
                        <label for="inputEmail">Descripción</label>
                        <input type="text" id="description" name="description" class="form-control" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="form-row">
                        <div class="col-md-6">
                          <div class="form-label-group">
                            <label for="inputPassword">Contraseña</label>
                            <input type="password" id="user_password" name="user_password" class="form-control" required>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-label-group">
                            <label for="role">Rol</label>
                              <select class="form-control" id="role" name="role" required>
                                <option value="">--Selecciona--</option>
                                <?=$role?>
                              </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                      <button class="btn btn-success" type="submit" >Agregar</button>
                    </div>
                  </form>
              </div>
              
            </div>
          </div>
        </div>


        <div class="modal fade " id="vud_update_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Actualizar Usuario</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                   <form id="form_users_update" name="form_users_update" onsubmit="return users_update();">
                    <div class="form-group">
                      <div class="form-row">
                        <div class="col-md-6">
                          <div class="form-label-group">
                            <label for="firstName">Nombre de Usuario</label>
                            <input type="text" id="update_user_name" name="update_user_name" class="form-control" required>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-label-group">
                            <label for="lastName">Número de Empleado</label>
                            <input type="number" id="update_employee_number" name="update_employee_number" class="form-control" required>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="form-label-group">
                        <label for="inputEmail">Descripción</label>
                        <input type="text" id="update_description" name="update_description" class="form-control" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="form-row">
                        <div class="col-md-6">
                          <div class="form-label-group">
                            <label for="inputPassword">Contraseña</label>
                            <input type="password" id="update_user_password" name="update_user_password" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-label-group">
                            <label for="role">Rol</label>
                              <select class="form-control" id="update_role" name="update_role" required>
                                <option value="">--Selecciona--</option>
                                <?=$role?>
                              </select>
                          </div>
                        </div>
                        <input type="hidden" id="id_user" name="id_user">
                       <input type="hidden" id="id_user_update" name="id_user_update">
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                      <button class="btn btn-success" type="submit" >Actualizar</button>
                    </div>
                  </form>
              </div>
            </div>
          </div>
        </div>


        <div class="modal fade " id="vud_user_permission" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Permisos de Usuario</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                   <form id="form_users_permission" name="form_users_permission" onsubmit="return users_create_permissions();">
                    <div class="card mb-3">
                        <div class="card-body">
                      <div class="table-responsive">
                    <table class="table table-bordered" id="" width="100%" cellspacing="0">
                    <thead>
                       <tr>
                      <th>Modulo 1</th>
                      <th>Lectura</th>
                      <th>Escritura</th>
                      <th>Eliminacion</th>
                      <th>Edicion</th>
                    </tr>
                  </thead>
                  <tbody id="permission-body">
                    </tbody>
                    </table>
                    </div>
                    </div>
                  </div>
                    <div class="modal-footer">
                      <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                      <button class="btn btn-success" type="submit" >Actualizar</button>
                    </div>
                  </form>
              </div>
            </div>
          </div>
        </div>


    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      
    </footer>

  </div>