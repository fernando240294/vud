<div id="main_menu">
    <div class="container-fluid">
        <h1 class="text-center">Solicitudes</h1>
        <section id="filtro">
            <div class="container">
                    <form id="form_filter-data-tracing" name="form_filter-data-tracing" onsubmit="return Filter_Data();">
                <div class="row">
                    <div class="col-md-4">
                            <div class="form-group">
                                <label for="request-folio">Folio</label>
                                <input type="text" name="request-folio" id="request-folio" class="form-control" placeholder="Folio" aria-describedby="">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label for="transact_code">Trámite</label>   
                            <select class="form-control" id="transact_code" name="transact_code">
                                <option value="">-Selecciona-</option>
                                <?= $transact ?>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <label for="modality_code">Modalidad</label>   
                            <select class="form-control" id="modality_code" name="modality_code">
                                <option value="">-Selecciona-</option>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <label for="request-status">Estatus</label>
                            <select class="form-control" id="request-status" name="request_status">
                                <option value="">-Selecciona-</option>
                                <?=$status?>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <label for="request_type_person">Tipo persona</label>
                            <select class="form-control" id="request_type_person" name="request_type_person">
                                <option value="">-Selecciona-</option>
                                <option value="1">Moral</option>
                                <option value="2">Física</option>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="request_date_begin">Fecha inicio</label>
                                <input type="date" name="request_date_begin" id="request_date_begin" class="form-control" placeholder="RFC" aria-describedby="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="request_date_end">Fecha fin</label>
                                <input type="date" name="request_date_end" id="request_date_end" class="form-control" placeholder="Teléfono" aria-describedby="">
                            </div>
                        </div>
                        <div class="container-fluid">
                            <button type="submit" class="btn offset-md-4 btn-outline-primary" >Buscar</button>
                            <button type="button" id="tracing_all_record" class="btn offset-md-1 btn-outline-success">Ver solicitudes</button>
                            <button type="button" id="clean_filter" class="btn offset-md-1 btn-outline-danger">Limpiar</button>
                        </div>
          
                    </div>
                </form>
            </div>
        </section>
    </div>            
    <br>
    <div class="container-fluid" id="container_table">
        <div align="center">
            <img id="loading_gif" src="<?=URL_ASSETS."img/loading.gif"?>" style="display: none;">
        </div>
        
        <?=$table?>
    </div>

</div>


<!-- MODAL QUE TURNA LA SOLICITUD -->
<div class="modal fade" id="request-turn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <form id="form_request_turn" name="form_request_turn" onsubmit="return request_create_turn();">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Turnar solicitud de trámite</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
            <section id="filtro">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="request_dictum_recibe_date">Folio de solicitud</label>
                                <input type="text" name="request_turn_folio" id="request_turn_folio" class="form-control" >
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="request_turn_date">Fecha de turno *</label>
                                <input type="date" name="request_turn_date" id="request_turn_date" class="form-control" >
                            </div>
                        </div>

                        <div class="col-md-7">
                            <label for="sexo">Área revisora *</label>
                            <select class="form-control" id="request_turn_area" name="request_turn_area">
                                <option value="">-Selecciona-</option>
                                <?=$area?>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="comment">Observaciones</label>
                            <textarea class="form-control" rows="5" name="request_turn_observations" id="request_turn_observations"></textarea>
                        </div>
                        <input type="hidden" name="id_request_turn" id="id_request_turn">
                        <input type="hidden" name="id_status_turn" id="id_status_turn">
                    </div>
                </div>
            </section>
        </div>            

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-outline-primary">Turnar</button>
      </div>
    </div>
</form>
  </div>
</div>


<!-- MODAL DE PREVENCION Y SUBSANACION-->
<div class="modal fade" id="tracing_code_49" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <form id="form_request_prevent" name="form_request_prevent" onsubmit="return Prevencion_request();">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Prevención y Subsanación</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
            <section id="filtro">
                <div class="container">
                    <div class="row">
                    <form id="form_request_prevent" name="form_request_prevent" onsubmit="return ();">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="request_prevent_date">Fecha de prevención</label>
                                <input type="date" name="request_prevent_date" id="request_prevent_date" class="form-control"  required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="prevent_citizen_date">Fecha que recibe ciudadano</label>
                                <input type="date" name="prevent_citizen_date" id="prevent_citizen_date" class="form-control" >
                            </div>
                        </div>
                        <input type="hidden" name="id_prevent" value="1">
                        <input class="id_request_prevent" type="hidden" name="id_request" value="">
                        
                        <div class="container-fluid" >
                            <button type="submit" class="btn btn-outline-info">Prevenir</button>
                            <br>
                        </div>
                    </form>
                    </div>
                    <hr>
                    <form id="form_request_correct" name="form_request_correct" valor ="2" onsubmit="return Prevencion_correct();">
                    <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="request_turn_date">Fecha de subsanación</label>
                                    <input type="date" name="request_prevent_date" id="request_turn_date" class="form-control" >
                                </div>
                            </div>
                            <input type="hidden" name="id_prevent" id="id_prevent" value="2">
                            <input class="id_request_prevent" type="hidden" name="id_request" value="">
                            <div class="container-fluid" >
                                <button type="submit" class="btn btn-outline-info">Subsanar</button>
                                <br>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
</form>
  </div>
</div>


<!-- MODAL  DE RECEPCION DE DICTAMEN-->
<div class="modal fade" id="request-dictum" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <form id="form_request_dictum" name="form_request_dictum" onsubmit="return request_create_dictum();">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Recepción del dictamen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
            <section id="filtro">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="request_dictum_recibe_date">Fecha recibe VUD *</label>
                                <input type="date" name="request_dictum_recibe_date" id="request_dictum_recibe_date" class="form-control" >
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="request_dictum_office">Número de folio</label>
                                <input type="tex" name="request_dictum_office" id="request_dictum_office" class="form-control" >
                            </div>
                        </div>

                        <div class="col-md-6">
                            <fieldset>
                                <h4>Respuesta dictamen *</h4>
                                <label class="input-vud" for="request_dictum_answer">
                                    <input type="radio" value="1" id="request_dictum_answer" name="request_dictum_answer">Autorizada
                                </label>
                                
                                <label class="input-vud" for="request_dictum_answer">
                                    <input type="radio" value="2" id="request_dictum_answer" name="request_dictum_answer">Rechazada
                                </label>
                            </fieldset>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="request_dictum_citizen">Fecha recibe ciudadano</label>
                                <input type="date" name="request_dictum_citizen" id="request_dictum_citizen" class="form-control" placeholder="Teléfono" aria-describedby="">
                            </div>
                        </div>
                        <input type="hidden" name="id_request_dictum" id="id_request_dictum">
                    </div>
                </div>
            </section>
        </div>            

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-outline-primary">Dictaminar</button>
      </div>
    </form>
    </div>
  </div>
</div>


<!-- MODAL PARA EL FINIQUITO DE LA SOLICITUD-->
<div class="modal fade" id="request_settlement" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <form id="form_request_settlement" name="form_request_settlement" onsubmit="return Settlement_request();">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Finiquitar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h3 class="text-center">Ingrese la siguiente información para la guardía y custodía de la solicitud:</h3>
        <h4 class="text-center" id="folio_settlement"></h4>
        <div class="container-fluid">
            <section id="filtro">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="request_settlement_date">Fecha de envío</label>
                                <input type="date" name="request_settlement_date" id="request_settlement_date" class="form-control" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="request_settlement_number">Número de oficio de envío</label>
                                <input type="tex" name="request_settlement_number" id="request_settlement_number" class="form-control" >
                            </div>
                        </div>
                        <input type="hidden" name="id_request_settlement" id="id_request_settlement">
                    </div>
                </div>
            </section>
        </div>            

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-outline-primary">Finiquitar</button>
      </div>
     </form>
    </div>
  </div>
</div>


<!--MODAL QUE MUESTRA TODA LA INFORMACION DE LA SOLICITUD-->
<div class="modal fade" id="request_acuse" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Información de la solicitud </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

        <div id="request_info_disabled">

            <div class="container">
            <h5 class="col-md-7">Solicitud de Trámite: <strong id="view_folio">003/2018</strong> <button type='button' class='btn btn-outline-danger btn-lg fa fa-eye col-md-2' id="view_acuse_info"></button></h3>
            </div>

            <div class="container-fluid">
                <h5 class="col-md-12" id="view_transact">Trámite:</h5>
                <h6 class="col-md-12" id="view_modality">Modalidad:</h6>
            </div>

            <br>
            <h4>Información de la prevención</h4>
            <hr>
                <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="prevent_date">Fecha de prevención *</label>
                                <input type="date" name="prevent_date" id="prevent_date" class="form-control" disabled>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="prevent_date_citizen">Fecha en la que recibe el ciudadano *</label>
                                <input type="date" name="prevent_date_citizen" id="prevent_date_citizen" class="form-control" disabled>
                            </div>
                        </div>
                </div>

            <br>
            <h4>Información de la subsanación</h4>
            <hr>
                <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="correct_date">Fecha en la que subsana el interesado *</label>
                                <input type="date" name="correct_date" id="correct_date" class="form-control" disabled>
                            </div>
                        </div>
                </div>

            <br>
            <h4>Información del dictamen</h4>
            <hr>
                <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="view_dictum_folio">Número de oficio: *</label>
                                <input type="text" name="view_dictum_folio" id="view_dictum_folio" class="form-control" disabled>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="view_date_dictum_vud">Fecha recibe VUD *</label>
                                <input type="date" name="view_date_dictum_vud" id="view_date_dictum_vud" class="form-control" disabled>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <fieldset>
                                <h4>Respuesta Dictamen *</h4>
                                <label class="input-vud" for="view_dictum_answer_1">
                                    <input type="radio" value="1" id="view_dictum_answer_1" name="view_dictum_answer_1" disabled>Autorizada
                                </label>
                                
                                <label class="input-vud" for="view_dictum_answer_2">
                                    <input type="radio" value="2" id="view_dictum_answer_2" name="view_dictum_answer_2" disabled>Rechazada
                                </label>
                            </fieldset>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="view_citizen_dictum">Fecha recibe ciudadano *</label>
                                <input type="date" name="view_citizen_dictum" id="view_citizen_dictum" class="form-control" disabled >
                            </div>
                        </div>
                </div>
            </div>

                <div id="info_request_acuse" style="display: none" >
                    <div class="container-fluid" id="request_tracing_data">
                        <?php include 'vw_tracing_acuse.php';?>
                    </div>
                </div>
          </div>
          <div class="modal-footer">

          </div>
    </div>
  </div>
</div>


<!-- MODAL PARA LA CANCELACION Y ACTIVACION DE LA COLICITUD -->
<div class="modal fade" id="request_activation_cancelation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <form id="form_request_cancelation" name="form_request_cancelation" onsubmit="return Request_Cancelation();">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cancelar solicitud</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
            <section id="filtro">
                <div class="container">
                    <div class="row">
                        <div class="container-fluid" align="center">
                        <h3>¿Desea cancelar la solicitud?</h3>
                        <h4>Ingrese el motivo de la cancelación</h4>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label for="request_cancelation_desc">Motivo:</label>
                              <textarea class="form-control" name="request_cancelation_desc" id="request_cancelation_desc" rows="5" required=""></textarea>
                            </div>
                            <input type="hidden" name="id_request" id="id_request">
                            <input type="hidden" name="status_request" id="status_request">
                            <input type="hidden" name="last_status" id="last_status">
                        </div>
                    </div>
                </div>
            </section>
        </div>            

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-outline-primary">Cancelar solicitud</button>
      </div>
    </div>
    </form>
  </div>
</div>

<div class="container-fluid" id="edit_request_data" style="display: none;" >
    <br>
    <?php include 'vw_tracing_edit_request.php';?>
</div>

<div id="dialog" title="Basic dialog" style="display: none;">
  <p>This is the default dialog which is useful for displaying information. The dialog window can be moved, resized and closed with the 'x' icon.</p>
</div>

