<!-- Contenido  -->
      <div class="container">
            <div class="contenedor"> 
                <style>
                    *{
                        margin: 0;
                        padding: 0;
                    }
                     .contenedor{
                         padding: 25px 30px;
                         margin: 0px 15%;
                     }
                     
                     .cabecera h1{
                         font-size: 18px; 
                         font-weight: bold;
                         text-align: center;            
                     }
                     .cabecera h2 {
                         font-size: 12px;
                         padding: 0px 60px;
                         font-weight: 600;
                         font-family: sans-serif;
                         text-align: center;     
                         color: #808080;
                             
                     }
                        
                     #informacion_del_proceso{
                         font-size: 10px;
                         display: grid; 
                         grid-template-columns: 160px auto 50px 50px;
                         grid-gap: 10px px;
                         padding-bottom: 5px;
                         padding-top: 15px;
                         border-bottom: 2px solid gray;
                         
                     }
                     .titulo{
                         font-weight: bold;
                     }
                     .contenedor-de-informacion{
                         margin-top: 30px;
                         font-size: 10px;
                     }
                     .contenedor-de-informacion h2{
                         font-size: 12px;
                         padding-left: 20px;
                         padding-bottom: 3px;
                         border-bottom: 2px solid gray;
                         margin-bottom: 15px;
                     }
                     .grid-informacion{
                         display: grid; 
                         grid-template-columns: 220px auto;
                         grid-gap: 8px 1px;                                    
                     }
                     .grid-title{
                         display: grid;
                         grid-template-columns: 220px auto 200px;
                     }
                        
                    .footer-grid{
                     display: grid;
                         grid-template-columns: auto 80px;
                        grid-gap: 10px 20px;
                    }
                    #clausulas{
                        margin-top: 10px;
                    }
                    #clausulas .clausula{               
                        font-size: 8px;
                        text-align: justify;
                        padding-top: 2px;
                    }
                    .nota{
                        color: gray;
                        text-align: center;
                        margin: 14px;
                        font-size: 10px;
                    }
                    .footer-grid img{
                        margin: auto;
             
                    }

                    @media all{
                       printed-div{
                           display:none;
                       }
                    }

                    @media print{
                       printed-div{
                           display:block;
                       }
                       .logo-print{
                           width:291px;
                           height:109px;
                           display: list-item;
                           list-style-image: url(../images/logo_print.png);
                           list-style-position: inside;
                       }
                    }
                </style>


                    <div class="cabecera">
                     <img src="<?= ROOT_URL?>assets/img/logo.png" alt="" width="25%">   
                     <h1>ACUSE DE RECIBO</h1>
                     <h2>"El presente acuse de recibo ampara solamente la recepción de documento y no implica de manera alguna la autorización del trámite"</h2>  
                    </div>
                     
                    <div id="informacion_del_proceso">
                        <div class="titulo">Fecha de ingreso: </div>
                        <div id="acuse-date"></div>
                        <div class="titulo">Folio</div>
                        <div id="acuse-folio"></div>           
                        <div class="titulo">Nombre del trámite:</div>
                        <div class="" id="acuse-transact-name"></div>
                        <div class="titulo"></div>
                        <div id=""></div> 
                        <div class="titulo">Modalidad:</div>
                        <div id="acuse-modality-name"></div>                      
                    </div>

                     <div id="datos_del_interesado" class="contenedor-de-informacion"> 
                         <h2>Datos del interesado, propietario o poseedor</h2>
                         <div class="grid-informacion">
                             <div class="titulo">Nombre o razón social</div>
                             <div class="acuse-name"></div>
                             <div class="titulo">Teléfono</div>
                             <div id ="acuse-phone">55-2563-7458</div>
                         </div>
                     </div>
                     
                     <div id="documentacion" class="contenedor-de-informacion">
                         <h2>Datos del predio, establecimiento, evento u obra.</h2>
                         <div class="grid-informacion">
                              <div class="titulo">Giro o uso</div>
                             <div id="acuse-giro">Venta de cerveza</div>
                              <div class="titulo">Tipo de obra</div>
                             <div id="acuse-obra"></div>
                              <div class="titulo">Dirección</div>
                             <div id="acuse-address"></div>                
                         </div>
                     </div>
                     <div id="documentacion" class="contenedor-de-informacion">            
                         <h2 class="grid-title">
                             <div>
                                 Documentación:     
                             </div>
                             <div id="acuse-doc-complete">
                                 
                             </div>
                             <div id="acuse-doc-49">
                                 
                             </div>
                         </h2>
                         <p class="nota">"Se recibe de conformidad con los requisitos establecidos en la normatividad aplicable al trámite"</p>
                         <div class="grid-informacion">
                             <div class="titulo">
                                 Observaciones
                             </div>
                             <div id="acuse-observaciones">
                                 
                             </div>
                         </div>
                     </div>
                     
                     <div id="recibe" class="contenedor-de-informacion">
                         <h2>Recibe acuse</h2>
                         <div class="grid-informacion">
                             <div class="titulo">Nombre</div>
                             <div class="acuse-name"></div>
                             <div class="titulo">Firma</div>
                             <div>_________________________</div>
                         </div>
                     </div>
                     
                     
                     <div id="seguimiento_al_tramite" class="contenedor-de-informacion">
                         <h2>Seguimiento al trámite</h2>
                         <div class="grid-informacion">
                             <div class="titulo">Fecha compromiso</div>
                             <div id="acuse-fecha-compromiso"></div>
                             <div class="titulo">Horario de atención</div>
                             <div>09:00 - 14:00 </div>
                             <div class="titulo">Medio por el cual solicita</div>
                             <div id="acuse-medio-solicitud"></div>
                             <div class="titulo">Módulo</div>
                             <div>Ventanilla Única Delegacional</div>
                             <div class="titulo">Operador</div>
                             <div id="acuse-user"></div>
                             
                         </div>
                     </div>
                     <!-- NOTA -->
                     <p class="nota">Nota: "Tiempo de respuesta conforme al manual de trámites y servicios al público del Distrito Federal vigente y la normatividad aplicada al trámite</p>
                     <div class="footer-grid">
                      
                       <div id="clausulas">
                                 
                        <p class="clausula">
                            “Los datos personales recabados serán protegidos, incorporados y tratados en el Sistema de Datos Personales de los Libros de Gobierno correspondientes y base de datos electrónica, el cual tiene su fundamento en los  artículos 41, 44 y 46 de la Ley de Procedimiento Administrativo de Distrito Federal, cuya finalidad es llevar un adecuado control de la asignación de folios y del registro de los datos del trámite realizado y podrán ser transmitidos a las unidades administrativas que conforme a sus facultades y atribuciones sean responsables de la emisión del dictamen correspondiente, además de otras transmisiones previstas en la Ley de Protección de Datos Personales para el Distrito Federal.
                            </p>
                        <p class="clausula">
                            Los datos marcados con un asterisco (*) son obligatorios y sin ellos no podrá acceder al servicio o completar el trámite de CUESTIONARIO DE AUTODIAGNÓSTICO EN MATERIA DE PROTECCIÓN CIVIL (PROGRAMA INTERNO).
                        </p>
                        <p class="clausula">
                            Asimismo, se le informa que sus datos no podrán ser difundidos sin su consentimiento expreso, salvo las excepciones previstas en la Ley.
                        </p>
                        <p class="clausula">
                            El responsable del Sistema de datos personales es para el caso de la Ventanilla Única Delegacional, el  LIC. Rodrigo Montoya Castillo, y la dirección donde podrá ejercer los derechos de acceso, rectificación, cancelación y oposición, así como la revocación del consentimiento es en la Oficina de Información Publica ubicada en planta baja del edificio delegacional, sita en 5 de Febrero esquina con Vicente Villada Col. Villa Gustavo A. Madero.
                        </p>
                        <p class="clausula">
                            El titular de los datos podrá dirigirse al Instituto de Acceso a la Información Pública del Distrito Federal, donde recibirá asesoría sobre los derechos que tutela la Ley de Protección de Datos Personales para el Distrito Federal al teléfono: 5636-4636; correo electrónico: datos.personales@infodf.org.mx o www.infodf.org.mx”
                        </p>
                       </div>
                  
                        
                         <img src="<?= ROOT_URL?>assets/img/footer.png" alt="" width="50%">    
                     </div>
                     
                </div>
                 
                 
                
                        <div class="container">
                        <button id="btnImprimir" type="button" class="btn offset-md-5 btn-primary">Imprimir</button>
                        <button id="info_back" type="button" class="btn btn-danger">Regresar</button>
                        
                        </div>

                        
                

      </div>

      <script>
             document.getElementById("btnImprimir").addEventListener("click", function() {
                  var div = document.querySelector(".contenedor");
                  imprimirElemento(div);
                });
            function imprimirElemento(elemento){
                    var ventana = window.open('', 'PRINT', 'height=1000,width=auto');
                    ventana.document.write(elemento.innerHTML);
                    setTimeout(() => {
                        ventana.document.close();
                        ventana.focus();
                        ventana.print();
                        ventana.close();
                        return true;    
                    }, 1);
                    
            }
        
        </script>
