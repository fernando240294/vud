 <form id="form_update_request_information" name="form_update_request_information" onsubmit="return Update_request_info();">
      <h4 class="text-center" id="edit_tittle"></h4>
      <br>
      <div class="row">
              <div class="col-md-2">
                <h4 class="text-center">Persona *</h4>
                <div align="center">
                <fieldset id="tipo_de_persona">
                    <label for="tipo_moral">
                        <input class="type_person_check" type="radio" id="tipo_moral" value="1" name="tipo_de_persona"> Moral
                    </label>

                    <label for="tipo_fisica">
                        <input class="type_person_check" type="radio" id="tipo_fisica" value="2" name="tipo_de_persona"> Física
                    </label>
                </fieldset>
                </div>
            </div>

              <div class="col-md-4">
                  <label for="transact_code">Tramite</label>   
                  <select class="form-control" id="transact_code_edit" name="transact_code_edit">
                      <option value="">-Selecciona-</option>
                      <?= $transact ?>
                  </select>
              </div>

              <div class="col-md-4">
                  <label for="modality_code">Modalidad</label>   
                  <select class="form-control" id="modality_code_edit" name="modality_code_edit">
                      <option value="">-Selecciona-</option>
                  </select>
              </div>
      </div>
      <br>
      <div class="row">
            <div class="contenedor-requisitos col-md-5 offset-md-1">
                <h5 class="text-center">Requisitos del trámite *</h5>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="check_all">
                    <label class="form-check-label" for="exampleCheck1">Marcar todos</label>
                </div> 
                <ul id="requirements_edit" style="height: 150px; overflow-y: scroll;">

                </ul>
            </div>

            <div class="col-md-5">
                <div class="form-group">
                  <label for="observations_update"></label>
                  <textarea class="form-control" rows="6" id="observations_update" name="observations_update"></textarea>
                </div>
            </div>
      </div>


        <div class="row" >
            <div class="col-md-2 offset-md-5">
                <h5 class="text-center">Ingreso *</h5>
                <fieldset id="tipo_de_persona">

                    <label for="tipo_moral">
                        <input class="entry_check" type="radio" id="ingreso_completa" value="1" name="update_status"> Completa
                    </label>

                    <label for="tipo_fisica">
                        <input class="entry_check" type="radio" id="ingreso_art_49" value="2" name="update_status"> Art. 49
                    </label>
                </fieldset>
            </div>
          </div>
          <br>
          <hr style="border-width: 2px;">
          <h3 class="text-center">Datos del interesado</h3>
          <br>
          <div class="row">
            <div class="col-md-3 offset-md-1">
                    <div class="form-group">
                        <label for="name_update">Nombre(s) / Razón Social *</label>
                        <input type="text" name="name_update" id="name_update" class="form-control" placeholder="" aria-describedby="" required>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="update_laste_name">Apellidos</label>
                        <input type="text" name="update_laste_name" id="update_laste_name" class="form-control" placeholder="Apellidos " aria-describedby="">
                    </div>
                </div>
                

          </div>

          <div class="row">
              <div class="col-md-3 offset-md-1">
                    <label for="edit_group_vulnerable">Grupo Vulnerable *</label>
                    <select class="form-control" id="edit_group_vulnerable" name="edit_group_vulnerable" required>
                        <option value="">--Grupo vulnerable--</option>
                        
                    </select>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="update_phone">Teléfono*</label>
                        <input type="number" name="update_phone" id="update_phone" class="form-control" placeholder="Teléfono" aria-describedby="" required>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="update_e_mail">Correo electrónico</label>
                        <input type="email" name="update_e_mail" id="update_e_mail" class="form-control" placeholder="Correo electrónico" aria-describedby="">
                    </div>
                </div>

            </div>

          <br>
          <hr style="border-width: 2px;">
          <h3 class="text-center">Dirección</h3>
          <br>
            <div class="row">
                <div class="col-md-3 offset-md-1">
                    <div class="form-group">
                        <label for="update_codigoPostal">Código Postal *</label>
                        <input type="number" name="update_codigoPostal" id="update_codigoPostal" class="form-control" placeholder="Código Postal" aria-describedby="" required>
                    </div>

                </div>
                <div class="col-md-3">
                    <div class="form-group" id="coloniaGroup">
                        <label for="colonia">Colonia *</label>
                        <input type="text" name="colonia" id="colonia" class="form-control" placeholder="Colonia" aria-describedby="" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group" id="delegacionGroup">
                        <label for="delegacion">Delegación *</label>
                        <input type="text" name="delegacion" id="delegacion" class="form-control" placeholder="Delegación" aria-describedby="" required>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-3 offset-md-1">
                    <div class="form-group">
                        <label for="update_street">Calle*</label>
                        <input type="text" name="update_street" id="update_street" class="form-control" placeholder="Calle" aria-describedby="" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="update_ext_number">Número exterior</label>
                        <input type="text" name="update_ext_number" id="update_ext_number" class="form-control" placeholder="Número exterior" aria-describedby="">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="update_int_number">Número interior</label>
                        <input type="text" name="update_int_number" id="update_int_number" class="form-control" placeholder="Número interior" aria-describedby="">
                    </div>
                </div>
            </div>

          <br>
          <hr style="border-width: 2px;">
          <h3 class="text-center">Datos del establecimiento</h3>
          <br>

          <div class="row">
                <div class="col-md-4 offset-md-2">
                    <div class="form-group">
                        <label for="update_turn">Giro o uso</label>
                        <input type="text" name="update_turn" id="update_turn" class="form-control" placeholder="Giro o Uso" aria-describedby="">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="update_type_work">Tipo de obra</label>
                        <input type="text" name="update_type_work" id="update_type_work" class="form-control" placeholder="Tipo de obra" aria-describedby="">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col offset-md-5">
                    <fieldset>
                        <h4>Medio por el cual solicita</h4>
                        <label class="input-vud" for="solicitado_en_linea" id >
                            <input type="radio" value="1" id="solicitado_en_linea" name="medio_por_el_cual_solicita">En línea
                        </label>
                        
                        <label class="input-vud" for="solicitado_presencialmente">
                            <input type="radio" value="2" id="solicitado_presencialmente" name="medio_por_el_cual_solicita">Presencial
                        </label>
                    </fieldset>
                </div>                
            </div>

            <div class="row">
              <div class="offset-md-2">
                <div class="terminos">
                    <p>Autorizado en términos de los artículos 41 y 42 de la Ley de Procedimiento Administrativo del Distrito
                        Federal</p>
                        <label class="input-vud" for="cartaPoder">
                            <input type="radio" value="1" id="cartaPoder" name="tipo_de_documento">Carta Poder
                        </label>
                        
                        <label class="input-vud" for="formato">
                            <input type="radio" value="2" id="formato" name="tipo_de_documento">Formato
                        </label>
                </div>
              </div>
            </div>
            <input type="hidden" name="update_id_request"  id="update_id_request" value="">
            <input type="hidden" name="update_id_address"  id="update_id_address" value="">
            <input type="hidden" name="update_id_interested"  id="update_id_interested" value="">
            <div class="container-fluid" style="padding-bottom: 50px">
                <button type="submit" class=" btn btn-outline-primary offset-md-5" id="guardarTramite">Actualizar</button>
                <button type="button" class=" btn btn-outline-danger" id="tracing_back">Regresar</button>
            </div>
            </form>