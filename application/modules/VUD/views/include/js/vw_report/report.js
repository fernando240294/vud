$(document).ready(function(){

});

$('#clean_filter').click(function (){
    $('#report_ejecicio').val("");
    $('#report_delegacion').val("");
    $('#report_estatus').val("");
    $('#report_area_revisorsa').val("");
    $('#report_resp_dictamen').val("");
    $('#report_tipo_persona').val("");
    $('#report_grupo_vulnerable').val("");
    $('#report_fecha_inicio').val("");
    $('#report_fecha_final').val("");
})

$(".nav-item-request").click(function (){
    event.preventDefault();
    var id = ($(this).prop("id"));
    if (id == "sub_nav_report"){
        $("#year_report").fadeOut();
        $("#main_menu").fadeIn();
    }else if(id == "sub_nav_report_year"){
        $("#main_menu").fadeOut();
        $("#year_report").fadeIn();
        
        
    }
})

function formTransact_delete() {
    var urlFormSave = $Ctr_transact + 'deleteTransact';
    var dataToSave = {id_transact: id};
    $.ajax({
        type: "POST",
        cache: true,
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function (data) {
            if (data.response) {
                swal ( "Bien" ,  "Se elimino el registro de manera correcta." ,  "success" );
                setTimeout(function(){
                    location.reload();
                }, 2000);
            } else {
                swal ("Ups!" , "Ocurrio un problema al eliminar el registro." , "error");
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            swal ("Ups!" , "Ocurrio un problema al eliminar el registro." , "error");
            return false;
        }
    });
}


function report_create(){
    var UrlFormSave = $Ctr_report + 'CreateReport';
    var dataToSave = $('#form_filter_report').serialize();
    $.ajax({
        type: "POST", 
        url: UrlFormSave,
        data: dataToSave,
        dataType: "json",
        
        error: function(xhr,textStatus,errorThrown){
            swal ("Ups!","Ocurrio un problema al actualizar la informacion","error");
            return false;
        }
    });
    return false;
}