$(document).ready(function () {
    get_request_status();
    get_request_total();
});
var totales = [];
var descripciones = [];
var colors = [];

function get_request_status(){    
    $.ajax({
        url: $Ctr_dashboard +'Estatus_chats_request',
        type: 'json',
        success: function(data){                        
            
            var obj = JSON.parse(data);
            estatus = Array.from(obj);
            for (i = 0; i < estatus.length; i++) { 
                descripciones.push(estatus[i]['description']);
                totales.push(estatus[i]['total_stado']);
                R=Math.floor((Math.random() * 255 )+ 1);
                G=Math.floor((Math.random() * 255 )+ 1);
                B=Math.floor((Math.random() * 255 )+ 1);
                colors.push('rgb('+R+','+G+', '+B+')');

            }
            create_chart_pie(descripciones,totales,colors);
        },
        error: function(xhr,textStatus,errorThrown){
        }
    });
}

function create_chart_pie(descripciones,totales,colors){
    data = {
    datasets: [{
        data: totales,
        backgroundColor: colors,
    }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: descripciones,
    };

    var ctx = document.getElementById('total_status').getContext('2d');
    var myDoughnutChart = new Chart(ctx, {
        type: 'doughnut',
        data: data,
    });
}

function get_request_total(){    
    $.ajax({
        url: $Ctr_dashboard +'Request_chart_line',
        type: 'json',
        success: function(data){                        

            var objeto = JSON.parse(data);
            total = Object.values(objeto);
            create_chart_line(total);
        },
        error: function(xhr,textStatus,errorThrown){
        }
    });
}

function create_chart_line(total){
        var speedData = {
          labels: ["Enero", 
                    "Febrero", 
                    "Marzo", 
                    "Abril", 
                    "Mayo", 
                    "Junio", 
                    "Julio",
                    "Agosto", 
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"],
          datasets: [{
            label: "Solicitudes por mes",
            data: total,
          }]
        };

        var grafica = document.getElementById('total_month').getContext('2d');
        var myLineChart = new Chart(grafica, {
            type: 'line',
            data: speedData,
    });
}





