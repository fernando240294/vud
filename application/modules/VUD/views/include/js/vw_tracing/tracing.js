
$(document).ready(function(){
    $('#dataTableTracing').dataTable({
            dom: 'Bflrtip',
            buttons: [
                'excel', 'print'
            ]
        });
    getCodigoPostal();
    get_vulnerable_group();
});

$('#tracing_back').click(function (){
    $("#edit_request_data").fadeOut();
    $("#main_menu").fadeIn();
    
})

function getCodigoPostal() {
    $(document).on('keyup', '#update_codigoPostal', function () {
        var valorBusqueda = $(this).val();
        obtener_registros(valorBusqueda)
    });
}

function obtener_registros(codigo) {
    let urlCodigo = 'https://api-codigos-postales.herokuapp.com/v2/codigo_postal/' + codigo
    $.ajax({
            url: urlCodigo,
            dataType: 'json'
        })
        .done(function (resultado) {
            /* Evaluación del colonias */
            let $htmlColoniaGroup;
            if (resultado.colonias != 0) {
                $htmlColoniaGroup = '<label for="colonia">Colonia*</label>'
                $htmlColoniaGroup += "<select class='form-control' id='colonia' name='colonia'>"
                if(resultado.colonias.length == 1){
                    $htmlColoniaGroup = '<label for="delegacion">Colonia*</label>'
                    $htmlColoniaGroup += '<input type="text" name="colonia" id="colonia" class="form-control" placeholder="' + resultado.colonias[0] + '"value="' + resultado.colonias[0] + '">'
                }else{
                    
                    resultado.colonias.forEach(element => {
                        $htmlColoniaGroup += "<option class='opciones-colonia'  value='" + element + "'> " + element + " </option>"
                    });
                }
                $htmlColoniaGroup += "</select>"
            } else {
                $htmlColoniaGroup = "<label for='colonia'>Colonia*</label>"
                $htmlColoniaGroup += "<input type='text' name='colonia' id='colonia' class='form-control' placeholder='Colonia' aria-describedby=''>"
            }
            $('#coloniaGroup').html($htmlColoniaGroup);
            /* Evaluación de municipio */
            let $htmlMunicipioGroup;
            if (resultado.municipio != 0) {
                $htmlMunicipioGroup = '<label for="delegacion">Delegacion*</label>'
                $htmlMunicipioGroup += '<input type="text" name="delegacion" id="delegacion" class="form-control" placeholder="' + resultado.municipio + '"value="' + resultado.municipio + '">'

            } else {
                $htmlMunicipioGroup = '<label for="delegacion">Delegación*</label>'
                $htmlMunicipioGroup += '<input type="text" name="delegacion" id="delegacion" class="form-control" placeholder="Delegación" aria-describedby="">'
            }
            $('#delegacionGroup').html($htmlMunicipioGroup)
        })
}

function set_dataTable(){
    $('#dataTableTracing').dataTable({
            dom: 'Bflrtip',
            buttons: [
                'excel', 'print'
            ]
        });
}

function get_vulnerable_group(){
    $.ajax({
        url: "VUD/Ctr_tracing/vud_data_vulnerable_group",
        type: "json",                
        success: function (response) {
            catalogoDeGruposVulnerables = JSON.parse(response)
            console.log(catalogoDeGruposVulnerables);
            let html = ''
            catalogoDeGruposVulnerables['result'].forEach(grupo => {
                html += `<option value="${grupo.id}">${grupo.description}</option>`
            });   
            $('#edit_group_vulnerable').append(html);            
        },
        error: function(x,e) {
            if (x.status==0) {
                alert('You are offline!!\n Please Check Your Network.');
            } else if(x.status==404) {
                alert('Requested URL not found.');
            } else if(x.status==500) {
                alert('Internel Server Error.');
            } else if(e=='parsererror') {
                alert('Error.\nParsing JSON Request failed.');
            } else if(e=='timeout'){
                alert('Request Time out.');
            } else {
                alert('Unknow Error.\n'+x.responseText);
            }
        }
    });
}

$("#clean_filter").click(function (){
    $("#request-folio").val('');
    $("#transact_code").val('');
    $("#modality_code").val('');
    $("#request-status").val('');
    $("#request_type_person").val('');
    $("#request_date_begin").val('');
    $("#request_date_end").val('');
})

function Filter_Data(){
    $("#loading_gif").fadeIn();
    var urlFormSave = $Ctr_tracing + 'Tracing_Filter_Data';
    var dataToSave = $('#form_filter-data-tracing').serialize();
    $.ajax({
        type: "POST",
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function(data){
            if (data.reponse) {                
                $("#loading_gif").fadeOut();
                $("#container_table").children().remove();                
                $("#container_table").html(data.filter);
                
                set_dataTable();
            }else{
                swal("Ups!", data.filter, "error");
            }
        },
        error: function(xhr,textStatus,errorThrown){
        }
    });
    return false;
}

$("#transact_code").change(function (){
    var urlFormSave = $Ctr_tracing + 'Transact_info';
    var id_selected  = $(this).val();
    var dataToSave = {code_transact : id_selected};
    $.ajax({
        type: "POST",
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function(data){
            if(data.status){
                $("#modality_code").append(data.result);
            }
        },
        error: function(xhr, textStatus, errorThrown){
            alert('ocurrio un error');
            return false
        }
    });
    return false;
});

$("#tracing_all_record").click(function(){
    $("#loading_gif").fadeIn();
    var urlFormSave = $Ctr_tracing + 'AllTracingRecord';
    $.ajax({
        url: urlFormSave,
        dataType: "json",
        success: function(data){
            if (data.response) {
                $("#loading_gif").fadeOut();
                $("#container_table").children().remove();                
                $("#container_table").html(data.table);
                set_dataTable();
            }
        },
        error: function(xhr,textStatus,errorThrown){
        }
    });
})

$("#requirements_area").delegate("button","click",function(){
    trancit_create_turn();
});

function trancit_create_turn(folio,id_request,id_status){
    $("#request_turn_folio").val(folio);
    $("#request_turn_folio").prop('disabled',true);
    $("#id_request_turn").val(id_request);
    $("#id_status_turn").val(id_status);
}

function request_create_turn(){
    var urlFormSave = $Ctr_tracing + 'Request_create_turn';
    var dataToSave = $('#form_request_turn').serialize();
    $.ajax({
        type: "POST",
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function(data){
            if (data.response) {
                swal("Bien!", 'Se turno de manera correcta la solicitud.' , "success");
                setTimeout(function(){
                    location.reload();
                }, 3000);
            }else{
                swal("Ups!", "Ocurrio un problema al turnar la solicitud.", "error");
            }
        },
        error: function(xhr,textStatus,errorThrown){
            swal("Ups!", "Ocurrio un problema al turnar la solicitud.", "error");
        }
    });
    return false;
}

function trancit_create_dictum(id_request,folio){
    $("#id_request_dictum").val(id_request);
    if (folio != ""){
        $("#request_dictum_recibe_date").prop('disabled',true);
        $("#request_dictum_answer").prop('disabled',true);
        $("#request_dictum_office").prop('disabled',true);
    }else{
        $("#request_dictum_recibe_date").prop('disabled',false);
        $("#request_dictum_answer").prop('disabled',false);
        $("#request_dictum_office").prop('disabled',false);
    }
}

function request_create_dictum(){
    var urlFormSave = $Ctr_tracing + 'Request_create_dictum';
    var dataToSave = $('#form_request_dictum').serialize();
    $.ajax({
        type: "POST",
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function(data){
            if (data.response) {
                swal("Bien!", 'Se creo el dictamen de manera correcta.' , "success");
                $('#request-dictum').modal('hide');
                setTimeout(function(){
                    location.reload();
                }, 2000);

            }else{
                swal("Ups!", "Ocurrio un problema al turnar la solicitud.", "error");
            }
        },
        error: function(xhr,textStatus,errorThrown){
            swal("Ups!", "Ocurrio un problema al turnar la solicitud.", "error");
        }
    });
    return false;
}

function tracing_view_request(id_request){
    $("#info_request_acuse").fadeOut();
    $("#request_info_disabled").fadeIn();
    var urlFormSave = $Ctr_tracing + 'Request_info';
    var dataToSave = {id_request : id_request};
    $.ajax({
        type: "POST",
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function(data){
            if(data){
                Request_view_data_acuse(data);
            }
        },
        error: function(xhr, textStatus, errorThrown){
            alert('ocurrio un error');
            return false
        }
    });
    return false;
}

function Request_view_data_acuse(data){
    //se carga la informacion de la modalidad y el tramite
    $("#view_folio").html(data.request.folio);
    $("#view_transact").html("Trámite: "+data.request.description);
        if(data.request.modality_desc == null || data.request.modality_desc == ""){
            data.request.modality_desc = "No cuenta con modalidades";
        }
    $("#view_modality").html("Modalidad: "+data.request.modality_desc);
    //se cargan los datos de la subsanacion y la prevencion
    $("#prevent_date").val(data.request.prevent_date);
    $("#prevent_date_citizen").val(data.request.citizen_prevent_date);
    $("#correct_date").val(data.request.correct_date);
    //Se muestran los datos del dictamen
    $("#view_dictum_folio").val(data.request.folio_dictum);
    $("#view_date_dictum_vud").val(data.request.date_receives_dictum);
    $("#view_citizen_dictum").val(data.request.citizen_dictum);
        if(data.request.answer_dictum != null && data.request.answer_dictum != ""){
                if (data.request.answer_dictum == 1){
                    $("#view_dictum_answer_1").prop('checked', true);    
                    $("#view_dictum_answer_2").prop('checked', false);    
                }else{
                    $("#view_dictum_answer_1").prop('checked', false);    
                    $("#view_dictum_answer_2").prop('checked', true);
                }
        }else{
                $("#view_dictum_answer_1").prop('checked', false);    
                $("#view_dictum_answer_2").prop('checked', false);
        }
    //informacion del acuse
    $("#acuse-date").html(data.request.date_creation);
    $("#acuse-folio").html(data.request.folio);
    $("#acuse-transact-name").html(data.request.description);
    $("#acuse-modality-name").html(data.request.modality_desc);
    $(".acuse-name").html(data.request.name+' '+data.request.last_name);
    $("#acuse-phone").html(data.request.phone);
        if(data.request.request_turn == null || data.request.request_turn == ""){
            data.request.request_turn = "N/A";
        }
        if(data.request.type_work == null || data.request.type_work == ""){
            data.request.type_work = "N/A";
        }
    $("#acuse-giro").html(data.request.request_turn);
    $("#acuse-obra").html(data.request.type_work);
        if(data.request.ext_number == null || data.request.ext_number == ""){
            data.request.ext_number = "N/A";
        }
        if (data.request.int_number == null || data.request.int_number == ""){
            data.request.int_number = "N/A";
        }
    $("#acuse-address").html(data.request.delegation+', '+data.request.colony+', '+data.request.postal_code+', '+data.request.street+',  Num. Ext. '+data.request.ext_number+', Num. Int. '+data.request.int_number);
    $("#acuse-observaciones").html(data.request.observation);
        if(data.request.id_status == 1){
            data.request.id_status = "<strong>Completa(X)</strong>";
            $("#acuse-doc-complete").html(data.request.id_status);
            $("#acuse-doc-49").html("");
        }else{
            data.request.id_status = "<strong>Art. 49(X)</strong>";
            $("#acuse-doc-49").html(data.request.id_status);
            $("#acuse-doc-complete").html("");
        }
    $("#acuse-observaciones").html(data.request.observation);
    $("#acuse-fecha-compromiso").html(data.request.date_commitment);
        if(data.request.means_request == 1){
            data.request.means_request = "En línea";
        }else if(data.request.means_request == 2){
            data.request.means_request = "Presencial";
        }
    $('#acuse-medio-solicitud').html(data.request.means_request);
    $('#acuse-user').html(data.request.user_name);
}

$("#view_acuse_info").click(function(){
    $("#request_info_disabled").fadeOut();
    $("#info_request_acuse").fadeIn();
})

$("#info_back").click(function(){
    $("#info_request_acuse").fadeOut();
    $("#request_info_disabled").fadeIn();
})

function set_request_prevent(id_request){
    $(".id_request_prevent").val(id_request);
}

//Funcion para Prevenir la solicitud
function Prevencion_request(){
    date_prevent =$("#request_prevent_date").val();
    date_citizen =$("#prevent_citizen_date").val();
        if((Date.parse(date_prevent) <= Date.parse(date_citizen)) || (date_prevent!= "" && date_citizen == "")){
                var urlFormSave = $Ctr_tracing + 'Prevencion_request';
                var dataToSave = $('#form_request_prevent').serialize();
                $.ajax({
                    type: "POST",
                    url: urlFormSave,
                    data: dataToSave,
                    dataType: "json",
                    success: function(data){
                        if(data.response){
                            $("#tracing_code_49").modal('hide');
                            swal("Bien!", 'Se pudo prevenir de manera correcta la solicitud.' , "success");
                            clean_input();
                            setTimeout(function(){
                                location.reload();
                            }, 2000);
                        }else{
                            $("#tracing_code_49").modal('hide');
                            swal("Ups!", 'Ocurrio un problema al prevenir la solicitud.' , "error");
                        }
                    },
                    error: function(xhr, textStatus, errorThrown){
                        $("#tracing_code_49").modal('hide');
                        swal("Ups!", 'Ocurrio un problema al subsanar la solicitud.' , "error");
                        return false
                    }
                });
                return false; 
        }else{
            event.preventDefault();
            swal("Ups!", 'Favor de verificar bien los datos de prevencion' , "error");
        }
}

function Prevencion_correct(){
    var urlFormSave = $Ctr_tracing + 'Prevencion_request';
    var dataToSave = $('#form_request_correct').serialize();
        $.ajax({
            type: "POST",
            url: urlFormSave,
            data: dataToSave,
            dataType: "json",
            success: function(data){
                if(data.response){
                    $("#tracing_code_49").modal('hide');
                    swal("Bien!", 'Se subsano de manera correcta la solicitud.' , "success");
                    clean_input();
                    setTimeout(function(){
                    location.reload();
                    }, 3000);
                }else{
                    $("#tracing_code_49").modal('hide');
                    swal("Ups!", 'Ocurrio un problema al subsanar la solicitud.' , "error");
                }
            },
            error: function(xhr, textStatus, errorThrown){
                $("#tracing_code_49").modal('hide');
                swal("Ups!", 'Ocurrio un problema al subsanar la solicitud.' , "error");
                return false
            }
        });
        return false;   
}

function set_request_cancelation(id_request,status,last_status){
    $("#id_request").val(id_request);
    $("#status_request").val(status);
    $("#last_status").val(last_status);
}

function Request_Cancelation(){
    var urlFormSave = $Ctr_tracing + 'Request_Cancelation';
    var dataToSave = $('#form_request_cancelation').serialize();
        $.ajax({
            type: "POST",
            url: urlFormSave,
            data: dataToSave,
            dataType: "json",
            success: function(data){
                if(data.response){
                    $("#request_activation_cancelation").modal('hide');
                    swal("Bien!", 'Se canceló de manera correcta la solicitud.' , "success");
                    clean_input();
                     setTimeout(function(){
                    location.reload();
                }, 3000);
                }else{
                    $("#request_activation_cancelation").modal('hide');
                    swal("Ups!", 'Ocurrio un problema al cancelar la solicitud.' , "error");
                }
            },
            error: function(xhr, textStatus, errorThrown){
                $("#request_activation_cancelation").modal('hide');
                swal("Ups!", 'Ocurrio un problema al cancelar la solicitud.' , "error");
                return false
            }
        });
        return false;   
}

function Alert_Activate_request(id_request,status,last_status){
    swal({
      title: "¿Éstas seguro de reactivar esta solicitud?",
      icon: "warning",
      buttons: {
            cancel: "Cancelar",
            Reactivar:{
                text: "Reactivar",
                value: true,
            },
      },
      dangerMode: true,
    })
    .then((willActive) => {
      if (willActive) {
        Activate_reques(id_request,status,last_status);
      }else{
        swal("Ok!, no se continuo con el procedo de reactivacion.");
      }
    });
}

function Activate_reques(id_request,status,cancelation){
    var urlFormSave = $Ctr_tracing + 'Activate_request';
    var dataToSave = {id_request : id_request,
                      status_request : status,
                      last_status : cancelation};
    $.ajax({
        type: "POST",
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function(data){
            if(data){
                swal("Bien!", 'Se reactivo de manera correcta.' , "success");
                setTimeout(function(){
                    location.reload();
                }, 3000);
            }else{
                swal("Ups!", "Ocurrio un problema al reactivar la solicitud.", "error");
            }
        },
        error: function(xhr, textStatus, errorThrown){
            swal("Ups!", "Ocurrio un problema al reactivar la solicitud.", "error");
            return false
        }
    });
    return false;
}

function Tracing_settlement_request(id_request,folio){
    $("#id_request_settlement").val(id_request);
    $("#folio_settlement").html("Solicitud con folio: "+folio);
}

function Settlement_request(){
    var urlFormSave = $Ctr_tracing + 'Create_settlement';
    var dataToSave = $('#form_request_settlement').serialize();
        $.ajax({
            type: "POST",
            url: urlFormSave,
            data: dataToSave,
            dataType: "json",
            success: function(data){
                if(data.response){
                    $("#request_settlement").modal('hide');
                    swal("Bien!", 'Se finiquito de manera correcta la solicitud.' , "success");
                    setTimeout(function(){
                    location.reload();
                }, 2000);
                }else{
                    $("#request_settlement").modal('hide');
                    swal("Ups!", 'Ocurrio un problema al finiquitar la solicitud.' , "error");
                }
            },
            error: function(xhr, textStatus, errorThrown){
                $("#request_settlement").modal('hide');
                swal("Ups!", 'Ocurrio un problema al finiquitar la solicitud.' , "error");
                return false
            }
        });
        return false;   
}

function clean_input(){
    $('input').val("");
}

function tracing_edit_request(id,folio,id_address,id_interested){
    $("#edit_request_data").fadeIn();
    $("#main_menu").fadeOut();
    $("#edit_tittle").html("Solicitud: "+folio);
    $("#update_id_request").val(id);
    $("#update_id_address").val(id_address);
    $("#update_id_interested").val(id_interested);
        var urlFormSave = $Ctr_tracing + 'Request_info_update';
        var dataToSave = {id_request : id};
        $.ajax({
            type: "POST",
            url: urlFormSave,
            data: dataToSave,
            dataType: "json",
            success: function(data){
                if(data){
                    Request_update_data(data);
                }
            },
            error: function(xhr, textStatus, errorThrown){
                alert('ocurrio un error al consultar los datos');
                return false
            }
        });
        return false;
}

function Request_update_data(data){
    $("#transact_code_edit").val(data.request.id_transact);
    $("#transact_code_edit").val(data.request.id_transact);
        if(data.request.id_modality == null || data.request.description_modality == null){
            $("#modality_code_edit").html("<option value='' selected>Sin Modalidad</option>");    
        }else{
            $("#modality_code_edit").html("<option value='"+data.request.id_modality+"' selected>"+data.request.description_modality+"</option>");
        }
    $("#observations_update").text(data.request.observations);
        if(data.request.type_person == 1 ){
            $("#tipo_moral").prop("checked", true);
        }else{
            $("#tipo_fisica").prop("checked", true);
        }
        if(data.request.id_status == 1 ){
            $("#ingreso_completa").prop("checked", true);
        }else{
            $("#ingreso_art_49").prop("checked", true);
        }
    $("#name_update").val(data.request.name);
    $("#update_laste_name").val(data.request.last_name);
    $("#edit_group_vulnerable").val(data.request.id_vulnerable_group); 
    $("#phone").val(data.request.phone); 
    $("#update_phone").val(data.request.phone); 
    $("#update_e_mail").val(data.request.e_mail);
    $("#update_codigoPostal").val(data.request.postal_code);
    $("#colonia").val(data.request.colony);
    $("#delegacion").val(data.request.delegation);
    $("#update_street").val(data.request.street); 
    $("#update_ext_number").val(data.request.ext_number); 
    $("#update_int_number").val(data.request.int_number); 
    $("#update_turn").val(data.request.request_turn);
    $("#update_type_work").val(data.request.type_work);
        if (data.request.means_request == 1){
            $("#solicitado_en_linea").prop("checked", true);
        }else{
            $("#solicitado_presencialmente").prop("checked",true); 
        }
        if(data.request.delivery == 1){
            $("#cartaPoder").prop("checked",true); 
        }else if(data.request.delivery == 2){
            $("#formato").prop("checked",true); 
        }
        $("#requirements_edit").html(data.select);
}

$("#check_all").click(function(e){
    if($('.check_requirement').prop('checked')){
       $('.check_requirement').prop('checked', false);
    }else{
       $('.check_requirement').prop('checked', true); 
    }
})

$("#transact_code_edit").change(function (){
    var urlFormSave = $Ctr_tracing + 'Transact_info';
    var id_selected  = $(this).val();
    var dataToSave = {code_transact : id_selected};
    $.ajax({
        type: "POST",
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function(data){
            if(data.status){
                $("#modality_code_edit").html(data.result);
            }else{
                $("#modality_code_edit").html("<option value='0' selected>Sin Modalidad</option>");
                $('#check_all').prop('checked', false);
                $("#requirements_edit").html(data.result);
            }
        },
        error: function(xhr, textStatus, errorThrown){
            alert('ocurrio un error');
            return false
        }
    });
    return false;
});


$("#modality_code_edit").change(function (){
    var urlFormSave = $Ctr_tracing + 'Modality_info';
    var id_selected  = $(this).val();
    var dataToSave = {code_modality : id_selected};
    $.ajax({
        type: "POST",
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function(data){
            if(data.status){
                $("#requirements_edit").html(data.requirements);
                $('#check_all').prop('checked', false);
            }
        },
        error: function(xhr, textStatus, errorThrown){
            alert('ocurrio un error');
            return false
        }
    });
    return false;
});

function Update_request_info(){
    var urlFormSave = $Ctr_tracing + 'Update_request_data';
    var dataToSave = $('#form_update_request_information').serialize();
        $.ajax({
            type: "POST",
            url: urlFormSave,
            data: dataToSave,
            dataType: "json",
            success: function(data){
                if(data.response){
                    swal("Bien!", data.message , "success");
                    $("#edit_request_data").fadeOut(); 
                    $("#main_menu").fadeIn(); 
                }else{
                    alert('esta aca');
                    swal("Ups!", data.message , "error");
                }
            },
            error: function(xhr, textStatus, errorThrown){
                swal("Ups!", 'Ocurrio un problema al actualizar la información' , "error");
                return false
            }
        });
        return false;   
}
