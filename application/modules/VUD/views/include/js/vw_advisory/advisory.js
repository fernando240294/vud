$(document).ready(function () {
//bootbox.alert("Hello world!");
});

/**
*funcion para mandar los datos de un nuevo usuario al controller Ctr_dashboard
**/
function users_create(){
    var urlFormSave = $Ctr_dashboard + 'CreateUser';
    var dataToSave = $('#form_users_create').serialize();
    $.ajax({
        type: "POST",
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function (data) {
            if (data.response) {
                $(".modal").hide();
                DialogError(data.message);
            } else {
                $(".modal").hide();
                DialogError(data.message);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            $(".modal").hide();
            DialogError(xhr.responseText);
            return false;
        }
    });
    return false;
}

/**
*funcion para cargar los datos actuales del usuario en los input.
**/
function user_update_data(id,user_name,employee_number,description,id_rol,register_employee_number){
    $('#id_user').val(id);
    $('#update_user_name').val(user_name);
    $('#update_employee_number').val(employee_number);
    $('#update_description').val(description);
    $('#id_user_update').val(register_employee_number);
    $("#update_role").val(id_rol);
}

/**
*funcion para mandar los datos del usuario para actualización.
**/
function users_update(){
    var urlFormSave = $Ctr_dashboard + 'UpdateUser';
    var dataToSave = $('#form_users_update').serialize();
    $.ajax({
        type: "POST",
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function (data) {
            if (data.response) {
                DialogError(data.message);
            } else {
                DialogError(data.message);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            DialogError(xhr.responseText);
            return false;
        }
    });
    return false;
}


function delete_user(id) {
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_DANGER,
        title: "Eliminar usuario.",
        message: "¿Estas seguro de eliminar este usuario?.",
        buttons: [{
            label: 'Cerrar',
            action: function (dialogItself) {
                dialogItself.close();
            }
        }, {
            cssClass: 'btn-danger',
            label: 'Eliminar',
            action: function () {
                $('.loading').css("display", "none");
                formUser_delete(id);
            }
        }]
    });
    return false;
}

function formUser_delete(id) {
    var urlFormSave = $Ctr_dashboard + 'user_delete';
    var dataToSave = {id_user: id};
    $.ajax({
        type: "POST",
        cache: true,
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function (data) {
            if (data.response) {
                alert('se elimino el registro de manera correcta');
            } else {
                DialogError(data.message);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            DialogError(xhr.responseText);
            return false;
        }
    });
}