$(document).ready(function () {
    $('#modality_sub_nav').prop('class','active');
});

/**
*funcion para mandar los datos de una nueva transaccion al controller Ctr_dashboard
**/
function modality_create(){
    var UrlFormSave = $Ctr_modality + 'CreateModality';
    var dataToSave = $('#form_modality_create').serialize();
    $.ajax({
        type: "POST",
        url: UrlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function(data){
            if(data.response){
                swal("Bien", "Se creo de manera correcta la modalidad", "success");
            }else{
                swal("Ups", "Ocurrio un problema al crear la modalidad", "error");
            }
        },
        error: function(xhr,textStatus,errorThrown){
            swal("Ups", "Ocurrio un problema al crear la modalidad", "error");
            return false;
        }
    });
    return false;
}

function Modality_update_data(id,code,description,legal_foundament,code_transact,days,type_days){
    $('#udpate_id_modality').val(id);
    $('#update_modality_transact').val(code_transact);
    $("#update_modality_transact").attr('disabled',true);
    $("#update_modality_code").val(code);
    $("#udpate_code_modality").val(code);
    $("#update_modality_code").attr('disabled',true);
    $('#update_modality_description').val(description);
    $('#update_modality_legal_foundament').val(legal_foundament);
    $('#update_modality_days').val(days);
    $('#update_type_days_modality').val(type_days);

    var urlFormSave = $Ctr_modality + 'Format_update_requirements';
    var dataToSave = {code_modality: code};
    $.ajax({
        type: "POST",
        cache: true,
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function (data) {
            if(data){
                $(".check-requirements").html(data.cheked);
            }else{
                swal("Ups", "Ocurrio un problema al cargar los requisitos de la modalidad", "error");
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            swal("Ups", "Ocurrio un problema al cargar los requisitos de la modalidad", "error");
            return false;
        }
    });
}

function modality_update(){
    var UrlFormSave = $Ctr_modality + 'UpdateModality';
    var dataToSave = $('#form_modality_update').serialize();
    $.ajax({
        type: "POST", 
        url: UrlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function(data){
            if(data.response){
                swal("¡Bien!", "Se actualizaron los datos de manera correcta", "success");
            }else{
                swal("Ups!", "Ocurrio un problema al actualizar la información", "error");
            }
        },
        error: function(xhr,textStatus,errorThrown){
            swal("Ups!", "Ocurrio un problema al actualizar la información", "error");
            return false;
        }
    });
    return false;
}

function delete_modality(id){
    swal({
      title: "¿Estas seguro de eliminar el registro",
      icon: "warning",
      buttons: {
            cancel: "Cancelar",
            Reactivar:{
                text: "Eliminar",
                value: true,
            },
      },
      dangerMode: true,
    })
    .then((willActive) => {
      if (willActive) {
        Modality_delete(id);
      }else{
        swal("Ok!, no se continuo con el proceso de eliminacion.");
      }
    });
}

function Modality_delete(id) {
    var urlFormSave = $Ctr_modality + 'DeleteModality';
    var dataToSave = {id_modality: id};
    $.ajax({
        type: "POST",
        cache: true,
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function (data) {
            if (data.response) {
                swal("¡Bien!", "Se elimino el registro de manera correcta.", "success");
                setTimeout(function(){
                    location.reload();
                }, 2000);
            } else {
                swal("Ups!", "Ocurrio un problema al eliminar el registro.", "error");
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            swal("Ups!", "Ocurrio un problema al eliminar el registro.", "error");
            return false;
        }
    });
}