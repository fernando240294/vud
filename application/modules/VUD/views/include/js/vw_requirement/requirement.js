$(document).ready(function () {
    $('#requirement_sub_nav').prop('class','active');
});

function requirement_create(){
    var UrlFormSave = $Ctr_requirement+ 'CreateRequirement';
    var dataToSave = $('#form_requirement_create').serialize();
    $.ajax({
        type: "POST",
        url: UrlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function(data){
            if(data.response){
                $("#add_transact").modal('hide');
                swal ( "Bien" ,  "Se creo el requisito de manera correcta" ,  "success" );
                setTimeout(function(){
                    location.reload();
                }, 2000);
            }else{
                swal ("Ups!" , "Ocurrio un error al insertar el requisito" , "error");
            }
        },
        error: function(xhr,textStatus,errorThrown){
            swal ("Ups!" , "Ocurrio un error al insertar el requisito" , "error");
            return false;
        }
    });
    return false;
}

function requirement_update_data(id,description){
    $("#update_requirement_description").val(description);
    $("#id_update_requirement").val(id);
}

function requirement_update(){
    var UrlFormSave = $Ctr_requirement + 'UpdateRequirement';
    var dataToSave = $('#form_requirement_update').serialize();
    $.ajax({
        type: "POST",
        url: UrlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function(data){
            if(data.response){
                $("#vud_update_requirement").modal('hide');
                swal ( "Bien" ,  "Se actualizo el requisito de manera correcta" ,  "success" );
                setTimeout(function(){
                    location.reload();
                }, 2000);
            }else{
                swal ("Ups!" , "Ocurrio un error al actualizar el requisito" , "error");
            }
        },
        error: function(xhr,textStatus,errorThrown){
            swal ("Ups!" , "Ocurrio un error al actualizar el requisito" , "error");
            return false;
        }
    });
    return false;
}


function delete_requirement(id){
    swal({
      title: "¿Éstas seguro de eliminar el registro?",
      icon: "warning",
      buttons: {
            cancel: "Cancelar",
            Reactivar:{
                text: "Eliminar",
                value: true,
            },
      },
      dangerMode: true,
    })
    .then((willActive) => {
      if (willActive) {
        formRequirement_delete(id);
      }else{
        swal("Ok!, no se continúo con el proceso de eliminación.");
      }
    });
}



function formRequirement_delete(id) {
    var urlFormSave = $Ctr_requirement + 'DeleteRequirement';
    var dataToSave = {id_requirement: id};
    $.ajax({
        type: "POST",
        cache: true,
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function (data) {
            if (data.response) {
                swal ( "Bien" ,  "Se elimino de manera correcta el registro" ,  "success" );
                setTimeout(function(){
                    location.reload();
                }, 2000);
            } else {
                swal ("Ups!" , "Ocurrio un error al eliminar el requisito" , "error");
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            swal ("Ups!" , "Ocurrio un error al eliminar el requisito" , "error");
            return false;
        }
    });
}

/**
*evento para crear los input, de los requisitos que se van a guardar
**/
$("#add_modality").click(function(){
    $("#requirements_area").append('<div class="col-md-6"><div class="form-label-group"><label for="firstName">Descripción</label><input type="text" name="create_requirement['+$count+'] class="form-control" required><br><button type="button" class="btn btn-danger delete_item">Borrar</button></div></div>');
    $("input").prop('class','form-control')
    $count++;
});

$("#requirements_area").delegate("button","click",function(){
  $(this).parent().parent().remove();
});