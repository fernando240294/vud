$(document).ready(function(){
    $('#user_sub_nav').prop('class','active');
});

/**
*funcion para mandar los datos de un nuevo usuario al controller Ctr_dashboard
**/
function users_create(){
    var urlFormSave = $Ctr_users + 'CreateUser';
    var dataToSave = $('#form_users_create').serialize();
    $.ajax({
        type: "POST",
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function (data) {
            if (data.response) {
                $('#add_user').modal('hide')
                swal("Bien", "Se guardo de manera correcta el usuario.", "success");
                setTimeout(function(){
                    location.reload();
                }, 2000);
            } else {
                swal("Ups!", "Ocurrio un problema al crear el usuario.", "error");
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            swal("Ups!", "Ocurrio un problema al crear el usuario.", "error");
            return false;
        }
    });
    return false;
}

/**
*funcion para cargar los datos actuales del usuario en los input.
**/
function user_update_data(id,user_name,employee_number,description,id_rol,register_employee_number){
    $('#id_user').val(id);
    $('#update_user_name').val(user_name);
    $('#update_employee_number').val(employee_number);
    $('#update_description').val(description);
    $('#id_user_update').val(register_employee_number);
    $("#update_role").val(id_rol);
}

/**
*funcion para mandar los datos del usuario para actualización.
**/
function users_update(){
    var urlFormSave = $Ctr_users + 'UpdateUser';
    var dataToSave = $('#form_users_update').serialize();
    $.ajax({
        type: "POST",
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function (data) {
            if (data.response) {
                $('#add_user').modal('hide')
                swal("Bien", "Se actualizo la información del usuario de manera correcta.", "success");
                setTimeout(function(){
                    location.reload();
                }, 2000);
            } else {
                swal("Ups!", "Ocurrio un problema al actualizar la información del usuario.", "error");
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            swal("Ups!", "Ocurrio un problema al actualizar la información del usuario.", "error");
            return false;
        }
    });
    return false;
}


function delete_user(id){
    swal({
      title: "¿Estas seguro de eliminar el registro?",
      icon: "warning",
      buttons: {
            cancel: "Cancelar",
            Reactivar:{
                text: "Eliminar",
                value: true,
            },
      },
      dangerMode: true,
    })
    .then((willActive) => {
      if (willActive) {
        formUser_delete(id);
      }else{
        swal("Ok!, no se continúo con el proceso de eliminación.");
      }
    });
}

function formUser_delete(id) {
    var urlFormSave = $Ctr_users + 'user_delete';
    var dataToSave = {id_user: id};
    $.ajax({
        type: "POST",
        cache: true,
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function (data) {
            if (data.response) {
                swal("Bien", "Se elimino el usuario de manera correcta", "success");
                setTimeout(function(){
                    location.reload();
                }, 2000);
            } else {
                swal("Ups!", "Ocurrio un problema al eliminar el usuario", "error");
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            swal("Ups!", "Ocurrio un problema al eliminar el usuario", "error");
            return false;
        }
    });
}


function UserPermissions(id) {
    var urlFormSave = $Ctr_users + 'UserPermissions';
    var dataToSave = {id_user: id};
    $.ajax({
        type: "POST",
        cache: true,
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function (data) {
            if (data.response) {
                $('#permission-body').children().remove();
                $('#permission-body').html(data.permissions);
            }else {
                $('#permission-body').children().remove();
                swal(data.permissions);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            swal(data.permissions);
            return false;
        }
    });
}


function users_create_permissions(){
    var urlFormSave = $Ctr_users + 'UpdatePermission';
    var dataToSave = $('#form_users_permission').serialize();
    $.ajax({
        type: "POST",
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function (data) {
            if (data.response) {
                swal("Bien",data.message, "success");
            } else {
                swal("Ups!", message, "error");
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            swal("Ups!", message, "error");
            return false;
        }
    });
    return false;
}
