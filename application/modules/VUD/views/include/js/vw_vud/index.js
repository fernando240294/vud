$(document).ready(function () {
});

var contador = "";
function validarUser(){
    /**
    *usuario y contraseña que se toman del formulario.
    **/
    var username = $("#login_username").val();
    var password = $("#login_password").val();

    /**
    *objeto de JSON que asignara los valor obtenidos de los imput y que se pasaran a ajax.
    **/
    data = {
        username: username,
        password: password
    };

    /**
    *Ajax que manda el Usuario y contraseña para validad que se encuentre en la base de datos.
    **/
    $.ajax({
        type: "POST",
        url: $ROOT_URL + "VUD/userAuth",
        data: data,
        dataType: "json",
        success: function (respuesta) {
            if (respuesta.response){
                $(location).attr('href', $ROOT_URL + 'dashboard');
            } else {
                if (contador != 1){
                    $("#error_login").append("");
                    $("#error_login").append("<div class='alert alert-danger'>\n" +
                        "<strong>Ups !</strong> Algo no salio bien, favor de verificar usuario o contraseña\n</div>");
                    contador = 1;
                }
            }
        }
    });
    return false;
}