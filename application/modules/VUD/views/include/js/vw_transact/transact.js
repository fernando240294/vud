$(document).ready(function () {
    $('#transact_sub_nav').prop('class','active');
});

/**
*funcion para mandar los datos de una nueva transaccion al controller Ctr_dashboard
**/
function transact_create(){
    var UrlFormSave = $Ctr_transact + 'CreateTransact';
    var dataToSave = $('#form_transact_create').serialize();
    $.ajax({
        type: "POST",
        url: UrlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function(data){
            if(data.response){
                swal ( "Bien" ,data.message,  "success" );
                setTimeout(function(){
                    location.reload();
                }, 2000);
            }else{
                swal ("Ups!",data.message,"error");
            }
        },
        error: function(xhr,textStatus,errorThrown){
            swal ("Ups!",data.message,"error");
            return false;
        }
    });
    return false;
}

function transact_update_data(id,code,description,legal_foundament,$modality,$days,$type_days){
    $('#id_update_transact').val(id);
    $('#update_transact_code').val(code);
    $('#update_transact_code').attr('disabled',true);
    $('#code_update_transact').val(code);
    $('#update_transact_description').val(description);
    $('#update_transact_foundament').val(legal_foundament);
    $('#update_transact_days').val($days);
    $('#update_type_days_transact').val($type_days);


    if ($modality > 0){
        $(".check-requirements").hide();
        $(".text-update").hide();
    }else{
        $(".check-requirements").show();
        $(".text-update").show();
        var urlFormSave = $Ctr_transact + 'Format_update_requirements';
        var dataToSave = {_code_transact: code};
        $.ajax({
            type: "POST",
            cache: true,
            url: urlFormSave,
            data: dataToSave,
            dataType: "json",
            success: function (data) {
                if (data) {
                    $(".check-requirements").html(data.cheked);
                }
            },
            error: function (xhr, textStatus, errorThrown){
                swal ("Ups!" , "Ocurrio un problema al eliminar el registro." , "error");
                return false;
            }
        });
    }
}

function transact_update(){
    var UrlFormSave = $Ctr_transact + 'UpdateTransact';
    var dataToSave = $('#form_transact_update').serialize();
    $.ajax({
        type: "POST", 
        url: UrlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function(data){
            if(data.response){
                swal ( "Bien" ,  "Se creo actualizo la información de manera correcta" ,  "success" );
                setTimeout(function(){
                    location.reload();
                }, 2000);
            }else{
                swal ("Ups!" , "Ocurrio un problema al actualizar la informacion" , "error");
            }
        },
        error: function(xhr,textStatus,errorThrown){
            swal ("Ups!" , "Ocurrio un problema al actualizar la informacion" , "error");
            return false;
        }
    });
    return false;
}

function delete_transact(id){
    swal({
      title: "¿Éstas seguro de eliminar el registro?",
      icon: "warning",
      buttons: {
            cancel: "Cancelar",
            Reactivar:{
                text: "Eliminar",
                value: true,
            },
      },
      dangerMode: true,
    })
    .then((willActive) => {
      if (willActive) {
        formTransact_delete(id);
      }else{
        swal("Ok!, no se continúo con el proceso de eliminación.");
      }
    });
}

function formTransact_delete(id) {
    var urlFormSave = $Ctr_transact + 'deleteTransact';
    var dataToSave = {id_transact: id};
    $.ajax({
        type: "POST",
        cache: true,
        url: urlFormSave,
        data: dataToSave,
        dataType: "json",
        success: function (data) {
            if (data.response) {
                swal ( "Bien" ,  "Se elimino el registro de manera correcta." ,  "success" );
                setTimeout(function(){
                    location.reload();
                }, 2000);
            } else {
                swal ("Ups!" , "Ocurrio un problema al eliminar el registro." , "error");
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            swal ("Ups!" , "Ocurrio un problema al eliminar el registro." , "error");
            return false;
        }
    });
}

/**
*evento para crear los input, de los requisitos que se van a guardar
**/
$("#add_modality").click(function(){
    $("#requirements_area").append('<div class="col-md-6"><div class="form-label-group"><label for="firstName">Descripcion</label><input type="text" name="create_requirement['+$count+'] class="form-control"><button type="button" class="btn btn-danger delete_item">Borrar</button></div></div>');
    $count++;

});

$("#requirements_area").delegate("button","click",function(){
  $(this).parent().parent().remove();
});