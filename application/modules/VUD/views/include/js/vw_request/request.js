$(document).ready(function () {
$('#transact-request').parent().attr("class","active");
});

$(".event-step").click(function (){
    event.preventDefault();
    id = $(this).attr("id");
    if(id == "data-request"){
        $('#request-transact').fadeOut();
        $('#request-acuse').fadeOut();
        $('#request-data').fadeIn();
        $('#data-request').parent().attr("class","active");
        $('#transact-request').parent().removeClass("active");
        $('#data-acuse').parent().removeClass("active");
    }
    if (id == "transact-request") {
        $('#request-data').hide();
        $('#request-acuse').fadeOut();
        $('#request-transact').fadeIn();
        $('#transact-request').parent().attr("class","active");
        $('#data-request').parent().removeClass("active");
        $('#data-acuse').parent().removeClass("active");
    }

});


/***************************************************************
 * *************************************************************
 * FUNCIONES DEFINIDAS POR SERGIO
 * *************************************************************
 **************************************************************/
/**
 * Data
 * 
 * Este objeto guarda toda la información de las interacciones 
 * 
 * @global
 * @author SERGIO
 * 
 */

var data = {
    tipoDePersona : "",
    tramite :       "",
    tramite_id: "",
    modalidad :  "Sin modalidad",
    modalidad_id: '0',
    requisitos :    [],
    observaciones : "",
    datosDelInteresado: {
        nombre: "AB",
        apellidoPaterno: '',
        apellidoMaterno: '',
        identificacion: '',
        sexo: '',
        grupoVulnerable: '',
        grupoVulnerable_id: '',
        RFC: '',
        telefono: '',
        correoElectronico: ''
    },
    direccion: {
        codigoPostal: '',
        colonia: '',
        delegacion: '',
        calle: '',
        noExterior: '',
        noInterior: ''
        
    },
    datosDelEstablecimiento: {
        giro: '',
        tipoDeObra: '',            
    },
    medioDeSolicitud: '',
    cartaPoderFormato: ''

}




/**
 * Catálogo de trámites 
 * 
 * Esta función debe de ser cargada durante el inicio de la función
 * con la petición al servidor que cargue el catálogo
 * 
 * @global
 * @var JSON
 */


var catalogoDeTramites = []

/**
 * Este objeto se utiliza para la comparación de cadenas del buscador,
 * en un formato estándar para que no haya problema con los acentos, espacios 
 * o elementos que puedan generar ligeras confusiones
 */
var catalogoDeTramites_limpio = []



var catalogoDeGruposVulnerables = []
/***************************************************************
 * *************************************************************
 * INSTANCIACIÓN DE ELEMENTOS
 * *************************************************************
 **************************************************************/

 validarAlInicio();

 /***************************************************************
 * *************************************************************
 * DEFINICIÓN DE FUNCIONES PÚBLICAS
 * *************************************************************
 **************************************************************/
/**
 * Validar al inicio
 * 
 * Esta función realiza acciones durante el inicio del programa.
 * Generalmente se usa en pruebas
 * 
 * @public
 * @param {}
 * @return {void}
 */
function validarAlInicio(){
    //Funciones que corren al cargar la página
    
    
    diseñarContenedorDeOpciones()
    solicitarTramites();        
        
    /* Agregar una advertencia al usuario hasta que esté listo */
    setTimeout(() => {                 
        buscador(data, catalogoDeTramites, catalogoDeTramites_limpio)
        crearModal()
        guardarProcesoTramite()
    }, 3200);    


    getCodigoPostal();
    eventoGeneralizado();
    

   
}

function diseñarContenedorDeOpciones(){
    try {
        $('#resultado_de_la_busqueda').hide();          
        let posicionAbsoluta = $('#search').offset();        
        let ancho = $('#buscador').width()        
        $('#resultado_de_la_busqueda').css({top: posicionAbsoluta.top + 38 , left: posicionAbsoluta.left, width: ancho, height: 240 });            
    } catch (error) {
        console.log("No está funcionando." + error)
    }  
}

function solicitarTramites(){        
    $.ajax({
        url: 'VUD/Ctr_request/returnAllRequest',
        type: 'json',
        success: function(data){                        
            console.log("************************************")
            console.log("Obteniendo trámites")
            catalogoDeTramites = JSON.parse(data);
            catalogoDeTramites.forEach(tramite => {
                catalogoDeTramites_limpio.push( tramite.code, formatearCadena(tramite.code+' '+tramite.description))
            });
        },
        error: function(x,e) {
            if (x.status==0) {
                alert('You are offline!!\n Please Check Your Network.');
            } else if(x.status==404) {
                alert('Requested URL not found.');
            } else if(x.status==500) {
                alert('Internel Server Error.');
            } else if(e=='parsererror') {
                alert('Error.\nParsing JSON Request failed.');
            } else if(e=='timeout'){
                alert('Request Time out.');
            } else {
                alert('Unknow Error.\n'+x.responseText);
            }
        }
    })

    $.ajax({
        url: "VUD/Ctr_request/vud_data_vulnerable_group",
        type: "json",                
        success: function (response) {
   
            catalogoDeGruposVulnerables = JSON.parse(response)
            console.log(catalogoDeGruposVulnerables['result'])
            let html = ''
            catalogoDeGruposVulnerables['result'].forEach(grupo => {
                html += `<option value="${grupo.id}----${grupo.description}">${grupo.description}</option>  `
            });   
            $('#grupoVulnerable').append(html);
            
            
        },
        error: function(x,e) {
            
            if (x.status==0) {
                alert('You are offline!!\n Please Check Your Network.');
            } else if(x.status==404) {
                alert('Requested URL not found.');
            } else if(x.status==500) {
                alert('Internel Server Error.');
            } else if(e=='parsererror') {
                alert('Error.\nParsing JSON Request failed.');
            } else if(e=='timeout'){
                alert('Request Time out.');
            } else {
                alert('Unknow Error.\n'+x.responseText);
            }
        }
    });
}


function buscador(data, catalogo, catalogoLimpio) {     
    
    diseñarContenedorDeOpciones();         
    //obtenerTipoDePersona(data);
    obtenerTramite(data, catalogo, catalogoLimpio);             
    obtenerObservaciones(data);

}

/**
 * Validar tipo de persona
 *  
 * Esta función permite esperar a que el usuario seleccione un 
 * tipo de persona en el primer campo del formulario, ya sea
 * de tipo moral o tipo físico
 * 
 * @event Click 
 * @public
 * @returns {void}
 */
/*function obtenerTipoDePersona(data){
    $('#tipo_moral').click(function(){                                    
        data.tipoDePersona = "1";        
    })
    $('#tipo_fisica').click(function(){                                    
        data.tipoDePersona = "2";        
    })      
} */ 

$(".type_person_check").click(function(){
    data.tipoDePersona = $(this).val();    
})

/**
 * Obtener trámite
 *  
 * Espera el evento Keyup en el cuadro del buscador. 
 * Este desencadena múltiples acciones mencionadas debajo
 * 
 * @event    keyup  
 * @returns  {void}
 * @requires validarSiHayContenidoEnLaBusqueda
 * @requires renderResultadosDeLaBusqueda
 * @requires validarSiExistenCoincidencias
 * @requires guardarTramiteSeleccionado
 * @requires esperarSelecciónDelInputTramite
 */
function obtenerTramite(data, catalogo, catalogoLimpio){
        
    /* #search: Id del input de buscar trámite */    
    $(document).on('keyup', '#search', function(){       
        console.log(catalogo) 
        const inputDelBuscador = $(this).val();
        validarSiHayContenidoEnLaBusqueda(inputDelBuscador)        
        let $html = renderResultadosDeLaBusqueda(inputDelBuscador, catalogo, catalogoLimpio)
        validarSiExistenCoincidencias($html);
        $('#resultado_de_la_busqueda').html($html);
        guardarTramiteSeleccionado(data);
        esperarSelecciónDelInputTramite();
        eventoGeneralizado();
        data.datosDelInteresado.nombre = "AB"
    })
    
}

/**
 * Validar si hay contenido en la búsqueda 
 * 
 * Cuando se teclea un elemento en el input Search 
 * muestra u oculta el cuadro de la búsqueda dependiendo 
 * de si hay o no una cadena en el input
 * 
 * @event   click
 * @private
 * @param {String} busqueda
 * @type {Render CSS} 
 */
function validarSiHayContenidoEnLaBusqueda(busqueda){
    if(busqueda == "" ){
        $('#resultado_de_la_busqueda').hide();        
    }else{
        $('#resultado_de_la_busqueda').show();                        
    }
}

/**
 * Render(Convertir cadena en HTML para) Resultados de la búsqueda 
 * 
 * Esta función transforma el criterio de la búsqueda a código HTML que incluye cada una 
 * de las coincidencias encontradas en el catálogo_limpio
 * 
 * 
 * @param   {String} Busqueda 
 * @param   {Array String} catalogo
 * @param   {Array String} catalogoLimpio
 * @private
 * @requires formatearCadena 
 */

function renderResultadosDeLaBusqueda(busqueda , catalogo, catalogoLimpio){    
    const valor_de_la_busqueda = formatearCadena(busqueda)        

    let $html = "";
    for (let i = 0; i < catalogo.length; i++) {
        const tramite = catalogo[i].code+', '+catalogo[i].description;
        console.log(tramite);
        const tramiteFormateado = catalogoLimpio[2*i + 1];
        if(tramiteFormateado.indexOf(valor_de_la_busqueda) >= 0){                
            $html += '<div class="opciones-tramites" data-tramite="'+ catalogoLimpio[2*i] + '"> ' + tramite +   ' </div>';       
        }          
    }    
    return $html; 
    
}

/**
 * Validar si existen coincidencias
 * 
 * Si el código está vacio, entonces, significa que no existen coincidencias
 * Si el código tiene coincidencias quitar el estilo.
 * 
 * @private
 * @param {String (htmlCode)} $html
 * @type {Render CSS} 
 */
function validarSiExistenCoincidencias($html){
    if($html != ""){
        if( $('#search').hasClass('has-danger')){
            $('#search').removeClass('has-danger')
        }
    }    
    else{
        $('#search').addClass('has-danger')        
    } 
}

/**
 * Guardar Tramite Seleccionado 
 * 
 * Esta función involucra los datos 
 *  Modalidad
 *  Requisitos
 *  Fundamento Jurídico
 * 
 * El objetivo de esta opción es que al selecciónar el trámite realice. 
 * 
 * 1. Dejar de mostrar las opciones
 * 2. Dar formato a los campos correspondientes: 
 *  2.1 El input de trámite debe tener el valor del trámite
 *  2.2 No debe existir requisitos 
 *  2.3 El valor global data.tramite toma el valor del código del trámite
 *  2.4 Crear objeto para realizar una petición Json
 * 3. Realizar petición AJAX al controlador de Requisitos 
 *  3.1 Si la respuesta no tiene modalidades 
 *   => Entonces Muestra requisitos directamente
 *  3.2 Si la respuesta tiene modalidad 
 *   => Mostrar modalidades
 *
 * 
 * @requires agregarModalidades
 * @requires @
 * @private
 */
function guardarTramiteSeleccionado(data){
    
    $('.opciones-tramites').click(function(){          
        $('#resultado_de_la_busqueda').hide();        

         let valor = $(this).html()    
         $('#search').val(valor)           
         data.requisitos = [];
         $('#requisitos_del_tramite').html("")
         data.tramite_id = $(this).data('tramite')         
         data.tramite =  $(this).html()
         const codeTramite =  {'code_transact': data.tramite_id} 

        $.ajax({
            type: "POST",
            url: "VUD/Ctr_request/Transact_info",
            data: codeTramite,
            dataType: "JSON",
            success: function (response) {
                if(response.transact_status){                    
                    let requisitos = (response.data)
                    $('#modalidades').html("<option > No tiene modalidades </option>")
                    data.modalidad_id = "0";
                    data.modalidad = 'No cuenta con modalidades'
                    agregarRequisitos(requisitos,data)                    
                }else{
                    console.log(response);                                     
                    let modalidades = (response.data);
                    agregarModalidades(modalidades, data)
                }
            }
        });
    });
}


/**
 * @private
 */
function esperarSelecciónDelInputTramite(){
    $('#search').focus(function(){            
        $('#search').html("")
        $('#search').val("")        
    })
}

function agregarModalidades(modalidades, data){    
    console.log(modalidades)
    let $html = '<option value="0">--Modalidad--</option>';    
    for (let i = 0; i < modalidades.length; i++) {
        const modalidad = modalidades[i];
        $html += `<option value="${modalidad.code},${modalidad.description}"  >${modalidad.description}</option>` 
    }
   $('#modalidades').html($html)

   /*Listenners */
   $('#modalidades').change(function(){
       let data_temp_tramite =  $(this).val().split(',')
       data.modalidad_id = data_temp_tramite[0]
       data.modalidad = data_temp_tramite[1]
       const datos = {
        'code_modality': data.modalidad_id
       }
       $.ajax({
           type: "POST",
           url: "VUD/Ctr_request/Modality_info",
           data: datos,
           dataType: "JSON",
           success: function (response) {
               agregarRequisitos(response.result ,data)               
           }
       });
   })
}

function agregarRequisitos(requisitos, data){
    console.log("INFO: REQUISITOS: " + requisitos)
    let $html = "";
    for (let i = 0; i < requisitos.length; i++) {
        const requisito = requisitos[i];
        $html += `
        <li class='checkbox checkbox-circle'>
        <input class="check_all" id='${requisito.description}' data-id-requisito=${requisito.id} type='checkbox' name='${requisito.description}' value='off'>
        <label  for='${requisito.description}'>${requisito.description}</label></li>`
                
    }
    
    $('#requisitos_del_tramite').html($html)
    $('#contenedor-de-fundamento-juridico').html(requisitos[0].legal_foundation)


    $('.all').click(function(){
    if ($(".all").prop('checked')){
            data.requisitos = [];
            $(".check_all").prop('checked',true);
            $(".checkbox input").each(function(index){
                data.requisitos.push($(this).data('id-requisito'));
            });
        }else{
            $(".check_all").prop('checked',false);
        }
    });

    /* Listenners */
    $('.checkbox input').click(function(){  
          if( $(this).val() == 'off'){
              $(this).val('on');                                             
              data.requisitos.push($(this).data('id-requisito'))
              console.log(data.requisitos);
          }else{
              $(this).val('off');
              eliminarItemDeArreglo(data.requisitos, $(this).data('id-requisito'))
          }            
    })
}

function obtenerObservaciones(data){
    $('#comentarios').blur(function (e) { 
        e.preventDefault();
        data.observaciones = $(this).val()
    });
}

function formatearCadena(cadena){    
 
    // Lo queremos devolver limpio en minusculas
    cadena = cadena.toString().toLowerCase();
 
    // Quitamos espacios y los sustituimos por _ porque nos gusta mas asi
    cadena = cadena.replace(/ /g,"_");
 
    // Quitamos acentos y "ñ". Fijate en que va sin comillas el primer parametro
    cadena = cadena.replace(/á/gi,"a");
    cadena = cadena.replace(/é/gi,"e");
    cadena = cadena.replace(/í/gi,"i");
    cadena = cadena.replace(/ó/gi,"o");
    cadena = cadena.replace(/ú/gi,"u");
    cadena = cadena.replace(/ñ/gi,"n");
    return cadena;
 }


  /**
  * 
  * @param {Array} arr 
  * @param {Mixed} item 
  */
 function eliminarItemDeArreglo ( arr, item ) {
    var i = arr.indexOf( item ); 
    if ( i !== -1 ) {
        arr.splice( i, 1 );
    }
}

/**
 * @description La función espera el evento keyup dentro del input 
 * código postal, cuando se detecta se toma el valor del input y se busca en 
 * obtener registros, función que utiliza el código para consulgar el api
 * y regresar los valores en los campos apropiados. 
 * @param int Codigo Postal, event KeyUp
 * @public 
 * @returns HTML in #delegacionGroup & #coloniaGroup
 * @export itsel
 * @callback obtener_registros
 */

 function getCodigoPostal() {
    $(document).on('keyup', '#codigoPostal', function () {
        
        var valorBusqueda = $(this).val();
        data.direccion.codigoPostal = valorBusqueda;
        obtener_registros(valorBusqueda)
    });
}


/**
 * Description: Obtener registros del api de código postal. 
 * @type  AJAX REQUEST
 * @param int Codigo Postal
 * @return HTML in ID(#delegacionGroup)
 */
function obtener_registros(codigo) {
    let urlCodigo = 'https://api-codigos-postales.herokuapp.com/v2/codigo_postal/' + codigo
    $.ajax({
            url: urlCodigo,
            dataType: 'json'
        })
        .done(function (resultado) {
            /* Evaluación del colonias */
            let $htmlColoniaGroup;
            if (resultado.colonias != 0) {
                $htmlColoniaGroup = '<label for="colonia">Colonia*</label>'
                $htmlColoniaGroup += "<select class='form-control' id='colonia' name='colonia'>"
                
                if(resultado.colonias.length == 1){
                    $htmlColoniaGroup = '<label for="delegacion">Colonia*</label>'
                    $htmlColoniaGroup += '<input type="text" name="delegacion" id="delegacion" class="form-control" placeholder="' + resultado.colonias[0] + '"value="' + resultado.colonias[0] + '">'
                    data.direccion.colonia = resultado.colonias[0]
                }else{
                    data.direccion.colonia = resultado.colonias[0]
                    resultado.colonias.forEach(element => {
                        $htmlColoniaGroup += "<option class='opciones-colonia'  value='" + element + "'> " + element + " </option>"
                    });
      
                }
                
                $htmlColoniaGroup += "</select>"
            } else {
                $htmlColoniaGroup = "<label for='colonia'>Colonia*</label>"
                $htmlColoniaGroup += "<input type='text' name='colonia' id='colonia' class='form-control' placeholder='Colonia' aria-describedby=''>"
                
            }
            $('#coloniaGroup').html($htmlColoniaGroup);

            /* Evaluación de municipio */
            let $htmlMunicipioGroup;
            if (resultado.municipio != 0) {
                $htmlMunicipioGroup = '<label for="delegacion">Delegacion*</label>'
                $htmlMunicipioGroup += '<input type="text" name="delegacion" id="delegacion" class="form-control" placeholder="' + resultado.municipio + '"value="' + resultado.municipio + '">'
                data.direccion.delegacion = resultado.municipio
            } else {
                $htmlMunicipioGroup = '<label for="delegacion">Delegación*</label>'
                $htmlMunicipioGroup += '<input type="text" name="delegacion" id="delegacion" class="form-control" placeholder="Delegación" aria-describedby="">'
            }
            $('#delegacionGroup').html($htmlMunicipioGroup)
        })

}

function eventoGeneralizado(){
        
    $("#nombre").blur(function (e) {     
        e.preventDefault();                
        data.datosDelInteresado.nombre = $(this).val()
        console.log(data.datosDelInteresado.nombre)
    });

    $("#apelidoPaterno").blur(function (e) {     
        e.preventDefault();                
        data.datosDelInteresado.apellidoPaterno = $(this).val()
        
    });
    $("#apellidoMaterno").blur(function (e) {     
        e.preventDefault();                
        data.datosDelInteresado.apellidoMaterno = $(this).val()
        
    });
    $('#identificacionOficial').change(function (e) { 
        e.preventDefault();
        data.datosDelInteresado.identificacion = $(this).val()
    });
    $('#sexo').change(function (e) { 
        e.preventDefault();
        data.datosDelInteresado.sexo = $(this).val()
    });
    $('#grupoVulnerable').change(function (e) { 
        e.preventDefault();
        let grupoID = $(this).val().split('----')
        data.datosDelInteresado.grupoVulnerable = grupoID[1]
        data.datosDelInteresado.grupoVulnerable_id = grupoID[0]
    });
    $('#rfc').blur(function (e) { 
        e.preventDefault();
        data.datosDelInteresado.RFC = $(this).val()
    });
    $('#telefono').blur(function (e) { 
        e.preventDefault();
        data.datosDelInteresado.telefono = $(this).val()
    });
    $('#email').blur(function (e) { 
        e.preventDefault();
        data.datosDelInteresado.correoElectronico = $(this).val()
    });
    /* : */
    $('#colonia').on("change", ".opciones-colonia" ,function (e) {         
        e.preventDefault();
        data.direccion.colonia = $(this).html()
    });
    $('#calle').blur(function (e) { 
        e.preventDefault();
        data.direccion.calle = $(this).val();
    });
    $('#giro').blur(function (e) { 
        e.preventDefault();
        data.datosDelEstablecimiento.giro = $(this).val();
    });
    $('#tipoDeObra').blur(function (e) { 
        e.preventDefault();
        data.datosDelEstablecimiento.tipoDeObra = $(this).val();
    });

    /* Sugiero que aquí se pongan IDs */
    $('#solicitado_en_linea').click(function(){                                    
        data.medioDeSolicitud = "1"
        data.medioDeSolicitudText = "En línea";
    })
    $('#solicitado_presencialmente').click(function(){                                    
        data.medioDeSolicitud = "2"
        data.medioDeSolicitudText = "Presencialmente"
    })

    $('#cartaPoder').click(function(){                                    
        data.cartaPoderFormato = "1"
        data.cartaPoderFormatoText = "Carta poder"
    })
    $('#formato').click(function(){                                    
        data.cartaPoderFormato = "2"
        data.cartaPoderFormatoText = "Formato"
    })
    $('#noExterior').blur(function(){                                    
        data.direccion.noExterior = $(this).val();
    })
    $('#noInterior').blur(function(){                                    
        data.direccion.noInterior = $(this).val();
    })
    $('.entry_check').click(function(){                                    
        data.status = $(this).val();
    })
}


function crearModal(){
    $('#crearModal').click(function (e) { 
        e.preventDefault();
        
        var fecha = new Date();
        var month = fecha.getMonth()+1;
        var day = fecha.getDate();
        var year = fecha.getFullYear();
        fecha =(month + '-' + day + '-' + year);


        let datosDeLaSolicitud = {
            'Trámite':   data.tramite ,
            'Modalidad': data.modalidad,
            'Fecha de solicitud': fecha,
            'Medio por el cual solicita': data.medioDeSolicitudText 
        }
            
    let datosDelInteresado = {
        'Nombre': data.datosDelInteresado.nombre+' '+data.datosDelInteresado.apellidoPaterno+' '+data.datosDelInteresado.apellidoMaterno,
        'Grupo vulnerable': data.datosDelInteresado.grupoVulnerable
    }
    let  numeros; 
    if(data.direccion.noInterior == ''){
        numeros = data.direccion.noExterior
    }else{
        numeros = data.direccion.noExterior + ' Interior: ' + data.direccion.noInterior;
    }
    
    let direccion = {
        'Delegación': data.direccion.delegacion,
        'Colonia': data.direccion.colonia, 
        'Código postal': data.direccion.codigoPostal,
        'Calle': data.direccion.calle,
        'No': numeros ,
        'Teléfono': data.datosDelInteresado.telefono,
        'Correo electrónico':  data.datosDelInteresado.correoElectronico
    }
        
    let datosDelEstablecimiento = {
        'Giro o uso': data.datosDelEstablecimiento.giro,
        'Tipo de obra': data.datosDelEstablecimiento.tipoDeObra
    }
        agregarCódigo(datosDeLaSolicitud, 'datos_de_la_solicitud_modal')
        agregarCódigo(datosDelInteresado, 'datos_del_interesado_modal')
        agregarCódigo(direccion, 'direccion_modal')
        agregarCódigo(datosDelEstablecimiento, 'datos_del_establecimiento_modal')
    });

    function agregarCódigo( jsonO, idObjective ){
        let node = document.createElement("div")
        node.innerHTML = crearModulo(jsonO)
        document.getElementById(idObjective).appendChild(node); 
     
    }
    
    function encontrarTramite(code){
        catalogoDeTramites.forEach(element => {
            if(element.code == code){
                return (element.description)
            }
            
        });
    }

    function validarNumerosExteriorInterior(){

    }
    
    function crearModulo( json){
        html = "<div class='grid-container'>"
        for (let i in json){
            html += `    <div class="titulo">
                            ${i}:
                        </div>
                        <div class="data">
                            ${json[i]}
                        </div>   `            
        }
        
        html += "</div>"
        return html
    }
    
}

$(".delete-info-modal").click(function(){
    $("#datos_de_la_solicitud_modal").children().empty();
    $("#datos_del_interesado_modal").children().empty();
    $("#direccion_modal").children().empty();
    $("#datos_del_establecimiento_modal").children().empty();
})

//function para capturar la informacion de la colonia del select
$("#coloniaGroup").delegate("select","change",function(){
    data.direccion.colonia = $(this).val();
});

//function para capturar la informacion de la colonia cuando aun es in input y no un select.
$('#colonia').change(function(){
    data.direccion.colonia = $(this).val();
})

$('#modalidades').change(function(){
    var res = $(this).val().split(",");
    data.modalidad_id = res[0];
})

function guardarProcesoTramite(){
    $('#guardarTramite').click(function(){ 
        

            console.log('Tramite guardandose...')
            let informacion = {
                status              :    data.status,
                tipoDePersona       :    data.tipoDePersona,
                tramite             :    data.tramite_id,
                modalidad           :    data.modalidad_id,
                modalidad_texto     :    data.modalidad,
                requisitos          :    data.requisitos, 
                observaciones       :    data.observaciones,
                nombre              :    data.datosDelInteresado.nombre,
                apellidoPaterno     :    data.datosDelInteresado.apellidoPaterno,
                apellidoMaterno     :    data.datosDelInteresado.apellidoMaterno,
                identificacion      :    data.datosDelInteresado.identificacion,
                sexo                :    data.datosDelInteresado.sexo,
                grupoVulnerable     :    data.datosDelInteresado.grupoVulnerable_id,
                rfc                 :    data.datosDelInteresado.rfc,
                telefono            :    data.datosDelInteresado.telefono,
                email               :    data.datosDelInteresado.correoElectronico,
                codigoPostal        :    data.direccion.codigoPostal,
                colonia             :    data.direccion.colonia,
                delegacion          :    data.direccion.delegacion,
                calle               :    data.direccion.calle,
                noExterior          :    data.direccion.noExterior,
                noInterior          :    data.direccion.noInterior,
                giro                :    data.datosDelEstablecimiento.giro,
                tipoDeObra          :    data.datosDelEstablecimiento.tipoDeObra,
                medioDeSolicitud    :    data.medioDeSolicitud,
                cartaPoderFormato   :    data.cartaPoderFormato
            }

            respuesta = Validate_information();
            if (respuesta[1] != 'false'){
                 var urlFormSave = $Ctr_request + 'RequestDataSave';
                 $.ajax({
                    type: "POST",
                    url: urlFormSave,
                    data: informacion,
                    dataType: "JSON",
                    success: function (data) {
                        if(data.response){
                            swal("Folio #"+data.folio, "La información se guardo de manera correcta", "success");
                            SetInformationAccuse(informacion,data);
                        }else{
                            swal("ocurrio un error", "ocurrio un error", "error");
                        }
                    },
                    error: function(xhr, textStatus, errorThrown){
                        swal("ocurrio un error", "ocurrio un error", "error");
                    return false
                    }
                }); 
                return false;
            }else{
                swal("Por favor revisa tu información", respuesta[0] , "error");
            }

           
    });
}

function Validate_information(){
    respuesta = [];
    message = "";
    status = true;
    if (($("#tipo_moral").is(':checked') == false) && $("#tipo_fisica").is(':checked') == false){
        message += 'Campo "Persona *" vacio\n';
        status = false;
    }
    if($("#search").val() == ""){
        message += 'Campo "Trámite *" vacio\n';
        status = false;
    }
    if (($("#completo").is(':checked') == false) && $("#art_49").is(':checked') == false){
        message += 'Campo "Ingreso *" vacio\n';
        status = false;
    }
    if($("#nombre").val() == ""){
        message += 'Campo "Nombre(s) / Razón social *" vacio\n';
        status = false;
    }
    if($("#grupoVulnerable").val() == ""){
        message += 'Campo "Grupo vulnerable *" vacio\n';
        status = false;
    }
    if($("#telefono").val() == ""){
        message += 'Campo "Telefono *" vacio\n';
        status = false;
    }
    if($("#codigoPostal").val() == ""){
        message += 'Campo "Código postal *" vacio\n';
        status = false;
    }
    if($("#colonia").val() == ""){
        message += 'Campo "Colonia *" vacio\n';
        status = false;
    }
    if($("#delegacion").val() == ""){
        message += 'Campo "Delegación *" vacio\n';
        status = false;
    }
    if($("#calle").val() == ""){
        message += 'Campo "Calle *" vacio\n';
        status = false;
    }
    if (($("#solicitado_en_linea").is(':checked') == false) && $("#solicitado_presencialmente").is(':checked') == false){
        message += 'Campo "Medio por el cual solicita *" vacio\n';
        status = false;
    }
    

    
    respuesta =[message,status];
    return respuesta;
}

function SetInformationAccuse(informacion,data){
    var fecha = new Date();
    var month = fecha.getMonth()+1;
    var day = fecha.getDate();
    var year = fecha.getFullYear();
    var fecha =(day + '-' + month + '-' + year);
    $("#data-request").prop('disabled', true);
    $("#transact-request").prop('disabled', true);
    $('#data-acuse').parent().attr("class","active");
    $('#data-request').parent().removeClass("active");    
    $('#request-data').hide();
    $('#request-acuse').fadeIn();
    $('#revision_formulario').modal('hide');
    $('.modal-backdrop').hide();
    $('#request-acuse').css('display','');
    $('#step3').css('display','');
    var str = $("#search").val();
    var transact = str.split(",");
    $('#acuse-date').html(fecha);
    $('#acuse-folio').html(data.folio);
    $('#acuse-transact-name').html(transact[1]);
    if(informacion.noExterior == ""){
            informacion.noExterior = "N/A";
        }
    if(informacion.noInterior == ""){
        informacion.noInterior = "N/A";
    }
    $('.acuse-name').html(informacion.nombre+' '+informacion.apellidoPaterno+' '+informacion.apellidoMaterno);
    $('#acuse-phone').html(informacion.telefono);
        
    if(informacion.giro == ""){
        informacion.giro ="N/A";
    }
    $('#acuse-giro').html(informacion.giro);    
    if(informacion.tipoDeObra == ""){
        informacion.tipoDeObra ="N/A";
    }
    $('#acuse-obra').html(informacion.tipoDeObra);
    $('#acuse-address').html(informacion.delegacion+', '+informacion.colonia+', '+informacion.codigoPostal+', '+informacion.calle+',  No. Ext. '+informacion.noExterior+', No. Int. '+informacion.noInterior);
    $('#acuse-observaciones').html(informacion.observaciones);
    
    if(informacion.medioDeSolicitud == 1){
        informacion.medioDeSolicitud = "En línea";
    }else if(informacion.medioDeSolicitud == 2){
        informacion.medioDeSolicitud = "Presencial";
    }
    $('#acuse-fecha-compromiso').html(data.fechaCompromiso);
    $('#acuse-medio-solicitud').html(informacion.medioDeSolicitud);
    $('#acuse-user').html($vud_user);
    if(informacion.status == 1){
        $("#acuse-doc-complete").html('Documentación Completa <strong>(X)</strong>');
        $("#acuse-doc-49").html('');
    }else{
        $("#acuse-doc-49").html('Artículo 49 <strong>(X)</strong>');
        $("#acuse-doc-complete").html('');
    }
    $('#acuse-modality-name').html(informacion.modalidad_texto);

    $("#data-request").hide();
    $("#transact-request").hide();
}

$("#delegacion").blur(function(){
    data.direccion.delegacion = $("#delegacion").val();
})







