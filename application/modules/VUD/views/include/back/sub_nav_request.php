    <!-- Navbar del  -->
    <nav id="request_nav">
        <ul>
            <li class='nav-item-request '>
                <a class="event-step" id="transact-request" href="">
                    <p>
                        <i class="fa fa-check-square"></i>
                    </p>
                    <p>Requisitos del trámite </p>
                </a>
            </li>
            <li class='nav-item-request'>
                <a class="event-step" id="data-request" href="">
                    <p>
                        <i class="fa fa-file-text"></i>
                    </p>
                    <p>Datos de la solicitud </p>
                </a>
            </li>
            <li class=''>
                <a class="event-step" id="data-acuse" href="">
                    <p>
                        <i class="fa fa-print"></i>
                    </p>
                    <p>Acuse </p>
                </a>
            </li>
        </ul>
    </nav>

