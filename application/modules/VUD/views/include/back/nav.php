  <!-- Navbar -->
    <navbar>
        <ul class="hover-ul-effect">            
            <li><a href="<?=ROOT_URL?>dashboard">Dashboard</a></li>
            <li><a href="<?=ROOT_URL?>solicitud">Solicitud de trámite</a></li>
            <li><a href="<?=ROOT_URL?>seguimiento">Seguimiento</a></li>
            <li><a href="<?=ROOT_URL?>reporte">Reportes</a></li>
            <li><a href="<?=ROOT_URL?>usuarios">Catálogos</a></li>
        </ul>
        <ul class="session-options">    
            <li> <a href="#">
                <i class="fa fa-book session"> Manual de Usuario</i>
                </a>
            </li>
            <li> <a href="#">
                <i class="fa fa-user-circle session"> <?= $this->session->userdata('username'); ?></i>
                </a>
            </li>
            <li> <a class="nav-link" data-toggle="modal" data-target="#logout">
                    <i class="fa fa-sign-out session"> Salir</i>
                </a>   
            </li>      
        </ul>
    </navbar>    
    
     <!-- Logout Modal-->
     <div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">¿Desea cerrar sesión?</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">De clic en "salir" para cerrar sesión.</div>
            <div class="modal-footer">
              <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
              <a class="btn btn-danger" href="<?=ROOT_URL;?>/VUD/closeSession">Salir</a>
            </div>
          </div>
        </div>
      </div>
