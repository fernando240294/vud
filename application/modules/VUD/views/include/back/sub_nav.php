<!-- Navbar del elementos  -->
   <nav id="request_nav">
       <ul>
           <li id="user_sub_nav" class='nav-item-request  '>
               <a href="<?=ROOT_URL?>usuarios">
                   <p>
                       <i class="fa fa-user" aria-hidden="true"></i>
                   </p>
                   <p>Usuarios </p>
               </a>
           </li>
           <li id="transact_sub_nav" class='nav-item-request '>
                   <a href="<?=ROOT_URL?>tramites">
                       <p>
                           <i class="fa fa-book" aria-hidden="true"></i>
                       </p>
                       <p> Trámites </p>
                   </a>
               </li>
           <li id="modality_sub_nav" class='nav-item-request '>
               <a href="<?=ROOT_URL?>modalidades">
                   <p>
                       <i class="fa fa-cubes" aria-hidden="true"></i>
                   </p>
                   <p>Modalidades</p>
               </a>
           </li>
           <li id="requirement_sub_nav" class='nav-item-request '>
                   <a href="<?=ROOT_URL?>requisitos">
                       <p>
                          <i class="fa fa-list" aria-hidden="true"></i>
                       </p>
                       <p> Requisitos </p>
                   </a>
               </li>
       </ul>
   </nav>
