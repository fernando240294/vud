<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>VUD-GAM</title>

    <link rel="stylesheet" href="<?=URL_TEMPLATE?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=URL_TEMPLATE?>css/style.css">
    <link rel="stylesheet" href="<?=URL_TEMPLATE?>css/processing.css">
    <link rel="stylesheet" href="<?=URL_TEMPLATE?>fonts/css/font-awesome.css">
    <script src="<?=URL_TEMPLATE?>js/jquery.js"></script>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</head>

<style>
    body{
      /*background-image: url("/images/del_background2.jpg");
      background-size: 100% 100%;
      background-attachment: fixed;*/
    }
</style>
<body class="fixed-nav sticky-footer" >