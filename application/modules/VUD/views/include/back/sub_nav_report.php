<!-- Navbar del elementos  -->
   <nav id="request_nav">
       <ul>
           <li id="sub_nav_report" class='nav-item-request'>
               <a href="">
                   <p>
                       <i class="fa fa-file-text-o" aria-hidden="true"></i>
                   </p>
                   <p>Reporte</p>
               </a>
           </li>
           <li id="sub_nav_report_year" class='nav-item-request '>
                   <a href="">
                       <p>
                           <i class="fa fa-book" aria-hidden="true"></i>
                       </p>
                       <p>Reporte anual</p>
                   </a>
               </li>
       </ul>
   </nav>
