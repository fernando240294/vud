<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ctr_modality extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_modality');
        $this->load->model('Mdl_log');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('encryption');

        if (!$this->session->userdata('loginFlag')){
            $uri = ROOT_URL;
            redirect($uri, 'redirect');
        }
    }

    public function index(){
      $result = $this->Mdl_modality->vud_modality_record();
      $data['modality'] = $this->FormatModalityRecord($result);
      $result = $this->Mdl_modality->vud_record_transact();
      $data['select_transact'] = $this->FormatRecordSelect($result,1);
      $result = $this->Mdl_modality->vud_record_requirement();
      $data['select_requirement'] = $this->FormatRecordSelect($result,2);
      $this->load->view('include/back/header');
      $this->load->view('include/back/nav');
      $this->load->view('include/back/sub_nav');
      $this->load->view('vw_modality/vw_modality',$data);
      $this->load->view('include/back/footer');
      $this->load->view('vw_modality/include/script');
    }

    public function CreateModality(){
      $data['user_register'] = $this->session->userdata('employee_number');
      $data['modality_transact'] = $this->input->post('modality_transact');
      $data['modality_code'] = $this->input->post('modality_code');
      $data['modality_description'] = $this->input->post('modality_description');
      $data['modality_legal_foundament'] = $this->input->post('modality_legal_foundament');
      $data['modality_days'] = $this->input->post('modality_days');
      $data['type_days_modality'] = $this->input->post('type_days_modality');

      $requirements = $this->input->post('modality_requirements');
      $response = false;
      $message = "";

      $result = $this->Mdl_modality->vud_create_modality($data,$requirements);
      if($result){
        $response = true;
        $message = "Se ingreso la modalidad de manera correcta";
      }else{
        $message = "Ups, tenemos un problema, valida bien la información.";
      }
      $this->Mdl_log->vud_log_insert($data['user_register'],$message);
      echo json_encode(['response'=>$response, 'message'=>$message]);
    }

    public function UpdateModality(){
      $data['user_register'] = $this->session->userdata('employee_number');
      $data['udpate_id_modality'] = $this->input->post('udpate_id_modality');
      $data['udpate_code_modality'] = $this->input->post('udpate_code_modality');
      $data['update_modality_description'] = $this->input->post('update_modality_description');
      $data['update_modality_legal_foundament'] = $this->input->post('update_modality_legal_foundament');
      $data['update_modality_days'] = $this->input->post('update_modality_days');
      $data['update_type_days_modality'] = $this->input->post('update_type_days_modality');
      $requirements = $this->input->post('modality_requirements');
      $response = false;
      $message = "";
      $result = $this->Mdl_modality->vud_update_modality($data,$requirements);
      if($result){
        $response = true;
        $message = "Se actualizaron los datos de manera correcta";
      }else{
        $message = "Los datos no se guardaron bien, favor de intentarlo de nuevo";
      }
      $this->Mdl_log->vud_log_insert($data['user_register'],$message);
      echo json_encode(['response'=>$response, 'message'=>$message]);
    }


    public function Format_update_requirements(){
      $_code_transact = $this->input->post('code_modality');
      $marked_requirement = $this->Mdl_modality->vud_get_modality_requirements($_code_transact);
      $requirements = $this->Mdl_modality->vud_record_requirement();
      $f=0;
      $cheked ="";
        foreach ($requirements as $key => $value){
          if((!empty($marked_requirement[$f]['id'])) && ($value['id'] == $marked_requirement[$f]['id'])){
            $cheked .= '<div class="checkbox">
                        <label><input name="modality_requirements[]" type="checkbox" value="'.$value['id'].'" checked>'.$value['description'].'</label>
                        </div>';
                          $f++;
          }else{
            $cheked .= '<div class="checkbox">
                        <label><input name="modality_requirements[]" type="checkbox" value="'.$value['id'].'">'.$value['description'].'</label>
                        </div>';
          }
        }
        echo json_encode(['cheked' =>$cheked]);
    }

    public function DeleteModality(){
      $data['user_register'] = $this->session->userdata('employee_number');
      $id_modality = $this->input->post('id_modality');
      $response = false;
      $message = "";
      $result = $this->Mdl_modality->vud_delete_modality($id_modality);
        if($result){
          $response = true;
          $message = "El registro se elimino de manera correcta.";
        }else{
          $message = "Ocurrio un problema al eliminar el registro.";
        }
      $this->Mdl_log->vud_log_insert($data['user_register'],$message);
      echo json_encode(['response' => $response, 'message' => $message]);
    }

    public function FormatModalityRecord($result){
      if($result){
      $count = 1;
      $modality = "";
      $modality .='<div class="card mb-3">
                    <div class="card-header">
                    <i class="fa fa-table"></i> Modalidades<button type="button" style="float: right; 50px; height: 40px;" class="btn btn-outline-success fa fa-plus-circle fa-lg" data-toggle="modal" data-target="#add_transact"></button></div>
                      <div class="table-responsive">
                    <table class="table table-bordered dataTable" id="" width="100%" cellspacing="0">
                    <thead>
                       <tr>
                      <th>ID</th>
                      <th>Clave</th>
                      <th>Descripción</th>
                      <th>Clave de Trámite</th>
                      <th>Descripción de Trámite</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>';

        foreach ($result as $key => $value) {
          $value['description'] = preg_replace("/[\r\n|\n|\r]+/", " ", $value['description']);
          $value['legal_foundation'] = preg_replace("/[\r\n|\n|\r]+/", " ", $value['legal_foundation']);
          $modality .= "<tr>
                          <td>".$count."</td>
                          <td>".$value['code']."</td>
                          <td>".$value['description']."</td>
                          <td>".$value['code_transact']."</td>
                          <td>".$value['description_transact']."</td>
                          <td> <button type='button' style='50px; height: 40px;' class='btn btn-outline-warning fa fa-pencil-square-o fa-lg' data-toggle='modal' data-target='#vud_update_transact' onclick='return Modality_update_data(".$value['id'].",".$value['code'].",\"$value[description]\",\"$value[legal_foundation]\",".$value['code_transact'].",".$value['days'].",\"$value[type_days]\");'></button>
                          <button type='button' style='50px; height: 40px;' class='btn btn-outline-danger fa fa-trash-o fa-lg' onclick='return delete_modality(".$value['id'].");'></button>";
          $count++;
        }
        $modality .='</tr>
                      </tbody>
                    </table>
                    </div>
                    </div>
                    <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
                  </div>';
  
      }else{
        $modality = "<div class='alert alert-danger'>
                    <strong>No se cuenta con ningun registro de modalidad.</strong>
                  </div>";
      }
      return $modality;
    }

    public function FormatRecordSelect($result,$number){
      $select = "";
      if(!empty($result)){
        if($number == 1 && count($result)>0){
          foreach ($result as $key => $value) {
            $value['description'] = substr($value['description'],0,100);
            $select .= '<option value="'.$value['code'].'">'.$value['code'].', '.$value['description'].'...'.'</option>';
          }
        }elseif ($number == 2 && count($result) > 0){
          foreach ($result as $key => $value) {
            $select .= '<div class="checkbox">
                        <label><input name="modality_requirements[]" type="checkbox" value="'.$value['id'].'">'.$value['description'].'</label>
                        </div>';
          }
        }  
      }
      
      return $select;
    }

}
