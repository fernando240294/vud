<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ctr_report extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_report');
        $this->load->model('Mdl_log');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('Pdf');
        date_default_timezone_set('America/Mexico_City');
        if (!$this->session->userdata('loginFlag')){
            $uri = ROOT_URL;
            redirect($uri, 'redirect');
        }
    }

    public function index(){
        //Se cargan todos los select necesarios de los filtros para el reporte
        $result = $this->Mdl_report->vud_status_list();
        $data['status'] = $this->Data_record_list($result,2);
        $result = $this->Mdl_report->vud_record_area_revisora();
        $data['area'] = $this->Data_record_list($result,2);
        $result = $this->Mdl_report->vud_record_vulnerable_group();
        $data['grupo_vulnerable'] = $this->Data_record_list($result,2);
        $result = $this->Mdl_report->vud_record_delegation();
        $data['delegation'] = $this->Data_record_list($result,3);
        $this->load->view('include/back/header');
        $this->load->view('include/back/nav');
        $this->load->view('include/back/sub_nav_report');
        $this->load->view('vw_report/vw_report',$data);
        $this->load->view('include/back/footer');
        $this->load->view('vw_report/include/script');
    }

    /**
    **Funcion para crear el reporte anual
    **/
    public function ReportYear(){
        //Se valida si se envio el año, del cual se quiere tener el reporte, si no, se toma el año en curso
        if (!empty($this->input->post('report_full_year'))) {
            $year = $this->input->post('report_full_year');
        }else{
            $year = date('Y');
        }
        //Variables que contienen las solicitudes por mes.
        $enero = 0;
        $febrero = 0;
        $marzo = 0;
        $abril = 0;
        $mayo = 0;
        $junio = 0;
        $julio = 0;
        $agosto = 0;
        $septiembre = 0;
        $octubre = 0;
        $noviembre = 0;
        $diciembre = 0;
        $total = 0;
        //se consulta la informacion.
        $result = $this->Mdl_report->vud_year_report($year);
        $count =1;
        //Se maqueta la table de la informacion obtenida.
        $table = "";
        $table .= "<table>
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Trámite</th>
                          <th>Modalidad</th>
                          <th>Enero</th>
                          <th>Febrero</th>
                          <th>Marzo</th>
                          <th>Abril</th>
                          <th>Mayo</th>
                          <th>Junio</th>
                          <th>Julio</th>
                          <th>Agosto</th>
                          <th>Septiembre</th>
                          <th>Octubre</th>
                          <th>Noviembre</th>
                          <th>Diciembre</th>
                          <th>Total</th>
                        </tr>
                      </thead>
                      <tbody>";
            foreach ($result as $key => $value) {
               $table .= "<tr>
                              <td>".$count."</td>
                              <td>".$value['clave_tramite'].', '.$value['descripcion_tramite']."</td>
                              <td>".$value['clave_modalidad'].' '.$value['descripcion_modalidad']."</td>
                              <td>".$value['enero']."</td>
                              <td>".$value['febrero']."</td>
                              <td>".$value['marzo']."</td>
                              <td>".$value['abril']."</td>
                              <td>".$value['mayo']."</td>
                              <td>".$value['junio']."</td>
                              <td>".$value['julio']."</td>
                              <td>".$value['agosto']."</td>
                              <td>".$value['septiembre']."</td>
                              <td>".$value['octubre']."</td>
                              <td>".$value['noviembre']."</td>
                              <td>".$value['diciembre']."</td>";
                              if($value['total_de_modalidades'] != 0){
                                $table .= "<td>".$value['total_de_modalidades']."</td>";
                              }else{
                                $table .= "<td>".$value['total_de_tramites']."</td>";
                              }
                            $table .="</tr>";
                            $count++;
                            //Se acumulan los totales por mes.
                            $enero = $enero+$value['enero'];
                            $febrero =$febrero + $value['febrero'];
                            $marzo = $marzo + $value['marzo'];
                            $abril = $abril + $value['abril'];
                            $mayo = $mayo + $value['mayo'];
                            $junio = $junio + $value['junio'];
                            $julio = $julio + $value['julio'];
                            $agosto = $agosto + $value['agosto'];
                            $septiembre = $septiembre + $value['septiembre'];
                            $octubre = $octubre + $value['octubre'];
                            $noviembre = $noviembre + $value['noviembre'];
                            $diciembre = $diciembre + $value['diciembre'];
                            $total = $total + $value['total_de_tramites'];
            }
            $table .="<tr>
                              <td></td>
                              <td></td>
                              <td>TOTAL</td>
                              <td>".$enero."</td>
                              <td>".$febrero."</td>
                              <td>".$marzo."</td>
                              <td>".$abril."</td>
                              <td>".$mayo."</td>
                              <td>".$junio."</td>
                              <td>".$julio."</td>
                              <td>".$agosto."</td>
                              <td>".$septiembre."</td>
                              <td>".$octubre."</td>
                              <td>".$noviembre."</td>
                              <td>".$diciembre."</td>
                              <td>".$total."</td>
                    </tr>";

            $table .= "</tbody>
                        </table>";
            $this->generar($table,2);
    }

    /**
    **Funcion para crear el reporte por filtro
    **/
    public function CreateReport(){
        //se cargan los datos de los filtros
        $count = 0;
        $filter = "";
        $column = "";
        $data['report_ejecicio'] = $this->input->post('report_ejecicio');
        $data['report_delegacion'] = $this->input->post('report_delegacion');
        $data['report_estatus'] = $this->input->post('report_estatus');
        $data['report_area_revisorsa'] = $this->input->post('report_area_revisorsa');
        $data['report_resp_dictamen'] = $this->input->post('report_resp_dictamen');
        $data['report_tipo_persona'] = $this->input->post('report_tipo_persona');
        $data['report_grupo_vulnerable'] = $this->input->post('report_grupo_vulnerable');
        $data['report_fecha_inicio'] = $this->input->post('report_fecha_inicio');
        $data['report_fecha_final'] = $this->input->post('report_fecha_final');

            /*
            **Se valida, en caso de que alguno de los campos contenga informacion, se agrega una codicion en **el where de la consulta, y aparte se agrega el campo que se mostrara en la clausula "SELECT"
            */
            if (!empty($data['report_ejecicio'])){
                $filter .= " and TO_CHAR(date_creation,'YYYY')= '".$data['report_ejecicio']."'";
                $count++;
            }
            if (!empty($data['report_delegacion'])){
                //columna que sera agregada en la clausula "SELECT"
                $column .= ",d.delegation";
                //condición que sera agregada en la clausula "WHERE"
                $filter .= " and d.delegation = '".$data['report_delegacion']."'";
                $count++;
            }
            if (!empty($data['report_estatus'])){
                $column .= ",h.description as estatus";
                $filter .= " and a.id_status = '".$data['report_estatus']."'";
                $count++;
            }
            if (!empty($data['report_area_revisorsa'])){
                $column .= ",j.description as review_area";
                $filter .= " and e.review_area = ".$data['report_area_revisorsa'];
                $count++;
            }
            if (!empty($data['report_resp_dictamen'])){
                $column .= ",f.answer_dictum";
                $filter .= " and f.answer_dictum = ".$data['report_resp_dictamen'];
                $count++;
            }
            if (!empty($data['report_tipo_persona'])){
                $column .= ",g.type_person";
                $filter .= " and g.type_person = ".$data['report_tipo_persona'];
                $count++;
            }
            if (!empty($data['report_grupo_vulnerable'])){
                $column .= ",i.description as grupo_vulnerable";
                $filter .= " and a.id_vulnerable_group = ".$data['report_grupo_vulnerable'];
                $count++;
            }
            if (!empty($data['report_fecha_inicio']) && !empty($data['report_fecha_final'])){
                 $filter .= " AND a.date_creation BETWEEN '".$data['report_fecha_inicio']."'
                             AND '".$data['report_fecha_final']."'";
                $count++;
            }
            
            /**
            **En caso de que se cuente con algun filtro ($count>0), se realiza la consulta y se agregan las **clausulas where y las columnas en la clausula select ($column,$filter)
            **De lo contrario se ejecuta la una consulta por default del año en curso
            **/
            if($count>0){
                //consulta en caso de que se seleccione algun filtro
                $result = $this->Mdl_report->vud_report_filter($column,$filter);
            }else{
                //consulta por defaul del año en curso
                $result = $this->Mdl_report->vud_record_report();
            }
            /**
            **En caso de que no se encuentre información, se carga el mensaje por defecto
            **"No se encontraron resultados.", en caso contrario se imprimen los recultados de las **solicitudes.
            **/
            if(!$result){
                $table ="<h1>No se encontraron resultados.</h1>";
                $this->generar($table,1);
            }else{
                $table = $this->FormatReportTable($result,$data);
                $this->generar($table,1);
            }
    }

    public function FormatReportTable($result,$data){
        $count = 1;
        $table = "";
        $table .=" <table>
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Folio</th>
                          <th>Trámite</th>
                          <th>Modalidad</th>
                          <th>Fecha creación</th>
                          <th>Fecha compromiso</th>";

                if(!empty($data['report_delegacion'])){
                    $table .="<th>Delegación</th>";
                }
                if(!empty($data['report_estatus'])){
                    $table .="<th>Estado</th>";
                }
                if(!empty($data['report_area_revisorsa'])){
                    $table .="<th>Area revisora</th>";
                }
                if(!empty($data['report_resp_dictamen'])){
                    $table .="<th>Dictamen</th>";
                }
                if(!empty($data['report_tipo_persona'])){
                    $table .="<th>Tipo de persona</th>";
                }
                if (!empty($data['report_grupo_vulnerable'])) {
                    $table .="<th>Grupo vulnerable</th>";
                }
        $table .="  </tr>
                    </thead>
                    <tbody>";

        foreach ($result as $key => $value) {
            $table .= "<tr>
                      <td>".$count."</td>
                      <td>".$value['folio']."</td>
                      <td>".$value['clave_tramite'].", ".$value['tramite']."</td>";
                
                if (!empty($value['clave_modalidad'])) {
                    $table .="<td>".$value['clave_modalidad'].", ".$value['modalidad']."</td>";
                }else{
                    $table .="<td>Sin Modalidad</td>";
                }

            $table .= "<td>".$value['fecha_creacion']."</td>
                      <td>".$value['fecha_compromiso']."</td>";

                if(!empty($data['report_delegacion'])){
                    $table .="<td>".$value['delegation']."</td>";
                }
                if(!empty($data['report_estatus'])){
                    $table .="<td>".$value['estatus']."</td>";
                }
                if(!empty($data['report_area_revisorsa'])){
                    $table .="<td>".$value['review_area']."</td>";
                }
                if(!empty($data['report_resp_dictamen'])){
                    if ($data['report_resp_dictamen']==1) {
                        $table .="<td>Autorizada</td>";
                    }else{
                        $table .="<td>Rechazada</td>";
                    }
                    
                }
                if(!empty($data['report_tipo_persona'])){
                    if ($data['report_tipo_persona']== 1) {
                        $table .="<td>Moral</td>";    
                    }else{
                        $table .="<td>Física</td>";
                    }
                    
                }
                if (!empty($data['report_grupo_vulnerable'])) {
                    $table .="<td>".$value['grupo_vulnerable']."</td>";
                }
            $table .="</tr>";   
                    $count++;
        }
        $table .="</tbody>
                </table>";      
        return $table;  
    }

    public function generar($table,$report){

        $pdf = new Pdf('L', 'mm', 'LETTER', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('RYC LATAM');
        $pdf->SetTitle('Reporte de Ventanilla Unica Digital.');
        $pdf->SetSubject('Tutorial TCPDF');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        /*$image_file = K_PATH_IMAGES.'img/logo_reporte.png';
        $pdf->Image($image_file, 10, 10, 15, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        $pdf->SetFont('helvetica', 'B', 20);
        // Title
        $pdf->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');*/

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH,'Ventanilla Única Delegacional', PDF_HEADER_STRING,'' , '');
        $pdf->setFooterData($tc = array(0, 64, 0), $lc = array(0, 64, 128));

        //datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries config
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //relación utilizada para ajustar la conversión de los píxeles
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // ---------------------------------------------------------
        // establecer el modo de fuente por defecto
        $pdf->setFontSubsetting(true);

        // Establecer el tipo de letra

        //Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
        // Helvetica para reducir el tamaño del archivo.
        $pdf->SetFont('Helvetica', '', 6, '', true);

        if ($report == 1) {
            // Añadir una página
            $pdf->AddPage('P','LETTER');    
        }else{
            // Añadir una página
            $pdf->AddPage('L','A4');
        }
        
        // Este método tiene varias opciones, consulta la documentación para más información.
        //fijar efecto de sombra en el texto
        $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));

        if ($report == 1){
            $html = "<style>
                        table {
                            font-family: arial, sans-serif;
                            border-collapse: collapse;
                            width: 100%;
                        }
                        td{
                            font-size: 10px;
                            border: 1px solid #dddddd;
                            text-align: left;
                            padding: 8px;
                        }
                        th {
                            font-size: 15px;
                            border: 1px solid #dddddd;
                            text-align: left;
                            padding: 8px;
                        }
                        h1 {
                            text-align: center;
                        }
                    </style>".$table;    
        }else{
            $html = "<style>
                        table {
                            font-family: arial, sans-serif;
                            border-collapse: collapse;
                            width: 100%;
                        }
                        td{
                            border: 1px solid #dddddd;
                            text-align: left;
                            padding: 8px;
                        }
                        th {
                            font-size: 10px;
                            border: 1px solid #dddddd;
                            text-align: left;
                            padding: 8px;
                        }
                        h1 {
                            text-align: center;
                        }
                    </style>".$table;
        }

        // Imprimimos el texto con writeHTMLCell()
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);

        // ---------------------------------------------------------
        // Cerrar el documento PDF y preparamos la salida
        // Este método tiene varias opciones, consulte la documentación para más información.
        $nombre_archivo = utf8_decode("Reporte.pdf");
        $pdf->Output($nombre_archivo, 'I');

        $data['user_register'] = $this->session->userdata('employee_number');
        $message = "El usuario ".$this->session->userdata('username')." genero un reporte";
        $this->Mdl_log->vud_log_insert($data['user_register'],$message);
    }

    /**
    **Funcion para cargar la información de los Select "Delegacion", "Area revisora","Estatus" y "Grupo **Vulnerable".
    **
    **/
    public function Data_record_list($result,$type){
        $record = "";
        if (!empty($result)) {
            if($type == 1){
                foreach ($result as $key => $value) {
                    $record .='<option value="'.$value["code"].'">'.$value["code"].', '.substr($value["description"],0, 100).'..'.'</option>';
                }
            }else if($type == 2){
                foreach ($result as $key => $value) {
                    $record .='<option value="'.$value["id"].'">'.$value['description'].'</option>';
                }
            }else if($type == 3){
                foreach ($result as $key => $value) {
                    $record .='<option value="'.$value["delegation"].'">'.$value['delegation'].'</option>';
                }
            }    
        }

        return $record;                  
    }
}
