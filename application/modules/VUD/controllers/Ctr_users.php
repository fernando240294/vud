<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ctr_users extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_users');
        $this->load->model('Mdl_log');
        $this->load->library('session');
        $this->load->helper('url');

        if (!$this->session->userdata('loginFlag')){
            $uri = ROOT_URL;
            redirect($uri, 'redirect');
        }
    }
    
    public function index(){
      $result = $this->Mdl_users->vud_users_record();
      $data['users'] = $this->FormatUserRecord($result);
      $result = $this->Mdl_users->vud_role();
      $data['role'] = $this->RoleRecord($result);
      $this->load->view('include/back/header');
      $this->load->view('include/back/nav');
      $this->load->view('include/back/sub_nav');
      $this->load->view('vw_users/vw_users',$data);
      $this->load->view('include/back/footer');
      $this->load->view('vw_users/include/script');
    }

    public function FormatUserRecord($result){
        $users = "";
        $count = 1;
        $users .='<div class="card mb-3">
                      <div class="card-header">
                        <i class="fa fa-table"></i> Usuarios <button type="button" style="float: right; width: 50px; height: 40px;" class="btn btn-outline-success fa fa-user-plus fa-lg" data-toggle="modal" data-target="#add_user"></button></div>
                        <div class="card-body">
                      <div class="table-responsive">
                    <table class="stripe table table-striped table-bordered dataTable" id="" width="100%" cellspacing="0">
                    <thead>
                       <tr>
                      <th>ID</th>
                      <th>Número de Empleado</th>
                      <th>Nombre Usuario</th>
                      <th>Usuario que dio alta</th>
                      <th>Rol</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>';
        foreach ($result as $key => $value) {
            $users .="<tr>
                  <td>".$count."</td>
                  <td>".$value['employee_number']."</td>
                  <td>".$value['user_name']."</td>
                  <td>".$value['user_register']."</td>
                  <td>".$value['rol']."</td>
                  <td>
                  <button type='button' style='width: 50px; height: 40px;' class='btn btn-outline-warning fa fa-pencil-square-o fa-lg'   data-toggle='modal' data-target='#vud_update_user' onclick='return user_update_data(". $value['id'] . ", \"$value[user_name]\",".$value['employee_number'].", \"$value[description]\",".$value['id_rol'].",".$this->session->userdata('employee_number').")'></button>
                  <button style='width: 50px; height: 40px;' type='button' class='btn btn-outline-danger fa fa-user-times fa-lg' onclick='return delete_user(" . $value['id'] . ");'></button>
                  </td>
                </tr>";

                //<button type='button' data-toggle='modal' data-target='#vud_user_permission' class='btn btn-primary' onclick='return UserPermissions(" . $value['id'] . ");'>Permisos</button>
            $count++;
        }
        $users .='</tbody>
                    </table>
                    </div>
                    </div>
                  </div>';
        return $users; 
    }

    public function RoleRecord($result){
        $role = "";
        foreach ($result as $key => $value) {
            $role .='<option value="'.$value["id"].'">'.$value["description"].'</option>';
        }
        return $role;                  
    }

    public function CreateUser(){
      $data['user_register'] = $this->session->userdata('employee_number');
      $data['user_name'] = $this->input->post('user_name');
      $data['employee_number'] = $this->input->post('employee_number');
      $data['description'] = $this->input->post('description');
      $data['user_password'] = md5($this->input->post('user_password'));
      $data['role'] = $this->input->post('role');
      $response = false;
      $message = "";
      if(!empty($data)){
        $result = $this->Mdl_users->vud_insert_user($data);
        if($result){
          $response = true;
          $message = "Un usuario se ingreso de manera correcta";
        }else{
          $message = "Sucedio un error al ingresar un usuario";
        }
      }else{
        $message = "Sucedio un error al ingresar un usuario";
      }
      $this->Mdl_log->vud_log_insert($data['user_register'],$message);
      echo json_encode(['response'=>$response, 'message'=>$message]);
    }

    public function UpdateUser(){
      $data['user_register'] = $this->session->userdata('employee_number');
      $data['id_user'] = $this->input->post('id_user');
      $data['user_register'] = $this->input->post('id_user_update');
      $data['user_name'] = trim($this->input->post('update_user_name'));
      $data['employee_number'] = $this->input->post('update_employee_number');
      $data['description'] = $this->input->post('update_description');
      $data['user_password'] = $this->input->post('update_user_password');
        if (!empty($data['user_password'])) {
          $data['user_password'] = md5($this->input->post('update_user_password'));
        }
      $data['role'] = $this->input->post('update_role');
      $response = false;
      $message = "";
      if(!empty($data)){
        $result = $this->Mdl_users->vud_update_user($data);
        if($result){
          $response = true;
          $message = "El usuario se actualizo de manera correcta.";
        }else{
          $message = "Ocurrio un problema al actualizar un usuario.";
        }
      }
      $this->Mdl_log->vud_log_insert($data['user_register'],$message);
      echo json_encode(['response'=>$response, 'message'=>$message]);
    }

    public function  user_delete(){
      $data['user_register'] = $this->session->userdata('employee_number');
      $id_user =  $this->input->post('id_user');
      $response = false;
      $message = "";
      if(!empty($id_user)){
        $result = $this->Mdl_users->vud_delete_user($id_user);
        if($result){
          $response = true;
          $message = "El usuario se elimino de manera correcta";
        }
      }else{
        $message = "faltan parametros";
      }
      $this->Mdl_log->vud_log_insert($data['user_register'],$message);
      echo json_encode(['message'=>$message, 'response'=>$response]);
    }

    public function UserPermissions(){
      $id_user = $this->input->post('id_user'); 
      $result = $this->Mdl_users->vud_users_permisions($id_user);
      $count = 0;
      $response = false;
      $permissions = "";

      if($result){
        foreach ($result as $key => $value) {
          $permissions .='<tr>
          <td>'.$value['name'].'<input type="hidden" name="id_module[]" value="'.$value['id_module'].'"></td>';
         
          if($value['read_permission']){
              $permissions .='<td><div class="checkbox">
                    <label><input name=permission['.$value['id_module'].'][] type="checkbox" value="R" checked></label>
                </div></td>';
          }else{
              $permissions .='<td><div class="checkbox">
                    <label><input name=permission['.$value['id_module'].'][] type="checkbox" value="R"></label>
                </div></td>';
          }

          if($value['write_permission']){
              $permissions .='<td><div class="checkbox">
                    <label><input name=permission['.$value['id_module'].'][] type="checkbox" value="W" checked></label>
                </div></td>';
          }else{
              $permissions .='<td><div class="checkbox">
                    <label><input name=permission['.$value['id_module'].'][] type="checkbox" value="W"></label>
                </div></td>';
          }
            
          if($value['delete_permission']){
              $permissions .='<td><div class="checkbox">
                    <label><input name=permission['.$value['id_module'].'][] type="checkbox" value="D" checked></label>
                </div></td>';
          }else{
              $permissions .='<td><div class="checkbox">
                    <label><input name=permission['.$value['id_module'].'][] type="checkbox" value="D"></label>
                </div></td>';
          }

          if($value['update_permission']){
            $permissions .='<td><div class="checkbox">
                            <label><input name=permission['.$value['id_module'].'][] type="checkbox" value="U" checked></label>
                        </div></td>
                        </tr>';  
          }else{
            $permissions .='<td><div class="checkbox">
                            <label><input name=permission['.$value['id_module'].'][] type="checkbox" value="U"></label>
                        </div></td>
                        </tr>';  
          }
          $count++;
        }
        $permissions.= '<input type="hidden" name="id_user_permission" value="'.$id_user.'">';
        $response = true;
      }else{
        $permissions = "Ocurrio un problema al cargar los permisos del usuario";
      }
      echo json_encode(['response'=>$response, 'permissions'=>$permissions]);
    }

    public function UpdatePermission(){
      $id_module = $this->input->post('id_module');
      $data['user_register'] = $this->session->userdata('employee_number');
      $data['id_user_permission'] = $this->input->post('id_user_permission');
      $permission = $this->input->post('permission');
      $response = false;
      $result = $this->Mdl_users->vud_insert_user_permission($data,$permission);
        if($result){
          $response = true;
          $message = "Se actualizaron de manera correcta los permisos.";
        }else{
          $message = "Ups, algo salio mal con la actualizacion de los permisos.";
        }
        $this->Mdl_log->vud_log_insert($data['user_register'],$message);
        echo json_encode(['response' =>$response, 'message'=>$message]);
    }


}
