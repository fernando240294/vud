<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ctr_tracing extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_tracing');
        $this->load->model('Mdl_log');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('encryption');
        
        if (!$this->session->userdata('loginFlag')){
            $uri = ROOT_URL;
            redirect($uri, 'redirect');
        }
    }

    public function index()
    {
        $result = $this->Mdl_tracing->vud_transact_record();
        $data['transact'] = $this->Data_record_list($result,1);
        $result = $this->Mdl_tracing->vud_status_list();
        $data['status'] = $this->Data_record_list($result,2);
        $result = $this->Mdl_tracing->vud_area_record();
        $data['area'] = $this->Data_record_list($result,2);
        $result = $this->Mdl_tracing->vud_request_record();
        $data['table'] = $this->FormatRecordRequest($result);

        $this->load->view('include/back/header');
        $this->load->view('include/back/nav');
        $this->load->view('vw_tracing/vw_tracing',$data);
        $this->load->view('include/back/footer');
        $this->load->view('vw_tracing/include/script');
    }

    /**
    *Funcion para cargar las opciones de los select
    *
    **/
    public function Data_record_list($result,$type){
        $record = "";
        if($type == 1){
            foreach ($result as $key => $value) {
              $record .='<option value="'.$value["code"].'">'.$value["code"].', '.substr($value["description"],0, 100).'..'.'</option>';
            }
        }else if($type == 2){
            foreach ($result as $key => $value) {
              $record .='<option value="'.$value["id"].'">'.$value['description'].'</option>';
            }
        }
      return $record;                  
    }

    /**
    *Funcion para mostrar las modalidades de un tramite
    *
    **/
    public function Transact_info(){
      $code_transact = $this->input->post('code_transact');
      $result = $this->Mdl_tracing->vud_transaction_data($code_transact); 
      $dinamic = "";
      $status =false;
        if (!$result['transact_status']) {
            $dinamic .='<option value="0">--Seleccione una opción--</option>';
            foreach ($result as $key => $value){
                    if (!empty($value['code'])) {
                        $dinamic .='<option value="'.$value["code"].'">'.$value["code"].', '.$value["description"].'
                          </option>';
                    }
            }
            $status = true;
        }else{
            foreach ($result as $key => $value){
                    if(!empty($value['id'])){
                        $dinamic .= "<div class='form-check'>
                                  <label class='form-check-label'>
                                    <input type='checkbox' class='form-check-input check_requirement' name='update_requeriments[]' value='".$value['id']."'>".$value['description']."
                                  </label>
                                </div>";
                    }
            }
            $status = false;
        }
      echo json_encode(['status'=>$status , 'result'=>$dinamic]);
    }

    public function Modality_info(){
        $code_modality = $this->input->post('code_modality');
        $result = $this->Mdl_tracing->vud_modality_data($code_modality);
        $requirements = ""; 
        $status = false;
            if($result){
                    foreach ($result as $key => $value){
                      $requirements .= "<div class='form-check'>
                              <label class='form-check-label'>
                                <input type='checkbox' class='form-check-input check_requirement' name='update_requeriments[]' value='".$value['id']."'>".$value['description']."
                              </label>
                            </div>";
                    }
                $status = true;
            }
        echo json_encode(['requirements'=>$requirements, 'status'=>$status]);
    }

    public function vud_data_vulnerable_group(){
      $result = $this->Mdl_tracing->vud_data_vulnerable_group();
      if($result){
        echo json_encode(['result'=>$result]);
      }else{
        $result = "No se cuenta con grupos vulnerables";
        echo json_encode(['result'=>$result]);  
      }
    }

    /**
    *Funcion para maquetar todas las solicitudes
    *
    **/
    public function FormatRecordRequest($result){
        $request = "";
        $count = 1;
        if ($result > 0) {
            $request .= "<table class='table dataTable' id= 'dataTableTracing'>
                          <thead class='thead-light'>
                            <tr>
                              <th scope='col'>#</th>
                              <th scope='col'>Folio</th>
                              <th scope='col'>Trámite/Modalidad</th>
                              <th scope='col'>Estatus</th>
                              <th scope='col'>Fecha de ingreso</th>
                              <th scope='col'>Fecha compromiso</th>
                              <th scope='col'>Turnar</th>
                              <th scope='col'>Prevención</th>
                              <th scope='col'>Dictamen</th>
                              <th scope='col'>Finiquitar</th>
                              <th scope='col'>Ver solicitud</th>
                              <th scope='col'>Modificar</th>
                              <th scope='col'>Cancelar<br>/Reactivar</th>
                            </tr>
                          </thead>
                          <tbody id='id_record_body'>";

            foreach ($result as $key => $value){
                if(!empty($value['modality_desc'])){
                    $description = $value['modality_desc'];
                }else{
                    $description = $value['description'];
                }
                $request .= "<tr>
                  <th scope='row'>".$count."</th>
                  <td>".$value['folio']."</td>
                  <td>".$description."</td>
                  <td>".$value['description_status']."</td>
                  <td>".$value['date_creation']."</td>
                  <td>".$value['date_commitment']."</td>";

                        if(!empty($value['id_turn']) || $value['id_status']==3 || $value['id_status']==6){
                            $request .="<td><button type='button' class='btn btn-outline-secondary btn-lg fa fa-undo' data-toggle='modal' data-target='#request-turn' disabled></button></td>";
                        }else{
                            $request .="<td><button id='turn_modal' type='button' class='btn btn-outline-primary btn-lg fa fa-undo' data-toggle='modal' data-target='#request-turn' onclick='return trancit_create_turn(\"$value[folio]\",".$value['id'].",".$value['id_status'].")'></button></td>";
                        }

                        if($value['id_status']==2 || $value['id_status']==7 || ($value['id_status']==5&& $value['last_status'] == 2)){
                            $request .="<td><button type='button' class='btn btn-outline-success btn-lg fa fa-check-circle-o' data-toggle='modal' data-target='#tracing_code_49' onclick='return set_request_prevent(".$value['id'].")'></button></td>"; 
                        }else{
                            $request .="<td><button type='button' class='btn btn-outline-danger btn-lg fa fa-times' data-toggle='modal' data-target='#request-turn' disabled></button></td>";    
                        }

                        if(!empty($value['citizen_dictum']) || $value['id_status']==3 || $value['id_status']==6){        
                                $request .="<td><button type='button' class='btn btn-outline-secondary btn-lg fa fa-list-alt' data-toggle='modal' data-target='#request-dictum' disabled></button></td>";    
                    
                        }else{
                            $request .="<td><button  type='button' class='btn btn-outline-info btn-lg fa fa-list-alt' data-toggle='modal' data-target='#request-dictum' onclick='return trancit_create_dictum(".$value['id'].",\"$value[folio_dictum]\")'></button></td>";
                        }

                        if (!empty($value['settlement_number'])|| $value['id_status']==3 || $value['id_status']==6 )  {
                            $request .="<td><button type='button' class='btn btn-outline-secondary btn-lg fa fa-archive' disabled></button></td>";
                        }else{
                            $request .="<td><button type='button' class='btn btn-outline-warning btn-lg fa fa-archive' data-toggle='modal' data-target='#request_settlement' onclick='return Tracing_settlement_request(".$value['id'].",\"$value[folio]\")'></button></td>";
                        }
                            
                            $request.="<td><button type='button' class='btn btn-outline-dark btn-lg fa fa-eye' data-toggle='modal' data-target='#request_acuse' onclick='return tracing_view_request(".$value['id'].")'></button></td>";

                        if ($value['id_status']==6 || $value['id_status']==3) {
                            $request.="<td><button type='button' class='btn btn-outline-dark btn-lg fa fa-pencil-square-o' disabled></button></td>";
                        }else{
                            $request.="<td><button type='button' class='btn btn-outline-dark btn-lg fa fa-pencil-square-o' onclick='return tracing_edit_request(".$value['id'].",\"$value[folio]\", ".$value['id_address'].",".$value['id_interested'].")'></button></td>";   
                        }
                        
                        if ($value['id_status']!= 6){
                            if($value['id_status']!= 3){                            
                                $request .= "<td><button type='button' class='btn btn-outline-danger btn-lg fa      fa-times-circle' data-toggle='modal' data-target='#request_activation_cancelation'  onclick='return set_request_cancelation(".$value['id'].",3,".$value['id_status'].")'></button></td></tr>";
                            
                            }else{
                                $request .= "<td><button type='button' class='btn btn-outline-success btn-lg fa      fa-check-circle' onclick='return Alert_Activate_request(".$value['id'].",".$value['id_status'].",".$value['last_status'].")'></button></td></tr>";
                            }    
                        }else{
                            $request .= "<td><button type='button' class='btn btn-outline-danger btn-lg fa      fa-times-circle' data-toggle='modal' data-target='#request_activation_cancelation'  onclick='return set_request_cancelation(".$value['id'].",3,".$value['last_status'].")' disabled></button></td></tr>";
                        }
                $count++;
            }
            $request .= "</tbody>
                        </table>";
        }else{
            $request = "<br><div class='alert alert-danger text-center' role='alert'><strong>No se cuenta con ninguna solicitud registrada</strong></div>";
        }
        return $request;
    }

    public function AllTracingRecord(){
        $data = [];
        $response = false;
        $result = $this->Mdl_tracing->vud_request_record();
        if ($result) {
            $data['table'] = $this->FormatRecordRequest($result);
            $response = true;
        }else{

            $data['table'] = "<br><div class='alert alert-danger text-center' role='alert'><strong>No se cuenta con ninguna solicitud registrada</strong></div>";;

        }

            $log['user_register'] = $this->session->userdata('employee_number');
            $message = "El usuario ".$this->session->userdata('username')." realizo una busqueda general de solicitudes en el modulo, seguimiento.";
            $this->Mdl_log->vud_log_insert($log['user_register'],$message);

        echo json_encode(['response' =>$response,'table' => $data['table']]);
    }

    /**
    *Funcion para mostrar las solicitudes aplicando el filtro
    *
    **/
    public function Tracing_Filter_Data(){
        $count = 0;
        $data['request-folio'] = $this->input->post('request-folio');
        $data['transact_code'] = $this->input->post('transact_code');
        $data['modality_code'] = $this->input->post('modality_code');
        $data['request_status'] = $this->input->post('request_status');
        $data['request_type_person'] = $this->input->post('request_type_person');
        $data['request_date_begin'] = $this->input->post('request_date_begin');
        $data['request_date_end'] = $this->input->post('request_date_end');
        $response = true;
        $filter = "";
            if($data['request-folio']){
                $request_folio = explode(',', $data['request-folio']);
                for ($i=0; $i<sizeof($request_folio) ; $i++) { 
                    $request_folio[$i] = "'".$request_folio[$i]."'";
                }
                $data['request-folio'] = implode(',',$request_folio);
                $filter .="and a.folio in (".$data['request-folio'].")";
                $count++;
            }
            if($data['transact_code']){
                $filter .= "AND a.id_transact = ".$data['transact_code']." ";
                $count++;
            }
            if($data['modality_code']){
                $filter .= "AND a.id_modality = ".$data['modality_code']." ";
                $count++;
            }
            if($data['request_status']){
                $filter .= "AND a.id_status = ".$data['request_status']." ";
                $count++;
            }
            if($data['request_type_person']){
                $filter .= "AND g.type_person = ".$data['request_type_person']." ";
                $count++;
            }
            if(!empty($data['request_date_begin']) && !empty($data['request_date_end'])){
                $filter .= "AND a.date_creation BETWEEN '".$data['request_date_begin']."'
                            AND '".$data['request_date_end']."'";
                $count++;
            }

            if($count < 1){
                $filter = "Es necesario ingresar algun filtro";
                $response = false;
            }else{
                $result = $this->Mdl_tracing->vud_filter_record($filter);    
                $filter = $this->FormatRecordRequest($result);
            }

            $log['user_register'] = $this->session->userdata('employee_number');
            $message = "El usuario ".$this->session->userdata('username')." realizo una busqueda especifica en seguimiento";
            $this->Mdl_log->vud_log_insert($log['user_register'],$message);

        echo json_encode(['reponse'=>$response,'filter'=>$filter]);
    }

    public function Request_create_turn(){
        $data['request_turn_date'] = $this->input->post('request_turn_date');
        $data['request_turn_area'] = $this->input->post('request_turn_area');
        $data['request_turn_observations'] = $this->input->post('request_turn_observations');
        $data['id_request_turn'] = $this->input->post('id_request_turn');
        $data['id_status_turn'] = $this->input->post('id_status_turn');
        $response = false;
        $result = $this->Mdl_tracing->vud_create_turn_request($data);        
            if($result){
                $response = true;
            }
        echo json_encode(['response'=>$response]);
    }

    public function Request_create_dictum(){
        $response = false;
        $data['request_dictum_recibe_date'] = $this->input->post('request_dictum_recibe_date');
        $data['request_dictum_office'] = $this->input->post('request_dictum_office');
        $data['request_dictum_answer'] = $this->input->post('request_dictum_answer');
        $data['request_dictum_citizen'] = $this->input->post('request_dictum_citizen');
        if(empty($data['request_dictum_citizen'])){
            $data['request_dictum_citizen']  = 'NULL';
        }else{
            $data['request_dictum_citizen']  = "'".$data['request_dictum_citizen']."'";
        }
        $data['id_request_dictum'] = $this->input->post('id_request_dictum');

        if (empty($data['request_dictum_recibe_date'])&& empty($data['request_dictum_office'])&& empty($data['request_dictum_answer'])){
            $result = $this->Mdl_tracing->vud_update_citizen_dictum($data);
        }else{
            $result = $this->Mdl_tracing->vud_create_dictum_request($data);    
        }
            if($result){
                $response = true;
            }
        echo json_encode(['response'=>$response]);
    }

    public function Request_info(){
        $id_request = $this->input->post('id_request');
        $result = $this->Mdl_tracing->vud_view_data_request($id_request);
        foreach ($result as $key => $value){
            $array_request = ['id'=> $value['id'],
                            'folio'=> $value['folio'],
                            'id_user'=> $value['id_user'],
                            'user_name'=> $value['user_name'],
                            'means_request'=> $value['means_request'],
                            'prevent_date'=> $value['prevent_date'],
                            'citizen_prevent_date'=> $value['citizen_prevent_date'],
                            'correct_date'=> $value['correct_date'],
                            'observation'=> $value['observation'],
                            'request_turn'=> $value['request_turn'],
                            'type_work'=> $value['type_work'],
                            'name'=> $value['name'],
                            'last_name'=> $value['last_name'],
                            'phone'=> $value['phone'],
                            'delegation'=> $value['delegation'],
                            'colony'=> $value['colony'],
                            'postal_code'=> $value['postal_code'],
                            'street'=> $value['street'],
                            'ext_number'=> $value['ext_number'],
                            'int_number'=> $value['int_number'],
                            'id_status'=> $value['id_status'],
                            'description_status'=> $value['description_status'],
                            'date_creation'=> $value['date_creation'],
                            'date_commitment'=> $value['date_commitment'],
                            'code'=> $value['code'],
                            'description'=> $value['description'],
                            'modality_desc'=> $value['modality_desc'],
                            'id_turn'=> $value['id_turn'],
                            'id_dictum'=> $value['id_dictum'],
                            'folio_dictum'=> $value['folio_dictum'],
                            'date_receives_dictum'=> $value['date_receives_dictum'],
                            'answer_dictum'=> $value['answer_dictum'],
                            'citizen_dictum'=> $value['citizen_dictum']];
        }
        echo json_encode(['request' =>$array_request]);
    }

    public function Prevencion_request(){
        $data['id_request'] = $this->input->post('id_request');
        $data['id_prevent'] = $this->input->post('id_prevent');
        $response= false;
                if ($data['id_prevent'] == 1){
                    $data['request_prevent_date'] = $this->input->post('request_prevent_date');
                    if(empty($this->input->post('prevent_citizen_date'))){
                        $data['prevent_citizen_date'] = 'NULL';
                    }else{
                        $data['prevent_citizen_date'] = "'".$this->input->post('prevent_citizen_date')."'";
                    }
                }else{
                    $data['request_prevent_date'] = $this->input->post('request_prevent_date');
                }
        $result = $this->Mdl_tracing->vud_prevent_request($data);
        if($result){
            $response = true;
        }
        echo json_encode(['response' => $response]);
    }

    //funcion para cancelar la solicitud y guardar el ultimo estado en el que se quedo.
    public function Request_Cancelation(){
        $data['id_request'] = $this->input->post('id_request');
        $data['status_request'] = $this->input->post('status_request');
        $data['last_status'] = $this->input->post('last_status');
        $data['request_cancelation_desc'] = $this->input->post('request_cancelation_desc');
        $response= false;
        $result = $this->Mdl_tracing->vud_update_status_request($data);
        if($result){
            $response = true;
            $message = "Se cancelo la solicitud de manera correcta";
        }
        echo json_encode(['response' => $response]);
    }

    //funcion para activar la funcion y volver a asignar el estatus con el que se cancelo.
    public function Activate_request(){
        $data['id_request'] = $this->input->post('id_request');
        $data['status_request'] = $this->input->post('status_request');
        $data['last_status'] = $this->input->post('last_status');
        $response= false;
        
        $result = $this->Mdl_tracing->vud_activate_request($data);
        if($result){
            $response = true;
            $message = "Se reactivo la solicitud de manera correcta";
        }
        echo json_encode(['response' => $response]);
    }

    public function Create_settlement(){
        $data['user_register'] = $this->session->userdata('employee_number');
        $data['id_request_settlement']= $this->input->post('id_request_settlement');
        $data['request_settlement_date']= $this->input->post('request_settlement_date');
        $data['request_settlement_number']= $this->input->post('request_settlement_number');
        $response= false;
        $message ="";
        $result = $this->Mdl_tracing->vud_create_settlement($data);
            if ($result) {
                $response = true;
                $message = "Se ingreso finiquito de manera correcta correcta la solicitud ".$data['id_request_settlement'];
            }else{
                $message = "Se intento finiquitar la solicitud ".$data['id_request_settlement'];
            }
            $this->Mdl_log->vud_log_insert($data['user_register'],$message);
            echo json_encode(['response' => $response]);
    }


    public function Request_info_update(){
        $data['id_request'] = $this->input->post('id_request');
        $result = $this->Mdl_tracing->vud_update_request_info($data);
            foreach ($result as $key => $value){
                $request = ['id'=> $value['id'],
                            'observations'=> $value['observations'],
                            'id_status'=> $value['id_status'],
                            'id_transact'=> $value['id_transact'],
                            'id_vulnerable_group'=> $value['id_vulnerable_group'],
                            'means_request'=> $value['means_request'],
                            'delivery'=> $value['delivery'],
                            'id_modality'=> $value['id_modality'],
                            'request_turn'=> $value['request_turn'],
                            'type_work'=> $value['type_work'],
                            'postal_code'=> $value['postal_code'],
                            'colony'=> $value['colony'],
                            'ext_number'=> $value['ext_number'],
                            'int_number'=> $value['int_number'],
                            'street'=> $value['street'],
                            'delegation'=> $value['delegation'],
                            'name'=> $value['name'],
                            'last_name'=> $value['last_name'],
                            'e_mail'=> $value['e_mail'],
                            'phone'=> $value['phone'],
                            'type_person'=> $value['type_person'],
                            'description_modality'=> $value['description_modality']];

                            $id_request = $value['id']; 
                            $transact = $value['id_transact'];
                            $modality = $value['id_modality'];
            }

            if($id_request){
                $request_requeriment = $this->Mdl_tracing->vud_record_request_requirement($id_request);
                if(!empty($modality)){
                    $code = $modality;
                    $requirements = $this->Mdl_tracing->vud_get_request_requirement($code,2);
                }else{
                    $code = $transact;
                    $requirements = $this->Mdl_tracing->vud_get_request_requirement($code,1);
                }
                $f = 0;
                $select = ""; 
                foreach ($requirements as $key => $value){
                    if(!empty($request_requeriment[$f]['id']) && $value['id'] == $request_requeriment[$f]['id']){
                        $select .= "<div class='form-check'>
                          <label class='form-check-label'>
                            <input type='checkbox' class='form-check-input check_requirement' name='update_requeriments[]' value='".$value['id']."' checked>".$value['description']."
                          </label>
                        </div>";
                        $f++;
                    }else{
                        $select .= "<div class='form-check'>
                          <label class='form-check-label'>
                            <input type='checkbox' class='form-check-input check_requirement' name='update_requeriments[]' value='".$value['id']."'>".$value['description']."
                          </label>
                        </div>";
                    }
                }
            }
        echo json_encode(['request' =>$request, 'select' => $select]);
    }

    public function Update_request_data(){
        //informacion de la direccion
        $address['update_id_address'] = $this->input->post('update_id_address');
        $address['update_codigoPostal'] = $this->input->post('update_codigoPostal');
        $address['colonia'] = $this->input->post('colonia');
        $address['update_ext_number'] = $this->input->post('update_ext_number');
        $address['update_int_number'] = $this->input->post('update_int_number');
        $address['update_street'] = $this->input->post('update_street');
        $address['delegacion'] = $this->input->post('delegacion');

        //informacion del interesado
        $interested['update_id_interested'] = $this->input->post('update_id_interested');
        $interested['name_update'] = $this->input->post('name_update');
        $interested['name_update'] = addslashes($interested['name_update']);
        $interested['update_laste_name'] = $this->input->post('update_laste_name');
        $interested['update_laste_name'] = addslashes($interested['update_laste_name']);
        $interested['update_e_mail'] = $this->input->post('update_e_mail');
        $interested['update_phone'] = $this->input->post('update_phone');
        $interested['tipo_de_persona'] = $this->input->post('tipo_de_persona');

        //informacion de la solicitud
        $request['update_id_request'] = $this->input->post('update_id_request');
        $request['observations_update'] = $this->input->post('observations_update');
        $request['update_status'] = $this->input->post('update_status');
        $request['transact_code_edit'] = $this->input->post('transact_code_edit');
        $request['edit_group_vulnerable'] = $this->input->post('edit_group_vulnerable');
        $request['update_type_work'] = $this->input->post('update_type_work');
        $request['update_turn'] = $this->input->post('update_turn');
        $request['medio_por_el_cual_solicita'] = $this->input->post('medio_por_el_cual_solicita');
        $request['tipo_de_documento'] = $this->input->post('tipo_de_documento');
        if (empty($request['tipo_de_documento'])) {
            $request['tipo_de_documento'] = 'NULL';
        }
        
        if(empty($this->input->post('modality_code_edit'))){
            $request['modality_code_edit'] = 'NULL';
        }else{
            $request['modality_code_edit'] = $this->input->post('modality_code_edit');
        }
        $response = false;
        $message = "";

        //requisitos
        $requeriments = $this->input->post('update_requeriments');
            $result = $this->Mdl_tracing->vud_update_request($request,$interested,$address,$requeriments);
            if ($result){
                $response = true;
                $message = "La información se actualizo de manera correcta";
            }else{
                $message = "Ocurrio un problema al actualizar la información, Favor de llenar los campos de manera correcta";
            }
        echo json_encode(['response'=>$response, 'message'=>$message]);
    } 

}

