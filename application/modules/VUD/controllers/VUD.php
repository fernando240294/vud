<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class VUD extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_vud');
        $this->load->model('Mdl_log');
        $this->load->library('session');
        $this->load->helper('url');

    }

    public function index()
    {
        if ($this->session->userdata('loginFlag')) {
            $uri = ROOT_URL. 'dashboard';
            redirect($uri, 'redirect');
        } else {
            
            $this->load->view('vw_vud/vw_login');
            $this->load->view('vw_vud/include/script');
            $this->load->view('include/back/footer');

        }

    }
    
    /**
     * Esta funcion se encarga de validar que el usuario y contraseña sean correctos
     * Esta funcion es invocada desde un ajax en el login en caso que no coincidan
     * los datos del usuario regresa un valor BOOL FALSE
     */
    public function userAuth()
    {
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));
        $flag = FALSE;
        $message = "El usuario ".$username." intengo ingresar al sistema.";
        $dataUser = ['username' => $username, 'password' => $password];
        $result = $this->Mdl_vud->userAuth($dataUser);
        if (!empty($result)) {
            $message = "El usuario ".$username." ingreso al sistema de manera correcta.";
            $id_user = $result[0]['id'];
            $num_empleado = $result[0]['employee_number'];
            $username = $result[0]['user_name'];
            //crea un arreglo con la informacion obtenida de la base de datos.
            $authUser = [
                'loginFlag' => TRUE,
                'user_id' => $id_user,
                'employee_number' => $num_empleado,
                'username' => $username
            ];
            $this->session->set_userdata($authUser);
            $flag = TRUE;
        }
        if($flag){
            $this->Mdl_log->vud_log_insert($num_empleado,$message);    
        }else{
            $this->Mdl_log->vud_log_insert(0,$message);    
        }

        echo json_encode(['response' => $flag]);
    }

    /**
     * Esta funcion cierra la session y redirecciona
     */
    public function closeSession()
    {
        $user_register = $this->session->userdata('employee_number');
        $message = "El usuario cerro sesion";
        $this->Mdl_log->vud_log_insert($user_register,$message);
        $this->session->unset_userdata('loginFlag');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('employee_number');
        $this->session->unset_userdata('username');
        redirect('VUD', 'refresh');

    }

}
