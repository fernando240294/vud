<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ctr_requirement extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_requirement');
        $this->load->model('Mdl_log');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('encryption');

        if (!$this->session->userdata('loginFlag')){
            $uri = ROOT_URL;
            redirect($uri, 'redirect');
        }
    }

    public function index(){
      $result = $this->Mdl_requirement->vud_record_requirement();
      $data['requirement'] = $this->FormatRequirementRecord($result);
      $this->load->view('include/back/header');
      $this->load->view('include/back/nav');
      $this->load->view('include/back/sub_nav');
      $this->load->view('vw_requirement/vw_requirement',$data);
      $this->load->view('include/back/footer');
      $this->load->view('vw_requirement/include/script');
    }

    public function FormatRequirementRecord($result){
      if($result){
      $count = 1;
      $requirement = "";
      $requirement .='<div class="card mb-3">
                      <div class="card-header">
                        <i class="fa fa-table"></i> Requisitos<button type="button" style="float: right; 50px; height: 40px;" class="btn btn-outline-success fa fa-plus-circle fa-lg" data-toggle="modal" data-target="#add_transact"></button></div>
                      <div class="table-responsive">
                    <table class="table table-bordered dataTable" id="" width="100%" cellspacing="0">
                    <thead>
                       <tr>
                      <th>ID</th>
                      <th>Descripción</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>';

        foreach ($result as $key => $value) {
          $requirement .= "<tr>
                          <td>".$count."</td>
                          <td>".$value['description']."</td>
                          <td> <button type='button' class='btn btn-outline-warning fa fa-pencil-square-o fa-lg' style='50px; height: 40px;' data-toggle='modal' data-target='#vud_update_requirement' onclick='return requirement_update_data(".$value['id'].",\"$value[description]\");'></button>
                          <button type='button' style='50px; height: 40px;' class='btn btn-outline-danger fa fa-trash-o fa-lg' onclick='return delete_requirement(".$value['id'].");'></button>";
          $count++;
        }
        $requirement .='</tr>
                      </tbody>
                    </table>
                    </div>
                    </div>
                    <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
                  </div>';
  
      }else{
        $requirement = "<div class='alert alert-danger'>
                    <strong>No se cuenta con ningun registro de requisitos.</strong>
                  </div>";
      }
      return $requirement;
    }

    public function CreateRequirement(){
      $data['user_register'] = $this->session->userdata('employee_number');
      $requirement = $this->input->post('create_requirement');
      $response = false;
      $message = "";
      $result = $this->Mdl_requirement->vud_create_requirement($requirement);
        if ($result){
          $response = true;
          $message = "Se guardaron de manera correcta los nuevos requisitos.";
        }else{
          $message = "Ocurrio un problema al momento de guardar los datos.";
        }
      $this->Mdl_log->vud_log_insert($data['user_register'],$message);
      echo json_encode(['response'=>$response, 'message' => $message]);
    }

    public function UpdateRequirement(){
      $data['user_register'] = $this->session->userdata('employee_number');
      $data['update_requirement_description'] = $this->input->post('update_requirement_description');
      $data['id_update_requirement'] = $this->input->post('id_update_requirement');
      $response = false;
      $message = "";
      $result = $this->Mdl_requirement->vud_update_requirement($data);
        if($result){
          $response = true;
          $message = "Se actualizo de manera correcta el requisito.";
        }else{
          $message = "Ocurrio un problema al insertar el registro.";
        }
      $this->Mdl_log->vud_log_insert($data['user_register'],$message);
      echo json_encode(['response'=> $response, 'message' => $message]);
    }

    public function DeleteRequirement(){
      $data['user_register'] = $this->session->userdata('employee_number');
      $id_requirement = $this->input->post('id_requirement');
      $response = false;
      $message = "";
      $result = $this->Mdl_requirement->vud_delete_requirement($id_requirement);
      if($result){
        $response = true;
        $message = "El registro se elimino de manera correcta.";
      }else{
        $message = "Ocurrio un problema al eliminar el registro.";
      }
      $this->Mdl_log->vud_log_insert($data['user_register'],$message);
      echo json_encode(['response' => $response, 'message' => $message]);
    }

}
