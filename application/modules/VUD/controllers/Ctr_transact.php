<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ctr_transact extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_transact');
        $this->load->model('Mdl_log');
        $this->load->library('session');
        $this->load->helper('url');

        if (!$this->session->userdata('loginFlag')){
            $uri = ROOT_URL;
            redirect($uri, 'redirect');
        }
    }

    public function index(){
      $result = $this->Mdl_transact->vud_record_transact();
      $data['transact']= $this->FormatTransactRecord($result);
      $result = $this->Mdl_transact->vud_record_requirement();
      $data['select_requirement'] = $this->FormatRecordSelect($result,2);
      
      $this->load->view('include/back/header');
      $this->load->view('include/back/nav');
      $this->load->view('include/back/sub_nav');
      $this->load->view('vw_transact/vw_transact',$data);
      $this->load->view('include/back/footer');
      $this->load->view('vw_transact/include/script');
    }

    public function FormatTransactRecord($result){
      if($result){
      $count = 1;
      $transact = "";
      $transact .='<div class="card mb-3">
                    <div class="card-header">
                    <i class="fa fa-table"></i> Trámites<button type="button" style="float: right; width: 50px; height: 40px;" class="btn btn-outline-success fa fa-plus-circle fa-lg" data-toggle="modal" data-target="#add_transact"></button></div>
                      <div class="table-responsive">
                    <table class="table table-bordered dataTable" id="" width="100%" cellspacing="0">
                    <thead>
                       <tr>
                      <th>ID</th>
                      <th>Clave</th>
                      <th>Descripción</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>';

        foreach ($result as $key => $value) {
          //Se eliminan los saltos de linea de las modalidades
          $value['description'] = preg_replace("/[\r\n|\n|\r]+/", " ", $value['description']);
          $value['legal_foundation'] = preg_replace("/[\r\n|\n|\r]+/", " ", $value['legal_foundation']);
          $transact .= "<tr>
                          <td>".$count."</td>
                          <td>".$value['code']."</td>
                          <td>".$value['description']."</td>
                          <td><button type='button' class='btn btn-outline-warning fa fa-pencil-square-o fa-lg' style='50px; height: 40px;' data-toggle='modal' data-target='#vud_update_transact' onclick='return transact_update_data(".$value['id'].",".$value['code'].",\"$value[description]\",\"$value[legal_foundation]\",".$value['total_modality'].",".$value['days'].",\"$value[type_days]\");'></button>
                          <button type='button' style='50px; height: 40px;' class='btn btn-outline-danger fa fa-trash-o' onclick='return delete_transact(".$value['id'].");'></button>";

                          /*if($value['total_modality']>0){
                            $transact .=" <button type='button' class='btn btn-primary' onclick='return formUser_delete(".$value['id'].");'>Modalidades</button></td>";
                          }else{
                            $transact .="<button type='button' class='btn btn-success' onclick='return formUser_delete(".$value['id'].");'>Requisitos</button></td>";
                          }*/
          $count++;
        }
        $transact .='</tr>
                      </tbody>
                    </table>
                    </div>
                    </div>
                  </div>';
  
      }else{
        $transact = "<div class='alert alert-danger'>
                    <strong>No se cuenta con ningun registro de tramites.</strong>
                  </div>";
      }
      return $transact;
    }

    public function CreateTransact(){
      $data['user_register'] = $this->session->userdata('employee_number');
      $data['transact_code'] = $this->input->post('transact_code');
      $data['transact_description'] = $this->input->post('transact_description');
      $data['transact_foundament'] = $this->input->post('transact_foundament');
      $data['transact_days'] = $this->input->post('transact_days');
      $data['type_days_transact'] = $this->input->post('type_days_transact');
      $requirements = $this->input->post('modality_requirements');
      $response = false;
      $message = "";      
      if(!empty($data)){
        $result = $this->Mdl_transact->vud_insert_transact($data,$requirements);
        if($result){
          $response = true;
          $message = "El tramite se guardo de manera correcta";
        }else{
          $message = "Sucedio un error al ingresar un usuario";
        }
      }else{
        $message = "Sucedio un error al ingresar un usuario";
      }
      $this->Mdl_log->vud_log_insert($data['user_register'],$message);
      echo json_encode(['response'=>$response, 'message'=>$message]);
    }

    public function UpdateTransact(){
      $data['user_register'] = $this->session->userdata('employee_number');
      $data['id_update_transact'] = $this->input->post('id_update_transact');
      $data['code_update_transact'] = $this->input->post('code_update_transact');
      $data['update_transact_description'] = $this->input->post('update_transact_description');
      $data['update_transact_foundament'] = $this->input->post('update_transact_foundament');
      $data['update_transact_days'] = $this->input->post('update_transact_days');
      $data['update_type_days_transact'] = $this->input->post('update_type_days_transact');
      $requeriments = $this->input->post('modality_requirements');
      $response  = false;
      $message = "";
      if(!empty($data)){
        $result = $this->Mdl_transact->vud_update_transact($data,$requeriments);
        if($result){
          $response = true;
          $message = "Se realizo de manera corrrecta la actualización";
        }else{
          $message = "Ocurrio un problema al actualizar la información";
        }
      }else{
        $message = "Ocurrio un problema al actualizar la información";
      }
      $this->Mdl_log->vud_log_insert($data['user_register'],$message);
      echo json_encode(['response'=>$response, 'message' => $message]);
    }

    public function deleteTransact(){
      $data['user_register'] = $this->session->userdata('employee_number');
      $id_transact = $this->input->post('id_transact');
      $response = false;
      $message = "";

      if(!empty($data)){
        $result = $this->Mdl_transact->vud_delete_transact($id_transact);
        if($result){
          $response = true;
          $message = "Se elimino de manera correcta el tramite.";
        }else{
          $message = "Error! ocurrio un problema al eliminar.";
        }
      }else{
        $message = "Ocurrio un problema al actualizar la información";
      }
      $this->Mdl_log->vud_log_insert($data['user_register'],$message);
      echo json_encode(['response'=> $response , 'message' => $message]);
    }

    public function Format_update_requirements(){
      $_code_transact = $this->input->post('_code_transact');
      $marked_requirement = $this->Mdl_transact->vud_get_transact_requirements($_code_transact);
      $requirements = $this->Mdl_transact->vud_record_requirement();
      $f=0;
      $cheked ="";
        foreach ($requirements as $key => $value){
          if((!empty($marked_requirement[$f]['id'])) && ($value['id'] == $marked_requirement[$f]['id'])){
            $cheked .= '<div class="checkbox">
                          <label><input name="modality_requirements[]" type="checkbox" value="'.$value['id'].'" checked>'.$value['description'].'</label>
                          </div>';
                          $f++;
          }else{
            $cheked .= '<div class="checkbox">
                          <label><input name="modality_requirements[]" type="checkbox" value="'.$value['id'].'">'.$value['description'].'</label>
                          </div>';
          }
        }
        echo json_encode(['cheked' =>$cheked]);
    }

    public function FormatRecordSelect($result,$number){
      $select = "";
      if(!empty($result)){
        if($number == 1 && count($result)>0){
          foreach ($result as $key => $value) {
            $select .= '<option value="'.$value['id'].'">'.$value['code'].', '.$value['description'].'</option>';
          }
        }elseif ($number == 2 && count($result) > 0){
          foreach ($result as $key => $value) {
            $select .= '<div class="checkbox">
                        <label><input name="modality_requirements[]" type="checkbox" value="'.$value['id'].'">'.$value['description'].'</label>
                        </div>';
          }
        } 
      }
      return $select;
    }

}
