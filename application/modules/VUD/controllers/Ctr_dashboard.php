<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ctr_dashboard extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_dashboard');
        $this->load->library('session');
        $this->load->helper('url');

        date_default_timezone_set('America/Mexico_City');
        if (!$this->session->userdata('loginFlag')){
            $uri = ROOT_URL;
            redirect($uri, 'redirect');
        }
    }

    public function index()
    {
        $this->load->view('include/back/header');
        $this->load->view('include/back/nav');
        $this->load->view('vw_dashboard/vw_index');
        $this->load->view('include/back/footer');
        $this->load->view('vw_dashboard/include/script');
    }

    /**
    **Funcion que consulta el total de solciitudes de acuerdo a su estatus, para hacer la grafica de dona,
    **Se manda un arreglo con la informacion necesaria para la grafica
    **/
    public function Estatus_chats_request(){
        //año en curso del que se hara la grafica
        $year = date('Y');
        $status= [];
        $result = $this->Mdl_dashboard->vud_chart_status($year);
        //arreglo con el total de solicitudes de acuerdo a estatus y su descripcion
        foreach ($result as $key => $value) {
            $data = ['id' => $value['id'],
                    'description' => $value['description'],
                    'total_stado' => $value['total_stado']];
             
            array_push($status, $data);
        }
        print_r(json_encode($status));
        
    }


    /**
    **Funcion para crear la grafica de linea del año en curso
    **/
    public function Request_chart_line(){
        //se toma el año en curso
        $year = date('Y');
        $result = $this->Mdl_dashboard->vud_total_request_month($year);
            //arreglo con los totales de las solicitudes de acuerdo al año en curso, ordenados por mes.
            foreach ($result as $key => $value){
                $total = ['enero' => $value['enero'],
                        'febrero' => $value['febrero'],
                        'marzo' => $value['marzo'],
                        'abril' => $value['abril'],
                        'mayo' => $value['mayo'],
                        'junio' => $value['junio'],
                        'julio' => $value['julio'],
                        'agosto' => $value['agosto'],
                        'septiembre' => $value['septiembre'],
                        'octubre' => $value['octubre'],
                        'noviembre' => $value['noviembre'],
                        'diciembre' => $value['diciembre']];
            }
        print_r(json_encode($total));
    }




    

}
