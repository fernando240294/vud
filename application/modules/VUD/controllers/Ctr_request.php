<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ctr_request extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_request');
        $this->load->model('Mdl_log');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('encryption');

        date_default_timezone_set('America/Mexico_City');
        if (!$this->session->userdata('loginFlag')){
            $uri = ROOT_URL;
            redirect($uri, 'redirect');
        }
    }

    public function index(){
      $this->load->view('include/back/header');
      $this->load->view('include/back/nav');
      $this->load->view('include/back/sub_nav_request');
      $this->load->view('vw_request/vw_index');
      $this->load->view('include/back/footer');
      $this->load->view('vw_request/include/script');
    }
    /**
     * Return All Request 
     * 
     * Regresa todos los trámites de la base de datos con un status
     */
    public function returnAllRequest(){
      $result = $this->Mdl_request->vud_transact_record(); 
      echo json_encode($result);     
    }

    /**
    *Funcion para consultar las modalidades o los requisitos de los tramites. usando como llave primaria Clave de tramite
    **/
    public function Transact_info(){
      $code_transact = $this->input->post('code_transact');
      $result = $this->Mdl_request->vud_transaction_data($code_transact);             
      $datos = [];
      $datos['data'] = []; 
      $datos['transact_status'] = false;
        foreach($result as $key => $value ){
          if($key === 'transact_status'){
            $datos['transact_status'] = $value;
          }else{
            array_push($datos['data'], $value);
          }
        }
      echo json_encode($datos);
    }

    /**
    *Funcion para consultar los requisitos de la modalidad seleccionada, tomando como llave la clave de la 
    *modalidad
    **/
    public function Modality_info(){
      $code_modality = $this->input->post('code_modality');
      $result = $this->Mdl_request->vud_modality_data($code_modality);
      if ($result) {
        echo json_encode(['result'=>$result]);  
      }else{
        $result = "no se encontraron datos";
        echo json_encode(['result'=>$result]);  
      }
    }

    public function vud_data_vulnerable_group(){
      $result = $this->Mdl_request->vud_data_vulnerable_group();
      if($result){
        echo json_encode(['result'=>$result]);
      }else{
        $result = "No se cuenta con grupos vulnerables";
        echo json_encode(['result'=>$result]);  
      }
    }

    public function RequestDataSave(){
      $address['calle'] = $this->input->post('calle');
      $address['delegacion'] = $this->input->post('delegacion');
      $address['codigoPostal'] = $this->input->post('codigoPostal');
      $address['colonia'] = $this->input->post('colonia');
      $address['noExterior'] = $this->input->post('noExterior');
      $address['noInterior'] = $this->input->post('noInterior');
      $interested['nombre'] = $this->input->post('nombre');
      $interested['nombre'] = addslashes($interested['nombre']);
      $interested['lastname'] = $this->input->post('apellidoPaterno').' '.$this->input->post('apellidoMaterno');
      $interested['lastname'] = addslashes($interested['lastname']);
      $interested['email'] = $this->input->post('email');
      $interested['telefono'] = $this->input->post('telefono');
      $interested['tipoDePersona'] = $this->input->post('tipoDePersona');
      $request['user_register'] = $this->session->userdata('employee_number');
      $request['observaciones'] = $this->input->post('observaciones');
      $request['tramite'] = $this->input->post('tramite');
      $request['grupoVulnerable'] = $this->input->post('grupoVulnerable');
      $request['giro'] = $this->input->post('giro');
      $request['tipoDeObra'] = $this->input->post('tipoDeObra');  
      $request['medioDeSolicitud'] = $this->input->post('medioDeSolicitud');      
      $request['cartaPoderFormato'] = $this->input->post('cartaPoderFormato');
        if (empty($request['cartaPoderFormato'])){
          $request['cartaPoderFormato'] = 'NULL';
        }
      $request['id_status'] = $this->input->post('status');
      $request['modalidad'] = $this->input->post('modalidad');
      $requisitos = $this->input->post('requisitos');
      $folio = $this->Mdl_request->vud_get_last_folio();
      $folio = $folio['0']['vud_get_last_folio'];
      $request['folio'] = $this->CalculateFolio($folio);
        if (!empty($request['modalidad'])) {
          $days = $this->Mdl_request->vud_modality_days($request);
          $request['FechaComprpomiso'] = $this->CalculateDateCommitment($days);
        }else{
          //consulta y calculo de fecha compromiso
          $days = $this->Mdl_request->vud_transact_days($request);
          $request['FechaComprpomiso'] = $this->CalculateDateCommitment($days);
          $request['modalidad'] = 'NULL';
        }
      $response = false;
      $message = "";
      $result = $this->Mdl_request->vud_request_data_save($address,$interested,$request,$requisitos);
        if($result){
          $response = true;
          $message = "La informacion se guardo de manera correcta.";
          $folio = $result[0]['vud_request_data'];
            //se guarda el usuario que creo la solicitud y una descripcion
            $id_user = $this->session->userdata('employee_number');
            $message_audit = "El usuario ".$this->session->userdata('username')." genero la solicitud con folio ".$folio;
            $this->Mdl_log->vud_request_audit_insert($id_user,$message_audit,$folio);
        }else{
          $message = "Ocurrio un proeblema al guardar la informacion, Favor de ver que los campos obligatorios (*) esten llenos.";
        }
      echo json_encode(['response' => $response , 'message'=>$message, 'folio'=>$folio, 'fechaCompromiso' =>$request['FechaComprpomiso']]);
    }

    /**
    **Funcion para calcular la fecha compromiso.
    **/
    public function CalculateDateCommitment($days){
      $Segundos = 0;
      date_default_timezone_set('America/Mexico_City');
      $fechaInicial = date("Y-m-d");
      $MaxDias = $days[0]['days'];
        if($days[0]['type_days'] == 'H'){
           for ($i=0; $i<$MaxDias; $i++){  
              $Segundos = $Segundos + 86400;  
              $caduca = date("D",time()+$Segundos);   
                if ($caduca == "Sat"){  
                    $i--;  
                }  
                else if ($caduca == "Sun"){  
                    $i--;  
                }  
                else{  
                  //Si no es sabado o domingo, y el for termina y nos muestra la nueva fecha  
                  $FechaFinal = date("d-m-Y",time()+$Segundos);  
                }  
            }  
        }else if($days[0]['type_days'] == 'N'){
            $Segundos = $MaxDias * 86400;
            $FechaFinal = date("d-m-Y",time()+$Segundos);  
        }else{
          $FechaFinal = $fechaInicial;
        }
          return $FechaFinal;
    }

    /**
    **Funcion para crear el folio.
    **/
    public function CalculateFolio($folio){
      $year = date('Y');
      if($folio > 0){
        $year = date('Y');
        $folio = explode("/",$folio);
          if($year == $folio['1']){
            $folio[0]++;
            $new_folio = $folio[0]."/".$folio['1']; 
          }else{
             $new_folio = "1/".$year;
          }
      }else{
        $new_folio = "1/".$year;
      }
      return $new_folio;
    }









}
