<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Clase para el controlador del modulo de Contacto
 * @author Ricardo Daniel Carrada Peña <rdcarrada@gmail.com>
 */
class Sendemail extends CI_Controller
{
    private $CI;

    function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library("session");
        $this->CI->load->library('Sendmessage');
        $this->view_siap = "mail/email_user_apikey";


    }

    /**
     * Funcion para mandar correo de los candidatos
     * @param array $params
     * @return bool
     */
    public function mail_siap($params = array())
    {
        $response = FALSE;

        if (!empty($params)) {
            $params['view'] = $this->view_siap;
            if ($this->CI->sendmessage->sendEmail($params)) {
                $response = TRUE;
            }
        }

        return $response;
    }


}

