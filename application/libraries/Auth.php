<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author Ricardo Daniel Carrada Peña <rdcarrada@gmail.com>
 */
class Auth
{

    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->helper('Jwt');
        $this->CI->config->load('jwt');
        $this->CI->load->helper('Auth_token');
    }

    /**
     * Funcion para validar que el token enviado en el Header es funcional
     */
    public function validateToken_post()
    {
        $headers = $this->CI->input->request_headers();
        $return = FALSE;
        $userId = 0;

        if(isset($headers['Authorization'])){
            $headers = str_replace("Bearer ", "", $headers['Authorization']);

            $decodedToken = AUTH_TOKEN::validateTimestamp($headers, $this->CI->config->item('jwt_key'));

            if ($decodedToken != FALSE) {

                $userId = $decodedToken->id;
                $return = TRUE;

            }
        }

        return [$return, $userId];

    }
}