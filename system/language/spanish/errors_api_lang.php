<?php

/*
 * Spanish language ERRORS
 */
/*ERRORES GENERAL*/
$lang['error_1'] = 'General (Desconocido) error.';
$lang['error_2'] = 'Faltan parámetros requeridos: %s.';
$lang['error_3'] = 'Error de base de datos.';
$lang['error_4'] = 'Protocolo no soportado.';

/*ERRORES API*/
$lang['error_101'] = 'Esta clave de API no existe en nuestros registros.';
$lang['error_102'] = 'Este token tiene un error o ha expirado, es necesario volver a generar un token.';
$lang['error_103'] = 'Las credenciales recibidas son invalidas.';
$lang['error_104'] = 'IP denegada.';
$lang['error_105'] = 'Acceso no autorizado.';
$lang['error_106'] = 'Esta clave de API no tiene acceso al controlador solicitado.';
$lang['error_107'] = 'Esta clave de API no tiene suficientes permisos.';
$lang['error_108'] = 'Esta clave de API ha alcanzado el límite de tiempo para este método.';
$lang['error_109'] = 'Método HTTP no está disponible.';
$lang['error_110'] = 'Esta dirección IP ha alcanzado el límite de tiempo para este método.';
$lang['error_111'] = 'Error de Token de acceso no válido.';
$lang['error_112'] = 'El valor typeVariable: %s, no es válido.';
