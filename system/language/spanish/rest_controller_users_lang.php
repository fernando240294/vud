<?php

/*
 * Spanish language USER
 */

$lang['user_params_empty'] = 'Faltan parametros: %s';
$lang['apiKey_expired_error'] = 'El token ha expirado, por favor solicita un nuevo token.';
