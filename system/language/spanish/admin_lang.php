<?php

/*
 * Spanish language Auth
 */
$lang['error_access_denegate'] = 'No cuenta con los permisos necesarios para acceder a este módulo';
$lang['login_title'] = 'Datos Abiertos';
$lang['login_placeholder_username'] = 'Correo';
$lang['login_placeholder_password'] = 'Contraseña';
$lang['login_placeholder_button'] = 'Iniciar Sesión';
$lang['login_message_error'] = '<strong>Error!</strong> El usuario o contraseña no coinciden con nuestros registros.';

