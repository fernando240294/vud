PGDMP             
            v           prueba    10.4    10.4 �    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    16393    prueba    DATABASE     �   CREATE DATABASE prueba WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Mexico.1252' LC_CTYPE = 'Spanish_Mexico.1252';
    DROP DATABASE prueba;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16402    users    TABLE     {  CREATE TABLE public.users (
    id integer NOT NULL,
    user_name character varying(255) NOT NULL,
    employee_number integer NOT NULL,
    description character varying(255) NOT NULL,
    rol integer NOT NULL,
    user_register integer NOT NULL,
    date_register timestamp with time zone NOT NULL,
    status integer NOT NULL,
    password character varying(300) NOT NULL
);
    DROP TABLE public.users;
       public         postgres    false    3            �            1255    25007 /   user_auth(character varying, character varying)    FUNCTION     	  CREATE FUNCTION public.user_auth(_user_name character varying, _password character varying) RETURNS SETOF public.users
    LANGUAGE sql
    AS $$ 
		SELECT
			* 
		FROM
			public."users" 
		WHERE
			user_name = _user_name 
			AND password = _password;
	$$;
 [   DROP FUNCTION public.user_auth(_user_name character varying, _password character varying);
       public       postgres    false    197    3            	           1255    33511 /   vud_activate_request(integer, integer, integer)    FUNCTION       CREATE FUNCTION public.vud_activate_request(_id_request integer, _status integer, _last_status integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE 
	public.request
SET 
	id_status = _last_status,
	last_status = _status
WHERE 
	id = _id_request;
END
$$;
 g   DROP FUNCTION public.vud_activate_request(_id_request integer, _status integer, _last_status integer);
       public       postgres    false    3    1            �            1259    33388    cat_area    TABLE     k   CREATE TABLE public.cat_area (
    id integer NOT NULL,
    description character varying(100) NOT NULL
);
    DROP TABLE public.cat_area;
       public         postgres    false    3            �            1255    33447    vud_area_record()    FUNCTION     �   CREATE FUNCTION public.vud_area_record() RETURNS SETOF public.cat_area
    LANGUAGE plpgsql
    AS $$
BEGIN

    RETURN QUERY SELECT * from cat_area order by id asc;

END
$$;
 (   DROP FUNCTION public.vud_area_record();
       public       postgres    false    225    3    1            �            1255    33460 J   vud_create_dictum_request(date, character varying, integer, date, integer)    FUNCTION     5  CREATE FUNCTION public.vud_create_dictum_request(_date_receives_dictum date, _folio_dictum character varying, _answer_dictum integer, _citizen_dictum date, _id_request integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
INSERT INTO public.tracing_dictum(
			date_receives_dictum, 
			folio_dictum, 
			answer_dictum, 
			citizen_dictum, 
			id_request)
VALUES (_date_receives_dictum,
		_folio_dictum,
		_answer_dictum,
		_citizen_dictum,
		_id_request);


UPDATE public.request
	
SET 
	id_status = 7
WHERE 
	id = _id_request;

END
$$;
 �   DROP FUNCTION public.vud_create_dictum_request(_date_receives_dictum date, _folio_dictum character varying, _answer_dictum integer, _citizen_dictum date, _id_request integer);
       public       postgres    false    1    3                       1255    16751 K   vud_create_modality(integer, character varying, character varying, integer)    FUNCTION     D  CREATE FUNCTION public.vud_create_modality(OUT code_modality integer, _code integer, _description character varying, _legal_foundation character varying, _id_transact integer) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
DECLARE code_inserted_modality integer;
BEGIN 
	INSERT INTO public.cat_modality(
	code, 
	description, 
	legal_foundation, 
	id_transact,
	status)
	VALUES (_code, _description, _legal_foundation, _id_transact,1)
	RETURNING ID into code_inserted_modality;

	RETURN QUERY SELECT code from public.cat_modality where id = code_inserted_modality;
END 
$$;
 �   DROP FUNCTION public.vud_create_modality(OUT code_modality integer, _code integer, _description character varying, _legal_foundation character varying, _id_transact integer);
       public       postgres    false    3    1            �            1255    16735 )   vud_create_requirement(character varying)    FUNCTION     �   CREATE FUNCTION public.vud_create_requirement(_description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO public.cat_requirement(description,status)
	VALUES (_description,1);
END
$$;
 M   DROP FUNCTION public.vud_create_requirement(_description character varying);
       public       postgres    false    3    1                       1255    33519 7   vud_create_settlement(integer, date, character varying)    FUNCTION     Q  CREATE FUNCTION public.vud_create_settlement(_id_request integer, _settlement_date date, _settlement_number character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE public.request
SET settlement_date= _settlement_date,
	settlement_number= _settlement_number,
	id_status = 6
WHERE id = _id_request;

END
$$;
 ~   DROP FUNCTION public.vud_create_settlement(_id_request integer, _settlement_date date, _settlement_number character varying);
       public       postgres    false    3    1            �            1255    33525 K   vud_create_turn_request(integer, date, integer, character varying, integer)    FUNCTION     �  CREATE FUNCTION public.vud_create_turn_request(_id_request integer, _date_turn date, _review_area integer, _observations character varying, _id_status integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
INSERT INTO public.tracing_turn(
	id_request, 
	date_turn, 
	review_area, 
	observations)
VALUES (_id_request,
		_date_turn,
		_review_area,
		_observations);

UPDATE 
	public.request
SET 
	id_status = 5,
	last_status = _id_status
WHERE 
	id = _id_request;

END
$$;
 �   DROP FUNCTION public.vud_create_turn_request(_id_request integer, _date_turn date, _review_area integer, _observations character varying, _id_status integer);
       public       postgres    false    1    3            �            1259    25106    vulnerable_group    TABLE     �   CREATE TABLE public.vulnerable_group (
    id integer NOT NULL,
    description character varying(255) NOT NULL,
    status smallint NOT NULL
);
 $   DROP TABLE public.vulnerable_group;
       public         postgres    false    3                       1255    33306    vud_data_vulnerable_group()    FUNCTION     �   CREATE FUNCTION public.vud_data_vulnerable_group() RETURNS SETOF public.vulnerable_group
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY SELECT * FROM vulnerable_group where status = 1 order by id asc;

END
$$;
 2   DROP FUNCTION public.vud_data_vulnerable_group();
       public       postgres    false    3    1    223                       1255    16755    vud_delete_modality(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_modality(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE public.cat_modality
	SET   status = 0
	WHERE id = _id;

END
$$;
 7   DROP FUNCTION public.vud_delete_modality(_id integer);
       public       postgres    false    3    1            �            1255    16737    vud_delete_requirement(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_requirement(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	UPDATE public.cat_requirement
	SET  status = 0
	WHERE id = _id;
END
$$;
 :   DROP FUNCTION public.vud_delete_requirement(_id integer);
       public       postgres    false    1    3            �            1255    16710    vud_delete_transact(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_transact(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
	UPDATE public.cat_transact
	SET status= 0
	WHERE id = _id;
END
$$;
 7   DROP FUNCTION public.vud_delete_transact(_id integer);
       public       postgres    false    1    3                       1255    25027    vud_delete_user(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_user(_id_user integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$

BEGIN

UPDATE public."users" SET status = 0 WHERE "id" = _id_user;
return true;

END
$$;
 8   DROP FUNCTION public.vud_delete_user(_id_user integer);
       public       postgres    false    3    1                       1255    16754 (   vud_eliminate_relation_modality(integer)    FUNCTION     �   CREATE FUNCTION public.vud_eliminate_relation_modality(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
DELETE FROM cat_modality_requirement WHERE id_modality = _id;
END
$$;
 C   DROP FUNCTION public.vud_eliminate_relation_modality(_id integer);
       public       postgres    false    3    1            �            1255    16774 (   vud_eliminate_relation_transact(integer)    FUNCTION     �   CREATE FUNCTION public.vud_eliminate_relation_transact(_id_transact integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	DELETE FROM cat_transact_requirement where id_transact = _id_transact;
END
$$;
 L   DROP FUNCTION public.vud_eliminate_relation_transact(_id_transact integer);
       public       postgres    false    3    1            �            1255    16764 B   vud_insert_transact(integer, character varying, character varying)    FUNCTION     �  CREATE FUNCTION public.vud_insert_transact(OUT _id_code integer, _code integer, _description character varying, _legal_foundation character varying) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
DECLARE _id_transact integer;
BEGIN

	INSERT INTO public.cat_transact(
		code, 
		description, 
		legal_foundation, 
		status)
	VALUES (_code, _description,_legal_foundation,1)
	RETURNING ID into _id_transact;

	RETURN QUERY SELECT code from public.cat_transact where id = _id_transact;

END
$$;
 �   DROP FUNCTION public.vud_insert_transact(OUT _id_code integer, _code integer, _description character varying, _legal_foundation character varying);
       public       postgres    false    1    3                       1255    25018 c   vud_insert_user(character varying, integer, character varying, integer, integer, character varying)    FUNCTION       CREATE FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying) RETURNS void
    LANGUAGE sql
    AS $$ 
	
		INSERT INTO 
	public."users"
		("user_name",
		"employee_number",
		"description",
		"rol", 
		"user_register",
		"date_register",
		"status",
		"password") 
	VALUES (_user_name,
		_employee_number,
		_description,
		_rol,
		_user_register,
		'now()',
		1,
		_password);
		
	$$;
 �   DROP FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying);
       public       postgres    false    3                       1255    25125 P   vud_insert_user_permission(integer, integer, integer, integer, integer, integer)    FUNCTION       CREATE FUNCTION public.vud_insert_user_permission(_id_user integer, _id_module integer, _read_permission integer, _write_permission integer, _delete_permission integer, _update_permission integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

INSERT INTO public.permission(
	id_user,
	id_module, 
	read_permission, 
	write_permission, 
	delete_permission, 
	update_permission)
VALUES (
	_id_user, 
	_id_module, 
	_read_permission, 
	_write_permission, 
	_delete_permission, 
	_update_permission);

END
$$;
 �   DROP FUNCTION public.vud_insert_user_permission(_id_user integer, _id_module integer, _read_permission integer, _write_permission integer, _delete_permission integer, _update_permission integer);
       public       postgres    false    3    1            �            1255    25005 *   vud_log_insert(integer, character varying)    FUNCTION     �   CREATE FUNCTION public.vud_log_insert(_id_user integer, _description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO 
	public."log"(id_user, description,date_time)
	VALUES (_id_user,_description ,NOW());
END
$$;
 W   DROP FUNCTION public.vud_log_insert(_id_user integer, _description character varying);
       public       postgres    false    1    3                       1255    25103    vud_modality_data(integer)    FUNCTION        CREATE FUNCTION public.vud_modality_data(_code_modality integer, OUT id integer, OUT description character varying, OUT legal_foundation character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN 
	RETURN QUERY select 
		a.id,
		a.description,
		c.legal_foundation

	from 
		public.cat_requirement as a 
	LEFT JOIN
		public.cat_modality_requirement as b
	on 
		a.id = b.id_requirement
	LEFT join 
		public.cat_modality as c
	on
		b.id_modality = c.code
	where c.code = _code_modality ;
END
$$;
 �   DROP FUNCTION public.vud_modality_data(_code_modality integer, OUT id integer, OUT description character varying, OUT legal_foundation character varying);
       public       postgres    false    3    1                       1255    33385    vud_modality_days(integer)    FUNCTION       CREATE FUNCTION public.vud_modality_days(_code integer, OUT days smallint, OUT type_days character) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY SELECT 
				a.days,
				a.type_days
			FROM 
				cat_modality as a
			where
				a.code = _code;

END
$$;
 c   DROP FUNCTION public.vud_modality_days(_code integer, OUT days smallint, OUT type_days character);
       public       postgres    false    1    3            �            1255    16756    vud_modality_record()    FUNCTION        CREATE FUNCTION public.vud_modality_record(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT id_transact integer, OUT code_transact integer, OUT description_transact character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN

	RETURN QUERY select 
	a.id,
	a.code,
	a.description,
	a.legal_foundation,
	b.id,
	b.code,
	b.description
from 
	cat_modality as a
LEFT JOIN 
	cat_transact as b
on
	a.id_transact = b.code
WHERE 
	a.status = 1
order by a.id asc;

END
$$;
 �   DROP FUNCTION public.vud_modality_record(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT id_transact integer, OUT code_transact integer, OUT description_transact character varying);
       public       postgres    false    3    1            �            1259    16726    cat_requirement    TABLE     �   CREATE TABLE public.cat_requirement (
    id integer NOT NULL,
    description character varying(5000) NOT NULL,
    status integer
);
 #   DROP TABLE public.cat_requirement;
       public         postgres    false    3                       1255    16734    vud_record_requirement()    FUNCTION     �   CREATE FUNCTION public.vud_record_requirement() RETURNS SETOF public.cat_requirement
    LANGUAGE plpgsql
    AS $$
BEGIN 
	RETURN QUERY select * from public.cat_requirement where status = 1 order by id asc;
END
$$;
 /   DROP FUNCTION public.vud_record_requirement();
       public       postgres    false    3    1    205            �            1255    16723    vud_record_transact()    FUNCTION     W  CREATE FUNCTION public.vud_record_transact(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT total_modality bigint, OUT id_transact integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$

	SELECT
		a.id,
		a.code,
		a.description,
		a.legal_foundation,
		count(b.id_transact) as total_modality,
		b.id_transact	
	FROM
		cat_transact  as a
	left JOIN 
		cat_modality as b
	on  a.code = b.id_transact
	WHERE
		a.status = 1
	GROUP BY b.id_transact,a.id,a.code,a.description,a.legal_foundation
	order by a.id asc;

$$;
 �   DROP FUNCTION public.vud_record_transact(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT total_modality bigint, OUT id_transact integer);
       public       postgres    false    3                       1255    16752 3   vud_relation_modality_requirement(integer, integer)    FUNCTION       CREATE FUNCTION public.vud_relation_modality_requirement(_id_modality integer, _id_requirement integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

INSERT INTO public.cat_modality_requirement(id_modality,id_requirement)
VALUES (_id_modality, _id_requirement);

END
$$;
 g   DROP FUNCTION public.vud_relation_modality_requirement(_id_modality integer, _id_requirement integer);
       public       postgres    false    1    3                       1255    16765 6   vud_relation_transaction_requirement(integer, integer)    FUNCTION       CREATE FUNCTION public.vud_relation_transaction_requirement(_id_transact integer, _id_requirement integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

INSERT INTO public.cat_transact_requirement(
	id_transact, 
	id_requirement)
VALUES (_id_transact,_id_requirement);

END
$$;
 j   DROP FUNCTION public.vud_relation_transaction_requirement(_id_transact integer, _id_requirement integer);
       public       postgres    false    3    1            �            1255    33380 q   vud_request_address(character varying, character varying, integer, integer, character varying, character varying)    FUNCTION     d  CREATE FUNCTION public.vud_request_address(_postal_code character varying, _colony character varying, _ext_number integer, _int_number integer, _street character varying, _delegation character varying) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
DECLARE _id_request_address integer;
BEGIN

INSERT INTO public.address(
		postal_code, 
		colony, 
		ext_number, 
		int_number, 
		street,
		delegation)
	
VALUES (_postal_code,
		_colony,
		_ext_number,
		_int_number,
		_street,
		_delegation)

RETURNING ID into _id_request_address;
RETURN QUERY SELECT _id_request_address;

END
$$;
 �   DROP FUNCTION public.vud_request_address(_postal_code character varying, _colony character varying, _ext_number integer, _int_number integer, _street character varying, _delegation character varying);
       public       postgres    false    3    1                       1255    33421 �   vud_request_data(integer, character varying, integer, integer, integer, integer, integer, character varying, character varying, integer, integer, date, integer)    FUNCTION     "  CREATE FUNCTION public.vud_request_data(_id_user integer, _observations character varying, _id_status integer, _id_address integer, _id_interested integer, _id_transact integer, _id_vulnerable_group integer, _request_turn character varying, _type_work character varying, _means_request integer, _delivery integer, _date_commitment date, _id_modality integer) RETURNS SETOF character varying
    LANGUAGE plpgsql
    AS $$
DECLARE id_request_created integer;
BEGIN

INSERT INTO public.request( id_user, 
							date_creation, 
							observations, 
							id_status, 
							id_address, 
							id_interested, 
							id_transact, 
							id_vulnerable_group, 
							request_turn, 
							type_work, 
							means_request, 
							delivery,
							date_commitment,
							flag,
							id_modality)
VALUES (_id_user, 
		NOW(), 
		_observations, 
		_id_status, 
		_id_address, 
		_id_interested, 
		_id_transact, 
		_id_vulnerable_group, 
		_request_turn, 
		_type_work, 
		_means_request, 
		_delivery,
		_date_commitment,
		1,
		_id_modality)
RETURNING ID into id_request_created;

	UPDATE public.request
	SET  folio = CONCAT(id_request_created,2018)
	WHERE id = id_request_created;

	RETURN QUERY select folio from public.request where id = id_request_created;

END
$$;
 f  DROP FUNCTION public.vud_request_data(_id_user integer, _observations character varying, _id_status integer, _id_address integer, _id_interested integer, _id_transact integer, _id_vulnerable_group integer, _request_turn character varying, _type_work character varying, _means_request integer, _delivery integer, _date_commitment date, _id_modality integer);
       public       postgres    false    1    3            �            1255    33328 k   vud_request_interested(character varying, character varying, character varying, character varying, integer)    FUNCTION     !  CREATE FUNCTION public.vud_request_interested(_name character varying, _last_name character varying, _e_mail character varying, _phone character varying, _type_person integer) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
DECLARE id_request_interested integer;
BEGIN
INSERT INTO public.interested(
	name, 
	last_name, 
	e_mail, 
	phone,
	type_person)
	VALUES (_name, 
			_last_name, 
			_e_mail, 
			_phone,
			_type_person)

RETURNING ID into id_request_interested;
RETURN QUERY SELECT id_request_interested;

END
$$;
 �   DROP FUNCTION public.vud_request_interested(_name character varying, _last_name character varying, _e_mail character varying, _phone character varying, _type_person integer);
       public       postgres    false    3    1                       1255    33522    vud_request_record()    FUNCTION     �  CREATE FUNCTION public.vud_request_record(OUT id integer, OUT folio character varying, OUT id_user integer, OUT id_status integer, OUT description_status character varying, OUT date_creation text, OUT date_commitment text, OUT code integer, OUT description character varying, OUT modality_desc character varying, OUT id_turn integer, OUT id_dictum integer, OUT last_status integer, OUT settlement_number character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY SELECT 
	a.id,
	a.folio,
	a.id_user,
	d.id as id_status,
	d.description as description_status,
	to_char(a.date_creation,'dd-mm-yyyy' ),
	to_char(a.date_commitment,'dd-mm-yyyy' ),
	b.code,
	b.description,
	c.description as modality_desc,
	e.id as id_turn,
	f.id as id_dictum,
	a.last_status,
	a.settlement_number
FROM 
	public.request as a
left join 
	public.cat_transact as b
on
	a.id_transact = b.id
left join 
	public.cat_modality as c
on 
	a.id_modality = c.code

left join 
	public.cat_status as d
on
	d.id = a.id_status
left join 
	public.tracing_turn as e
on
	e.id_request = a.id
left join
	public.tracing_dictum as f
on
	f.id_request = a.id
WHERE
	flag = 1
order by id desc;

END
$$;
 �  DROP FUNCTION public.vud_request_record(OUT id integer, OUT folio character varying, OUT id_user integer, OUT id_status integer, OUT description_status character varying, OUT date_creation text, OUT date_commitment text, OUT code integer, OUT description character varying, OUT modality_desc character varying, OUT id_turn integer, OUT id_dictum integer, OUT last_status integer, OUT settlement_number character varying);
       public       postgres    false    1    3            �            1259    16413    role    TABLE     g   CREATE TABLE public.role (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);
    DROP TABLE public.role;
       public         postgres    false    3                        1255    25015 
   vud_role()    FUNCTION     �   CREATE FUNCTION public.vud_role() RETURNS SETOF public.role
    LANGUAGE sql
    AS $$ 
		SELECT
			*
		FROM
			public. "role";
	$$;
 !   DROP FUNCTION public.vud_role();
       public       postgres    false    199    3            �            1259    25079 
   cat_status    TABLE     m   CREATE TABLE public.cat_status (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);
    DROP TABLE public.cat_status;
       public         postgres    false    3            !           1255    33428    vud_status_list()    FUNCTION     �   CREATE FUNCTION public.vud_status_list() RETURNS SETOF public.cat_status
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY select 
				* 
			from 
				public.cat_status;
END
$$;
 (   DROP FUNCTION public.vud_status_list();
       public       postgres    false    217    1    3                       1255    33347    vud_transact_days(integer)    FUNCTION     "  CREATE FUNCTION public.vud_transact_days(_code integer, OUT days integer, OUT type_days character) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY SELECT 
				a.days,
				a.type_days
			FROM 
				cat_transact as a
			where
				a.code = _code;
				
END
$$;
 b   DROP FUNCTION public.vud_transact_days(_code integer, OUT days integer, OUT type_days character);
       public       postgres    false    1    3            �            1255    33332    vud_transact_record()    FUNCTION     *  CREATE FUNCTION public.vud_transact_record(OUT code integer, OUT description character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY SELECT a.code,
						a.description
				FROM public.cat_transact as a 
				WHERE status = 1 
				order by id desc;
END
$$;
 _   DROP FUNCTION public.vud_transact_record(OUT code integer, OUT description character varying);
       public       postgres    false    1    3            �            1259    16694    cat_modality    TABLE     '  CREATE TABLE public.cat_modality (
    id integer NOT NULL,
    code integer NOT NULL,
    description character varying(2000) NOT NULL,
    legal_foundation character varying(3000),
    id_transact integer NOT NULL,
    status integer NOT NULL,
    days smallint,
    type_days character(1)
);
     DROP TABLE public.cat_modality;
       public         postgres    false    3                        1255    16821 %   vud_transact_record_modality(integer)    FUNCTION     �   CREATE FUNCTION public.vud_transact_record_modality(_code integer) RETURNS SETOF public.cat_modality
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY SELECT * FROM public.cat_modality where id_transact = _code order by id asc;
END
$$;
 B   DROP FUNCTION public.vud_transact_record_modality(_code integer);
       public       postgres    false    3    1    203            �            1255    25101 !   vud_transact_requeriment(integer)    FUNCTION     �  CREATE FUNCTION public.vud_transact_requeriment(_id_code integer, OUT id integer, OUT description character varying, OUT legal_foundation character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
	
	RETURN QUERY select 
		a.id,
		a.description ,
		c.legal_foundation
	from 
		public.cat_requirement as a
	inner join public.cat_transact_requirement as b
	on a.id = b.id_requirement
	inner join public.cat_transact as c
	on c.code = b.id_transact
	where c.code = _id_code;

END
$$;
 �   DROP FUNCTION public.vud_transact_requeriment(_id_code integer, OUT id integer, OUT description character varying, OUT legal_foundation character varying);
       public       postgres    false    1    3                       1255    33481 !   vud_update_correct(integer, date)    FUNCTION     �   CREATE FUNCTION public.vud_update_correct(_id_request integer, _correct_date date) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
UPDATE public.request
	
SET 	
	correct_date = _correct_date,
	id_status = 7
WHERE 
	id = _id_request;
END
$$;
 R   DROP FUNCTION public.vud_update_correct(_id_request integer, _correct_date date);
       public       postgres    false    1    3            
           1255    16753 B   vud_update_modality(integer, character varying, character varying)    FUNCTION     )  CREATE FUNCTION public.vud_update_modality(_id integer, _description character varying, _legal_foundation character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE public.cat_modality
	SET   description=_description, legal_foundation=_legal_foundation
	WHERE id = _id;

END
$$;
 |   DROP FUNCTION public.vud_update_modality(_id integer, _description character varying, _legal_foundation character varying);
       public       postgres    false    3    1                       1255    33480 *   vud_update_prevention(integer, date, date)    FUNCTION     L  CREATE FUNCTION public.vud_update_prevention(_id_request integer, _prevent_date date, _citizen_prevent_date date) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
UPDATE public.request
	
SET 
	prevent_date = _prevent_date, 
	citizen_prevent_date = _citizen_prevent_date,
	id_status = 7
WHERE 
	id = _id_request;
END
$$;
 q   DROP FUNCTION public.vud_update_prevention(_id_request integer, _prevent_date date, _citizen_prevent_date date);
       public       postgres    false    1    3                       1255    16736 2   vud_update_requirement(integer, character varying)    FUNCTION     �   CREATE FUNCTION public.vud_update_requirement(_id integer, _description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

	UPDATE public.cat_requirement
	SET  description=_description
	WHERE id = _id;
END
$$;
 Z   DROP FUNCTION public.vud_update_requirement(_id integer, _description character varying);
       public       postgres    false    1    3                       1255    33509 G   vud_update_status_request(integer, integer, integer, character varying)    FUNCTION     ~  CREATE FUNCTION public.vud_update_status_request(_id_request integer, _status integer, _last_status integer, _cancelation_description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE 
	public.request
SET 
	id_status = _status,
	last_status = _last_status,
	cancelation_description = _cancelation_description
WHERE 
	id = _id_request;
END
$$;
 �   DROP FUNCTION public.vud_update_status_request(_id_request integer, _status integer, _last_status integer, _cancelation_description character varying);
       public       postgres    false    3    1                       1255    16766 B   vud_update_transact(integer, character varying, character varying)    FUNCTION     (  CREATE FUNCTION public.vud_update_transact(_id integer, _description character varying, _legal_foundation character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
	UPDATE public.cat_transact
	SET  description=_description, legal_foundation=_legal_foundation
	WHERE id = _id;
END
$$;
 |   DROP FUNCTION public.vud_update_transact(_id integer, _description character varying, _legal_foundation character varying);
       public       postgres    false    1    3                       1255    25020 l   vud_update_user(integer, character varying, integer, character varying, integer, integer, character varying)    FUNCTION       CREATE FUNCTION public.vud_update_user(_id_user integer, _user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$ 
	BEGIN
	IF (_password = '') THEN
		update 
		public."users"
	SET		
		user_name = _user_name,
		employee_number = _employee_number,
		description = _description,
		rol = _rol, 
		user_register = _user_register
	WHERE 
		id = _id_user;
	ELSE 
		update 
		public."users"
	SET		
		user_name = _user_name,
		employee_number = _employee_number,
		description = _description,
		rol = _rol, 
		user_register = _user_register,
		password = _password
	WHERE 
		id = _id_user;
	END IF;
	
	RETURN true;
	END
	$$;
 �   DROP FUNCTION public.vud_update_user(_id_user integer, _user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying);
       public       postgres    false    3    1            �            1255    25122    vud_users_permisions(integer)    FUNCTION     z  CREATE FUNCTION public.vud_users_permisions(_id_user integer, OUT id integer, OUT name character varying, OUT id_module integer, OUT id_user integer, OUT read_permission smallint, OUT write_permission smallint, OUT delete_permission smallint, OUT update_permission smallint) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY SELECT 
	a.id,
	a.name,
	b.id_module,
	b.id_user,
	b.read_permission,
	b.write_permission,
	b.delete_permission,
	b.update_permission
FROM
	PUBLIC.modules AS A 
	LEFT JOIN PUBLIC.permission AS b ON a.id = b.id_module

WHERE
	b.id_user = _id_user
order by a.id;
END
$$;
   DROP FUNCTION public.vud_users_permisions(_id_user integer, OUT id integer, OUT name character varying, OUT id_module integer, OUT id_user integer, OUT read_permission smallint, OUT write_permission smallint, OUT delete_permission smallint, OUT update_permission smallint);
       public       postgres    false    3    1            �            1255    33472    vud_users_record()    FUNCTION       CREATE FUNCTION public.vud_users_record(OUT id integer, OUT user_name character varying, OUT employee_number integer, OUT user_register integer, OUT rol character varying, OUT description character varying, OUT id_rol integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$ 
		SELECT
			a.id,
			a.user_name,
			a.employee_number,
			a.user_register,
			b.description,
			a.description,
			b.id
		FROM
			public. "users" as a
		INNER JOIN public. "role" as b
		ON a.rol = b.id
		where a.status = 1 order by a.id asc;
	$$;
 �   DROP FUNCTION public.vud_users_record(OUT id integer, OUT user_name character varying, OUT employee_number integer, OUT user_register integer, OUT rol character varying, OUT description character varying, OUT id_rol integer);
       public       postgres    false    3                       1255    33483    vud_view_data_request(integer)    FUNCTION     �	  CREATE FUNCTION public.vud_view_data_request(_id_request integer, OUT id integer, OUT folio character varying, OUT id_user integer, OUT user_name character varying, OUT means_request smallint, OUT prevent_date text, OUT citizen_prevent_date text, OUT correct_date text, OUT observation character varying, OUT request_turn character varying, OUT type_work character varying, OUT name character varying, OUT last_name character varying, OUT phone character varying, OUT delegation character varying, OUT colony character varying, OUT postal_code character varying, OUT street character varying, OUT ext_number integer, OUT int_number integer, OUT id_status integer, OUT description_status character varying, OUT date_creation text, OUT date_commitment text, OUT code integer, OUT description character varying, OUT modality_desc character varying, OUT id_turn integer, OUT id_dictum integer, OUT folio_dictum character varying, OUT date_receives_dictum text, OUT answer_dictum integer, OUT citizen_dictum text) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY SELECT 
	a.id,
	a.folio,
	a.id_user,
	i.user_name,
	a.means_request,
	to_char(a.prevent_date,'yyyy-mm-dd'),
	to_char(a.citizen_prevent_date,'yyyy-mm-dd'),
	to_char(a.correct_date,'yyyy-mm-dd'),
	a.observations,
	a.request_turn,
	a.type_work,
	g.name,
	g.last_name,
	g.phone,
	h.delegation,
	h.colony,
	h.postal_code,
	h.street,
	h.ext_number,
	h.int_number,
	d.id as id_status,
	d.description as description_status,
	to_char(a.date_creation,'yyyy-mm-dd' ),
	to_char(a.date_commitment,'yyyy-mm-dd' ),
	b.code,
	b.description,
	c.description as modality_desc,
	e.id as id_turn,
	f.id as id_dictum,
	f.folio_dictum,
	to_char(f.date_receives_dictum,'yyyy-mm-dd'),
	f.answer_dictum,
	to_char(f.citizen_dictum,'yyyy-mm-dd')
FROM 
	public.request as a
left join 
	public.cat_transact as b
on
	a.id_transact = b.id
left join 
	public.cat_modality as c
on 
	a.id_modality = c.code

left join 
	public.cat_status as d
on
	d.id = a.id_status
left join 
	public.tracing_turn as e
on
	e.id_request = a.id
left join
	public.tracing_dictum as f
on
	f.id_request = a.id
left join
	public.interested as g
on 
	a.id_interested = g.id
left join
	public.address as h
on
	a.id_address = h.id
left join 
	public.users as i
on 
	i.id = a.id_user
WHERE
	flag = 1 
AND a.id = _id_request
order by id asc;

END
$$;
 �  DROP FUNCTION public.vud_view_data_request(_id_request integer, OUT id integer, OUT folio character varying, OUT id_user integer, OUT user_name character varying, OUT means_request smallint, OUT prevent_date text, OUT citizen_prevent_date text, OUT correct_date text, OUT observation character varying, OUT request_turn character varying, OUT type_work character varying, OUT name character varying, OUT last_name character varying, OUT phone character varying, OUT delegation character varying, OUT colony character varying, OUT postal_code character varying, OUT street character varying, OUT ext_number integer, OUT int_number integer, OUT id_status integer, OUT description_status character varying, OUT date_creation text, OUT date_commitment text, OUT code integer, OUT description character varying, OUT modality_desc character varying, OUT id_turn integer, OUT id_dictum integer, OUT folio_dictum character varying, OUT date_receives_dictum text, OUT answer_dictum integer, OUT citizen_dictum text);
       public       postgres    false    3    1            �            1259    16411    ROLE_id_seq    SEQUENCE     �   CREATE SEQUENCE public."ROLE_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public."ROLE_id_seq";
       public       postgres    false    199    3            �           0    0    ROLE_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public."ROLE_id_seq" OWNED BY public.role.id;
            public       postgres    false    198            �            1259    16400    USERS_Id_seq    SEQUENCE     �   CREATE SEQUENCE public."USERS_Id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public."USERS_Id_seq";
       public       postgres    false    197    3            �           0    0    USERS_Id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public."USERS_Id_seq" OWNED BY public.users.id;
            public       postgres    false    196            �            1259    25034    address    TABLE       CREATE TABLE public.address (
    id integer NOT NULL,
    postal_code character varying(7) NOT NULL,
    colony character varying(50) NOT NULL,
    ext_number integer,
    int_number integer,
    street character varying(255) NOT NULL,
    delegation character varying(255) NOT NULL
);
    DROP TABLE public.address;
       public         postgres    false    3            �            1259    25032    address_id_seq    SEQUENCE     �   CREATE SEQUENCE public.address_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.address_id_seq;
       public       postgres    false    209    3            �           0    0    address_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.address_id_seq OWNED BY public.address.id;
            public       postgres    false    208            �            1259    33386    cat_area_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_area_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.cat_area_id_seq;
       public       postgres    false    225    3            �           0    0    cat_area_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.cat_area_id_seq OWNED BY public.cat_area.id;
            public       postgres    false    224            �            1259    16692    cat_modality_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_modality_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.cat_modality_id_seq;
       public       postgres    false    203    3            �           0    0    cat_modality_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.cat_modality_id_seq OWNED BY public.cat_modality.id;
            public       postgres    false    202            �            1259    16746    cat_modality_requirement    TABLE     f   CREATE TABLE public.cat_modality_requirement (
    id_modality integer,
    id_requirement integer
);
 ,   DROP TABLE public.cat_modality_requirement;
       public         postgres    false    3            �            1259    16724    cat_requeriment_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_requeriment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.cat_requeriment_id_seq;
       public       postgres    false    205    3            �           0    0    cat_requeriment_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.cat_requeriment_id_seq OWNED BY public.cat_requirement.id;
            public       postgres    false    204            �            1259    25077    cat_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.cat_status_id_seq;
       public       postgres    false    217    3            �           0    0    cat_status_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.cat_status_id_seq OWNED BY public.cat_status.id;
            public       postgres    false    216            �            1259    16674    cat_transact    TABLE       CREATE TABLE public.cat_transact (
    id integer NOT NULL,
    code integer NOT NULL,
    description character varying(2000) NOT NULL,
    legal_foundation character varying(3000),
    status integer NOT NULL,
    days integer,
    type_days character(1)
);
     DROP TABLE public.cat_transact;
       public         postgres    false    3            �            1259    16672    cat_transact_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_transact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.cat_transact_id_seq;
       public       postgres    false    201    3            �           0    0    cat_transact_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.cat_transact_id_seq OWNED BY public.cat_transact.id;
            public       postgres    false    200            �            1259    16757    cat_transact_requirement    TABLE     x   CREATE TABLE public.cat_transact_requirement (
    id_transact integer NOT NULL,
    id_requirement integer NOT NULL
);
 ,   DROP TABLE public.cat_transact_requirement;
       public         postgres    false    3            �            1259    25053 
   interested    TABLE     �   CREATE TABLE public.interested (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    last_name character varying(100),
    e_mail character varying(100),
    phone character varying(15) NOT NULL,
    type_person smallint NOT NULL
);
    DROP TABLE public.interested;
       public         postgres    false    3            �            1259    25051    interested_id_seq    SEQUENCE     �   CREATE SEQUENCE public.interested_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.interested_id_seq;
       public       postgres    false    3    213            �           0    0    interested_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.interested_id_seq OWNED BY public.interested.id;
            public       postgres    false    212            �            1259    33466    log    TABLE     �   CREATE TABLE public.log (
    id integer NOT NULL,
    id_user integer NOT NULL,
    description character varying(255) NOT NULL,
    date_time timestamp with time zone NOT NULL
);
    DROP TABLE public.log;
       public         postgres    false    3            �            1259    33464 
   log_id_seq    SEQUENCE     �   CREATE SEQUENCE public.log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 !   DROP SEQUENCE public.log_id_seq;
       public       postgres    false    3    231            �           0    0 
   log_id_seq    SEQUENCE OWNED BY     9   ALTER SEQUENCE public.log_id_seq OWNED BY public.log.id;
            public       postgres    false    230            �            1259    25095    modules    TABLE     �   CREATE TABLE public.modules (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    status smallint NOT NULL
);
    DROP TABLE public.modules;
       public         postgres    false    3            �            1259    25093    modules_id_seq    SEQUENCE     �   CREATE SEQUENCE public.modules_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.modules_id_seq;
       public       postgres    false    3    221            �           0    0    modules_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.modules_id_seq OWNED BY public.modules.id;
            public       postgres    false    220            �            1259    25087 
   permission    TABLE     �   CREATE TABLE public.permission (
    id integer NOT NULL,
    id_user integer NOT NULL,
    id_module integer NOT NULL,
    read_permission smallint,
    write_permission smallint,
    delete_permission smallint,
    update_permission smallint
);
    DROP TABLE public.permission;
       public         postgres    false    3            �            1259    25085    permission_id_seq    SEQUENCE     �   CREATE SEQUENCE public.permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.permission_id_seq;
       public       postgres    false    3    219            �           0    0    permission_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.permission_id_seq OWNED BY public.permission.id;
            public       postgres    false    218            �            1259    25042    request    TABLE     1  CREATE TABLE public.request (
    id integer NOT NULL,
    folio character varying(50),
    id_user integer NOT NULL,
    date_creation date NOT NULL,
    observations character varying(2000),
    id_status integer NOT NULL,
    id_address integer NOT NULL,
    id_interested integer NOT NULL,
    id_transact integer NOT NULL,
    id_vulnerable_group smallint NOT NULL,
    request_turn character varying(100),
    type_work character varying(100),
    means_request smallint NOT NULL,
    delivery smallint NOT NULL,
    date_commitment date,
    flag smallint,
    id_modality integer,
    prevent_date date,
    citizen_prevent_date date,
    correct_date date,
    cancelation_description character varying(2000),
    last_status integer,
    settlement_date date,
    settlement_number character varying(50)
);
    DROP TABLE public.request;
       public         postgres    false    3            �            1259    25064    request_audit    TABLE     �   CREATE TABLE public.request_audit (
    id integer NOT NULL,
    id_user integer NOT NULL,
    accion character varying NOT NULL,
    id_request integer NOT NULL
);
 !   DROP TABLE public.request_audit;
       public         postgres    false    3            �            1259    25040    request_id_seq    SEQUENCE     �   CREATE SEQUENCE public.request_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.request_id_seq;
       public       postgres    false    3    211                        0    0    request_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.request_id_seq OWNED BY public.request.id;
            public       postgres    false    210            �            1259    33453    tracing_dictum    TABLE     �   CREATE TABLE public.tracing_dictum (
    id integer NOT NULL,
    date_receives_dictum date NOT NULL,
    folio_dictum character varying(50) NOT NULL,
    answer_dictum integer NOT NULL,
    citizen_dictum date,
    id_request integer NOT NULL
);
 "   DROP TABLE public.tracing_dictum;
       public         postgres    false    3            �            1259    33451    tracing_dictum_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tracing_dictum_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.tracing_dictum_id_seq;
       public       postgres    false    3    229                       0    0    tracing_dictum_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.tracing_dictum_id_seq OWNED BY public.tracing_dictum.id;
            public       postgres    false    228            �            1259    33431    tracing_turn    TABLE     �   CREATE TABLE public.tracing_turn (
    id integer NOT NULL,
    id_request integer NOT NULL,
    date_turn date NOT NULL,
    review_area integer NOT NULL,
    observations character varying(2000)
);
     DROP TABLE public.tracing_turn;
       public         postgres    false    3            �            1259    33429    tracing_turn_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tracing_turn_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.tracing_turn_id_seq;
       public       postgres    false    227    3                       0    0    tracing_turn_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.tracing_turn_id_seq OWNED BY public.tracing_turn.id;
            public       postgres    false    226            �            1259    25059 
   user_audit    TABLE     �   CREATE TABLE public.user_audit (
    id integer NOT NULL,
    id_user integer NOT NULL,
    accion character varying(255) NOT NULL,
    id_user_modification integer NOT NULL
);
    DROP TABLE public.user_audit;
       public         postgres    false    3            �            1259    25104    vulnerable_group_id_seq    SEQUENCE     �   CREATE SEQUENCE public.vulnerable_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.vulnerable_group_id_seq;
       public       postgres    false    223    3                       0    0    vulnerable_group_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.vulnerable_group_id_seq OWNED BY public.vulnerable_group.id;
            public       postgres    false    222                       2604    25037 
   address id    DEFAULT     h   ALTER TABLE ONLY public.address ALTER COLUMN id SET DEFAULT nextval('public.address_id_seq'::regclass);
 9   ALTER TABLE public.address ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    208    209    209                       2604    33391    cat_area id    DEFAULT     j   ALTER TABLE ONLY public.cat_area ALTER COLUMN id SET DEFAULT nextval('public.cat_area_id_seq'::regclass);
 :   ALTER TABLE public.cat_area ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    224    225    225                       2604    16697    cat_modality id    DEFAULT     r   ALTER TABLE ONLY public.cat_modality ALTER COLUMN id SET DEFAULT nextval('public.cat_modality_id_seq'::regclass);
 >   ALTER TABLE public.cat_modality ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    203    202    203                       2604    16729    cat_requirement id    DEFAULT     x   ALTER TABLE ONLY public.cat_requirement ALTER COLUMN id SET DEFAULT nextval('public.cat_requeriment_id_seq'::regclass);
 A   ALTER TABLE public.cat_requirement ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    205    204    205                       2604    25082    cat_status id    DEFAULT     n   ALTER TABLE ONLY public.cat_status ALTER COLUMN id SET DEFAULT nextval('public.cat_status_id_seq'::regclass);
 <   ALTER TABLE public.cat_status ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    217    216    217                       2604    16677    cat_transact id    DEFAULT     r   ALTER TABLE ONLY public.cat_transact ALTER COLUMN id SET DEFAULT nextval('public.cat_transact_id_seq'::regclass);
 >   ALTER TABLE public.cat_transact ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    201    200    201                       2604    25056    interested id    DEFAULT     n   ALTER TABLE ONLY public.interested ALTER COLUMN id SET DEFAULT nextval('public.interested_id_seq'::regclass);
 <   ALTER TABLE public.interested ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    213    212    213                       2604    33469    log id    DEFAULT     `   ALTER TABLE ONLY public.log ALTER COLUMN id SET DEFAULT nextval('public.log_id_seq'::regclass);
 5   ALTER TABLE public.log ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    231    230    231                       2604    25098 
   modules id    DEFAULT     h   ALTER TABLE ONLY public.modules ALTER COLUMN id SET DEFAULT nextval('public.modules_id_seq'::regclass);
 9   ALTER TABLE public.modules ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    220    221    221                       2604    25090    permission id    DEFAULT     n   ALTER TABLE ONLY public.permission ALTER COLUMN id SET DEFAULT nextval('public.permission_id_seq'::regclass);
 <   ALTER TABLE public.permission ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    218    219    219                       2604    25045 
   request id    DEFAULT     h   ALTER TABLE ONLY public.request ALTER COLUMN id SET DEFAULT nextval('public.request_id_seq'::regclass);
 9   ALTER TABLE public.request ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    211    210    211                       2604    16416    role id    DEFAULT     d   ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public."ROLE_id_seq"'::regclass);
 6   ALTER TABLE public.role ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    198    199    199                       2604    33456    tracing_dictum id    DEFAULT     v   ALTER TABLE ONLY public.tracing_dictum ALTER COLUMN id SET DEFAULT nextval('public.tracing_dictum_id_seq'::regclass);
 @   ALTER TABLE public.tracing_dictum ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    229    228    229                       2604    33434    tracing_turn id    DEFAULT     r   ALTER TABLE ONLY public.tracing_turn ALTER COLUMN id SET DEFAULT nextval('public.tracing_turn_id_seq'::regclass);
 >   ALTER TABLE public.tracing_turn ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    226    227    227                       2604    16405    users id    DEFAULT     f   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public."USERS_Id_seq"'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    197    196    197                       2604    25109    vulnerable_group id    DEFAULT     z   ALTER TABLE ONLY public.vulnerable_group ALTER COLUMN id SET DEFAULT nextval('public.vulnerable_group_id_seq'::regclass);
 B   ALTER TABLE public.vulnerable_group ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    222    223    223            �          0    25034    address 
   TABLE DATA               f   COPY public.address (id, postal_code, colony, ext_number, int_number, street, delegation) FROM stdin;
    public       postgres    false    209   �+      �          0    33388    cat_area 
   TABLE DATA               3   COPY public.cat_area (id, description) FROM stdin;
    public       postgres    false    225   �-      �          0    16694    cat_modality 
   TABLE DATA               u   COPY public.cat_modality (id, code, description, legal_foundation, id_transact, status, days, type_days) FROM stdin;
    public       postgres    false    203   (.      �          0    16746    cat_modality_requirement 
   TABLE DATA               O   COPY public.cat_modality_requirement (id_modality, id_requirement) FROM stdin;
    public       postgres    false    206   �:      �          0    16726    cat_requirement 
   TABLE DATA               B   COPY public.cat_requirement (id, description, status) FROM stdin;
    public       postgres    false    205   �?      �          0    25079 
   cat_status 
   TABLE DATA               5   COPY public.cat_status (id, description) FROM stdin;
    public       postgres    false    217   %�      �          0    16674    cat_transact 
   TABLE DATA               h   COPY public.cat_transact (id, code, description, legal_foundation, status, days, type_days) FROM stdin;
    public       postgres    false    201   ��      �          0    16757    cat_transact_requirement 
   TABLE DATA               O   COPY public.cat_transact_requirement (id_transact, id_requirement) FROM stdin;
    public       postgres    false    207   ��      �          0    25053 
   interested 
   TABLE DATA               U   COPY public.interested (id, name, last_name, e_mail, phone, type_person) FROM stdin;
    public       postgres    false    213   ݩ      �          0    33466    log 
   TABLE DATA               B   COPY public.log (id, id_user, description, date_time) FROM stdin;
    public       postgres    false    231   ��      �          0    25095    modules 
   TABLE DATA               3   COPY public.modules (id, name, status) FROM stdin;
    public       postgres    false    221   ��      �          0    25087 
   permission 
   TABLE DATA               �   COPY public.permission (id, id_user, id_module, read_permission, write_permission, delete_permission, update_permission) FROM stdin;
    public       postgres    false    219   �      �          0    25042    request 
   TABLE DATA               l  COPY public.request (id, folio, id_user, date_creation, observations, id_status, id_address, id_interested, id_transact, id_vulnerable_group, request_turn, type_work, means_request, delivery, date_commitment, flag, id_modality, prevent_date, citizen_prevent_date, correct_date, cancelation_description, last_status, settlement_date, settlement_number) FROM stdin;
    public       postgres    false    211   A�      �          0    25064    request_audit 
   TABLE DATA               H   COPY public.request_audit (id, id_user, accion, id_request) FROM stdin;
    public       postgres    false    215   �      �          0    16413    role 
   TABLE DATA               /   COPY public.role (id, description) FROM stdin;
    public       postgres    false    199   ;�      �          0    33453    tracing_dictum 
   TABLE DATA               {   COPY public.tracing_dictum (id, date_receives_dictum, folio_dictum, answer_dictum, citizen_dictum, id_request) FROM stdin;
    public       postgres    false    229   |�      �          0    33431    tracing_turn 
   TABLE DATA               \   COPY public.tracing_turn (id, id_request, date_turn, review_area, observations) FROM stdin;
    public       postgres    false    227   $�      �          0    25059 
   user_audit 
   TABLE DATA               O   COPY public.user_audit (id, id_user, accion, id_user_modification) FROM stdin;
    public       postgres    false    214   ̸      �          0    16402    users 
   TABLE DATA               �   COPY public.users (id, user_name, employee_number, description, rol, user_register, date_register, status, password) FROM stdin;
    public       postgres    false    197   �      �          0    25106    vulnerable_group 
   TABLE DATA               C   COPY public.vulnerable_group (id, description, status) FROM stdin;
    public       postgres    false    223   ��                 0    0    ROLE_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public."ROLE_id_seq"', 3, true);
            public       postgres    false    198                       0    0    USERS_Id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public."USERS_Id_seq"', 41, true);
            public       postgres    false    196                       0    0    address_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.address_id_seq', 190, true);
            public       postgres    false    208                       0    0    cat_area_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.cat_area_id_seq', 1, false);
            public       postgres    false    224                       0    0    cat_modality_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.cat_modality_id_seq', 57, true);
            public       postgres    false    202            	           0    0    cat_requeriment_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.cat_requeriment_id_seq', 481, true);
            public       postgres    false    204            
           0    0    cat_status_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.cat_status_id_seq', 1, true);
            public       postgres    false    216                       0    0    cat_transact_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.cat_transact_id_seq', 31, true);
            public       postgres    false    200                       0    0    interested_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.interested_id_seq', 157, true);
            public       postgres    false    212                       0    0 
   log_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.log_id_seq', 67, true);
            public       postgres    false    230                       0    0    modules_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.modules_id_seq', 1, false);
            public       postgres    false    220                       0    0    permission_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.permission_id_seq', 53, true);
            public       postgres    false    218                       0    0    request_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.request_id_seq', 139, true);
            public       postgres    false    210                       0    0    tracing_dictum_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.tracing_dictum_id_seq', 11, true);
            public       postgres    false    228                       0    0    tracing_turn_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.tracing_turn_id_seq', 28, true);
            public       postgres    false    226                       0    0    vulnerable_group_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.vulnerable_group_id_seq', 1, false);
            public       postgres    false    222            #           2606    16418    role ROLE_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.role
    ADD CONSTRAINT "ROLE_pkey" PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.role DROP CONSTRAINT "ROLE_pkey";
       public         postgres    false    199                       2606    16410    users USERS_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.users
    ADD CONSTRAINT "USERS_pkey" PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.users DROP CONSTRAINT "USERS_pkey";
       public         postgres    false    197            /           2606    25039    address address_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.address DROP CONSTRAINT address_pkey;
       public         postgres    false    209            A           2606    33393    cat_area cat_area_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.cat_area
    ADD CONSTRAINT cat_area_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.cat_area DROP CONSTRAINT cat_area_pkey;
       public         postgres    false    225            )           2606    16791 "   cat_modality cat_modality_code_key 
   CONSTRAINT     ]   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_code_key UNIQUE (code);
 L   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_code_key;
       public         postgres    false    203            +           2606    16789    cat_modality cat_modality_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_pkey;
       public         postgres    false    203            -           2606    16731 $   cat_requirement cat_requeriment_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.cat_requirement
    ADD CONSTRAINT cat_requeriment_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.cat_requirement DROP CONSTRAINT cat_requeriment_pkey;
       public         postgres    false    205            9           2606    25084    cat_status cat_status_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.cat_status
    ADD CONSTRAINT cat_status_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.cat_status DROP CONSTRAINT cat_status_pkey;
       public         postgres    false    217            %           2606    16690 "   cat_transact cat_transact_code_key 
   CONSTRAINT     ]   ALTER TABLE ONLY public.cat_transact
    ADD CONSTRAINT cat_transact_code_key UNIQUE (code);
 L   ALTER TABLE ONLY public.cat_transact DROP CONSTRAINT cat_transact_code_key;
       public         postgres    false    201            '           2606    16688    cat_transact cat_transact_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.cat_transact
    ADD CONSTRAINT cat_transact_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.cat_transact DROP CONSTRAINT cat_transact_pkey;
       public         postgres    false    201            3           2606    25058    interested interested_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.interested
    ADD CONSTRAINT interested_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.interested DROP CONSTRAINT interested_pkey;
       public         postgres    false    213            G           2606    33471    log log_pkey 
   CONSTRAINT     J   ALTER TABLE ONLY public.log
    ADD CONSTRAINT log_pkey PRIMARY KEY (id);
 6   ALTER TABLE ONLY public.log DROP CONSTRAINT log_pkey;
       public         postgres    false    231            =           2606    25100    modules modules_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.modules
    ADD CONSTRAINT modules_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.modules DROP CONSTRAINT modules_pkey;
       public         postgres    false    221            ;           2606    25092    permission permission_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.permission
    ADD CONSTRAINT permission_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.permission DROP CONSTRAINT permission_pkey;
       public         postgres    false    219            7           2606    25071     request_audit request_audit_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.request_audit
    ADD CONSTRAINT request_audit_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.request_audit DROP CONSTRAINT request_audit_pkey;
       public         postgres    false    215            1           2606    25050    request request_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.request
    ADD CONSTRAINT request_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.request DROP CONSTRAINT request_pkey;
       public         postgres    false    211            E           2606    33458 "   tracing_dictum tracing_dictum_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.tracing_dictum
    ADD CONSTRAINT tracing_dictum_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.tracing_dictum DROP CONSTRAINT tracing_dictum_pkey;
       public         postgres    false    229            C           2606    33439    tracing_turn tracing_turn_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.tracing_turn
    ADD CONSTRAINT tracing_turn_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.tracing_turn DROP CONSTRAINT tracing_turn_pkey;
       public         postgres    false    227            5           2606    25063    user_audit user_audit_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.user_audit
    ADD CONSTRAINT user_audit_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.user_audit DROP CONSTRAINT user_audit_pkey;
       public         postgres    false    214            !           2606    33531    users users_employee_number_key 
   CONSTRAINT     e   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_employee_number_key UNIQUE (employee_number);
 I   ALTER TABLE ONLY public.users DROP CONSTRAINT users_employee_number_key;
       public         postgres    false    197            ?           2606    25111 &   vulnerable_group vulnerable_group_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.vulnerable_group
    ADD CONSTRAINT vulnerable_group_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.vulnerable_group DROP CONSTRAINT vulnerable_group_pkey;
       public         postgres    false    223            I           2606    16815 *   cat_modality cat_modality_id_transact_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_id_transact_fkey FOREIGN KEY (id_transact) REFERENCES public.cat_transact(code);
 T   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_id_transact_fkey;
       public       postgres    false    2853    201    203            K           2606    16838 B   cat_modality_requirement cat_modality_requirement_id_modality_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality_requirement
    ADD CONSTRAINT cat_modality_requirement_id_modality_fkey FOREIGN KEY (id_modality) REFERENCES public.cat_modality(code);
 l   ALTER TABLE ONLY public.cat_modality_requirement DROP CONSTRAINT cat_modality_requirement_id_modality_fkey;
       public       postgres    false    2857    203    206            J           2606    16797 E   cat_modality_requirement cat_modality_requirement_id_requirement_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality_requirement
    ADD CONSTRAINT cat_modality_requirement_id_requirement_fkey FOREIGN KEY (id_requirement) REFERENCES public.cat_requirement(id);
 o   ALTER TABLE ONLY public.cat_modality_requirement DROP CONSTRAINT cat_modality_requirement_id_requirement_fkey;
       public       postgres    false    205    2861    206            L           2606    16776 E   cat_transact_requirement cat_transact_requirement_id_requirement_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_transact_requirement
    ADD CONSTRAINT cat_transact_requirement_id_requirement_fkey FOREIGN KEY (id_requirement) REFERENCES public.cat_requirement(id) ON UPDATE CASCADE ON DELETE CASCADE;
 o   ALTER TABLE ONLY public.cat_transact_requirement DROP CONSTRAINT cat_transact_requirement_id_requirement_fkey;
       public       postgres    false    207    205    2861            M           2606    16832 B   cat_transact_requirement cat_transact_requirement_id_transact_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_transact_requirement
    ADD CONSTRAINT cat_transact_requirement_id_transact_fkey FOREIGN KEY (id_transact) REFERENCES public.cat_transact(code);
 l   ALTER TABLE ONLY public.cat_transact_requirement DROP CONSTRAINT cat_transact_requirement_id_transact_fkey;
       public       postgres    false    201    2853    207            N           2606    33440    tracing_turn review_area_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.tracing_turn
    ADD CONSTRAINT review_area_fk FOREIGN KEY (review_area) REFERENCES public.cat_area(id);
 E   ALTER TABLE ONLY public.tracing_turn DROP CONSTRAINT review_area_fk;
       public       postgres    false    225    227    2881            H           2606    25021    users users_id_fkey    FK CONSTRAINT     m   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_id_fkey FOREIGN KEY (rol) REFERENCES public.role(id);
 =   ALTER TABLE ONLY public.users DROP CONSTRAINT users_id_fkey;
       public       postgres    false    2851    197    199            �     x���Kn�0���)�l7����KF��]ب�(R&�Hi�o�3��X�4E6�Q����>g����a@�8r)J���M}x3bO�́��T���("���R�+#,}/�C	�,��5h~x�vz0�$M'1�W�*s��2����{I&ACc�L�Œ�di6��%a;����[IsA��@Q��G��'���4q�Bt��D�E�H$���k�c�i��}����`����*-J���i��(�y��iS�0��)_(�wQ"�(p�@�&��Vz98;:�)G.M�w���Ґf<�Z[	¿P�f���ҰZ��CZ!�L`u{�%���Y�z��W���Q8�J�.қ���#��F#�ہ�ͥu���7�?��ӐGs��^N��k��`S�w�I���wqg�j=.�kz+��@xO5	2����w!�4P`���O�s����M�֫���mV^N�{w�����q�������AkA��_����`����      �      x�u��	�@D�V���_Z�0
�U�lC -唒ܘ��ܲ�a�9Y]�u{W���S�,h�)��b���b�x�jp��r]|{eej�dI�z꜇����πs��|Ӫ1;}&�t�����YW-#<G ��>�      �   �  x��\�r�F]�_��q��� ��J��Uq�X��S�Mh1�� TVv�'�ٌw^�?6�v @��-*QM
� м}�>�oC��9S���JD2�YʬU��8�Y0bv�f,1;	K����2���_�`�`J\e*�)�qCב�*�9.�U���"�Tr�Ș�-*"/�"��{�2/D�1��'+)�L�H���W��0K�
%OP�q��e/q�Gtu����e��O9;�e�#��r}���0�Hs_�"��<�l���mT�},�Hb%҈�Ȕ���r�1e?sH���,Z��5�$c��	t:��B��c&j��JA<&~�$B�U�qi�HͰ�XfD�F,c���R`d��F#�Iӓ9Z���~J�Ӳ?�G��N�ހ@��ܑtd��ڊ��܌Έy#r�){�xXy����#x�sM��q��Hc�H�U���(���+�ű~|�Z� uZr'��>��0�;�J3I�xK���֝wo�s��k��y��4�┄F�,�^���Q�M+F������^�H@�=LL�BB˞�T��/�m����t���y�t�*	�w�./T"�Vx߰�e�˄��|�������<w[z�f���Z;�_&���l}�$c?����h� ���xO�����]�،�>�)�����m�5��_%1_�ޥX��ΰ7�M2�d��i��&0|'SQ���D��W���7�E��,T��̂ѽ���'k�O,�r|��g�c�ݤF�N�d(0׳�����5�<*��+�᰼� �]ATr�G�~c Z�,�ʾbo��8��^��q���'Q�I�`y�2 ��>�Y�{�P�&�)V\3э`b%��R�賛
�׈llU����&��=�o��D����8�T��V4h���J�G��$Ku��A?+(pT�^�_1m���$�$u}�����=(��]�C�yb��*㡞,-��C�MJ���	K�����=�p�Dc.��D���6"��Fh��P���)B�`#��2
��f:��8������e�
4	��C�#��r	�ļ�:q��V7Z1�G��i�8>b�`u���ot����>���T��Z!D��i�UY�܋2W3}k~��y-��#� Pz_1���Ojf���,@><�ʴ����`����զ�0e�I���ЕG��0�.���,ַ��E �<]�$2�I�ٕ���v��3u��6Ii;��y��y���F
��S���Z\TT�욌}$!��2E��5��l��fҵ�1e4�o�	�:fסZ�Ds=�ؚ�>9�8���z-�&���P8��_ʌC�?��C�����g:7.iLfʸT^Q�k
����p�q��Ҕ�"�����z�y�NUt\[�]�u��l��Nڎ�n��oF
�}��e��.S��٦H���1�>���4a��Y8�?�hdޮ$)K��w�麠�j�]Ҷ3_S0�J|K3�E��aO���	|��� 5�Ղ̱�x���~�"�Q����m�~6�׶� �R�񋰼���řv<�2�w�A����X׬����42I�tT	>��>$w��kR?F ��)6�Һ���
���S�q�z5N+~)�V0�Qv�Z���5B����Y,xQA��)2�ר��T��K�(򷩋N˸(U�2ÍG/������߈6 �R�5<$���|aM�4>D��r!MH����X�z�wy��BՒ&�te4�����O��_eić�"��W��	�1�η���R�����J�b�f
�N��j� �`}[e�<[(a���?��T#�dq��M��ZP5�؂��s��[����%J�Tn�b{y�����f���u��7mu%V��Q��Z*)S��0��Kڃ4[�<^H�ր��O�^�|g�Y���u���bv����$�t��w��۟d��vq��&�^�Ӻ}3�� ��3��S���q;�p�	����<_	bk�X��c�ջ��~?p���g���O���`�IK�䉓���O[.5"����f���Ʈ�GЋ��S�W�9�G��)9���&Gڧ��I��^�&\��9�9����](V���4�����H�!���9}����=M6�-�:(N �m�4�{��XP�Q*f=r�	���]�Ǒ��΍z�V��,ְ|M]�(��Kj
�J��aY���T�D�wl��޴3b����{���i�Lj�h3��iV7@����L���4^x�6�	͹�=<��n����i7`Vz�Är{Z�f�T�Ħ��#Mk9Pиc��S����ME�����ϑ3�vc$�$��%r�ʵi��aE]�Dƒ+}n��R���aeZ<��/���C-�;'�Q�~
�q�F;	߃\p�)��D�A�7����s� R'�2��f���
�ڠ����t��������23n�mI��)��EI�������rH}��~��7��Y#'�K�Ի�.�	�aj}}tyqzd���������(����p��?
H�1���%L�"���J+_��X���' Y�lj�}?���t,�؅͘Q�v��G6@n���!7֐s���v1�轀�ze�����l�e�aH8���8����J�̼XB�%3$	��)L�=*�,�|��?V�H��˸:y��SR����������1z�ΫY����<-d~z�I�2�7��q�h��-�ڀ˛���q��Aؚhl����$u�R�������I��$�V-O-Ͷ,�F�?֋�o�|%���t�<h��x��=p|b��뎝��݄H��IWz��_����m����|A�~L�426�1�4�>ȩ�w�~�=j�S�]��Q��=��ZF1o�c�zW����#�;�@�y�ls���>���|ݷS�w?��q�B�ƺ.�çũ�.07h���}��+�m{���~}rYa� hj��"�D�.D��ncD�E��a�ʕ�s��LG���
z�<h^P��X0v����?BP�*12��G�m^��c?{�C��6u����R{��ڭ�^��7U�|���R�����15�~�q�o\��8�����b�v!{B�~����O_�\^���3Ag��K'��Wr'�l��=����|�!��擦|zS���}b��/	���KZv�e.=�U�p�9�k]P�q�v(x�ހ�C�G��b�N���M��Y�2&}ˑ�K<���U[^֣��)_�+H'sv�N��sO��s���<jj�>���J��2�5�]�?���7����ٳg��/��      �   �  x�]�K��8Ǯ�t��-����և�7�O`�Jds�]��W�-��<}G<[�q9G_ߙ���c��G���ǣ?�Zs�-ϳ�82�c���׳n������U�ٵ:�<޽�i�"˞[��-gl�gl�r�����m����c���y��+�{|�V[�չ�?[�q�q��$s���їW�L��f�9��h.����-7�mϖv�&�j����L��e��{~�{����o>G����Z�G���k���Ou�>��x����go�x�ɾM[֑������[>-��S����z�)�-�m�se�j���[ږg�����s���wJ{��yn�������~~%g^�s��c�G_�÷{��sY�������{�ڊ���� e����S�)V�M�ۖ�"�8�,Z>�A�hy���l��m��f��v&J�F(���)J<��չ˵�,O	��|�N���:�(�6��!��.!�R�k*�l�(��ʐ�,�{��Z~?��R�����V��ȝ�'�������"0�r3��U_�|C���S�7a)}���X֧�^�r����n)&ťh�璢��Uڋ#��wO�&) �J��l�*�w���<R^)C��\ڢ�+������q�CS��*��Ğ������Q�Ɣa:�L���EM�
%*K��i�"s�$`O�~����[��z�EL���XtP��:�nF������t~�S�c��E{��d�^��`����|�����[��y�HyJ��W[�gi[^�z�`���ÿV��?eho������K��AaJ;����L%E)�/q�0� @�(�:M��2F�5��i�Vk�Z�����F�5:��z��k�\����h�F�5:�т�n�^���׮_�U6:�]?:d���kt_����h�F6���pw�;��w������pwH8p*�T���S�N�m�*m��Cۯ_���C�/��<��%�w������pw�����{�=�phh����>��{�=`T �@�=�p��5X�h�@P���s�y(������{�=��pO�'��	��{�=��Nh'��	�vB'�^R��	��{�=�T*�@^����{^ƹ8C(��'��	��{�=�^p/�����{��8aQ��E�
d�Z܋�U)*PT��@Q��E
�u�V�q���e���CĽ�^p/������w�j#��_V�Ym#���V���aw�_���}z�����y������wc9/�eZK���^�Zr"��C������/�3�      �      x��}KoY���+.�0U|�e���2K������Ơ�F���2X�`z��^��w��"��0������{#��dgN��PU�D���}�3z��"OL��"1��r�����j�Uy�o~���4�-J��e��*I�(+�>U��0%�+Y��_���y���ʦS��l.`��������
F��}[�n���{����y���ʹ�-�l�͗I����[�OYm�w
S%Ӽ�ïp�E���i>�O�gӤZ��~:�:�>��ƹO���wEr[d��`�JҼ웻<����������9,4�%.�%�=[N��jG����*�o�n�i�{��x0�|��`&S���U��
�y�(�O�6u~��i_/Lr�����`�ER�6͢��9�j�8*�m��h��|��a���1��������V�����"O������񒾁�,V�%�f��Ws�
�'�N������9�{��>����Op�5��ǆ���pG%,�LaU�����Ug��
���R��e��V@[��̀ڀ`�vq�x.2�����d0�[��)��=�|s���i�	&� ���q�*�d���f	�v�ۙp��K��GR��p�~яX3M>�8_£)�C~�T ��]&ppd��S�#�.���N湼��2�Yi�C�QV�40�*)`������毫��K�	X���J�[#y%\GZ�F�uY��.3~|�eI���K͖D�4�lj���[<H~����Sx���%"�gV��p��_�>�]^O��������	�'��w�U���.i��|Z���rN��:Fe~��opCoWY�.M���.�;��%r���i���RM���o"_��{=K�������!n<o��N�2Y%�#���Ռ�)��x(���C�E����]�	XV���!��l�?K�c�P¨g�c	��a�}�T�j�ˎ3?��fp��ƳzZ�%S#���\��H��#��y
[Bz(�l����n��k"$bS�>|�M�;F�"Y�go+d����Lr|������ �t�HH
�fU���l�dV���)���g�6���+�n���ۇ�Ȋ�y=��\'0��ŴX�ao�3Z-�-�q�<���{�����NC��i�
x�:�HVy��	��_��e��{��<�+��k��F�6���D���ZX�`N���(R�,"��"������IΗ�A~JO�-G���[�����%0	_�b^�f@Rr8�%�cp>V(3��B�1�4̠?m�X厉���Ӽ��
+����$Z
��'K����ڰ8��)s�Aa0d����]$���z�O�g��>�	�L�Ñ��\m^��*�/�C��Ă���1H�|�<�D�]3Έr��'�(a���1�q��*�ڙ�ɢʦT%xda(�A��%�-��q��WnO�gtpY]�eiY�՟�3������ ����n>�+�m�;8O� 8���~���];I�a�w����*v	mX��25�d�%����*�%*� Z�7D�b�!�=
�A�w�b�y�GOא�� 'ԧ�\P{������%^���L��̛_�$�9���6wr�7���sf�S{6Nu@V���~�dѾ�}w�"�\��i�O)�"�`�;���(�d]�6�q�'M�3к+""&n��n� np�BQ�/�أ۬I��/�W�W�̎#��nȰ���Rf��M܉��N���IZ�Do�ތ�����=�,��)�?K���֚��q���?$̌�d�fk�K�,�υUh�����]��`Qɧr�%M�\�Q�S� � 3�c��<�ô��c4��\�lf���d��{��m�Uy��t��a	�5M�-���@/	�F��R�^1-Q�o?�m�6���� ^Xd������ߐ́u!g@g����Y_<Ԛ{ݪ���YvG�nC�q:�ϫ�b��z�[���y�:���ɝ���K��<A��O���n-Ơ�����6���Z$u�&���Hӟ'^�v�I� �������-�n� �}j�sS6�|ܹg���Ue��\�?�ӂL-#%�z^�@s& �a	�Z��$�<C:]�<N��M���]�U��I5�v3�t��g�D$'�؁!�`n�4�TL»�$ć6x�$�;��~*�e��	do����YZ6�Z6R�l�u�$��4��آ�=J��uX��S�{7��`5o�"��[�I�z)� _n>W����|1�)ڗ0Q�P�� ���L�d��̉n��\�n�O{����>2/d���P`��
Y��,� mT��J�t�� �cRl�=�Q�����l�t+��h)�6�?��[ޏΜ|��%p
0�n+e8j��}��gP��q��].��v5<ݾ-����,�W~���N�Y�f�s�x��-Q"�Y�W�w���s#�84K9d����k8b��:�Ȩ��l۬�S`��m_���h���v���(zai:a�o�d`��ϕTFy4[ͽ��H|C's�R0�K^�S��Bq=�7侬�9'���ހ�Dr��
�~lg����=�۫y)��?]&�� E�@w��j �A.�P/�@���[�	9��YK;�mfԚ�5�<��{r�'���䷦�3(�Ɛՠ%l4��Ӿ#0J�;Ng&EI<�[%,�3�RQ����e�$u�f��w��7��a�Q��a��u����e�Y���u��Ybl����a&�K�Ϫ��p�v:.�#V#azP��$�&�[�ڑL\�D��6-��^΅�X���;�\o5��lzo���������f�(����wA�US$�3�P0�>�UM�E*|y�of����gl�g�Cbv��B^q�^`r���e��^��:;Of�곕KD?K0����\M-x�xG�=�0�P�õ�[��g(���$���\H/�,�iF�5K����+��9����z~ifP��U̶~pkK�}�'��kk>	�M���,q<���z� �cJW�l��g��Prѐ��=��]'��̳iV��7b�[V�c�6wd�t�B9B�B�J!^S������>8�n��䶒 �)YQԆ�B���(��=O���6������ϻ��[�_P�._�Q�} N��ş_b$m��q8]���}VU��B�RP�k�����ڔ]G��k����X���2���Y�8pԐ�3�e~�m�7�@+\"��o2'&�)��u@�Ƈ"m���H��{�њ���E�*�|x\��D����Fe�c�
����ˊD���D�<�}b7��1�OkZ�Fؘ(HDO��PE+|���	��q�����ȫrI���ܱ�Wy<������t���:�w��y��!K�b�H�a
|���8����(���J�-=�Q�9x�(9�1����%P����R���7t��x�]\����k��Sx&�ޞ�	������:L����:½��}��ޥ��-�mo)��=�D+?�R���ͨ7A�mY�;��m�a3썍�֡z=KP���%�بw���5陔�/)nxnus ���MsE?ݦ�O�ӌ�x��6���s �c;'-Aͅ��W��̃Y��sK�H�fM������QbH�G��(ג���D���l��ҝ�&Ef�L�6��wm� ��5nu4T�e�._��o�ť���y�n�o篯�����]�\���|��\h?�{n�{��
9�u�.J8�'���w���=;�fiy���P�^�y]���Mߜ���;��ө�o��r���ݑf�5~ �i�!�:n螄+�3 "K��������З��)�':!�"aG��8Y3�d��=��	��^��U���D�+��9?�������?���p�`3����4�-�\���8�ƪ���=�l�s�U3���dB�`j�,JrA�C6�œ�ӎ�=7�B��1��Q�/ ׬x�z�"�Ǵ�����w8����E�p���	OT�p?U*�,�/D�ű�_�![�do}4�Zi�u���|�T���
��9�.��HP�    L���_��=���޳�ݕp\Nc�\�.]վE~��[�3a)�3�� �q�Bu�m\��V�6��;N��i}.��9ő��p�)�gm1��Mi���=��&+�L]�I�X��d���p KIX�ކ{�w�M
R��x��Fp"\�6.��G�Nݒ^̜~@K
Wf���%r[''2�t����p_n��@*kF��$N^`�R*p?LU�	>��f>ڀK��9�%i�2��U>'��E��=��B�̞{M�Sm�f���ē����z��-�a���wx�7A�r���������F(y:-J�[�B��A-�y��ڷS�;<q��t���S�O>ɣAd����b����뤀>�s�8�8s��߬]�9��D �\°Jw��i���O�h�E㒅��A�G��>��5eI���,5S�F/-�cwS�S�0�H'I`3L�v'd�HUP�9콱�ʻ䡬T�����2hP��@�OU�MB�S�3��#�Gц�X�9��=���v_G�]QC��\���Vrܻ��yYze��\ ��|V��	��
�Ibv�{�qr0�O�n�h�c���Ԅ���d�%��h�=����*:��G'�X�,(�	M��iФ����̟A�Q��3�ly������f����+�6+И=���'���+1\б��F�����e��!�]Q�+SB]�j���ԙ5[d|7n�RP�sm���?�S�Sd���}��?�mYB8ިVE��c����� O�eج�dN)��_�
\V�����/s)Kd�K��*���0}�S)�v����t-��6)��/�2�Ϲ��:3��sO��~���><k|Ĳ�����PX�_�bQi�c��Έ�tT��I@��e������//�t����{Q��Aq?�L�*s�=��'J	�t �@>$X��^2�U�?�E ����&�. 7t(�V�T}$R�-�������|To���PT+Ύ��9�ܨ�`Zl�P���|ʭą=e�I�1��; -�dJ�sEnF�* B\�q�0�VL��v1�̹׌��S0��;��a�L^R E�l�z�cRK�x�
��t�s���V�qڻ�|�����Ϳ�~a^��zX�aå�I0�q@L�*Z^q�,B�Ɛ�}������܀M����Z�w��+Ka���2/�x�D��D?��w._�8�7��_�ܖ�
6F6-�4���s�("��B`�%�Ȟ��LY:\�k]�����䧊��J*A6Y�6H��GJJ��C��F��	S�O�^e�:�*H2]ͷ窗�oa�֤�>N����	"/ߚ�ɢ�;�+����^�Ry��������2q�Z�e�sT#�w��pr�c_��Q�>�=�3fL���x=;8~Tx�:?\�7��,��#�@F	�����X�L���Y�Wt�	\�S''� �I�,V�ws�I�Yv�
j��fX��ϻ]Y_q�W?�)��-�ɔ��ʗEAR���M��p�P�l���b<�w`�%J���Qngʓ㔶|a+3�Շ��/�����C.G1�S����JurU\�(&�K���!��(UhX��JKn޸�?F9w:�6�٦`Q	S���v�!� ��e�*e�q�+�]�K6��7�.��>pN�
+�|�TꋀQA�<�����TT�s/�fK!]T����X4ܓ[��i���H�e!�k�P��
۶��4.�đ�w����8M�9c�Z)"�iQ�/�KJZ�M*?YtNddEO�}�l�a23#[ �N��N�圪e9#U������W�����S�
E�-qD�T��7� PI��A+��h�4:;�:�.)2����0�1�Y9͸���=̻��5��-V�ϵ,J[=  	P�$+ï��*},#3Q���bJ4|��I��=��0ƌ^JV�o���"�dM[vlV� ٸJp<����Ju�URz���� �����8P��h���5%׺��U�DL9Q̖۟ĵ�gN�zj����B4us��?�)�5=$�kU3���l~�gd-f�=����NN���ht�l��U��b\�i��>Y��������W Y\��e�&A��%طU�ևr���ǆ��ݦ�Q��z�,=R��'�H>qO�.[5�v[O~bF6��p0��s����w���2lՁ"�t��]R�J�6�i8h��x�֐Qr���4�٘4vCN^��i\
�񔶉OS�>�]a�0�fT��8���s�Q�����jE��<܌��]�:��S*0]���.��NU|qA���(92%��5���!۞&Hx��k�t͕�s���UJ��4���q )�X7�3x�Z(pQ��EJ�c^�y��`p�y����7�<"��a��%�R��f0��DA0IML���3�a��u�-�d�v�t�`�����"��jH����n
A���J,6�q�o�_m��	�H��ͯb�����60-{=��JKi�tkb�%�{Ǻ�����J�t\?�� x�A�Ջ|�Z��*g��C~��������d��8��C�1��J�\��S��\�D���X�H|1Wma�_5�!�YM`�S�h���h��@v��S1+%��1��`�SNs��'����U��0S���wVӲ�R��f
�ᰠs@k�6lps6�ą�j�:�Uƻ�V6��+DX�z�xnoVb4��gKY���}�v#��b[�z�P�D__Y;�ZY��3h�M?�?���~Cs���0g�c��oa@�C�����T.��X#�6q�V�aI���}1辗2G��%@Y��U�C�Thw�"�D5?\��8�ŕu
�>�{�����{�z��s`�w�v����'�%J|b(s�"�-+�O�\�5��������2r��A���ϥNwC��N]�x��D�4�JD(k�`0�[֙3B�*��)!�*j���mO9��aL���U��}��@Ҝ�@���5���Ý��w4Ф1�S�)��6Ϭ�Js�!��	xJ�Z�̧�0+������+?��v����`m��$mS�iѫH���h?G�Ѽ�g�EVQe#�YO��Y5���]!�xhIϼ�TK�|h��pݰ��*c�����c@%�:v| ���g	:�kO���6�	T���7��p��j�3P���*�h=��^�Q�R~�ˀ}N�"�i�`�g�"D��*4kuךSտ`S�߰�_*�3҄ ���D�8�����F�}'n7kc��q�E�E�/�~��X�t��8@Z��J�>����@W[d�ھM�P�=)+2����=}�? 'HSc�Q���ǹ]V�]f$�=��U=We�Ʈ��Ja� A-)��0��<K��D�s%>��yٸ! |�y#.����І�#��qj�߬F�) PM#��M�B�y����qa�i��b�l�x�o��9\Ր��_�W��iSe�Ա���xj���>
�c�DW��W��H`̭k{F̐�S�c�ӡ�s���$������)����*�c.��}��T|z#&n~���\�a�٬,6���$I�Fr<~��J(�C�,��ƃǀ��{�+��u%L���'p��|��Q
"n,nj�U5�����ϓe������ѽs%5��F_u8`�Sj}�������iK�/���ڭ��؉�6�wx|ڻ�E�Rt�>xC����cU��ҘY���c��U�,pۤ��>Q���(�B�hC ���o�^I6&��B���$�귕0�M
ԁ%�kC��~W�NhZ�0Q�Km��0J|��,���T0汖�d��R�ۄ��c��c_��x��6i�,OV�h�������
\�����/8y�����H��Ʈ��H5a���%� �������ش?,YH�{��I�?�pr�
�N�a<�L쓴����-k���HTc8�m�lyb*`nmuñ��AB4l zlzL�OE|~�H�l�e8�ߝ���s/X�'����,�f��K)EVٟ�(�����`�?���Q�5l8[l<��J��6��`��s�O�!����o]A��0���-9u���]Z#"K��r)y��z[��3.�.�wG�C����c�sAh�aB�Z��nx�,���s)[ZR��s�A���q6~�6+~4\�/�]
B��6� P�&t�x'^�v�W���� cI��J#    ��T*yF�-7�c���=���|�?�՟���l���ז��/y�6�5�tr2h�`�5��<���� ~g�Z����n�Wwǧo�%Ц˴ǒѮ%�v�`A�ZŭC�HfI
��8�eE�kL��VAn:}D(� �ķm�F���R��l(������6��bt��4ݶ�E�J**-)���4������7��N��h3郙��H���#v�a�0U����#ya�`0��%�aL:��*`-ܪ���s�UƆ�yrK�����b����GX��'zp�g���[ݵ��ƃa���__�E�!��KBVq����nxՖ����5q@kg[g�*OW|�6tz8	E�Y�|�qH�0�u�bnu*k�rӥ�<ʱC����⺽ɇ�=}U��:T+S�o��}. �jUB��w�X�7�5�B�o��]�J��+͵�S><�Y'o����^�O��>q�&�%�+�8W�L^z��vD�оή����"�<`*/��d��=t]���b@Um���u ��ˌ�*�'�ŭ��a��N?s<���gT1��I}{�(x$6��*i�h�T�g��W�wm����ht�'=��Ũ��3��}߄���K�(k���1������v�i�6�Y�į�����3|	���F-tXSܨ��㪙溆�d6�~_	�$┾���*���z�i��f��  ���*�Č0.�1K#5bĩ��n�~��U�ƌ�,�:}r2���-�%�Lv.,W˶ѫ5m%-mm"Q��$��$��CVy(�6����NL��Խp�(�MhN�@���jT��IC}� G5�c��-.P_���Y)Et�썔���R�8�}�<�(o��K��Y:���� ��E���HͲ9�Ks��@�Ya���f�v�mʓל����6M�}�!%��K�-�0<��-0JN\Gl��:r���)^�J���%C�8x�@���D�vI�1%�pwJ�1�'�R�0��t�.��u�k�[�+]b�B�9	�y)�i2xZ��A�EH��&�+\o��[��|�Q�.$)��Q]�T,�q�ڱ̰~x�{�s�/,u�@$���o�a�cQ��|�H���&І�-�!~��k���sK��-=�l@�� �66#::�}D��be���9m+ߟm/ŝ�膇I���� 2�ʶ������(>��Jp� Y�¶	Z��U��n��ZZE@��ʥ���qMt�'#T]���m		{g��9t<a�M5�]�Ͻ0W�5;�������V�x5�����E�1���us4���A�R�x߭��.|Ꮪ�}v}Z Xz�Ǔ/7��z��u��H��3�(���~kE��^{�+�����L���J���-o��*�-,�&�Y�q֘�H���ס���	�}9��� @��x�`�3m4҉(�F��D��X�-la@��N�#�:�@ÃC33�&����f=H;s�'�y�:|�Tw���Z���6f"�yZ.�U��1�(h����yi"Ź�����qE�;�Be�=�h���!�����Hܒ���tO��|�<J�0���7�\[A�o�����Pkj�tp�T�~KM�c[�����K>د6ڟ�n�w�bE��ἜS�	�@۶����O��ڠ�3��V�x&0�͎��&fP�nN�\}�	��g��0��	#ݺ^d�>%Ʌ���ܤ��^[�na+���V�Y����Z����b˕r.\��ʧ�d��t>��B������*��Y�v�͏'P���ƕ����O�#V��k$.��Ts���U!��y�/�i�(Za�Q�2��y���K�RM4@�,N���-�coA���û/A�'ۚ�!�Pt?	�5Qَs�R�]�(O��$�@M�14��V%�g*�خ~r�sJ�! 
T^���;�:��7��(�2���_"���5f���T�F5�Y�����ጅ��[v���2���U2��c���CST�C����yę%��Wɶ��jB��\ [i�ݪ��5n��dK`�*PY:F�B����ZЧ���{��d*��ŊG�h3Y!�S��|�x��曟K��l��3��̸�mI}ߴ�c�r�o�z'��9���v�.x"������b�a�v�E�p��]lѨ�;Jт��
;�1�e��t��=�p�]��1��p�Gw5S��Xc��˸�Rb3y��a�:�2s��z�x�Lj��a�0�Ib��N�zg@v���1�� �`i�e�6�&"̎��.����1�'95d	����&+�M-����l�sغ�PE��l.�F�$��}�F��8�m�Y��Sa�$�f�����MYJ� ��s؄8n`d�ä��/�&F�?�I|�|jU��N���C%����?uzo-� ��F��2��"�ӧ�y��>�� Яw�������l��{3
;���c ߗ$�7!0L�V��l���b�O�NFz:vyi*��o�ټ���%���gT5�{��('�"�n>�֟m}�������������������X5��^���Ro4(.ċ����d1hR�����ML�S~d޿_:~5�Ɣ�ٽ|�
c��^�	��v��QGt��U��pd)rG���=Lգܯ��${*��C��8���
yk�I[:靯(��;���db3�\�d�1�VN-֤��1�S{�'���ŏ��������� J��b%��O��&���e䳠W�bK��0�L䋫��g͌P�ZL��X �� �u���o^ed�7���a�̧������=��F
�&�{U:B�rLN7IA�i^JF�if�H�'���6����4��K���#��WV;�BP\O�x�Ӿ�He���C�	ko�3L1CKL��5�)�i"4��k'#�`Sz��.mf�'ѥ1os�Yԑ �����Ι&;�6u+��N��bQq��J�íǋ٧3�Dz�Э�v��2|��{Ӈ��Ɵ�2����`��-E��j/�h4hm/�8����D���[����h���Ҵ������_�0LW��z'�e��c��F+�p� ��Q�I̤�lJJ8���aٚ\g����������������=�p��Y�'%S�r��\b�9�^/�Z��ֻ��YO��q��]\�z�ܱI�	@S�'c���)_��2?XD^t�L�L�/�	$ݱ�C�4��7�2l1ܫ"�,���F�J:z�|S����y�G�����|�[��v��B����\��V�j<K<��W���NF�O��:yw�{��!�u�ܟG}T�K�1�v�n�-_ ��C2W%�{[�F�%Zb�����͜�l{U/3Á�O}^������Y��O���pT.�e�<4�/��뎎GD@[����K`�^�9��+�ѮC����39iw�r����	AF�=H�>�f�������x?�4�f킀 ��|>���N�ަ-2U�׬6��|$�,"#w�9����|5x���Dw�7Ap���8]D�42���=�!�2s쬪L�k���L��sl9,1
	�� \�v��
ue9�e�{b�䤨���,�yG��#'7kLU�k=aEe��GX��y�ä+*�N���Oz�*j�.�x��0cV<2��U���b�������b�z�v9��"p�)�\�1�j[�<�d�(x]��5'[�zB>"G/,h�*�Nwz��`�=Ad0���	���t�^I�-��U����=�x��$�!	�p
��՜(��=��Oe#�l�%9�� ;y�h4�R$)(��&HN��������<U�}&����!�ܟ��|��p5}���j������c/H�
}���Z� @l||��9CI�	X�!.|��τ����쥲�����jI���6�Y�}����_�.1I�c�����!�U���?"�f�g��On���R]z�wd�����)�SR�ȭбfCzhk�]ۧI9o��������7Md%T��6�{�����ٕ�Y?�	���d�v���
�gf]��)(�������K[�F����N�+�_��t���krB|Kc6�br�!����G    k_�FZ����\*l�Ra�{+{]�y/�Ԯ�=�-�+����J�����n�L�����n�!��t��)(/�O]� ���3W�Q�R�Y�B�o�j��$�����=����hwj{9�w�M���{|�H����c���T=Z�VEڝ�E�� R�D�mC���zz��0Mbᐵ����5	�)���,�^ �!���!b��� v1QEąEy�2��n��l��O=����{?䷼_�m��l�j��J��Ԙ��	��53�\���#r���Ԫ�ڶ8��}�.���j0Y�6�FQ�Z�2�B;���,U#j���@�k�!�6NtԻ�m�v�T�u�A��;�2[�2�F�v#yD]{�B�vq�	�+���z[�����Ю5���S.ڳaL�a�C��PO\�5\/�^i������"y=j�x=�����
�}�Z�����������@hǧ��E�5KZ�:�2Ne��*�g��C��}C�%��n���r�+��T��D�S���4�0�e��YT��4�Qܑ���2z��՚��K��x`��:P�T� L����oF�179aD�-y�L"���J^��k(���(t"�p��y���ʻ�_�q���.��ϲ�)��U!���:1U�����CWsB.�#���]֊�u�j�c���^l���-�5��D���7�G�(+s�bD�Mp�Kߗ�&�n,��76�q���K��O��u(+c��{��8Vh����"�.�lҔy���9g���������.�����;w��>!�ju7�am�q�$$A���2��9����:�����[��b��3i�����z�m�G8�������m�Z}��6<$�8J|��n$�[�֫�C����t�8�C�j"u�"�ñ�9���{t�i�	+����!�"؇��Q�f҈ؓ'M'�q��U�L4�v1	�+�ց��^ZGbq)Ս��2�#�٭jA��QVʙbeʡ��i`��8Kb�E�&R��GR-�������xx�5�P�n�Ʌ��Z�n5ۺ0땅�ЮQ���Q�c�Pc���p3�:ju�ҳ�����!l��T�x��Z��ԏV�����������s����Y�_c9�-wqH�L|6&�	��E(��-x�-�UݬM����ޅM�h $��A8�m���Ie���pp�&��8��b�9<,X�sw ;g��tH�wsK��ɑ�>:�Q��߄��@�C�FH��I��u)���f���/��0Q���/ҥ�a��ԷVh9 tT�<6k~K�w��N�BD5��J[��6��2.8߷���O27��FNu"JRFI���2Y$>\������%TN��5C�p�#��%.�G���ߥ�����q��T�54�u8�}��P�,˔(|*��e�)65fj{_r�����[OEb�<-� �g����	t��p �~��v��5�4��b,�+Uj	U����2j���6Q�{ʪ�j��ڊP���
~ ��� �8����ܞ��1TaK��oeH���j?���q�C|�=%n�x�b-�����&s~�i�)��`:�4����v�z��?�$�r�!"_����$&[iq����g��tP��k���O�0�?��b����p�iS�Q����_�mL��>ǵ�Cfm�OON��i����U�V�	)�+�)�5nMlƯp�>�}B8��
$�E�j~���';}�82n�f�?�OK�U�p��4��r�~i���_�s����l9�͏��V��1� �7�%�ȇUvOq7����n�fވэ.P�++*=%ʶd�}�p���-���R2TQ��ȲS$.��|x�P��QD��@�G�%D��[�	�s����b��30��>�NO�����`?���3۔G@��z��`xۖ��k��wA*<��vQ��ڼ�(^��>����w��h��!0�G�S�lx+�є��ʛu`66���u�(k�K
s�Wϱ�1��y��ƽ���j'=��y�q�	c���uq�k��|�1�����;�-���0�j����7��[�h�s"��?�U�Yь�(���G���Q�6	�6%�^���$gn��0O��#�Sl۲�Kݙ����0A>��ȗ@��đa�3�g��Bt:s!��Y�?�]"��KyE�^ҽ��GM��ǥ�v'�*�^\��C*�)	]q�m*r��5�m� ����Ч.��缐��HH)HN��M
�=�}F��mcZ�0��֥�E���,}kg���EMz�&՟�\��t1:�؆ i�d��ʡ߂�ȎGN� �I��Gr�5��q9^�M훳)���Jj�<o���cb۔����tV��8k�ia���C�W����K�%��B;���ֱu(���s|f�M?(T�������N�Y�q�l�Y��d*nS�;5k�Z����('��:Rܭ��=�cA�+�[����V�1�l´�Z�(#��p��Z0���f��j_ ��o���q7 ��ɕ��kq�����&��pwqj0E崝�DP��?�OH��Z�ha�h��̭��r���힨�r�_�p£�r4���%��,å�FQ���^��v,!�E5Jѽz���:'��ʁ�ѣ����@`+��U�{��^�>G:hL� \x�~�b�Je�;വV�}�fZn�b�]����� hp���aϡ���a.<��<.9s��}�Z�H.�Wa��Oqؾ�.���˧)��=< �����	Q�0`�[Q��J�gĭ��Z9�=(�XۨzV���=�a ����q�IZC���V0�ʾe�����]��Ĭ*]dńCBD4A�Ax[�@P��G��Y�NX��cá��M�*Z�ݷ�Ƕq+߻�v�O�b�1΂(7g��\������n�K���^C��{���Z�ӍW��(0�mp". \���!�uxǁCw��ZW���x���!�$����ET�nN+T`Oٝ�h[�I��/�R�e�_/uvdУ҈!�Sa�H�AR g�'y�vx8�{��cK�uJ����Jר������@i������N�b'1�DRd9GR�%��	8[������tώ]�5A�4u|AEp?辡��$w�z9Q��\�0�o73������HL���T�')]q!��q�6l�2 ��'���HbA�Hp+ki��-I���p��.
�:uvk�6�6�����#�C\��A����hi�����0�4Љ(��5;
��UV��Nw����ƃF���M勌����p��SW<�A�N�T4r��,������P[Fh��Ȟ
�?�Sd�JH�y�#�R�L��ߊK�A�"EW�4s�ማg�4�O&�Qaؑ,{0�?��3t��u��ʩǝg��4p����w���C�z���J�S�$
$¦�D��B2�MưKX�HD�+~k/�n02؃Sa��|X�S��Ɲ��E��L����$w;��!��x<�+3��k��b�v�	�1��.v��vĢ�9�Y���k鿒Ơl��;U`:;���+i�|�@�WQ<uxX��ջ�>��Ҩ����~������c-'+~����:;�7V
\���)o�-j�(
��`fE�:8�#x���G̶���H���`�Yn
����)�m�?���.}�P������W��I���~��i�8~���k��ݚq�ɠ��gZŠ������s|�}g_�:OFOr�+�>�V�6w�RA�������'�Z�*&��;P��Y����-> _��Q
�S4�e�*<����tJ��u�DQ�B�I��t������Q�z�Pv�{��-^�o�mr`�[�}`� ۜ;����'Gݴ��=�1��k���Ȼ"p�53j�4N=�PθIj<b���!Y��W�Iܠ��F��mǻ��o�H�c��z�R����<�j*�<]�m|>\�P�u��$�Vv�{�sr��V�]'i&�Ӣx��y��(���Y��K�5����F�Y���ٚ��v� �pށ�a��{��N��y�F���Ƈ�3��<�+�";�5&)r�2���U��.�wBy�T)o<>"�7�g    3ɂ����ޝ�E��t8�vX#�-m��Q��=hm��wLK﹚#c�n���dg;!콵q}��{���l�\6�a��
�f�ir����yFϗ�<o�xM��&�����Y�D$B���W�>ȶJ�s;ubƌ��BYG�Itau��<���~t.�rh)���3�I��o2��W �(��m��.1��5J���,����-H�����e<W�6�A�������VQ���������'��N�^F������o���m��A]�S�נ�IN_�.I\ls�#��#�yn�Xr�fE!4{��N�X�f�݀��~ڝ��Y���)�\a��P����(fY7EV�����%�a.�潋X'��$+���Uf̣����z6��G\�H��Cm�~i3��9}B
�d����t��(��H��j�z	�L��#�:(7�i��QA��RE�3bVp%�l�"%��	B�P5FXb��Mzo8�1%�ڰO�}�ʸ�_�G��~��r�$E_m@�"A��I}�c!��^�y�a߁��L�P�������h!�#�:n��𙳙�r�#t��&��wg�c�؅"�l5��:a�l���%�O�etJvB��2~��(y
w_�[.ϫ����iV�kO�s���)j�+����^̢ϧt�$�<-׺��;�A�{H�q<��\�zsq�Ùݯ�!�y^߃��>�Qgm8��E!}^	Cb�*�0�Y�Ɩ%��
��"���l,�^�y��`p�y����7��`@����=��^���\pQ�[���G��C`V��>i�H��J����;�.��<A�nk�%��W��iG�r�M���5?x3�&�esc����SqaAק�N�q����nU%;�~@1^�j����MZz��y[�yZ�)���>�E�����m���~|��!ŰP����o���!ͬڍ��N��R�ɟ&��J,�����"KQ*�b�i�2�u>-��ʢ�-uy�["4w���j
i�����q_t*�GG�n,�>��CG ������l�f)͇G����L}�;��N`R>4H�]�~g[*
f����q9�<^F�6N�mcQ�p!��TH�/h\��$~��Jm�sg��o-�JH;X��"�ب7|dqL�3i���C�,_�å媂&݊9a�	(Z�(��J$5��Fݿ�c3�l�зi���_h8	�T�:4{�?M��ޞ�HM߻v�������t�c�h��U�
Y�;o��s�gEށ�	��½����T� "/X��:��n͝�,��|��T�8�yv�����̰o��i�7%�X?
�KI��ig�'���Gj������LWu	.ް5���G��{j���(�kE2�g8�F��$�A΂l;T^h�F	֋\`��Ձ�g�Ș��q>t�uHIK8��8z����R��=�6��2�ӆ�Ƃ�SE���e;���8@���;�J���R�����&�I&�_+�|!]<�GW�I�Ƭ��.���	�ҩ�<��9���_�⅂�CԼ��+���2����T:�R&[���-ĭ�L�z�ZIX@Ns��mu.�l�����#�5==�����Z�W�&����E�����.��WA�u�}�`dj�f0���P
GR�I/�=�b4���ұ�Ei�)܏��S��O����� ����m��k�YP[f��I@W�>�&gs�"	�V.ÀM>���a�z�d�dտ�44Y����C�(�oY6C���B}�U�ɠ�q��m+.*T��N�颌�b2h�a)�3n���sā��*�
��`�Q�`�D��_� ��J~ ��۳������B�69w�B{`yD��QZ��4!r>���qi����l��}[U��I�,1��Y���bQ��]#�)	ph|'��X��(}�kb{�
�l�W��.��f*XR�
����h�M4B���!�pp}�t��=�4�i&N�MdgZSC�]4pt�KRa��`I�i)=�Y��	������������rv�Q��͂!�^?�0��&Kf��r��#�<�{mK]9�9x#�цq�a��+P��Y�t3�� ����Y����D#w2l�u�-9l��:���]n�}�4X�)%�y2lp�0��ԕ 9K4B�0��f�dxM��>/{���U.�8��7v��,�	�C�NF�dxO�e��d4h�h�h��Vӗ�]_�����i�Ѩ�h� ��=c8�$�x}���Hy��k��gp�!�bD>!�G���WG݁Eϳ�� -��ă1A�����ւ�"*)�j�wZ�$Z��Jv��+�!LFG�������:5���O(�-�B�ߦ̈,tȠ*�F�����#��u��0�A�^zu�MKʈ-Ը�H���ҟ�\���\�A6��1�tn,�����vzKV��g��9$w�MT�ϪZ�Qc��[Vd�k�ö�&l��=8�fX㹤:c���-ܗ4�$]gM�娸���)�W$B̸$����yP�,�y)O�d0��?�?����ͷ�,�����T�,&�g��1W��l�;�BG1���c��v��Nm�|��#�_z��4�	#!q�o��*%ќ�QCd!���Vj�n$�/U�JP�U�44Ը{(�<�ԅ*��}���I�Q�46GP���x+��V�H+E_�QO�Is�m�~���1\�HJ��ȟ$��a���� �1�"y�jG��K��:G�뚜�H�=��G������~3�S�ߤ]bg��d|�\M��7�Z\��L��ӆ��9�O�{����d�-O2��4S�)?�y
���sҪy��m��d�jwZ���	�u�nP�.0�.�,N�;��01����y������y�E+�Z�	B��InĮ�ahL=���st(���~�g��\*����ks�������_����[�����y���˷-����x��3]tV���INp��o�ť���y�n�o篯�����.o������N�� (�~�e���~��Z��wߟ�������!�NW�!;޷ۦ�ڗzKf�#n��O�p��R.|烴��{���aUm~A(.��Ay9ι�)X��q��Ƥ������[�f"e ��e�f���RQd�R�
Wဵ	���ցL4�ㆮ� 7��4����ݞ�2A0��+��me4��;�P���<me��ўg��<�p�/Oq��&�/��C4�]$�"�ˀ9�J�w��yaг86�O+,��S����3V�_�2H)�#�=���Ҽ��Q�׺�[�X�$��&b�4�WH����ѣD�Q��S�g|��J��Af���@�����Jˣ�n�қ��-e|)u���G8k�@�K<0Wâ����X�[�h�s�ߞ��F�O�B�%��{t�%s��3�q����e�W7��}�؍��|��Y� \$m }�Ƥ����J\�K�j�T�Vq��l�2磣G������a�^�y�;ߤ
��>h,��My�M�]hPG��o�;��K}*`!�������-�G�s�6���Gk�f���_���f����{��)��[\PQ*��,ʥ`����ƭ��/\��E�)����չ��d��@��'e�����l��<��n�u�ګ5 �:۠�:/Y���׍zg]��m��(�5���]���E=����Z��6a���k���H�fia
��4c�a�W�s��(7�r�M�o6
j����u�/9�	Ng���BL���!��'	�t ��x'��3yJ�~)f��+�q��?Æ��_A�� �r��ES���Մ���b�Y�lOb�P8�j,?e�8�m-w5�a$+���A�K����.�Baj���#`���9H���G�_�^\ϳ=ɍs��r>a}�v��F�_�Qc��݅O��щ;��;D�m`y�;2(������˷�P��^dPe2g?�{squ���Ϳ�_��t� R&�L���]����~+ �+��(�5���|�+�����s���A�~�yJ�0�h�����_+q>�V0vPlQw��ᰥ!ʮa[w ����~�۱W���9P�G�9�jH   1� �!�r6��Ǡ�����=�Os�-1h�L��Ņ��U�9S��e�#����t{Gx��-�����-~��u��ysb�3�����BW���(�^l�����y�ͯ5<�k�Á���}EB3i&����Ρ���Ƈ�ov.F���Y�͉�����?%h�<��s��Y�`��{���e?�*���f������H����؇Pu�G�G��������[Ug��^��f��I���0C�t�ő�OZb�@�?fS���^~Կ���q<�q=|Z�?�����9��𑷠I�t&`�t��M�~��-]G�/R��?���#єrۡZ.]��xޖ�g?-N%V4�7{����=�Y�Kj����Fa�(��J����:�H�h!r�X��7�
�ה
c�:�с�����*~X�*�
�.�J��Br����ܩ��IL��a��r���0�4l�O�	�ņ��`5��B�r��5�9�%�0��ޒ���=ᛪ�l�X�Qk�����yk���&Jq`B9���R���l�]��5OC�b��M���e�}��7SE��\������������<��S��E.�r�.ғ�]��ms�$DN�{l�T����n�"Í�5 h��/���ݟU�}DV�?!�1^M�(�;/���T�O�	��BBU�$�b���_(��J�+��.Jj3���o���&R~�<��킕r"Q��aO���x�vf���J:be!�xj`@�|��ʘ�����5�z�M�o�%����ݟ�Hb��ַh�q�Oi���xv�gk^�]u��zDg��=>�����,���.c���7����L����f�瞓_�
�&UFC���4PIt���xL`��O�oº�.']�
=�����H���aϡC����!"g�	����
~���Si��"qo�Sη�
��4F*(�?��B�@�1�}'	�4qڰ�ͧ1�(�`EO/k�������lS��-w}]"	�+�*k瘢n������_�>�����������P�i�� ��9�T�a}5ťI���j
AN� !�`�asY��;P
>%\J@��w����i�!g7�
�/��^C�ڮ}8M�� ���@X���[MvݯnAr&ܚ3��m>����E;WP��X�������;x�qo���&��b��2,Q��ȾS�]׏���G���q��ޱ�/���xg'���ǟVY�Z�%�W��e���h�'�NKX��Vjߖ1�	o~.�x��ʹz�n�L�
Jk8mv{J�����PAbT ��/$dN�_->S\󂛥����L�"���t�~L����f�b4B�?��Њ<�i"��f��2�Z�c-��E�0j84:��΃�&�l�o�SS������#�ѽ�W<_�a�y��Z��Wi�#�w����#sQ$իҸ���͞�ajlx�E����m;��XNl��s�y{��]�fv��.#-�.�|��Bw-0�L��5w�@f������^�n����+��%7r�Zo(�7td��NO��4$6Q$)����b*1��yx��1�O
�ў1���?���TQ      �   ]   x�3�t,*�S0��2�tN�KN�ILI�2�(J-K���9��srRK�L9CJ��@
�8�2�2K3K@<s��<��������Լ�|�=... ��      �      x��\Ks��>ÿb�[E)ċ�'Y�7��m��U1U{��[$� ���-#�=��K|�c���u��1 (Qe;�\� ����==�������"���0^}N�o2_H1O31�����\fR�}�e�;Q����8���h*��\N24OR1_F)�ߣ,�2��a:��8QB7�E�S/3�D�"�E*x?��_�=�������4��.��ᬃl��.���}�y]M�Ϧ�*ޖ��"p�5�Ÿ�&�Ds���ׄ��#��qO��ϓ�
O�y(��t�۹�;�'nD�_��vFTI�D�< �O�0��}F3Z�8*6�y<iM?���C4F����FtG��x9�^�����L��'ʧ�1����W'�i���E�Y�L�y/	w�(�I����ơ^ܹ�[8���Z�C_	Vt��z��y�s3��8��2��|i�0�׌A������ut�����,R}�^}�Y*~^�4�7���.����Y�HQ~]}�/�#γ�j�1�:PMQ^{���Y�����B^�S��$����_ރ��7�r���i�&�٤�x5<������p�Ny]\6.�{�Z����t��a4��<&�8���"��limqY��؎Wk]�M�=�$d1��fS�^���S�l�����p,�:�)"1Q�MH%�Q�X���VZ�\�"���8��92ީ��l_T�t�:���Ϩ#��t@\���������B9}\x��b.�}�#������=��/��3��O��%Lg��z�e����$&��W���c*�#h*	K^�J.�T��%�$v9�5����ۑ�	�'J�d�?h[v�I�q��z+IʺV��k9�����Yx�d��Y�r6��B%�l�E�q/��S���ʀ7M,ۇp��dsy)��Dq�����0���C��`�9�y�9*�Ř�G40�*"��.0��K����0@���[���5ƪ����?���(�� r�
�^Qba��+\P �������R
�#�-�xXc"�l|����ow�F�)!�}@��h)B,�����h��,�Z�ɍ�s�K�.e�����àpo^�͇X��0�8/�7�(�<�
����l�Z���qM�
��>�m�h}v��Yٵ�oe�Zy�½��c��;�y�g��]��g����-��p���E�S�a���ٮ��{�.K�Y�Z��yc��ٸ�uك�e�ކ�.N��j�{����,]�	��]"ޣ4~,X�
_�n;�"���^���aU-6����͞���u*�CZ"�yq�G��l>�h I) �n�ý���;�y(�`O F���{�r�@��5����I�XgK�޲���C�o�&go�Lh]�ch}����Lz,_�E�I�(���F�`@�	���o��P�l�\^Ǌ_)��|�f��S؏�L ^Iٔ+@��0�|_\��Y,��s]üJ�qJ뜑�L�r�B&r�e�3�%� �'��]�`f���`w�,�(hf�E&��dR�Q��,2Fs����CU۵l��Z�q���Bʄ�	S�BN�!�-�Y{��z-N��)q��v���R�*�;b4�࿣�W}�0����e�	E����d(g��)mh�=J��ۭ�C��R��i�j��ۘ��4��y��g�~�SdL1���n�p���R��P��~ڌßWq�/6f�X�Mt6�U���e5��$�.�`��f0(��|.s�E�o�H��ř�(-h3�GA���B]Z��_-����'��;0FD��v̛Ǆ�[ƉO��xϾ+x�����sx���L8����H�
8������ٿ��'��-%�2R�냓R[E�[��ԗ�N�n�`��w�u�|cn�^^��{��������4���Ĉ��V8A"���WL�� �5����;����J��3Ťw�	~�]��3g�;��a}l,<� �on���vӪ�T�S2Y}�G������o���g��qP���ӡ�o'~����I�u��&�*{�G�!փ�[2Y*����>[Y(rQ�H�5"�Ǌ
��jͺF�2�[�鶻��p�v>7�N��������i"N�;�k���B������H���>yE.�!��F���z���}�2`�+zZ���٪*��e��F@z'�,���Z(�f�k��BQ��N�/_��x�$�>eq�w�����r�,��mh���gʢY��U�f��=���n���vw��l�e����l�ݕ��u����
�]I�Y��fkQ��A�Y�̢�k��7��9��ӊ�7�7�~���XX�n�hd���Y0?t��H�FO7��l�o g<k�5�x!��7T����:����57�ztU�NsN�
S��QQ-M�h�0���/�����#�u{���Ȣ�6y����	�J�]��c��VãZ�uX�P�l�ږ�$e���%�O-�Hg���<e%�4����$�H�̮�f
5&��
�b��!$3Ubtc�MҬ���Qz�5N�فQ3RVIЖ�������ձ��d��އ��{�>��~o���;f�Z���N�j��lZ~]>nDg�\�v�\�% ��֠��P_i�"R���T�
O|�̘]��S��kń�?7J���6(��5ٰ��Z����f��.n�~P��݁�uV�4}>=�'����^�41 0�����r��
Z��t�OX��%B�B��a�Q٤Bǹ�xd�OG��O���/P���y��F�ES�(�
j'=��D�����'	� �	#�؁!J��(�W�{���Yo�6
W�z���-��}=���ԟѝ�J�nT��$�Q��'h�<�d�F-�9�p6�w��K(V�hJ	Ds�5�1�[H��a='���G��T�ȏ��W1նg�h�-+���~h1����&�u�Z=�N�?6*e�x`����.��f���n9��n�#A
�lXN���G�"_�Wd��$��#�51�ٌq����Wqv�6��*�V!-����]q`��pd��ϮV$*r�w��n�L/���
z�uTm��85/�/ǈ�\���e���y�����:�!V�k���.�bX�Bh�7C��Bզ{%!��g�$&�믃�{Y�:XS�������~�9���ۣH�$X���d��v���h��)�ON�\h.�r|w3�ڳ�!	Q�Q ��n�Kl���J��� �z������2���aJx�
d����p�[�ǆ��7�Lq&h��G<Q.��)�-��C�U2
�bwuy���_L�`�]��6���Rɾ�V1e��6�UJ���y����-�!���cq�����Z����RuQ���2hx�c�C�&诠(����O2�R�W	9~��8�g1O��v41E����+R�:[fQoj�]~�sP$�tL)�%���29����I�I��Cm-���Dn�S�������9u$M�G�H�6�4�> TmU�B���#4>ѹ�,��2��e9��qZ"<T�=��{ŝrm!��#�O��N��]g����ݻ���F����z��s*r4����U��O�����Ṛ���,��@����}q^�W�^��6�����/��`�M�9��)���C�c}
k3(�;�����Z�8�{�;���1�mh�V�mP���)Lb��Bv��Z�QaW΁�㮁�ڑ�I�M������pLGF����V�>a���ŭ�ҟ0�)�؃�D��]���L}��~���oY-�K(�\k	�ftQOe��y�n99��7�0���u��烁��E���r��	��,J�?�Y��6�!�U-��(�]�x��ܐ��6U�
��p�Fxރc!/h�@vT�bn���Lh���r�>]j�Sۻŷ�	I�glW�"��SJ���d��T��<��_J^�����aX��0 ��-�2�?�v���_R�����������m�M�&��۔�P�+�v�@�D�1��C�9�m�L����*Y}F{�{�o�_6|*��0��_G���� �   ��T���)˞~qm5hj�Khz��C9_}Z��Cwѧ�HĹ�V1�K��%=����*M�c*�IS�[���f��2�;?�}1˨0w��A���C�Ux0�o�.B�O�&.���fD�K]�ijRS��=䦣�"�]B����](~��
�o��x=Hjx�Gy��u�>�Y'���:(�ӿ�	��?�&��9#8��SyY�U-���!���K�ǯ�/^��/+��      �   @  x��UK�![7�I����#`Y��l2K���ף�Lozl��-t~#�J+p�`ň	�T? �+�+'��,�)�<�, ;�{5&��J�����I��YU��\�N��t&b�LSBKB1VSI��(�;�Rؠe���`�1Tz%Q��"�_���:DZ�3�-ݩ��)�Q�?بP�Ί6nӱ��	I�c*g��[��Ī��~��ZKi�#���ÿz;K�i\��eXţP�e��Xr�?��e'N`�'��F���A檀)��8�$�&g�w�:k]HjB.�=78��s��Ka�о-4&XI��X�h��"�A0l�hɇL2�xg�W35wb���L"�@
|��s�=/xaԭS��7�g����yOƳQgj�N�t
'D(O-*P���y�"D�}��K�-zO��Y���챚ȇ�3�W䯂�W1����Z�qn�|d�dg���#�Ȟ$��5�QF���H��3���!�������^kȻ6xW�{���?`���v�O���s��ۘ)p�^��Iׯ�L������|3���Ե���5;'�0X��)���?��J�      �   �  x���]n�0ǟ�S�E�!�UH�l��.�R�E�,�mb�V���ΰ����&Ef�nD�#�?�IQ�=/����ʕdVVO�ɪ|��`ծ�"�].�V�8��$�x�7���|)���pu����}�.sq'@뺲�J^�+Ulζ?E���# }'�??L$x��� �TD����)G��$�x<"R��*��oq��lW�ǭzܫa)��I��ࠋ������?����� t�N���} #>a`<L�Bo�u�����Ą�^��?�!@:9���Q��,r?-�E���
��A`�3�?��}�/4�XR�f���J=^��?8�lc��d��L3�����:�<�ƔġG�����xKw|H�*i�Uhc�bV�c�v���w�9He�7�B@.�|���-���-�,�q�<ԡ%� �(�Og�E�1-�wZ��@�h��b�����^7h#��5�ްF��f=s'�ӓ+A6�H�p����)<�c^��̠8���G8�����u�R��n��b�jB
,�#�����Yw����y�ֵ��Y�I!z��oq��� ~�;�X��a]�q>�(�.'�{�?�P]�`>���(m��$8I<�Z�D��cܖ����J:ȟ��i�k�qoB̗��g���\,�9��F��{�Hi;cȮ�l	�#�}ͰC�}�Յ��rӽk����l6��P       �   �  x���Kn�8���)t��$���	���������͜��d�I�i�c��,���R=��i����v���yZ���}^����0,��u>�i7��i~���yx���:��u��OS�!@��|< ��#@HJ��������u=������� f@���a�����\��L�$I���F�#K`U�\��°��bF6)'S�4��(�
�z�F0/����2B��*j�=�=�']��ұ+=� !�
���s�����<��Z������M��y�xzZ�D���T��ԮC���c�X��}܁R�C��� �ǉ-�ԇ�$R��'���ŗ�w4qYԐrtM�:��UlD ��N�J؎�@�e�=I��;m$
�@+��Z��\��%�R�Oҏξ�F�%�{�ݥ�Q��. DkP�׷��}�/?���g����4��~9=?xF��%�#���`��2�?s��>��}���K�HH8�.���w��j!	��l1\2E��ĢD�HX��^��L/>'��������_O���_S5���%��%u��w}&�j0�L�N�����{�����.y��e� �u��Zz5�=�_��'a��>h��e��޿��s>���ʥ�T��Įg�����T����kz.[��l`ձv�E\������_r/W���K�_�dMx�z�h���L�ܼ[���%_�#d��V ��I�܍T\܌�Asz�h�F�Ŋf�gjr;�r������;�I��rĚ5���U\��ݦ�j�9��Q׉���(mr�g&��k��o��o{�K��-Z�u��;a��IS-կ�<x݄���m/�3S��,T��^?.��-��V�nP�m��O=0k��Fb�$5�q�L8��En3�0�v���q{�3q����kUu��ij�8�J�������`��n���+A1�L]�Z��}�����Hy	�q�:�1v��F�� c̵�b����Ż�Q���?����'Y8*�      �   Q   x��K
�0�uz��N�&� Z�Mr�ˁ�d����PN3�w4X�;W�~X�6�a��Q�'`���dz*��6|L)�4o�      �       x�35�4D@.Sc e��B�@�+F��� ij�      �   �  x��W�r�8}v�B?�)�w���2���0���Ӽ[R��e�ڿ����`��
&�}�[�O���=�|_��S}����x�jk�Z�F�lk��m��O۔�����m�ޟ'��F� A��ZS��lQ��%��ּ���������ë@!��/��><
d�1��m���:�����#�v��F�q���D��B��$��q�lG��M"�;�^��r&X)A?��;�0/���?���A�@R"�'�E�͜�×0�o}@,��:�Bq�y�F��K��m󓻗�������i�"Fc�����ݐ�bx������WuE��� �o��;�M�L@R��3�b�R��\({�D��&��Q�ye��_�Tsm�i��6�8g>��#=��GG�pR�͢���>r���`Dj����	p]�%����J>#���<�������h8b\���;@/\���j��Y�"�M�ʶ���+1���t�����,��iVY.���(���X�Ջ�Ŧ�D�5��/�]�viqևh�����s��x��n*��h˽0��G�M��=>]�tI��4Y#2���R�׫A��A֥��VOͺ�9h%Mh�2zx�|>gϨ�ԗ�;�7��W�������[3V�Z�I��.�Rh΂Y�l�0�R��2��4��t�!"G=y�_9��R��WT���åj�P����l��^]"
�hc���j\���b))7��/����z��Hg�r�7锶��Y��"]k���u�WJ���^.P_H�1�ċ�J�4E���r�tk���\��a2��.���w�?��}��$U=⥟���r�x����΢Ʀ ĩt�mQa�y����WE��RT兘��=�F�c(��2�‌�B������ɯ�k��	t��:��gsm`��{�Evc�m����|T��0���QX�8P�����UL}��l��ef-w�C��A�!�'�����N�[M�5-�Y�7ŉ��%�"���a�����l���wl6�^�w�*�7n$�a��7�w��6��y
н/R`l�xNՠd�n&����@
�lrL*0��XO���Ks����Y��ϡB8�rgZN�H��;���g�#gb�H��m�7=9�]�註`��x��&�ǧbL2v��O��G����"�q���H>h>���F��F娢3��8H�:=$��N`�?�nnn�KH�      �      x������ � �      �   1   x�3��/H-JL�/�2�tL����,.���9�R����rR�b���� b��      �   �   x�m�A!E������S�&n���?��($,xy�����7��XԪ�-�P�!
MK���c��e��EV|@����?���z���g���j�8&P������P[�14k	�*�1cJ+�}��bi#x"=,��z���=�J�����		�Kq      �   �  x�}S[n�0����H���u��,`�ȆH=N�ҋu��1M�J�@����b
e
V\�æ8l�QF�*�p������qz
�����]��Soth��C�K%�I��At컥w��*U���>Q]��]�z�/���yqqF���(oI*N��fݻ0��[��G2*ۻ�qSe�Tj?>�Ȕh����*g����>��=<���Jf����VI�Nb[u��HCf��CV)��A�=\�>If��]���������@� `��?&�-rDo���d���'Jb����1��ڵ�R!��"�X~T8U�$dӵ�=���xoq�V�r�F�vU�5��)2���y#z�	c��j�#1&cr��V'?1�7�5+��L��8E����S��a��>?�`�Γ��A�/D��� �      �      x������ � �      �     x�m�An�0��+��$Eɔ�ҋD�i�4�8@��*���fg���gy؏c�^m+m�9t(���a�8�����h˲ T�, 9��"L�yt�m�rik������^��:����K䀽D�爣�@�g	:VjZ��V�JCƒ��2��λ�JW�n��}�*�r���="�Ly��H��7#Ӡ�RW���$3��I���gp7ێ����a��G���La�n�M�5��hiKmc�@���&��?F��/�z^)      �   �   x�UPKj�@]�O��u��2�E�H�
��ٌ�ј���wʢ��X���xz�j��0QH����!���g�L[K���j��M>�ãۨ��#t�A��ᄝѨ��F��H�r-�F-�#����=:c
�V���#��,����!SklXϧ�������=G%qB[�.io���K�ˣz�V8�^z�3C@NET_��a��"}���>�i�GIǂ��81D����yiס-��XUU��yx1     