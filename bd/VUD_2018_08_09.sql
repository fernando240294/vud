PGDMP         +        	        v           prueba    10.4    10.4 X    U           0    0    ENCODING    ENCODING         SET client_encoding = 'LATIN1';
                       false            V           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            W           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            X           1262    16393    prueba    DATABASE     �   CREATE DATABASE prueba WITH TEMPLATE = template0 ENCODING = 'LATIN1' LC_COLLATE = 'Spanish_Mexico.1252' LC_CTYPE = 'Spanish_Mexico.1252';
    DROP DATABASE prueba;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            Y           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            Z           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1255    16768    usando_for()    FUNCTION     �   CREATE FUNCTION public.usando_for() RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	resultado integer;
BEGIN
	resultado :=1;
	FOR i in 1..10 LOOP
			resultado := i;
	END LOOP;
	RETURN resultado;
END
$$;
 #   DROP FUNCTION public.usando_for();
       public       postgres    false    1    3            �            1255    16772    usando_for(integer[])    FUNCTION       CREATE FUNCTION public.usando_for(array_p integer[]) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	resultado integer;
BEGIN
	resultado :=1;
	FOR i in 1..2 LOOP
			resultado := array_length(arreglo,int);
	END LOOP;
	RETURN resultado;
END
$$;
 4   DROP FUNCTION public.usando_for(array_p integer[]);
       public       postgres    false    1    3            �            1259    16402    USERS    TABLE     }  CREATE TABLE public."USERS" (
    id integer NOT NULL,
    user_name character varying(255) NOT NULL,
    employee_number integer NOT NULL,
    description character varying(255) NOT NULL,
    rol integer NOT NULL,
    user_register integer NOT NULL,
    date_register timestamp with time zone NOT NULL,
    status integer NOT NULL,
    password character varying(300) NOT NULL
);
    DROP TABLE public."USERS";
       public         postgres    false    3            �            1255    16440 /   user_auth(character varying, character varying)    FUNCTION       CREATE FUNCTION public.user_auth(_user_name character varying, _password character varying) RETURNS SETOF public."USERS"
    LANGUAGE sql
    AS $$ 
		SELECT
			* 
		FROM
			public."USERS" 
		WHERE
			user_name = _user_name 
			AND password = _password;
	$$;
 [   DROP FUNCTION public.user_auth(_user_name character varying, _password character varying);
       public       postgres    false    3    199            �            1255    16751 K   vud_create_modality(integer, character varying, character varying, integer)    FUNCTION     �  CREATE FUNCTION public.vud_create_modality(OUT id_modality integer, _code integer, _description character varying, _legal_foundation character varying, _id_transact integer) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY INSERT INTO public.cat_modality(
	code, 
	description, 
	legal_foundation, 
	id_transact,
	status)
	VALUES (_code, _description, _legal_foundation, _id_transact,1)
	RETURNING ID AS id_inserted_requirement;
END 
$$;
 �   DROP FUNCTION public.vud_create_modality(OUT id_modality integer, _code integer, _description character varying, _legal_foundation character varying, _id_transact integer);
       public       postgres    false    1    3            �            1255    16735 )   vud_create_requirement(character varying)    FUNCTION     �   CREATE FUNCTION public.vud_create_requirement(_description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO public.cat_requirement(description,status)
	VALUES (_description,1);
END
$$;
 M   DROP FUNCTION public.vud_create_requirement(_description character varying);
       public       postgres    false    1    3            �            1255    16755    vud_delete_modality(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_modality(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE public.cat_modality
	SET   status = 0
	WHERE id = _id;

END
$$;
 7   DROP FUNCTION public.vud_delete_modality(_id integer);
       public       postgres    false    1    3            �            1255    16737    vud_delete_requirement(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_requirement(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	UPDATE public.cat_requirement
	SET  status = 0
	WHERE id = _id;
END
$$;
 :   DROP FUNCTION public.vud_delete_requirement(_id integer);
       public       postgres    false    1    3            �            1255    16710    vud_delete_transact(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_transact(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
	UPDATE public.cat_transact
	SET status= 0
	WHERE id = _id;
END
$$;
 7   DROP FUNCTION public.vud_delete_transact(_id integer);
       public       postgres    false    1    3            �            1255    16487    vud_delete_user(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_user(_id_user integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$

BEGIN

UPDATE public."USERS" SET status = 0 WHERE "id" = _id_user;
return true;

END
$$;
 8   DROP FUNCTION public.vud_delete_user(_id_user integer);
       public       postgres    false    3    1            �            1255    16754 (   vud_eliminate_relation_modality(integer)    FUNCTION     �   CREATE FUNCTION public.vud_eliminate_relation_modality(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
DELETE FROM cat_modality_requirement WHERE id_modality = _id;
END
$$;
 C   DROP FUNCTION public.vud_eliminate_relation_modality(_id integer);
       public       postgres    false    3    1            �            1255    16774 (   vud_eliminate_relation_transact(integer)    FUNCTION     �   CREATE FUNCTION public.vud_eliminate_relation_transact(_id_transact integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	DELETE FROM cat_transact_requirement where id_transact = _id_transact;
END
$$;
 L   DROP FUNCTION public.vud_eliminate_relation_transact(_id_transact integer);
       public       postgres    false    3    1            �            1255    16764 B   vud_insert_transact(integer, character varying, character varying)    FUNCTION     �  CREATE FUNCTION public.vud_insert_transact(OUT id_transact integer, _code integer, _description character varying, _legal_foundation character varying) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
BEGIN

	RETURN QUERY INSERT INTO public.cat_transact(
		code, 
		description, 
		legal_foundation, 
		status)
	VALUES (_code, _description,_legal_foundation,1)
	RETURNING ID AS id_inserted_transact;

END
$$;
 �   DROP FUNCTION public.vud_insert_transact(OUT id_transact integer, _code integer, _description character varying, _legal_foundation character varying);
       public       postgres    false    1    3            �            1255    16451 c   vud_insert_user(character varying, integer, character varying, integer, integer, character varying)    FUNCTION       CREATE FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying) RETURNS void
    LANGUAGE sql
    AS $$ 
	
		INSERT INTO 
	public."USERS"
		("user_name",
		"employee_number",
		"description",
		"rol", 
		"user_register",
		"date_register",
		"status",
		"password") 
	VALUES (_user_name,
		_employee_number,
		_description,
		_rol,
		_user_register,
		'now()',
		1,
		_password);
		
	$$;
 �   DROP FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying);
       public       postgres    false    3            �            1255    16450    vud_insert_user(character varying, integer, character varying, integer, integer, character varying, integer, character varying)    FUNCTION     "  CREATE FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _date_register character varying, _status integer, _password character varying) RETURNS void
    LANGUAGE sql
    AS $$ 
	
		INSERT INTO 
	public."USERS"
		("user_name",
		"employee_number",
		"description",
		"rol", "user_register",
		"date_register",
		"status",
		"password") 
	VALUES (_user_name,
		12376,
		'',
		2,
		23454,
		'now()',
		1,
		'');
		
	$$;
 �   DROP FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _date_register character varying, _status integer, _password character varying);
       public       postgres    false    3            �            1255    16522 *   vud_log_insert(integer, character varying)    FUNCTION       CREATE FUNCTION public.vud_log_insert(_id_user integer, _description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

BEGIN

INSERT INTO 
	"public"."LOG"("id_user", "description", "date_time")
VALUES (_id_user, _description, 'NOW()');

END
$$;
 W   DROP FUNCTION public.vud_log_insert(_id_user integer, _description character varying);
       public       postgres    false    1    3            �            1255    16756    vud_modality_record()    FUNCTION     5  CREATE FUNCTION public.vud_modality_record(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT id_transact integer, OUT code_transact integer, OUT description_transact character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN

	RETURN QUERY select 
	a.id,
	a.code,
	a.description,
	a.legal_foundation,
	b.id,
	b.code,
	b.description
from 
	cat_modality as a
INNER JOIN 
	cat_transact as b
on
	a.id_transact = b.id
WHERE 
	a.status = 1
order by a.id asc;

END
$$;
 �   DROP FUNCTION public.vud_modality_record(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT id_transact integer, OUT code_transact integer, OUT description_transact character varying);
       public       postgres    false    3    1            �            1259    16726    cat_requirement    TABLE     �   CREATE TABLE public.cat_requirement (
    id integer NOT NULL,
    description character varying(255) NOT NULL,
    status integer
);
 #   DROP TABLE public.cat_requirement;
       public         postgres    false    3            �            1255    16734    vud_record_requirement()    FUNCTION     �   CREATE FUNCTION public.vud_record_requirement() RETURNS SETOF public.cat_requirement
    LANGUAGE plpgsql
    AS $$
BEGIN 
	RETURN QUERY select * from public.cat_requirement where status = 1 order by id asc;
END
$$;
 /   DROP FUNCTION public.vud_record_requirement();
       public       postgres    false    3    1    207            �            1255    16723    vud_record_transact()    FUNCTION     F  CREATE FUNCTION public.vud_record_transact(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT total_modality bigint, OUT id_transact integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$


	SELECT
		a.id,
		a.code,
		a.description,
		a.legal_foundation,
		count(b.id_transact) as total_modality,
		b.id_transact	
	FROM
		cat_transact  as a
	left JOIN 
		cat_modality as b
	on  a.id = b.id_transact
	WHERE
		a.status = 1
	GROUP BY b.id_transact,a.id,a.code,a.description
	order by a.id asc;


$$;
 �   DROP FUNCTION public.vud_record_transact(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT total_modality bigint, OUT id_transact integer);
       public       postgres    false    3            �            1255    16752 3   vud_relation_modality_requirement(integer, integer)    FUNCTION       CREATE FUNCTION public.vud_relation_modality_requirement(_id_modality integer, _id_requirement integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

INSERT INTO public.cat_modality_requirement(id_modality,id_requirement)
VALUES (_id_modality, _id_requirement);

END
$$;
 g   DROP FUNCTION public.vud_relation_modality_requirement(_id_modality integer, _id_requirement integer);
       public       postgres    false    1    3            �            1255    16765 6   vud_relation_transaction_requirement(integer, integer)    FUNCTION       CREATE FUNCTION public.vud_relation_transaction_requirement(_id_transact integer, _id_requirement integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

INSERT INTO public.cat_transact_requirement(
	id_transact, 
	id_requirement)
VALUES (_id_transact,_id_requirement);

END
$$;
 j   DROP FUNCTION public.vud_relation_transaction_requirement(_id_transact integer, _id_requirement integer);
       public       postgres    false    1    3            �            1259    16413    ROLE    TABLE     i   CREATE TABLE public."ROLE" (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);
    DROP TABLE public."ROLE";
       public         postgres    false    3            �            1255    16444 
   vud_role()    FUNCTION     �   CREATE FUNCTION public.vud_role() RETURNS SETOF public."ROLE"
    LANGUAGE sql
    AS $$ 
		SELECT
			*
		FROM
			public. "ROLE";
	$$;
 !   DROP FUNCTION public.vud_role();
       public       postgres    false    3    201            �            1259    16674    cat_transact    TABLE     �   CREATE TABLE public.cat_transact (
    id integer NOT NULL,
    code integer NOT NULL,
    description character varying(255) NOT NULL,
    legal_foundation character varying(255) NOT NULL,
    status integer NOT NULL
);
     DROP TABLE public.cat_transact;
       public         postgres    false    3            �            1255    16775    vud_transact_record()    FUNCTION     �   CREATE FUNCTION public.vud_transact_record() RETURNS SETOF public.cat_transact
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY SELECT * FROM public.cat_transact;
END
$$;
 ,   DROP FUNCTION public.vud_transact_record();
       public       postgres    false    3    203    1            �            1255    16753 B   vud_update_modality(integer, character varying, character varying)    FUNCTION     )  CREATE FUNCTION public.vud_update_modality(_id integer, _description character varying, _legal_foundation character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE public.cat_modality
	SET   description=_description, legal_foundation=_legal_foundation
	WHERE id = _id;

END
$$;
 |   DROP FUNCTION public.vud_update_modality(_id integer, _description character varying, _legal_foundation character varying);
       public       postgres    false    3    1            �            1255    16736 2   vud_update_requirement(integer, character varying)    FUNCTION     �   CREATE FUNCTION public.vud_update_requirement(_id integer, _description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

	UPDATE public.cat_requirement
	SET  description=_description
	WHERE id = _id;
END
$$;
 Z   DROP FUNCTION public.vud_update_requirement(_id integer, _description character varying);
       public       postgres    false    3    1            �            1255    16766 B   vud_update_transact(integer, character varying, character varying)    FUNCTION     (  CREATE FUNCTION public.vud_update_transact(_id integer, _description character varying, _legal_foundation character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
	UPDATE public.cat_transact
	SET  description=_description, legal_foundation=_legal_foundation
	WHERE id = _id;
END
$$;
 |   DROP FUNCTION public.vud_update_transact(_id integer, _description character varying, _legal_foundation character varying);
       public       postgres    false    1    3            �            1255    16484 l   vud_update_user(integer, character varying, integer, character varying, integer, integer, character varying)    FUNCTION       CREATE FUNCTION public.vud_update_user(_id_user integer, _user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$ 
	BEGIN
	IF (_password = '') THEN
		update 
		public."USERS"
	SET		
		user_name = _user_name,
		employee_number = _employee_number,
		description = _description,
		rol = _rol, 
		user_register = _user_register
	WHERE 
		id = _id_user;
	ELSE 
		update 
		public."USERS"
	SET		
		user_name = _user_name,
		employee_number = _employee_number,
		description = _description,
		rol = _rol, 
		user_register = _user_register,
		password = _password
	WHERE 
		id = _id_user;
	END IF;
	
	RETURN true;
	END
	$$;
 �   DROP FUNCTION public.vud_update_user(_id_user integer, _user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying);
       public       postgres    false    1    3            �            1255    16717    vud_users_record()    FUNCTION     �  CREATE FUNCTION public.vud_users_record(OUT id integer, OUT user_name character varying, OUT employee_number integer, OUT user_register integer, OUT rol character varying, OUT description character varying, OUT id_rol integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$ 
		SELECT
			a.id,
			a.user_name,
			a.employee_number,
			a.user_register,
			b.description,
			a.description,
			b.id
		FROM
			public. "USERS" as a
		INNER JOIN public. "ROLE" as b
		ON a.rol = b.id;
	$$;
 �   DROP FUNCTION public.vud_users_record(OUT id integer, OUT user_name character varying, OUT employee_number integer, OUT user_register integer, OUT rol character varying, OUT description character varying, OUT id_rol integer);
       public       postgres    false    3            �            1259    16396    LOG    TABLE     �   CREATE TABLE public."LOG" (
    id integer NOT NULL,
    id_user integer NOT NULL,
    description character varying(255) NOT NULL,
    date_time timestamp with time zone NOT NULL
);
    DROP TABLE public."LOG";
       public         postgres    false    3            �            1259    16394 
   LOG_id_seq    SEQUENCE     �   CREATE SEQUENCE public."LOG_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public."LOG_id_seq";
       public       postgres    false    3    197            [           0    0 
   LOG_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public."LOG_id_seq" OWNED BY public."LOG".id;
            public       postgres    false    196            �            1259    16411    ROLE_id_seq    SEQUENCE     �   CREATE SEQUENCE public."ROLE_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public."ROLE_id_seq";
       public       postgres    false    201    3            \           0    0    ROLE_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public."ROLE_id_seq" OWNED BY public."ROLE".id;
            public       postgres    false    200            �            1259    16400    USERS_Id_seq    SEQUENCE     �   CREATE SEQUENCE public."USERS_Id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public."USERS_Id_seq";
       public       postgres    false    3    199            ]           0    0    USERS_Id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public."USERS_Id_seq" OWNED BY public."USERS".id;
            public       postgres    false    198            �            1259    16694    cat_modality    TABLE     �   CREATE TABLE public.cat_modality (
    id integer NOT NULL,
    code integer NOT NULL,
    description character varying(255) NOT NULL,
    legal_foundation character varying(255) NOT NULL,
    id_transact integer NOT NULL,
    status integer NOT NULL
);
     DROP TABLE public.cat_modality;
       public         postgres    false    3            �            1259    16692    cat_modality_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_modality_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.cat_modality_id_seq;
       public       postgres    false    3    205            ^           0    0    cat_modality_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.cat_modality_id_seq OWNED BY public.cat_modality.id;
            public       postgres    false    204            �            1259    16746    cat_modality_requirement    TABLE     f   CREATE TABLE public.cat_modality_requirement (
    id_modality integer,
    id_requirement integer
);
 ,   DROP TABLE public.cat_modality_requirement;
       public         postgres    false    3            �            1259    16724    cat_requeriment_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_requeriment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.cat_requeriment_id_seq;
       public       postgres    false    3    207            _           0    0    cat_requeriment_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.cat_requeriment_id_seq OWNED BY public.cat_requirement.id;
            public       postgres    false    206            �            1259    16672    cat_transact_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_transact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.cat_transact_id_seq;
       public       postgres    false    203    3            `           0    0    cat_transact_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.cat_transact_id_seq OWNED BY public.cat_transact.id;
            public       postgres    false    202            �            1259    16757    cat_transact_requirement    TABLE     x   CREATE TABLE public.cat_transact_requirement (
    id_transact integer NOT NULL,
    id_requirement integer NOT NULL
);
 ,   DROP TABLE public.cat_transact_requirement;
       public         postgres    false    3            �
           2604    16399    LOG id    DEFAULT     d   ALTER TABLE ONLY public."LOG" ALTER COLUMN id SET DEFAULT nextval('public."LOG_id_seq"'::regclass);
 7   ALTER TABLE public."LOG" ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    196    197    197            �
           2604    16416    ROLE id    DEFAULT     f   ALTER TABLE ONLY public."ROLE" ALTER COLUMN id SET DEFAULT nextval('public."ROLE_id_seq"'::regclass);
 8   ALTER TABLE public."ROLE" ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    200    201    201            �
           2604    16405    USERS id    DEFAULT     h   ALTER TABLE ONLY public."USERS" ALTER COLUMN id SET DEFAULT nextval('public."USERS_Id_seq"'::regclass);
 9   ALTER TABLE public."USERS" ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    198    199    199            �
           2604    16697    cat_modality id    DEFAULT     r   ALTER TABLE ONLY public.cat_modality ALTER COLUMN id SET DEFAULT nextval('public.cat_modality_id_seq'::regclass);
 >   ALTER TABLE public.cat_modality ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    205    204    205            �
           2604    16729    cat_requirement id    DEFAULT     x   ALTER TABLE ONLY public.cat_requirement ALTER COLUMN id SET DEFAULT nextval('public.cat_requeriment_id_seq'::regclass);
 A   ALTER TABLE public.cat_requirement ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    207    206    207            �
           2604    16677    cat_transact id    DEFAULT     r   ALTER TABLE ONLY public.cat_transact ALTER COLUMN id SET DEFAULT nextval('public.cat_transact_id_seq'::regclass);
 >   ALTER TABLE public.cat_transact ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    202    203    203            F          0    16396    LOG 
   TABLE DATA               D   COPY public."LOG" (id, id_user, description, date_time) FROM stdin;
    public       postgres    false    197   e�       J          0    16413    ROLE 
   TABLE DATA               1   COPY public."ROLE" (id, description) FROM stdin;
    public       postgres    false    201   �       H          0    16402    USERS 
   TABLE DATA               �   COPY public."USERS" (id, user_name, employee_number, description, rol, user_register, date_register, status, password) FROM stdin;
    public       postgres    false    199   .�       N          0    16694    cat_modality 
   TABLE DATA               d   COPY public.cat_modality (id, code, description, legal_foundation, id_transact, status) FROM stdin;
    public       postgres    false    205   �       Q          0    16746    cat_modality_requirement 
   TABLE DATA               O   COPY public.cat_modality_requirement (id_modality, id_requirement) FROM stdin;
    public       postgres    false    208   8�       P          0    16726    cat_requirement 
   TABLE DATA               B   COPY public.cat_requirement (id, description, status) FROM stdin;
    public       postgres    false    207   g�       L          0    16674    cat_transact 
   TABLE DATA               W   COPY public.cat_transact (id, code, description, legal_foundation, status) FROM stdin;
    public       postgres    false    203   ��       R          0    16757    cat_transact_requirement 
   TABLE DATA               O   COPY public.cat_transact_requirement (id_transact, id_requirement) FROM stdin;
    public       postgres    false    209   �       a           0    0 
   LOG_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public."LOG_id_seq"', 4, true);
            public       postgres    false    196            b           0    0    ROLE_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public."ROLE_id_seq"', 3, true);
            public       postgres    false    200            c           0    0    USERS_Id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public."USERS_Id_seq"', 14, true);
            public       postgres    false    198            d           0    0    cat_modality_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.cat_modality_id_seq', 34, true);
            public       postgres    false    204            e           0    0    cat_requeriment_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.cat_requeriment_id_seq', 23, true);
            public       postgres    false    206            f           0    0    cat_transact_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.cat_transact_id_seq', 39, true);
            public       postgres    false    202            �
           2606    16418    ROLE ROLE_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public."ROLE"
    ADD CONSTRAINT "ROLE_pkey" PRIMARY KEY (id);
 <   ALTER TABLE ONLY public."ROLE" DROP CONSTRAINT "ROLE_pkey";
       public         postgres    false    201            �
           2606    16410    USERS USERS_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public."USERS"
    ADD CONSTRAINT "USERS_pkey" PRIMARY KEY (id);
 >   ALTER TABLE ONLY public."USERS" DROP CONSTRAINT "USERS_pkey";
       public         postgres    false    199            �
           2606    16791 "   cat_modality cat_modality_code_key 
   CONSTRAINT     ]   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_code_key UNIQUE (code);
 L   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_code_key;
       public         postgres    false    205            �
           2606    16789    cat_modality cat_modality_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_pkey;
       public         postgres    false    205            �
           2606    16731 $   cat_requirement cat_requeriment_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.cat_requirement
    ADD CONSTRAINT cat_requeriment_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.cat_requirement DROP CONSTRAINT cat_requeriment_pkey;
       public         postgres    false    207            �
           2606    16690 "   cat_transact cat_transact_code_key 
   CONSTRAINT     ]   ALTER TABLE ONLY public.cat_transact
    ADD CONSTRAINT cat_transact_code_key UNIQUE (code);
 L   ALTER TABLE ONLY public.cat_transact DROP CONSTRAINT cat_transact_code_key;
       public         postgres    false    203            �
           2606    16688    cat_transact cat_transact_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.cat_transact
    ADD CONSTRAINT cat_transact_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.cat_transact DROP CONSTRAINT cat_transact_pkey;
       public         postgres    false    203            �
           2606    16792 *   cat_modality cat_modality_id_transact_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_id_transact_fkey FOREIGN KEY (id_transact) REFERENCES public.cat_transact(id);
 T   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_id_transact_fkey;
       public       postgres    false    205    2751    203            �
           2606    16802 B   cat_modality_requirement cat_modality_requirement_id_modality_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality_requirement
    ADD CONSTRAINT cat_modality_requirement_id_modality_fkey FOREIGN KEY (id_modality) REFERENCES public.cat_modality(id);
 l   ALTER TABLE ONLY public.cat_modality_requirement DROP CONSTRAINT cat_modality_requirement_id_modality_fkey;
       public       postgres    false    205    208    2755            �
           2606    16797 E   cat_modality_requirement cat_modality_requirement_id_requirement_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality_requirement
    ADD CONSTRAINT cat_modality_requirement_id_requirement_fkey FOREIGN KEY (id_requirement) REFERENCES public.cat_requirement(id);
 o   ALTER TABLE ONLY public.cat_modality_requirement DROP CONSTRAINT cat_modality_requirement_id_requirement_fkey;
       public       postgres    false    208    2757    207            �
           2606    16776 E   cat_transact_requirement cat_transact_requirement_id_requirement_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_transact_requirement
    ADD CONSTRAINT cat_transact_requirement_id_requirement_fkey FOREIGN KEY (id_requirement) REFERENCES public.cat_requirement(id) ON UPDATE CASCADE ON DELETE CASCADE;
 o   ALTER TABLE ONLY public.cat_transact_requirement DROP CONSTRAINT cat_transact_requirement_id_requirement_fkey;
       public       postgres    false    207    2757    209            �
           2606    16781 B   cat_transact_requirement cat_transact_requirement_id_transact_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_transact_requirement
    ADD CONSTRAINT cat_transact_requirement_id_transact_fkey FOREIGN KEY (id_transact) REFERENCES public.cat_transact(id) ON UPDATE CASCADE ON DELETE CASCADE;
 l   ALTER TABLE ONLY public.cat_transact_requirement DROP CONSTRAINT cat_transact_requirement_id_transact_fkey;
       public       postgres    false    2751    209    203            �
           2606    16419    USERS user_role    FK CONSTRAINT     m   ALTER TABLE ONLY public."USERS"
    ADD CONSTRAINT user_role FOREIGN KEY (rol) REFERENCES public."ROLE"(id);
 ;   ALTER TABLE ONLY public."USERS" DROP CONSTRAINT user_role;
       public       postgres    false    2747    199    201            F   r   x���;�0 �99E.��v~v6,!�H�z�s�J�{hD���|�k�}3�����[��$ N�C��9�$ ���O�V�Ww�s:tw�iu��5�����
3%��'Xk�8�+�      J   7   x�3�tL����,.)JL�/�2��/H-J,�,��2�J-.��+NL�I����� {�       H   �  x�u�M��0���S���{�fX !��CB&1��d�ș�����`wZ�ЍU�e}�z�$|9��'���Ї�眦��)�y��	�a{`��5Et�8)��1��0�R|�~	��_�?��s��;Xk۶��.�s7�H�Pr}N���:dNaä�76Q0����(�眾�����1d���8w^.����B�ϣ�(�Umy_�-�u�:a��؟?<����ݛW�aJ�i<%"������B��B�),�V7�����Ǜ͕|���['�1\�Kߥ�1e����K��׸�S�Z�cvR���i���uJ ��	FQ:!�&����w�T�0����~�q�r͡쏾_��%��vL9�ŵ�f�MO�x$( ��쿬��B�h�����.APnSİ�b��M�k{��!�jW�����_�|j!�{�ԍ      N   5   x�36�44266�LL/MT(�/IL�I�L+�KI�M�+�W@7��4����� �I{      Q      x�36�4�26�4� �D�r��qqq A
      P   
  x�mP;N�@��S�(���\�H���1�)��̄Ʉ��� ;��`���{�]�����H<��)@��Іa��}��00�c5W�Ek��5&��戓�ͅʹOݾ�u4Ǳ��u��C�K]	m��`�#YY�㔜�\¢��"���d=�rŀޞ��<c<ٟ�#�N.^����tm��M��Z������Q{v�� ��wGѾ�<�3(��KĪ � �c0V��c���/���
&��d���x&>I��M�e�mـ�      L   {   x�3��4210�)J��,IUHIU�)�R(H,JT(�,�L�KI��OI,V�b�P�B	�N��/JM��WHJ+�Vqr[r��s:��:*8��F(s&��&B�-�O�/622*����� ��'Y      R      x�3��42�2��`�Ls��qqq A�/     