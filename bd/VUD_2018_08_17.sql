PGDMP     0                    v           prueba    10.4    10.4 �    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    16393    prueba    DATABASE     �   CREATE DATABASE prueba WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Mexico.1252' LC_CTYPE = 'Spanish_Mexico.1252';
    DROP DATABASE prueba;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1255    16768    usando_for()    FUNCTION     �   CREATE FUNCTION public.usando_for() RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	resultado integer;
BEGIN
	resultado :=1;
	FOR i in 1..10 LOOP
			resultado := i;
	END LOOP;
	RETURN resultado;
END
$$;
 #   DROP FUNCTION public.usando_for();
       public       postgres    false    1    3            �            1255    16772    usando_for(integer[])    FUNCTION       CREATE FUNCTION public.usando_for(array_p integer[]) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	resultado integer;
BEGIN
	resultado :=1;
	FOR i in 1..2 LOOP
			resultado := array_length(arreglo,int);
	END LOOP;
	RETURN resultado;
END
$$;
 4   DROP FUNCTION public.usando_for(array_p integer[]);
       public       postgres    false    1    3            �            1259    16402    users    TABLE     {  CREATE TABLE public.users (
    id integer NOT NULL,
    user_name character varying(255) NOT NULL,
    employee_number integer NOT NULL,
    description character varying(255) NOT NULL,
    rol integer NOT NULL,
    user_register integer NOT NULL,
    date_register timestamp with time zone NOT NULL,
    status integer NOT NULL,
    password character varying(300) NOT NULL
);
    DROP TABLE public.users;
       public         postgres    false    3            �            1255    25007 /   user_auth(character varying, character varying)    FUNCTION     	  CREATE FUNCTION public.user_auth(_user_name character varying, _password character varying) RETURNS SETOF public.users
    LANGUAGE sql
    AS $$ 
		SELECT
			* 
		FROM
			public."users" 
		WHERE
			user_name = _user_name 
			AND password = _password;
	$$;
 [   DROP FUNCTION public.user_auth(_user_name character varying, _password character varying);
       public       postgres    false    199    3            �            1255    16751 K   vud_create_modality(integer, character varying, character varying, integer)    FUNCTION     D  CREATE FUNCTION public.vud_create_modality(OUT code_modality integer, _code integer, _description character varying, _legal_foundation character varying, _id_transact integer) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
DECLARE code_inserted_modality integer;
BEGIN 
	INSERT INTO public.cat_modality(
	code, 
	description, 
	legal_foundation, 
	id_transact,
	status)
	VALUES (_code, _description, _legal_foundation, _id_transact,1)
	RETURNING ID into code_inserted_modality;

	RETURN QUERY SELECT code from public.cat_modality where id = code_inserted_modality;
END 
$$;
 �   DROP FUNCTION public.vud_create_modality(OUT code_modality integer, _code integer, _description character varying, _legal_foundation character varying, _id_transact integer);
       public       postgres    false    3    1            �            1255    16735 )   vud_create_requirement(character varying)    FUNCTION     �   CREATE FUNCTION public.vud_create_requirement(_description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO public.cat_requirement(description,status)
	VALUES (_description,1);
END
$$;
 M   DROP FUNCTION public.vud_create_requirement(_description character varying);
       public       postgres    false    3    1            �            1255    16755    vud_delete_modality(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_modality(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE public.cat_modality
	SET   status = 0
	WHERE id = _id;

END
$$;
 7   DROP FUNCTION public.vud_delete_modality(_id integer);
       public       postgres    false    1    3            �            1255    16737    vud_delete_requirement(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_requirement(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	UPDATE public.cat_requirement
	SET  status = 0
	WHERE id = _id;
END
$$;
 :   DROP FUNCTION public.vud_delete_requirement(_id integer);
       public       postgres    false    3    1            �            1255    16710    vud_delete_transact(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_transact(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
	UPDATE public.cat_transact
	SET status= 0
	WHERE id = _id;
END
$$;
 7   DROP FUNCTION public.vud_delete_transact(_id integer);
       public       postgres    false    1    3            �            1255    25027    vud_delete_user(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_user(_id_user integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$

BEGIN

UPDATE public."users" SET status = 0 WHERE "id" = _id_user;
return true;

END
$$;
 8   DROP FUNCTION public.vud_delete_user(_id_user integer);
       public       postgres    false    1    3                       1255    16754 (   vud_eliminate_relation_modality(integer)    FUNCTION     �   CREATE FUNCTION public.vud_eliminate_relation_modality(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
DELETE FROM cat_modality_requirement WHERE id_modality = _id;
END
$$;
 C   DROP FUNCTION public.vud_eliminate_relation_modality(_id integer);
       public       postgres    false    3    1            �            1255    16774 (   vud_eliminate_relation_transact(integer)    FUNCTION     �   CREATE FUNCTION public.vud_eliminate_relation_transact(_id_transact integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	DELETE FROM cat_transact_requirement where id_transact = _id_transact;
END
$$;
 L   DROP FUNCTION public.vud_eliminate_relation_transact(_id_transact integer);
       public       postgres    false    1    3            �            1255    16764 B   vud_insert_transact(integer, character varying, character varying)    FUNCTION     �  CREATE FUNCTION public.vud_insert_transact(OUT _id_code integer, _code integer, _description character varying, _legal_foundation character varying) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
DECLARE _id_transact integer;
BEGIN

	INSERT INTO public.cat_transact(
		code, 
		description, 
		legal_foundation, 
		status)
	VALUES (_code, _description,_legal_foundation,1)
	RETURNING ID into _id_transact;

	RETURN QUERY SELECT code from public.cat_transact where id = _id_transact;

END
$$;
 �   DROP FUNCTION public.vud_insert_transact(OUT _id_code integer, _code integer, _description character varying, _legal_foundation character varying);
       public       postgres    false    3    1                       1255    25018 c   vud_insert_user(character varying, integer, character varying, integer, integer, character varying)    FUNCTION       CREATE FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying) RETURNS void
    LANGUAGE sql
    AS $$ 
	
		INSERT INTO 
	public."users"
		("user_name",
		"employee_number",
		"description",
		"rol", 
		"user_register",
		"date_register",
		"status",
		"password") 
	VALUES (_user_name,
		_employee_number,
		_description,
		_rol,
		_user_register,
		'now()',
		1,
		_password);
		
	$$;
 �   DROP FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying);
       public       postgres    false    3            �            1255    25005 *   vud_log_insert(integer, character varying)    FUNCTION     �   CREATE FUNCTION public.vud_log_insert(_id_user integer, _description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO 
	public."log"(id_user, description,date_time)
	VALUES (_id_user,_description ,NOW());
END
$$;
 W   DROP FUNCTION public.vud_log_insert(_id_user integer, _description character varying);
       public       postgres    false    3    1                       1255    25103    vud_modality_data(integer)    FUNCTION        CREATE FUNCTION public.vud_modality_data(_code_modality integer, OUT id integer, OUT description character varying, OUT legal_foundation character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN 
	RETURN QUERY select 
		a.id,
		a.description,
		c.legal_foundation

	from 
		public.cat_requirement as a 
	LEFT JOIN
		public.cat_modality_requirement as b
	on 
		a.id = b.id_requirement
	LEFT join 
		public.cat_modality as c
	on
		b.id_modality = c.code
	where c.code = _code_modality ;
END
$$;
 �   DROP FUNCTION public.vud_modality_data(_code_modality integer, OUT id integer, OUT description character varying, OUT legal_foundation character varying);
       public       postgres    false    3    1            �            1255    16756    vud_modality_record()    FUNCTION        CREATE FUNCTION public.vud_modality_record(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT id_transact integer, OUT code_transact integer, OUT description_transact character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN

	RETURN QUERY select 
	a.id,
	a.code,
	a.description,
	a.legal_foundation,
	b.id,
	b.code,
	b.description
from 
	cat_modality as a
LEFT JOIN 
	cat_transact as b
on
	a.id_transact = b.code
WHERE 
	a.status = 1
order by a.id asc;

END
$$;
 �   DROP FUNCTION public.vud_modality_record(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT id_transact integer, OUT code_transact integer, OUT description_transact character varying);
       public       postgres    false    1    3            �            1259    16726    cat_requirement    TABLE     �   CREATE TABLE public.cat_requirement (
    id integer NOT NULL,
    description character varying(5000) NOT NULL,
    status integer
);
 #   DROP TABLE public.cat_requirement;
       public         postgres    false    3            �            1255    16734    vud_record_requirement()    FUNCTION     �   CREATE FUNCTION public.vud_record_requirement() RETURNS SETOF public.cat_requirement
    LANGUAGE plpgsql
    AS $$
BEGIN 
	RETURN QUERY select * from public.cat_requirement where status = 1 order by id asc;
END
$$;
 /   DROP FUNCTION public.vud_record_requirement();
       public       postgres    false    3    207    1            �            1255    16723    vud_record_transact()    FUNCTION     1  CREATE FUNCTION public.vud_record_transact(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT total_modality bigint, OUT id_transact integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$

	SELECT
		a.id,
		a.code,
		a.description,
		a.legal_foundation,
		count(b.id_transact) as total_modality,
		b.id_transact	
	FROM
		cat_transact  as a
	left JOIN 
		cat_modality as b
	on  a.code = b.id_transact
	WHERE
		a.status = 1
	GROUP BY b.id_transact,a.id,a.code,a.description
	order by a.id asc;

$$;
 �   DROP FUNCTION public.vud_record_transact(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT total_modality bigint, OUT id_transact integer);
       public       postgres    false    3            �            1255    16752 3   vud_relation_modality_requirement(integer, integer)    FUNCTION       CREATE FUNCTION public.vud_relation_modality_requirement(_id_modality integer, _id_requirement integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

INSERT INTO public.cat_modality_requirement(id_modality,id_requirement)
VALUES (_id_modality, _id_requirement);

END
$$;
 g   DROP FUNCTION public.vud_relation_modality_requirement(_id_modality integer, _id_requirement integer);
       public       postgres    false    1    3                       1255    16765 6   vud_relation_transaction_requirement(integer, integer)    FUNCTION       CREATE FUNCTION public.vud_relation_transaction_requirement(_id_transact integer, _id_requirement integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

INSERT INTO public.cat_transact_requirement(
	id_transact, 
	id_requirement)
VALUES (_id_transact,_id_requirement);

END
$$;
 j   DROP FUNCTION public.vud_relation_transaction_requirement(_id_transact integer, _id_requirement integer);
       public       postgres    false    1    3            �            1259    16413    role    TABLE     g   CREATE TABLE public.role (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);
    DROP TABLE public.role;
       public         postgres    false    3            �            1255    25015 
   vud_role()    FUNCTION     �   CREATE FUNCTION public.vud_role() RETURNS SETOF public.role
    LANGUAGE sql
    AS $$ 
		SELECT
			*
		FROM
			public. "role";
	$$;
 !   DROP FUNCTION public.vud_role();
       public       postgres    false    3    201            �            1259    16674    cat_transact    TABLE     �   CREATE TABLE public.cat_transact (
    id integer NOT NULL,
    code integer NOT NULL,
    description character varying(2000) NOT NULL,
    legal_foundation character varying(3000),
    status integer NOT NULL
);
     DROP TABLE public.cat_transact;
       public         postgres    false    3                       1255    16775    vud_transact_record()    FUNCTION     �   CREATE FUNCTION public.vud_transact_record() RETURNS SETOF public.cat_transact
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY SELECT * FROM public.cat_transact;
END
$$;
 ,   DROP FUNCTION public.vud_transact_record();
       public       postgres    false    3    203    1            �            1259    16694    cat_modality    TABLE     �   CREATE TABLE public.cat_modality (
    id integer NOT NULL,
    code integer NOT NULL,
    description character varying(2000) NOT NULL,
    legal_foundation character varying(3000),
    id_transact integer NOT NULL,
    status integer NOT NULL
);
     DROP TABLE public.cat_modality;
       public         postgres    false    3            
           1255    16821 %   vud_transact_record_modality(integer)    FUNCTION     �   CREATE FUNCTION public.vud_transact_record_modality(_code integer) RETURNS SETOF public.cat_modality
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY SELECT * FROM public.cat_modality where id_transact = _code order by id asc;
END
$$;
 B   DROP FUNCTION public.vud_transact_record_modality(_code integer);
       public       postgres    false    3    205    1            �            1255    25101 !   vud_transact_requeriment(integer)    FUNCTION     �  CREATE FUNCTION public.vud_transact_requeriment(_id_code integer, OUT id integer, OUT description character varying, OUT legal_foundation character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
	
	RETURN QUERY select 
		a.id,
		a.description ,
		c.legal_foundation
	from 
		public.cat_requirement as a
	inner join public.cat_transact_requirement as b
	on a.id = b.id_requirement
	inner join public.cat_transact as c
	on c.code = b.id_transact
	where c.code = _id_code;

END
$$;
 �   DROP FUNCTION public.vud_transact_requeriment(_id_code integer, OUT id integer, OUT description character varying, OUT legal_foundation character varying);
       public       postgres    false    1    3            �            1255    16753 B   vud_update_modality(integer, character varying, character varying)    FUNCTION     )  CREATE FUNCTION public.vud_update_modality(_id integer, _description character varying, _legal_foundation character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE public.cat_modality
	SET   description=_description, legal_foundation=_legal_foundation
	WHERE id = _id;

END
$$;
 |   DROP FUNCTION public.vud_update_modality(_id integer, _description character varying, _legal_foundation character varying);
       public       postgres    false    3    1                        1255    16736 2   vud_update_requirement(integer, character varying)    FUNCTION     �   CREATE FUNCTION public.vud_update_requirement(_id integer, _description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

	UPDATE public.cat_requirement
	SET  description=_description
	WHERE id = _id;
END
$$;
 Z   DROP FUNCTION public.vud_update_requirement(_id integer, _description character varying);
       public       postgres    false    3    1                       1255    16766 B   vud_update_transact(integer, character varying, character varying)    FUNCTION     (  CREATE FUNCTION public.vud_update_transact(_id integer, _description character varying, _legal_foundation character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
	UPDATE public.cat_transact
	SET  description=_description, legal_foundation=_legal_foundation
	WHERE id = _id;
END
$$;
 |   DROP FUNCTION public.vud_update_transact(_id integer, _description character varying, _legal_foundation character varying);
       public       postgres    false    1    3            �            1255    25020 l   vud_update_user(integer, character varying, integer, character varying, integer, integer, character varying)    FUNCTION       CREATE FUNCTION public.vud_update_user(_id_user integer, _user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$ 
	BEGIN
	IF (_password = '') THEN
		update 
		public."users"
	SET		
		user_name = _user_name,
		employee_number = _employee_number,
		description = _description,
		rol = _rol, 
		user_register = _user_register
	WHERE 
		id = _id_user;
	ELSE 
		update 
		public."users"
	SET		
		user_name = _user_name,
		employee_number = _employee_number,
		description = _description,
		rol = _rol, 
		user_register = _user_register,
		password = _password
	WHERE 
		id = _id_user;
	END IF;
	
	RETURN true;
	END
	$$;
 �   DROP FUNCTION public.vud_update_user(_id_user integer, _user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying);
       public       postgres    false    1    3            �            1255    25016    vud_users_record()    FUNCTION       CREATE FUNCTION public.vud_users_record(OUT id integer, OUT user_name character varying, OUT employee_number integer, OUT user_register integer, OUT rol character varying, OUT description character varying, OUT id_rol integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$ 
		SELECT
			a.id,
			a.user_name,
			a.employee_number,
			a.user_register,
			b.description,
			a.description,
			b.id
		FROM
			public. "users" as a
		INNER JOIN public. "role" as b
		ON a.rol = b.id
		where a.status = 1 order by a.id asc;
	$$;
 �   DROP FUNCTION public.vud_users_record(OUT id integer, OUT user_name character varying, OUT employee_number integer, OUT user_register integer, OUT rol character varying, OUT description character varying, OUT id_rol integer);
       public       postgres    false    3            �            1259    16396    log    TABLE     �   CREATE TABLE public.log (
    id integer NOT NULL,
    id_user integer NOT NULL,
    description character varying(255) NOT NULL,
    date_time timestamp with time zone NOT NULL
);
    DROP TABLE public.log;
       public         postgres    false    3            �            1259    16394 
   LOG_id_seq    SEQUENCE     �   CREATE SEQUENCE public."LOG_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public."LOG_id_seq";
       public       postgres    false    197    3            �           0    0 
   LOG_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public."LOG_id_seq" OWNED BY public.log.id;
            public       postgres    false    196            �            1259    16411    ROLE_id_seq    SEQUENCE     �   CREATE SEQUENCE public."ROLE_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public."ROLE_id_seq";
       public       postgres    false    201    3            �           0    0    ROLE_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public."ROLE_id_seq" OWNED BY public.role.id;
            public       postgres    false    200            �            1259    16400    USERS_Id_seq    SEQUENCE     �   CREATE SEQUENCE public."USERS_Id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public."USERS_Id_seq";
       public       postgres    false    199    3            �           0    0    USERS_Id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public."USERS_Id_seq" OWNED BY public.users.id;
            public       postgres    false    198            �            1259    25034    address    TABLE     �   CREATE TABLE public.address (
    id integer NOT NULL,
    postal_code integer NOT NULL,
    colony character varying(50) NOT NULL,
    ext_number integer,
    int_number integer,
    street character varying(255) NOT NULL
);
    DROP TABLE public.address;
       public         postgres    false    3            �            1259    25032    address_id_seq    SEQUENCE     �   CREATE SEQUENCE public.address_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.address_id_seq;
       public       postgres    false    211    3            �           0    0    address_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.address_id_seq OWNED BY public.address.id;
            public       postgres    false    210            �            1259    16692    cat_modality_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_modality_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.cat_modality_id_seq;
       public       postgres    false    205    3            �           0    0    cat_modality_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.cat_modality_id_seq OWNED BY public.cat_modality.id;
            public       postgres    false    204            �            1259    16746    cat_modality_requirement    TABLE     f   CREATE TABLE public.cat_modality_requirement (
    id_modality integer,
    id_requirement integer
);
 ,   DROP TABLE public.cat_modality_requirement;
       public         postgres    false    3            �            1259    16724    cat_requeriment_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_requeriment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.cat_requeriment_id_seq;
       public       postgres    false    207    3            �           0    0    cat_requeriment_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.cat_requeriment_id_seq OWNED BY public.cat_requirement.id;
            public       postgres    false    206            �            1259    25079 
   cat_status    TABLE     m   CREATE TABLE public.cat_status (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);
    DROP TABLE public.cat_status;
       public         postgres    false    3            �            1259    25077    cat_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.cat_status_id_seq;
       public       postgres    false    219    3            �           0    0    cat_status_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.cat_status_id_seq OWNED BY public.cat_status.id;
            public       postgres    false    218            �            1259    16672    cat_transact_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_transact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.cat_transact_id_seq;
       public       postgres    false    3    203            �           0    0    cat_transact_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.cat_transact_id_seq OWNED BY public.cat_transact.id;
            public       postgres    false    202            �            1259    16757    cat_transact_requirement    TABLE     x   CREATE TABLE public.cat_transact_requirement (
    id_transact integer NOT NULL,
    id_requirement integer NOT NULL
);
 ,   DROP TABLE public.cat_transact_requirement;
       public         postgres    false    3            �            1259    25053 
   interested    TABLE     �   CREATE TABLE public.interested (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    last_name character varying(100) NOT NULL,
    "e-mail" character varying(100) NOT NULL,
    phone integer NOT NULL
);
    DROP TABLE public.interested;
       public         postgres    false    3            �            1259    25051    interested_id_seq    SEQUENCE     �   CREATE SEQUENCE public.interested_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.interested_id_seq;
       public       postgres    false    215    3            �           0    0    interested_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.interested_id_seq OWNED BY public.interested.id;
            public       postgres    false    214            �            1259    25095    modules    TABLE     c   CREATE TABLE public.modules (
    id integer NOT NULL,
    name character varying(100) NOT NULL
);
    DROP TABLE public.modules;
       public         postgres    false    3            �            1259    25093    modules_id_seq    SEQUENCE     �   CREATE SEQUENCE public.modules_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.modules_id_seq;
       public       postgres    false    3    223            �           0    0    modules_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.modules_id_seq OWNED BY public.modules.id;
            public       postgres    false    222            �            1259    25087 
   permission    TABLE     �   CREATE TABLE public.permission (
    id integer NOT NULL,
    id_user integer NOT NULL,
    id_module integer NOT NULL,
    permission smallint NOT NULL
);
    DROP TABLE public.permission;
       public         postgres    false    3            �            1259    25085    permission_id_seq    SEQUENCE     �   CREATE SEQUENCE public.permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.permission_id_seq;
       public       postgres    false    221    3            �           0    0    permission_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.permission_id_seq OWNED BY public.permission.id;
            public       postgres    false    220            �            1259    25042    request    TABLE     r  CREATE TABLE public.request (
    id integer NOT NULL,
    folio character varying(50) NOT NULL,
    id_user integer NOT NULL,
    "date_creation " timestamp with time zone NOT NULL,
    observations character varying(500) NOT NULL,
    id_status integer NOT NULL,
    id_address integer NOT NULL,
    id_interested integer NOT NULL,
    id_transact integer NOT NULL
);
    DROP TABLE public.request;
       public         postgres    false    3            �            1259    25064    request_audit    TABLE     �   CREATE TABLE public.request_audit (
    id integer NOT NULL,
    id_user integer NOT NULL,
    accion character varying NOT NULL,
    id_request integer NOT NULL
);
 !   DROP TABLE public.request_audit;
       public         postgres    false    3            �            1259    25040    request_id_seq    SEQUENCE     �   CREATE SEQUENCE public.request_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.request_id_seq;
       public       postgres    false    213    3            �           0    0    request_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.request_id_seq OWNED BY public.request.id;
            public       postgres    false    212            �            1259    25059 
   user_audit    TABLE     �   CREATE TABLE public.user_audit (
    id integer NOT NULL,
    id_user integer NOT NULL,
    accion character varying(255) NOT NULL,
    id_user_modification integer NOT NULL
);
    DROP TABLE public.user_audit;
       public         postgres    false    3            �            1259    25106    vulnerable_group    TABLE     �   CREATE TABLE public.vulnerable_group (
    id integer NOT NULL,
    description character varying(255) NOT NULL,
    status smallint NOT NULL
);
 $   DROP TABLE public.vulnerable_group;
       public         postgres    false    3            �            1259    25104    vulnerable_group_id_seq    SEQUENCE     �   CREATE SEQUENCE public.vulnerable_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.vulnerable_group_id_seq;
       public       postgres    false    225    3            �           0    0    vulnerable_group_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.vulnerable_group_id_seq OWNED BY public.vulnerable_group.id;
            public       postgres    false    224            �
           2604    25037 
   address id    DEFAULT     h   ALTER TABLE ONLY public.address ALTER COLUMN id SET DEFAULT nextval('public.address_id_seq'::regclass);
 9   ALTER TABLE public.address ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    210    211    211            �
           2604    16697    cat_modality id    DEFAULT     r   ALTER TABLE ONLY public.cat_modality ALTER COLUMN id SET DEFAULT nextval('public.cat_modality_id_seq'::regclass);
 >   ALTER TABLE public.cat_modality ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    204    205    205            �
           2604    16729    cat_requirement id    DEFAULT     x   ALTER TABLE ONLY public.cat_requirement ALTER COLUMN id SET DEFAULT nextval('public.cat_requeriment_id_seq'::regclass);
 A   ALTER TABLE public.cat_requirement ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    207    206    207            �
           2604    25082    cat_status id    DEFAULT     n   ALTER TABLE ONLY public.cat_status ALTER COLUMN id SET DEFAULT nextval('public.cat_status_id_seq'::regclass);
 <   ALTER TABLE public.cat_status ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    218    219    219            �
           2604    16677    cat_transact id    DEFAULT     r   ALTER TABLE ONLY public.cat_transact ALTER COLUMN id SET DEFAULT nextval('public.cat_transact_id_seq'::regclass);
 >   ALTER TABLE public.cat_transact ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    203    202    203            �
           2604    25056    interested id    DEFAULT     n   ALTER TABLE ONLY public.interested ALTER COLUMN id SET DEFAULT nextval('public.interested_id_seq'::regclass);
 <   ALTER TABLE public.interested ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    214    215    215            �
           2604    16399    log id    DEFAULT     b   ALTER TABLE ONLY public.log ALTER COLUMN id SET DEFAULT nextval('public."LOG_id_seq"'::regclass);
 5   ALTER TABLE public.log ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    197    196    197            �
           2604    25098 
   modules id    DEFAULT     h   ALTER TABLE ONLY public.modules ALTER COLUMN id SET DEFAULT nextval('public.modules_id_seq'::regclass);
 9   ALTER TABLE public.modules ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    223    222    223            �
           2604    25090    permission id    DEFAULT     n   ALTER TABLE ONLY public.permission ALTER COLUMN id SET DEFAULT nextval('public.permission_id_seq'::regclass);
 <   ALTER TABLE public.permission ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    221    220    221            �
           2604    25045 
   request id    DEFAULT     h   ALTER TABLE ONLY public.request ALTER COLUMN id SET DEFAULT nextval('public.request_id_seq'::regclass);
 9   ALTER TABLE public.request ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    213    212    213            �
           2604    16416    role id    DEFAULT     d   ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public."ROLE_id_seq"'::regclass);
 6   ALTER TABLE public.role ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    200    201    201            �
           2604    16405    users id    DEFAULT     f   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public."USERS_Id_seq"'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    198    199    199            �
           2604    25109    vulnerable_group id    DEFAULT     z   ALTER TABLE ONLY public.vulnerable_group ALTER COLUMN id SET DEFAULT nextval('public.vulnerable_group_id_seq'::regclass);
 B   ALTER TABLE public.vulnerable_group ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    225    224    225            �          0    25034    address 
   TABLE DATA               Z   COPY public.address (id, postal_code, colony, ext_number, int_number, street) FROM stdin;
    public       postgres    false    211   �       �          0    16694    cat_modality 
   TABLE DATA               d   COPY public.cat_modality (id, code, description, legal_foundation, id_transact, status) FROM stdin;
    public       postgres    false    205   6�       �          0    16746    cat_modality_requirement 
   TABLE DATA               O   COPY public.cat_modality_requirement (id_modality, id_requirement) FROM stdin;
    public       postgres    false    208   ��       �          0    16726    cat_requirement 
   TABLE DATA               B   COPY public.cat_requirement (id, description, status) FROM stdin;
    public       postgres    false    207   H�       �          0    25079 
   cat_status 
   TABLE DATA               5   COPY public.cat_status (id, description) FROM stdin;
    public       postgres    false    219   L      �          0    16674    cat_transact 
   TABLE DATA               W   COPY public.cat_transact (id, code, description, legal_foundation, status) FROM stdin;
    public       postgres    false    203   �      �          0    16757    cat_transact_requirement 
   TABLE DATA               O   COPY public.cat_transact_requirement (id_transact, id_requirement) FROM stdin;
    public       postgres    false    209   (-      �          0    25053 
   interested 
   TABLE DATA               J   COPY public.interested (id, name, last_name, "e-mail", phone) FROM stdin;
    public       postgres    false    215   �1      �          0    16396    log 
   TABLE DATA               B   COPY public.log (id, id_user, description, date_time) FROM stdin;
    public       postgres    false    197   2      �          0    25095    modules 
   TABLE DATA               +   COPY public.modules (id, name) FROM stdin;
    public       postgres    false    223   �3      �          0    25087 
   permission 
   TABLE DATA               H   COPY public.permission (id, id_user, id_module, permission) FROM stdin;
    public       postgres    false    221   �3      �          0    25042    request 
   TABLE DATA               �   COPY public.request (id, folio, id_user, "date_creation ", observations, id_status, id_address, id_interested, id_transact) FROM stdin;
    public       postgres    false    213   4      �          0    25064    request_audit 
   TABLE DATA               H   COPY public.request_audit (id, id_user, accion, id_request) FROM stdin;
    public       postgres    false    217   64      �          0    16413    role 
   TABLE DATA               /   COPY public.role (id, description) FROM stdin;
    public       postgres    false    201   S4      �          0    25059 
   user_audit 
   TABLE DATA               O   COPY public.user_audit (id, id_user, accion, id_user_modification) FROM stdin;
    public       postgres    false    216   �4      �          0    16402    users 
   TABLE DATA               �   COPY public.users (id, user_name, employee_number, description, rol, user_register, date_register, status, password) FROM stdin;
    public       postgres    false    199   �4      �          0    25106    vulnerable_group 
   TABLE DATA               C   COPY public.vulnerable_group (id, description, status) FROM stdin;
    public       postgres    false    225   85      �           0    0 
   LOG_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public."LOG_id_seq"', 36, true);
            public       postgres    false    196            �           0    0    ROLE_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public."ROLE_id_seq"', 3, true);
            public       postgres    false    200            �           0    0    USERS_Id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public."USERS_Id_seq"', 34, true);
            public       postgres    false    198            �           0    0    address_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.address_id_seq', 1, false);
            public       postgres    false    210            �           0    0    cat_modality_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.cat_modality_id_seq', 57, true);
            public       postgres    false    204            �           0    0    cat_requeriment_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.cat_requeriment_id_seq', 367, true);
            public       postgres    false    206            �           0    0    cat_status_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.cat_status_id_seq', 1, true);
            public       postgres    false    218            �           0    0    cat_transact_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.cat_transact_id_seq', 101, true);
            public       postgres    false    202            �           0    0    interested_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.interested_id_seq', 1, false);
            public       postgres    false    214            �           0    0    modules_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.modules_id_seq', 1, false);
            public       postgres    false    222            �           0    0    permission_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.permission_id_seq', 1, false);
            public       postgres    false    220            �           0    0    request_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.request_id_seq', 1, false);
            public       postgres    false    212            �           0    0    vulnerable_group_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.vulnerable_group_id_seq', 1, false);
            public       postgres    false    224            �
           2606    16418    role ROLE_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.role
    ADD CONSTRAINT "ROLE_pkey" PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.role DROP CONSTRAINT "ROLE_pkey";
       public         postgres    false    201            �
           2606    16410    users USERS_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.users
    ADD CONSTRAINT "USERS_pkey" PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.users DROP CONSTRAINT "USERS_pkey";
       public         postgres    false    199                       2606    25039    address address_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.address DROP CONSTRAINT address_pkey;
       public         postgres    false    211            �
           2606    16791 "   cat_modality cat_modality_code_key 
   CONSTRAINT     ]   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_code_key UNIQUE (code);
 L   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_code_key;
       public         postgres    false    205                       2606    16789    cat_modality cat_modality_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_pkey;
       public         postgres    false    205                       2606    16731 $   cat_requirement cat_requeriment_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.cat_requirement
    ADD CONSTRAINT cat_requeriment_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.cat_requirement DROP CONSTRAINT cat_requeriment_pkey;
       public         postgres    false    207                       2606    25084    cat_status cat_status_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.cat_status
    ADD CONSTRAINT cat_status_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.cat_status DROP CONSTRAINT cat_status_pkey;
       public         postgres    false    219            �
           2606    16690 "   cat_transact cat_transact_code_key 
   CONSTRAINT     ]   ALTER TABLE ONLY public.cat_transact
    ADD CONSTRAINT cat_transact_code_key UNIQUE (code);
 L   ALTER TABLE ONLY public.cat_transact DROP CONSTRAINT cat_transact_code_key;
       public         postgres    false    203            �
           2606    16688    cat_transact cat_transact_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.cat_transact
    ADD CONSTRAINT cat_transact_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.cat_transact DROP CONSTRAINT cat_transact_pkey;
       public         postgres    false    203            	           2606    25058    interested interested_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.interested
    ADD CONSTRAINT interested_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.interested DROP CONSTRAINT interested_pkey;
       public         postgres    false    215                       2606    25100    modules modules_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.modules
    ADD CONSTRAINT modules_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.modules DROP CONSTRAINT modules_pkey;
       public         postgres    false    223                       2606    25092    permission permission_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.permission
    ADD CONSTRAINT permission_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.permission DROP CONSTRAINT permission_pkey;
       public         postgres    false    221                       2606    25071     request_audit request_audit_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.request_audit
    ADD CONSTRAINT request_audit_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.request_audit DROP CONSTRAINT request_audit_pkey;
       public         postgres    false    217                       2606    25050    request request_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.request
    ADD CONSTRAINT request_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.request DROP CONSTRAINT request_pkey;
       public         postgres    false    213                       2606    25063    user_audit user_audit_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.user_audit
    ADD CONSTRAINT user_audit_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.user_audit DROP CONSTRAINT user_audit_pkey;
       public         postgres    false    216                       2606    25111 &   vulnerable_group vulnerable_group_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.vulnerable_group
    ADD CONSTRAINT vulnerable_group_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.vulnerable_group DROP CONSTRAINT vulnerable_group_pkey;
       public         postgres    false    225                       2606    16815 *   cat_modality cat_modality_id_transact_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_id_transact_fkey FOREIGN KEY (id_transact) REFERENCES public.cat_transact(code);
 T   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_id_transact_fkey;
       public       postgres    false    203    205    2811                       2606    16838 B   cat_modality_requirement cat_modality_requirement_id_modality_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality_requirement
    ADD CONSTRAINT cat_modality_requirement_id_modality_fkey FOREIGN KEY (id_modality) REFERENCES public.cat_modality(code);
 l   ALTER TABLE ONLY public.cat_modality_requirement DROP CONSTRAINT cat_modality_requirement_id_modality_fkey;
       public       postgres    false    205    208    2815                       2606    16797 E   cat_modality_requirement cat_modality_requirement_id_requirement_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality_requirement
    ADD CONSTRAINT cat_modality_requirement_id_requirement_fkey FOREIGN KEY (id_requirement) REFERENCES public.cat_requirement(id);
 o   ALTER TABLE ONLY public.cat_modality_requirement DROP CONSTRAINT cat_modality_requirement_id_requirement_fkey;
       public       postgres    false    208    207    2819                       2606    16776 E   cat_transact_requirement cat_transact_requirement_id_requirement_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_transact_requirement
    ADD CONSTRAINT cat_transact_requirement_id_requirement_fkey FOREIGN KEY (id_requirement) REFERENCES public.cat_requirement(id) ON UPDATE CASCADE ON DELETE CASCADE;
 o   ALTER TABLE ONLY public.cat_transact_requirement DROP CONSTRAINT cat_transact_requirement_id_requirement_fkey;
       public       postgres    false    207    2819    209                       2606    16832 B   cat_transact_requirement cat_transact_requirement_id_transact_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_transact_requirement
    ADD CONSTRAINT cat_transact_requirement_id_transact_fkey FOREIGN KEY (id_transact) REFERENCES public.cat_transact(code);
 l   ALTER TABLE ONLY public.cat_transact_requirement DROP CONSTRAINT cat_transact_requirement_id_transact_fkey;
       public       postgres    false    203    209    2811                       2606    25021    users users_id_fkey    FK CONSTRAINT     m   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_id_fkey FOREIGN KEY (rol) REFERENCES public.role(id);
 =   ALTER TABLE ONLY public.users DROP CONSTRAINT users_id_fkey;
       public       postgres    false    201    199    2809            �      x������ � �      �   _  x��[Ms�F=ÿb��*J$H�O�$'��cŲ]�*_����>���r�O�Q�\�7�l_�  @��,y�J�J 0���zzf�}���Pg�p�L��3,�,AO�ĕ��H��(,��2�Ȍ���J%r%���L"S4��\G�R�\��YηUν�:ѱ4k�����X����P�Dsq�,dd�g�UZdB,��B�Y�L�eZ�Wj'HZ�7eDWg�ϳX�Y�T�c]F2�o/����K,}���bʘ���먌7��#��J#�@�Ef��2��O��EBVl�h�'�gqI�2����Q
������{GЮT��Wh�]ŢQ�Z��6�IdA]����Adv���Z6�h��c��H�����y�ҧ�1�	h:ܼ��ґv��~�Ž���=����c��Ȱb��O�=��J���ͱ,��l��ž�&�TW1�x�riL���(,���3��q�ȭ����A�a_\��䋷$�7h�y�v:eY_�y,���3�Q��(�)�c*�V�Xc������H�������}���.\�a s<������1��JC񣺢�3�����W��f�]���1o���	$p1��kO֬^ykk�?����:Y^�:��ϥ�N�7�D��\} ���/�.V-~�.U��o�|y����5�uy	�u��f�dd����R
M`�Q���/�������}%��)�B���-h��7�䌇#���ä8q^)UM��y?-���~Z�[�q?-�����Ӣ�<-��1�z��pv�	�Kk5����̗S�2k��Qxx �҂�tzH�k�|�i芣6��M߭�F��Q�C�V���΅_ul�q��| �� � ��ۆ60���:��� �@�`H���V�kE(���d���Zr�߫A�㽥Q8�L)d֢A��0~7$����L�O)��!�z�x�)���ub�����n��>��@���OA������'�\�z�y_۝^�L�{�C�/t�@~���b�X,?#/l��Y*]�е�]���a�_��rf��4:l[��U5h��!�b�< �#�������d!�e����A�H(BD9D0J�%2����Z�˸(M�������R�U4g(j�ӤT��j�[��º��b�ꟙ(g���0��Jte�њ�X���6΅5Nb̗�S�f�B>�F�йɸ�p�E��!��-�"ې��d��񏦄�1�3��t���oC��4V��Z��s�U���<�g?�؞?fQ�,٦�6��D�3�v�l���3d��X^�"<���*���G1��ch�3��KI;)[�� +.?��&���B
��&��6�5��3�YH�u'$��H���m�ѫs�:��W�7%"A��8���`�](��Ke��� ���:@GN�.@gKx���j"ɳ�Qx�ԕdS=�%RI�zB�9�R[@e}�� �pW��+1G���#`�K,[1��$pυ=�pa�r��7j�
e�N��2�8NN	U\Ҧ %.�����"푻G��#����
���D^e�n)n��a���ò�C��_���@�J18��_��C(/�ۤay�&�@�a/Hi�LE���3�Z.�.�J��2��L@KZ6����(��&.�%�{8�3^}Ӡ���tw�!�B+aa4�/�*�v��9}��D���F�ߡ6��4j47X���H�X�2�(;�
�Ʈ��T�L�.� r��/*T��:ˠ�y��d��X��@ y\�4�/����(��l,���J�KF�d�!1���~�6-�������-�:hH;N
%��r|�q5۟vF�ԭ@�Ht�JX�G}Q2��X��Q�}|�ǷG�o�[�9����'?�F��-�,@9&�n�V=�h{��������s�$�Mp��i�P$�>����@ ���������<zjنN0~ɡ���F��~^�R�<z���Eq�OQ7Q��3�;�6�Լji�^�6���h��=�Ru>������\�0��M����VTd����M�)zt$��z�gɷ��<_�q���� X�{j���+�{�����tAG��Iߢ0K;v�F�K:��R1��4�Ʋ��q8+u&�a����;-���0T&��3U��i#Յ�iV������h��w:�a��:诔R,�=޾�݈�1d>�L�np�l�`���M,hU�Qy�ȥ��'��8}g������y�d��`�ȧ�ꊈHQ)&�;H��R�4º/��.d�c-ou�Zr�ؕ(Ӫr�9��������ٶncA�˴!\oSeϨڧ�Z��IVm�a�	m��j�������hSG�Y^�<��U9(�V���Dȏ��b9O����,w��`�	G��9��8��U]�Q��L]�M�z\8�~�|S%�yS��F� ���.�
u��mLl��\}wě�I�|� �b�t}�O��״��TJ�M�Wkv�kFp�N��]���ɗ�7.��n�UmU㹦������hJz�	X���k�W�:�W�B� ��ѯ�e�~�̍L�������`0嫃�����Mq�}���J�Q����7�k�i�@u��of���#<�`�s]�(2��hN2v��>ȋ���SpM�U�YW#\��M���|�i��m`���p��������šfu���]6*s𴭯�ҶQv�<�f3~ZVX+liH����.��5qF[��0g@����i���`�N���n4���I�^�j�UE��m�ɱn���,_;�V �D��[��U�+����n��F�]׷@}�ݫ��
�@%K��Y� v!�L��>�[�-U,�ćN]�[g�����抶��G��:���BcZZ�t�yi� :G�9ģ�n���p�Yڻ��=�5�{k?��z�jOr(�����ק�i�����nm�����*nܞ��	��۵D��2ח�"�(:�/�	���hԘ��_�(q?y��p}C������Bo8^WW��q�����������s{��9g���h[�7��_k���bn��}G�c}�~z�zǴ�1
�n˞����m��y݊����b�gA���q��n=Z[�"��<c����w���}�̃;��7�wpv�5t�z��ɓ����      �   �  x�e�A�$!D�U��'��w���c�u6�D�̬��Wx�ϰ��\��2���g}�=�H�r"C6�~u��P/2���.J�8/�/�L����n�C�����/+D7��$}�/2�ͮ7O?\?���R�¦�L��ǹ���}��Z����}�-���?���M�w����O�Q���~n�<�4-� ��H�v�wwA���'�YQ���y��+�u}-R��[��Z�zV�p�H�}�#S�oI���ڷY�mp$e%��]�\�b/�9�9����\AKa=s��\lE
����F~u��ڜ4G1���;�ZfR�'�}-�w�iaS����6����_V��-��w�P�]�7�I-�(r���p�c�G,���<I����P���D{�zEC���h�cC�>_��AtnO��w�l;w8������M(2��8�p^9&�?�"�b�����%�����j�VI�֫Z���NA\���jgd��1��r����f	��2���3:B'� �G�Lx��Az~/[�T�a8g�sjO�5o����:Wɳ.\u����[F9x���]g��g�Ub@�9[H���C����oAxN	�8�:9Ǹ�h>�P9�<�5zl�#N����b�N-�O�g$'tfs�r���h��f���~�� %`�      �      x��}K��֖�X�W4`_(=�i�*e'Ǎ�Ѷ\4РDV�E�I�`yvC��,C�Af����]Ͻ��C��$�v�I\���Z{�׷�g��U���h�V��:[�I%i�(�M�i6e�*^fe��q�M��q]V����_�QGy�L�e���bS��2/ov?�Q�9�7e���
�Σe\��æ�VeѬ���~���ʖ�8����gx���J��ߚ��w���wuz������Q�D��V�lCo[��u���E�I�����(/���ċ<]f�QZ�J�O�ѷi�V0k�doR��Q���n2\����K���6�7��i:����4T]�6������z]I��Q�hS���M:�_.�$^�U�oMVg�8)�m�ы>�#Xf�_���f���uZհ��5� |�*a���9�aS�T1|�s:]��$MN�MW�8���Τ�]�	ܛպ*q�G
O��ZlC��P�Ѻ�����nRظm�l�<�,���r��8l�˴����~]���u︧���ӹ���-��i=E�i`�n`��.�y�uZ��+`w`r��¯e3`��i����:�U����ˈa��cJ�U�	�)p˗FA3$��-yG��)L>i�Y�8*�k>\3n!�A��;J?�>�@7�E��d�4!�h�<c�,�բJ������B�޽>=+�r�pw��8D�K�4C��U�*�G�.�<����w_X��%q���޳o�Q4;9�a��_������䡯>;�������&����h�ۨ�m����zY!Aꇷ%�	<+�]#�c+� ����u��Z �Bv�;��Op�p�g�����o�M磷<��6�f@��;�����j�t�����ݗj�eM��6��c�x�
ތ��H�pl�+�+x�mV,X���c@��fni2k���lM�<dY0�ر�$m��D�J$HU�hx���['���Ie��R젧����\��ߥ�H` 0�s4���/������Xg��(e�tQ���-��@W��p��G>�5��F9r���������e�K����it癿���rň-vW.�q�5S"�����@�G�M�T�������F69����E����y5pGf��d�b�;�fp+�]��C˞M�?�Av5��nX��\0�T���0��Ъ)���T)	M�c���*0��.T�>��]gU	bY �o���`ͧ����G�ΌN8��������~��8�Kw�q��e�^kNFo*�
V��6�����㞶\�Mڽ���w�"�tS5Vq��VuvX��(�?6)��*]�~*���9p�e�p�ǰ���9x��b�:�3���E���sh=��'�������M���!��cᢨ���tD�TI'V$��i%W*��������LG��(� ���w���d�o���7�Y{]���@�KU�k_�����=���M�Y�
/�k�ӷ�'m��+8�<k�j��[ط8��Ԭ��s�����pp�#��]��Fqp)�*�Ҭ���7t&p�������6�v�������rxsXg���ZۊR��~l�T�
b#�KX!pP�����Fxev��bT�V�3���S�
{8]�8����9��4��{{��0*���mV�m�#��i�Ҵ�����K{@�#�.K1��2�e�~��y�t��D��ɨF����T)T�?�p?����
$��S�gU0�L&�
�����pڻ�`WA&�(v��L��SHWd6��}�/x��:��;d��D�A�ޚ�����T��<�¹y�Z^�~K���$}_�Xؠ4�n@��?;�]��7�Lm���g\(Pk�9�%���"%[���Po����덑�W(kqb'nb���X����Ⱥ�ۖ�w��� �	�^"��S��Ӝ��V�1ڳ��Hi�	-�����2�B�2��0�9>3{L�xw�".���YKO`�m���	�ez��1t9D��+�V��=+�]Yp���Ϩ�������q�f��	̧�q/�q͞�a����:������O�qF�$l
\��/��Q���;����"[���h��V7�q�A��	����o�5�p0��Kx�%l�RT������A?7��
�GKT6�c/uNU�e���;;�/���,ߠUsgq~r�A/��ɟ���^{�#��m
��{ �I|�V��'�n�_���_tǭ�w��;y:i� 5�X���)�,5k�eq+|Tg�f\il��_"�N��0d�tq葋z�v|����9�����5s;@ӧ�?�Iy�YI�sŔ���U���5q�溬Vȫ��~�ɕ�ߒ��vHuK@�霌^�C�:�-+�|�B��j���E���t��*s��,X:���Po�w!x�9�ʯ��4�����3��������L@y��zYzqܙN �D�*�BWMήHޓ^S��s0�0�Hb+KD��H�^5)Y�5���$�
-�[�m�ŀ���|�V���zd�{L�?�P�O/�<� \�����[�t�:�58"�ɢy�-�𬨫�ʔPx���u��C��'p��ej*쑸V?�*=(�Φ^��g�j�}]�P�}�����D��~V�I�����$N�%�@T5ˊt���s�.�C�M2-��sYg)[��o�0}������H��h����tn\�q�3ϔh(n���.{�j��x�e&N�^��G��Ju��Eu��xtIdg]��N���k��-O>C��͓P�X7j��M|OV r1���(�[�k����6����<�c$)ԡk26�>��i� ��T��֎���>�9�r�L�ґ Ǹ�@��=+7�躁݁){�Oag���C&:�mS�v� ����+r��UU@�8������zW�_B�1�F.0�u��)�&�`�"�[X:�0�����e�U���;@Ρ����G�q�%ث������^f�Ʊ	��O����S�8'�q`����������*"]����q��x���Md.ѷ�� Sa�׫�e\~�������B�zS�v�!77a_��t	���I�_�tEy�?j�aEf;pG�h���Rz���9L�V��8���;�A�Y )��kk��Y�YTd�)P$���Ͻ�xR����M�iX�m�G���mm����!/�����6z���T�yZk����7Q}u���#Q��u��6��+WXn�|t��0��?:�[��� $1�xz��ʥ�Y����/�c��1����� v���:H0�!��zэ*��y/#� ����r1�S���
�07 Ͳͦ���PǚU�;B��^A��:��{0�⏥�H��W�ŁO�г5mv�=��⭋b�M���QtCVJN
��Uʒʑ7��f��F�~�P�]� �'ʾ\����ۣ��	bx(�.��;ݫ���T�:	S��]K���,j�'Ƒ��(:AO��ـ�A*X�)7l�����T/"�oKwE�
��Z'�7�j�*�'�O<u������3�"ſ�8�,�6���_:G���5���E^��瀬Ѽ6򈤨KF"�Y�#�hp�.�"i���'�4��N?-�[���ݗM))m� 1{�!��w� �T#�˜o N��5c�*R���T�얱f�HV�M�rE)Z�q��P�>|c�@��^>'�SǺX�5�^K�F��������8m���
CE�L<�<���B��~��4�`�؞u��R�ˢ���`���}@�9�n�-%#*��bC[�'(-�x���V�ɯ>�돘m��3ʬ��K�rV������D���F-L8�m<km#��"�<�&�6lj�r�����cx�b�?]�����k�ˆ?b5@j+'�<=��{��,n�ݨ%z
ڦ�μ�	|���H�.K�����4N� rl(�/1q4oj6���"��b��w C
,�+2o�)�t<����T�3�T֔��8�ޏ�u(���郞�2�)|� �,��8�����ȳ.{�'����U��X���[?��Zi���Y�A�*�	6�@d3-�vs    �6W�O3�Nf�1zCCD~����P3��d>z���#��`���u�#�ژ��"H�%e�D�(�A�)bg]A�"n>a0�P5��IK#��Q��֔��
V�o֍ư��B�����p�9Ձ�5-��dϚ��xI'��9f��e�~,ړ����e���2mi�יsT��MȚ�\^2E.�MԱP6�y��9�Y�Y q0�G�^��j29�*z��ͻ�f�|����f"�)�+}&�Ʌ�B�@v:���p�����c��NC:d֑D��h*nXK��Sӣِ��ʙ�h�5(�E��S�[{S�~�t(����5�R�s�&
�D�e��ب��ᔕ��k�|4_�Ve�a�8&y��\-��A��kt��]Z#�@���@����N.�����h�R���dr��eu�Imp���!�z�:�'1\���ܭ����[D?r.
�R����F��@+y�I�%:=1��"�eY%Y�؄F�*�r�P+���"�^��WƆ;l`�'�ЉM5wś<<��t#�ﱹ��ŵ��eG���L��4 �Qo���U���4�c�3���mO�dt��lc^�}�����&&���?N��֎�� �Kr1�Rۀ�5�9���i��ol>E��9�&q>z�j�@�q�Roَ[UM���R=`�Ҵ)u��$����٘���|��-V$d�Ng��BW:%^�gp)�+m�]^+U>|��u�Q�K��~�Kpu�i?b(ߩUUIH_GC��ʼؖ��_}GwQ_�B��XG���BO��<�s��b�ɶVwG-��E���ǈ��7��t2��@R�;�Q�9_�N+�\�w�rR<ӵ�M�=�Ӄ����ng��q �NG�U�PQ>::�r�~r�U@����I@sx��:�W���.%���,BP�O?�ė��-������ w����p�U~7��{ځ�,��O �r.FY�i��>ZJ@���g�s�/�a�2f��un�\�A��vQP�
����=*��Ɉǌ?A�Z|�l����k�~`��/F-<�G�ؖ]b������j�0����[&uH�O�>�*�N�Z/�c]x��j	�D)���r�����U�U���/b�C��
r}"ɶ%Ir��n���3���u�`*�KT���D&(I=��sUQ�>%�\���E�,g>5	����0��.�1�ۼl[���U5`�a>PK�i�B�:K����,�\1a���ʁʳ"u�4����;�U�:>?	"
��9�]�����5�D�MU�v1�эu<"&ŷ�&KSt=Z4i!��W�\������ɑ@�x����Y]5)b6  �` YgthՖ�w���[ZB)ɹ��Qyc��~yC�TbJء,�M~����������#Ɉ�.\���&��ה��ۏƍ�ޥ��go�kx���N��u1�z�M>K����r�z�&�6j��^y&���3�Ɔ��Oo �>Z5�/�3�
J�:B�Njy*�$�x	�V�H'���i��)�u#�l�&��Ɵ��s����}�(�?b��M<lJ��qVW��[v݇�����fL�c��5
�g��ƙ#�J��{��m�HD!RwW+"w��sJS���t|�JFUjkJ-E�>9>�=�bX��I��h�t�]e�ؕ��;����f�R���>�ۘ��_]w������#���y�����(2�OH)�]��c�_ϧ_��z:�u�w=��T4b�������m"�&�|���8��Ew���
�z��Q��
TP�eX���)O#
瘩�a�?y��O�K��ݗ��wD�fÚ$�4�NQ��T��~sX�-��_�'��42�(B���N��tخ��y���Y-�r�.�
hP ��GyB1\E *���蹕�|������p�iQn`��t��YS[��  e"PDM�j��"�q�(���i�*J�U&\E�%���H�:�%LL�ta�/p����Q'�"*�	5��L��`�M����d2�����^Cv�`쀂�9��Ђ3p�4=2���~^-�ʦA4�	�"�h2�1��_��3x��(.���u�j_a����Z.'��Tk1������=�>�M�.����N@�2��4R:������j�BI����|��/n�<�͚-)j�q��Ȓ�wMC/'��K��&��وgG���;������8��~3�6��v����њI?� �ֵ�ӠU�dРuB�5~'���ՙ��Oj�n3]����`l������L��܈�3�c��o7t-��3{�[jz �8@a�Z9��+/4߬�:W`X���w{�5橃m�CPP�1�L�H,_�ۍ�5E������������2����vL V�Z��W�,*��|��n�V�#P.?�c[��Q���O�k\��Rw*�ia�z��"����������z�l����QXd�!����ՔO.��s�A����X�`3��G��E���S�������8���W�I�+��ep��=}���eG$}�����؉z,AS��C}�4�i�փҽ�Ƣ��X���5'�G� eď��K' ��؄z	q�v�s2 W��a!9����b���U�x-�����tyJ�%I���Hr�ݦ����+xUuˤ���-o�p�d����A;S��ͳ����+P�5�o{�*%ǗN��?�:8�~��-�������~��$���=�8]W���t;�ɓ!Y QZ��<�ҙ����������}���V�g�1�Rf�o%s�0���D���Tɥy��N�ֆ�2����3��9��08�rcy�C�̫�[�+M?K4��0}s|�7����'�r=s�q������?����P�y��4Z�)%�ZQ�W�V�FK�0 �~��Z�@��%0�E���䐑f��mbؠ�i��s���F>�C���*�[�$ ���puF	��c�n�'�Ȁ��L����G�{�miO�y��R�+�d��0�O���-� }�sUΒ��ʤ��ǝ�Y!I��\���aN��N��5����Fa�,E�Y�\(�HæBZ
~�ǭ�^ӂ;E�=> ����[i�Z���κK�OAq�)�ˋ��χ�>���
�A�+�{��Ӌ����4\��{��=�T8�4��k����S����!�9�V�DֺV�� ����Q���������z�.����������YT�s^g��A��$л���wO�Gh�8�im��`���c�&n����I�'5�����/`�n��m�#�U����N��G�~o���e��'H�����h�\j8�Q$��C�7Q�lة�y�)3�����Z	�h��vO���	�
�1�s��f�$����`�7���\�~A�:b�U�d�=�@7�܊m'�B�Ac,�Iך�ը�������I�bg	�Z�������+�������2�|���W��-�ʏ%M��鲎�2�Y���QKS.[g�KBm����5�ֻ/k.�B� C8���Sq�N�p ��	6�nb��A��wěTdpNm�VҘ@�<�RW����:<�<�l+����?��|�G[��<�e9"9ð_�=�$� ^c_R7|����u�5(TKY�\([M����X�y��MyM��̾����a�{���[�.���K�Y*^S��r��։n0�_�X���E�G1��9b�A�'q�n4��-J"݄(H�{��L#D���*/����F��^T>�֨#+�cubN�u��/A����  I�|bPx2��W`���o�<�����@ �\[��`%�8}�F��x�xG��<���e|1h�0U���)�[pIJ��d5�y�I[�P�ߋ��X:9:Y��q+X)��j�VYoCk��}�j��m'2i�gq���E2Z�Jt,Q��	�Y�ح٫Zc���!�K*u�P��2��!�q�}�j_��[�b�&���ѷ0�D{�o/�]����)�LJ���b�o�(�āWc�k�A5ə��;���G/��|��ǂD&�[*j�v]�Ujh�Do=�3�܌    �j�G�j�i2����!sY�@�L�=�h��ă�ٌv�m��Xc�3�*��d�˸߲οI�����D��Q:��W�5s����[�%�7�Śʒ%��&́��ͣ�?��3��2��,{z��[�LÍ������U�h7��Xh?)�g5b<.j��Z�
����ez~��,�����14��;\D�b�w��z�n����ϯb������.rn��r3��N���`=9��w���Q��ҧYy�>����A6�}ӑ�*a̞T�^3��$���SoJD�41�	6)�'�%��'m�O�AFz��%Y6���4w}�qn�>��Ժ�
k|��T�G�F>Ǣ�l�� G�;��!�$��^�iz��p!�$&�T.�#�A�X`U���(W40�#J��qx�~��(�q��O�B�aK�@�(Fj���HʛM&>�ʈ�-yy}a�Hv{#�M3�h���U��on��@��]-S�;#nh2��cM�fĞ���&�y	�oKv��>���[|�1;�<H�ökf�#�R�P� �w�,�>�袔SI�T��u�����fA?��L)h�e�D���Y����<h`�$u�ږ�x�!sȀ��J�a5I&D<�ULb��0K�:�,elK��R7��� ���ы5��H�S�3J�$_��a&�3�E��u�7N�٣�sM_#x��`a���¬�V�6����t���ނ�x��W]CXi���jV�I�Mz��u����w<P-��M�_UϛM����9����8��ўG�e�4n:��i˗����zx��� 3�RMy�|).)��y�%O�?�z�}�݋w��^���X����
e�c�ͺd �|p�%���zS�Ա��uE�~��`�o��_��l�~�T��Ŭ�dJ�UZ���e��������>��Vw�L�G#��ڄ�:ǹ��jS����ぞu��4���D�q��r��z㑫cZ��/.����m�Y⑙ְ
@�G��y�x��n���N�D+�< 5�'i`e� Z�����=��.7�Ո�'���1.L����Hls+�p[�J�����#����+��U��(�.'T�q(劊M�8��7����8�����-jo,�������wn��������n�����-cG��o���I��1�^�9���n���o�S���AjΰS����6��6�Ngr�O�c�yG� A�t���sIO��FZX�6�NC�]�汶:�S�a�`��\V���/�.[�7��$F� �eX�|Х2�M���v֌�e��x��R1q�ggh��fO���W�Џ��eV}��10}l��\�A�n�D[����$CV�����7y���ڥ��|j�O�k/Utz�uY�)�"մӱf-`���܋Bo�\b�K�>���m8����!��=�ȋ�j�-p�/�-戮,��� �(J��=� uy�r�w�W�rB2)�.�8f"��̄j2?i���~'<L�\�UI���$YMX��c�M������!�*h�4ˏ� ���O5'z�Hp��#�_���؃���rB�v�w�2�v����'U۟����] �V�ܰ���a��=�y�t�ݦ�q%j������q����T�5,��ݺ`�9>�:#���U�8]�V�WnM������ߐ/�p��l$��=G��;��KR�����S��A��D���ܶk'��qK���s���S�W	���z�CUh�����P���!F�($� ����4е�l;nF�ܱ�V��i��a�KK�H/�|žB�p��ATqП�hI�o�-ٶ�/��,Vy�����S��lɳKó��+�b
vɮ:����8���
�6QY�:��i�	˓Ux��ř���ƣ�W�+�����ق��KQ�M_ò���yL��M��N{3���̧C�|
��Uժm�����tq@���da�6mY_
jg�h��������IQ��1�O� t':���f�ˬ\��?�Cr�Y���ݖ<"�!�x ��b%�c�-�m���jmj����hq[�ނOp��a{W��u�+��6��(]N�j����X�f����5���*X0h�nc�B���Y�͸��8t7�軪Y��%,�]���?���@m )���/�r��w���4c��bp���ٳ蓶�e���<L ���,��a���� ���ME�T!��H_w�V^2���\�/oO{����p�t��*cu��@f̅�)�n��6w�o���$S1�8p��H����+X�F�X],��1L�PY�����r��?�~�Y8^�&'� �H_[&�W
��Xc�C.&�&eLT�0[�c�v�}�`��3���!����蘿�I�w8(tF��q��e�Ka������f#N�v�m:�9v
ӄ��U����c�4ڸ�!2"�.ME�޽���8z�����=���}>�.��a|p�C>�Ǵp*�-a��jtf�z�/�`:H��+�ƃ@:�^�	{��=	b��2i��[7�& &D����B���>ʾaj� qm�H��Q�x��I����J�W�����ؑ�j"uT���E"m�����[e���:R�U|��u4�i�+A��ݝ吞�,�6O��0��?�"ۣZ�[N�i����&3�3��V�/�����u.I]SCYF>ps�1���&
}��a�l���}��.˼=�b��4̴�P@}/:�<��n�e�;��=��s�w������<*v�1[lKf�7����(��61�լWO�͊��6�bMx�b�:�F�@�	'��'�F�����lnWC
CRK3g-T2�V��=N翣�8�s�O8�4%�1(i�Z�S7	A�!�W��>L�&q�e��0�vױ�q�Y�Ԡ�b�@����u�Q�O�۸.D�Ÿ�T0�r �NA�Qdt(�	�L���"�y8$_1���	Q�R�v_��'��C����7�������l�I���:�H)����O�^�S��:9a� ��p0���.T�Rk�j���.��Zx��0�܏KNy�+"Q P�-'8�v����*(�|���I�����\z,�;0�
.z�n�W�U��[]�XK���5H.��'�FB���l�v[%���[Η�"��$Ë[�RT��� 1��Q�~�C�(T��E���p�B�
��[���p)�G��8��+������g��n�f�����3J��3�σT�����W��n��8�#���@�5E<ZU���䱋	�� [����
k`��t辊8���6�M�R�_g<Ƙ����U��AI�]�]��U,OG����.M ���g������5��c�r�y�����G�<�Ȣl��WVT�CA:�[��p��xQe
�&��,�b�t�DN3��)�>�2ԃ�-��(��0+	�pZ	�o�$���x�'ˣ�kl��I�k1n�����t��>��q,0��`o]�g�}iO�� �����G�By�m��E������n��}��39�7��F�v���RTy���N��q��D��W%�E�Qn�N�>��|��/�{FWO��d�9ʘQ�e�6ꋛ��%�`�o��^����	ŝi��$�j����q�)��mU����s B���F$��W�܆6�
�"ϬIH�)�6���t��k��,I�,Ϗa�d���.գ(�2{x��Z�À�1��o�W_b(B��ӕ�>���xg��t.E-W�I7�:5����J�ێI�	�|�\8c�mRiwI��5�#T�,[��1|�}�>/$$�%����96�V�����;m������{�ë{��=�Uܽ���P��&�ь.�gԂ��	0pu�|+�R�o=�l_�1��t�M�
g�:��V�
z��75��#�W�l�m����ݴI�0��a����,�+G�}>����m�'���7w���z|�M>0�@�Q{ղ��=$��D�X -��tV��Z`<�Q4��h�4i�A#X��٩	����x���������{®&I�-�("�	�e�i�Xr|2�9F>.ʭ�Nn��s    D� �7&l�������@�����H��He��lݰW���zqtt줔���*�!��iz`E�����0��$�Lz�z�BJx�h��k�<��R�Ni/��7J��}�6���ow=T�pzF*�s8D�q���D���r;߾+�ݻ( ����0U�u]Ga�PX�	ߩ���',Y������H�Z���*<;��.���6�ë((�\�m������P	���7�����^�y�W?<����W�o_���9�?�����w�v��\�굆�>:Џ/X?�w���B��-_<�{ez���~���b�9��f��1h���ע����&uҥ.����O�iT�!��S������]ru����l�qv��ӿW����|����j�����M]�wLo3YV����AFC�@5o�j� /o)�����!y�����T^:��#�T�����uq�Q���ى7��8p`�9h���[i���AKi�F�glVj�p����`ӯ�0Z��{^���v�+�Bo�f>l�+EI�K�&(f�h�������E�
V���i��{�!�c���1����9W}6`�:���F߱����_��_E� F��R'ֶ���;ĮD����y�A�RHo~�+NJO�� �)K������Q�a�z�����E�!���U�B��d��Z�_^��9���,�����A�?�m��eV4T6����0m�I�@o�G���ks�q��H~�MW
�a������ܪ3��K�B���Co%y�%���+[p˥�g/$g4���'����LIޭÿ�&��Z�|�%=f,�ȶ�R�P�C�����Q'�Lo��:}�b}lS������S���l�kq����V�JG%��޶���c� YOnR����3@�azU�h��(������|:g[ZG��;ދ�`��c__�V킋��c���W�R�xCr_�
�����_d�1gA�4h����ʩ���$�;������?z#�f��"��N��3ݑ/E���YrJ��`���?�;�l*o��$͚L�qgs-�.���5�Ó԰�':Mv;x>z�
ʎ�z��U�XM�林���(�$��0�zC���uϮ8	�z��g��]�(x���� �}��)&!A�G89rҷ�[�h�H��9�쀊���L��q`�I�r��Ƽoс� ��&�hRG��z�ͱ����U��y����7ɤX�[�J������p��5�O�p5:		��[��5+�D�tS�:b�œ�=k�_/Y�k�B=[K��
2AܫhN�Qp�7�a|һ��k!�� ����Y����c>>R�w�;�������v�]3.�N��=o��;?�F�������_yN�B�]�l�j
�8=�{�#f�/�'�Pw!��q@�/C���gt��~�Y�����Eκ��/�
~���C��t���&ڑ��=|��&�nix�YP�`'��=t�p�ٻ}b�������<��XU��[�;��Ey�7���]�@�:�{�{�sadr��O�;�fwx�F�u�G�"��W�*��jVҽT��.�b*hEu�K�(ZT�k�;̂�O��m��V�c�xK��tt�&��]�G�n�`c�~��(S����v*a��r >L?u	*�3��Y N)��S��l�{U�� sqK�ߤ�7� O��=��5���`d�������bB����K7��8�~�-7��~���b��d���� ���&�7ҩ��%�W����$F�=�O�G�ilO��=���Vyi:���L�
Js��́G@5\� Vb�3	��I�y�g�s^sH�bz�g�`�b���H�+�c2�q4�V]�\J3yJ	3u�kL���̆�r��1s���m�3!0w2Cl�U��6 W�C����L5�g\�O6?YJ������������K���o?\8�>�*��t�^����N�SI�����"x4i��S2L#o�Ȝ�à��?~�b������Z���2�b��a<3�@|a�pD�@�.�i��ͥ@#�al�m?��Q;b�l��a` wuk�����j��^�V�.���{l�0�k�sW�!��BX�Hf�F"FcF��u6�}�=f~6�L�]�OԆWMfMX��b�v�:nwo��:�.B(�y��3�?EW$}Ie�K��Mi:�,�O.�S=P�D�ڑ��)ay�Ƀ���h��ы�7GcP7��׹$��@�2&���=XT��ؒ�65�)n(�3�,��l �N��>�Y0�Ը�}�X�$������BBb��,��w�8�,�h�H�<Q*�qSH�;b�,����ۈ�~�Nd����u�L	f漌��)�UB�����
���(J��~����=���[<�vj�6j��wGq���3~쥡����{;���,�G�E&7Y��Mƞ�9bH�4{��M`�����%efQ�D��N�Ό|�吐�����aK-���sB{S6瓳�,8����F���%(٥�~p%��䶌��<��3�\��U�������.͓�]���d��!�3�P��|r��u���K�L9�/��(��&L�DԸ�6`4�R�|�{1����l� �Ҁ�R�:HueQS�O#n��~"���NG�yR`�P���[Fo�a �N�eM�qg'l�?�ѵ��Qa8aHsE�6(&�J��0��P�o ,��lA�Hĝj�+{݁�R�������&!Ǩ	mH�҃�I9=D~4�3Ҿ\}��F��%A��X0BB,��>��zn��.��]��FRy�!K@�;
/C���K��&�[2�mL��әm�L�-�5�2��Q�􁯊���X<�ڝߥ"�B��9 �R:FxU�A^��� 8�H]����ܮ���� k����m��	�0�
�f�S��M4L�׍��l�bK�n���*W����A1�Q(r9�Сep�6���g��|�;���ݏ����B���2�0�1��v�a*�E'x�-��JMd���K0��&
��ä2q�[�*���wXq�船��C.��KT�.e� ˎ���G9�`c:
 �Lbt�ӷ����a�ߞ���rc"���33�KN�7lm?c������J��AG>��4A��D��B	�1<�Y��8L�i!hZ ����Pq?�d��L'����4�M�KL�N�b�e�+�-IJ���u-)���Q�u�g`,�й8 �F�]���3܇\��v|e6�o�S)�2k4,M�ʔ%p�d��|w5��@F�����ͷ�S W�B6ә��+������욘�gXg���a;�b��sŝ�Sl�H?)㢖�|w��傓^���OԄe���c�2�K���"�`;VZ0�Y4I�(`��6��lj`^Qû��k�4O� �ti�,��I��ZT�=�#��p}-�<َx�	�v��`��p<����R��B�o���fK�w�n�PK4W�H@ψ� hD$X�߅d���:X�DX�x��u�;�e|�WH�{�!(��)s'�U��:;W��_�K$�M�����8��7�M �۔$�� ْmEe����5Є����B
�#�-hcp���d�t���2��	�Qz�s�p)�sa�S����綦�|c[�E�r-ז�Q�Kt}��ֱ�2���a)O~%8|kl��Ul�Rq�J�V�uAE�C����V�l[�
��X>�Ŋ��(*.9N;�{\�>�))d���^�)�VgT/>����~ctG�!����wLL�S딶�'�4G�"�h�[�"g�+$b1��H���[���r�2�v�L�j��6E�L���������������i�6�S,,��
��CT�'dRc<&[MQ����	2��@���7���\�M�V5B�-N`e��I���S`����t1x�e�T�`��+a������^y�B�w@�jﾬ�9�z���k�O޹{�ﲣ�h��-v"3��� O}z�~�}�V�%k�\��/��|:=��'�|�s�Y �ֱ���W��0�;���
�a�6}��y�h^�À�ǣ�; �#'���_���b!<x� �	  � �Ji�ڠ����؏j ��Q&�����~c��g�m\�`�[Q����h�Y�w�Ƃ�D�@ی�Լsĸ�=/t�i`����Rt�J��t�}�ei \`=��}���Ef���$\����I���U��!=�56n'����(��y"��}�N;D��Ň�i}9>�R�Ѝ�:�+��_&UZ�*��^|[F����^+&7p2{~�u��)���?'������E5��˒r��`�������H��ą����"�c���vlu������ 9F�S�*���C��6&�Dg���&�!ж�m�M���#��T���@�I��4�����F�WEZ@��Ek�����`$�4�j奄��7ƀ
�v�0�7�	A���p�5"RH���ə����&���pgZNhr�i鱙��&�L6I�e3�=γ�1]6� l�m�Q��i�(Y���a�s���10C(��`nS]:IC%�BH��?i�k�&��ŐNE�aאX��-��c%eW��M?>���t�5%��"��%@������K�;���9>h3"���z��y��-R�Ϛ�
�}�[>{�'�	p������mWW��١>٩oT:6��m��]r �v����#׻_r��i���b�K�ΩEA7���r�T$n�E��&��E�V?����5�c+���\W��X���Z֛ ����:=o+dܭ�j����P�hz&]��9��S�!�Tryh�J	����-�Q�����$��P�i���n(�W����d�����T�vö�����"�,�iJ[il(0@~h��Z�G�R���c{�y��MJғ e>!�W��}l>�0�*i�D��>��K�l2Ҷv:-�B��T���|��BO����mZ��S⳼)i^��9s�=�>�n�Cic���A�Y�t�NG���hf�6N[���ghQ���v����W��w_�r�3:g�E��EI� ��-�=AN9=���=<6�t�w��tU<n g穥�!W�ȁAxX�@���<�4d�$,�9&�C��ZQK��}j(
2��lm1u�ME���8u��P��c���j�bҕ2 ��Yݘ:����IQޚ.J�<G�'�֡��c�����8����ľ�z��4%�!U��Vbpg��=i���¦�D�M�B�oEԺ ���<Ḁ��t�[va�+l#�9	SC��.�|��#�2�_������5FJk��|�懚�#�k�yW��y`��m������CpB��P�<�>�	{�(�LT�9�E{s �q ��r�}y7�R.���3��}`s��(�@N����!�)���8�w?䔞�9��S�������j���9@5%�΀|�Xߓ*��bWV��C�Șٖj�џ4���5�Wh)�ݰ	�ѳ�-L�&Q4�|��zJ������#�Z���. �Q��ǓQ�9zdU��a:�c�x�e�	�u~<{���GhW���P*,|��m�y2VYQ��|��l��R�	�.�GZ�[�s�Ww��/�kȂ�'d��z�H�[x �$����.v۪6IZ��cr��f0ۢ���������U89�f��ca���O�i�����ȯ�&�Θ�>��C��#�k��u.��jZ�ٞ�x�^�V�\�D��ؽw��M�2��@:�'�fL3;����b�;�N�Tn��U�I{f�% ��������F�Y���@�f��dzu����6�� Í�=X�4�f���߈�вSYc
���9�t�C��Pd=�l�
�l�d%�Z�c��,oM:X9�j�N��F�~�J�YS��-��������{��!o��ph��f�4�R�:+�xRb����N�+c�D>�ʫ�7p�Q��!ڷ&�;�9�����Qz�mp'���+A}���U,�xi�����?��̈́:1z��Go��}s���1O����[��6c�bd�ݗ���K��tki�zW�KTL�i�;�0��$��괇��ǒ%1�'�����!��1̲d��8(��F���bL���SVЊy��BA��K=>�y|��iW�K�M˻���ژK�6R�ii>?E��-Ap	uB/�Y�o�/ę�}�N{:oeD�}�"f��jx��܁Ǫ �_%Py �������^_+8Ia[-)��]�)��z���O=$A�UnB�v�fav�����2�6� {� �dط@	tW�.s�����#i&/�T��Y�Ȳ`��1|�lf�;�Jb�Y�қ��+��(�˾�Nx�cV��Nc�"���p]>���nx���Ԁ��m�C���b,j��U�;�'Zq׬�+��������O!r���b��*���8��Z0S,w�rA��A�ٖ+�2/���F����^�!PS1H�h]��4$�kל���$�_~5��~�{���W��dB3���3��y�i��	�)��0s�8�{`�w�gԗ�K!�U��'Q���9RA<xҲ�^�Js��_����?�?�H      �   �   x�3���K/J-NLI�2�JM�H��R�B2Ss�9KK�2a�%aN��ԢD��T��)�vt93dC�%�9�R�R�2�n��.M*N�C���"��(?��"93?��� IC����P'�cH��qqq 6#T�      �   6  x��\�r�F>�O1WW�Z��ZNqK�����!�!8�'�������9��+_l��@ i��%с�0 �����ڱ��k��X���@��Fl&X��S�_�R��8�!��Y��ߧ�b�H-W�i'�sm������[���ݐ}�!��h��zofH؂+�p��L�J.�_�/�;1���'}J��cS��L�a�G_]�EP�/���n��@�u��(��3v"n�f=Sq fr.5KG������7�f8���9M�t)�vu�y���J�����x<������2.]��K�a�\���`�o��D^����_��7"���	n�ϴ?,���ᖽK�@�Q�P�^����x�NiK��9�q��-�\N�㺥��R(+M���
E��2.x[&J�а�f~��8�Q�`��O��8z�_������Op�ۉ�+���w��r�g�g��Kmh�|��H�#���ӫ�ϸ�3]a�����e�u&��;�P0��B �P?�BQ�A���w*X"�W��s2<�Imx���*�	M7��"Nd�&�^%C�j�dɧ����71'������ڟ�Y��	�N�@�@-�URX�*�����9������hu7K�&lZ�hF�J���!�l�����g&'�w��%K/g����1�K��1�+$��
Y!.=�Џ%�2�)�~a��b���v��5{�%�'s��7�>�p}�h�/�@ϣ�#�Ȅ%��܊/f��f��W.���Y\���xx=�k�U�-r�AC����(��b�'�z��>�����ا���T��Y�K
j���ş�R8	�K��v΍a\��ƨ�Yդ um4#��PC��q�=^�{ט�u��������-�8��bb�'��x��fY
���ҫ��ڭ��{�&@��\y�Y(�ȕVVY]a�buӪJ�s�ӻ�ZiWh7s>p�F�o��J�%�CM3]A߰�s�����`k�=�����-Y��B;�*�+�>�����!s�j�=3h�VäE��](Y4$��[�g΅�����!�G\=6�����/��#�GЏ�?�[���"ȇ���{�	���0J[��W+my�,ɂ'Z�xA��o�Mfk����]�}Jӣ4B�t�&- �m������O�Z/����~��V$8�>S��@�P �֨ئ���T	��R.S���;��)
[����amY*$e�R/���eE�wgaC0@���`@A4m��s�����c�'���n�s���~�K��Kر����kqwPˊMdVƵ�%纷\?�)�\�:�Upl��;1r�xk?��>J��m�[�bdN�K۶Au۶���$�1�煠�����){���ۣ�ǧ�?�������8��:��ffK��.)>�?n�̎NT7�ݒ��K����{�Xo:������Gz��vJ��p��}��\����[�W��`1�4j�3�͚,&7�kLL�a�5{Sx�6���(����1/J�z�{e�ze��o�co�=��������T��A�����m��G"[��I	���2jN9�T��9�mV��([�
(�S+������B-��X�kD�3�f���S8�L�i���Z}U*��V֜(�G�YMy��#�|2/���X��P��'z��0�����o{��[�8՝������lǣ����*��Û�j����������ELh|�W5B��XÎ�vM�H�#yE����4������lPd�ɩ|�[f@'^]��,ZOOߵ�s�ӭǪf�����u�`���Y��]��,���k�jS׭�͘mg�z3�=�Wq
K4�=�\��iC�P\�:)e��َ�� ����kV���:d��s�z��׾p��;���L��;瑽��u��{{f��B�	fD�ٙ�KCE��X7[l\Z�7�r��=�+�V��� q�%^�������N}�VH|����W�X>���S�]��	��p#��CT?��՗Ȭ��ߵ+��W!��R����� �b���eQ�WI´�����H��������E�X�9�c;>uZ|���t����^�m��1�E��7/o��2�H�#��ګ�7I�E����)�<n`9]o#�
/%�u�tP"�3�M���]��	���5z�R� �����;�����4���~�E/�F�6�e4�J�$ͳ�82i�s�|���.U�W[��K�u���K �˓���k���ݺ�<���~��iy96]���>b�ے�`���Wc��{�EqJȻS{�ڴ���1tT�Mc�,���:�4�_�Q&����|��U~JEG|2�6�O�:�B_��R���x��d\xOq1�$=��k�tG:duO°q�C�')�ӺNV��^���ppX��C�u�|v���g��3� �b���<�IX�a����.>eR\c^����ns��&�
l��˕�[q_��L���iv?��0���h��)e�t*�b��8��l�?�e7��=��� ��}|��\��C*���WJ~�w��l�q#���I�Xu�*�i'Rc}��sW寑1�_���~��7�����k�	G�9&W"(/��o�gr�e.�R!I�yY����c��z���i׆5�I�>Jx����uM!8J����	h�)f�_�I7��L���r"�m�IhG���~%J��E90w�<�/��+���vr|���L?跳ַ�����1��`�1Q�z������Z�I��p������Jа�O�z�A��|QŪ�ߩXr*�к�򞷾��X�OųͿP�pE;k|�w���Ύ����\Z=0d�s�@�UL4��o��A�>��hV������J��w6|	2׊Ϲ�fy�������Z�]���dXNVrll6&$VwA$)jŕ�/-�^Bp�l��eɛ"��W6J6�vB55'�\1�<2���bmG3��:>T�bl�R1��� ���|���>I�e
�O�݋�=i2�9a%��w
���fߜ���t:c��v�>�ZPXEk��%�mH�sgCnSa-j��-���zr}�������"D��6c�#ͮ�f|a�}Ê�b���'rpv�.���BetK�7(�]"��:��?1���&���}�KM!:�Z�/F*���(���5��>p}���.w`���܇�В�%�8��c���kӻ^:0�����s���-�:�S<�P,��vN���5��;ڠRA��3ɯ��W��7H�9��-a���ױz��x�Pj���\�8H�fC��(�)b5��W�'��Mм��λ�O�tyP�`��у��(ů�">����k��o,˟�'�m��X-�G�m�߮�6\iϓZ�䘑�����&	��/�^��?蟥j      �   �  x��V[��)|N3��x����P�I��������?�Q�꿾��k�W�e�#T��3���ngH9R��P>N�s�l�365&�b�Nt�a*�c�cս5B��03`9���p9��|-8����R�GX�y�y�=L4t���)i͙��`9�����ο'��c,�� xD�����̆��M�P)t1�� ����;,�оx���G��G�:@h���N�䈈�\+���V��G�*�vѽ��=N{h�:�>���<�ejEp�p���J)����r���tǓ�q't�'0�h������x��0O�����:B��bģ>D���N-����7��{�<���r����1�b��F�\*P��C�1���C"�`�c��B�VQ��2��Lm�1˟�@ەQ:��$��k����flXSS *j��?^Z��֋����:���a肝.t��n��5������x�׸~�a �gy��KvQ�"��yt�=���,h/6�C>�u��>~g�i_nY����,�n���i��tg����n��$��V���{Q�؏1�N���CA���P`�Aߡ����rJ9����3W ��}�4Г�^���E��M
B/!D����=�HAQIEb��H|Ў�G���(��'�кP��N���Գ/>o�R�¨P(9����b`Xl��sѠ�B�r1����|�������s���FK�!}��4�+t��V��MԘr��<�\�]�b�V���~�3�O2qkW����ˬ5M�`7jn5�]�4(��j��la?y�7;��*\Lɦ��p�K�ir'�މ*\C� t�vמ����Uە��e��m��>
e+}J�C����}��܈��޹�N�s���/��{�;a�������r�g���o�}[��|��Ȳ�GC��rm�6~��-���ҷ�&��aN��O��>�23�v.���vno>:���o4[��k�j��f�~_p�|��M��y�E؈4���k��d�Qc٬�H��|(�}\M֞%���Ֆ�ʆ2W�o6��-�o����r#NS��³�臰F=�#/�z���O�;F���q���Xr�&�{�s&ƕi�*S	s�Q�-���>M
�,�<�c�>��Q��d�t^yH���xP{)�L�JS�\V���P�la��J�I�E������#�4�      �      x������ � �      �   j  x���An� �ur
.4�0f�#t�rhd)�v6�U�Ћ�`EJ�M*� ����٘��Q��s,è>S�1�G5�CIӨ�QM�4�ST��N1�U?���9����ΐ2����Q�h�x�OI�~>���5>�9��l��k�]�� ��C��q�r�������c��jKd��!Z:�!?c�`�"j��E��P˯�E��fD'�Y��~�K4�,G���ce�R"u�Qg]��z�J�D��1�u�OL˨����M�9��5_,7��UCm-,$ޑ5G9���80N�h_ڞ�j��l�{�^���Շ��RZ�o��<�8����d'^��&��Q$�����W�&`��E�����ӣ~�      �   M   x��M
�@е&��4m$%>�I���|�f
o����PEɀ��C:҂7�Ҡ��v������B�=�|N��J�|      �      x�3�4A.#0m����� }      �      x������ � �      �      x������ � �      �   1   x�3��/H-JL�/�2�tL����,.���9�R����rR�b���� b��      �      x������ � �      �   w   x����� ��3L�@����,��C�^�
���4z�_O�B4����6d��Զ�>r��s(�!/(�9��A������Ю�bтE�8<0�@Gռ�Es�v�k�y����ы�r�zk��S1�      �   �   x�UPKj�@]�O��u��2�E�H�
��ٌ�ј���wʢ��X���xz�j��0QH����!���g�L[K���j��M>�ãۨ��#t�A��ᄝѨ��F��H�r-�F-�#����=:c
�V���#��,����!SklXϧ�������=G%qB[�.io���K�ˣz�V8�^z�3C@NET_��a��"}���>�i�GIǂ��81D����yiס-��XUU��yx1     