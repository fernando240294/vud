PGDMP         	                v           prueba    10.4    10.4 @    :           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            ;           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            <           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            =           1262    16393    prueba    DATABASE     �   CREATE DATABASE prueba WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Mexico.1252' LC_CTYPE = 'Spanish_Mexico.1252';
    DROP DATABASE prueba;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            >           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            ?           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16402    USERS    TABLE     }  CREATE TABLE public."USERS" (
    id integer NOT NULL,
    user_name character varying(255) NOT NULL,
    employee_number integer NOT NULL,
    description character varying(255) NOT NULL,
    rol integer NOT NULL,
    user_register integer NOT NULL,
    date_register timestamp with time zone NOT NULL,
    status integer NOT NULL,
    password character varying(300) NOT NULL
);
    DROP TABLE public."USERS";
       public         postgres    false    3            �            1255    16440 /   user_auth(character varying, character varying)    FUNCTION       CREATE FUNCTION public.user_auth(_user_name character varying, _password character varying) RETURNS SETOF public."USERS"
    LANGUAGE sql
    AS $$ 
		SELECT
			* 
		FROM
			public."USERS" 
		WHERE
			user_name = _user_name 
			AND password = _password;
	$$;
 [   DROP FUNCTION public.user_auth(_user_name character varying, _password character varying);
       public       postgres    false    199    3            �            1255    16487    vud_delete_user(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_user(_id_user integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$

BEGIN

UPDATE public."USERS" SET status = 0 WHERE "id" = _id_user;
return true;

END
$$;
 8   DROP FUNCTION public.vud_delete_user(_id_user integer);
       public       postgres    false    1    3            �            1255    16451 c   vud_insert_user(character varying, integer, character varying, integer, integer, character varying)    FUNCTION       CREATE FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying) RETURNS void
    LANGUAGE sql
    AS $$ 
	
		INSERT INTO 
	public."USERS"
		("user_name",
		"employee_number",
		"description",
		"rol", 
		"user_register",
		"date_register",
		"status",
		"password") 
	VALUES (_user_name,
		_employee_number,
		_description,
		_rol,
		_user_register,
		'now()',
		1,
		_password);
		
	$$;
 �   DROP FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying);
       public       postgres    false    3            �            1255    16450    vud_insert_user(character varying, integer, character varying, integer, integer, character varying, integer, character varying)    FUNCTION     "  CREATE FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _date_register character varying, _status integer, _password character varying) RETURNS void
    LANGUAGE sql
    AS $$ 
	
		INSERT INTO 
	public."USERS"
		("user_name",
		"employee_number",
		"description",
		"rol", "user_register",
		"date_register",
		"status",
		"password") 
	VALUES (_user_name,
		12376,
		'',
		2,
		23454,
		'now()',
		1,
		'');
		
	$$;
 �   DROP FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _date_register character varying, _status integer, _password character varying);
       public       postgres    false    3            �            1255    16522 *   vud_log_insert(integer, character varying)    FUNCTION       CREATE FUNCTION public.vud_log_insert(_id_user integer, _description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

BEGIN

INSERT INTO 
	"public"."LOG"("id_user", "description", "date_time")
VALUES (_id_user, _description, 'NOW()');

END
$$;
 W   DROP FUNCTION public.vud_log_insert(_id_user integer, _description character varying);
       public       postgres    false    1    3            �            1259    16538    cat_transact    TABLE     �   CREATE TABLE public.cat_transact (
    id integer NOT NULL,
    code integer NOT NULL,
    description character varying NOT NULL,
    legal_foundation character varying(255) NOT NULL,
    status integer NOT NULL
);
     DROP TABLE public.cat_transact;
       public         postgres    false    3            �            1255    16643    vud_record_transact()    FUNCTION     �   CREATE FUNCTION public.vud_record_transact() RETURNS SETOF public.cat_transact
    LANGUAGE plpgsql
    AS $$

BEGIN
RETURN QUERY select * from public."cat_transact";
END
$$;
 ,   DROP FUNCTION public.vud_record_transact();
       public       postgres    false    3    1    204            �            1259    16413    ROLE    TABLE     i   CREATE TABLE public."ROLE" (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);
    DROP TABLE public."ROLE";
       public         postgres    false    3            �            1255    16444 
   vud_role()    FUNCTION     �   CREATE FUNCTION public.vud_role() RETURNS SETOF public."ROLE"
    LANGUAGE sql
    AS $$ 
		SELECT
			*
		FROM
			public. "ROLE";
	$$;
 !   DROP FUNCTION public.vud_role();
       public       postgres    false    201    3            �            1255    16484 l   vud_update_user(integer, character varying, integer, character varying, integer, integer, character varying)    FUNCTION       CREATE FUNCTION public.vud_update_user(_id_user integer, _user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$ 
	BEGIN
	IF (_password = '') THEN
		update 
		public."USERS"
	SET		
		user_name = _user_name,
		employee_number = _employee_number,
		description = _description,
		rol = _rol, 
		user_register = _user_register
	WHERE 
		id = _id_user;
	ELSE 
		update 
		public."USERS"
	SET		
		user_name = _user_name,
		employee_number = _employee_number,
		description = _description,
		rol = _rol, 
		user_register = _user_register,
		password = _password
	WHERE 
		id = _id_user;
	END IF;
	
	RETURN true;
	END
	$$;
 �   DROP FUNCTION public.vud_update_user(_id_user integer, _user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying);
       public       postgres    false    3    1            �            1255    16486    vud_users_record()    FUNCTION     �  CREATE FUNCTION public.vud_users_record(OUT id integer, OUT user_name character varying, OUT employee_number integer, OUT user_register integer, OUT rol character varying, OUT description character varying, OUT id_rol integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$ 
		SELECT
			a.id,
			a.user_name,
			a.employee_number,
			a.user_register,
			b.description,
			a.description,
			b.id
	FROM
			public. "USERS" as a
		INNER JOIN public. "ROLE" as b
		ON a.rol = b.id
	WHERE a.status = 1;
	$$;
 �   DROP FUNCTION public.vud_users_record(OUT id integer, OUT user_name character varying, OUT employee_number integer, OUT user_register integer, OUT rol character varying, OUT description character varying, OUT id_rol integer);
       public       postgres    false    3            �            1259    16396    LOG    TABLE     �   CREATE TABLE public."LOG" (
    id integer NOT NULL,
    id_user integer NOT NULL,
    description character varying(255) NOT NULL,
    date_time timestamp with time zone NOT NULL
);
    DROP TABLE public."LOG";
       public         postgres    false    3            �            1259    16394 
   LOG_id_seq    SEQUENCE     �   CREATE SEQUENCE public."LOG_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public."LOG_id_seq";
       public       postgres    false    197    3            @           0    0 
   LOG_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public."LOG_id_seq" OWNED BY public."LOG".id;
            public       postgres    false    196            �            1259    16411    ROLE_id_seq    SEQUENCE     �   CREATE SEQUENCE public."ROLE_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public."ROLE_id_seq";
       public       postgres    false    3    201            A           0    0    ROLE_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public."ROLE_id_seq" OWNED BY public."ROLE".id;
            public       postgres    false    200            �            1259    16400    USERS_Id_seq    SEQUENCE     �   CREATE SEQUENCE public."USERS_Id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public."USERS_Id_seq";
       public       postgres    false    199    3            B           0    0    USERS_Id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public."USERS_Id_seq" OWNED BY public."USERS".id;
            public       postgres    false    198            �            1259    16560    cat_modality    TABLE     �   CREATE TABLE public.cat_modality (
    id integer NOT NULL,
    code integer NOT NULL,
    description character varying(255) NOT NULL,
    legal_foundation character varying(255) NOT NULL,
    id_transact integer NOT NULL,
    status integer NOT NULL
);
     DROP TABLE public.cat_modality;
       public         postgres    false    3            �            1259    16558    cat_modality_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_modality_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.cat_modality_id_seq;
       public       postgres    false    208    3            C           0    0    cat_modality_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.cat_modality_id_seq OWNED BY public.cat_modality.id;
            public       postgres    false    207            �            1259    16533    cat_requeriment_transact    TABLE     x   CREATE TABLE public.cat_requeriment_transact (
    id_transact integer NOT NULL,
    id_requirement integer NOT NULL
);
 ,   DROP TABLE public.cat_requeriment_transact;
       public         postgres    false    3            �            1259    16549    cat_requirement    TABLE     �   CREATE TABLE public.cat_requirement (
    id integer NOT NULL,
    code integer NOT NULL,
    description character varying NOT NULL
);
 #   DROP TABLE public.cat_requirement;
       public         postgres    false    3            �            1259    16547    cat_requirement_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_requirement_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.cat_requirement_id_seq;
       public       postgres    false    206    3            D           0    0    cat_requirement_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.cat_requirement_id_seq OWNED BY public.cat_requirement.id;
            public       postgres    false    205            �            1259    16536    cat_transact_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_transact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.cat_transact_id_seq;
       public       postgres    false    3    204            E           0    0    cat_transact_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.cat_transact_id_seq OWNED BY public.cat_transact.id;
            public       postgres    false    203            �
           2604    16399    LOG id    DEFAULT     d   ALTER TABLE ONLY public."LOG" ALTER COLUMN id SET DEFAULT nextval('public."LOG_id_seq"'::regclass);
 7   ALTER TABLE public."LOG" ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    196    197    197            �
           2604    16416    ROLE id    DEFAULT     f   ALTER TABLE ONLY public."ROLE" ALTER COLUMN id SET DEFAULT nextval('public."ROLE_id_seq"'::regclass);
 8   ALTER TABLE public."ROLE" ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    200    201    201            �
           2604    16405    USERS id    DEFAULT     h   ALTER TABLE ONLY public."USERS" ALTER COLUMN id SET DEFAULT nextval('public."USERS_Id_seq"'::regclass);
 9   ALTER TABLE public."USERS" ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    199    198    199            �
           2604    16563    cat_modality id    DEFAULT     r   ALTER TABLE ONLY public.cat_modality ALTER COLUMN id SET DEFAULT nextval('public.cat_modality_id_seq'::regclass);
 >   ALTER TABLE public.cat_modality ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    207    208    208            �
           2604    16552    cat_requirement id    DEFAULT     x   ALTER TABLE ONLY public.cat_requirement ALTER COLUMN id SET DEFAULT nextval('public.cat_requirement_id_seq'::regclass);
 A   ALTER TABLE public.cat_requirement ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    206    205    206            �
           2604    16541    cat_transact id    DEFAULT     r   ALTER TABLE ONLY public.cat_transact ALTER COLUMN id SET DEFAULT nextval('public.cat_transact_id_seq'::regclass);
 >   ALTER TABLE public.cat_transact ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    203    204    204            ,          0    16396    LOG 
   TABLE DATA               D   COPY public."LOG" (id, id_user, description, date_time) FROM stdin;
    public       postgres    false    197   -S       0          0    16413    ROLE 
   TABLE DATA               1   COPY public."ROLE" (id, description) FROM stdin;
    public       postgres    false    201   �S       .          0    16402    USERS 
   TABLE DATA               �   COPY public."USERS" (id, user_name, employee_number, description, rol, user_register, date_register, status, password) FROM stdin;
    public       postgres    false    199   �S       7          0    16560    cat_modality 
   TABLE DATA               d   COPY public.cat_modality (id, code, description, legal_foundation, id_transact, status) FROM stdin;
    public       postgres    false    208   �U       1          0    16533    cat_requeriment_transact 
   TABLE DATA               O   COPY public.cat_requeriment_transact (id_transact, id_requirement) FROM stdin;
    public       postgres    false    202   �U       5          0    16549    cat_requirement 
   TABLE DATA               @   COPY public.cat_requirement (id, code, description) FROM stdin;
    public       postgres    false    206   V       3          0    16538    cat_transact 
   TABLE DATA               W   COPY public.cat_transact (id, code, description, legal_foundation, status) FROM stdin;
    public       postgres    false    204   .V       F           0    0 
   LOG_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public."LOG_id_seq"', 4, true);
            public       postgres    false    196            G           0    0    ROLE_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public."ROLE_id_seq"', 3, true);
            public       postgres    false    200            H           0    0    USERS_Id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public."USERS_Id_seq"', 13, true);
            public       postgres    false    198            I           0    0    cat_modality_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.cat_modality_id_seq', 5, true);
            public       postgres    false    207            J           0    0    cat_requirement_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.cat_requirement_id_seq', 1, false);
            public       postgres    false    205            K           0    0    cat_transact_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.cat_transact_id_seq', 7, true);
            public       postgres    false    203            �
           2606    16418    ROLE ROLE_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public."ROLE"
    ADD CONSTRAINT "ROLE_pkey" PRIMARY KEY (id);
 <   ALTER TABLE ONLY public."ROLE" DROP CONSTRAINT "ROLE_pkey";
       public         postgres    false    201            �
           2606    16410    USERS USERS_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public."USERS"
    ADD CONSTRAINT "USERS_pkey" PRIMARY KEY (id);
 >   ALTER TABLE ONLY public."USERS" DROP CONSTRAINT "USERS_pkey";
       public         postgres    false    199            �
           2606    16568    cat_modality cat_modality_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_pkey;
       public         postgres    false    208            �
           2606    16557 $   cat_requirement cat_requirement_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.cat_requirement
    ADD CONSTRAINT cat_requirement_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.cat_requirement DROP CONSTRAINT cat_requirement_pkey;
       public         postgres    false    206            �
           2606    16572 "   cat_transact cat_transact_code_key 
   CONSTRAINT     ]   ALTER TABLE ONLY public.cat_transact
    ADD CONSTRAINT cat_transact_code_key UNIQUE (code);
 L   ALTER TABLE ONLY public.cat_transact DROP CONSTRAINT cat_transact_code_key;
       public         postgres    false    204            �
           2606    16570    cat_transact cat_transact_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.cat_transact
    ADD CONSTRAINT cat_transact_pkey PRIMARY KEY (id, code);
 H   ALTER TABLE ONLY public.cat_transact DROP CONSTRAINT cat_transact_pkey;
       public         postgres    false    204    204            �
           1259    16573    cat_transact_code_idx    INDEX     U   CREATE UNIQUE INDEX cat_transact_code_idx ON public.cat_transact USING btree (code);
 )   DROP INDEX public.cat_transact_code_idx;
       public         postgres    false    204            �
           2606    16574 *   cat_modality cat_modality_id_transact_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_id_transact_fkey FOREIGN KEY (id_transact) REFERENCES public.cat_transact(code) ON UPDATE CASCADE ON DELETE CASCADE;
 T   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_id_transact_fkey;
       public       postgres    false    2729    204    208            �
           2606    16419    USERS user_role    FK CONSTRAINT     m   ALTER TABLE ONLY public."USERS"
    ADD CONSTRAINT user_role FOREIGN KEY (rol) REFERENCES public."ROLE"(id);
 ;   ALTER TABLE ONLY public."USERS" DROP CONSTRAINT user_role;
       public       postgres    false    201    199    2726            ,   r   x���;�0 �99E.��v~v6,!�H�z�s�J�{hD���|�k�}3�����[��$ N�C��9�$ ���O�V�Ww�s:tw�iu��5�����
3%��'Xk�8�+�      0   7   x�3�tL����,.)JL�/�2��/H-J,�,��2�J-.��+NL�I����� {�       .   �  x�u�M��0���S�m��{��h;$db3IOƎ�IKp+���p:-DOV-���WU�)��j9p�5�j-�b�tf�sA����=C��^I���V�}�O5,)b���7w�j�	ι��6r_ǹK�1�Z���bw��ȸ2���&�����}��Gꟾ>��SqK^ϑ���r��Z��!o@�ov�k�^�:ϝ��9�x���+�?�~����\8��BS�i��~�q۵�ꤾ�p*�����7�=������n��KdVHy��]<�J�z}�p�5��Լ�,ꬵWR���e�X�:���$���TM���V��Κ��p|X��8�����?���Z�@��s�9gZ-����HPB-���!Z����V*gv��݂|a��?�.�E      7   H   x�3�4552��)�RH�S(��LILI-�L+�KI�M�+�W�*-�L�L�WHIU��OI�)ᴰ�4����� ���      1      x������ � �      5      x������ � �      3   K   x�3㴰�,)J��,IUHIUHL/M�L+�KI�M�+�W�*-�L�L��A�qr�sZZ"��)�¥	(��1F��� �n&z     