PGDMP     !    4                v           prueba    10.4    10.4 [    Y           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            Z           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            [           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            \           1262    16393    prueba    DATABASE     �   CREATE DATABASE prueba WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Mexico.1252' LC_CTYPE = 'Spanish_Mexico.1252';
    DROP DATABASE prueba;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            ]           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            ^           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1255    16768    usando_for()    FUNCTION     �   CREATE FUNCTION public.usando_for() RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	resultado integer;
BEGIN
	resultado :=1;
	FOR i in 1..10 LOOP
			resultado := i;
	END LOOP;
	RETURN resultado;
END
$$;
 #   DROP FUNCTION public.usando_for();
       public       postgres    false    3    1            �            1255    16772    usando_for(integer[])    FUNCTION       CREATE FUNCTION public.usando_for(array_p integer[]) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	resultado integer;
BEGIN
	resultado :=1;
	FOR i in 1..2 LOOP
			resultado := array_length(arreglo,int);
	END LOOP;
	RETURN resultado;
END
$$;
 4   DROP FUNCTION public.usando_for(array_p integer[]);
       public       postgres    false    3    1            �            1259    16402    USERS    TABLE     }  CREATE TABLE public."USERS" (
    id integer NOT NULL,
    user_name character varying(255) NOT NULL,
    employee_number integer NOT NULL,
    description character varying(255) NOT NULL,
    rol integer NOT NULL,
    user_register integer NOT NULL,
    date_register timestamp with time zone NOT NULL,
    status integer NOT NULL,
    password character varying(300) NOT NULL
);
    DROP TABLE public."USERS";
       public         postgres    false    3            �            1255    16440 /   user_auth(character varying, character varying)    FUNCTION       CREATE FUNCTION public.user_auth(_user_name character varying, _password character varying) RETURNS SETOF public."USERS"
    LANGUAGE sql
    AS $$ 
		SELECT
			* 
		FROM
			public."USERS" 
		WHERE
			user_name = _user_name 
			AND password = _password;
	$$;
 [   DROP FUNCTION public.user_auth(_user_name character varying, _password character varying);
       public       postgres    false    199    3            �            1255    16751 K   vud_create_modality(integer, character varying, character varying, integer)    FUNCTION     D  CREATE FUNCTION public.vud_create_modality(OUT code_modality integer, _code integer, _description character varying, _legal_foundation character varying, _id_transact integer) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
DECLARE code_inserted_modality integer;
BEGIN 
	INSERT INTO public.cat_modality(
	code, 
	description, 
	legal_foundation, 
	id_transact,
	status)
	VALUES (_code, _description, _legal_foundation, _id_transact,1)
	RETURNING ID into code_inserted_modality;

	RETURN QUERY SELECT code from public.cat_modality where id = code_inserted_modality;
END 
$$;
 �   DROP FUNCTION public.vud_create_modality(OUT code_modality integer, _code integer, _description character varying, _legal_foundation character varying, _id_transact integer);
       public       postgres    false    3    1            �            1255    16735 )   vud_create_requirement(character varying)    FUNCTION     �   CREATE FUNCTION public.vud_create_requirement(_description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO public.cat_requirement(description,status)
	VALUES (_description,1);
END
$$;
 M   DROP FUNCTION public.vud_create_requirement(_description character varying);
       public       postgres    false    3    1            �            1255    16755    vud_delete_modality(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_modality(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE public.cat_modality
	SET   status = 0
	WHERE id = _id;

END
$$;
 7   DROP FUNCTION public.vud_delete_modality(_id integer);
       public       postgres    false    3    1            �            1255    16737    vud_delete_requirement(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_requirement(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	UPDATE public.cat_requirement
	SET  status = 0
	WHERE id = _id;
END
$$;
 :   DROP FUNCTION public.vud_delete_requirement(_id integer);
       public       postgres    false    1    3            �            1255    16710    vud_delete_transact(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_transact(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
	UPDATE public.cat_transact
	SET status= 0
	WHERE id = _id;
END
$$;
 7   DROP FUNCTION public.vud_delete_transact(_id integer);
       public       postgres    false    3    1            �            1255    16487    vud_delete_user(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_user(_id_user integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$

BEGIN

UPDATE public."USERS" SET status = 0 WHERE "id" = _id_user;
return true;

END
$$;
 8   DROP FUNCTION public.vud_delete_user(_id_user integer);
       public       postgres    false    1    3            �            1255    16754 (   vud_eliminate_relation_modality(integer)    FUNCTION     �   CREATE FUNCTION public.vud_eliminate_relation_modality(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
DELETE FROM cat_modality_requirement WHERE id_modality = _id;
END
$$;
 C   DROP FUNCTION public.vud_eliminate_relation_modality(_id integer);
       public       postgres    false    1    3            �            1255    16774 (   vud_eliminate_relation_transact(integer)    FUNCTION     �   CREATE FUNCTION public.vud_eliminate_relation_transact(_id_transact integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	DELETE FROM cat_transact_requirement where id_transact = _id_transact;
END
$$;
 L   DROP FUNCTION public.vud_eliminate_relation_transact(_id_transact integer);
       public       postgres    false    1    3            �            1255    16764 B   vud_insert_transact(integer, character varying, character varying)    FUNCTION     �  CREATE FUNCTION public.vud_insert_transact(OUT _id_code integer, _code integer, _description character varying, _legal_foundation character varying) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
DECLARE _id_transact integer;
BEGIN

	INSERT INTO public.cat_transact(
		code, 
		description, 
		legal_foundation, 
		status)
	VALUES (_code, _description,_legal_foundation,1)
	RETURNING ID into _id_transact;

	RETURN QUERY SELECT code from public.cat_transact where id = _id_transact;

END
$$;
 �   DROP FUNCTION public.vud_insert_transact(OUT _id_code integer, _code integer, _description character varying, _legal_foundation character varying);
       public       postgres    false    1    3            �            1255    16451 c   vud_insert_user(character varying, integer, character varying, integer, integer, character varying)    FUNCTION       CREATE FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying) RETURNS void
    LANGUAGE sql
    AS $$ 
	
		INSERT INTO 
	public."USERS"
		("user_name",
		"employee_number",
		"description",
		"rol", 
		"user_register",
		"date_register",
		"status",
		"password") 
	VALUES (_user_name,
		_employee_number,
		_description,
		_rol,
		_user_register,
		'now()',
		1,
		_password);
		
	$$;
 �   DROP FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying);
       public       postgres    false    3            �            1255    16450    vud_insert_user(character varying, integer, character varying, integer, integer, character varying, integer, character varying)    FUNCTION     "  CREATE FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _date_register character varying, _status integer, _password character varying) RETURNS void
    LANGUAGE sql
    AS $$ 
	
		INSERT INTO 
	public."USERS"
		("user_name",
		"employee_number",
		"description",
		"rol", "user_register",
		"date_register",
		"status",
		"password") 
	VALUES (_user_name,
		12376,
		'',
		2,
		23454,
		'now()',
		1,
		'');
		
	$$;
 �   DROP FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _date_register character varying, _status integer, _password character varying);
       public       postgres    false    3            �            1255    16522 *   vud_log_insert(integer, character varying)    FUNCTION     �   CREATE FUNCTION public.vud_log_insert(_id_user integer, _description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO 
	public."LOG"(id_user, description,date_time)
	VALUES (_id_user,_description ,NOW());
END
$$;
 W   DROP FUNCTION public.vud_log_insert(_id_user integer, _description character varying);
       public       postgres    false    3    1            �            1259    16726    cat_requirement    TABLE     �   CREATE TABLE public.cat_requirement (
    id integer NOT NULL,
    description character varying(3000) NOT NULL,
    status integer
);
 #   DROP TABLE public.cat_requirement;
       public         postgres    false    3            �            1255    16844    vud_modality_data(integer)    FUNCTION     �  CREATE FUNCTION public.vud_modality_data(_code_modality integer) RETURNS SETOF public.cat_requirement
    LANGUAGE plpgsql
    AS $$
BEGIN 
	RETURN QUERY select 
		a.* 
	from 
		public.cat_requirement as a 
	LEFT JOIN
		public.cat_modality_requirement as b
	on 
		a.id = b.id_requirement
	LEFT join 
		public.cat_modality as c
	on
		b.id_modality = c.code
	where c.code = _code_modality ;
END
$$;
 @   DROP FUNCTION public.vud_modality_data(_code_modality integer);
       public       postgres    false    207    1    3            �            1255    16756    vud_modality_record()    FUNCTION        CREATE FUNCTION public.vud_modality_record(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT id_transact integer, OUT code_transact integer, OUT description_transact character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN

	RETURN QUERY select 
	a.id,
	a.code,
	a.description,
	a.legal_foundation,
	b.id,
	b.code,
	b.description
from 
	cat_modality as a
LEFT JOIN 
	cat_transact as b
on
	a.id_transact = b.code
WHERE 
	a.status = 1
order by a.id asc;

END
$$;
 �   DROP FUNCTION public.vud_modality_record(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT id_transact integer, OUT code_transact integer, OUT description_transact character varying);
       public       postgres    false    1    3            �            1255    16734    vud_record_requirement()    FUNCTION     �   CREATE FUNCTION public.vud_record_requirement() RETURNS SETOF public.cat_requirement
    LANGUAGE plpgsql
    AS $$
BEGIN 
	RETURN QUERY select * from public.cat_requirement where status = 1 order by id asc;
END
$$;
 /   DROP FUNCTION public.vud_record_requirement();
       public       postgres    false    1    3    207            �            1255    16723    vud_record_transact()    FUNCTION     1  CREATE FUNCTION public.vud_record_transact(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT total_modality bigint, OUT id_transact integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$

	SELECT
		a.id,
		a.code,
		a.description,
		a.legal_foundation,
		count(b.id_transact) as total_modality,
		b.id_transact	
	FROM
		cat_transact  as a
	left JOIN 
		cat_modality as b
	on  a.code = b.id_transact
	WHERE
		a.status = 1
	GROUP BY b.id_transact,a.id,a.code,a.description
	order by a.id asc;

$$;
 �   DROP FUNCTION public.vud_record_transact(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT total_modality bigint, OUT id_transact integer);
       public       postgres    false    3            �            1255    16752 3   vud_relation_modality_requirement(integer, integer)    FUNCTION       CREATE FUNCTION public.vud_relation_modality_requirement(_id_modality integer, _id_requirement integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

INSERT INTO public.cat_modality_requirement(id_modality,id_requirement)
VALUES (_id_modality, _id_requirement);

END
$$;
 g   DROP FUNCTION public.vud_relation_modality_requirement(_id_modality integer, _id_requirement integer);
       public       postgres    false    3    1            �            1255    16765 6   vud_relation_transaction_requirement(integer, integer)    FUNCTION       CREATE FUNCTION public.vud_relation_transaction_requirement(_id_transact integer, _id_requirement integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

INSERT INTO public.cat_transact_requirement(
	id_transact, 
	id_requirement)
VALUES (_id_transact,_id_requirement);

END
$$;
 j   DROP FUNCTION public.vud_relation_transaction_requirement(_id_transact integer, _id_requirement integer);
       public       postgres    false    3    1            �            1259    16413    ROLE    TABLE     i   CREATE TABLE public."ROLE" (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);
    DROP TABLE public."ROLE";
       public         postgres    false    3            �            1255    16444 
   vud_role()    FUNCTION     �   CREATE FUNCTION public.vud_role() RETURNS SETOF public."ROLE"
    LANGUAGE sql
    AS $$ 
		SELECT
			*
		FROM
			public. "ROLE";
	$$;
 !   DROP FUNCTION public.vud_role();
       public       postgres    false    3    201            �            1259    16674    cat_transact    TABLE     �   CREATE TABLE public.cat_transact (
    id integer NOT NULL,
    code integer NOT NULL,
    description character varying(2000) NOT NULL,
    legal_foundation character varying(3000),
    status integer NOT NULL
);
     DROP TABLE public.cat_transact;
       public         postgres    false    3            �            1255    16775    vud_transact_record()    FUNCTION     �   CREATE FUNCTION public.vud_transact_record() RETURNS SETOF public.cat_transact
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY SELECT * FROM public.cat_transact;
END
$$;
 ,   DROP FUNCTION public.vud_transact_record();
       public       postgres    false    203    1    3            �            1259    16694    cat_modality    TABLE     �   CREATE TABLE public.cat_modality (
    id integer NOT NULL,
    code integer NOT NULL,
    description character varying(2000) NOT NULL,
    legal_foundation character varying(255),
    id_transact integer NOT NULL,
    status integer NOT NULL
);
     DROP TABLE public.cat_modality;
       public         postgres    false    3            �            1255    16821 %   vud_transact_record_modality(integer)    FUNCTION     �   CREATE FUNCTION public.vud_transact_record_modality(_code integer) RETURNS SETOF public.cat_modality
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY SELECT * FROM public.cat_modality where id_transact = _code order by id asc;
END
$$;
 B   DROP FUNCTION public.vud_transact_record_modality(_code integer);
       public       postgres    false    1    205    3            �            1255    16837 !   vud_transact_requeriment(integer)    FUNCTION     �  CREATE FUNCTION public.vud_transact_requeriment(_id_code integer) RETURNS SETOF public.cat_requirement
    LANGUAGE plpgsql
    AS $$
BEGIN
	
	RETURN QUERY select 
		a.* 
	from 
		public.cat_requirement as a
	inner join public.cat_transact_requirement as b
	on a.id = b.id_requirement
	inner join public.cat_transact as c
	on c.code = b.id_transact
	where c.code = _id_code;

END
$$;
 A   DROP FUNCTION public.vud_transact_requeriment(_id_code integer);
       public       postgres    false    3    207    1            �            1255    16753 B   vud_update_modality(integer, character varying, character varying)    FUNCTION     )  CREATE FUNCTION public.vud_update_modality(_id integer, _description character varying, _legal_foundation character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE public.cat_modality
	SET   description=_description, legal_foundation=_legal_foundation
	WHERE id = _id;

END
$$;
 |   DROP FUNCTION public.vud_update_modality(_id integer, _description character varying, _legal_foundation character varying);
       public       postgres    false    1    3            �            1255    16736 2   vud_update_requirement(integer, character varying)    FUNCTION     �   CREATE FUNCTION public.vud_update_requirement(_id integer, _description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

	UPDATE public.cat_requirement
	SET  description=_description
	WHERE id = _id;
END
$$;
 Z   DROP FUNCTION public.vud_update_requirement(_id integer, _description character varying);
       public       postgres    false    3    1            �            1255    16766 B   vud_update_transact(integer, character varying, character varying)    FUNCTION     (  CREATE FUNCTION public.vud_update_transact(_id integer, _description character varying, _legal_foundation character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
	UPDATE public.cat_transact
	SET  description=_description, legal_foundation=_legal_foundation
	WHERE id = _id;
END
$$;
 |   DROP FUNCTION public.vud_update_transact(_id integer, _description character varying, _legal_foundation character varying);
       public       postgres    false    3    1            �            1255    16484 l   vud_update_user(integer, character varying, integer, character varying, integer, integer, character varying)    FUNCTION       CREATE FUNCTION public.vud_update_user(_id_user integer, _user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$ 
	BEGIN
	IF (_password = '') THEN
		update 
		public."USERS"
	SET		
		user_name = _user_name,
		employee_number = _employee_number,
		description = _description,
		rol = _rol, 
		user_register = _user_register
	WHERE 
		id = _id_user;
	ELSE 
		update 
		public."USERS"
	SET		
		user_name = _user_name,
		employee_number = _employee_number,
		description = _description,
		rol = _rol, 
		user_register = _user_register,
		password = _password
	WHERE 
		id = _id_user;
	END IF;
	
	RETURN true;
	END
	$$;
 �   DROP FUNCTION public.vud_update_user(_id_user integer, _user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying);
       public       postgres    false    1    3            �            1255    16717    vud_users_record()    FUNCTION     �  CREATE FUNCTION public.vud_users_record(OUT id integer, OUT user_name character varying, OUT employee_number integer, OUT user_register integer, OUT rol character varying, OUT description character varying, OUT id_rol integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$ 
		SELECT
			a.id,
			a.user_name,
			a.employee_number,
			a.user_register,
			b.description,
			a.description,
			b.id
		FROM
			public. "USERS" as a
		INNER JOIN public. "ROLE" as b
		ON a.rol = b.id;
	$$;
 �   DROP FUNCTION public.vud_users_record(OUT id integer, OUT user_name character varying, OUT employee_number integer, OUT user_register integer, OUT rol character varying, OUT description character varying, OUT id_rol integer);
       public       postgres    false    3            �            1259    16396    LOG    TABLE     �   CREATE TABLE public."LOG" (
    id integer NOT NULL,
    id_user integer NOT NULL,
    description character varying(255) NOT NULL,
    date_time timestamp with time zone NOT NULL
);
    DROP TABLE public."LOG";
       public         postgres    false    3            �            1259    16394 
   LOG_id_seq    SEQUENCE     �   CREATE SEQUENCE public."LOG_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public."LOG_id_seq";
       public       postgres    false    3    197            _           0    0 
   LOG_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public."LOG_id_seq" OWNED BY public."LOG".id;
            public       postgres    false    196            �            1259    16411    ROLE_id_seq    SEQUENCE     �   CREATE SEQUENCE public."ROLE_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public."ROLE_id_seq";
       public       postgres    false    3    201            `           0    0    ROLE_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public."ROLE_id_seq" OWNED BY public."ROLE".id;
            public       postgres    false    200            �            1259    16400    USERS_Id_seq    SEQUENCE     �   CREATE SEQUENCE public."USERS_Id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public."USERS_Id_seq";
       public       postgres    false    199    3            a           0    0    USERS_Id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public."USERS_Id_seq" OWNED BY public."USERS".id;
            public       postgres    false    198            �            1259    16692    cat_modality_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_modality_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.cat_modality_id_seq;
       public       postgres    false    3    205            b           0    0    cat_modality_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.cat_modality_id_seq OWNED BY public.cat_modality.id;
            public       postgres    false    204            �            1259    16746    cat_modality_requirement    TABLE     f   CREATE TABLE public.cat_modality_requirement (
    id_modality integer,
    id_requirement integer
);
 ,   DROP TABLE public.cat_modality_requirement;
       public         postgres    false    3            �            1259    16724    cat_requeriment_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_requeriment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.cat_requeriment_id_seq;
       public       postgres    false    207    3            c           0    0    cat_requeriment_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.cat_requeriment_id_seq OWNED BY public.cat_requirement.id;
            public       postgres    false    206            �            1259    16672    cat_transact_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_transact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.cat_transact_id_seq;
       public       postgres    false    203    3            d           0    0    cat_transact_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.cat_transact_id_seq OWNED BY public.cat_transact.id;
            public       postgres    false    202            �            1259    16757    cat_transact_requirement    TABLE     x   CREATE TABLE public.cat_transact_requirement (
    id_transact integer NOT NULL,
    id_requirement integer NOT NULL
);
 ,   DROP TABLE public.cat_transact_requirement;
       public         postgres    false    3            �
           2604    16399    LOG id    DEFAULT     d   ALTER TABLE ONLY public."LOG" ALTER COLUMN id SET DEFAULT nextval('public."LOG_id_seq"'::regclass);
 7   ALTER TABLE public."LOG" ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    196    197    197            �
           2604    16416    ROLE id    DEFAULT     f   ALTER TABLE ONLY public."ROLE" ALTER COLUMN id SET DEFAULT nextval('public."ROLE_id_seq"'::regclass);
 8   ALTER TABLE public."ROLE" ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    200    201    201            �
           2604    16405    USERS id    DEFAULT     h   ALTER TABLE ONLY public."USERS" ALTER COLUMN id SET DEFAULT nextval('public."USERS_Id_seq"'::regclass);
 9   ALTER TABLE public."USERS" ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    198    199    199            �
           2604    16697    cat_modality id    DEFAULT     r   ALTER TABLE ONLY public.cat_modality ALTER COLUMN id SET DEFAULT nextval('public.cat_modality_id_seq'::regclass);
 >   ALTER TABLE public.cat_modality ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    205    204    205            �
           2604    16729    cat_requirement id    DEFAULT     x   ALTER TABLE ONLY public.cat_requirement ALTER COLUMN id SET DEFAULT nextval('public.cat_requeriment_id_seq'::regclass);
 A   ALTER TABLE public.cat_requirement ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    206    207    207            �
           2604    16677    cat_transact id    DEFAULT     r   ALTER TABLE ONLY public.cat_transact ALTER COLUMN id SET DEFAULT nextval('public.cat_transact_id_seq'::regclass);
 >   ALTER TABLE public.cat_transact ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    202    203    203            J          0    16396    LOG 
   TABLE DATA               D   COPY public."LOG" (id, id_user, description, date_time) FROM stdin;
    public       postgres    false    197   ��       N          0    16413    ROLE 
   TABLE DATA               1   COPY public."ROLE" (id, description) FROM stdin;
    public       postgres    false    201   $�       L          0    16402    USERS 
   TABLE DATA               �   COPY public."USERS" (id, user_name, employee_number, description, rol, user_register, date_register, status, password) FROM stdin;
    public       postgres    false    199   k�       R          0    16694    cat_modality 
   TABLE DATA               d   COPY public.cat_modality (id, code, description, legal_foundation, id_transact, status) FROM stdin;
    public       postgres    false    205   ��       U          0    16746    cat_modality_requirement 
   TABLE DATA               O   COPY public.cat_modality_requirement (id_modality, id_requirement) FROM stdin;
    public       postgres    false    208   ͏       T          0    16726    cat_requirement 
   TABLE DATA               B   COPY public.cat_requirement (id, description, status) FROM stdin;
    public       postgres    false    207   �       P          0    16674    cat_transact 
   TABLE DATA               W   COPY public.cat_transact (id, code, description, legal_foundation, status) FROM stdin;
    public       postgres    false    203   ��       V          0    16757    cat_transact_requirement 
   TABLE DATA               O   COPY public.cat_transact_requirement (id_transact, id_requirement) FROM stdin;
    public       postgres    false    209   ��       e           0    0 
   LOG_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public."LOG_id_seq"', 5, true);
            public       postgres    false    196            f           0    0    ROLE_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public."ROLE_id_seq"', 3, true);
            public       postgres    false    200            g           0    0    USERS_Id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public."USERS_Id_seq"', 19, true);
            public       postgres    false    198            h           0    0    cat_modality_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.cat_modality_id_seq', 57, true);
            public       postgres    false    204            i           0    0    cat_requeriment_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.cat_requeriment_id_seq', 367, true);
            public       postgres    false    206            j           0    0    cat_transact_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.cat_transact_id_seq', 101, true);
            public       postgres    false    202            �
           2606    16418    ROLE ROLE_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public."ROLE"
    ADD CONSTRAINT "ROLE_pkey" PRIMARY KEY (id);
 <   ALTER TABLE ONLY public."ROLE" DROP CONSTRAINT "ROLE_pkey";
       public         postgres    false    201            �
           2606    16410    USERS USERS_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public."USERS"
    ADD CONSTRAINT "USERS_pkey" PRIMARY KEY (id);
 >   ALTER TABLE ONLY public."USERS" DROP CONSTRAINT "USERS_pkey";
       public         postgres    false    199            �
           2606    16791 "   cat_modality cat_modality_code_key 
   CONSTRAINT     ]   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_code_key UNIQUE (code);
 L   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_code_key;
       public         postgres    false    205            �
           2606    16789    cat_modality cat_modality_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_pkey;
       public         postgres    false    205            �
           2606    16731 $   cat_requirement cat_requeriment_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.cat_requirement
    ADD CONSTRAINT cat_requeriment_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.cat_requirement DROP CONSTRAINT cat_requeriment_pkey;
       public         postgres    false    207            �
           2606    16690 "   cat_transact cat_transact_code_key 
   CONSTRAINT     ]   ALTER TABLE ONLY public.cat_transact
    ADD CONSTRAINT cat_transact_code_key UNIQUE (code);
 L   ALTER TABLE ONLY public.cat_transact DROP CONSTRAINT cat_transact_code_key;
       public         postgres    false    203            �
           2606    16688    cat_transact cat_transact_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.cat_transact
    ADD CONSTRAINT cat_transact_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.cat_transact DROP CONSTRAINT cat_transact_pkey;
       public         postgres    false    203            �
           2606    16815 *   cat_modality cat_modality_id_transact_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_id_transact_fkey FOREIGN KEY (id_transact) REFERENCES public.cat_transact(code);
 T   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_id_transact_fkey;
       public       postgres    false    205    203    2753            �
           2606    16838 B   cat_modality_requirement cat_modality_requirement_id_modality_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality_requirement
    ADD CONSTRAINT cat_modality_requirement_id_modality_fkey FOREIGN KEY (id_modality) REFERENCES public.cat_modality(code);
 l   ALTER TABLE ONLY public.cat_modality_requirement DROP CONSTRAINT cat_modality_requirement_id_modality_fkey;
       public       postgres    false    205    208    2757            �
           2606    16797 E   cat_modality_requirement cat_modality_requirement_id_requirement_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality_requirement
    ADD CONSTRAINT cat_modality_requirement_id_requirement_fkey FOREIGN KEY (id_requirement) REFERENCES public.cat_requirement(id);
 o   ALTER TABLE ONLY public.cat_modality_requirement DROP CONSTRAINT cat_modality_requirement_id_requirement_fkey;
       public       postgres    false    208    2761    207            �
           2606    16776 E   cat_transact_requirement cat_transact_requirement_id_requirement_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_transact_requirement
    ADD CONSTRAINT cat_transact_requirement_id_requirement_fkey FOREIGN KEY (id_requirement) REFERENCES public.cat_requirement(id) ON UPDATE CASCADE ON DELETE CASCADE;
 o   ALTER TABLE ONLY public.cat_transact_requirement DROP CONSTRAINT cat_transact_requirement_id_requirement_fkey;
       public       postgres    false    209    2761    207            �
           2606    16832 B   cat_transact_requirement cat_transact_requirement_id_transact_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_transact_requirement
    ADD CONSTRAINT cat_transact_requirement_id_transact_fkey FOREIGN KEY (id_transact) REFERENCES public.cat_transact(code);
 l   ALTER TABLE ONLY public.cat_transact_requirement DROP CONSTRAINT cat_transact_requirement_id_transact_fkey;
       public       postgres    false    2753    209    203            �
           2606    16419    USERS user_role    FK CONSTRAINT     m   ALTER TABLE ONLY public."USERS"
    ADD CONSTRAINT user_role FOREIGN KEY (rol) REFERENCES public."ROLE"(id);
 ;   ALTER TABLE ONLY public."USERS" DROP CONSTRAINT user_role;
       public       postgres    false    2751    201    199            J   �   x���1� @�N��l0`�Pʀ%4[��&R�U����I�nm.�ߗ�o�!��d<I	�p�袰7���ǂ�&ΘÕ��rV)���1�:���B��)a���K!�Q�1]>(r�Ucp�e�:��>�7C�/��
3�x��Z� �`9�      N   7   x�3�tL����,.)JL�/�2��/H-J,�,��2�J-.��+NL�I����� {�       L   5  x���M��0�וS�c�\���C�		4��t�t�3q˙n	n�87��iFd:#"/��\�޳�o]��&����vS�9ClS���y�����Hw�l������9s#Hؤ�1ǩce���7��C���o���7�?l�4�m�kmN��������*�3�207}T��9}�6�_��ꚢ�RA�P<i�P��
�6�q,@m�j���.�a}�>�%".��~����ϯ߽��;i,��S����:�_ �Z��tC�B��S��5-n �o�y�ͧ[q%�+�&
��.}s�}��חK��;��Ţ�9���+?/��=	hy�u ͚C$���;kj+T0��~j�qj�~<���w���K-x`�6H�FY����nW!AN��]<�D<9��΀y7�
��"��7��]ګ9�h�5�H����߱B����Pz�^���84g�M�H���������}��|mV_�R�&`#��dV2���7�42�{�8KO��@�� 4����q��H���/��m��k���w���h��ݴo�U)�Q
�Xii�/���?�g:�      R      x������ � �      U      x������ � �      T      x��}K��֖�X�W4`_(uJR=�Q����"~���4X"�L�"դTmyvC���Ct#���Z���ko�RU%9�o&qI"�k��^ߚ�^dy�*��y:/��TI�,�2y�̛����z�s�&u����|<��i6��^�7M
ߥ����r�5[�i��I�X�YO.
xs}��n����Wn�y�,����e�X6�UZ���Z�75�?˛|��n�e�$�>k�yY���UQ�e��yY��ϫ�e���{����,�u�;���r�u�\�E%��i���T�j�v�F�wE��m��^��[�~-���ۢ�9��˴MaU��7<��Nx�9����,Y���
�|��0a��2m���!`��1
����y���5���� ���k8����y���_��UqK��������U�U&���+Xhӻ�g�g�|�@�݅��).�X١y���&_6y� ]$e~����>���K>0�
(�L+�ax3��A2=>�a��_��.ڹ�<9|�O��z2��>��I:�Y7��MR�6�ɴ�v��aH�𶬀1W�oؕe]��U�˱�WMڻ7����P��o\|�w�ߟ�z��t�ٿ����5��FoyB�-]�u�5<��=��[��vլ�t���Jb���,��ni���7eʴ^�����yto�-���D��xU�����,�Bϋ%����e��R�u0�4���pr�l%����Z�́�<�?cR9�I)vГ��kb.|��rX$0 ���9�����Ox�t�p�@RE�$���̯w�%��j�w��Ӿk�&)_�6�M�.r�WZ��|���g��$�����_��w�s�b��+�����)�,S`tu�&��U��[�����M�o�)y���-΁L�ȴ���Z,r' �ne:��~�o������ˮ���e�b�&��@��TF�Z�+��?MNB��X~;�
Lg��5칓���hj��
H}����[>��d�#8�y�����c}�?�������c�t�'(X�A�E�p<zӠV�H���0�\(zʎ{/�,2��{���뫼�M}�ؤH�X��~�R����Α�|��T��g���ŕ�>���^���P��jIy	go�/����"�z6[O��-(?3�,�r�2.��3�Y8�%���h�*�I"GSZɥJ&��0D���5��,1�-�)8��ZM2�Ƿo�
ǀ�Ղ�=��x�3 깪��5�ί{vlv����u�Y�
/�k�ӷg�1����fYĳ�~�,��[�̀dZ����f�r8��N��o�4�F�Gi�Q�;��LgG��1ܚ���`�2�����I�-.#o
���N=��ƊR��~\�f(�FXְB��"�҄7����I�Z�ΰ�GxN�+T��4t���^�f��������f���#+�YHU��Ɛ�@M�����t�\�"��w����Ue�����<p:ESúZ&��[AJSk�P����h�����7DOk<��	���0Y�y{��1����!�,_���V���"t
���f�"�z���a��#s�%jn����t$|��"��䕘��y�R^�~Kj��$}_�Xؠ�,n@��?;�]��7�Lm����	�%��x
Tp���%�������덑��(kqb�nb���X����Ⱥ��H��;�BO�ۄV/��щ��iN�t	+X�Y�A��ń��eR�w_�s�q���!O�����ċ`��J��[4xd,=��6�;t�`&�n�������$Z�sd��L,aa�p���Ϩ��������i�f��	̧�qσq͞�a���5^u3,�*���I\W���C�PKTGYV��۟�b�Gg��1�[�&�cb�O(�M�|��ᆃ���#,a[����<d����Th?����{�s<�"�s=�~���nY�d�
��;���c�z!O�M���6�ڋ�oP ~ة �4H�3��"~�x?>s��mt��;n%���ɓ��h��2WN�xԬ���%��Q[$��W[''c��7]z�|El��-@TqX}��L�{ɚ�����4f{�t{�>UX���+sA_Ў��POf���z��I���� �YF����k�k��|p����z�㞳/�RL���tw��b?�;3���ku]��uc������SQ=�x}դ��<M�i���c���&�`Ԃ�RE	����2�Fe��wF�`p&o�70
`@U^�w&� �S,�
.��d�&�I�a��9�'�w^e��L�&��/�9٫-����&�[�=	2�����(V��䅨��I�2���j�듿���q����~��}���j����g�m^��tѬ���~��g�s���i�YG�^d:߁"ϊ�[��
u�G���&��j��H2x�\d��������mܽ�t�Uex�����p�o:K���k���*4�2{��d�U�Ĩɡ�FŵnH�;�9�P�bCd6�$�j����D��,r���{�b��i)��q1��UZf۟;,�9����9�<s���qR�;����Z�%��8�|	T��Vl
��k�,*��G�";�H�p��~ޢ��=��3$ܼ�r��˱n�=�����"*9��Q���%����m�a�3���HR���d��p}�'b! .H���X,�c�}Dt�3ř
�#�q�́t��*�G��5�L��|
b�DO2с�nL��Ȁs����j�}5��NG?���Sb�p�
��0ՆPB�b
�Iw�H�����oE�n0�`�x���a�@����)�z����1N�;�}|�|�t������=6��@v&��}��v%ǁ���痷O�����&!�����6q���������$�qf�L�h�~�q���t/�ݺ+q/�M�_�]��ݜ�}i`��sа=��D2��+�K��P�r wD��|�	.᠇Hq������he)]��Ã
D�����	��F|��>�&Uq�Ebb���돁o!7� �����߆A~T��(ކ��ޠ����j�l��w��Y�|�<Zl��l��Z�l!j�MT�_�qd"�w��pv�F�u�͙�NzF��G�v�Q<��a) �O�bq�r)uVAǉ*�K:nL��3?F0�E5y7���O�7��wc�m�G�g�*�s\���ʩ5A�H��fō�fP�ۇ�SM�j����� WQE�=m��ZU�T�RI���'@�Œ6�M�|���%)y:���$�!+%'z�*�\��qz��s��?_S _� �'ʾ\0���ۃ�
"�(��g�;ݫ���4�:	z��]J��,Y_)�'����f=n��R��U�b�& l��zq#�N��Z�� Ԫ1�n��"
m�Y��Gg�D��Q�E+\c	~��R�����oȳy~4B��F���#��
�,�f��{��\4�R�Y�h����ǡO5���?��[�����U-	r� 1��!��w�F2��&��9� 2���kƎZ6�0�#����S�Ց�+4��iP�h���B-��e��)z���,O�bY:��z�)�Qӧރ�r���$�})Q5/�J���y(�M�.�9�(1"'[7Ăq[`{����V_���w ���Ω\���Rj�r�$5��BW�+�/7rN~��_�l~_P��<]Ӕ������҆~�u��Q]��B��5���h��Ui�2�ެSQ��k�桦^ �h^;N�>�W(���M����ݢP����#V��p���3���-���ڍ����mJ���+��'۟��첼��@^�����)�ה8�����7-���M�gw>z�;��!σY�79�t<����T�3��؜��8�ޏ�u(���U�C��2ѝ�)�
�A�Y$0��2�r�I���=��]�Zժ�_,�~᭟�Zi���YB�*��6�@d3-�vs��6�O3�N;c���������3���l���G"��X���D7FT�1�E��KʗITQ���SĘв6��E����xC�^�M���ɰ﷥�(��Y�5"    '���I
��	�'�^��Q�M@�,IkJ�t����mj��n?��Q��ײA�\x�6����9*��& Nn./�'�X(��<O˜Ǭ�\!q0�'�^���������7ﾙ��̏�43Iv�\�3Y�H.b����������v�!밎$:<��fq�"��NM�fEJ*gB�=,��<�+COn�M��������_��K�ϩ�(�=����QGgé��E׬�h�!��)�LqL��ϫzq��`�]�[��yj6���'����N�H�5Q�&�L����Q8��w��D$�ԮPG�$�+P��z�[�����pI`�R��at4��Z)מ�X���;.r^�MVT�Mh�q1*��"`��.��쵯�qel���q���D3a�Ƀ�Ȧ�W2��{�Z\d'�.;�F7�e�6CX�zHO�L���~��ٝ����=i����l��9x��Hh�����4��8]$T�c=��Te�+�R���}���W��#b�$�|J~8xs�L�l�B-��@�q�Roَ[UM���R�c�Ҵ)���������Uݨ���.����L�
]�x����"���{ax�T��O,�'.��$���<��i�~�P(�Ӫ������&ŕy����[}'wQ_�B�LYG���BO�v<�s��b��X���AϢ_Q�mD¥���MyZF ����(Ĝ}F��&.#ݻN9Ş��g�Ȟ������t����8h'�w�b��Pv;:�J�~r�U@����I@sx���h��ְ]JϯY��
7�~��/�/9��뉏]X,��	WgTZ�iWq���#�i�3_o���*��e�k�	t���h)U�ߟ�/�a��!H˘��.��5�J-.Q#�EEU0��Ӫ��n�'#3�]hK���"/S����ɧ#�������]v��R�[��c��Ì0����2�A�~j�T�e^�z����5P%jH�'o���������B�x�+�V��H�.(I�Y�v˘L���@�-�S9��$�'2AI�I��k�*��(9��H���x/Zf9��I�����>wA���fyۺ����1 k���L��Z�Yz�O�f1�J�����\P�PYT�s��w����9�b���qQX>4�A�:	ޅ�l�H���(�kc�X�#bR|l�5Eד�u^Il�շ����؇�񋋿�	�笝q���U�"��T�_:�����h�������h�Z��x�g���CJ�o�+A�b�_ސ&���v(�v��/[cF	~Z'I�#��H2�%�W�I.�%�D����q�w��������%���|��(�^y�O�B;~���ĝ��d�$�Opv�gb���1�ll(�������U���;�桡���#dy�$@�V�NǋH�G#���?X�i���i�k�g�76���XI�D5�+'PG�s�n�aS厳"�.�ߊ���oL�x��0c"��`��Qp4=7�!W�f��'�U"�H�]���B��)L�~���Q�+U)֔"E�>�>�=�bX��I��h�t�]��إ��;��J�f�R���>��ۘ��_]w������#���y�����x�'�TƮ���)��g�o�}=��:κe	J�	��[��P���6R]�	�Q"�|t�ݯp���]q�'�U�FV.�y��ӄ�%f*�_��O�m�����b�5#�ЭY�&�6�S�+����VyK(/�;�ɦ"��2���?�����t�)��"���\/���.�
hP ��GyB)\E *��F_�������h]�a�\�Sm��,k�K�j�R�K2��^V��Z¡g�L�q�G��͟�j�g���E(:m� v���Uv��D�z�{����z���b���H�7C"E� �g�|ta5�E�I#i���=3҈ ��?$h"�t���V��T��D����A)N7�Tv���s1]4�Ě�kQ����I�X�g~�����O+:����hZn0� C\֥��N����"G�!/�a!.�$��S���+v
o�D5ХI�b]B�7�iB�V��9�1�Z};����w�O�	T/o�;��Ǡ�0�GJ��Nn�b���{���8ry<��ͳXd�qC����?�iٚwM�[�G���I���:�[���N���Nv���7�&�gRNc�/y|G4R0�ƅl\�JO���M�͈ �?4��f�$��RfP~Rc�L�j�[�(]c��u��ǧ{O��J�Bʌ���]ѵL��쬏,�@�z��N���D4ߢ��d�c�c��G� ̿U��t|��0��D�1_��;��6@����J���3��e!�
��+�5܌�3�&ф�r�da�۩��������N�@T$.*��Ij�QP��Kb�%!�"���F��/� >٭������|��u\�lk,�Pc��|��)�S?�	�@]Ae���W�����w�PN}a�D�N�dXiة�����*[�Eҗy���B��C�hj�{����:��PF=
Y8�Oط����D�cS�	t7�tl��T�;h;عP��
��W9��+�cÇ�U�$����MÊ�s&I���H���� �Ɗ6�&�J4w��m�7(9/��P͸��m����Z&eESY�k��+4U@�۞,jI���e��N,����tK:�,����)K�hg;x�a��9F<V>��U��O�+x�y�H=#x����|��t-��+-k���9�6XՇC�WgW���[X�b�2���d`�)�&?8��b��=
��%Z%T,�p���#�'� )�G�Ȁp�-�f�|(�xQ��̣����͑&���Q{:,tU!H �g>�l���P&='?���
I�,9�K��3�t�u,3�*�������`)�X	�a�@r����eE�����)��Y�a�G��2�hU�K:�.�>%�u��9,�!?���#�~��!V�w3�}E���Ee��*��P,;��p�X+���7n�jc���/W@���Z���`��Q�Aj��W�`v�ۃj=ZY���xp�s3T�<9��w���3h?�$z�l����I�� �dW)��HR0���g��7�I�����Д�Yz��0F���.�*s����N�+B�~o٠��M��z��-W�o.5 2)�p	�K~�����I�lة�e��0�C����h\L�#lK؄m�k�ý��"U�����`�7���^lA,�6a?I�B�=�@;�l�M'�B<c,�ɗ�Z�ը�������q�`�T�O`@q�K���*�������>v����9���iV8]�Qrޢ�ܪ�B1�4�4�::\j�ER�	/��~]r	�����I��?�v���} ���M�t-�`�F�J ެq.}�hь	�C)uUY�8��c�k, �X��_����>�],�t���d�~9o��0x�}i����W�1נ�,�dIr�`lʹ�.t��j΂k����3�2���e���/�W�U�B�	�s����"0�y\������+�N@C�y?�CN��֨�T��?�� �N�wɦ�x�@��/pť�Q�֔ga�D0��pa��)'n������ݓ�T���w�kB\��=�H��h�yA��LJ��Z���^T>�,N�����ް$����� �o�]�R$:��!+��]�(O������4�v�f%�GC�&NߤQ9ݝj�Qy��/�j�mA9\�7�EA�c �zJHh�\��dz�����$�`�����P,/XH��aG����h1NT�l��!Zj�]Q�69f"��{���_X$�%���cB5� �e�ݚ��5��_�CǤ�u`
�,o�����ZЧ����a-����U<�'��[Z����{Wm�0�1e�i��=X�l�-��4�jl-W�&9�]�z'��
��y?>��H��DcKE�_]v�E�3�J-6���5�Ђ��Z����RM:M���0.�R�ˁ�q�m6��x0%��N��]�kL��b.UC�wɎp9���W��f���ĽG2C    ���Rͅ:;] �m�cA���Ke���	s༇�g�d��?F$/�LA-0˞��e�(����A��as`��?	U��F�,X����17��� =	�����`A�29;CS�2Hn���0w1�B��E7��^��7����T�Ki��6T�@���s����r
Л�a�V�層A���%ќ��^6��}���ɞ��^3�Ü���PojēD�f����	bͧ���X�Cf���c22MF�X�ؼY��\�7��+��w}�Q n�>��Ժ�ʜ
�v�*T�G�@>�����D G�;�t�4fu�#�xfSռ�`�O���ɠq,@)�EV������8�~?u|ӆx����Ya���+#��Tǩ�E��^EpZ��"wD-q8����P���m.����p���B�dkB�:~В�F�k�Y㊊��%�}���x�'�2'�j����I�=d���C�Se����������`- �M3��v�0�E����%�?�&~�r������Ʉ.���ޯ��|�U�@�{�C�>��;�|�7{=�ökj�9��P�#L��lԙ�ZRX@o�re#�/
��j����T�F?��Ȭ�����NЃ�b]w�#�i�~"�ތg�RF.%�H�"�b�b�JȌe���H��K[�v����K�DNZb��8U@��%_P"&9o$3K��	-�$V:�8�s�E�e�k�W��Og����aX7��Sc�����_{���e_u��`1�����N������_g��z����n��wU����{����W����юG�e�4�=��I��o��}	=<�8ɀ�4�栄o��\�2ߐ��^�N�>��Ż�o/�agp�C�s���OJ�f�>@?>��*gx{��h�آ��!�à��s���W��;6-]���[*�d�b�S2%�*-�EX8G�D�D�|s@��;vJcU�PJmF�h]$C���������t����7M7�6��>*d\Ǹ�ޚ��H[ ���U�T�j<K<��׬��AF�w^7����;�D���J���o�I�^"��ח�����zEHE�І� hL�ʠ �ْN�Yb%�����UD����~�yE\\F�BwF	G�r��XՏCC�Yp)�B��)�NXdݢxc���F�<ſ��\�7.������.a�G��}�<�\����>Xn�ѽ�j0�T����\f��¥���rʣ5��!��5��w<���HӒ�Ԛ_4O�+����i^>�C�@��m!����p���]d���G����;��9*і�:�8ɧ�9l�d�l��uz�|"�OE��/�=֢!���*�$ձ�8`����i�bbJP?�T��m8�̈�!�9>�8�����.�f�S�aw��1pa�ifu���r�\��n��2H
�b��8f&z�̄j2?i����$<L�\�UIv���EKX����6��RZ�K�+
KRR�GS���৚A�A��8O�ү|��F���I�)^�:��k;�w�i����\w
���.��$�]&��|��������,_:�nS���J>���o���[<5.X��r�53Iv�^1J��C����e�buk.�X���J[nM�����ߐ/l�rAUQ�$���XM��ؙV�� L���J;ʴ'���3��|�}��Ǒ���3��Ռ�U�Oea���*��K`r#2���h��Q*
�(@�} =tm�\�N�b�`����[�y�0)D�%�}�`���#@�ؑ�.�b���}FK*��Po�&�PU�X�ѡ����@��z\��g��
��RcuL�+T	p��'(4~1QY�:H�iń��*�z��L~nw�Q��Q���⊧��$�M_ò�t��yL��P��N;��P擧#�W3��VUA1�`��K5t���i��g���ù�fp>���f�Y�TQ5�49
T>Fu8�@���D'����|�8��r���X9;�2�Fxw#yD�/B4Z1�����<`�-Km�[R�jmj�����r�Z�ފOpW;s�N,<���d�3����bܲY�ĸ����uae�}�^����n}uC|;q�@hMf�ή�"�KZ��i�)�Zq�������Vc��e�O�
3�h��sh�߰����%B����ve���9���F!QB1��,F�E��=��Ԥ@dF2�f����bd��1����;Fms�F�ߓ�$�p��*3��oCm�T�c:���%ƺkcA*�? #�;�<�>��x�֥��֎��e�}��
�8nRMRA�,w�1�n�Uk��w���O9�I_� k�!	���;����2�(F�* ���y)���pbi��t������C>9N�PV��'m&�z�!�;��9"�!���T����K���7�_>�_���������{W<�>yL�J۝E9�z��wR7d��\���o<�# �m��` �u0� 6��+�fH���
�ΠL�\�7�=�A������o��<H\.iu<���r"�[����W��܃�4�#�"�D�"�Þ�
D����l���"u�u�^T5�:��תyuw�B=��9�/�I�4��Vd{��{��;-���V��d�����e�e�����=�T��%�ved�7)� �"J�g��ʦuY�ǎ����3�4uJC�i��������lr��1M�0Gw٪Z&�,U�bg����F�D���*����i���W�^=��*>��t���9zn҆}��0�&�垞��ۥjp�g�F`�]�'I�ń��*�����߿�C�(��n8�4%?1>hSc��$�׈�<L. <�4��pa@�����	 ,9MP�[�m^,��.�p �~�6���z0�2U:�\��SIs�� ;�pmxTW\�Җ L܌(|.	)ۯ��3l=�����o�����xX�ޤ KMg��W�oR�^�S�7%�C� �rpt��.T�R��Z�ǵ��M"�n�e��]��|ǽ��( �4���N���o)>�j'�� M=d߉g�=ť��~��ࢗq��Ү�<��s�b-�B׫��k�uz�x瓳e�G��[���E�SH&���R���� ᴰg���CT�]>��@ϼ��
��[� zp��O8ݦ+��o`��{927}��u�!Iv.��;]�GgAJYW�{���T���\��s�Z ܖ���)�dc�إ��؎Щ��|I�50�A>O+t_%�
P��wĦE��Re�3S�V�
�&Q��,��.\U���.K�<vz�;��S��ƕ?��ι	<
ya�l�n�?��ר����P�L[�i�:�j
��[YJ�J1
:uA���-�`��@��8�o�A�I��DZ�	Qb�����)������j�w�=���`-��N��]��g�9���)C�,��/=�'����21�&cW�n��M��Ù�^z6�׍7���]&�����9��U*xoց��i�2N�Ԉ/�%<�}�������%*w���j����L�W�n����_����ѳ��:ޛxڙ������΄��&��H{�9l�zv������iD��yE�kh,`+��t��yhC?�y�7���@+,��)����IvL|�.���������CI�fx��"DI!:]8���(��Op�,�R��r��t��Q�`q��$�vL��(5��n�j�KBW�ח��Bc�B-���?q���d����g	)F�9�i�=�Hyz|��N5	��]�TԢAGQdm����A�ܠ�����d�ۼ��F=�A������hǠIh���F(N�Q�;��yP5i|�I��d8H�e�Q�cjU8��A,F]*����TCO�^5���e���i��c`�Îۋ+g%ɮ$���h�7D�ݞl�'�܉~~���|`&�6�;���uk;{H�6�FYD��	2���~����FФ�k҈R��d�)@��x�X�Y��P�����	�om�>
�g¾`m3��sNz����rk��e���ABɍ	�6�}�,��7�f:ڧ�<�=�}ަX���Uw�    ���tt<����)u\��kz`%?�����0�.,��z�z�*�STk�}L5��9�Ĵg��%�ĩ���
-�ow=T<�qz�5IK8D�q��b����r;߾+�ݻ(
�0��GU�u]Ga�PX�	ߩ���',)宣�������*<==] ��^�WQ ����t��m����y)ox�����x��_.|����ׯ.޾x��s��|��ً��_m���|=�k�Pt��^�R�nϱ��bS[[w������'p�@/7���t���=c�*|$ͯ�E��}M�K]�ۙ��Ө�Cbm��6	7駽��JQ���2Z��.ͧ���}q����;����W#X%;���:����������L�p<�j�`I�	^�RZ/��%,�6x�����tJ!H"�f�:ꪠ֍��co[�����t@�)��7�)�Y��;����I#��<�U=��f���:�;w����f,<\�؆}�	�N�2)~��^�|�������F�
V񫹣i�e}�!�1P��|z� D,��>ók�c��� ?"�����O�s��@��r�q�A�=�y��g��\��(��Co?��y/�sa]�1�����ۺ�nu{Cf����YKd����_�o`T�\gE92K�8�6"��k����)TZ����_y(wR4k��)*�˶����C0*�&�T2���N(����M�@[����Em#�//`�4C�����i�Z��6j����TG��?2��I�C��G��נu㨩����c���C����:W��Eg%{P�!���I���!&oH|\q��g/Ķ?}��~�,���u:�wk�﬙/����/�q��l�-�ĵ[T�k��1jmV(�S���B��x�N]��~4����]=�����rVi�dB��w!{�W�_�?�.��i�50��e�_O��
^��\|{�&����C����b�Yj� �G���(r|�|R ��ŲV���綪��~r�% �}�ť�_��*hEI:S���{��od��l<~���PRy�5�ȗR3���Y�J��H���_�U6U���$ـ���9|���Z8d&���k�{�r�v��4YtI{t�V���|��/��v�Oi{s�Q��s�q�>�m\����)��AC"�S��!�S�=��F ��"g���զ�Q�"͏�?8��,P�_���8� ��$J!�k\*�
���&w����D�?�^%����G#���I&Ŧ�������;!	K�����F'!�Z~�~�dC�H�N`�s���xҷg����E�v�,D��Z~�Fm���^Es����H�����v_���#o�V?1zxZ�||H�춐�Yw��#:������ƻ�\z�,�z��;?�F=��p���_zN�B��]��^�+�8=��#f@0�K�N�����(�˗�7������+vv�;[����!"g]�h��yx���C8 
���剥�|��&Fx�4��,����`��:mضU�ޅ`~�,���?�}�]Sl�����G�U��7�J�E|�ĵ��ڙ��b����@w�-��D��kߏBE����w*�#���{��=��;��\���5�/��jQ}�u�1f���	xm�|N�.]��х�p�z�9�9T`>����&����\��ˁ�u0��e"i��b���+<Տ�ι?�� �00���M�z�������N_�[۱G6��� �?yFU�NI_:7zѮӲ��w������Ë����Ύs/�˟�y��^Z�*�b��P�'1Z���|��81]d{�Q6���K�#p�fjUP��ygt8d��3�*�@۟IȜ�>Zv��8�%���'w}f	&,�8��4�`�1�R'o`�ս�宓w�P�Q���:��th*�}�K$�ܦ?��d�s�>g�OC`rE�:T���XΔ�y��g�dr���k�.��N��a�h���Å��c���NF9H\LD
�8g�H"P55�I�͞�a�hH�EC�Ub ;�����:5u���F_\C��"@��0��viO���n.�`�n���;f�VOE'�#h�)�yd�'�x�^�V������l�0�k3tWz#���D��Hf�J�t�G��U�>�3?��T����̚�0?���u��ݽ��ꤺ�P����x��r�\��%�.4����?��]�@u2�[G.Χ���$�#��O/��A\I�sQ]�����ʔ�����a�tYc�f�����4�|5?xL� �;=l��g��R��9��$=�W?	y�e"��s��{�f�$˵�!���+�5H�-�_j�Y��B…߱Y�� S�Y%�9/c9}
�ѯ�o2��8��<Hrx�/����f��x@G8���,N�$�M�zw�&��p�9��G^��H��#iM�yDZr3��m��
�L�T��S�,~B���5I)��Z���jg�����0N�r���1�x��2L�퍵��\�߳n���yin+��e\�e���ׄI��J1W4{� �PNr1�sS�v/�حA���	p�נ07��P1[���	����@��~`_$k���<�HHu�;�P0�\ј��t�p¨�����E?ҋ��4W$�A�0�Uꊇq��J���$
�e)	�h��:�r��f�_Z���M��4g��+��miN�O�u�L��W߯7I�kI�3`��+���u���-j�s.�WVj���}(2,�6�ː��q Mx��&��ŁEz�ջ;�M���BA�*3I��"��D1t#0��i���2��c��ykig�Ŷ��4)����*w��/|�;f��� ��I#���Pv
���2Ȍn�C�D���.�&���U]N6]�9!FG୴ʅ@�}!9�`#�?':(g�S�(���t���`qg�|����1_Q_0�]ff;�m玹� ��~Z�ԍ<�H"��N��[����!#�$U{�T&n�c�>+ŔY�/	�|��P�e"�Du_�R�,ѱ2�I�z��N+6쒠��$F�e|`1�78��#|{n���Q��0���̘.9q߰�݌el��v�+�ZxbD:�FP���2Iv�u L����s/��b"M�9���������� t��)��`����S�S4����}K�R|�!r]�v�7gqT�@����4�c�k��K�kM�(U�k�7Qf�����.�0�F��4Q��LY�J6Q�wW��|q�
d�s��-��|�P>r5���0��]��l���,���y6�u�j���.�0�;��	�1���W�E���2,/\�+NzU�}B�h	@�;���e`#���GB���v,�`�s��H�E����e�P���2]�נyb4¤K�e���5���U#��#�>�谊��Ɏ�;Np$���D�h��H���t���r���2��vJ��uk�w����6=#6iEx�<H�t����u0ω����f	��*���<zw�C$ �.S�6N`���}�#�,��P��(H�Q�1(q<��_#�7]���)I���%�ˈ$ʲ �}c�S_~ͅ�^�-����\a2�T�q��tr�!F+7=��n._�¹�ܩ�VLE���MP�)��c[�BT�r-���Q�K�}��ֱ�2l��a)O>8|kl��Ul�Rq�j�V�uAU��iț��ޛHS�[�˧MѰ��%ji�z���:%�ɲ<݋6'�ꂊ��z���o��18Dׄ��1�	�c�Ni{�K�./Ҫu~K���qŃD,�T3��wKtZ��AnT<�ΐ	���ݦ���%v�&��v�@��|��2�1M FzJ��Z�p����Lj���d�A#{!���pQ	AF^�(Q�`�����B�ko�ڪF�±��ɔ,��;�r�f����>-]^uY�-��z%�"y�b��+�#�y4����ZI/�_�.^k-��[��0
��o�b�,�<�*էw�6�wh�^��u����h�&���> z2�w!�;g����5 �v���A����]P�����8���2h�|��?����<~�"��	+�b!<x�P��^�ڠ���u�j ��Q&�����~S�,g�o\󝰾#� �  �}�Q4{���ӱ 8Ql�v
�5��Z��aX�Aa ��]��f'�~�,�
���Md69�pPI������6u@<Y?��J�v�=EH�uG����If'���m�bd)�;ڤ���CDq�8� �M��7YՊ��nD�y\�607�2k�T��6��0�D�x�������}�1�y�,�G��b�P�:30�J-�.K�UJ��o;��~c���
�s�s�E��4{b�� ԙ�:{r��LU<�ɇ��X�@E?)�5=Do!�l��sl 8QlP�I.�TK�B�a�oԁrU�E[�(�\n	�}�HK��r�(��c@~{倫7��)����-�RHρ�ə�I.�@U��t��m*'׸������|�� &��w��������+nOk�m�ц�m��\��cp�s����7C���`nS]:IC%�JH��?y�k�f�������ސX�-[2r��Jʮ��~|�=�@Z
R����"I�%������s�T��9;�����zF�&���)-hMbB�+>���޺�hp���?�#C�rhx���s�h�M�{c[4u�ȡ� d�lzt����������L��R�wjQ�%�Bk�;.��x���	�x�������=Ek��؊&ķ���,,՜��~�&@79;��N�b%�l��5��w�9j�CϤ� �1G7y�� d��C.'�AH)y�<�m0*T8_���j2���8�e�
y29<L\��Sr*K/\����f�h��4��z5 ?���Z��R��V�c{�y��UNғ e>!�g���}\�a@W���:>��%Y	6	i;��� r1-���ߡm�Г��kv�W�0Ĕ�,o��/R�>�����w�mlr ⵑ �.����Ɉ<`������Ʀdi�pz�Ԡ;
��P׹{e0�S���+annF���̼(������'�o��7t�����f���!�v)�U�`��)���\�#L�a9u�w!XԐ哰@瘬�2L2�0�暣���	2�C������"��R���:E����1��O-j1�#K�xO�nL�܂�Έ�(oM�`������ ���li ��6�!�2�o�]) MA H������<hO"��Tش��#[��I�XHӯ��%`�9Os���=:��.v�mD0'aj���%�Mw�K�:B؆��`6���q���Zԛo��P't$t�^޿�<�mv�����e4��Oq�:�+��8ǹho53`\��/5F[��8�'up&��l�V�� �*�q6���?�CU���A����"���}تe ��?�o��:�S�m�IP����Ou kWvRE��B�J��|(�
f���s�'z2zMX
Z�{�:yz�td��6Ƃ����8�XO�^<��켳R�$����~��pԁ�NY�a��X<��^�oB|�M���G����<^J�+_:maw�B�*k!�8��ރ��ӚKf;���U�rt���n���SuYD���5W;�s�d�d� ����n�ʇ,����k�7��V=`��:|T�%�؍��16[5�=�1J?���Ey�_M}�1_}��|�-F��t7��8\v�jZ�鎅x�^j�V�\�L��ؽw��̓���@:ܜ�fL3;}k'���,��&�Yz�Y	�cT�9;�ʮ���d�:`<;>����ߙ���e{�m�M8%���0g�}-�dx�
�9ҳS�Ѱ˧�
��zT�,L��#�Jb����ww>X;�u�{,L�2Ead�2�YR
�-U�����Ɲ���	1l�eqh����5��:2�	mQae�d��)�3�9�wW��]�W��]pQQ��!|�f�;3�Ǹ2�$�5c���1iK8�yaYJ+n�l]�O�W����J.���Go�|{���1O��U������34�JW֖ݗ���%K"�|c"c�KW�VJQ��i�:X�0��$��紇6��%1 ��	��X��T��f�veP�1wTLe S1��m-)�g����EB�s=>�ez�����a�3 �H��zH�7#��k0CD~�@d
�*y^$=����_�3�$%���mx?	�1+�<Û^�V1��W@	T�Gu�(P�����kE��l�&�P�D�0g��Cz�`���MZ���ѿ��d2O?�G�����6	b�s����������q�q�      P      x������ � �      V      x������ � �     