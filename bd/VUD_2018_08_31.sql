PGDMP     3                    v           prueba    10.4    10.4 �    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    16393    prueba    DATABASE     �   CREATE DATABASE prueba WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Mexico.1252' LC_CTYPE = 'Spanish_Mexico.1252';
    DROP DATABASE prueba;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1255    16768    usando_for()    FUNCTION     �   CREATE FUNCTION public.usando_for() RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	resultado integer;
BEGIN
	resultado :=1;
	FOR i in 1..10 LOOP
			resultado := i;
	END LOOP;
	RETURN resultado;
END
$$;
 #   DROP FUNCTION public.usando_for();
       public       postgres    false    1    3            �            1255    16772    usando_for(integer[])    FUNCTION       CREATE FUNCTION public.usando_for(array_p integer[]) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	resultado integer;
BEGIN
	resultado :=1;
	FOR i in 1..2 LOOP
			resultado := array_length(arreglo,int);
	END LOOP;
	RETURN resultado;
END
$$;
 4   DROP FUNCTION public.usando_for(array_p integer[]);
       public       postgres    false    1    3            �            1259    16402    users    TABLE     {  CREATE TABLE public.users (
    id integer NOT NULL,
    user_name character varying(255) NOT NULL,
    employee_number integer NOT NULL,
    description character varying(255) NOT NULL,
    rol integer NOT NULL,
    user_register integer NOT NULL,
    date_register timestamp with time zone NOT NULL,
    status integer NOT NULL,
    password character varying(300) NOT NULL
);
    DROP TABLE public.users;
       public         postgres    false    3            �            1255    25007 /   user_auth(character varying, character varying)    FUNCTION     	  CREATE FUNCTION public.user_auth(_user_name character varying, _password character varying) RETURNS SETOF public.users
    LANGUAGE sql
    AS $$ 
		SELECT
			* 
		FROM
			public."users" 
		WHERE
			user_name = _user_name 
			AND password = _password;
	$$;
 [   DROP FUNCTION public.user_auth(_user_name character varying, _password character varying);
       public       postgres    false    199    3            �            1259    33388    cat_area    TABLE     k   CREATE TABLE public.cat_area (
    id integer NOT NULL,
    description character varying(100) NOT NULL
);
    DROP TABLE public.cat_area;
       public         postgres    false    3            �            1255    33447    vud_area_record()    FUNCTION     �   CREATE FUNCTION public.vud_area_record() RETURNS SETOF public.cat_area
    LANGUAGE plpgsql
    AS $$
BEGIN

    RETURN QUERY SELECT * from cat_area order by id asc;

END
$$;
 (   DROP FUNCTION public.vud_area_record();
       public       postgres    false    3    1    227            �            1255    33460 J   vud_create_dictum_request(date, character varying, integer, date, integer)    FUNCTION     �  CREATE FUNCTION public.vud_create_dictum_request(_date_receives_dictum date, _folio_dictum character varying, _answer_dictum integer, _citizen_dictum date, _id_request integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
INSERT INTO public.tracing_dictum(
			date_receives_dictum, 
			folio_dictum, 
			answer_dictum, 
			citizen_dictum, 
			id_request)
VALUES (_date_receives_dictum,
		_folio_dictum,
		_answer_dictum,
		_citizen_dictum,
		_id_request);

END
$$;
 �   DROP FUNCTION public.vud_create_dictum_request(_date_receives_dictum date, _folio_dictum character varying, _answer_dictum integer, _citizen_dictum date, _id_request integer);
       public       postgres    false    1    3                       1255    16751 K   vud_create_modality(integer, character varying, character varying, integer)    FUNCTION     D  CREATE FUNCTION public.vud_create_modality(OUT code_modality integer, _code integer, _description character varying, _legal_foundation character varying, _id_transact integer) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
DECLARE code_inserted_modality integer;
BEGIN 
	INSERT INTO public.cat_modality(
	code, 
	description, 
	legal_foundation, 
	id_transact,
	status)
	VALUES (_code, _description, _legal_foundation, _id_transact,1)
	RETURNING ID into code_inserted_modality;

	RETURN QUERY SELECT code from public.cat_modality where id = code_inserted_modality;
END 
$$;
 �   DROP FUNCTION public.vud_create_modality(OUT code_modality integer, _code integer, _description character varying, _legal_foundation character varying, _id_transact integer);
       public       postgres    false    1    3            �            1255    16735 )   vud_create_requirement(character varying)    FUNCTION     �   CREATE FUNCTION public.vud_create_requirement(_description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO public.cat_requirement(description,status)
	VALUES (_description,1);
END
$$;
 M   DROP FUNCTION public.vud_create_requirement(_description character varying);
       public       postgres    false    1    3            �            1255    33446 B   vud_create_turn_request(integer, date, integer, character varying)    FUNCTION     w  CREATE FUNCTION public.vud_create_turn_request(_id_request integer, _date_turn date, _review_area integer, _observations character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
INSERT INTO public.tracing_turn(
	id_request, 
	date_turn, 
	review_area, 
	observations)
VALUES (_id_request,
				_date_turn,
				_review_area,
				_observations);
END
$$;
 �   DROP FUNCTION public.vud_create_turn_request(_id_request integer, _date_turn date, _review_area integer, _observations character varying);
       public       postgres    false    3    1            �            1259    25106    vulnerable_group    TABLE     �   CREATE TABLE public.vulnerable_group (
    id integer NOT NULL,
    description character varying(255) NOT NULL,
    status smallint NOT NULL
);
 $   DROP TABLE public.vulnerable_group;
       public         postgres    false    3                       1255    33306    vud_data_vulnerable_group()    FUNCTION     �   CREATE FUNCTION public.vud_data_vulnerable_group() RETURNS SETOF public.vulnerable_group
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY SELECT * FROM vulnerable_group where status = 1 order by id asc;

END
$$;
 2   DROP FUNCTION public.vud_data_vulnerable_group();
       public       postgres    false    225    3    1                       1255    16755    vud_delete_modality(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_modality(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE public.cat_modality
	SET   status = 0
	WHERE id = _id;

END
$$;
 7   DROP FUNCTION public.vud_delete_modality(_id integer);
       public       postgres    false    1    3            �            1255    16737    vud_delete_requirement(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_requirement(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	UPDATE public.cat_requirement
	SET  status = 0
	WHERE id = _id;
END
$$;
 :   DROP FUNCTION public.vud_delete_requirement(_id integer);
       public       postgres    false    3    1            �            1255    16710    vud_delete_transact(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_transact(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
	UPDATE public.cat_transact
	SET status= 0
	WHERE id = _id;
END
$$;
 7   DROP FUNCTION public.vud_delete_transact(_id integer);
       public       postgres    false    3    1                       1255    25027    vud_delete_user(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_user(_id_user integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$

BEGIN

UPDATE public."users" SET status = 0 WHERE "id" = _id_user;
return true;

END
$$;
 8   DROP FUNCTION public.vud_delete_user(_id_user integer);
       public       postgres    false    3    1                       1255    16754 (   vud_eliminate_relation_modality(integer)    FUNCTION     �   CREATE FUNCTION public.vud_eliminate_relation_modality(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
DELETE FROM cat_modality_requirement WHERE id_modality = _id;
END
$$;
 C   DROP FUNCTION public.vud_eliminate_relation_modality(_id integer);
       public       postgres    false    3    1            �            1255    16774 (   vud_eliminate_relation_transact(integer)    FUNCTION     �   CREATE FUNCTION public.vud_eliminate_relation_transact(_id_transact integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	DELETE FROM cat_transact_requirement where id_transact = _id_transact;
END
$$;
 L   DROP FUNCTION public.vud_eliminate_relation_transact(_id_transact integer);
       public       postgres    false    3    1            �            1255    16764 B   vud_insert_transact(integer, character varying, character varying)    FUNCTION     �  CREATE FUNCTION public.vud_insert_transact(OUT _id_code integer, _code integer, _description character varying, _legal_foundation character varying) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
DECLARE _id_transact integer;
BEGIN

	INSERT INTO public.cat_transact(
		code, 
		description, 
		legal_foundation, 
		status)
	VALUES (_code, _description,_legal_foundation,1)
	RETURNING ID into _id_transact;

	RETURN QUERY SELECT code from public.cat_transact where id = _id_transact;

END
$$;
 �   DROP FUNCTION public.vud_insert_transact(OUT _id_code integer, _code integer, _description character varying, _legal_foundation character varying);
       public       postgres    false    3    1                       1255    25018 c   vud_insert_user(character varying, integer, character varying, integer, integer, character varying)    FUNCTION       CREATE FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying) RETURNS void
    LANGUAGE sql
    AS $$ 
	
		INSERT INTO 
	public."users"
		("user_name",
		"employee_number",
		"description",
		"rol", 
		"user_register",
		"date_register",
		"status",
		"password") 
	VALUES (_user_name,
		_employee_number,
		_description,
		_rol,
		_user_register,
		'now()',
		1,
		_password);
		
	$$;
 �   DROP FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying);
       public       postgres    false    3            	           1255    25125 P   vud_insert_user_permission(integer, integer, integer, integer, integer, integer)    FUNCTION       CREATE FUNCTION public.vud_insert_user_permission(_id_user integer, _id_module integer, _read_permission integer, _write_permission integer, _delete_permission integer, _update_permission integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

INSERT INTO public.permission(
	id_user,
	id_module, 
	read_permission, 
	write_permission, 
	delete_permission, 
	update_permission)
VALUES (
	_id_user, 
	_id_module, 
	_read_permission, 
	_write_permission, 
	_delete_permission, 
	_update_permission);

END
$$;
 �   DROP FUNCTION public.vud_insert_user_permission(_id_user integer, _id_module integer, _read_permission integer, _write_permission integer, _delete_permission integer, _update_permission integer);
       public       postgres    false    1    3            �            1255    25005 *   vud_log_insert(integer, character varying)    FUNCTION     �   CREATE FUNCTION public.vud_log_insert(_id_user integer, _description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO 
	public."log"(id_user, description,date_time)
	VALUES (_id_user,_description ,NOW());
END
$$;
 W   DROP FUNCTION public.vud_log_insert(_id_user integer, _description character varying);
       public       postgres    false    3    1                       1255    25103    vud_modality_data(integer)    FUNCTION        CREATE FUNCTION public.vud_modality_data(_code_modality integer, OUT id integer, OUT description character varying, OUT legal_foundation character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN 
	RETURN QUERY select 
		a.id,
		a.description,
		c.legal_foundation

	from 
		public.cat_requirement as a 
	LEFT JOIN
		public.cat_modality_requirement as b
	on 
		a.id = b.id_requirement
	LEFT join 
		public.cat_modality as c
	on
		b.id_modality = c.code
	where c.code = _code_modality ;
END
$$;
 �   DROP FUNCTION public.vud_modality_data(_code_modality integer, OUT id integer, OUT description character varying, OUT legal_foundation character varying);
       public       postgres    false    1    3                       1255    33385    vud_modality_days(integer)    FUNCTION       CREATE FUNCTION public.vud_modality_days(_code integer, OUT days smallint, OUT type_days character) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY SELECT 
				a.days,
				a.type_days
			FROM 
				cat_modality as a
			where
				a.code = _code;

END
$$;
 c   DROP FUNCTION public.vud_modality_days(_code integer, OUT days smallint, OUT type_days character);
       public       postgres    false    1    3                        1255    16756    vud_modality_record()    FUNCTION        CREATE FUNCTION public.vud_modality_record(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT id_transact integer, OUT code_transact integer, OUT description_transact character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN

	RETURN QUERY select 
	a.id,
	a.code,
	a.description,
	a.legal_foundation,
	b.id,
	b.code,
	b.description
from 
	cat_modality as a
LEFT JOIN 
	cat_transact as b
on
	a.id_transact = b.code
WHERE 
	a.status = 1
order by a.id asc;

END
$$;
 �   DROP FUNCTION public.vud_modality_record(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT id_transact integer, OUT code_transact integer, OUT description_transact character varying);
       public       postgres    false    3    1            �            1259    16726    cat_requirement    TABLE     �   CREATE TABLE public.cat_requirement (
    id integer NOT NULL,
    description character varying(5000) NOT NULL,
    status integer
);
 #   DROP TABLE public.cat_requirement;
       public         postgres    false    3                       1255    16734    vud_record_requirement()    FUNCTION     �   CREATE FUNCTION public.vud_record_requirement() RETURNS SETOF public.cat_requirement
    LANGUAGE plpgsql
    AS $$
BEGIN 
	RETURN QUERY select * from public.cat_requirement where status = 1 order by id asc;
END
$$;
 /   DROP FUNCTION public.vud_record_requirement();
       public       postgres    false    1    207    3            �            1255    16723    vud_record_transact()    FUNCTION     1  CREATE FUNCTION public.vud_record_transact(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT total_modality bigint, OUT id_transact integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$

	SELECT
		a.id,
		a.code,
		a.description,
		a.legal_foundation,
		count(b.id_transact) as total_modality,
		b.id_transact	
	FROM
		cat_transact  as a
	left JOIN 
		cat_modality as b
	on  a.code = b.id_transact
	WHERE
		a.status = 1
	GROUP BY b.id_transact,a.id,a.code,a.description
	order by a.id asc;

$$;
 �   DROP FUNCTION public.vud_record_transact(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT total_modality bigint, OUT id_transact integer);
       public       postgres    false    3                       1255    16752 3   vud_relation_modality_requirement(integer, integer)    FUNCTION       CREATE FUNCTION public.vud_relation_modality_requirement(_id_modality integer, _id_requirement integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

INSERT INTO public.cat_modality_requirement(id_modality,id_requirement)
VALUES (_id_modality, _id_requirement);

END
$$;
 g   DROP FUNCTION public.vud_relation_modality_requirement(_id_modality integer, _id_requirement integer);
       public       postgres    false    1    3                       1255    16765 6   vud_relation_transaction_requirement(integer, integer)    FUNCTION       CREATE FUNCTION public.vud_relation_transaction_requirement(_id_transact integer, _id_requirement integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

INSERT INTO public.cat_transact_requirement(
	id_transact, 
	id_requirement)
VALUES (_id_transact,_id_requirement);

END
$$;
 j   DROP FUNCTION public.vud_relation_transaction_requirement(_id_transact integer, _id_requirement integer);
       public       postgres    false    3    1            �            1255    33380 q   vud_request_address(character varying, character varying, integer, integer, character varying, character varying)    FUNCTION     d  CREATE FUNCTION public.vud_request_address(_postal_code character varying, _colony character varying, _ext_number integer, _int_number integer, _street character varying, _delegation character varying) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
DECLARE _id_request_address integer;
BEGIN

INSERT INTO public.address(
		postal_code, 
		colony, 
		ext_number, 
		int_number, 
		street,
		delegation)
	
VALUES (_postal_code,
		_colony,
		_ext_number,
		_int_number,
		_street,
		_delegation)

RETURNING ID into _id_request_address;
RETURN QUERY SELECT _id_request_address;

END
$$;
 �   DROP FUNCTION public.vud_request_address(_postal_code character varying, _colony character varying, _ext_number integer, _int_number integer, _street character varying, _delegation character varying);
       public       postgres    false    1    3                       1255    33421 �   vud_request_data(integer, character varying, integer, integer, integer, integer, integer, character varying, character varying, integer, integer, date, integer)    FUNCTION     "  CREATE FUNCTION public.vud_request_data(_id_user integer, _observations character varying, _id_status integer, _id_address integer, _id_interested integer, _id_transact integer, _id_vulnerable_group integer, _request_turn character varying, _type_work character varying, _means_request integer, _delivery integer, _date_commitment date, _id_modality integer) RETURNS SETOF character varying
    LANGUAGE plpgsql
    AS $$
DECLARE id_request_created integer;
BEGIN

INSERT INTO public.request( id_user, 
							date_creation, 
							observations, 
							id_status, 
							id_address, 
							id_interested, 
							id_transact, 
							id_vulnerable_group, 
							request_turn, 
							type_work, 
							means_request, 
							delivery,
							date_commitment,
							flag,
							id_modality)
VALUES (_id_user, 
		NOW(), 
		_observations, 
		_id_status, 
		_id_address, 
		_id_interested, 
		_id_transact, 
		_id_vulnerable_group, 
		_request_turn, 
		_type_work, 
		_means_request, 
		_delivery,
		_date_commitment,
		1,
		_id_modality)
RETURNING ID into id_request_created;

	UPDATE public.request
	SET  folio = CONCAT(id_request_created,2018)
	WHERE id = id_request_created;

	RETURN QUERY select folio from public.request where id = id_request_created;

END
$$;
 f  DROP FUNCTION public.vud_request_data(_id_user integer, _observations character varying, _id_status integer, _id_address integer, _id_interested integer, _id_transact integer, _id_vulnerable_group integer, _request_turn character varying, _type_work character varying, _means_request integer, _delivery integer, _date_commitment date, _id_modality integer);
       public       postgres    false    3    1            �            1255    33328 k   vud_request_interested(character varying, character varying, character varying, character varying, integer)    FUNCTION     !  CREATE FUNCTION public.vud_request_interested(_name character varying, _last_name character varying, _e_mail character varying, _phone character varying, _type_person integer) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
DECLARE id_request_interested integer;
BEGIN
INSERT INTO public.interested(
	name, 
	last_name, 
	e_mail, 
	phone,
	type_person)
	VALUES (_name, 
			_last_name, 
			_e_mail, 
			_phone,
			_type_person)

RETURNING ID into id_request_interested;
RETURN QUERY SELECT id_request_interested;

END
$$;
 �   DROP FUNCTION public.vud_request_interested(_name character varying, _last_name character varying, _e_mail character varying, _phone character varying, _type_person integer);
       public       postgres    false    1    3            �            1255    33463    vud_request_record()    FUNCTION     e  CREATE FUNCTION public.vud_request_record(OUT id integer, OUT folio character varying, OUT id_user integer, OUT id_status integer, OUT description_status character varying, OUT date_creation text, OUT date_commitment text, OUT code integer, OUT description character varying, OUT modality_desc character varying, OUT id_turn integer, OUT id_dictum integer) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY SELECT 
	a.id,
	a.folio,
	a.id_user,
	d.id as id_status,
	d.description as description_status,
	to_char(a.date_creation,'dd-mm-yyyy' ),
	to_char(a.date_commitment,'dd-mm-yyyy' ),
	b.code,
	b.description,
	c.description as modality_desc,
	e.id as id_turn,
	f.id as id_dictum
FROM 
	public.request as a
left join 
	public.cat_transact as b
on
	a.id_transact = b.id
left join 
	public.cat_modality as c
on 
	a.id_modality = c.code

left join 
	public.cat_status as d
on
	d.id = a.id_status
left join 
	public.tracing_turn as e
on
	e.id_request = a.id
left join
	public.tracing_dictum as f
on
	f.id_request = a.id
WHERE
	flag = 1
order by id asc;

END
$$;
 d  DROP FUNCTION public.vud_request_record(OUT id integer, OUT folio character varying, OUT id_user integer, OUT id_status integer, OUT description_status character varying, OUT date_creation text, OUT date_commitment text, OUT code integer, OUT description character varying, OUT modality_desc character varying, OUT id_turn integer, OUT id_dictum integer);
       public       postgres    false    3    1            �            1259    16413    role    TABLE     g   CREATE TABLE public.role (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);
    DROP TABLE public.role;
       public         postgres    false    3                       1255    25015 
   vud_role()    FUNCTION     �   CREATE FUNCTION public.vud_role() RETURNS SETOF public.role
    LANGUAGE sql
    AS $$ 
		SELECT
			*
		FROM
			public. "role";
	$$;
 !   DROP FUNCTION public.vud_role();
       public       postgres    false    3    201            �            1259    25079 
   cat_status    TABLE     m   CREATE TABLE public.cat_status (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);
    DROP TABLE public.cat_status;
       public         postgres    false    3                       1255    33428    vud_status_list()    FUNCTION     �   CREATE FUNCTION public.vud_status_list() RETURNS SETOF public.cat_status
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY select 
				* 
			from 
				public.cat_status;
END
$$;
 (   DROP FUNCTION public.vud_status_list();
       public       postgres    false    219    3    1                       1255    33347    vud_transact_days(integer)    FUNCTION     "  CREATE FUNCTION public.vud_transact_days(_code integer, OUT days integer, OUT type_days character) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY SELECT 
				a.days,
				a.type_days
			FROM 
				cat_transact as a
			where
				a.code = _code;
				
END
$$;
 b   DROP FUNCTION public.vud_transact_days(_code integer, OUT days integer, OUT type_days character);
       public       postgres    false    3    1                       1255    33332    vud_transact_record()    FUNCTION     *  CREATE FUNCTION public.vud_transact_record(OUT code integer, OUT description character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY SELECT a.code,
						a.description
				FROM public.cat_transact as a 
				WHERE status = 1 
				order by id desc;
END
$$;
 _   DROP FUNCTION public.vud_transact_record(OUT code integer, OUT description character varying);
       public       postgres    false    3    1            �            1259    16694    cat_modality    TABLE     '  CREATE TABLE public.cat_modality (
    id integer NOT NULL,
    code integer NOT NULL,
    description character varying(2000) NOT NULL,
    legal_foundation character varying(3000),
    id_transact integer NOT NULL,
    status integer NOT NULL,
    days smallint,
    type_days character(1)
);
     DROP TABLE public.cat_modality;
       public         postgres    false    3                       1255    16821 %   vud_transact_record_modality(integer)    FUNCTION     �   CREATE FUNCTION public.vud_transact_record_modality(_code integer) RETURNS SETOF public.cat_modality
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY SELECT * FROM public.cat_modality where id_transact = _code order by id asc;
END
$$;
 B   DROP FUNCTION public.vud_transact_record_modality(_code integer);
       public       postgres    false    205    1    3            �            1255    25101 !   vud_transact_requeriment(integer)    FUNCTION     �  CREATE FUNCTION public.vud_transact_requeriment(_id_code integer, OUT id integer, OUT description character varying, OUT legal_foundation character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
	
	RETURN QUERY select 
		a.id,
		a.description ,
		c.legal_foundation
	from 
		public.cat_requirement as a
	inner join public.cat_transact_requirement as b
	on a.id = b.id_requirement
	inner join public.cat_transact as c
	on c.code = b.id_transact
	where c.code = _id_code;

END
$$;
 �   DROP FUNCTION public.vud_transact_requeriment(_id_code integer, OUT id integer, OUT description character varying, OUT legal_foundation character varying);
       public       postgres    false    3    1                       1255    16753 B   vud_update_modality(integer, character varying, character varying)    FUNCTION     )  CREATE FUNCTION public.vud_update_modality(_id integer, _description character varying, _legal_foundation character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE public.cat_modality
	SET   description=_description, legal_foundation=_legal_foundation
	WHERE id = _id;

END
$$;
 |   DROP FUNCTION public.vud_update_modality(_id integer, _description character varying, _legal_foundation character varying);
       public       postgres    false    1    3                       1255    16736 2   vud_update_requirement(integer, character varying)    FUNCTION     �   CREATE FUNCTION public.vud_update_requirement(_id integer, _description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

	UPDATE public.cat_requirement
	SET  description=_description
	WHERE id = _id;
END
$$;
 Z   DROP FUNCTION public.vud_update_requirement(_id integer, _description character varying);
       public       postgres    false    1    3                       1255    16766 B   vud_update_transact(integer, character varying, character varying)    FUNCTION     (  CREATE FUNCTION public.vud_update_transact(_id integer, _description character varying, _legal_foundation character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
	UPDATE public.cat_transact
	SET  description=_description, legal_foundation=_legal_foundation
	WHERE id = _id;
END
$$;
 |   DROP FUNCTION public.vud_update_transact(_id integer, _description character varying, _legal_foundation character varying);
       public       postgres    false    1    3                       1255    25020 l   vud_update_user(integer, character varying, integer, character varying, integer, integer, character varying)    FUNCTION       CREATE FUNCTION public.vud_update_user(_id_user integer, _user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$ 
	BEGIN
	IF (_password = '') THEN
		update 
		public."users"
	SET		
		user_name = _user_name,
		employee_number = _employee_number,
		description = _description,
		rol = _rol, 
		user_register = _user_register
	WHERE 
		id = _id_user;
	ELSE 
		update 
		public."users"
	SET		
		user_name = _user_name,
		employee_number = _employee_number,
		description = _description,
		rol = _rol, 
		user_register = _user_register,
		password = _password
	WHERE 
		id = _id_user;
	END IF;
	
	RETURN true;
	END
	$$;
 �   DROP FUNCTION public.vud_update_user(_id_user integer, _user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying);
       public       postgres    false    3    1            �            1255    25122    vud_users_permisions(integer)    FUNCTION     z  CREATE FUNCTION public.vud_users_permisions(_id_user integer, OUT id integer, OUT name character varying, OUT id_module integer, OUT id_user integer, OUT read_permission smallint, OUT write_permission smallint, OUT delete_permission smallint, OUT update_permission smallint) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY SELECT 
	a.id,
	a.name,
	b.id_module,
	b.id_user,
	b.read_permission,
	b.write_permission,
	b.delete_permission,
	b.update_permission
FROM
	PUBLIC.modules AS A 
	LEFT JOIN PUBLIC.permission AS b ON a.id = b.id_module

WHERE
	b.id_user = _id_user
order by a.id;
END
$$;
   DROP FUNCTION public.vud_users_permisions(_id_user integer, OUT id integer, OUT name character varying, OUT id_module integer, OUT id_user integer, OUT read_permission smallint, OUT write_permission smallint, OUT delete_permission smallint, OUT update_permission smallint);
       public       postgres    false    3    1            �            1259    16396    log    TABLE     �   CREATE TABLE public.log (
    id integer NOT NULL,
    id_user integer NOT NULL,
    description character varying(255) NOT NULL,
    date_time timestamp with time zone NOT NULL
);
    DROP TABLE public.log;
       public         postgres    false    3            �            1259    16394 
   LOG_id_seq    SEQUENCE     �   CREATE SEQUENCE public."LOG_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public."LOG_id_seq";
       public       postgres    false    3    197            �           0    0 
   LOG_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public."LOG_id_seq" OWNED BY public.log.id;
            public       postgres    false    196            �            1259    16411    ROLE_id_seq    SEQUENCE     �   CREATE SEQUENCE public."ROLE_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public."ROLE_id_seq";
       public       postgres    false    3    201            �           0    0    ROLE_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public."ROLE_id_seq" OWNED BY public.role.id;
            public       postgres    false    200            �            1259    16400    USERS_Id_seq    SEQUENCE     �   CREATE SEQUENCE public."USERS_Id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public."USERS_Id_seq";
       public       postgres    false    199    3            �           0    0    USERS_Id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public."USERS_Id_seq" OWNED BY public.users.id;
            public       postgres    false    198            �            1259    25034    address    TABLE       CREATE TABLE public.address (
    id integer NOT NULL,
    postal_code character varying(7) NOT NULL,
    colony character varying(50) NOT NULL,
    ext_number integer,
    int_number integer,
    street character varying(255) NOT NULL,
    delegation character varying(255) NOT NULL
);
    DROP TABLE public.address;
       public         postgres    false    3            �            1259    25032    address_id_seq    SEQUENCE     �   CREATE SEQUENCE public.address_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.address_id_seq;
       public       postgres    false    211    3            �           0    0    address_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.address_id_seq OWNED BY public.address.id;
            public       postgres    false    210            �            1259    33386    cat_area_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_area_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.cat_area_id_seq;
       public       postgres    false    227    3            �           0    0    cat_area_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.cat_area_id_seq OWNED BY public.cat_area.id;
            public       postgres    false    226            �            1259    16692    cat_modality_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_modality_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.cat_modality_id_seq;
       public       postgres    false    205    3            �           0    0    cat_modality_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.cat_modality_id_seq OWNED BY public.cat_modality.id;
            public       postgres    false    204            �            1259    16746    cat_modality_requirement    TABLE     f   CREATE TABLE public.cat_modality_requirement (
    id_modality integer,
    id_requirement integer
);
 ,   DROP TABLE public.cat_modality_requirement;
       public         postgres    false    3            �            1259    16724    cat_requeriment_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_requeriment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.cat_requeriment_id_seq;
       public       postgres    false    207    3            �           0    0    cat_requeriment_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.cat_requeriment_id_seq OWNED BY public.cat_requirement.id;
            public       postgres    false    206            �            1259    25077    cat_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.cat_status_id_seq;
       public       postgres    false    219    3            �           0    0    cat_status_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.cat_status_id_seq OWNED BY public.cat_status.id;
            public       postgres    false    218            �            1259    16674    cat_transact    TABLE       CREATE TABLE public.cat_transact (
    id integer NOT NULL,
    code integer NOT NULL,
    description character varying(2000) NOT NULL,
    legal_foundation character varying(3000),
    status integer NOT NULL,
    days integer NOT NULL,
    type_days character(1) NOT NULL
);
     DROP TABLE public.cat_transact;
       public         postgres    false    3            �            1259    16672    cat_transact_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_transact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.cat_transact_id_seq;
       public       postgres    false    3    203            �           0    0    cat_transact_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.cat_transact_id_seq OWNED BY public.cat_transact.id;
            public       postgres    false    202            �            1259    16757    cat_transact_requirement    TABLE     x   CREATE TABLE public.cat_transact_requirement (
    id_transact integer NOT NULL,
    id_requirement integer NOT NULL
);
 ,   DROP TABLE public.cat_transact_requirement;
       public         postgres    false    3            �            1259    25053 
   interested    TABLE     �   CREATE TABLE public.interested (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    last_name character varying(100),
    e_mail character varying(100),
    phone character varying(15) NOT NULL,
    type_person smallint NOT NULL
);
    DROP TABLE public.interested;
       public         postgres    false    3            �            1259    25051    interested_id_seq    SEQUENCE     �   CREATE SEQUENCE public.interested_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.interested_id_seq;
       public       postgres    false    215    3            �           0    0    interested_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.interested_id_seq OWNED BY public.interested.id;
            public       postgres    false    214            �            1259    25095    modules    TABLE     �   CREATE TABLE public.modules (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    status smallint NOT NULL
);
    DROP TABLE public.modules;
       public         postgres    false    3            �            1259    25093    modules_id_seq    SEQUENCE     �   CREATE SEQUENCE public.modules_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.modules_id_seq;
       public       postgres    false    3    223            �           0    0    modules_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.modules_id_seq OWNED BY public.modules.id;
            public       postgres    false    222            �            1259    25087 
   permission    TABLE     �   CREATE TABLE public.permission (
    id integer NOT NULL,
    id_user integer NOT NULL,
    id_module integer NOT NULL,
    read_permission smallint,
    write_permission smallint,
    delete_permission smallint,
    update_permission smallint
);
    DROP TABLE public.permission;
       public         postgres    false    3            �            1259    25085    permission_id_seq    SEQUENCE     �   CREATE SEQUENCE public.permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.permission_id_seq;
       public       postgres    false    3    221            �           0    0    permission_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.permission_id_seq OWNED BY public.permission.id;
            public       postgres    false    220            �            1259    25042    request    TABLE     O  CREATE TABLE public.request (
    id integer NOT NULL,
    folio character varying(50),
    id_user integer NOT NULL,
    date_creation date NOT NULL,
    observations character varying(2000),
    id_status integer NOT NULL,
    id_address integer NOT NULL,
    id_interested integer NOT NULL,
    id_transact integer NOT NULL,
    id_vulnerable_group smallint NOT NULL,
    request_turn character varying(100),
    type_work character varying(100),
    means_request smallint NOT NULL,
    delivery smallint NOT NULL,
    date_commitment date,
    flag smallint,
    id_modality integer
);
    DROP TABLE public.request;
       public         postgres    false    3            �            1259    25064    request_audit    TABLE     �   CREATE TABLE public.request_audit (
    id integer NOT NULL,
    id_user integer NOT NULL,
    accion character varying NOT NULL,
    id_request integer NOT NULL
);
 !   DROP TABLE public.request_audit;
       public         postgres    false    3            �            1259    25040    request_id_seq    SEQUENCE     �   CREATE SEQUENCE public.request_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.request_id_seq;
       public       postgres    false    3    213            �           0    0    request_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.request_id_seq OWNED BY public.request.id;
            public       postgres    false    212            �            1259    33453    tracing_dictum    TABLE     �   CREATE TABLE public.tracing_dictum (
    id integer NOT NULL,
    date_receives_dictum date NOT NULL,
    folio_dictum character varying(50) NOT NULL,
    answer_dictum integer NOT NULL,
    citizen_dictum date,
    id_request integer NOT NULL
);
 "   DROP TABLE public.tracing_dictum;
       public         postgres    false    3            �            1259    33451    tracing_dictum_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tracing_dictum_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.tracing_dictum_id_seq;
       public       postgres    false    231    3            �           0    0    tracing_dictum_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.tracing_dictum_id_seq OWNED BY public.tracing_dictum.id;
            public       postgres    false    230            �            1259    33431    tracing_turn    TABLE     �   CREATE TABLE public.tracing_turn (
    id integer NOT NULL,
    id_request integer NOT NULL,
    date_turn date NOT NULL,
    review_area integer NOT NULL,
    observations character varying(2000)
);
     DROP TABLE public.tracing_turn;
       public         postgres    false    3            �            1259    33429    tracing_turn_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tracing_turn_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.tracing_turn_id_seq;
       public       postgres    false    3    229            �           0    0    tracing_turn_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.tracing_turn_id_seq OWNED BY public.tracing_turn.id;
            public       postgres    false    228            �            1259    25059 
   user_audit    TABLE     �   CREATE TABLE public.user_audit (
    id integer NOT NULL,
    id_user integer NOT NULL,
    accion character varying(255) NOT NULL,
    id_user_modification integer NOT NULL
);
    DROP TABLE public.user_audit;
       public         postgres    false    3            �            1259    25104    vulnerable_group_id_seq    SEQUENCE     �   CREATE SEQUENCE public.vulnerable_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.vulnerable_group_id_seq;
       public       postgres    false    225    3            �           0    0    vulnerable_group_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.vulnerable_group_id_seq OWNED BY public.vulnerable_group.id;
            public       postgres    false    224                       2604    25037 
   address id    DEFAULT     h   ALTER TABLE ONLY public.address ALTER COLUMN id SET DEFAULT nextval('public.address_id_seq'::regclass);
 9   ALTER TABLE public.address ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    210    211    211                       2604    33391    cat_area id    DEFAULT     j   ALTER TABLE ONLY public.cat_area ALTER COLUMN id SET DEFAULT nextval('public.cat_area_id_seq'::regclass);
 :   ALTER TABLE public.cat_area ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    226    227    227                       2604    16697    cat_modality id    DEFAULT     r   ALTER TABLE ONLY public.cat_modality ALTER COLUMN id SET DEFAULT nextval('public.cat_modality_id_seq'::regclass);
 >   ALTER TABLE public.cat_modality ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    204    205    205                       2604    16729    cat_requirement id    DEFAULT     x   ALTER TABLE ONLY public.cat_requirement ALTER COLUMN id SET DEFAULT nextval('public.cat_requeriment_id_seq'::regclass);
 A   ALTER TABLE public.cat_requirement ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    206    207    207                       2604    25082    cat_status id    DEFAULT     n   ALTER TABLE ONLY public.cat_status ALTER COLUMN id SET DEFAULT nextval('public.cat_status_id_seq'::regclass);
 <   ALTER TABLE public.cat_status ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    218    219    219                       2604    16677    cat_transact id    DEFAULT     r   ALTER TABLE ONLY public.cat_transact ALTER COLUMN id SET DEFAULT nextval('public.cat_transact_id_seq'::regclass);
 >   ALTER TABLE public.cat_transact ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    202    203    203                       2604    25056    interested id    DEFAULT     n   ALTER TABLE ONLY public.interested ALTER COLUMN id SET DEFAULT nextval('public.interested_id_seq'::regclass);
 <   ALTER TABLE public.interested ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    214    215    215            	           2604    16399    log id    DEFAULT     b   ALTER TABLE ONLY public.log ALTER COLUMN id SET DEFAULT nextval('public."LOG_id_seq"'::regclass);
 5   ALTER TABLE public.log ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    196    197    197                       2604    25098 
   modules id    DEFAULT     h   ALTER TABLE ONLY public.modules ALTER COLUMN id SET DEFAULT nextval('public.modules_id_seq'::regclass);
 9   ALTER TABLE public.modules ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    223    222    223                       2604    25090    permission id    DEFAULT     n   ALTER TABLE ONLY public.permission ALTER COLUMN id SET DEFAULT nextval('public.permission_id_seq'::regclass);
 <   ALTER TABLE public.permission ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    221    220    221                       2604    25045 
   request id    DEFAULT     h   ALTER TABLE ONLY public.request ALTER COLUMN id SET DEFAULT nextval('public.request_id_seq'::regclass);
 9   ALTER TABLE public.request ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    212    213    213                       2604    16416    role id    DEFAULT     d   ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public."ROLE_id_seq"'::regclass);
 6   ALTER TABLE public.role ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    201    200    201                       2604    33456    tracing_dictum id    DEFAULT     v   ALTER TABLE ONLY public.tracing_dictum ALTER COLUMN id SET DEFAULT nextval('public.tracing_dictum_id_seq'::regclass);
 @   ALTER TABLE public.tracing_dictum ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    230    231    231                       2604    33434    tracing_turn id    DEFAULT     r   ALTER TABLE ONLY public.tracing_turn ALTER COLUMN id SET DEFAULT nextval('public.tracing_turn_id_seq'::regclass);
 >   ALTER TABLE public.tracing_turn ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    229    228    229            
           2604    16405    users id    DEFAULT     f   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public."USERS_Id_seq"'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    198    199    199                       2604    25109    vulnerable_group id    DEFAULT     z   ALTER TABLE ONLY public.vulnerable_group ALTER COLUMN id SET DEFAULT nextval('public.vulnerable_group_id_seq'::regclass);
 B   ALTER TABLE public.vulnerable_group ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    224    225    225            �          0    25034    address 
   TABLE DATA               f   COPY public.address (id, postal_code, colony, ext_number, int_number, street, delegation) FROM stdin;
    public       postgres    false    211   �      �          0    33388    cat_area 
   TABLE DATA               3   COPY public.cat_area (id, description) FROM stdin;
    public       postgres    false    227   �      �          0    16694    cat_modality 
   TABLE DATA               u   COPY public.cat_modality (id, code, description, legal_foundation, id_transact, status, days, type_days) FROM stdin;
    public       postgres    false    205   l      �          0    16746    cat_modality_requirement 
   TABLE DATA               O   COPY public.cat_modality_requirement (id_modality, id_requirement) FROM stdin;
    public       postgres    false    208   k      �          0    16726    cat_requirement 
   TABLE DATA               B   COPY public.cat_requirement (id, description, status) FROM stdin;
    public       postgres    false    207   �      �          0    25079 
   cat_status 
   TABLE DATA               5   COPY public.cat_status (id, description) FROM stdin;
    public       postgres    false    219   sk      �          0    16674    cat_transact 
   TABLE DATA               h   COPY public.cat_transact (id, code, description, legal_foundation, status, days, type_days) FROM stdin;
    public       postgres    false    203   �k      �          0    16757    cat_transact_requirement 
   TABLE DATA               O   COPY public.cat_transact_requirement (id_transact, id_requirement) FROM stdin;
    public       postgres    false    209   y      �          0    25053 
   interested 
   TABLE DATA               U   COPY public.interested (id, name, last_name, e_mail, phone, type_person) FROM stdin;
    public       postgres    false    215   ~      �          0    16396    log 
   TABLE DATA               B   COPY public.log (id, id_user, description, date_time) FROM stdin;
    public       postgres    false    197   ��      �          0    25095    modules 
   TABLE DATA               3   COPY public.modules (id, name, status) FROM stdin;
    public       postgres    false    223   ��      �          0    25087 
   permission 
   TABLE DATA               �   COPY public.permission (id, id_user, id_module, read_permission, write_permission, delete_permission, update_permission) FROM stdin;
    public       postgres    false    221   �      �          0    25042    request 
   TABLE DATA               �   COPY public.request (id, folio, id_user, date_creation, observations, id_status, id_address, id_interested, id_transact, id_vulnerable_group, request_turn, type_work, means_request, delivery, date_commitment, flag, id_modality) FROM stdin;
    public       postgres    false    213   =�      �          0    25064    request_audit 
   TABLE DATA               H   COPY public.request_audit (id, id_user, accion, id_request) FROM stdin;
    public       postgres    false    217   ��      �          0    16413    role 
   TABLE DATA               /   COPY public.role (id, description) FROM stdin;
    public       postgres    false    201   ̉      �          0    33453    tracing_dictum 
   TABLE DATA               {   COPY public.tracing_dictum (id, date_receives_dictum, folio_dictum, answer_dictum, citizen_dictum, id_request) FROM stdin;
    public       postgres    false    231   �      �          0    33431    tracing_turn 
   TABLE DATA               \   COPY public.tracing_turn (id, id_request, date_turn, review_area, observations) FROM stdin;
    public       postgres    false    229   n�      �          0    25059 
   user_audit 
   TABLE DATA               O   COPY public.user_audit (id, id_user, accion, id_user_modification) FROM stdin;
    public       postgres    false    216   @�      �          0    16402    users 
   TABLE DATA               �   COPY public.users (id, user_name, employee_number, description, rol, user_register, date_register, status, password) FROM stdin;
    public       postgres    false    199   ]�      �          0    25106    vulnerable_group 
   TABLE DATA               C   COPY public.vulnerable_group (id, description, status) FROM stdin;
    public       postgres    false    225   �      �           0    0 
   LOG_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public."LOG_id_seq"', 106, true);
            public       postgres    false    196            �           0    0    ROLE_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public."ROLE_id_seq"', 3, true);
            public       postgres    false    200            �           0    0    USERS_Id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public."USERS_Id_seq"', 34, true);
            public       postgres    false    198            �           0    0    address_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.address_id_seq', 188, true);
            public       postgres    false    210            �           0    0    cat_area_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.cat_area_id_seq', 1, false);
            public       postgres    false    226                        0    0    cat_modality_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.cat_modality_id_seq', 57, true);
            public       postgres    false    204                       0    0    cat_requeriment_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.cat_requeriment_id_seq', 367, true);
            public       postgres    false    206                       0    0    cat_status_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.cat_status_id_seq', 1, true);
            public       postgres    false    218                       0    0    cat_transact_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.cat_transact_id_seq', 101, true);
            public       postgres    false    202                       0    0    interested_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.interested_id_seq', 155, true);
            public       postgres    false    214                       0    0    modules_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.modules_id_seq', 1, false);
            public       postgres    false    222                       0    0    permission_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.permission_id_seq', 51, true);
            public       postgres    false    220                       0    0    request_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.request_id_seq', 137, true);
            public       postgres    false    212                       0    0    tracing_dictum_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.tracing_dictum_id_seq', 5, true);
            public       postgres    false    230            	           0    0    tracing_turn_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.tracing_turn_id_seq', 14, true);
            public       postgres    false    228            
           0    0    vulnerable_group_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.vulnerable_group_id_seq', 1, false);
            public       postgres    false    224                       2606    16418    role ROLE_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.role
    ADD CONSTRAINT "ROLE_pkey" PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.role DROP CONSTRAINT "ROLE_pkey";
       public         postgres    false    201                       2606    16410    users USERS_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.users
    ADD CONSTRAINT "USERS_pkey" PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.users DROP CONSTRAINT "USERS_pkey";
       public         postgres    false    199            (           2606    25039    address address_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.address DROP CONSTRAINT address_pkey;
       public         postgres    false    211            :           2606    33393    cat_area cat_area_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.cat_area
    ADD CONSTRAINT cat_area_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.cat_area DROP CONSTRAINT cat_area_pkey;
       public         postgres    false    227            "           2606    16791 "   cat_modality cat_modality_code_key 
   CONSTRAINT     ]   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_code_key UNIQUE (code);
 L   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_code_key;
       public         postgres    false    205            $           2606    16789    cat_modality cat_modality_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_pkey;
       public         postgres    false    205            &           2606    16731 $   cat_requirement cat_requeriment_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.cat_requirement
    ADD CONSTRAINT cat_requeriment_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.cat_requirement DROP CONSTRAINT cat_requeriment_pkey;
       public         postgres    false    207            2           2606    25084    cat_status cat_status_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.cat_status
    ADD CONSTRAINT cat_status_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.cat_status DROP CONSTRAINT cat_status_pkey;
       public         postgres    false    219                       2606    16690 "   cat_transact cat_transact_code_key 
   CONSTRAINT     ]   ALTER TABLE ONLY public.cat_transact
    ADD CONSTRAINT cat_transact_code_key UNIQUE (code);
 L   ALTER TABLE ONLY public.cat_transact DROP CONSTRAINT cat_transact_code_key;
       public         postgres    false    203                        2606    16688    cat_transact cat_transact_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.cat_transact
    ADD CONSTRAINT cat_transact_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.cat_transact DROP CONSTRAINT cat_transact_pkey;
       public         postgres    false    203            ,           2606    25058    interested interested_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.interested
    ADD CONSTRAINT interested_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.interested DROP CONSTRAINT interested_pkey;
       public         postgres    false    215            6           2606    25100    modules modules_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.modules
    ADD CONSTRAINT modules_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.modules DROP CONSTRAINT modules_pkey;
       public         postgres    false    223            4           2606    25092    permission permission_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.permission
    ADD CONSTRAINT permission_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.permission DROP CONSTRAINT permission_pkey;
       public         postgres    false    221            0           2606    25071     request_audit request_audit_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.request_audit
    ADD CONSTRAINT request_audit_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.request_audit DROP CONSTRAINT request_audit_pkey;
       public         postgres    false    217            *           2606    25050    request request_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.request
    ADD CONSTRAINT request_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.request DROP CONSTRAINT request_pkey;
       public         postgres    false    213            >           2606    33458 "   tracing_dictum tracing_dictum_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.tracing_dictum
    ADD CONSTRAINT tracing_dictum_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.tracing_dictum DROP CONSTRAINT tracing_dictum_pkey;
       public         postgres    false    231            <           2606    33439    tracing_turn tracing_turn_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.tracing_turn
    ADD CONSTRAINT tracing_turn_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.tracing_turn DROP CONSTRAINT tracing_turn_pkey;
       public         postgres    false    229            .           2606    25063    user_audit user_audit_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.user_audit
    ADD CONSTRAINT user_audit_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.user_audit DROP CONSTRAINT user_audit_pkey;
       public         postgres    false    216            8           2606    25111 &   vulnerable_group vulnerable_group_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.vulnerable_group
    ADD CONSTRAINT vulnerable_group_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.vulnerable_group DROP CONSTRAINT vulnerable_group_pkey;
       public         postgres    false    225            @           2606    16815 *   cat_modality cat_modality_id_transact_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_id_transact_fkey FOREIGN KEY (id_transact) REFERENCES public.cat_transact(code);
 T   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_id_transact_fkey;
       public       postgres    false    203    2846    205            B           2606    16838 B   cat_modality_requirement cat_modality_requirement_id_modality_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality_requirement
    ADD CONSTRAINT cat_modality_requirement_id_modality_fkey FOREIGN KEY (id_modality) REFERENCES public.cat_modality(code);
 l   ALTER TABLE ONLY public.cat_modality_requirement DROP CONSTRAINT cat_modality_requirement_id_modality_fkey;
       public       postgres    false    2850    208    205            A           2606    16797 E   cat_modality_requirement cat_modality_requirement_id_requirement_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality_requirement
    ADD CONSTRAINT cat_modality_requirement_id_requirement_fkey FOREIGN KEY (id_requirement) REFERENCES public.cat_requirement(id);
 o   ALTER TABLE ONLY public.cat_modality_requirement DROP CONSTRAINT cat_modality_requirement_id_requirement_fkey;
       public       postgres    false    207    208    2854            C           2606    16776 E   cat_transact_requirement cat_transact_requirement_id_requirement_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_transact_requirement
    ADD CONSTRAINT cat_transact_requirement_id_requirement_fkey FOREIGN KEY (id_requirement) REFERENCES public.cat_requirement(id) ON UPDATE CASCADE ON DELETE CASCADE;
 o   ALTER TABLE ONLY public.cat_transact_requirement DROP CONSTRAINT cat_transact_requirement_id_requirement_fkey;
       public       postgres    false    2854    207    209            D           2606    16832 B   cat_transact_requirement cat_transact_requirement_id_transact_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_transact_requirement
    ADD CONSTRAINT cat_transact_requirement_id_transact_fkey FOREIGN KEY (id_transact) REFERENCES public.cat_transact(code);
 l   ALTER TABLE ONLY public.cat_transact_requirement DROP CONSTRAINT cat_transact_requirement_id_transact_fkey;
       public       postgres    false    209    2846    203            E           2606    33440    tracing_turn review_area_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.tracing_turn
    ADD CONSTRAINT review_area_fk FOREIGN KEY (review_area) REFERENCES public.cat_area(id);
 E   ALTER TABLE ONLY public.tracing_turn DROP CONSTRAINT review_area_fk;
       public       postgres    false    227    229    2874            ?           2606    25021    users users_id_fkey    FK CONSTRAINT     m   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_id_fkey FOREIGN KEY (rol) REFERENCES public.role(id);
 =   ALTER TABLE ONLY public.users DROP CONSTRAINT users_id_fkey;
       public       postgres    false    199    2844    201            �   �  x���Mn�0F��^��*�$�KQ!�@�R�f��j#�T-��z.VGm��%�����3�n�	� HO�j��}��0bG�&@Ya��k�nEd5%X*3m��/�M%��%S��uY��|�۬��$l�vb2�/��s�5ӅH���I'(ilIg�;6�����a[����#�\�'0��8b�vb�(�x�&v*�w�H���Dg����K�QDx�:�k��DI�h/��r�Q�2�'�O���56:n�����rA��E�����)C q�X��ȵ�rpv�1;�\�R�Ӛ�KC��,��%�AqV���g���i�LM�����K���'�b���{�~������v#F+�Hܦ�j�u;#�\Z���d��[^���!���^NW|7x��2�w���[���p'BU^���^�����[M��3����C�i���37?��� � ��X��b�d��p9��p��ݫ�F��Aj��      �      x�u��	�@D�V���_Z�0
�U�lC -唒ܘ��ܲ�a�9Y]�u{W���S�,h�)��b���b�x�jp��r]|{eej�dI�z꜇����πs��|Ӫ1;}&�t�����YW-#<G ��>�      �   �  x��\�r�F]C_��q���x��rª�V,G��ʦ	��Nh��]�d��"��������� P�$K*�������}���ۭ�3�Ϝ��\�YbD�D$c}��L�z�)�_|�b��Ώ�R�I�Wc�K�J�D:�������"��C��̈�*P��v�~�����Ј�L��6{�|��K�KrB���y��D���O�b�������������Y�T��L��e�T�������'���'c��=1����Q�1p��<n4�kؐ~���5¸��Ԓ�A���$1!�?'�!��uBŤ|�!�z�8��N+���,~�%�_��	���ש�35�Ԯ�X��>��ҩ/�Bo:d@_^���k�4��=џ����.�����sf���;���x΀��/U�- k�@��4cQ���J#� ���+�*��3� �&i�s�/T��ҥIu��%Ցe�1��"T��ߎ!s�-FK$�3��XP�_K`9%=	��x�e@�
��j��@0���g�`�@��Lq��
򰍄q��8�	d��D(l�W	.���+�՟���%�W �CB�t��Ц*g�|�.W��7p�G�.�P��ɂ��압�0r�����Q����A��xRG���5z��=�ǂ�ŚC���#�H�5��;5>�����^ּ%;���R��<́�VCsΞ�{�����4c�G���e`��B�[	��Z��\:�@��6]�i���fL��6�ڍQ����XhHy@�� h`C�Q1�8�}���Y�t&
EO��ãx_�����Ac��@��ȃ��6�'i��]��_瑲�z���ud�4M�� $ʋ��=�����j���z�ks�b|?�竫�/Ua�������V����Đ$�?�2�L�1��cU�/[;��P��x���
ѓZ0�����LG�����ag�=ׁ"��s�!_��ݘݤ��)ld0�n<l�A|,W�CҊX�>&�<� �[�P�C��p�ٿ�l#��v�ߛE"���^=5)����$�3���@�w�b.��4W�ՐdUraI�s��lE�v[ʚV�?�%Zdk#�&g[R�zMhlMj�ɷ拍�qP�)�¾�NkGP�hR��֓V�(!s��k�A�7�2�U���F����Q�^;�L:[]	��)Ƅ�";��(	�w<�;A飤���������Px��0������n
���D3���Έ(+�t�z�����j�����P��@:H'NC6@h�������81"5��m�D����Ȅ������T��M�Yԇ� h��2|�����^[�q���nCZg�=<�=�`S�# k=QK�� ykj�v����I1�
s*�.�iP��p�1Yz;�>z��3��#R�n�t#"�����充�b�ݺL}�&)��>���P}��䜾)�"눔/�4�a~.��s�*ѐ�2���%I#���!N�!�d�c\1��Og?Oa?؏ר���(�^�Y�^ӥ"�:g߁�I��R~4������Y���}T��Qy�F_�5�qb�a�����f�u/���_��fb�6}����)����� �W��X"{lSE��Z���R���yam8$��XY�,���74��y�i��lk��ͨ�z�7.e����Eק?���]ڪ��:o� /r��8& vr��>5s��;uƃ�#*����b��3?�Z9Ǎ�Z�t��j��}�1�K�U��ϪU�I�U|.ȌM���0�`l�4�x�e0X3���,'[���M/��F,�i�arP�Äb{�j� �L\�K$�i)�>4lh�n"z@|����s��x��g�Q��IBn;H�ʅVq��0���d�C-���;�"���گr���
5k��ŶiCA+�<X.�紕�>Z�@��H^"����s��TMm��h�@�j���4��S�p�Z�D%�$_�3��<^}��>�Y�<D��WC(#�*=G<�EB�˞ue���u7a�~�s�H6Ӫ��B:#����.�y��x�*�} u��(l$I`nc��~�π�wT�kj���ER��w��#��� D~?rb&�����)��n=np��/�`�@M	^c����bm�~��Tb�u��ʅ���q �<���?�\#�|�{|t���ݣ9>xnu�Q�2
L��}�`h�
@u�G��"Q֑ӌ�蒅�M%��HdhN6u)͠�7�#��76��J~Jn6y��B��4�L��T�=�D�1x�!��ٲ���fASn5N;+�y><�ί}r�m�l+=�?5)��$���d@�]&J�l�v�|^�x3�}�b�׵����_�[��y��;�C����7�x:�wP�?��u��M�;�u�?�lu;t�K�[�ck�<a��Aꛂ�7$H��Q��M}&��r��A��������~{��F7Oݎa�� * ��r��GYP�
몲��6|�����7߽�?.����)2%D�k��M���cD�Vֻ@��w�Md�e*%�D�A�N����s��>Z��uwj�Y��f���q�z�<m����
x�f��[ǧ@/�K-W���5/5�mu�sjŖ���n~�����Q��W�sm���ݾ��,�vU�wDG�l��y�+��^��h�}i� {�fR����67�{��1kD�i<o4�dW�|��v�,O�Í�quPp�x��^o��~�X�{��������8�}�?6��[�3DBŤ-Oj_�U�[�ݶ�}\���.r��3#y�Ů8�Xh��Xh�M6Lh5���YHtxx���K��=:|)�Y1�!&z�:��Ig��s���N:Kj�N:w'����_z�y쌇#DC{v�5��R��5�EL���+��K���u���	\��YYwu��v��E�}�����OJ�xK�m6]���0~�=�/����1:G�/��=�[ωD	&���Ӫ����A)�3+�m_[}��&j�����dZwX�V��H}ڎ:s��D�<w�m����a�����;�[�PEuW�!�K�����4eq���/i�ȜaN�&��Q�e�"R���x'�6�"}��B��f�i�ˡ��R�y'��׼�����y�ho6�t6�L�ԊHX&���E�k��-�ϔ݀%�����N����.�%B��s�\�Or4�䳝iY�^U�Dأ�:m=tj��[N��r�t�b�%��R:;2M������7a}U��=�]�[�&����v�[��&h�&���
'�|\����������?Ȳt      �   N  x�e�[��(D�9��^<������ʱ���}l�5�	�@��
����r��>��&�1ڍ�r�V�׏��>?:^ʴު����G�K�_#3�T�~+���GI^J��Q����[i|T_ʼު㭸�ڣ��})t�x��d��uN=}����+gz����+F���-����]�++ڮ��qV�U��\y��\��D\S�Ʃ���v��y�O��R%��=o���u�\$�.�گ�̹�[��Q��я��Uθ�\w��5Ԯʾ�9WI�ԑ����GW����]1R�'�ίd��S)��T�v߶���i�w���NZ�fR�g���<�b��w�CW��,z4���1�G�@y9��fՖ(��\ѷn�f@F�J���λ�K�1�*�c*2���YefU��x�?����^KZנ2�8����k����h��Uw��^���Ǥ��o%�"V+E�JLu^f�o�1"�c����d�sݨW�'ǕI;�;�"��F�j���d�cvݍ��*/��?���Fl�\{4�����L��Ιe������Ω�l�L�c8'��S��i�9y�H%�l8�j�:����ڸ�Ud L^g��f��2q���S&8��t���D%FD��ѳh~̪ڕc���]��,U�x3�������8�`	l�
 ��
S��?3�p�����<���JjX矜_��������of�n�c�q�/Z�X�>,���<��ۃ�O�'ʼ�,�w�������}��ݧ'�i�3��KpX����ƀ�)�]	�{2�.��%|^�=��W�3q���/�&��Cc�Dl���[��&�&�&�&�&�&�&�&�&�&�&�&�&�&�&�&�&�&�&�&�'Ӫ����ɍp_�M��D�ƨ����8TM��44444444444444444444�=�VUg$Mi���n��':4FMĆ��ǡj�Ԥ����������������������������������������ɴ�*8#ij#�w�?ѡ1j"6�?U��%MMMMMMMMMMMMMMMMMMMMkO�UU�I�Ᾰ�����Q�!��q�zh����������      �      x��}Ko�ؖ�X�Wl4�@r�rْ����89��<:I��"�0�H5)Qf=�㞝Y�Ѝ�
w��cw=���$�����IŒ��Z{�׷�'�ћ���Me*�����O�,���,���y���l���_�f�Y�$u]Ee.gU��6�g���63�|�ԙ�
s���,��d�g岂�
�5���f�|6��YR.�¬��L��%E�&�|��4����fQ�)�6�3�"O�R���3�����'�����`rI���fY�]��L�f���Y[T�Y��.`~��'M~��JS�pE���M�]�0ol�l��0���:�>K\���YS���4y3����]&�{��Zy��������#�3���6/X2,QWt4�h��VS&˶N��s�?)�&��s��k�u�k�g�I��s��񳦂�����Y���%�:k��Y5_TM��9��OƲϸq70���T���l����w>�E��2_�ER,�x����s����4_f����`���#37-�A�ߖ��I�s[��_�O_T�mRz� ���}�fr2���fY�3�׌�s��٥��!n�����!�5S�w@qp��#X |5�+8"�6����А�`^���|&ӢU~�a^�m|_)�14�"��W�I��	���Z�2N7-cQ��[��J�&3_�_�����:��_Jo�i>[&󌈪��k����#z��6�H�Χ-ͳ�Ƶ��-R�7�~W��_](����ٷ_���ܮ��B"�rS�ÿq#a_�d.<��5ٓ&��D�g@�I�g~�:�s�S�w!�3H+ݗ�mv�~��Kj8�%^�o%q�G�p�1�q	c��M2����l\�y�u0�|e.�߽6�.͛���߯��ū�s_�z������wO�sXZ���Ц����/�z~�O=�.�����^�N��3fp��sܙ����=]�����!��q&���e����i���s�؂� zỊYRÍYT��<85��tI�zr�X�^����\��3>�2��FX��\���ly9��&�+���㱃4K����|��ޑ@jZ�=��(x���Oo~o����	y��0cؼe�d	�	����ǽ��l�_��;b�N��0-�=	�R�"�DRr��.+(��H���\^�_���O?�~g޿=vuq�����4N�V���<Z��f󤣃�S�m�r�}��Z�]M̏ي{�|vN+zSWpi�-������JP�7�/�d�]��� ���Y�4�[��br>{�@�����*�`J��� 	<������/�Qp1�+���jY��E6�XM[�<PBIZ[���X@�WW�H[��Er�|Pe=�m>���
�7B��V3�_H�D�^ۦw����Ц�sKW�sm�N��./xS����W��P��ײe��7�t�(��nZMW��g�Ƀ����ob�Z<<QR�
�Q�*6�>�Oa�)C�4��Μ��R����_H�o��z61�����f�r��eXA;��3�����Uw9�֢�%�T�Kd0"\��A�f8�=39:�af�o)�v������C_}���bu
��T�r2�oKU�4�QA	�ݛ���£��߈ ��enp	q�g�����o�LGo.�v���i��|�+TC��p8�������B#�\�A)�D��+���B��1���F����fmʦ 2o!��:�nEYjk�;����FJ�=��!��g�F4�@�!M����'5�3P5�8y�p�y���#��]�|��o�f���6/_zz����8����#��d�'l�o����&3*[�&��9���-F�e��Lzɦ�"w�E�4:o�Wۖ=9��X[��dݰ4�����u)2�}7AdN�
L|,�T&�Q���ä���>����<��#�
���a�����ћ�7�ȕ�ҝ�� g4C}.܅�(|��>)��{Z`��?�q�8��l�^T`5�>j��$r���d�@�PV��9�f`��P�S����J��&N���Pf�-jIYgo�/�&g�Kh=��'���.C� _I��I�E��I@G����/�D�&���A C�_s0�OFG�ly_ß������ �<�@�3U��2�u�/��>�f7��T���K������Q� �j�Y��Ơߙy�B?���4�ţ��v����?�������(
?J�����-`d:��t���m���`�\�4=�X'�=,K�|�4a�)�9[FA��;y=�G3$�e�ؽ�h� fl�$�����'�Nj�2k��::r���t���k�^m�s����q���4�<����zxG�v�_ٍ�_t�hp;���<ޏ|��9�s��" �V���)6�����y��'�o��k�lI7%>���w�x��K��n�����	�_T�9�\1�fRbT�W;��w���]�D�����J='U���B�T�Ѩ��kU�o�����<��T^���C�7j�^��,��9|����s,`��F����w^���Q�'68�7����虆�[�3� ��|^�p�.ڂMޓ^E��s0����]���%�9/A�%�������AT����?P��Y�sǧ��UY�+��V���Y��:�a�V��e�fd���M���~~6��=>�yf?d�h��x�l@��i���9�OpDlq����<+��Be�rc�8�}4��;��xn��9P<X7����9�����؋���\��v�=��`� �����g%FR�0p�I�))�Nn����.�]|2��zf}uc�Ife���C��S4(��ml�<�����F�Uǭ2��3#�)0V�;�� ���k��Xfb]?`:dYZE����Z�,zip@�&���s+���=�.�d�"�cݨEo9�_g�ÕQP��Q������.�����c$)��0Bl��7�- t$������n��P�{}�iT��y�u�r��k�;�O����C&:�m��t5@ )�b��+r�
m�: B����GTU]S|{�=D� �Fc}�ƿ�o�Ơ�>�3,��ߒ�����񪏉�^�p���d��8�ӑe�|	6*�����%h�7L-B�AN@e~��B'_@���"��w��MA=�������rZ�X>8���#�����cϏ�g;�6�K���\a�^�=<<�:���ceC���&r}�ٔ��ΛpL<��I��/L�@J7�LH��]��HJ�#��ٽ��E�i���Ƕ$�iI�K7QQE����<����Ã��@�!�6r��)�9E='V(���~��9k%���b�x�X�`�b�a@�g�-s�����O�%R �k\��.k�A��(����4��=b��~_0/�X���EV�@���j��Kƻx�#NmW����@�F�<���RY�ܔ������V9'/�َ��t~�O���I\Y���3䄸��r28p�eft�dI�$���2N3y����o�x`�xs�$�R�pTE�_خC�n�9}��ѳlV$^��g2^.��@C,bl�9q7PQ�C	�F�{�31������%Aڟyd�
1��t���wʷ{:�wϲK6�wY�~n�d�ZGL�����p�%�8�K�G�/�ӧ�ce��Ꮞ{4�@���[�v�bH��������J�~IG���=���#�E]v$e�����,k�=r��uVEro9�ܞ��ƹ�@�]'+�w��4����'@���6�1@�3�1&!��>6斤j�8K�#��S _������c6k)
,�;�T�zlD���۽�W�J(�Ϧ�;����@��DN��-$��g
b�k��c��9>��(�c?�wY-�%��L/�odp�_{�N�F'�ԏ�g�>餤�L�����(4�!Oa�-f"�����	�66�0��%=��p��䄦Z�7C���%������%Jldf��?%���جӰ�v�i��ٝ���.+ɲb���jF�{��-�!Lհ1i��,���U�R�5M�n�hʯ�_Y���#�?.сӴ��&��6&    ��9��p��e�ހ�8�D��>u播K��8��B_JT����,e�������h���G�i[��m��Y�믍�̄Է����Ω\���Q~�r�x��0������˕��[���2����sJ���+�r�|V���E�s�5�U�R����xm#�1�<֕ܶ�X��qa�н�#��#��+i���:[a�nP��Z6�� �ͭ\r�3<�%z�`qk���Q�mS���	|&XF�$0N����l�9&��R�^b�.�iاuS'�qvg���ʀ���y9+�UF,��7�B��֪�}���Q�����R��]�V7��^2�_�*G7��#��"�r$+�<�����Sk?�/��t�?�G�����f=��Ь��U�� ��M���{}Z����3F��K��X�{I�A�OGo�؀�c���[�A�;$쒒.�
y[��u+�FP-H�OA(b"���_X�4�o���pm)O��� p�9K��5-V�gAO2�u�nS��u��hO{/�f��5F�˴����ֻʿ�,m��;�d��MԲP�I�<}�<fE���	޼z������̻�o�}7��Ǚ�ijƄϕ>�U��B�lqc���A�5\<�����h��Y��$��9����ts:5=�%)y��	���\O e����<��qko������<[[/W�V /��,)�k6z��^tÚ��������)N�I�y]ͯ��A�4��RQ��l0�O�ڃ�3:=��<)[��U(*�\A񱉆pRK�o痒(y�D���H@1w�ao3�#��L`PR��~t4��:�֑Ktz�`�EΪ�N�Ҳ	��q��c����Xl4��F�0�����`����Ts�+��P��R��`/�T����eK�������� �ө$��}i����D[�ڍ4��>��L�1G\�>��}ª���4����H��v�o�cK�]A��t{<����ιǯ�9xN�M�`�����f�����J-��@��Iβ���������T�i��:X�����Y�13�"��j5�9�kŹ�)٢���BWY%^�gp)���pA�P�|��'>�����~�K+�-�ڴ�1
J7�������I�"^l��nV_�f�5(4Ȅu���(��t��8�^-������z}�Z�!.Yf6�6��xIe�\G!�P�VblZ�s{r�6ӵK��=�Ӄ�g=�ݟE�-Ł@8���-CM)��@+���F�Uu�o�S"'��S�	� �?j�Ԋjjf7,BP�O?6�KpKƚv����u�'ȝpu�
AK8�*��z���vf������#�F�@Ǯ���P�g��3s�"��i���9�_�[Yh��y�.J*�0�� ޣ�y�9��'�B[���-y�zmԇK>�Ũ��~\c�D�%�JQ�ߨ}�iZ`~���2)=@�~���2��l�BYօ'�
i0�S�"O�.�[��Y�]�*8(�"��J�V��h$�J���2&xgNy�ԉ����P���$�$��]J��(9�H��Ux/f9S�/��Ca��6 q0�x���G׿�� �1���l�\hQg�S祟�m��Wdh6�R���̺Ҽwzs��C"�:>=
"
���(�]���������x�.������S����߂o��d�۬����ICo�˭�/���%v�G<c�s�X]��Z1t���<������X^U��ݺ�XNC�B�:�-!������@�x�D�����&H�CY���|i�ip�ieb�q?��h���%ixm�E��Dn��~��8�m�(~�6��'\�sau<�#!J�S��ӰZ���(�z�G06�W&�	NϜ�L�7�#f�
lo�>���hո��ά~h((��i8	P����u LP�G#���?X:�g|��nZI��7y.l�	��1=
�j�W�66)PG�;o�aS�B�P����{�1q���Ì��v<�}�L�9B��ʹ�O�D"
���Z���S*���t���@W�T�XS�%z�hK�`C� p��$[�i�t�m��؅�1�[i�f�R���>�ۘ��_]w������#�y�*���x�%���]�c�_O�������Q��������.���w�����"�MP�q�ó�~�#��n��<a�� 7ʰb��2�
Y�a�RZ�?�.����_S�ކn͒5I�i���\�/(�P���;�
��	O6i8d�Q���K;8=0L��b�a�6��|\���v~�HC��pxW@��ͮ8��I�*Q�c�/�� BO=���H�u��A`�6�)���Mm	c�˂ ��"j� T2�Q���=3�p�I�U^���-m�J"U��0U0�=Ѕ)��*N��6�Ћ�4^��x�c�Q.�n��0������߿�|����
#'(�t��CK����L��8ַ�e~MV6��M� a������
���.PG�1|\��4������������aT�wx�|�GuS���.�#�L�FjCg����w����PRV;��^�ō�g�Y�"�O-5�yy��i����y�z�d}~2?��Qx7��|�Y�s�9�b��u��i;͎��㈦@��S�I�Z�i�/2h�:����[H���c�&5V������y��S2:γ�'�j)z^����Ɉ�̞�HMD��!,P+�o�:��#�#2
��w����}��5`�,��Fv�Fj�����j#�1��l�.��gϜ��{�}s�T.V4_؎	��WJ˙9 /�*ӹh�۩�c��d�W#�@TZ'����9�SF�I����E��=��R����x���?��2CdM�%�)�U�A����X�`s,9B،�|��Q�x�q�y;p��}Wj,ᯤSs�tj����H�_+Iҗy��8D������w�{���z�-P�Tm�L�/
,�b����J-�ck��	�e;�B���9h;ع�+����X���IJ^.�WU�b��6	S�)	�$����"�Iw�����l�xJ���v�o������4B�1���A[S��ͳ����+Q�5�o��W��K���y�X
?`���@Gy?"M����gǣs���_�n�<y2� �)2��R�W�����s�ܖ��޲�$C����V2ǌPk+��R%��-0�&c[�m�*�v̌��i��V8�j��,	�WK�W������0}sr�7O7�'���Y�8V���e����kx�2�l�3?53~� ��^_��U�6�2
��!\���_-�V%��za	\��kd�6�xj~�n�Bݓ����0���h�����\����q�1x�+[��Ȁ��L����G�{���'�<�k%�i2�u��B�⠀����*g�H_e�s��N������NEx!B̓aV�5V����[���Fa�?E��\(QGöFZ
~Z$Q���"h����o����}i�jpI'�%���`8���z��CC��}t�� ������Yn�|s�����F>�tj!QZ���!��PA�1\��VP��օB��xԕ/�z�z`"��A]��G��¢<ҡ�t88	��;�u��@�aJ�{:���{b���n����/���Z��H����$�`�O������ž�1�U�wU��Ω��nZ:1��\��e�h*�>]����n�Y�Rs���a"	@��Z��a�5�a�.�٧ܳ�, �\�Z /��A^�^�Wx�f�1�h"6P%)�4_�� �%���|��Î���L{�A�s+V��	��p'[h"vT�Jrd�B��̜�%`k}��d{���Q׎&��'��
��Z����iV8]�Q2Fd�"/C1��D�r�	.	���.7cЬ�.�`
U}�d�����[YUO��m	�o��V*9!/}��dpNm�VҘp,Z\W����:4�"��W���K>����"OFoY�H�0�u�[	3��З�;4we-s
ղR�$
�V�-���K�]�i��MyM��̦����a����Y�6�l��&��d��q9�@�D����/Rxޝ��    z����2������?x	D�@d�{�W �����N8���<�W��ED,�y����8�3i>�X'q�n4��}hA��
���{��L#D�^�s��dR��[�T�tQ��[㷓�����Ͼ9|�lB�G��|Pxr'�Q����»�����@���!�&�oҨ�O屮T��[5�&��/F��Ƣ��0�X>57�&)L�瓞7yi+Jr{�4KG{Gs�"nE�+��<�����!Zj�]Qm79ƞ#��{�4�o,�ђG]T�c��N��2�n���5F�_n��Rw`
5�,o�l����ZЧ|�Vw�Zl���?�xt]5��Z����{W��08�iB�=X�t���$�j�-��&Y�]�z+����Y?��P�EcKE�߮KXp2�J-6��G>>�GT����(դ��oCCn��)���{��f�k�S�me��&���/f^��g�q�b��Yϱ?��{$o��"%0�B��S��'u���f�D���b�8�!D���#���Nɋ!SP�gOoCzt�4�8(���ؿ�OB��ּ��DJ�Y��k�V��C��@�C� j�upz��,�����14�LGڋI���-���?�K\��m��.rn��r3��gV�m���4Y��5J���E}��@pb?Я�����;���!���+��IX&���|[!d����`/%�����>|B�ߏ�>1d�f�pd������݇� C����R��+s*��R!���)����|��>$��`3�k��<;Mg4x\�'��'����d�8X�"+�9L����t^��:>�y\<���?�h�L���7�����51҇��&��.��7+���B���V`�2�fp�V��󠝞w0�,���lk����y�&z 8�oFܸ&�n�P��q�d':�2_��ť�Cˁ$=l�&vDX*j8c<�"��$��T�)�&.c�j:8��Y��&�4*GYY�*�rG���B 6�*I��6���Xd�qV#��20�c���f���i"f�+˒��m&�A�9� s8z�� �5����1�W i�i��LhQǱ����8x��k���:XX����0��U���>��l��N��k��8��W]CXirMռ�I����.��AwU�x�F���m��Uϛ�o視�9����8�G���Ҹ��;"_z���K��I�'̤�4�!|󹸤�bي<5���y{���������1전�B����"C�.����=+�Q7�j���Mojr�<�Y��~�Gq���K
[�-��>;2F1�(�d����"f�%g�g"h�� ��Y�0��0�R�R�M�8Wܠ�6�9�����Q;��M��L��]�2�#\�Ug<ruL�-��b�+*�F��%�i-����|d����׍�v�n���M��ϡ��[q�$,@
������u�$��A�=y 4��I���l���}��۲
_Et��͎\DN]"�������Q�]N�b�p(㊊e�8��7����8�m���-�7�N�t���P�[7��}��YqO7������沴�v*+���8��O�«�8G���2��r
��6H�v�"@��c�4q�p�w:�����%�Վ@�~i�Q�ϥ�d4�>� "!�����}�ȼC���fū޸5���*�>���$1��ú�.��d�)�IAag��%��3<0+�\v����].������pU���^ުw��Ǯp�%��F%��^'�b�൸�;�|/oV�\\��,ͥ��dP��REg6�g����ӱf-`G��܋Bﻹ�ė0����n8����!��=�ȋ�j���s��S�@��ct�L�O���Q9�ҫI9!�
q�i3�CfB5���u�S[��`.Ȫ$�BM��!��ƶ��&�bq`s␙W�h�4ˏ^:�w�jN�
�2>�<�GH���?#3'�x9����`�W�!��O��?�-u|dn{��ܰ�憍���>n��3��Kg��$4ލ+Q���Ė�O<��HH�^�r�ݭ׌R#�gQgds7�
�g��6���B�m��2,���W��B��$Ա���b�Z|I
� 0�z��l4(�~N�9��]���;{�8<q�X�z
\�*�S����P�r�%0�y��gi��Q*
�(@�} =t\�*�c���Q�'tb�����a����X�L��}�� 	_�b_!b�P� �8�Oa��\�7���"��1�<:�0}����0��٦����s�b
�ɮ:����8���
���|��3ʴb�rd^�{q&7��x���tETt4�1���RTe�7�l*F�#E�`j� ����-�<s�БOAs��F�m�<X���6h=��,3��-��3A��a�M����TQ5��1
T>F���@�n�D�~o��q��͖w>`Hn<�H�ݍ�A�_�h��@>��8���4�g�Ҫ}hS?�����G�-�������]�W������tY�*�uQ�X�.���5���(>&�`Фݦ46�R�MN�͸��8t7�n�9�E�k�o�l�6���3�/����⛡i&����Η_�g�%my�.����,1�VK{0�(j�&��[��#4A
K3��0"}�I��d�i��\�/o�zZ0��p�t��}Uƚ@� �1���v�m�����$S1�Xp��L=!y�W��0\cu�\��0CeNz��? ��[�\���p��m!N ��f�L���C���\BHMʘ�(�a,V������B��S���/�͇�D��O��A���1�8�lp\�y)���pbI��d�I�ֻ�@G.�Na�P@�^Em��8o0�M��;"�!��Ҕy��������������cs�����l�© �o	�V�3�̹~�q!�A�^�7���"�Lؓ?X�I�$�I3�ݦ��l�`2A���(������$��Ԝ��w}�D~m��$~��=�΁9�&RG���X� �f���W|�,R�\G�E��1����{]	"�vwjTCz��8c��waH;~�l�jyo9����o�Mf,�+f�_�+������.I]PlF>p�p㱌��Ab�<̕�~iY;�e�g�gA�T����
��E��*�x���b��~��n2���ښ�@��8fk�-c�lpf��ݍ�$��i�&�zZmV�w�
k�3t%5;��7�	�p�9z�o����ʁ����5�0$�4�֢�J慪$e������;�܆�<MImJ��-;-�(J9}ݤj"�M�m" ��"q9��:K�T������|��8��	?c׆��w�
&md�)��3�E8�����x�W{� &nJ>�D��We�)��cj�Pq�8���lzwEbb<,[o�}���N7R
,!?�'�_�S��:aL F\Z�A�|�x��~���5>\xc"�p�e0�ڌKNy�[+"Q P�-'X�v�oR�t�`����z\��17|�����̳��^���J�j��u���L�mls㒯y�i$�<^֖��*�\�w˹\�?�dxq�"_��5t$�W��ŭ���2Q�[����� ���>��RT��o8��+���`��{yOW}������3J6�3O�T�����U��n��X�#��5@�E<ZU����K��{ �6Z��%��	�,)�}e8��l6�M�J�_k<&������Qؠ4��6F��𲴼K `{�;ơ����~�͸k<
ya�l�M7���(ۨ�U5��P�N��i��&��s�\YJ
J1�DZuA"��k��@�����B�I��i�	Q��	��9G^�}����65ϥU�o1�6������.ʓ�X`�0��ƺb�`�Ҟ\QAx/h!>���{��ʼv�(��ޟ�mu���d��!x���7괚�m�B�ڙu`6v���͛
aj^U�G��=}�x`~�ћ���]<�ɢs�	�.����/n��[��x���^����	ŝi��(�j���㸋�����gKw�)� !HK    #B�+BnC`[Q�IH�)�~��=rnQ�h�y�=%X���8Ɇ��m�GYq7d����B��c(	��>�P�()D�s�}��y���l�$Z�p�nmu$j"H!�ҕ�ێI�)�|�\8c�nR����k�"G�^Y�P�c����}NHH�|$�<L3�M
l\� ��͕w��?��v2�m^������Wq�v��c@iꚴF3:�Q3�`����JT��z48Y�
�HJ)�1�UX�ׂ&F]*�������j�W���M����vܞ_[�Eve��ۧ��߰�����7xr͝��Q����fhý=j�Z5~gI��&�(HKa=��2��O�BM|5zU�4b�|��ɱ	���M��vi�BA���=aW���-�("�	�e�i�X�|��#��J'7���#S��6�m��Y��n ��d�M�x$�y��2`M�h�+Ʌ��J��:ZvR�A����D��z`�����0��$�Lz�z!%<L�\��h�<��2�Ni/N77J3�ľGX�̷�*O�=#�$��8��m"��rI��kߕ�ݻ( ����0U����PX�	��N����}�QX[
K$P��{v�������6o«((�\��u��m�á?)ox����_��\����w�^�:{���K0�z������W��o.^�Z�R��+֏��m9��Pl�/���z���~���r���Nco��-c�* $ͯ�EY{}M�K]�۩��Ө�CbM��ل����]�u����l�q�K��߫�t_��d:z.���ڸj_I��.͜�fw9�|V����A�!d��7X�F ��w��K�W�DH.a���hV�S
�=�����˜Z7NN����āk�Bc&@��J���Z�H��b?c��P��n��~��Ѫ����.淍]��z+6�al)(�HR_���	x�������E�
V���i��{�!�1P��|z؃E���>�k��c'���0��/�w��5����ԉ�����;ĮD���ς<ؠQ)�7��
'��⏯Hhʒ�����,���έ�@=p��׵�t�w��M�����(ja~y3�\
b��ÇOc]�p�ih���y�R���!w�k#L�zC?�VN�Ӎ�F�g�R �dv�\���lX�
�z+ɠțι5�yC��[.�?�������O���BP\�5%y����79�ג�/�1c�G~�-�
�9T����u2������5M!��6�hJ��I?�ȶ�W�k�~j��tT�<ݫ��;?�� ��MJ��y�=L�jM�����I=�O��l�u�9��X>�s���v��mp1�X.V�����7$��_aQ�P?��<y�YP%ڿj"�r��-"��N���я{od��l\�o�S�q�L��K{������_�C��/�*�*Z>5�A�&�X�C��\l�	�l����i汝':Mv;x>z�Jʎ�{�7�*F,����S����(�I�8a���8i��<��$��)v�~D�D�C���E[�KL1		"�����I{��	��#ٲ���*���2U��������yс� ��&�h2K�ϝ�������*Q��<i���dR���-z!z��}a�$,aM�������-�邕r"Q:��:��'}{�_'Y�k�B�����j`� �U4��(8	�x����Z�$ ���9+�Y������Ej�n��u�Aw<����݆߮�k�f��2��X����������������q�+u�v�2z<Ԗ�qz&���ȟ�wB݅L��]�=�o|?�}����#�Ȓ��9�"D�?'+�!j啦S��o�m|
��:ob��H�͂���B�N�C�G���w!v0K�zY��3��U����������t�$쮼F���{�{Rpad���'����hPzm�Q��t�յJ��t/���3���	ZQӂ�R*�����f��'B�6�F���	w��Kr<:�N�ή�#k7�
�0�1C?�`�W���Χ�L����@|:�~fT�g�m{�8��
Oucp�m�U�"ȁ��.�~���[xm��h����F6��5|%gZ%}a]�y�&E��������f������N\'�˟۬XJ�&��N1�Q�#1Z��h\���xMc{�V�9��K����WAig�9����$��A�!!s���h���=;�������������zl����}�Mi&O)af�N{��U4���T���)f�s�U&�N�-�]�j��2�䊢u�\=�4�WM����OV��c?~�~t��?�;����9��N���J�;��X���	q*�I�j6�G�f�=#��hx�F�U��;����,tj�*s.�>��30J��G��ҞVn�n.�`�ckv�܍�[fkWO��#h�)��E�'���Э�%��{l��t���mE� @h�
a!"�UK�q���D��{��l���bP|�6��h2k�z�Ӷ���q�{���Iu*@������P�y�\��%�.4���y�}�����$�6�\�O	�ȫH<�G�G��{W{o�Ơ.%6/o
I�^�jeB0P79F{�����%3mj�S\R�g���=�� ��}ڳ`*�q۸Ա�I�����BB�3������q�Yf�J�<Q*�q[J�۰[��T���6…߱Y����C��2�9/cY}
�ү|ߤ�����3����:�x�Acܣ#�:�g���Lt��m���3~褡����{;�֋Y �H�\n����eΞ�)bH�4{��M`��k�~���EMm�:%;3�يCB
\'.8�������d�ڛ�9�?	ʂ��o4��/A�.}��-�}�%��z��t!�<�%�Q��y'��SSyO"_�����!�:�d�fضJ����-�{�-�</M3�ܾ�k�|�=�5a���ʹa ��T
('��܋)h�$�V`�PG8���Z�A�+3mY=5�(F�����NG����CU
/oe�p� 
��) �����N�?�ѵ��Qa8aHsELJ���R�;�{5T��'Q�-�1ܹ�������*E<�Q���4	�8FMhI����C�G��1���������A>c�h��.�\��1��+;�v`I�ه<an9(���n/E^z܆��8��HO�1��Ng~�i2�XkRe*ɣ�_e�8S,����T��(�r�\I��*Y�K/�4\g6���#�멅�������B(;~[qEb�B.̡B�Yi��Spu�6'��،@�#�VZ�\ �0(f0�� E.'Z�l�S�&���dlN����3>��xؘ�(�/0�63���Ĺc�6����Y��FA&��Ȱ��K0���T�`R����	�e��;��Et��C.�%���2W���eGOR֣�tR�1mH0/1:�������s�X���m��rc"���33�KN����f�2���6�+����:��$�	
����jO���c��܏����A�������LY��� t�S���&��%�n�~1�Õ�>IJ��ȵ-)����ju���X�s#p@p��t	\٧܇B��v|e6�o�S)�2�iX�&�7�)K�T�-��j�+.P����;�ũ�oʧ@��l�3eSW�����������cXg���`;K�b5��sŝ�Sl��>)㢖�|w���5'�jS�h��9M��e`����/E��v̵`�s�~$������t��I��9E�~�,�k�<1�K{�"=�����u-�Ё�>��
����v�;N� ���ch��H���t�������}[�6[2\ށ�5B-�\m"=#~���y�`=�~���'�`�a͓���v0�������CP�S�6N`��uv�#�,�(Q�H)0�Q�1(�<��oě.@�Ĕ$�� ْ�.�$�"'�}�A&��)|	lA��)�
���ҕ��ː�&<ZD�6��ʥօiO�b��fS�ۚ.�m����-ҕkٶ���m����w��͖a�K �  y�+����	YQ%�V-���i�\TFb�����)�`i*tK`��+����8�D�p1�X����,O����:�z�^�>���;Ѷմ�cb���SZĞf�͉����]`�x���;�T������-p�U��gȄ�A�ݦ(���m�y}A�U4�������i�6�S",ԣ>���OȤ�8$L�4��q	d���%
�/m,�R�MV�j�*[���8�I�3�)0��}Z���
[*�#�u�JxȆLdh䅊}�<�P�-�<���j�X��x��ɝ��.;���o�b'2�y�O��l�oЪ�d��~#��E��LG�}@�d�oB���8�>:�,@�����}���wA��C��G0��EТ�:�8�����~�"��+�b!<�yD �-�4pm����؏j ӎQ&���g�~��g�m\�N��AT.�+�=����X��(�7c�4�1���aX�Aa ��J���ZIo'�~�,����	�czp�#3�t;	W�l�x�~p{��M�=EH��G��{�c����셝�b�'�;����CDq/>� �M���YV���nD�yl�607�2���T���ŷb����E1�������X�{eq.��g�	5�S��G53�eI�J�a���������
�3/*j� �I��FرA�1;6v4��N��x�͇�:_z�&�83h-�k���m���>b��Aw�0��\xxR�-� ax�G�Q���U�>���Gk����H("i,���-�o����Ő���K���3mY�Bz�N�$N,ڄo��&�;�rB�mLK�M|�m2�d��Z6��c`={�Y�������8�<�%k�;8Ly.t��!f���m�K'i�dX
)R�'+{��4�r1��Cb�5$��aKFn�XI���aӏ�� -(BC��a)�HEp	P���e���q��1��m&C$�M^Ϩ��;�E
���&1�����g��h����?�#C��+��d[���5*{��ߢ���@��C��Ý#7�o�E�����b�Kyaբ�I�E��w�*7�"깂	^]��L����S����h_K]\pQ���C�kY��N�`���A6�n�T�f��jF��3�:@����<�|�H�!���x��0��.��2ʭ/HJ�?5y��8�-e�
�9��76��)9��ݰ����fa4�r�ʯ4�(0@~h�]-�=Z�xjKņ�{�9��eFғ e>!�W��}l?�2�*i�D�9P�,�����?����j1,/���A�Z��v�5��JYbJ|�7�-ËP9g!�G����P��Ɂ��J���,Z�z�#��a~�o�V~��b��O<�E���i��/=�E��ո�J��_�9C/:�^�f�Z�B���S�����g��N��]���yji`�>�`�#P'\#B(Y>	t���P)�$�#j�y��OQ�� �X��h��;m&R�-��X�S���:�,s8�ԠÐ��)�$����4�Tv'Ey�uQ
��8�<Ѭ	�h��-pM���=DT&��4K��)q�R�
�;��I���dK4t(��P����1����ӡ���`W�Fs��j;]��dCG�Y�#�����Î�a)�E����5�GB�V� 7$w�b���O�ӝ�CpB��P�<�>�	{�(�L*c碽Y �$�n��ɮ�m)�W�;d��Lz�!�)���S<�t �pJo�� N��9���`N� ����t:=���*DvPMI��@>����I���-��K�!�Ι�Vj�џ4���5�Wh)�n�����/L\yL�h(<��(�zJ��N�g���#�Z���. �Q�����R�y��t ������&�����A��z$��]��U�T�v��~ͭ���Z�*����f�fRM@wy3҂����[��~�D]C>8|J����
����2O��\ xl�bWQ�I��N�5���l�0�,>*�Z�J���-��������N���tȯ^M�Θ�.��A��#�k��u.;^5-�d�Bx/uS��\�T��ؽ�B�fA�v�E �@3�����xzx6���;�N�Ln��U�I{f�% �Q}��hA#�,ID���f���`+�.r���(^�׬z�,i�	�>ce�K!�SYc
���)�t�C��Pd�l�	�l� d%�Z�c�<,oM;Xr,մ2��"���YP����������{��!o��ph��f�4y�M^b�$�4	�3�9�w�b݊4���W�-n�^��iB�oMV�0s�V&��c܊=i�JP��aK#^���ܧ�����fB�������{o�~ػ�{�S>�E���-�h���i1��ݗ���K��l��(��T%���8��8�(�a�I@3�i(Y�%K,b OV]��c�+B���1˦+�� ���"�Yy�cZ��ׂ���̣��� z6��#�Y$�N롟vU��۴��>���4m�!�����c��(��P't쑏����83�O�i��Q6�	@&�91+,TÛ^<V!�
(�����/��p��F�IJ�Ւ��p�u��Y�л��~4�!	b7Q�	=ک���{o�J�9d�mx% ��ɰ�@	tW�.s�����4�G�L�cd�`��1|dmf�;� *b�yQқ��s��(����NxiV��Nc�2q��p]>���nx���Ԁ��]�C���b,j��׏;�'Zq׬�W�{��g���?������Tíe/���(��b��J���V\�;�G���qx��҉;jk��tm*Wҁ��$|]''p"8�����g޽�����>������?q��7mR:�3e�\fN�yL{G}փ���R�n�h�IT�?q�T��4k��A���&�?��O�/^9�      �   8   x�3�t,*�S0��2�tN�KN�ILI�2�(J-K���9��srRK�b���� _��      �   S  x��\�r�H�����U0�8H��H��nɲ֒U
����)��PY���9t��٤|���n D� E��h<
D��������gy'��,�Gl"��g�ALߦA"�c��LDl&�Ob6_�q~l$�⛟�q��dN��솹]���b�29�[5C��<��_i�Y�1�����_mr6`�2HM&�a�C.}`E�D}�]�eX��CF+��3v$n�f=Ib_L�Y I:�̂�H�YpK�K�X���|F�"e�_�`����&ᾒ�x<6����s�._���4W9����5:����r�/���k�H�Q	��/���A��ᆽ�#?����J�'��ҍ�ք߳�U��9�l�6rq>75=�2�q"�r�J�ꭂ
�-��P4��7�W�>���1#��Y��vĳŗ0�J�
c���Hy��a(���~0��3����h�|��H� �Ҧ_?�FI�F8����e�}��7AJδ%�`r ~���?	y��炥�%�*Nf��'�T<�8�I�S�n��8
1�D�� �Ic"�f�2���S�&f�v8��I��'%C�r������Q+y�V���@3�$ s���B�xf��:��6Lb:ф&�Z�����dl������y�;恀����rv��r1{L�NDB�BV�KO�*�c)��hJ�_(9��AnX����<蒖'u��o}���L>^ف�GRGܑ
k:�����c�hX��3~�s�a�F�ݙ���g%_�Y�:K~��4y�U9x+^������˰ֹWP\};%��K�xkTԂY�>�I�5ؔ.���C;	��Z��` m��V��6�-v����z�h�R�+y���wn�
��-�av�T���@���V�S!߃(k�Eټu�|u��������T]�SjN�7�ք:jLЗ�PW��Z|۹i�����N�y��o.�[����4���9O�l��?)��n�4dZkm��im6h����K~z��}���|_�C�ƟTh>�}=W�?E��lK]p���>Q��Y��,�7F�ų�Dȧ� �Iy*HlL*3�e��+B�A-��˖f��R�S�g�~��
	X�ղXӔ8%Vhe	V>Te���=U�k3����s�g�?�۩�ۡ�eզ����$�B��b�Cɍ�3��F�<�y�m.������.�� ��3N` AZ ����Ƃ�o,$tI�i�y���e��x�������m�{(�0`a�ꑜ����S��������L��n�V ǎ(���3�\'	/F��;�7}�p�����^�������EqZ�]>�L_��*4�?��E䶮��,��u��\P����c����냷��g��??�_���x��t��������5�	GZ���6v1��F +�4��5��,ϰ\wؘ��:�UI�K�2>�����
�ē !�,9sb���?Wa��*�۱N�j֔���^{qq��pq4�B����� �o�i(�����Rv0�lR��k��]ب��ޣ� ������/}P���n�ohR��1�ZDVLc�,�ʨBT������*H���d%�m�T��t���nG�o{�Kv�oC� � ���j�$WVU�<�#�E'��J��D:ʫ6�� �ZX���K��X�H� �`����`�%�ŗH嶝S��x�� /Z��>I����Ρ⪈��ܞm�5'��~1�<�3	_f"�"�iC��Z�T��Ӝ�`Yӛ�a)˜K�q�ԆH��dJ�3(!dp �g�|JE1�Ѝ�6�1iJ�H{J��#���
���1�h��2.�P�8z�:
BE���k� �ׄ�Q%Ъ�ݬ1o�Ҙ3qD@W�98aՎM��H�]Ҽe��m��2r�ܡ�H�M2v�,����@e��)���Ŧ[��M�5(6���З}�CF3��b�M�s?Ɵ��x*&UN�@�Rm�e[۱Df���9 ���/�0��X�#k�<M���[��Z4k9#�-ɔI���v�m�쑇T{��Z�n�����2��x�a��P�>k�V����`��~I�X:��`�Yq��j�����Fq���w��5vx�bv����9�1�G�*���Þ���awh`zzWN����c��VZZ��߇�Gk�ӗLCo5E����Mذ� �wiR��Q��ۤ*��\x�@�j���uVNu^T�L�b�笏F���&H
��e�<ڳ�v郲 �bօ��pY^���l�:�#�O�;q�L����� av�4;f�R��Yx
�eW�n��h�l�h���2�u���Nˌ�JHӻW�LW�I�O��Bx��oy\�˲,y�Y0��+��Ltɋ�`h��&)'�a
_�җ@�h9U��p[�n٠���2��â��9�5�~�[����[�s��K��Z���ʗ������d�u�����F��9f�j��p{*�y"}���Қ�J�ĵ�����4�u�<��9��g9�%7
ߩֹ�1�����$bCd��ii��ٝN=��l"�;��8�ߺ=u��{ ����U_y[P^�-6)��j��9Z�}k½���X�OC���OF�)��uSr�A��æ�Á���\���=B"+Q6\B�e�l��U�Y��C��3��
1WL����h݂%�'6 ���P ~16D b|H�by�/:[|��Y LF$�$��I�2.�WB��)��坊'�x2�����'��ݮ���_��Ss����.�3ԩ���"rU`[�l򈾂E��rO��3���C硠�`��l$ɵ^���/Y������
�:'��Ҏ73U�[[xe�ji���u�:k�b�냆��WV�`]6ܳ����(o���R��%ȅ�8ɔ����~�qB���Q1�}	��[U�0�P\#��'aEDٌ�aT$Ǉl�����s�K�$�[(c��3�:ew}��Q�JNy�V�r�Foe"��r�W?��~>S	�� 
`�?���I�ʕ^�PL���w.�%�����$��.�A��hP���]oS��p+�*N�v�v8d�X|�*P�ZYDME�(�p������������I���-�/Q��jT��(����>��j�L�	�t@�p���X���u� H���q��s{`Z����S/�۹��t�zĖ��?�B
�{x�	-�ݖz��.j�u�c�t�[���·V�Б�8��{6x>D���מ���u���Ҷ9�x^;�[�TLU0�@�x	T)���U�P��f4O�j��͠U#���`������h��DԎVL���K�^�yf�rW��F��)z:�C����]|�������3�]X�>��\n��2��x�c�4�=Ԗ�Dx+�=ėY+�$Qg���I����;�}��㷜����U>v��������      �   �  x��WY�#9�Nf�B�]���	"�t�z���%@hI�^�e���q��^�d���|ԘXw�Q;�a��lP�U�U����l̀�0�B�嘚��hN�'>�&Jipa94��!v�8h�Գ�S(,Қ39L�r2w'��՝Oġ�XB���X��ə'�M�P)t1�� �rz�qh_<�c�r�Q���(lF��y'9"b9�
�r���qB�.�����c�TG�+��W]�\r�Q���	��QJY7Rx��Z��3j�'��N�(�O`��Za�T`C�an���v� R~a��y���"o�SKs����-�^� ������y�=�]1F_}�(�K��":�c菡?���c��Ad���u̟V(�**ST�򞩍7f��h{dD��o3���Zt�g|26��)5�����?�]m��y���_g3���"]�Ӆ��mݣ�f�������p��w�+p�e\�5,�i�G7�c�n?�Ҁv����;h`��;�L�r�ږ��eau�<h���H�x���?�U8ߟv��O.�N���sA�����R��P
Ȱ7�6+����8sb�Ga�N=I��}u�#���'G���7��X�v��W]V�Mp�7 Y���0�7x�@aT(�@P��``X�.��A���bh�e!�b�kѓ��V��c�%Ӑ���EHC�A�je�m�D�)����K����ǈ}a��L��Lܚ�Uh�r�2kM��;3؍�[�v�4
�~����<��p%��.Wn�*�E��{�@���B�Y�S�܋��2W��ne���_u}��܍>%�^~U��������h��+�@�g.y�����wcr,ו�?��;�BN��̟�������g?�,{}4�蜽�W�?��֣C�vJϜy-��h�C����[%��̥�+�ӢYi>-���/1;�������ij&?��k,��n�����9a���|�sm���2j,�����B����ڳ䚚gC�e����՟��~$ym��l��Gt\�|i�^F���0:O֤��(���?�<�r��¸�=��B�b���V.+M��M�P٢|�����;&N�ǣ�5������ǳ����_��[yF/�G�;:�c���(@���s�!�Q�8��x}���O�;���o�����M����{�w�?���O
B/!����  �A�����!��U[�A�����������z�65�      �   �  x����R�0�ϛ��0�-����:ɭ��6��@b��t�V}��XWk��I�b��V������L$p�ۖ�5
��3
,����*/�����j]�A�$I�X�	� H�n�\��{��_�f7��:�]��r0�*]��k���[K����'��#�Bd��K�HB�Q	� ��^�s��{IY��J�H�L����N��aOӡ�
�������n�YJ"<@�)�K]A���P�a@�|/��̈?��\0���0���0�FIƌwh�Ob����|g������Ks�2������.pf~�{|(Td��ݣ���|�W[�@3Y�'[�����:އJEt1����� �G�_2p�AЅ��p�fCޕQ�x��P;k��r��2�-�_|�gs���W��0�A�0H( 5��[�G�(�_L��bK(h�{�4� 5XMM��X��6� j��)6���O��7������>tC� �S�E��,��-�i�r�]��m���iXq�(m���Fra	�;�l��z�]����_^�n]�L_���$)��({Oz����]����h;N���Ɲ�ux*�H�����n=B��@;���It:�t�ɢ��O��~KIN���:8_uwӈ����W���/�#1u      �   �  x���Mn�G���)t������=Bw��p$@r6�U�Ћ�Cʆ�lԚ��z@q��/)�x��y���c9�w.����yw<}�����yw=^_���������p��Η���e���}��d��n`Mu���.�����n���c�|��|�u�ٔTm�>�[�m0r��q����^�����X���6�,�%������t�6��E�K��qɆ���ђ5>�lԛ"��`٧�/�BS�!Y���t��(��L�x��O��e�|=�O?���͆��G᡿�軸^�o���/��%/5lDj=���q\��+�w`"�fox�A�:��ܺ6v�(J���x:�O/�������;s2v�]5xE�Y��@Z�+�IW`
�����+ϸ���2��j"��i����3E
[tGύ���@�F���y���&�h0%�iCk0I *���>6�M�q�9#�\��E��I�&j�M.�&ªro��$�"K��*lD����X���룫F��)u��[���T
��q��ak��/�"W�,��l�(�G��{���G�\Ad�k�RP�	��4S��wF��.��J�t�6��e`��b��C7����mB�Ā!����@3Ңq���Y�Rs��2�DIK�^8�«=�`K�$U�+G���o�f4��T�W�1)e�i��J����bnE�h�������MAs�h��J������ �>�pF�_ft�|J��Voti��.�|�J�Aǹqos�{�@�N��ҧ��r�U�؊ݽ��,�B�zc�5��4�Z�O�@�}�O��y�P�Nw�
�|(�x�Q�O[�ۗ��h#�>
�{k��R�A�̨��Aw���O����L�T纶в#�#&ƨ���i�f���:31UJ8����Y1��Rj�y�Xe�UJ��Fֆ�|�*���0� �2��·�V%ՠ�?Kc��WI5�ȫdD��bURM��F�1%�J��%ͷQ���V�T��>n�sz�3�B���Y��mi�^+�jZ�Ɔ{���P�N�ef<t��n�J]��e�g����B��x�[P��#1�P�c=jw-J�6�:��6�"�P�s-�����=��:�:�G܂YB��H��cو+�����)�*���'����QcI��J���A�z�ER}��
)4Q���3�L2���z�I!�]��7:�g{�{\t�E��m#nω/�h��#y[G��v��H�GO�U��W�q����vp j��v͂^%SX��M�?-����J�qU�/��+�LM|�P�6*���Ny���=>>����      �   Q   x��K
�0�uz��N�&� Z�Mr�ˁ�d����PN3�w4X�;W�~X�6�a��Q�'`���dz*��6|L)�4o�      �      x�35�4D@.Se��B�@�+F��� h��      �   b  x����n�0���S����_��I�v+uaڽ�!�O�&�j�~�Z6��J����|�����K.Ox�uÓ9�T�mCOz"{BF!<�����H�=�GB�O'SY��E޴",(c'
�
�G��^+�i�6�V���\`l����}X�nu�6U��-1�'�Z&�]�������|�4(,�s'
}"�����O:yX�g+sl��'
�x{p��.Ze��KL�OZt����IGh�������svD�f݈لiR�� 
h+� .����H����s���)/���W�ҥa� p��R�@��3$����C�b~n�aa� r�(>�����+��A�dIʳ�H^[u��Pn�A"�(�u&u�5	����a�쀤�(��� ���u^? �%-(ȯ��FX�\�',����铣����d����X��د���mj�|g�H2^K�t-��Rә$!����ٔ@c�Yу�x������+�%h��f���zs�����k�Rь�$�:��V�jc�R�������K�,1��B�j�>����4�*��KŶծ:j�Q�u�١nz]���3��`�̨�::��즿�P���_�2�_�o��i��1�՚�R�GU�b�������w���~�.��*\�>�B�fX����R;	�!Xr/|GQ!����	���{FB���rD��ri��GeM[ߕnJ���)I|�l�ru����	�ݛ;I�>���=��n6����Z}�}��yOpBVZէ��-�o�͇�2 ]!�P�{.y�,���b�>>LөY�j��|�K �?�+��a�Ň��`��N�x�=o�>��R���o����(J�      �      x������ � �      �   1   x�3��/H-JL�/�2�tL����,.���9�R����rR�b���� b��      �   Q   x�m���0��.T`HEw��s�O��������R4�m���-�%��tQ�y�eQ�16]��4]׏���.~�����!      �   �   x�e�[��0E��U��T&�����!Ӻj��ؑ����<��!q9�:��5��S�NM�M��B>��Ü+�dQ���j۝VX��Z��)�L��F �\��@��*�ߢ��{qᬛ��%,�W���;G��j%s}��sIk�s�S���?.�C��\|�$��ʏ������|����I3����σ��	��g{      �      x������ � �      �   w   x���A� ��5��H�B��Y�@E3g���	\��%���+]�Ϡi�_��[O��l4(3Ȍ<�Dϑщ,�����Ю�B֌Y��0-@GѴ����R���+�iu D���欵7�1�      �   �   x�UPKj�@]�O��u��2�E�H�
��ٌ�ј���wʢ��X���xz�j��0QH����!���g�L[K���j��M>�ãۨ��#t�A��ᄝѨ��F��H�r-�F-�#����=:c
�V���#��,����!SklXϧ�������=G%qB[�.io���K�ˣz�V8�^z�3C@NET_��a��"}���>�i�GIǂ��81D����yiס-��XUU��yx1     