PGDMP     3    4                v           prueba    10.4    10.4 �    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    16393    prueba    DATABASE     �   CREATE DATABASE prueba WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Mexico.1252' LC_CTYPE = 'Spanish_Mexico.1252';
    DROP DATABASE prueba;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16402    users    TABLE     {  CREATE TABLE public.users (
    id integer NOT NULL,
    user_name character varying(255) NOT NULL,
    employee_number integer NOT NULL,
    description character varying(255) NOT NULL,
    rol integer NOT NULL,
    user_register integer NOT NULL,
    date_register timestamp with time zone NOT NULL,
    status integer NOT NULL,
    password character varying(300) NOT NULL
);
    DROP TABLE public.users;
       public         postgres    false    3            �            1255    25007 /   user_auth(character varying, character varying)    FUNCTION     	  CREATE FUNCTION public.user_auth(_user_name character varying, _password character varying) RETURNS SETOF public.users
    LANGUAGE sql
    AS $$ 
		SELECT
			* 
		FROM
			public."users" 
		WHERE
			user_name = _user_name 
			AND password = _password;
	$$;
 [   DROP FUNCTION public.user_auth(_user_name character varying, _password character varying);
       public       postgres    false    197    3            	           1255    33511 /   vud_activate_request(integer, integer, integer)    FUNCTION       CREATE FUNCTION public.vud_activate_request(_id_request integer, _status integer, _last_status integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE 
	public.request
SET 
	id_status = _last_status,
	last_status = _status
WHERE 
	id = _id_request;
END
$$;
 g   DROP FUNCTION public.vud_activate_request(_id_request integer, _status integer, _last_status integer);
       public       postgres    false    1    3            �            1259    33388    cat_area    TABLE     k   CREATE TABLE public.cat_area (
    id integer NOT NULL,
    description character varying(100) NOT NULL
);
    DROP TABLE public.cat_area;
       public         postgres    false    3            �            1255    33447    vud_area_record()    FUNCTION     �   CREATE FUNCTION public.vud_area_record() RETURNS SETOF public.cat_area
    LANGUAGE plpgsql
    AS $$
BEGIN

    RETURN QUERY SELECT * from cat_area order by id asc;

END
$$;
 (   DROP FUNCTION public.vud_area_record();
       public       postgres    false    225    3    1            �            1255    33460 J   vud_create_dictum_request(date, character varying, integer, date, integer)    FUNCTION     5  CREATE FUNCTION public.vud_create_dictum_request(_date_receives_dictum date, _folio_dictum character varying, _answer_dictum integer, _citizen_dictum date, _id_request integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
INSERT INTO public.tracing_dictum(
			date_receives_dictum, 
			folio_dictum, 
			answer_dictum, 
			citizen_dictum, 
			id_request)
VALUES (_date_receives_dictum,
		_folio_dictum,
		_answer_dictum,
		_citizen_dictum,
		_id_request);


UPDATE public.request
	
SET 
	id_status = 7
WHERE 
	id = _id_request;

END
$$;
 �   DROP FUNCTION public.vud_create_dictum_request(_date_receives_dictum date, _folio_dictum character varying, _answer_dictum integer, _citizen_dictum date, _id_request integer);
       public       postgres    false    1    3            �            1255    33537 _   vud_create_modality(integer, character varying, character varying, integer, integer, character)    FUNCTION     �  CREATE FUNCTION public.vud_create_modality(OUT code_modality integer, _code integer, _description character varying, _legal_foundation character varying, _id_transact integer, _days integer, _type_days character) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
DECLARE code_inserted_modality integer;
BEGIN 
	INSERT INTO public.cat_modality(
	code, 
	description, 
	legal_foundation, 
	id_transact,
	status,
	days,
	type_days)
	VALUES (_code, 
			_description, 
			_legal_foundation, 
			_id_transact,
			1,
			_days,
			_type_days)
	RETURNING ID into code_inserted_modality;

	RETURN QUERY SELECT code from public.cat_modality where id = code_inserted_modality;
END 
$$;
 �   DROP FUNCTION public.vud_create_modality(OUT code_modality integer, _code integer, _description character varying, _legal_foundation character varying, _id_transact integer, _days integer, _type_days character);
       public       postgres    false    3    1                       1255    33576 :   vud_create_request_requeriment(character varying, integer)    FUNCTION     �  CREATE FUNCTION public.vud_create_request_requeriment(_folio character varying, _id_requeriment integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE _id_request integer;
BEGIN


_id_request = (SELECT 
	id 
from 
	public.request as a
where 
	a.folio = _folio);

INSERT INTO public.request_requirement(
	id_request, 
	id_requirement)
VALUES (_id_request,_id_requeriment);

END
$$;
 h   DROP FUNCTION public.vud_create_request_requeriment(_folio character varying, _id_requeriment integer);
       public       postgres    false    3    1            �            1255    16735 )   vud_create_requirement(character varying)    FUNCTION     �   CREATE FUNCTION public.vud_create_requirement(_description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO public.cat_requirement(description,status)
	VALUES (_description,1);
END
$$;
 M   DROP FUNCTION public.vud_create_requirement(_description character varying);
       public       postgres    false    3    1                       1255    33519 7   vud_create_settlement(integer, date, character varying)    FUNCTION     Q  CREATE FUNCTION public.vud_create_settlement(_id_request integer, _settlement_date date, _settlement_number character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE public.request
SET settlement_date= _settlement_date,
	settlement_number= _settlement_number,
	id_status = 6
WHERE id = _id_request;

END
$$;
 ~   DROP FUNCTION public.vud_create_settlement(_id_request integer, _settlement_date date, _settlement_number character varying);
       public       postgres    false    3    1            �            1255    33525 K   vud_create_turn_request(integer, date, integer, character varying, integer)    FUNCTION     �  CREATE FUNCTION public.vud_create_turn_request(_id_request integer, _date_turn date, _review_area integer, _observations character varying, _id_status integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
INSERT INTO public.tracing_turn(
	id_request, 
	date_turn, 
	review_area, 
	observations)
VALUES (_id_request,
		_date_turn,
		_review_area,
		_observations);

UPDATE 
	public.request
SET 
	id_status = 5,
	last_status = _id_status
WHERE 
	id = _id_request;

END
$$;
 �   DROP FUNCTION public.vud_create_turn_request(_id_request integer, _date_turn date, _review_area integer, _observations character varying, _id_status integer);
       public       postgres    false    3    1            �            1259    25106    vulnerable_group    TABLE     �   CREATE TABLE public.vulnerable_group (
    id integer NOT NULL,
    description character varying(255) NOT NULL,
    status smallint NOT NULL
);
 $   DROP TABLE public.vulnerable_group;
       public         postgres    false    3                       1255    33306    vud_data_vulnerable_group()    FUNCTION     �   CREATE FUNCTION public.vud_data_vulnerable_group() RETURNS SETOF public.vulnerable_group
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY SELECT * FROM vulnerable_group where status = 1 order by id asc;

END
$$;
 2   DROP FUNCTION public.vud_data_vulnerable_group();
       public       postgres    false    223    3    1                       1255    16755    vud_delete_modality(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_modality(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE public.cat_modality
	SET   status = 0
	WHERE id = _id;

END
$$;
 7   DROP FUNCTION public.vud_delete_modality(_id integer);
       public       postgres    false    3    1            �            1255    16737    vud_delete_requirement(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_requirement(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	UPDATE public.cat_requirement
	SET  status = 0
	WHERE id = _id;
END
$$;
 :   DROP FUNCTION public.vud_delete_requirement(_id integer);
       public       postgres    false    1    3            �            1255    16710    vud_delete_transact(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_transact(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
	UPDATE public.cat_transact
	SET status= 0
	WHERE id = _id;
END
$$;
 7   DROP FUNCTION public.vud_delete_transact(_id integer);
       public       postgres    false    3    1                       1255    25027    vud_delete_user(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_user(_id_user integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$

BEGIN

UPDATE public."users" SET status = 0 WHERE "id" = _id_user;
return true;

END
$$;
 8   DROP FUNCTION public.vud_delete_user(_id_user integer);
       public       postgres    false    3    1                       1255    16754 (   vud_eliminate_relation_modality(integer)    FUNCTION     �   CREATE FUNCTION public.vud_eliminate_relation_modality(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
DELETE FROM cat_modality_requirement WHERE id_modality = _id;
END
$$;
 C   DROP FUNCTION public.vud_eliminate_relation_modality(_id integer);
       public       postgres    false    1    3            �            1255    16774 (   vud_eliminate_relation_transact(integer)    FUNCTION     �   CREATE FUNCTION public.vud_eliminate_relation_transact(_id_transact integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	DELETE FROM cat_transact_requirement where id_transact = _id_transact;
END
$$;
 L   DROP FUNCTION public.vud_eliminate_relation_transact(_id_transact integer);
       public       postgres    false    1    3            �            1255    33550    vud_get_last_folio()    FUNCTION     �   CREATE FUNCTION public.vud_get_last_folio() RETURNS SETOF character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
	
	RETURN QUERY select folio from request  order by id desc limit 1;

END
$$;
 +   DROP FUNCTION public.vud_get_last_folio();
       public       postgres    false    1    3            �            1255    33534 V   vud_insert_transact(integer, character varying, character varying, integer, character)    FUNCTION     �  CREATE FUNCTION public.vud_insert_transact(OUT _id_code integer, _code integer, _description character varying, _legal_foundation character varying, _days_transact integer, _type_days_transact character) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
DECLARE _id_transact integer;
BEGIN

	INSERT INTO public.cat_transact(
		code, 
		description, 
		legal_foundation, 
		status,
		days,
		type_days)
	VALUES (_code,
			_description,
			_legal_foundation,
			1,
			_days_transact,
			_type_days_transact)
	
	RETURNING ID into _id_transact;

	RETURN QUERY SELECT code from public.cat_transact where id = _id_transact;

END
$$;
 �   DROP FUNCTION public.vud_insert_transact(OUT _id_code integer, _code integer, _description character varying, _legal_foundation character varying, _days_transact integer, _type_days_transact character);
       public       postgres    false    1    3                       1255    25018 c   vud_insert_user(character varying, integer, character varying, integer, integer, character varying)    FUNCTION       CREATE FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying) RETURNS void
    LANGUAGE sql
    AS $$ 
	
		INSERT INTO 
	public."users"
		("user_name",
		"employee_number",
		"description",
		"rol", 
		"user_register",
		"date_register",
		"status",
		"password") 
	VALUES (_user_name,
		_employee_number,
		_description,
		_rol,
		_user_register,
		'now()',
		1,
		_password);
		
	$$;
 �   DROP FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying);
       public       postgres    false    3                       1255    25125 P   vud_insert_user_permission(integer, integer, integer, integer, integer, integer)    FUNCTION       CREATE FUNCTION public.vud_insert_user_permission(_id_user integer, _id_module integer, _read_permission integer, _write_permission integer, _delete_permission integer, _update_permission integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

INSERT INTO public.permission(
	id_user,
	id_module, 
	read_permission, 
	write_permission, 
	delete_permission, 
	update_permission)
VALUES (
	_id_user, 
	_id_module, 
	_read_permission, 
	_write_permission, 
	_delete_permission, 
	_update_permission);

END
$$;
 �   DROP FUNCTION public.vud_insert_user_permission(_id_user integer, _id_module integer, _read_permission integer, _write_permission integer, _delete_permission integer, _update_permission integer);
       public       postgres    false    3    1            �            1255    25005 *   vud_log_insert(integer, character varying)    FUNCTION     �   CREATE FUNCTION public.vud_log_insert(_id_user integer, _description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO 
	public."log"(id_user, description,date_time)
	VALUES (_id_user,_description ,NOW());
END
$$;
 W   DROP FUNCTION public.vud_log_insert(_id_user integer, _description character varying);
       public       postgres    false    3    1                       1255    25103    vud_modality_data(integer)    FUNCTION        CREATE FUNCTION public.vud_modality_data(_code_modality integer, OUT id integer, OUT description character varying, OUT legal_foundation character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN 
	RETURN QUERY select 
		a.id,
		a.description,
		c.legal_foundation

	from 
		public.cat_requirement as a 
	LEFT JOIN
		public.cat_modality_requirement as b
	on 
		a.id = b.id_requirement
	LEFT join 
		public.cat_modality as c
	on
		b.id_modality = c.code
	where c.code = _code_modality ;
END
$$;
 �   DROP FUNCTION public.vud_modality_data(_code_modality integer, OUT id integer, OUT description character varying, OUT legal_foundation character varying);
       public       postgres    false    1    3                       1255    33385    vud_modality_days(integer)    FUNCTION       CREATE FUNCTION public.vud_modality_days(_code integer, OUT days smallint, OUT type_days character) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY SELECT 
				a.days,
				a.type_days
			FROM 
				cat_modality as a
			where
				a.code = _code;

END
$$;
 c   DROP FUNCTION public.vud_modality_days(_code integer, OUT days smallint, OUT type_days character);
       public       postgres    false    3    1                       1255    33549    vud_modality_record()    FUNCTION     x  CREATE FUNCTION public.vud_modality_record(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT id_transact integer, OUT code_transact integer, OUT description_transact character varying, OUT days integer, OUT type_days character) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY select 
	a.id,
	a.code,
	a.description,
	a.legal_foundation,
	b.id,
	b.code,
	b.description,
	a.days,
	a.type_days
from 
	cat_modality as a
LEFT JOIN 
	cat_transact as b
on
	a.id_transact = b.code
WHERE 
	a.status = 1
order by a.id asc;

END
$$;
 "  DROP FUNCTION public.vud_modality_record(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT id_transact integer, OUT code_transact integer, OUT description_transact character varying, OUT days integer, OUT type_days character);
       public       postgres    false    3    1            �            1259    16726    cat_requirement    TABLE     �   CREATE TABLE public.cat_requirement (
    id integer NOT NULL,
    description character varying(5000) NOT NULL,
    status integer
);
 #   DROP TABLE public.cat_requirement;
       public         postgres    false    3                       1255    16734    vud_record_requirement()    FUNCTION     �   CREATE FUNCTION public.vud_record_requirement() RETURNS SETOF public.cat_requirement
    LANGUAGE plpgsql
    AS $$
BEGIN 
	RETURN QUERY select * from public.cat_requirement where status = 1 order by id asc;
END
$$;
 /   DROP FUNCTION public.vud_record_requirement();
       public       postgres    false    205    1    3                       1255    33535    vud_record_transact()    FUNCTION     �  CREATE FUNCTION public.vud_record_transact(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT total_modality bigint, OUT id_transact integer, OUT days integer, OUT type_days character) RETURNS SETOF record
    LANGUAGE sql
    AS $$

	SELECT
		a.id,
		a.code,
		a.description,
		a.legal_foundation,
		count(b.id_transact) as total_modality,
		b.id_transact,
		a.days,	
		a.type_days
	FROM
		cat_transact  as a
	left JOIN 
		cat_modality as b
	on  a.code = b.id_transact
	WHERE
		a.status = 1
	GROUP BY b.id_transact,a.id,a.code,a.description,a.legal_foundation,a.days,a.type_days
	order by a.id asc;

$$;
 �   DROP FUNCTION public.vud_record_transact(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT total_modality bigint, OUT id_transact integer, OUT days integer, OUT type_days character);
       public       postgres    false    3                       1255    16752 3   vud_relation_modality_requirement(integer, integer)    FUNCTION       CREATE FUNCTION public.vud_relation_modality_requirement(_id_modality integer, _id_requirement integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

INSERT INTO public.cat_modality_requirement(id_modality,id_requirement)
VALUES (_id_modality, _id_requirement);

END
$$;
 g   DROP FUNCTION public.vud_relation_modality_requirement(_id_modality integer, _id_requirement integer);
       public       postgres    false    3    1                       1255    16765 6   vud_relation_transaction_requirement(integer, integer)    FUNCTION       CREATE FUNCTION public.vud_relation_transaction_requirement(_id_transact integer, _id_requirement integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

INSERT INTO public.cat_transact_requirement(
	id_transact, 
	id_requirement)
VALUES (_id_transact,_id_requirement);

END
$$;
 j   DROP FUNCTION public.vud_relation_transaction_requirement(_id_transact integer, _id_requirement integer);
       public       postgres    false    1    3                       1255    33561 �   vud_request_address(character varying, character varying, character varying, character varying, character varying, character varying)    FUNCTION     x  CREATE FUNCTION public.vud_request_address(_postal_code character varying, _colony character varying, _ext_number character varying, _int_number character varying, _street character varying, _delegation character varying) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
DECLARE _id_request_address integer;
BEGIN

INSERT INTO public.address(
		postal_code, 
		colony, 
		ext_number, 
		int_number, 
		street,
		delegation)
	
VALUES (_postal_code,
		_colony,
		_ext_number,
		_int_number,
		_street,
		_delegation)

RETURNING ID into _id_request_address;
RETURN QUERY SELECT _id_request_address;

END
$$;
 �   DROP FUNCTION public.vud_request_address(_postal_code character varying, _colony character varying, _ext_number character varying, _int_number character varying, _street character varying, _delegation character varying);
       public       postgres    false    3    1                       1255    33553 �   vud_request_data(integer, character varying, character varying, integer, integer, integer, integer, integer, character varying, character varying, integer, integer, date, integer)    FUNCTION     �  CREATE FUNCTION public.vud_request_data(_id_user integer, _folio character varying, _observations character varying, _id_status integer, _id_address integer, _id_interested integer, _id_transact integer, _id_vulnerable_group integer, _request_turn character varying, _type_work character varying, _means_request integer, _delivery integer, _date_commitment date, _id_modality integer) RETURNS SETOF character varying
    LANGUAGE plpgsql
    AS $$
DECLARE id_request_created integer;
BEGIN

INSERT INTO public.request( id_user,
							folio,
							date_creation, 
							observations, 
							id_status, 
							id_address, 
							id_interested, 
							id_transact, 
							id_vulnerable_group, 
							request_turn, 
							type_work, 
							means_request, 
							delivery,
							date_commitment,
							flag,
							id_modality)
VALUES (_id_user,
		_folio, 
		NOW(), 
		_observations, 
		_id_status, 
		_id_address, 
		_id_interested, 
		_id_transact, 
		_id_vulnerable_group, 
		_request_turn, 
		_type_work, 
		_means_request, 
		_delivery,
		_date_commitment,
		1,
		_id_modality)
RETURNING ID into id_request_created;

	RETURN QUERY select folio from public.request where id = id_request_created;

END
$$;
 �  DROP FUNCTION public.vud_request_data(_id_user integer, _folio character varying, _observations character varying, _id_status integer, _id_address integer, _id_interested integer, _id_transact integer, _id_vulnerable_group integer, _request_turn character varying, _type_work character varying, _means_request integer, _delivery integer, _date_commitment date, _id_modality integer);
       public       postgres    false    3    1            �            1255    33328 k   vud_request_interested(character varying, character varying, character varying, character varying, integer)    FUNCTION     !  CREATE FUNCTION public.vud_request_interested(_name character varying, _last_name character varying, _e_mail character varying, _phone character varying, _type_person integer) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
DECLARE id_request_interested integer;
BEGIN
INSERT INTO public.interested(
	name, 
	last_name, 
	e_mail, 
	phone,
	type_person)
	VALUES (_name, 
			_last_name, 
			_e_mail, 
			_phone,
			_type_person)

RETURNING ID into id_request_interested;
RETURN QUERY SELECT id_request_interested;

END
$$;
 �   DROP FUNCTION public.vud_request_interested(_name character varying, _last_name character varying, _e_mail character varying, _phone character varying, _type_person integer);
       public       postgres    false    1    3            !           1255    33566    vud_request_record()    FUNCTION     �  CREATE FUNCTION public.vud_request_record(OUT id integer, OUT folio character varying, OUT id_user integer, OUT id_status integer, OUT description_status character varying, OUT date_creation text, OUT date_commitment text, OUT code integer, OUT description character varying, OUT modality_desc character varying, OUT id_turn integer, OUT id_dictum integer, OUT last_status integer, OUT settlement_number character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY SELECT 
	a.id,
	a.folio,
	a.id_user,
	d.id as id_status,
	d.description as description_status,
	to_char(a.date_creation,'dd-mm-yyyy' ),
	to_char(a.date_commitment,'dd-mm-yyyy' ),
	b.code,
	b.description,
	c.description as modality_desc,
	e.id as id_turn,
	f.id as id_dictum,
	a.last_status,
	a.settlement_number
FROM 
	public.request as a
left join 
	public.cat_transact as b
on
	a.id_transact = b.code
left join 
	public.cat_modality as c
on 
	a.id_modality = c.code

left join 
	public.cat_status as d
on
	d.id = a.id_status
left join 
	public.tracing_turn as e
on
	e.id_request = a.id
left join
	public.tracing_dictum as f
on
	f.id_request = a.id
WHERE
	flag = 1
order by id desc;

END
$$;
 �  DROP FUNCTION public.vud_request_record(OUT id integer, OUT folio character varying, OUT id_user integer, OUT id_status integer, OUT description_status character varying, OUT date_creation text, OUT date_commitment text, OUT code integer, OUT description character varying, OUT modality_desc character varying, OUT id_turn integer, OUT id_dictum integer, OUT last_status integer, OUT settlement_number character varying);
       public       postgres    false    1    3            �            1259    16413    role    TABLE     g   CREATE TABLE public.role (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);
    DROP TABLE public.role;
       public         postgres    false    3                       1255    25015 
   vud_role()    FUNCTION     �   CREATE FUNCTION public.vud_role() RETURNS SETOF public.role
    LANGUAGE sql
    AS $$ 
		SELECT
			*
		FROM
			public. "role";
	$$;
 !   DROP FUNCTION public.vud_role();
       public       postgres    false    199    3            �            1259    25079 
   cat_status    TABLE     m   CREATE TABLE public.cat_status (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);
    DROP TABLE public.cat_status;
       public         postgres    false    3            $           1255    33428    vud_status_list()    FUNCTION     �   CREATE FUNCTION public.vud_status_list() RETURNS SETOF public.cat_status
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY select 
				* 
			from 
				public.cat_status;
END
$$;
 (   DROP FUNCTION public.vud_status_list();
       public       postgres    false    217    1    3                       1255    33347    vud_transact_days(integer)    FUNCTION     "  CREATE FUNCTION public.vud_transact_days(_code integer, OUT days integer, OUT type_days character) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY SELECT 
				a.days,
				a.type_days
			FROM 
				cat_transact as a
			where
				a.code = _code;
				
END
$$;
 b   DROP FUNCTION public.vud_transact_days(_code integer, OUT days integer, OUT type_days character);
       public       postgres    false    3    1                       1255    33332    vud_transact_record()    FUNCTION     *  CREATE FUNCTION public.vud_transact_record(OUT code integer, OUT description character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY SELECT a.code,
						a.description
				FROM public.cat_transact as a 
				WHERE status = 1 
				order by id desc;
END
$$;
 _   DROP FUNCTION public.vud_transact_record(OUT code integer, OUT description character varying);
       public       postgres    false    3    1            �            1259    16694    cat_modality    TABLE     &  CREATE TABLE public.cat_modality (
    id integer NOT NULL,
    code integer NOT NULL,
    description character varying(2000) NOT NULL,
    legal_foundation character varying(3000),
    id_transact integer NOT NULL,
    status integer NOT NULL,
    days integer,
    type_days character(1)
);
     DROP TABLE public.cat_modality;
       public         postgres    false    3            #           1255    16821 %   vud_transact_record_modality(integer)    FUNCTION     �   CREATE FUNCTION public.vud_transact_record_modality(_code integer) RETURNS SETOF public.cat_modality
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY SELECT * FROM public.cat_modality where id_transact = _code order by id asc;
END
$$;
 B   DROP FUNCTION public.vud_transact_record_modality(_code integer);
       public       postgres    false    3    203    1            �            1255    25101 !   vud_transact_requeriment(integer)    FUNCTION     �  CREATE FUNCTION public.vud_transact_requeriment(_id_code integer, OUT id integer, OUT description character varying, OUT legal_foundation character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
	
	RETURN QUERY select 
		a.id,
		a.description ,
		c.legal_foundation
	from 
		public.cat_requirement as a
	inner join public.cat_transact_requirement as b
	on a.id = b.id_requirement
	inner join public.cat_transact as c
	on c.code = b.id_transact
	where c.code = _id_code;

END
$$;
 �   DROP FUNCTION public.vud_transact_requeriment(_id_code integer, OUT id integer, OUT description character varying, OUT legal_foundation character varying);
       public       postgres    false    3    1                        1255    33481 !   vud_update_correct(integer, date)    FUNCTION     �   CREATE FUNCTION public.vud_update_correct(_id_request integer, _correct_date date) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
UPDATE public.request
	
SET 	
	correct_date = _correct_date,
	id_status = 7
WHERE 
	id = _id_request;
END
$$;
 R   DROP FUNCTION public.vud_update_correct(_id_request integer, _correct_date date);
       public       postgres    false    3    1            
           1255    33547 V   vud_update_modality(integer, character varying, character varying, integer, character)    FUNCTION     z  CREATE FUNCTION public.vud_update_modality(_id integer, _description character varying, _legal_foundation character varying, _days integer, _type_days character) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE public.cat_modality
	SET   description=_description, legal_foundation=_legal_foundation, days=_days, type_days = _type_days
	WHERE id = _id;

END
$$;
 �   DROP FUNCTION public.vud_update_modality(_id integer, _description character varying, _legal_foundation character varying, _days integer, _type_days character);
       public       postgres    false    3    1                       1255    33480 *   vud_update_prevention(integer, date, date)    FUNCTION     L  CREATE FUNCTION public.vud_update_prevention(_id_request integer, _prevent_date date, _citizen_prevent_date date) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
UPDATE public.request
	
SET 
	prevent_date = _prevent_date, 
	citizen_prevent_date = _citizen_prevent_date,
	id_status = 7
WHERE 
	id = _id_request;
END
$$;
 q   DROP FUNCTION public.vud_update_prevention(_id_request integer, _prevent_date date, _citizen_prevent_date date);
       public       postgres    false    1    3                       1255    16736 2   vud_update_requirement(integer, character varying)    FUNCTION     �   CREATE FUNCTION public.vud_update_requirement(_id integer, _description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

	UPDATE public.cat_requirement
	SET  description=_description
	WHERE id = _id;
END
$$;
 Z   DROP FUNCTION public.vud_update_requirement(_id integer, _description character varying);
       public       postgres    false    3    1                       1255    33509 G   vud_update_status_request(integer, integer, integer, character varying)    FUNCTION     ~  CREATE FUNCTION public.vud_update_status_request(_id_request integer, _status integer, _last_status integer, _cancelation_description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE 
	public.request
SET 
	id_status = _status,
	last_status = _last_status,
	cancelation_description = _cancelation_description
WHERE 
	id = _id_request;
END
$$;
 �   DROP FUNCTION public.vud_update_status_request(_id_request integer, _status integer, _last_status integer, _cancelation_description character varying);
       public       postgres    false    1    3            �            1255    33536 V   vud_update_transact(integer, character varying, character varying, integer, character)    FUNCTION     y  CREATE FUNCTION public.vud_update_transact(_id integer, _description character varying, _legal_foundation character varying, _days integer, _type_days character) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
	UPDATE public.cat_transact
	SET  description=_description, legal_foundation=_legal_foundation, days = _days, type_days = _type_days
	WHERE id = _id;
END
$$;
 �   DROP FUNCTION public.vud_update_transact(_id integer, _description character varying, _legal_foundation character varying, _days integer, _type_days character);
       public       postgres    false    3    1                       1255    25020 l   vud_update_user(integer, character varying, integer, character varying, integer, integer, character varying)    FUNCTION       CREATE FUNCTION public.vud_update_user(_id_user integer, _user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$ 
	BEGIN
	IF (_password = '') THEN
		update 
		public."users"
	SET		
		user_name = _user_name,
		employee_number = _employee_number,
		description = _description,
		rol = _rol, 
		user_register = _user_register
	WHERE 
		id = _id_user;
	ELSE 
		update 
		public."users"
	SET		
		user_name = _user_name,
		employee_number = _employee_number,
		description = _description,
		rol = _rol, 
		user_register = _user_register,
		password = _password
	WHERE 
		id = _id_user;
	END IF;
	
	RETURN true;
	END
	$$;
 �   DROP FUNCTION public.vud_update_user(_id_user integer, _user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying);
       public       postgres    false    1    3                        1255    25122    vud_users_permisions(integer)    FUNCTION     z  CREATE FUNCTION public.vud_users_permisions(_id_user integer, OUT id integer, OUT name character varying, OUT id_module integer, OUT id_user integer, OUT read_permission smallint, OUT write_permission smallint, OUT delete_permission smallint, OUT update_permission smallint) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY SELECT 
	a.id,
	a.name,
	b.id_module,
	b.id_user,
	b.read_permission,
	b.write_permission,
	b.delete_permission,
	b.update_permission
FROM
	PUBLIC.modules AS A 
	LEFT JOIN PUBLIC.permission AS b ON a.id = b.id_module

WHERE
	b.id_user = _id_user
order by a.id;
END
$$;
   DROP FUNCTION public.vud_users_permisions(_id_user integer, OUT id integer, OUT name character varying, OUT id_module integer, OUT id_user integer, OUT read_permission smallint, OUT write_permission smallint, OUT delete_permission smallint, OUT update_permission smallint);
       public       postgres    false    1    3            �            1255    33472    vud_users_record()    FUNCTION       CREATE FUNCTION public.vud_users_record(OUT id integer, OUT user_name character varying, OUT employee_number integer, OUT user_register integer, OUT rol character varying, OUT description character varying, OUT id_rol integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$ 
		SELECT
			a.id,
			a.user_name,
			a.employee_number,
			a.user_register,
			b.description,
			a.description,
			b.id
		FROM
			public. "users" as a
		INNER JOIN public. "role" as b
		ON a.rol = b.id
		where a.status = 1 order by a.id asc;
	$$;
 �   DROP FUNCTION public.vud_users_record(OUT id integer, OUT user_name character varying, OUT employee_number integer, OUT user_register integer, OUT rol character varying, OUT description character varying, OUT id_rol integer);
       public       postgres    false    3            �            1255    33563    vud_view_data_request(integer)    FUNCTION     �	  CREATE FUNCTION public.vud_view_data_request(_id_request integer, OUT id integer, OUT folio character varying, OUT id_user integer, OUT user_name character varying, OUT means_request smallint, OUT prevent_date text, OUT citizen_prevent_date text, OUT correct_date text, OUT observation character varying, OUT request_turn character varying, OUT type_work character varying, OUT name character varying, OUT last_name character varying, OUT phone character varying, OUT delegation character varying, OUT colony character varying, OUT postal_code character varying, OUT street character varying, OUT ext_number character varying, OUT int_number character varying, OUT id_status integer, OUT description_status character varying, OUT date_creation text, OUT date_commitment text, OUT code integer, OUT description character varying, OUT modality_desc character varying, OUT id_turn integer, OUT id_dictum integer, OUT folio_dictum character varying, OUT date_receives_dictum text, OUT answer_dictum integer, OUT citizen_dictum text) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY SELECT 
	a.id,
	a.folio,
	a.id_user,
	i.user_name,
	a.means_request,
	to_char(a.prevent_date,'yyyy-mm-dd'),
	to_char(a.citizen_prevent_date,'yyyy-mm-dd'),
	to_char(a.correct_date,'yyyy-mm-dd'),
	a.observations,
	a.request_turn,
	a.type_work,
	g.name,
	g.last_name,
	g.phone,
	h.delegation,
	h.colony,
	h.postal_code,
	h.street,
	h.ext_number,
	h.int_number,
	d.id as id_status,
	d.description as description_status,
	to_char(a.date_creation,'yyyy-mm-dd' ),
	to_char(a.date_commitment,'yyyy-mm-dd' ),
	b.code,
	b.description,
	c.description as modality_desc,
	e.id as id_turn,
	f.id as id_dictum,
	f.folio_dictum,
	to_char(f.date_receives_dictum,'yyyy-mm-dd'),
	f.answer_dictum,
	to_char(f.citizen_dictum,'yyyy-mm-dd')
FROM 
	public.request as a
left join 
	public.cat_transact as b
on
	a.id_transact = b.code
left join 
	public.cat_modality as c
on 
	a.id_modality = c.code

left join 
	public.cat_status as d
on
	d.id = a.id_status
left join 
	public.tracing_turn as e
on
	e.id_request = a.id
left join
	public.tracing_dictum as f
on
	f.id_request = a.id
left join
	public.interested as g
on 
	a.id_interested = g.id
left join
	public.address as h
on
	a.id_address = h.id
left join 
	public.users as i
on 
	i.id = a.id_user
WHERE
	flag = 1 
AND a.id = _id_request
order by id asc;

END
$$;
   DROP FUNCTION public.vud_view_data_request(_id_request integer, OUT id integer, OUT folio character varying, OUT id_user integer, OUT user_name character varying, OUT means_request smallint, OUT prevent_date text, OUT citizen_prevent_date text, OUT correct_date text, OUT observation character varying, OUT request_turn character varying, OUT type_work character varying, OUT name character varying, OUT last_name character varying, OUT phone character varying, OUT delegation character varying, OUT colony character varying, OUT postal_code character varying, OUT street character varying, OUT ext_number character varying, OUT int_number character varying, OUT id_status integer, OUT description_status character varying, OUT date_creation text, OUT date_commitment text, OUT code integer, OUT description character varying, OUT modality_desc character varying, OUT id_turn integer, OUT id_dictum integer, OUT folio_dictum character varying, OUT date_receives_dictum text, OUT answer_dictum integer, OUT citizen_dictum text);
       public       postgres    false    1    3            �            1259    16411    ROLE_id_seq    SEQUENCE     �   CREATE SEQUENCE public."ROLE_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public."ROLE_id_seq";
       public       postgres    false    3    199            �           0    0    ROLE_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public."ROLE_id_seq" OWNED BY public.role.id;
            public       postgres    false    198            �            1259    16400    USERS_Id_seq    SEQUENCE     �   CREATE SEQUENCE public."USERS_Id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public."USERS_Id_seq";
       public       postgres    false    197    3            �           0    0    USERS_Id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public."USERS_Id_seq" OWNED BY public.users.id;
            public       postgres    false    196            �            1259    25034    address    TABLE     ;  CREATE TABLE public.address (
    id integer NOT NULL,
    postal_code character varying(7) NOT NULL,
    colony character varying(50) NOT NULL,
    ext_number character varying(32),
    int_number character varying(32),
    street character varying(255) NOT NULL,
    delegation character varying(255) NOT NULL
);
    DROP TABLE public.address;
       public         postgres    false    3            �            1259    25032    address_id_seq    SEQUENCE     �   CREATE SEQUENCE public.address_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.address_id_seq;
       public       postgres    false    209    3            �           0    0    address_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.address_id_seq OWNED BY public.address.id;
            public       postgres    false    208            �            1259    33386    cat_area_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_area_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.cat_area_id_seq;
       public       postgres    false    3    225            �           0    0    cat_area_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.cat_area_id_seq OWNED BY public.cat_area.id;
            public       postgres    false    224            �            1259    16692    cat_modality_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_modality_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.cat_modality_id_seq;
       public       postgres    false    3    203            �           0    0    cat_modality_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.cat_modality_id_seq OWNED BY public.cat_modality.id;
            public       postgres    false    202            �            1259    16746    cat_modality_requirement    TABLE     f   CREATE TABLE public.cat_modality_requirement (
    id_modality integer,
    id_requirement integer
);
 ,   DROP TABLE public.cat_modality_requirement;
       public         postgres    false    3            �            1259    16724    cat_requeriment_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_requeriment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.cat_requeriment_id_seq;
       public       postgres    false    3    205                        0    0    cat_requeriment_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.cat_requeriment_id_seq OWNED BY public.cat_requirement.id;
            public       postgres    false    204            �            1259    25077    cat_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.cat_status_id_seq;
       public       postgres    false    217    3                       0    0    cat_status_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.cat_status_id_seq OWNED BY public.cat_status.id;
            public       postgres    false    216            �            1259    16674    cat_transact    TABLE       CREATE TABLE public.cat_transact (
    id integer NOT NULL,
    code integer NOT NULL,
    description character varying(2000) NOT NULL,
    legal_foundation character varying(3000),
    status integer NOT NULL,
    days integer,
    type_days character(1)
);
     DROP TABLE public.cat_transact;
       public         postgres    false    3            �            1259    16672    cat_transact_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_transact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.cat_transact_id_seq;
       public       postgres    false    201    3                       0    0    cat_transact_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.cat_transact_id_seq OWNED BY public.cat_transact.id;
            public       postgres    false    200            �            1259    16757    cat_transact_requirement    TABLE     x   CREATE TABLE public.cat_transact_requirement (
    id_transact integer NOT NULL,
    id_requirement integer NOT NULL
);
 ,   DROP TABLE public.cat_transact_requirement;
       public         postgres    false    3            �            1259    25053 
   interested    TABLE     �   CREATE TABLE public.interested (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    last_name character varying(100),
    e_mail character varying(100),
    phone character varying(15) NOT NULL,
    type_person smallint NOT NULL
);
    DROP TABLE public.interested;
       public         postgres    false    3            �            1259    25051    interested_id_seq    SEQUENCE     �   CREATE SEQUENCE public.interested_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.interested_id_seq;
       public       postgres    false    213    3                       0    0    interested_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.interested_id_seq OWNED BY public.interested.id;
            public       postgres    false    212            �            1259    33466    log    TABLE     �   CREATE TABLE public.log (
    id integer NOT NULL,
    id_user integer NOT NULL,
    description character varying(255) NOT NULL,
    date_time timestamp with time zone NOT NULL
);
    DROP TABLE public.log;
       public         postgres    false    3            �            1259    33464 
   log_id_seq    SEQUENCE     �   CREATE SEQUENCE public.log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 !   DROP SEQUENCE public.log_id_seq;
       public       postgres    false    3    231                       0    0 
   log_id_seq    SEQUENCE OWNED BY     9   ALTER SEQUENCE public.log_id_seq OWNED BY public.log.id;
            public       postgres    false    230            �            1259    25095    modules    TABLE     �   CREATE TABLE public.modules (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    status smallint NOT NULL
);
    DROP TABLE public.modules;
       public         postgres    false    3            �            1259    25093    modules_id_seq    SEQUENCE     �   CREATE SEQUENCE public.modules_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.modules_id_seq;
       public       postgres    false    221    3                       0    0    modules_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.modules_id_seq OWNED BY public.modules.id;
            public       postgres    false    220            �            1259    25087 
   permission    TABLE     �   CREATE TABLE public.permission (
    id integer NOT NULL,
    id_user integer NOT NULL,
    id_module integer NOT NULL,
    read_permission smallint,
    write_permission smallint,
    delete_permission smallint,
    update_permission smallint
);
    DROP TABLE public.permission;
       public         postgres    false    3            �            1259    25085    permission_id_seq    SEQUENCE     �   CREATE SEQUENCE public.permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.permission_id_seq;
       public       postgres    false    3    219                       0    0    permission_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.permission_id_seq OWNED BY public.permission.id;
            public       postgres    false    218            �            1259    25042    request    TABLE     1  CREATE TABLE public.request (
    id integer NOT NULL,
    folio character varying(50),
    id_user integer NOT NULL,
    date_creation date NOT NULL,
    observations character varying(2000),
    id_status integer NOT NULL,
    id_address integer NOT NULL,
    id_interested integer NOT NULL,
    id_transact integer NOT NULL,
    id_vulnerable_group smallint NOT NULL,
    request_turn character varying(100),
    type_work character varying(100),
    means_request smallint NOT NULL,
    delivery smallint NOT NULL,
    date_commitment date,
    flag smallint,
    id_modality integer,
    prevent_date date,
    citizen_prevent_date date,
    correct_date date,
    cancelation_description character varying(2000),
    last_status integer,
    settlement_date date,
    settlement_number character varying(50)
);
    DROP TABLE public.request;
       public         postgres    false    3            �            1259    25064    request_audit    TABLE     �   CREATE TABLE public.request_audit (
    id integer NOT NULL,
    id_user integer NOT NULL,
    accion character varying NOT NULL,
    id_request integer NOT NULL
);
 !   DROP TABLE public.request_audit;
       public         postgres    false    3            �            1259    25040    request_id_seq    SEQUENCE     �   CREATE SEQUENCE public.request_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.request_id_seq;
       public       postgres    false    211    3                       0    0    request_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.request_id_seq OWNED BY public.request.id;
            public       postgres    false    210            �            1259    33567    request_requirement    TABLE     `   CREATE TABLE public.request_requirement (
    id_request integer,
    id_requirement integer
);
 '   DROP TABLE public.request_requirement;
       public         postgres    false    3            �            1259    33453    tracing_dictum    TABLE     �   CREATE TABLE public.tracing_dictum (
    id integer NOT NULL,
    date_receives_dictum date NOT NULL,
    folio_dictum character varying(50) NOT NULL,
    answer_dictum integer NOT NULL,
    citizen_dictum date,
    id_request integer NOT NULL
);
 "   DROP TABLE public.tracing_dictum;
       public         postgres    false    3            �            1259    33451    tracing_dictum_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tracing_dictum_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.tracing_dictum_id_seq;
       public       postgres    false    3    229                       0    0    tracing_dictum_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.tracing_dictum_id_seq OWNED BY public.tracing_dictum.id;
            public       postgres    false    228            �            1259    33431    tracing_turn    TABLE     �   CREATE TABLE public.tracing_turn (
    id integer NOT NULL,
    id_request integer NOT NULL,
    date_turn date NOT NULL,
    review_area integer NOT NULL,
    observations character varying(2000)
);
     DROP TABLE public.tracing_turn;
       public         postgres    false    3            �            1259    33429    tracing_turn_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tracing_turn_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.tracing_turn_id_seq;
       public       postgres    false    227    3            	           0    0    tracing_turn_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.tracing_turn_id_seq OWNED BY public.tracing_turn.id;
            public       postgres    false    226            �            1259    25059 
   user_audit    TABLE     �   CREATE TABLE public.user_audit (
    id integer NOT NULL,
    id_user integer NOT NULL,
    accion character varying(255) NOT NULL,
    id_user_modification integer NOT NULL
);
    DROP TABLE public.user_audit;
       public         postgres    false    3            �            1259    25104    vulnerable_group_id_seq    SEQUENCE     �   CREATE SEQUENCE public.vulnerable_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.vulnerable_group_id_seq;
       public       postgres    false    223    3            
           0    0    vulnerable_group_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.vulnerable_group_id_seq OWNED BY public.vulnerable_group.id;
            public       postgres    false    222                       2604    25037 
   address id    DEFAULT     h   ALTER TABLE ONLY public.address ALTER COLUMN id SET DEFAULT nextval('public.address_id_seq'::regclass);
 9   ALTER TABLE public.address ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    208    209    209                        2604    33391    cat_area id    DEFAULT     j   ALTER TABLE ONLY public.cat_area ALTER COLUMN id SET DEFAULT nextval('public.cat_area_id_seq'::regclass);
 :   ALTER TABLE public.cat_area ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    224    225    225                       2604    16697    cat_modality id    DEFAULT     r   ALTER TABLE ONLY public.cat_modality ALTER COLUMN id SET DEFAULT nextval('public.cat_modality_id_seq'::regclass);
 >   ALTER TABLE public.cat_modality ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    202    203    203                       2604    16729    cat_requirement id    DEFAULT     x   ALTER TABLE ONLY public.cat_requirement ALTER COLUMN id SET DEFAULT nextval('public.cat_requeriment_id_seq'::regclass);
 A   ALTER TABLE public.cat_requirement ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    204    205    205                       2604    25082    cat_status id    DEFAULT     n   ALTER TABLE ONLY public.cat_status ALTER COLUMN id SET DEFAULT nextval('public.cat_status_id_seq'::regclass);
 <   ALTER TABLE public.cat_status ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    217    216    217                       2604    16677    cat_transact id    DEFAULT     r   ALTER TABLE ONLY public.cat_transact ALTER COLUMN id SET DEFAULT nextval('public.cat_transact_id_seq'::regclass);
 >   ALTER TABLE public.cat_transact ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    200    201    201                       2604    25056    interested id    DEFAULT     n   ALTER TABLE ONLY public.interested ALTER COLUMN id SET DEFAULT nextval('public.interested_id_seq'::regclass);
 <   ALTER TABLE public.interested ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    212    213    213            #           2604    33469    log id    DEFAULT     `   ALTER TABLE ONLY public.log ALTER COLUMN id SET DEFAULT nextval('public.log_id_seq'::regclass);
 5   ALTER TABLE public.log ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    231    230    231                       2604    25098 
   modules id    DEFAULT     h   ALTER TABLE ONLY public.modules ALTER COLUMN id SET DEFAULT nextval('public.modules_id_seq'::regclass);
 9   ALTER TABLE public.modules ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    221    220    221                       2604    25090    permission id    DEFAULT     n   ALTER TABLE ONLY public.permission ALTER COLUMN id SET DEFAULT nextval('public.permission_id_seq'::regclass);
 <   ALTER TABLE public.permission ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    219    218    219                       2604    25045 
   request id    DEFAULT     h   ALTER TABLE ONLY public.request ALTER COLUMN id SET DEFAULT nextval('public.request_id_seq'::regclass);
 9   ALTER TABLE public.request ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    211    210    211                       2604    16416    role id    DEFAULT     d   ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public."ROLE_id_seq"'::regclass);
 6   ALTER TABLE public.role ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    198    199    199            "           2604    33456    tracing_dictum id    DEFAULT     v   ALTER TABLE ONLY public.tracing_dictum ALTER COLUMN id SET DEFAULT nextval('public.tracing_dictum_id_seq'::regclass);
 @   ALTER TABLE public.tracing_dictum ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    228    229    229            !           2604    33434    tracing_turn id    DEFAULT     r   ALTER TABLE ONLY public.tracing_turn ALTER COLUMN id SET DEFAULT nextval('public.tracing_turn_id_seq'::regclass);
 >   ALTER TABLE public.tracing_turn ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    226    227    227                       2604    16405    users id    DEFAULT     f   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public."USERS_Id_seq"'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    197    196    197                       2604    25109    vulnerable_group id    DEFAULT     z   ALTER TABLE ONLY public.vulnerable_group ALTER COLUMN id SET DEFAULT nextval('public.vulnerable_group_id_seq'::regclass);
 B   ALTER TABLE public.vulnerable_group ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    223    222    223            �          0    25034    address 
   TABLE DATA               f   COPY public.address (id, postal_code, colony, ext_number, int_number, street, delegation) FROM stdin;
    public       postgres    false    209   �5      �          0    33388    cat_area 
   TABLE DATA               3   COPY public.cat_area (id, description) FROM stdin;
    public       postgres    false    225   W6      �          0    16694    cat_modality 
   TABLE DATA               u   COPY public.cat_modality (id, code, description, legal_foundation, id_transact, status, days, type_days) FROM stdin;
    public       postgres    false    203   �6      �          0    16746    cat_modality_requirement 
   TABLE DATA               O   COPY public.cat_modality_requirement (id_modality, id_requirement) FROM stdin;
    public       postgres    false    206   D      �          0    16726    cat_requirement 
   TABLE DATA               B   COPY public.cat_requirement (id, description, status) FROM stdin;
    public       postgres    false    205   I      �          0    25079 
   cat_status 
   TABLE DATA               5   COPY public.cat_status (id, description) FROM stdin;
    public       postgres    false    217   N�      �          0    16674    cat_transact 
   TABLE DATA               h   COPY public.cat_transact (id, code, description, legal_foundation, status, days, type_days) FROM stdin;
    public       postgres    false    201   ��      �          0    16757    cat_transact_requirement 
   TABLE DATA               O   COPY public.cat_transact_requirement (id_transact, id_requirement) FROM stdin;
    public       postgres    false    207   �      �          0    25053 
   interested 
   TABLE DATA               U   COPY public.interested (id, name, last_name, e_mail, phone, type_person) FROM stdin;
    public       postgres    false    213   f�      �          0    33466    log 
   TABLE DATA               B   COPY public.log (id, id_user, description, date_time) FROM stdin;
    public       postgres    false    231   �      �          0    25095    modules 
   TABLE DATA               3   COPY public.modules (id, name, status) FROM stdin;
    public       postgres    false    221   ��      �          0    25087 
   permission 
   TABLE DATA               �   COPY public.permission (id, id_user, id_module, read_permission, write_permission, delete_permission, update_permission) FROM stdin;
    public       postgres    false    219   �      �          0    25042    request 
   TABLE DATA               l  COPY public.request (id, folio, id_user, date_creation, observations, id_status, id_address, id_interested, id_transact, id_vulnerable_group, request_turn, type_work, means_request, delivery, date_commitment, flag, id_modality, prevent_date, citizen_prevent_date, correct_date, cancelation_description, last_status, settlement_date, settlement_number) FROM stdin;
    public       postgres    false    211   H�      �          0    25064    request_audit 
   TABLE DATA               H   COPY public.request_audit (id, id_user, accion, id_request) FROM stdin;
    public       postgres    false    215   ��      �          0    33567    request_requirement 
   TABLE DATA               I   COPY public.request_requirement (id_request, id_requirement) FROM stdin;
    public       postgres    false    232   �      �          0    16413    role 
   TABLE DATA               /   COPY public.role (id, description) FROM stdin;
    public       postgres    false    199   <�      �          0    33453    tracing_dictum 
   TABLE DATA               {   COPY public.tracing_dictum (id, date_receives_dictum, folio_dictum, answer_dictum, citizen_dictum, id_request) FROM stdin;
    public       postgres    false    229   }�      �          0    33431    tracing_turn 
   TABLE DATA               \   COPY public.tracing_turn (id, id_request, date_turn, review_area, observations) FROM stdin;
    public       postgres    false    227   ��      �          0    25059 
   user_audit 
   TABLE DATA               O   COPY public.user_audit (id, id_user, accion, id_user_modification) FROM stdin;
    public       postgres    false    214   ��      �          0    16402    users 
   TABLE DATA               �   COPY public.users (id, user_name, employee_number, description, rol, user_register, date_register, status, password) FROM stdin;
    public       postgres    false    197   Ժ      �          0    25106    vulnerable_group 
   TABLE DATA               C   COPY public.vulnerable_group (id, description, status) FROM stdin;
    public       postgres    false    223   �                 0    0    ROLE_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public."ROLE_id_seq"', 3, true);
            public       postgres    false    198                       0    0    USERS_Id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public."USERS_Id_seq"', 41, true);
            public       postgres    false    196                       0    0    address_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.address_id_seq', 205, true);
            public       postgres    false    208                       0    0    cat_area_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.cat_area_id_seq', 1, false);
            public       postgres    false    224                       0    0    cat_modality_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.cat_modality_id_seq', 59, true);
            public       postgres    false    202                       0    0    cat_requeriment_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.cat_requeriment_id_seq', 481, true);
            public       postgres    false    204                       0    0    cat_status_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.cat_status_id_seq', 1, true);
            public       postgres    false    216                       0    0    cat_transact_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.cat_transact_id_seq', 35, true);
            public       postgres    false    200                       0    0    interested_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.interested_id_seq', 172, true);
            public       postgres    false    212                       0    0 
   log_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.log_id_seq', 82, true);
            public       postgres    false    230                       0    0    modules_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.modules_id_seq', 1, false);
            public       postgres    false    220                       0    0    permission_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.permission_id_seq', 53, true);
            public       postgres    false    218                       0    0    request_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.request_id_seq', 151, true);
            public       postgres    false    210                       0    0    tracing_dictum_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.tracing_dictum_id_seq', 11, true);
            public       postgres    false    228                       0    0    tracing_turn_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.tracing_turn_id_seq', 28, true);
            public       postgres    false    226                       0    0    vulnerable_group_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.vulnerable_group_id_seq', 1, false);
            public       postgres    false    222            )           2606    16418    role ROLE_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.role
    ADD CONSTRAINT "ROLE_pkey" PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.role DROP CONSTRAINT "ROLE_pkey";
       public         postgres    false    199            %           2606    16410    users USERS_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.users
    ADD CONSTRAINT "USERS_pkey" PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.users DROP CONSTRAINT "USERS_pkey";
       public         postgres    false    197            5           2606    25039    address address_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.address DROP CONSTRAINT address_pkey;
       public         postgres    false    209            G           2606    33393    cat_area cat_area_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.cat_area
    ADD CONSTRAINT cat_area_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.cat_area DROP CONSTRAINT cat_area_pkey;
       public         postgres    false    225            /           2606    16791 "   cat_modality cat_modality_code_key 
   CONSTRAINT     ]   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_code_key UNIQUE (code);
 L   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_code_key;
       public         postgres    false    203            1           2606    16789    cat_modality cat_modality_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_pkey;
       public         postgres    false    203            3           2606    16731 $   cat_requirement cat_requeriment_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.cat_requirement
    ADD CONSTRAINT cat_requeriment_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.cat_requirement DROP CONSTRAINT cat_requeriment_pkey;
       public         postgres    false    205            ?           2606    25084    cat_status cat_status_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.cat_status
    ADD CONSTRAINT cat_status_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.cat_status DROP CONSTRAINT cat_status_pkey;
       public         postgres    false    217            +           2606    16690 "   cat_transact cat_transact_code_key 
   CONSTRAINT     ]   ALTER TABLE ONLY public.cat_transact
    ADD CONSTRAINT cat_transact_code_key UNIQUE (code);
 L   ALTER TABLE ONLY public.cat_transact DROP CONSTRAINT cat_transact_code_key;
       public         postgres    false    201            -           2606    16688    cat_transact cat_transact_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.cat_transact
    ADD CONSTRAINT cat_transact_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.cat_transact DROP CONSTRAINT cat_transact_pkey;
       public         postgres    false    201            9           2606    25058    interested interested_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.interested
    ADD CONSTRAINT interested_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.interested DROP CONSTRAINT interested_pkey;
       public         postgres    false    213            M           2606    33471    log log_pkey 
   CONSTRAINT     J   ALTER TABLE ONLY public.log
    ADD CONSTRAINT log_pkey PRIMARY KEY (id);
 6   ALTER TABLE ONLY public.log DROP CONSTRAINT log_pkey;
       public         postgres    false    231            C           2606    25100    modules modules_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.modules
    ADD CONSTRAINT modules_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.modules DROP CONSTRAINT modules_pkey;
       public         postgres    false    221            A           2606    25092    permission permission_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.permission
    ADD CONSTRAINT permission_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.permission DROP CONSTRAINT permission_pkey;
       public         postgres    false    219            =           2606    25071     request_audit request_audit_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.request_audit
    ADD CONSTRAINT request_audit_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.request_audit DROP CONSTRAINT request_audit_pkey;
       public         postgres    false    215            7           2606    25050    request request_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.request
    ADD CONSTRAINT request_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.request DROP CONSTRAINT request_pkey;
       public         postgres    false    211            K           2606    33458 "   tracing_dictum tracing_dictum_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.tracing_dictum
    ADD CONSTRAINT tracing_dictum_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.tracing_dictum DROP CONSTRAINT tracing_dictum_pkey;
       public         postgres    false    229            I           2606    33439    tracing_turn tracing_turn_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.tracing_turn
    ADD CONSTRAINT tracing_turn_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.tracing_turn DROP CONSTRAINT tracing_turn_pkey;
       public         postgres    false    227            ;           2606    25063    user_audit user_audit_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.user_audit
    ADD CONSTRAINT user_audit_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.user_audit DROP CONSTRAINT user_audit_pkey;
       public         postgres    false    214            '           2606    33531    users users_employee_number_key 
   CONSTRAINT     e   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_employee_number_key UNIQUE (employee_number);
 I   ALTER TABLE ONLY public.users DROP CONSTRAINT users_employee_number_key;
       public         postgres    false    197            E           2606    25111 &   vulnerable_group vulnerable_group_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.vulnerable_group
    ADD CONSTRAINT vulnerable_group_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.vulnerable_group DROP CONSTRAINT vulnerable_group_pkey;
       public         postgres    false    223            O           2606    16815 *   cat_modality cat_modality_id_transact_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_id_transact_fkey FOREIGN KEY (id_transact) REFERENCES public.cat_transact(code);
 T   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_id_transact_fkey;
       public       postgres    false    2859    201    203            Q           2606    16838 B   cat_modality_requirement cat_modality_requirement_id_modality_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality_requirement
    ADD CONSTRAINT cat_modality_requirement_id_modality_fkey FOREIGN KEY (id_modality) REFERENCES public.cat_modality(code);
 l   ALTER TABLE ONLY public.cat_modality_requirement DROP CONSTRAINT cat_modality_requirement_id_modality_fkey;
       public       postgres    false    2863    206    203            P           2606    16797 E   cat_modality_requirement cat_modality_requirement_id_requirement_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality_requirement
    ADD CONSTRAINT cat_modality_requirement_id_requirement_fkey FOREIGN KEY (id_requirement) REFERENCES public.cat_requirement(id);
 o   ALTER TABLE ONLY public.cat_modality_requirement DROP CONSTRAINT cat_modality_requirement_id_requirement_fkey;
       public       postgres    false    206    205    2867            R           2606    16776 E   cat_transact_requirement cat_transact_requirement_id_requirement_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_transact_requirement
    ADD CONSTRAINT cat_transact_requirement_id_requirement_fkey FOREIGN KEY (id_requirement) REFERENCES public.cat_requirement(id) ON UPDATE CASCADE ON DELETE CASCADE;
 o   ALTER TABLE ONLY public.cat_transact_requirement DROP CONSTRAINT cat_transact_requirement_id_requirement_fkey;
       public       postgres    false    2867    207    205            S           2606    16832 B   cat_transact_requirement cat_transact_requirement_id_transact_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_transact_requirement
    ADD CONSTRAINT cat_transact_requirement_id_transact_fkey FOREIGN KEY (id_transact) REFERENCES public.cat_transact(code);
 l   ALTER TABLE ONLY public.cat_transact_requirement DROP CONSTRAINT cat_transact_requirement_id_transact_fkey;
       public       postgres    false    207    2859    201            T           2606    33440    tracing_turn review_area_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.tracing_turn
    ADD CONSTRAINT review_area_fk FOREIGN KEY (review_area) REFERENCES public.cat_area(id);
 E   ALTER TABLE ONLY public.tracing_turn DROP CONSTRAINT review_area_fk;
       public       postgres    false    227    2887    225            N           2606    25021    users users_id_fkey    FK CONSTRAINT     m   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_id_fkey FOREIGN KEY (rol) REFERENCES public.role(id);
 =   ALTER TABLE ONLY public.users DROP CONSTRAINT users_id_fkey;
       public       postgres    false    199    2857    197            �   b   x�320�45�41����M,V���KR�
���ə�7�qrr:��)�;�:��+8:{�y:�s:�dV%^������X���_��ed`
5�6�=... �C�      �      x�u��	�@D�V���_Z�0
�U�lC -唒ܘ��ܲ�a�9Y]�u{W���S�,h�)��b���b�x�jp��r]|{eej�dI�z꜇����πs��|Ӫ1;}&�t�����YW-#<G ��>�      �     x��\Ms�6>3��͌�H���'WvZ�4����L.	���CIO�[����C/�[����H��)9��z��LUS$�������;�Xg�W"��Lf��|�{PDi���k6e���IP�l�*����e�)q���'h�M��P�p���L���%������Ed9_D"t�Bf��9���x�C�>c)�<el�g,Hc�ɓ�R;FԲ��C�:_^D2H����d�=_߼�#L/i�s\�� �G�L6[߄E�E8V"	�^䩒x ן�3��������_�K<	�@&臄��My�D�;�w������*b5��B$�YF$3��F,e���\�e-�ZMIÓ:�S��4�ϼc>�c��� ��h�;RaC��~��^����္2�	{�xPZ����X�5u73��?F�2%�}'UZ�."��Td\�4��� �W�@W�R�;	����A�7d��H����5�}3�kZ_�e�c�9�$$�G����L�U�$��H��L�R� b�Rv| :w�lad�F�ȡ�M�:&SVr�~�ty�� �����,�R"h��3�g

l�:�ܓ4+�������"W��@�)����ɳZ���^��d*���U��M��H���\�o2�����*��Z�.Ĳ�յ���f��$��gW�,j���LDi��j���_�~JE_�gE��P)zRZwB򶜬�7���г�2Z�gס�z��d 0 ף�����/-*��+�ᠸF�^�U2�G�b~m Z$,�J�a�����\��q���%������be@Ι}���r�8*8��"��;@�J
"a�$�ϮK�^���UeZ��������c�?&��R��\���R���v�pq�h�4'�Qz�}�,�Ib��4�p��*�7�S� 8 ry�g):3��.�� iI_D�oJE��OP���B�e��c��d�Z�و Ϗj��!^C��&hA��PR��y�i�ƣ��w�φFJ��%�P_�)��M�s�Wvl�~��cR�t;��#�rV��N��Z{Iر�����Nu~L���B���RP����Ҽ(r��7�gM
���~�f��p�.ܗK�qz��!<��C}�O�|�2�f��q}���Mj�u��ÄS�x�̣�z�g��V��f�k#����?���$��
PmI; }8��(2�ĥ=d�ԟk;�V�h� !����A����������e�`�; 	!��Ib�ۈ~����HǶ�D��H��Ƒ*�]�jHq��u�ckj{����d�S�'���}����+5�������������ظ�6�I�yIn�N�:g��ĽTK�z��؆�~,6��N[Y�q��f����!�y6;i���ۮɭve�w��n3M��e�$��G���r�a��qS����QK"�f&IQʮ)��OW	u[͔�������J�'5y:��aCJh;��d������C'��2P�v. l[����;�G���_��*���4����@�?������?G:g]�?*���	J_$��������:�"���b�(͛�R,�<�}{?�7�Wᴤ��o�#i�г�4���,.hA�8�ń��"Uz�
I���D��"~��hVDy�j�j;zRL�O������� B���	n5���
cC4�-Ҹ���o�D��y�v�҄��.�� c�����	���4	y?#�g����%�1�5�����TV����ݣ�G�c��L ұ�^U͚ �oʈ��%��1�'��j(�4���)�\I�z[�Zt�z�3紴�DJ�ȍUl�#�����c���0��t�����J�2LU�K�E��9)&QAk�f��G�1���ۣ�q�;�ߩuZ�d�x]-4���;=4{h>
4����9���?���:��|GW��i޾��[� ������c؏�qZ�p���
��,[	"�w�=X�#�ս{��n�s���c���O���,�}MI�ĉ����[&5 �ު�b���Ʈ���[�S�W]9���)�Ө�"ڦʢI�Ԟ�"\��ur4"y��P����)Di�wy�vA`_9�K�l{_{�l��_VP�>��߷�Rc�tޡA]G)�u�8� v�'T=���9��X�;�XM�U5آ�.�(P(�}�u��:0R����}�1��V�����Y�Ӻ�3�آ�d&iU �{0��4յZ�x�U0�0&4�ft���^��~��&m�Y�A@c���hl��])c���4�4��F-����O�o"r,��'��Q�CA;�t-��V��HB�#��2�\�}��z��5+�r���|!͚mj)���A睋�����%�
�.��	L��N��<������k
�Z����4�ݖ���
emq���o\��v�q��h���E�ˤ(
���5w��7C��w�ĥP&���A�|��|R;�T<F�k򂪃[�A���.�gG�=<:��±����-+�1�J���R~Lۣ��B	㠨Ghw����x,O�����tbWu?���Zl������U���\��Cn�!�X�'hc�ѹ��<2�Y �k6{n��F3xH"�+��"5K�]9e'�:%�I��GE���/���r�Y�B/AJ�l�C���P�4ԑ=�O�Y�@#����{�'�,�O'��.�~=�~����O���>}�'�=���Xc˳.)�������!u �&��ضNZ�H[?7ۼA�TM��O���r���������񑑲K�;V�6��L��˽5��_|���PPr^3�I8��TC"##�IR0J��CeQ㝒o�Ԕ���&�P�9q�q��lk5��w����8��f���]����ڧS�6?��~�D���&�æĨ�.0�h�e�]��ɶm��/�}urQb� hj��
"�D�6D0�nbD�"�f3niJ�ŹKW��RIh�	=-�/�J��?�VKY�To"B�ڼt�zis������]P�eҷ@/�K�E|�ױ��l�����v~���>{uLűZ�������y����*"x���Y�o���g�J�V�
ړD_Z!���b��9�;���>��D�#IytR��&]l����6�ꐖ�>̥ǲ�g��r�s*;�����w��,,���`��Π��	/!c�5�α��r�]��EՊ�{��j�{N2��#6�Z�=�Z��{{Lhp-��!�j���~1d�J�9:?{IoD�M,$�Sq�Yj�R�Xp�~ԩ��/T�-�p�j�h��?����{�M�1��L,dǁ\�ӝZ/3�n~0�@w�y��ޚN'���K<y�?��l      �     x�]�[��8E�s�
�ęK��{�K*C0�x�Sv�_�l\5�x����l��x}M|g�r�ul�q�����=k���<�n�ȼ���Î_Ϻ���g��W�g����x�����,{vle �������m˱ǣOO��[�c���r�+��r��i[m9W��lyƱ�5�̑c�G_^�3�g�:��y��w䢹��[�;����=[ڱ������3����������ߖg�����ky�o?��F�?eԑ���-�Ӳ����m�%�6mYG�w��sFKo��̖3N�S���٧��{�=Ε5�U��ni[�Q>sׯ�ݟr��)���繭{<�6������y}Ϲ�S�=}y�J�r�q��e�����3��	k+���p��:�O�O��XeR4Uo[�G���|�h��������������Rڙ(��po��N0�(��W�.׆�<!$(���;�W��(�d�ڐG�ۆw��K)����ɢ�/>(CβX�e7[h����J��.�[qn"w>��2�Cv�O��u|����طW}�����O9߄}��)>N�cY��{��a�J���-?�K��
Vi/��C��=U�� T+��%�D�M���Hy�):ri�⮴��g
\�
�-M	ϫx�{�gR��G�S:��J0u�S5a�+�d�4,q�^������=���z�Som��1��Zb�A��f�|���7C[����aN��-~��ۓ�{����w���RR��n);��#�))Z^m9��my�ꑂ�#��Ze��������bzV.!��)�\:�3����A�l8
  ��4�4��}�h�F�5Z��k�fk�K�����F�5z��s�^k4[��]���F6Z��{��kt_�~1W���v��y(����}��k�_��Ԃ�����pw�;��w����!�T���S�N�
8ph;���8��m�~1�/�m��3��N(��;��w�����p�����{�=�T ��h�������qP����{�=��`���A�2�E�X�p�����{�=�pO�'��	��{B;���Nh'��	��zI�
$��	��{R��y�bhN��y����pO�'��	��{���^p/�����E�
(*P�-jQp/�RT��@Q��E�
(���[I�A��^�q2E��{���^p/���n�7��ߥ����YMg�����[��^�ݵ~�^���~�^���~f^�g�5����i=,�[�{kɉ��Ϸ��f	?"�ȿw�������������<      �      x��}KoY���+.�0U|�e���2K������Ơ�F���2X�`z��^��w��"��0������{#��dgN��PU�D���}�3z��"OL��"1��r�����j�Uy�o~���4�-J��e��*I�(+�>U��0%�+Y��_���y���ʦS��l.`��������
F��}[�n���{����y���ʹ�-�l�͗I����[�OYm�w
S%Ӽ�ïp�E���i>�O�gӤZ��~:�:�>��ƹO���wEr[d��`�JҼ웻<����������9,4�%.�%�=[N��jG����*�o�n�i�{��x0�|��`&S���U��
�y�(�O�6u~��i_/Lr�����`�ER�6͢��9�j�8*�m��h��|��a���1��������V�����"O������񒾁�,V�%�f��Ws�
�'�N������9�{��>����Op�5��ǆ���pG%,�LaU�����Ug��
���R��e��V@[��̀ڀ`�vq�x.2�����d0�[��)��=�|s���i�	&� ���q�*�d���f	�v�ۙp��K��GR��p�~яX3M>�8_£)�C~�T ��]&ppd��S�#�.���N湼��2�Yi�C�QV�40�*)`������毫��K�	X���J�[#y%\GZ�F�uY��.3~|�eI���K͖D�4�lj���[<H~����Sx���%"�gV��p��_�>�]^O��������	�'��w�U���.i��|Z���rN��:Fe~��opCoWY�.M���.�;��%r���i���RM���o"_��{=K�������!n<o��N�2Y%�#���Ռ�)��x(���C�E����]�	XV���!��l�?K�c�P¨g�c	��a�}�T�j�ˎ3?��fp��ƳzZ�%S#���\��H��#��y
[Bz(�l����n��k"$bS�>|�M�;F�"Y�go+d����Lr|������ �t�HH
�fU���l�dV���)���g�6���+�n���ۇ�Ȋ�y=��\'0��ŴX�ao�3Z-�-�q�<���{�����NC��i�
x�:�HVy��	��_��e��{��<�+��k��F�6���D���ZX�`N���(R�,"��"������IΗ�A~JO�-G���[�����%0	_�b^�f@Rr8�%�cp>V(3��B�1�4̠?m�X厉���Ӽ��
+����$Z
��'K����ڰ8��)s�Aa0d����]$���z�O�g��>�	�L�Ñ��\m^��*�/�C��Ă���1H�|�<�D�]3Έr��'�(a���1�q��*�ڙ�ɢʦT%xda(�A��%�-��q��WnO�gtpY]�eiY�՟�3������ ����n>�+�m�;8O� 8���~���];I�a�w����*v	mX��25�d�%����*�%*� Z�7D�b�!�=
�A�w�b�y�GOא�� 'ԧ�\P{������%^���L��̛_�$�9���6wr�7���sf�S{6Nu@V���~�dѾ�}w�"�\��i�O)�"�`�;���(�d]�6�q�'M�3к+""&n��n� np�BQ�/�أ۬I��/�W�W�̎#��nȰ���Rf��M܉��N���IZ�Do�ތ�����=�,��)�?K���֚��q���?$̌�d�fk�K�,�υUh�����]��`Qɧr�%M�\�Q�S� � 3�c��<�ô��c4��\�lf���d��{��m�Uy��t��a	�5M�-���@/	�F��R�^1-Q�o?�m�6���� ^Xd������ߐ́u!g@g����Y_<Ԛ{ݪ���YvG�nC�q:�ϫ�b��z�[���y�:���ɝ���K��<A��O���n-Ơ�����6���Z$u�&���Hӟ'^�v�I� �������-�n� �}j�sS6�|ܹg���Ue��\�?�ӂL-#%�z^�@s& �a	�Z��$�<C:]�<N��M���]�U��I5�v3�t��g�D$'�؁!�`n�4�TL»�$ć6x�$�;��~*�e��	do����YZ6�Z6R�l�u�$��4��آ�=J��uX��S�{7��`5o�"��[�I�z)� _n>W����|1�)ڗ0Q�P�� ���L�d��̉n��\�n�O{����>2/d���P`��
Y��,� mT��J�t�� �cRl�=�Q�����l�t+��h)�6�?��[ޏΜ|��%p
0�n+e8j��}��gP��q��].��v5<ݾ-����,�W~���N�Y�f�s�x��-Q"�Y�W�w���s#�84K9d����k8b��:�Ȩ��l۬�S`��m_���h���v���(zai:a�o�d`��ϕTFy4[ͽ��H|C's�R0�K^�S��Bq=�7侬�9'���ހ�Dr��
�~lg����=�۫y)��?]&�� E�@w��j �A.�P/�@���[�	9��YK;�mfԚ�5�<��{r�'���䷦�3(�Ɛՠ%l4��Ӿ#0J�;Ng&EI<�[%,�3�RQ����e�$u�f��w��7��a�Q��a��u����e�Y���u��Ybl����a&�K�Ϫ��p�v:.�#V#azP��$�&�[�ڑL\�D��6-��^΅�X���;�\o5��lzo���������f�(����wA�US$�3�P0�>�UM�E*|y�of����gl�g�Cbv��B^q�^`r���e��^��:;Of�곕KD?K0����\M-x�xG�=�0�P�õ�[��g(���$���\H/�,�iF�5K����+��9����z~ifP��U̶~pkK�}�'��kk>	�M���,q<���z� �cJW�l��g��Prѐ��=��]'��̳iV��7b�[V�c�6wd�t�B9B�B�J!^S������>8�n��䶒 �)YQԆ�B���(��=O���6������ϻ��[�_P�._�Q�} N��ş_b$m��q8]���}VU��B�RP�k�����ڔ]G��k����X���2���Y�8pԐ�3�e~�m�7�@+\"��o2'&�)��u@�Ƈ"m���H��{�њ���E�*�|x\��D����Fe�c�
����ˊD���D�<�}b7��1�OkZ�Fؘ(HDO��PE+|���	��q�����ȫrI���ܱ�Wy<������t���:�w��y��!K�b�H�a
|���8����(���J�-=�Q�9x�(9�1����%P����R���7t��x�]\����k��Sx&�ޞ�	������:L����:½��}��ޥ��-�mo)��=�D+?�R���ͨ7A�mY�;��m�a3썍�֡z=KP���%�بw���5陔�/)nxnus ���MsE?ݦ�O�ӌ�x��6���s �c;'-Aͅ��W��̃Y��sK�H�fM������QbH�G��(ג���D���l��ҝ�&Ef�L�6��wm� ��5nu4T�e�._��o�ť���y�n�o篯�����]�\���|��\h?�{n�{��
9�u�.J8�'���w���=;�fiy���P�^�y]���Mߜ���;��ө�o��r���ݑf�5~ �i�!�:n螄+�3 "K��������З��)�':!�"aG��8Y3�d��=��	��^��U���D�+��9?�������?���p�`3����4�-�\���8�ƪ���=�l�s�U3���dB�`j�,JrA�C6�œ�ӎ�=7�B��1��Q�/ ׬x�z�"�Ǵ�����w8����E�p���	OT�p?U*�,�/D�ű�_�![�do}4�Zi�u���|�T���
��9�.��HP�    L���_��=���޳�ݕp\Nc�\�.]վE~��[�3a)�3�� �q�Bu�m\��V�6��;N��i}.��9ő��p�)�gm1��Mi���=��&+�L]�I�X��d���p KIX�ކ{�w�M
R��x��Fp"\�6.��G�Nݒ^̜~@K
Wf���%r[''2�t����p_n��@*kF��$N^`�R*p?LU�	>��f>ڀK��9�%i�2��U>'��E��=��B�̞{M�Sm�f���ē����z��-�a���wx�7A�r���������F(y:-J�[�B��A-�y��ڷS�;<q��t���S�O>ɣAd����b����뤀>�s�8�8s��߬]�9��D �\°Jw��i���O�h�E㒅��A�G��>��5eI���,5S�F/-�cwS�S�0�H'I`3L�v'd�HUP�9콱�ʻ䡬T�����2hP��@�OU�MB�S�3��#�Gц�X�9��=���v_G�]QC��\���Vrܻ��yYze��\ ��|V��	��
�Ibv�{�qr0�O�n�h�c���Ԅ���d�%��h�=����*:��G'�X�,(�	M��iФ����̟A�Q��3�ly������f����+�6+И=���'���+1\б��F�����e��!�]Q�+SB]�j���ԙ5[d|7n�RP�sm���?�S�Sd���}��?�mYB8ިVE��c����� O�eج�dN)��_�
\V�����/s)Kd�K��*���0}�S)�v����t-��6)��/�2�Ϲ��:3��sO��~���><k|Ĳ�����PX�_�bQi�c��Έ�tT��I@��e������//�t����{Q��Aq?�L�*s�=��'J	�t �@>$X��^2�U�?�E ����&�. 7t(�V�T}$R�-�������|To���PT+Ύ��9�ܨ�`Zl�P���|ʭą=e�I�1��; -�dJ�sEnF�* B\�q�0�VL��v1�̹׌��S0��;��a�L^R E�l�z�cRK�x�
��t�s���V�qڻ�|�����Ϳ�~a^��zX�aå�I0�q@L�*Z^q�,B�Ɛ�}������܀M����Z�w��+Ka���2/�x�D��D?��w._�8�7��_�ܖ�
6F6-�4���s�("��B`�%�Ȟ��LY:\�k]�����䧊��J*A6Y�6H��GJJ��C��F��	S�O�^e�:�*H2]ͷ窗�oa�֤�>N����	"/ߚ�ɢ�;�+����^�Ry��������2q�Z�e�sT#�w��pr�c_��Q�>�=�3fL���x=;8~Tx�:?\�7��,��#�@F	�����X�L���Y�Wt�	\�S''� �I�,V�ws�I�Yv�
j��fX��ϻ]Y_q�W?�)��-�ɔ��ʗEAR���M��p�P�l���b<�w`�%J���Qngʓ㔶|a+3�Շ��/�����C.G1�S����JurU\�(&�K���!��(UhX��JKn޸�?F9w:�6�٦`Q	S���v�!� ��e�*e�q�+�]�K6��7�.��>pN�
+�|�TꋀQA�<�����TT�s/�fK!]T����X4ܓ[��i���H�e!�k�P��
۶��4.�đ�w����8M�9c�Z)"�iQ�/�KJZ�M*?YtNddEO�}�l�a23#[ �N��N�圪e9#U������W�����S�
E�-qD�T��7� PI��A+��h�4:;�:�.)2����0�1�Y9͸���=̻��5��-V�ϵ,J[=  	P�$+ï��*},#3Q���bJ4|��I��=��0ƌ^JV�o���"�dM[vlV� ٸJp<����Ju�URz���� �����8P��h���5%׺��U�DL9Q̖۟ĵ�gN�zj����B4us��?�)�5=$�kU3���l~�gd-f�=����NN���ht�l��U��b\�i��>Y��������W Y\��e�&A��%طU�ևr���ǆ��ݦ�Q��z�,=R��'�H>qO�.[5�v[O~bF6��p0��s����w���2lՁ"�t��]R�J�6�i8h��x�֐Qr���4�٘4vCN^��i\
�񔶉OS�>�]a�0�fT��8���s�Q�����jE��<܌��]�:��S*0]���.��NU|qA���(92%��5���!۞&Hx��k�t͕�s���UJ��4���q )�X7�3x�Z(pQ��EJ�c^�y��`p�y����7�<"��a��%�R��f0��DA0IML���3�a��u�-�d�v�t�`�����"��jH����n
A���J,6�q�o�_m��	�H��ͯb�����60-{=��JKi�tkb�%�{Ǻ�����J�t\?�� x�A�Ջ|�Z��*g��C~��������d��8��C�1��J�\��S��\�D���X�H|1Wma�_5�!�YM`�S�h���h��@v��S1+%��1��`�SNs��'����U��0S���wVӲ�R��f
�ᰠs@k�6lps6�ą�j�:�Uƻ�V6��+DX�z�xnoVb4��gKY���}�v#��b[�z�P�D__Y;�ZY��3h�M?�?���~Cs���0g�c��oa@�C�����T.��X#�6q�V�aI���}1辗2G��%@Y��U�C�Thw�"�D5?\��8�ŕu
�>�{�����{�z��s`�w�v����'�%J|b(s�"�-+�O�\�5��������2r��A���ϥNwC��N]�x��D�4�JD(k�`0�[֙3B�*��)!�*j���mO9��aL���U��}��@Ҝ�@���5���Ý��w4Ф1�S�)��6Ϭ�Js�!��	xJ�Z�̧�0+������+?��v����`m��$mS�iѫH���h?G�Ѽ�g�EVQe#�YO��Y5���]!�xhIϼ�TK�|h��pݰ��*c�����c@%�:v| ���g	:�kO���6�	T���7��p��j�3P���*�h=��^�Q�R~�ˀ}N�"�i�`�g�"D��*4kuךSտ`S�߰�_*�3҄ ���D�8�����F�}'n7kc��q�E�E�/�~��X�t��8@Z��J�>����@W[d�ھM�P�=)+2����=}�? 'HSc�Q���ǹ]V�]f$�=��U=We�Ʈ��Ja� A-)��0��<K��D�s%>��yٸ! |�y#.����І�#��qj�߬F�) PM#��M�B�y����qa�i��b�l�x�o��9\Ր��_�W��iSe�Ա���xj���>
�c�DW��W��H`̭k{F̐�S�c�ӡ�s���$������)����*�c.��}��T|z#&n~���\�a�٬,6���$I�Fr<~��J(�C�,��ƃǀ��{�+��u%L���'p��|��Q
"n,nj�U5�����ϓe������ѽs%5��F_u8`�Sj}�������iK�/���ڭ��؉�6�wx|ڻ�E�Rt�>xC����cU��ҘY���c��U�,pۤ��>Q���(�B�hC ���o�^I6&��B���$�귕0�M
ԁ%�kC��~W�NhZ�0Q�Km��0J|��,���T0汖�d��R�ۄ��c��c_��x��6i�,OV�h�������
\�����/8y�����H��Ʈ��H5a���%� �������ش?,YH�{��I�?�pr�
�N�a<�L쓴����-k���HTc8�m�lyb*`nmuñ��AB4l zlzL�OE|~�H�l�e8�ߝ���s/X�'����,�f��K)EVٟ�(�����`�?���Q�5l8[l<��J��6��`��s�O�!����o]A��0���-9u���]Z#"K��r)y��z[��3.�.�wG�C����c�sAh�aB�Z��nx�,���s)[ZR��s�A���q6~�6+~4\�/�]
B��6� P�&t�x'^�v�W���� cI��J#    ��T*yF�-7�c���=���|�?�՟���l���ז��/y�6�5�tr2h�`�5��<���� ~g�Z����n�Wwǧo�%Ц˴ǒѮ%�v�`A�ZŭC�HfI
��8�eE�kL��VAn:}D(� �ķm�F���R��l(������6��bt��4ݶ�E�J**-)���4������7��N��h3郙��H���#v�a�0U����#ya�`0��%�aL:��*`-ܪ���s�UƆ�yrK�����b����GX��'zp�g���[ݵ��ƃa���__�E�!��KBVq����nxՖ����5q@kg[g�*OW|�6tz8	E�Y�|�qH�0�u�bnu*k�rӥ�<ʱC����⺽ɇ�=}U��:T+S�o��}. �jUB��w�X�7�5�B�o��]�J��+͵�S><�Y'o����^�O��>q�&�%�+�8W�L^z��vD�оή����"�<`*/��d��=t]���b@Um���u ��ˌ�*�'�ŭ��a��N?s<���gT1��I}{�(x$6��*i�h�T�g��W�wm����ht�'=��Ũ��3��}߄���K�(k���1������v�i�6�Y�į�����3|	���F-tXSܨ��㪙溆�d6�~_	�$┾���*���z�i��f��  ���*�Č0.�1K#5bĩ��n�~��U�ƌ�,�:}r2���-�%�Lv.,W˶ѫ5m%-mm"Q��$��$��CVy(�6����NL��Խp�(�MhN�@���jT��IC}� G5�c��-.P_���Y)Et�썔���R�8�}�<�(o��K��Y:���� ��E���HͲ9�Ks��@�Ya���f�v�mʓל����6M�}�!%��K�-�0<��-0JN\Gl��:r���)^�J���%C�8x�@���D�vI�1%�pwJ�1�'�R�0��t�.��u�k�[�+]b�B�9	�y)�i2xZ��A�EH��&�+\o��[��|�Q�.$)��Q]�T,�q�ڱ̰~x�{�s�/,u�@$���o�a�cQ��|�H���&І�-�!~��k���sK��-=�l@�� �66#::�}D��be���9m+ߟm/ŝ�膇I���� 2�ʶ������(>��Jp� Y�¶	Z��U��n��ZZE@��ʥ���qMt�'#T]���m		{g��9t<a�M5�]�Ͻ0W�5;�������V�x5�����E�1���us4���A�R�x߭��.|Ꮪ�}v}Z Xz�Ǔ/7��z��u��H��3�(���~kE��^{�+�����L���J���-o��*�-,�&�Y�q֘�H���ס���	�}9��� @��x�`�3m4҉(�F��D��X�-la@��N�#�:�@ÃC33�&����f=H;s�'�y�:|�Tw���Z���6f"�yZ.�U��1�(h����yi"Ź�����qE�;�Be�=�h���!�����Hܒ���tO��|�<J�0���7�\[A�o�����Pkj�tp�T�~KM�c[�����K>د6ڟ�n�w�bE��ἜS�	�@۶����O��ڠ�3��V�x&0�͎��&fP�nN�\}�	��g��0��	#ݺ^d�>%Ʌ���ܤ��^[�na+���V�Y����Z����b˕r.\��ʧ�d��t>��B������*��Y�v�͏'P���ƕ����O�#V��k$.��Ts���U!��y�/�i�(Za�Q�2��y���K�RM4@�,N���-�coA���û/A�'ۚ�!�Pt?	�5Qَs�R�]�(O��$�@M�14��V%�g*�خ~r�sJ�! 
T^���;�:��7��(�2���_"���5f���T�F5�Y�����ጅ��[v���2���U2��c���CST�C����yę%��Wɶ��jB��\ [i�ݪ��5n��dK`�*PY:F�B����ZЧ���{��d*��ŊG�h3Y!�S��|�x��曟K��l��3��̸�mI}ߴ�c�r�o�z'��9���v�.x"������b�a�v�E�p��]lѨ�;Jт��
;�1�e��t��=�p�]��1��p�Gw5S��Xc��˸�Rb3y��a�:�2s��z�x�Lj��a�0�Ib��N�zg@v���1�� �`i�e�6�&"̎��.����1�'95d	����&+�M-����l�sغ�PE��l.�F�$��}�F��8�m�Y��Sa�$�f�����MYJ� ��s؄8n`d�ä��/�&F�?�I|�|jU��N���C%����?uzo-� ��F��2��"�ӧ�y��>�� Яw�������l��{3
;���c ߗ$�7!0L�V��l���b�O�NFz:vyi*��o�ټ���%���gT5�{��('�"�n>�֟m}�������������������X5��^���Ro4(.ċ����d1hR�����ML�S~d޿_:~5�Ɣ�ٽ|�
c��^�	��v��QGt��U��pd)rG���=Lգܯ��${*��C��8���
yk�I[:靯(��;���db3�\�d�1�VN-֤��1�S{�'���ŏ��������� J��b%��O��&���e䳠W�bK��0�L䋫��g͌P�ZL��X �� �u���o^ed�7���a�̧������=��F
�&�{U:B�rLN7IA�i^JF�if�H�'���6����4��K���#��WV;�BP\O�x�Ӿ�He���C�	ko�3L1CKL��5�)�i"4��k'#�`Sz��.mf�'ѥ1os�Yԑ �����Ι&;�6u+��N��bQq��J�íǋ٧3�Dz�Э�v��2|��{Ӈ��Ɵ�2����`��-E��j/�h4hm/�8����D���[����h���Ҵ������_�0LW��z'�e��c��F+�p� ��Q�I̤�lJJ8���aٚ\g����������������=�p��Y�'%S�r��\b�9�^/�Z��ֻ��YO��q��]\�z�ܱI�	@S�'c���)_��2?XD^t�L�L�/�	$ݱ�C�4��7�2l1ܫ"�,���F�J:z�|S����y�G�����|�[��v��B����\��V�j<K<��W���NF�O��:yw�{��!�u�ܟG}T�K�1�v�n�-_ ��C2W%�{[�F�%Zb�����͜�l{U/3Á�O}^������Y��O���pT.�e�<4�/��뎎GD@[����K`�^�9��+�ѮC����39iw�r����	AF�=H�>�f�������x?�4�f킀 ��|>���N�ަ-2U�׬6��|$�,"#w�9����|5x���Dw�7Ap���8]D�42���=�!�2s쬪L�k���L��sl9,1
	�� \�v��
ue9�e�{b�䤨���,�yG��#'7kLU�k=aEe��GX��y�ä+*�N���Oz�*j�.�x��0cV<2��U���b�������b�z�v9��"p�)�\�1�j[�<�d�(x]��5'[�zB>"G/,h�*�Nwz��`�=Ad0���	���t�^I�-��U����=�x��$�!	�p
��՜(��=��Oe#�l�%9�� ;y�h4�R$)(��&HN��������<U�}&����!�ܟ��|��p5}���j������c/H�
}���Z� @l||��9CI�	X�!.|��τ����쥲�����jI���6�Y�}����_�.1I�c�����!�U���?"�f�g��On���R]z�wd�����)�SR�ȭбfCzhk�]ۧI9o��������7Md%T��6�{�����ٕ�Y?�	���d�v���
�gf]��)(�������K[�F����N�+�_��t���krB|Kc6�br�!����G    k_�FZ����\*l�Ra�{+{]�y/�Ԯ�=�-�+����J�����n�L�����n�!��t��)(/�O]� ���3W�Q�R�Y�B�o�j��$�����=����hwj{9�w�M���{|�H����c���T=Z�VEڝ�E�� R�D�mC���zz��0Mbᐵ����5	�)���,�^ �!���!b��� v1QEąEy�2��n��l��O=����{?䷼_�m��l�j��J��Ԙ��	��53�\���#r���Ԫ�ڶ8��}�.���j0Y�6�FQ�Z�2�B;���,U#j���@�k�!�6NtԻ�m�v�T�u�A��;�2[�2�F�v#yD]{�B�vq�	�+���z[�����Ю5���S.ڳaL�a�C��PO\�5\/�^i������"y=j�x=�����
�}�Z�����������@hǧ��E�5KZ�:�2Ne��*�g��C��}C�%��n���r�+��T��D�S���4�0�e��YT��4�Qܑ���2z��՚��K��x`��:P�T� L����oF�179aD�-y�L"���J^��k(���(t"�p��y���ʻ�_�q���.��ϲ�)��U!���:1U�����CWsB.�#���]֊�u�j�c���^l���-�5��D���7�G�(+s�bD�Mp�Kߗ�&�n,��76�q���K��O��u(+c��{��8Vh����"�.�lҔy���9g���������.�����;w��>!�ju7�am�q�$$A���2��9����:�����[��b��3i�����z�m�G8�������m�Z}��6<$�8J|��n$�[�֫�C����t�8�C�j"u�"�ñ�9���{t�i�	+����!�"؇��Q�f҈ؓ'M'�q��U�L4�v1	�+�ց��^ZGbq)Ս��2�#�٭jA��QVʙbeʡ��i`��8Kb�E�&R��GR-�������xx�5�P�n�Ʌ��Z�n5ۺ0땅�ЮQ���Q�c�Pc���p3�:ju�ҳ�����!l��T�x��Z��ԏV�����������s����Y�_c9�-wqH�L|6&�	��E(��-x�-�UݬM����ޅM�h $��A8�m���Ie���pp�&��8��b�9<,X�sw ;g��tH�wsK��ɑ�>:�Q��߄��@�C�FH��I��u)���f���/��0Q���/ҥ�a��ԷVh9 tT�<6k~K�w��N�BD5��J[��6��2.8߷���O27��FNu"JRFI���2Y$>\������%TN��5C�p�#��%.�G���ߥ�����q��T�54�u8�}��P�,˔(|*��e�)65fj{_r�����[OEb�<-� �g����	t��p �~��v��5�4��b,�+Uj	U����2j���6Q�{ʪ�j��ڊP���
~ ��� �8����ܞ��1TaK��oeH���j?���q�C|�=%n�x�b-�����&s~�i�)��`:�4����v�z��?�$�r�!"_����$&[iq����g��tP��k���O�0�?��b����p�iS�Q����_�mL��>ǵ�Cfm�OON��i����U�V�	)�+�)�5nMlƯp�>�}B8��
$�E�j~���';}�82n�f�?�OK�U�p��4��r�~i���_�s����l9�͏��V��1� �7�%�ȇUvOq7����n�fވэ.P�++*=%ʶd�}�p���-���R2TQ��ȲS$.��|x�P��QD��@�G�%D��[�	�s����b��30��>�NO�����`?���3۔G@��z��`xۖ��k��wA*<��vQ��ڼ�(^��>����w��h��!0�G�S�lx+�є��ʛu`66���u�(k�K
s�Wϱ�1��y��ƽ���j'=��y�q�	c���uq�k��|�1�����;�-���0�j����7��[�h�s"��?�U�Yь�(���G���Q�6	�6%�^���$gn��0O��#�Sl۲�Kݙ����0A>��ȗ@��đa�3�g��Bt:s!��Y�?�]"��KyE�^ҽ��GM��ǥ�v'�*�^\��C*�)	]q�m*r��5�m� ����Ч.��缐��HH)HN��M
�=�}F��mcZ�0��֥�E���,}kg���EMz�&՟�\��t1:�؆ i�d��ʡ߂�ȎGN� �I��Gr�5��q9^�M훳)���Jj�<o���cb۔����tV��8k�ia���C�W����K�%��B;���ֱu(���s|f�M?(T�������N�Y�q�l�Y��d*nS�;5k�Z����('��:Rܭ��=�cA�+�[����V�1�l´�Z�(#��p��Z0���f��j_ ��o���q7 ��ɕ��kq�����&��pwqj0E崝�DP��?�OH��Z�ha�h��̭��r���힨�r�_�p£�r4���%��,å�FQ���^��v,!�E5Jѽz���:'��ʁ�ѣ����@`+��U�{��^�>G:hL� \x�~�b�Je�;വV�}�fZn�b�]����� hp���aϡ���a.<��<.9s��}�Z�H.�Wa��Oqؾ�.���˧)��=< �����	Q�0`�[Q��J�gĭ��Z9�=(�XۨzV���=�a ����q�IZC���V0�ʾe�����]��Ĭ*]dńCBD4A�Ax[�@P��G��Y�NX��cá��M�*Z�ݷ�Ƕq+߻�v�O�b�1΂(7g��\������n�K���^C��{���Z�ӍW��(0�mp". \���!�uxǁCw��ZW���x���!�$����ET�nN+T`Oٝ�h[�I��/�R�e�_/uvdУ҈!�Sa�H�AR g�'y�vx8�{��cK�uJ����Jר������@i������N�b'1�DRd9GR�%��	8[������tώ]�5A�4u|AEp?辡��$w�z9Q��\�0�o73������HL���T�')]q!��q�6l�2 ��'���HbA�Hp+ki��-I���p��.
�:uvk�6�6�����#�C\��A����hi�����0�4Љ(��5;
��UV��Nw����ƃF���M勌����p��SW<�A�N�T4r��,������P[Fh��Ȟ
�?�Sd�JH�y�#�R�L��ߊK�A�"EW�4s�ማg�4�O&�Qaؑ,{0�?��3t��u��ʩǝg��4p����w���C�z���J�S�$
$¦�D��B2�MưKX�HD�+~k/�n02؃Sa��|X�S��Ɲ��E��L����$w;��!��x<�+3��k��b�v�	�1��.v��vĢ�9�Y���k鿒Ơl��;U`:;���+i�|�@�WQ<uxX��ջ�>��Ҩ����~������c-'+~����:;�7V
\���)o�-j�(
��`fE�:8�#x���G̶���H���`�Yn
����)�m�?���.}�P������W��I���~��i�8~���k��ݚq�ɠ��gZŠ������s|�}g_�:OFOr�+�>�V�6w�RA�������'�Z�*&��;P��Y����-> _��Q
�S4�e�*<����tJ��u�DQ�B�I��t������Q�z�Pv�{��-^�o�mr`�[�}`� ۜ;����'Gݴ��=�1��k���Ȼ"p�53j�4N=�PθIj<b���!Y��W�Iܠ��F��mǻ��o�H�c��z�R����<�j*�<]�m|>\�P�u��$�Vv�{�sr��V�]'i&�Ӣx��y��(���Y��K�5����F�Y���ٚ��v� �pށ�a��{��N��y�F���Ƈ�3��<�+�";�5&)r�2���U��.�wBy�T)o<>"�7�g    3ɂ����ޝ�E��t8�vX#�-m��Q��=hm��wLK﹚#c�n���dg;!콵q}��{���l�\6�a��
�f�ir����yFϗ�<o�xM��&�����Y�D$B���W�>ȶJ�s;ubƌ��BYG�Itau��<���~t.�rh)���3�I��o2��W �(��m��.1��5J���,����-H�����e<W�6�A�������VQ���������'��N�^F������o���m��A]�S�נ�IN_�.I\ls�#��#�yn�Xr�fE!4{��N�X�f�݀��~ڝ��Y���)�\a��P����(fY7EV�����%�a.�潋X'��$+���Uf̣����z6��G\�H��Cm�~i3��9}B
�d����t��(��H��j�z	�L��#�:(7�i��QA��RE�3bVp%�l�"%��	B�P5FXb��Mzo8�1%�ڰO�}�ʸ�_�G��~��r�$E_m@�"A��I}�c!��^�y�a߁��L�P�������h!�#�:n��𙳙�r�#t��&��wg�c�؅"�l5��:a�l���%�O�etJvB��2~��(y
w_�[.ϫ����iV�kO�s���)j�+����^̢ϧt�$�<-׺��;�A�{H�q<��\�zsq�Ùݯ�!�y^߃��>�Qgm8��E!}^	Cb�*�0�Y�Ɩ%��
��"���l,�^�y��`p�y����7��`@����=��^���\pQ�[���G��C`V��>i�H��J����;�.��<A�nk�%��W��iG�r�M���5?x3�&�esc����SqaAק�N�q����nU%;�~@1^�j����MZz��y[�yZ�)���>�E�����m���~|��!ŰP����o���!ͬڍ��N��R�ɟ&��J,�����"KQ*�b�i�2�u>-��ʢ�-uy�["4w���j
i�����q_t*�GG�n,�>��CG ������l�f)͇G����L}�;��N`R>4H�]�~g[*
f����q9�<^F�6N�mcQ�p!��TH�/h\��$~��Jm�sg��o-�JH;X��"�ب7|dqL�3i���C�,_�å媂&݊9a�	(Z�(��J$5��Fݿ�c3�l�зi���_h8	�T�:4{�?M��ޞ�HM߻v�������t�c�h��U�
Y�;o��s�gEށ�	��½����T� "/X��:��n͝�,��|��T�8�yv�����̰o��i�7%�X?
�KI��ig�'���Gj������LWu	.ް5���G��{j���(�kE2�g8�F��$�A΂l;T^h�F	֋\`��Ձ�g�Ș��q>t�uHIK8��8z����R��=�6��2�ӆ�Ƃ�SE���e;���8@���;�J���R�����&�I&�_+�|!]<�GW�I�Ƭ��.���	�ҩ�<��9���_�⅂�CԼ��+���2����T:�R&[���-ĭ�L�z�ZIX@Ns��mu.�l�����#�5==�����Z�W�&����E�����.��WA�u�}�`dj�f0���P
GR�I/�=�b4���ұ�Ei�)܏��S��O����� ����m��k�YP[f��I@W�>�&gs�"	�V.ÀM>���a�z�d�dտ�44Y����C�(�oY6C���B}�U�ɠ�q��m+.*T��N�颌�b2h�a)�3n���sā��*�
��`�Q�`�D��_� ��J~ ��۳������B�69w�B{`yD��QZ��4!r>���qi����l��}[U��I�,1��Y���bQ��]#�)	ph|'��X��(}�kb{�
�l�W��.��f*XR�
����h�M4B���!�pp}�t��=�4�i&N�MdgZSC�]4pt�KRa��`I�i)=�Y��	������������rv�Q��͂!�^?�0��&Kf��r��#�<�{mK]9�9x#�цq�a��+P��Y�t3�� ����Y����D#w2l�u�-9l��:���]n�}�4X�)%�y2lp�0��ԕ 9K4B�0��f�dxM��>/{���U.�8��7v��,�	�C�NF�dxO�e��d4h�h�h��Vӗ�]_�����i�Ѩ�h� ��=c8�$�x}���Hy��k��gp�!�bD>!�G���WG݁Eϳ�� -��ă1A�����ւ�"*)�j�wZ�$Z��Jv��+�!LFG�������:5���O(�-�B�ߦ̈,tȠ*�F�����#��u��0�A�^zu�MKʈ-Ը�H���ҟ�\���\�A6��1�tn,�����vzKV��g��9$w�MT�ϪZ�Qc��[Vd�k�ö�&l��=8�fX㹤:c���-ܗ4�$]gM�娸���)�W$B̸$����yP�,�y)O�d0��?�?����ͷ�,�����T�,&�g��1W��l�;�BG1���c��v��Nm�|��#�_z��4�	#!q�o��*%ќ�QCd!���Vj�n$�/U�JP�U�44Ը{(�<�ԅ*��}���I�Q�46GP���x+��V�H+E_�QO�Is�m�~���1\�HJ��ȟ$��a���� �1�"y�jG��K��:G�뚜�H�=��G������~3�S�ߤ]bg��d|�\M��7�Z\��L��ӆ��9�O�{����d�-O2��4S�)?�y
���sҪy��m��d�jwZ���	�u�nP�.0�.�,N�;��01����y������y�E+�Z�	B��InĮ�ahL=���st(���~�g��\*����ks�������_����[�����y���˷-����x��3]tV���INp��o�ť���y�n�o篯�����.o������N�� (�~�e���~��Z��wߟ�������!�NW�!;޷ۦ�ڗzKf�#n��O�p��R.|烴��{���aUm~A(.��Ay9ι�)X��q��Ƥ������[�f"e ��e�f���RQd�R�
Wဵ	���ցL4�ㆮ� 7��4����ݞ�2A0��+��me4��;�P���<me��ўg��<�p�/Oq��&�/��C4�]$�"�ˀ9�J�w��yaг86�O+,��S����3V�_�2H)�#�=���Ҽ��Q�׺�[�X�$��&b�4�WH����ѣD�Q��S�g|��J��Af���@�����Jˣ�n�қ��-e|)u���G8k�@�K<0Wâ����X�[�h�s�ߞ��F�O�B�%��{t�%s��3�q����e�W7��}�؍��|��Y� \$m }�Ƥ����J\�K�j�T�Vq��l�2磣G������a�^�y�;ߤ
��>h,��My�M�]hPG��o�;��K}*`!�������-�G�s�6���Gk�f���_���f����{��)��[\PQ*��,ʥ`����ƭ��/\��E�)����չ��d��@��'e�����l��<��n�u�ګ5 �:۠�:/Y���׍zg]��m��(�5���]���E=����Z��6a���k���H�fia
��4c�a�W�s��(7�r�M�o6
j����u�/9�	Ng���BL���!��'	�t ��x'��3yJ�~)f��+�q��?Æ��_A�� �r��ES���Մ���b�Y�lOb�P8�j,?e�8�m-w5�a$+���A�K����.�Baj���#`���9H���G�_�^\ϳ=ɍs��r>a}�v��F�_�Qc��݅O��щ;��;D�m`y�;2(������˷�P��^dPe2g?�{squ���Ϳ�_��t� R&�L���]����~+ �+��(�5���|�+�����s���A�~�yJ�0�h�����_+q>�V0vPlQw��ᰥ!ʮa[w ����~�۱W���9P�G�9�jH   1� �!�r6��Ǡ�����=�Os�-1h�L��Ņ��U�9S��e�#����t{Gx��-�����-~��u��ysb�3�����BW���(�^l�����y�ͯ5<�k�Á���}EB3i&����Ρ���Ƈ�ov.F���Y�͉�����?%h�<��s��Y�`��{���e?�*���f������H����؇Pu�G�G��������[Ug��^��f��I���0C�t�ő�OZb�@�?fS���^~Կ���q<�q=|Z�?�����9��𑷠I�t&`�t��M�~��-]G�/R��?���#єrۡZ.]��xޖ�g?-N%V4�7{����=�Y�Kj����Fa�(��J����:�H�h!r�X��7�
�ה
c�:�с�����*~X�*�
�.�J��Br����ܩ��IL��a��r���0�4l�O�	�ņ��`5��B�r��5�9�%�0��ޒ���=ᛪ�l�X�Qk�����yk���&Jq`B9���R���l�]��5OC�b��M���e�}��7SE��\������������<��S��E.�r�.ғ�]��ms�$DN�{l�T����n�"Í�5 h��/���ݟU�}DV�?!�1^M�(�;/���T�O�	��BBU�$�b���_(��J�+��.Jj3���o���&R~�<��킕r"Q��aO���x�vf���J:be!�xj`@�|��ʘ�����5�z�M�o�%����ݟ�Hb��ַh�q�Oi���xv�gk^�]u��zDg��=>�����,���.c���7����L����f�瞓_�
�&UFC���4PIt���xL`��O�oº�.']�
=�����H���aϡC����!"g�	����
~���Si��"qo�Sη�
��4F*(�?��B�@�1�}'	�4qڰ�ͧ1�(�`EO/k�������lS��-w}]"	�+�*k瘢n������_�>�����������P�i�� ��9�T�a}5ťI���j
AN� !�`�asY��;P
>%\J@��w����i�!g7�
�/��^C�ڮ}8M�� ���@X���[MvݯnAr&ܚ3��m>����E;WP��X�������;x�qo���&��b��2,Q��ȾS�]׏���G���q��ޱ�/���xg'���ǟVY�Z�%�W��e���h�'�NKX��Vjߖ1�	o~.�x��ʹz�n�L�
Jk8mv{J�����PAbT ��/$dN�_->S\󂛥����L�"���t�~L����f�b4B�?��Њ<�i"��f��2�Z�c-��E�0j84:��΃�&�l�o�SS������#�ѽ�W<_�a�y��Z��Wi�#�w����#sQ$իҸ���͞�ajlx�E����m;��XNl��s�y{��]�fv��.#-�.�|��Bw-0�L��5w�@f������^�n����+��%7r�Zo(�7td��NO��4$6Q$)����b*1��yx��1�O
�ў1���?���TQ      �   ]   x�3�t,*�S0��2�tN�KN�ILI�2�(J-K���9��srRK�L9CJ��@
�8�2�2K3K@<s��<��������Լ�|�=... ��      �      x��\Ks�Ɩ^ÿ����4ċ�W�l�ʎIQi��i�-)�� �ʺ����ҋl&�,�l�s�n�(J�=�
2��q�w�}��;ǹ*�|)�t��\L�P�B%���j�b��{<M��pޫ���S)5U�y���q��Ps���MZ,�t��wj�r9=����O�Y=q��tO�Q�\�D�qO�.���q��p�����m^,n��.�zt����=9a ~�e����6��̓����s�N��T�@��ց����I�"i�3�Ty*7� ���߈7j��K�bP�X*V8���ǭ�[��h��$��Xi�`(=�7.�N�h!Q*�y/�����i�Y^��j2K紖r��*e�"MN���b��:}���w<��j����.���B���C�/'
Α��ӌ�0�f�\��e�\Me5�d�y����l!'y6���fmJ)���'rbqL�r:UIZ?2m��d���2 m}�� �<~��T=/���S�!�b�=�G�>`�z�zX������W���y��I�����nVv̂ ��c~�U�R�$cR����(���e�����I�� x�^�����Xd7+-&�D,�{Q�R�A|Em����i�i�Q�P$�DM%�[��Ͷ���X�+�#]M�����	?a���~��%���=G�ZS�ъs5�P҂�o����}1Y�)K��Ws���"�B'��&����`yB�����>ME�\����
���.A{�� 2�a��8����H���. `NaC���(��xz�<��Dn����ǯ���5��9ϲ��Q[w�b�I���)��7��P���y�R������N�'bM�u�y�ć�E���R^�E��<�Z.t�q�b���Su��[�g�b��x��ye+
�>��+Em�20�\���\�Yߙ6��2.��y��o!�[�T`�0^�n�P�	4x
 E+��
��h	���4m@	�W�mx ;<L8܄���K�ٰ�ee�ʶ��kc]+�J��-l�|ظ�e]�u�w�[�}�g��h�(��Ӭ��J�@��ܲ_�����������i�J����X-��m�:�q���r)28n��v������W�u���d�#*����	(�c`9:|1n��w�J
���t{XeĦqx{����y��}��tHJ�/��(�-����?-�Er�w��)����� V�!�sI�Rפ$w,�5�Xn�vf���rz*�3U���R��1Jy��dL�t[���"g�^��ѥA�(&�g� �]�A>��o!�Sͯ�D>���)��X����l�\�y�J�wq%Se�d	ϩ��z�"���9#-9/�SB�(d�gS����lY؁��v����pf���(��Uu�V��}W� 3�2���F���Yd��9�iĵ�����}�}�(�H9gm��>�Sb}b�~����J����@�o?Վݭx�\L�r��'.�+�ߋs�W]`��G/kNh2���%�r�^��҆�K1���п]K�;�Ff:�Y��`n~k��מg��1ρ�K�ʘb���FX���$-21�᥈<\�P���~��y ^�šؚa�c�.�ۖW��f����A�08D�v)J�XȂGD*�-��S�uqf��j���Q�����!�>�7苯U���'��q��
4C�w	�w��2]��}W��ӆg����7��*2�`$ڛN�CRm"�+�����Ϊ?j��=�>�|�����^�NJ����R_�91#���4M?e�j����{Y��t�z��oT�^{���?�=`�'H|��i�#�l��	[��+#]|tW����z���cf������������a}l�,l;t��W\�i5��f�N��J}Z({P�yη���۳��^�����O��|k~�/	똄]w�Pa��W��=��|ܒ��Q�zX�lu�H�V׈�U"o�e�H��=�f������}���9�\RFw'�dsqB�Ie8���G(����������맠*��:d4�hqC��@ӡ��W�ECoK
vV;�L���l�q+� z'�����Z	�f������굽�k˟���$Y����Y9�}~��:�%҅D��y�\�2̮i�4�c�h��ZD�v�ݕo²u����G�wW*^����%��4%�fmJڭUH[�
dC2���%��t���<�w�����߯'�����*Y/v�zX��Z��|�Rc���Gb	��k^�C��#h-�O&���{zMčU9�.�Ih���]�)dA�CS-�'j����!/����-�s{Ud�s�����ZQ�������K�e�ة���V�sFV&d��gl�LRPf���X-rUTt]f3�La���Z�t�dR�Z�W�c1&��
�0�hHH��V��0�&Y^ׁ�W�[Q��6h�"s#�f����-q]��+�ca
FI��}+3zCr��*�!�js�j7��э\Z~}>nDg�|�v�|�%"��N܏HR���Y������r�3���8�ך	�?Y7[J�X϶(Wjk�au�F�����#���,nQ?hƹ~lq��|�!���i�d���c��p�a`��ոT+h�f�]<a���
�ޏz��&�x�m�#{|:�.~j�`�}�ҧ�G�k;Q4��"��8���$���w�<I���&j��`G�F�ίR���� n�)�=��xK��s_O�����N��[Ux[�mCr-���#����g�Boc��b��6�\�fܷ��$+�a9'	?/�=_hrR#߂�_�T�[�������n,�o1�T�r��u�.Z=�N��nU�r����Z��"wd[�Ǭ����Tus5���Ҝ rt�t@��P,�乼$O���6`��[/�,No��:�m��M��7B:짝=���хU6�1�Z�1ב��s|��%����\�m����V�X���vN/WE�.��_�ؕ|��JwЬ�&BT�h��	�
��F�]�FCӽ���J��s}��뇛�ͽ���i�Q|Շ�ǖ�Cf��{���A$o��b�kq�^{0����ΩŞ�R��_y���W�YF���Q�Q ��~�O\~�Es
���ptyD���iw�C+{�ذ=�<�2C���_�Vf�s��r�m#S�	zL�O���_��Q��Ȩl��/�ȣ�%�jB0�ֺ�))�Ҷ��|*���&�����&�N	�68�����8A��\�N�ՙ�J>�U�Z��3��)���pU��G�7T�?AQ�����e��R|�	9~�8��)O���c���L���U)�2[gQo��.��9(�*S���~�ݰI����b�q���P[�q�;=7M�)��D^���]N]��&�G䑢-�b���U�Q��u�������\]�\�e�GVS9�j�7Uk/�q~���^[Bk뉳���㣃~�;8~�^��}�l�ÿ�Y��\|9ZK�����{�'�Ύx����LO��^� ����CqV�W�^`9�1�����/����U�=��S�'���6fP�	=zؿ��� <rv1ҏl��؆��h�y÷�]���@۞J��!�jώl�o�uvc(8�[�R��iSb��R����U�XA�ҋ��}�:��p�Y}L{�~O�� 8���m�s,]���E���1���÷��|>�+Z\��vtQOg��y�~9y���$[��u�����N�$KV3�H>@6�� �M��o��Դ6ԕ��}��mf�mYn�t��öh5A6���A�w,D��#վ�� ~��� �{�g��^����w�o�d�)��]��h�O)m?��H�L��"��]J^_<)K��
� 6	�[ u��쐋���|�7׼������޶�TgB`��m)��2^Q�a�THT#|8��cڦ��� ���T^���=��͛����؏=��P��I�v��z��=e�n��pM݃�Q횞*#`;����Kb�z�.�T�D�����T3�oZ�&驈6��ӯ�Д=�B09/R
]��|�a0T��Y�M�Ƕ/e�
{�={���S+��ԷދP�u��vҎhx�1���6��M�܋�v	1� L  F�ڝw���R+X#��v�� ����<�O/�-hE���1iy�%&U�	~��K0����,+i�t���L���P<�4�S3�Y�O����ɪF*�W#�Sw��|�	��
۫��,@�,<���zr>�N��rt�Z�Cٽg��N���+���Ԥ
�)�SU%�����d�Rˇ�����غ��G�"b��xî>�M�_��@]��NGq�PUY�.8�)ƕsa��]�8w������~(��k�\6E��SZ���H��_!��V{����_޾~U�W�8!�s�Z�ҥ2f��d�X�0��5��p�9��殇]'$#���/�A8�      �   :  x��UI�]![?��%�?G�eQ�l�K��y�i�dz�c#�n��iWZ��� +FL���^�^9yO�f�L�!g�	ޫ1�V*.h�>NZD0̪bG��hvj��3A�d�Z�e5��*�R�+[
�,��3�#&��@��$ʹ^�`|�\ �X'�H�p渥;56�;E6������m:6�"!�sl�B��^y�|T�X�����]ki#��o�6v�7CO`g�>��z��xj!��yK���|��i������A7�\0%1'�$���NB]g�I���k�	d����RX>�o@�	VR0'��$��u,6�j�M&�E���ī��Zwb����D���F��8�s^�¨[��3�o<��n9o�1�vƳQgj�N�t
'D��N6��0�BBI}Z�tO�������$�Z���^Χ�W��#G�WC}��έ��;���ߥ�Ȟ$��5�QF���H��2��y{���w����E������B�/�jQ�ݾ�t��G;��|����5�t��������?�M�F�P��������\�`�?� k~Zk��)E�      �   r   x�347�ts�s�s��t��vuvTpu	r�r�LK��+NMNtHLJN�L�,I��˭�455635�071�4�247���vU�	��pr�RPv�s�p��k����%Ȁ=... $�      �   �  x���Kn9���)�!X�b�����F�;F��H�fN?E��Ď��%�B~���_�;��c;�_6�u?}]���q?����r�O��t\���y3=.��f�6���pXN�t��K._2M�g�9�d���%�=��?,��~:.�u��त잱ԓ4�,��Sa�
��͊��Ȳ�Փ2�,���ĉD�P��f���� 9ד6�,�d lX�>.y��Ȟ[��qp���pa��r�#�a=Q8�	��a(]�̩�pn9
�j�L����f���� �3�sv�8�˿0����Fo?X��IX�3��äT�P[>�aB�{F�V��C��������1LK�P��?��F��\_�?��8ڢ$+=���|S��� )����B����Ѡ���D��>#&����>j,q.X�RV���<�N1�f��Zش�>jvɔ�̹ή��g@^.�a_����e=}���l7�q�]���cx�`���%9��BF�=�HI5X���︤�H	��gip���|O� ҼE��*jGYk'" �f�mgz�9�(tm��=��z�a$M���C�\R�\Ҧ*D�tn�j�.��t�<���������G��Ai-�l	c�C��\�
ME%�ë{�zԞ!���S-��v?���?���*5�D�S�_�"	m�\�uJ*7����:u��t�7�ZY��Bjv1\ϊ���W������,ņ���1݆k�O]��g�?�����.�R�s�~�� �Y�UA�a���ӛq%I�׈�m�R�h!*�q~aO�G��+�w7�Rpg:r�YRh^��\���Q"�{L�"w>}B��׎�M���$��Z��[�۾�9)j�V~��O��)涉�������"��ܶ霙_�s��2�q�;ݓjH�6A�o�][��{��[aI��7�ƚ�'��kB?Q��a2sꛡ����rxZ?� z4"�>����C�51���A�A9�W�U4�*r���}&o���ϰ�#u'�V�����c$��2M��X"T�R�F����|m�_j��k��:jJt��B)S����".����\'N��ܲ����P��B�سZ4�U*}��#�k�Q�"�>�MQ����6j��&��6�h/���Z�>N��тB�5CUNV#��>���g?5d:�i8��v�(l�J��%u��ġ�$�;��KjϞA��.Q>�E;�VE�HBx���Z�=�R��O+���G=�uz��XH�i��;������      �   Q   x��K
�0�uz��N�&� Z�Mr�ˁ�d����PN3�w4X�;W�~X�6�a��Q�'`���dz*��6|L)�4o�      �       x�35�4D@.Sc e��B�@�+F��� ij�      �   �   x�mNA�0<o_���Ɗ{ 1@ z�R�1C	(�w�I�If'��Tp+ �K�8"�<�ty��4�t�O��E�m��8��7}j"�iH��+�[������
���8��s��xk�0�mmx�zn�Dmww�af35c7�|ZQ	j�1r���mcn;�      �      x������ � �      �      x�345�4����� 
��      �   1   x�3��/H-JL�/�2�tL����,.���9�R����rR�b���� b��      �      x������ � �      �      x������ � �      �      x������ � �      �     x�m�An�0��+��$Eɔ�ҋD�i�4�8@��*���fg���gy؏c�^m+m�9t(���a�8�����h˲ T�, 9��"L�yt�m�rik������^��:����K䀽D�爣�@�g	:VjZ��V�JCƒ��2��λ�JW�n��}�*�r���="�Ly��H��7#Ӡ�RW���$3��I���gp7ێ����a��G���La�n��hb�I�FK[j�#�4�4��1z��R^(      �   �   x�UPKj�@]�O��u��2�E�H�
��ٌ�ј���wʢ��X���xz�j��0QH����!���g�L[K���j��M>�ãۨ��#t�A��ᄝѨ��F��H�r-�F-�#����=:c
�V���#��,����!SklXϧ�������=G%qB[�.io���K�ˣz�V8�^z�3C@NET_��a��"}���>�i�GIǂ��81D����yiס-��XUU��yx1     