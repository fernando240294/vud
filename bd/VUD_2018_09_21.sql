/*
PostgreSQL Backup
Database: prueba/public
Backup Time: 2018-09-21 18:47:12
*/

DROP SEQUENCE IF EXISTS "public"."ROLE_id_seq";
DROP SEQUENCE IF EXISTS "public"."USERS_Id_seq";
DROP SEQUENCE IF EXISTS "public"."address_id_seq";
DROP SEQUENCE IF EXISTS "public"."cat_area_id_seq";
DROP SEQUENCE IF EXISTS "public"."cat_modality_id_seq";
DROP SEQUENCE IF EXISTS "public"."cat_requeriment_id_seq";
DROP SEQUENCE IF EXISTS "public"."cat_status_id_seq";
DROP SEQUENCE IF EXISTS "public"."cat_transact_id_seq";
DROP SEQUENCE IF EXISTS "public"."interested_id_seq";
DROP SEQUENCE IF EXISTS "public"."log_id_seq";
DROP SEQUENCE IF EXISTS "public"."modules_id_seq";
DROP SEQUENCE IF EXISTS "public"."permission_id_seq";
DROP SEQUENCE IF EXISTS "public"."request_id_seq";
DROP SEQUENCE IF EXISTS "public"."tracing_dictum_id_seq";
DROP SEQUENCE IF EXISTS "public"."tracing_turn_id_seq";
DROP SEQUENCE IF EXISTS "public"."vulnerable_group_id_seq";
DROP TABLE IF EXISTS "public"."address";
DROP TABLE IF EXISTS "public"."cat_area";
DROP TABLE IF EXISTS "public"."cat_modality";
DROP TABLE IF EXISTS "public"."cat_modality_requirement";
DROP TABLE IF EXISTS "public"."cat_requirement";
DROP TABLE IF EXISTS "public"."cat_status";
DROP TABLE IF EXISTS "public"."cat_transact";
DROP TABLE IF EXISTS "public"."cat_transact_requirement";
DROP TABLE IF EXISTS "public"."interested";
DROP TABLE IF EXISTS "public"."log";
DROP TABLE IF EXISTS "public"."modules";
DROP TABLE IF EXISTS "public"."permission";
DROP TABLE IF EXISTS "public"."request";
DROP TABLE IF EXISTS "public"."request_audit";
DROP TABLE IF EXISTS "public"."request_requirement";
DROP TABLE IF EXISTS "public"."role";
DROP TABLE IF EXISTS "public"."tracing_dictum";
DROP TABLE IF EXISTS "public"."tracing_turn";
DROP TABLE IF EXISTS "public"."user_audit";
DROP TABLE IF EXISTS "public"."users";
DROP TABLE IF EXISTS "public"."vulnerable_group";
DROP FUNCTION IF EXISTS "public"."user_auth(_user_name varchar, _password varchar)";
DROP FUNCTION IF EXISTS "public"."vud_activate_request(_id_request int4, _status int4, _last_status int4)";
DROP FUNCTION IF EXISTS "public"."vud_area_record()";
DROP FUNCTION IF EXISTS "public"."vud_create_dictum_request(_date_receives_dictum date, _folio_dictum varchar, _answer_dictum int4, _citizen_dictum date, _id_request int4)";
DROP FUNCTION IF EXISTS "public"."vud_create_modality(OUT code_modality int4, _code int4, _description varchar, _legal_foundation varchar, _id_transact int4, _days int4, _type_days bpchar)";
DROP FUNCTION IF EXISTS "public"."vud_create_request_requeriment(_folio varchar, _id_requeriment int4)";
DROP FUNCTION IF EXISTS "public"."vud_create_requirement(_description varchar)";
DROP FUNCTION IF EXISTS "public"."vud_create_settlement(_id_request int4, _settlement_date date, _settlement_number varchar)";
DROP FUNCTION IF EXISTS "public"."vud_create_turn_request(_id_request int4, _date_turn date, _review_area int4, _observations varchar, _id_status int4)";
DROP FUNCTION IF EXISTS "public"."vud_data_vulnerable_group()";
DROP FUNCTION IF EXISTS "public"."vud_delete_modality(_id int4)";
DROP FUNCTION IF EXISTS "public"."vud_delete_requirement(_id int4)";
DROP FUNCTION IF EXISTS "public"."vud_delete_transact(_id int4)";
DROP FUNCTION IF EXISTS "public"."vud_delete_user(_id_user int4)";
DROP FUNCTION IF EXISTS "public"."vud_eliminate_relation_modality(_id int4)";
DROP FUNCTION IF EXISTS "public"."vud_eliminate_relation_transact(_id_transact int4)";
DROP FUNCTION IF EXISTS "public"."vud_get_last_folio()";
DROP FUNCTION IF EXISTS "public"."vud_get_modality_requirements(_modality_code int4, OUT id int4, OUT description varchar)";
DROP FUNCTION IF EXISTS "public"."vud_get_request_requirement(_code int4, _type_request int4, OUT id int4, OUT description varchar)";
DROP FUNCTION IF EXISTS "public"."vud_get_transact_requirements(_transact_code int4, OUT id int4, OUT description varchar)";
DROP FUNCTION IF EXISTS "public"."vud_insert_transact(OUT _id_code int4, _code int4, _description varchar, _legal_foundation varchar, _days_transact int4, _type_days_transact bpchar)";
DROP FUNCTION IF EXISTS "public"."vud_insert_user(_user_name varchar, _employee_number int4, _description varchar, _rol int4, _user_register int4, _password varchar)";
DROP FUNCTION IF EXISTS "public"."vud_insert_user_permission(_id_user int4, _id_module int4, _read_permission int4, _write_permission int4, _delete_permission int4, _update_permission int4)";
DROP FUNCTION IF EXISTS "public"."vud_log_insert(_id_user int4, _description varchar)";
DROP FUNCTION IF EXISTS "public"."vud_modality_data(_code_modality int4, OUT id int4, OUT description varchar, OUT legal_foundation varchar)";
DROP FUNCTION IF EXISTS "public"."vud_modality_days(_code int4, OUT days int4, OUT type_days bpchar)";
DROP FUNCTION IF EXISTS "public"."vud_modality_record(OUT id int4, OUT code int4, OUT description varchar, OUT legal_foundation varchar, OUT id_transact int4, OUT code_transact int4, OUT description_transact varchar, OUT days int4, OUT type_days bpchar)";
DROP FUNCTION IF EXISTS "public"."vud_record_area_revisora(OUT id int4, OUT description varchar)";
DROP FUNCTION IF EXISTS "public"."vud_record_report(OUT folio varchar, OUT clave_tramite int4, OUT tramite varchar, OUT clave_modalidad int4, OUT modalidad varchar, OUT fecha_creacion text, OUT fecha_compromiso text)";
DROP FUNCTION IF EXISTS "public"."vud_record_request_requirement(_code int4, _type_request int4, OUT id int4, OUT description varchar)";
DROP FUNCTION IF EXISTS "public"."vud_record_request_requirement(_id_request int4, OUT id int4, OUT description varchar)";
DROP FUNCTION IF EXISTS "public"."vud_record_requirement()";
DROP FUNCTION IF EXISTS "public"."vud_record_transact(OUT id int4, OUT code int4, OUT description varchar, OUT legal_foundation varchar, OUT total_modality int8, OUT id_transact int4, OUT days int4, OUT type_days bpchar)";
DROP FUNCTION IF EXISTS "public"."vud_record_vulnerable_group(OUT id int4, OUT description varchar)";
DROP FUNCTION IF EXISTS "public"."vud_relation_modality_requirement(_id_modality int4, _id_requirement int4)";
DROP FUNCTION IF EXISTS "public"."vud_relation_transaction_requirement(_id_transact int4, _id_requirement int4)";
DROP FUNCTION IF EXISTS "public"."vud_request_address(_postal_code varchar, _colony varchar, _ext_number varchar, _int_number varchar, _street varchar, _delegation varchar)";
DROP FUNCTION IF EXISTS "public"."vud_request_data(_id_user int4, _folio varchar, _observations varchar, _id_status int4, _id_address int4, _id_interested int4, _id_transact int4, _id_vulnerable_group int4, _request_turn varchar, _type_work varchar, _means_request int4, _delivery int4, _date_commitment date, _id_modality int4)";
DROP FUNCTION IF EXISTS "public"."vud_request_interested(_name varchar, _last_name varchar, _e_mail varchar, _phone varchar, _type_person int4)";
DROP FUNCTION IF EXISTS "public"."vud_request_record(OUT id int4, OUT folio varchar, OUT id_user int4, OUT id_status int4, OUT description_status varchar, OUT date_creation text, OUT date_commitment text, OUT code int4, OUT description varchar, OUT modality_desc varchar, OUT id_turn int4, OUT id_dictum int4, OUT last_status int4, OUT settlement_number varchar, OUT id_address int4, OUT id_interested int4, OUT citizen_dictum date, OUT folio_dictum varchar)";
DROP FUNCTION IF EXISTS "public"."vud_role()";
DROP FUNCTION IF EXISTS "public"."vud_status_list()";
DROP FUNCTION IF EXISTS "public"."vud_transact_days(_code int4, OUT days int4, OUT type_days bpchar)";
DROP FUNCTION IF EXISTS "public"."vud_transact_record(OUT code int4, OUT description varchar)";
DROP FUNCTION IF EXISTS "public"."vud_transact_record_modality(_code int4)";
DROP FUNCTION IF EXISTS "public"."vud_transact_requeriment(_id_code int4, OUT id int4, OUT description varchar, OUT legal_foundation varchar)";
DROP FUNCTION IF EXISTS "public"."vud_update_citizen_dictum(_id_request int4, _date_citizen_dictum date)";
DROP FUNCTION IF EXISTS "public"."vud_update_correct(_id_request int4, _correct_date date)";
DROP FUNCTION IF EXISTS "public"."vud_update_data_address(_id_address int4, _postal_code varchar, _colony varchar, _ext_number varchar, _int_number varchar, _street varchar, _delegation varchar)";
DROP FUNCTION IF EXISTS "public"."vud_update_data_interested(_id_interested int4, _name varchar, _last_name varchar, _e_mail varchar, _phone varchar, _type_person int4)";
DROP FUNCTION IF EXISTS "public"."vud_update_data_request(_id_request int4, _observations varchar, _id_status int4, _id_transact int4, _id_vulnerable_group int4, _request_turn varchar, _type_work varchar, _means_request int4, _delivery int4, _id_modality int4)";
DROP FUNCTION IF EXISTS "public"."vud_update_modality(_id int4, _description varchar, _legal_foundation varchar, _days int4, _type_days bpchar)";
DROP FUNCTION IF EXISTS "public"."vud_update_prevention(_id_request int4, _prevent_date date, _citizen_prevent_date date)";
DROP FUNCTION IF EXISTS "public"."vud_update_request_info(_id_request int4, OUT id int4, OUT observations varchar, OUT id_status int4, OUT id_transact int4, OUT id_vulnerable_group int2, OUT means_request int2, OUT delivery int2, OUT id_modality int4, OUT request_turn varchar, OUT type_work varchar, OUT postal_code varchar, OUT colony varchar, OUT ext_number varchar, OUT int_number varchar, OUT street varchar, OUT delegation varchar, OUT name varchar, OUT last_name varchar, OUT e_mail varchar, OUT phone varchar, OUT type_person int2, OUT description_modality varchar)";
DROP FUNCTION IF EXISTS "public"."vud_update_request_requeriment(_id_request int4, _id_requeriment int4)";
DROP FUNCTION IF EXISTS "public"."vud_update_requirement(_id int4, _description varchar)";
DROP FUNCTION IF EXISTS "public"."vud_update_status_request(_id_request int4, _status int4, _last_status int4, _cancelation_description varchar)";
DROP FUNCTION IF EXISTS "public"."vud_update_transact(_id int4, _description varchar, _legal_foundation varchar, _days int4, _type_days bpchar)";
DROP FUNCTION IF EXISTS "public"."vud_update_user(_id_user int4, _user_name varchar, _employee_number int4, _description varchar, _rol int4, _user_register int4, _password varchar)";
DROP FUNCTION IF EXISTS "public"."vud_users_permisions(_id_user int4, OUT id int4, OUT name varchar, OUT id_module int4, OUT id_user int4, OUT read_permission int2, OUT write_permission int2, OUT delete_permission int2, OUT update_permission int2)";
DROP FUNCTION IF EXISTS "public"."vud_users_record(OUT id int4, OUT user_name varchar, OUT employee_number int4, OUT user_register int4, OUT rol varchar, OUT description varchar, OUT id_rol int4)";
DROP FUNCTION IF EXISTS "public"."vud_view_data_request(_id_request int4, OUT id int4, OUT folio varchar, OUT id_user int4, OUT user_name varchar, OUT means_request int2, OUT prevent_date text, OUT citizen_prevent_date text, OUT correct_date text, OUT observation varchar, OUT request_turn varchar, OUT type_work varchar, OUT name varchar, OUT last_name varchar, OUT phone varchar, OUT delegation varchar, OUT colony varchar, OUT postal_code varchar, OUT street varchar, OUT ext_number varchar, OUT int_number varchar, OUT id_status int4, OUT description_status varchar, OUT date_creation text, OUT date_commitment text, OUT code int4, OUT description varchar, OUT modality_desc varchar, OUT id_turn int4, OUT id_dictum int4, OUT folio_dictum varchar, OUT date_receives_dictum text, OUT answer_dictum int4, OUT citizen_dictum text)";
DROP FUNCTION IF EXISTS "public"."vud_year_report(_year varchar, OUT clave_tramite int4, OUT descripcion_tramite varchar, OUT clave_modalidad int4, OUT descripcion_modalidad varchar, OUT enero int8, OUT febrero int8, OUT marzo int8, OUT abril int8, OUT mayo int8, OUT junio int8, OUT julio int8, OUT agosto int8, OUT septiembre int8, OUT octubre int8, OUT noviembre int8, OUT diciembre int8, OUT total_de_tramites int8, OUT total_de_modalidades int8)";
CREATE SEQUENCE "ROLE_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "USERS_Id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "address_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "cat_area_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "cat_modality_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "cat_requeriment_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "cat_status_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "cat_transact_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "interested_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "log_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "modules_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "permission_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "request_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "tracing_dictum_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "tracing_turn_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "vulnerable_group_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "address" (
  "id" int4 NOT NULL DEFAULT nextval('address_id_seq'::regclass),
  "postal_code" varchar(7) COLLATE "pg_catalog"."default" NOT NULL,
  "colony" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "ext_number" varchar(50) COLLATE "pg_catalog"."default",
  "int_number" varchar(50) COLLATE "pg_catalog"."default",
  "street" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "delegation" varchar(255) COLLATE "pg_catalog"."default" NOT NULL
)
;ALTER TABLE "address" OWNER TO "postgres";
CREATE TABLE "cat_area" (
  "id" int4 NOT NULL DEFAULT nextval('cat_area_id_seq'::regclass),
  "description" varchar(100) COLLATE "pg_catalog"."default" NOT NULL
)
;ALTER TABLE "cat_area" OWNER TO "postgres";
CREATE TABLE "cat_modality" (
  "id" int4 NOT NULL DEFAULT nextval('cat_modality_id_seq'::regclass),
  "code" int4 NOT NULL,
  "description" varchar(2000) COLLATE "pg_catalog"."default" NOT NULL,
  "legal_foundation" varchar(3000) COLLATE "pg_catalog"."default",
  "id_transact" int4 NOT NULL,
  "status" int4 NOT NULL,
  "days" int4,
  "type_days" char(1) COLLATE "pg_catalog"."default"
)
;ALTER TABLE "cat_modality" OWNER TO "postgres";
CREATE TABLE "cat_modality_requirement" (
  "id_modality" int4,
  "id_requirement" int4
)
;ALTER TABLE "cat_modality_requirement" OWNER TO "postgres";
CREATE TABLE "cat_requirement" (
  "id" int4 NOT NULL DEFAULT nextval('cat_requeriment_id_seq'::regclass),
  "description" varchar(5000) COLLATE "pg_catalog"."default" NOT NULL,
  "status" int4
)
;ALTER TABLE "cat_requirement" OWNER TO "postgres";
CREATE TABLE "cat_status" (
  "id" int4 NOT NULL DEFAULT nextval('cat_status_id_seq'::regclass),
  "description" varchar(255) COLLATE "pg_catalog"."default" NOT NULL
)
;ALTER TABLE "cat_status" OWNER TO "postgres";
CREATE TABLE "cat_transact" (
  "id" int4 NOT NULL DEFAULT nextval('cat_transact_id_seq'::regclass),
  "code" int4 NOT NULL,
  "description" varchar(2000) COLLATE "pg_catalog"."default" NOT NULL,
  "legal_foundation" varchar(3000) COLLATE "pg_catalog"."default",
  "status" int4 NOT NULL,
  "days" int4,
  "type_days" char(1) COLLATE "pg_catalog"."default"
)
;ALTER TABLE "cat_transact" OWNER TO "postgres";
CREATE TABLE "cat_transact_requirement" (
  "id_transact" int4 NOT NULL,
  "id_requirement" int4 NOT NULL
)
;ALTER TABLE "cat_transact_requirement" OWNER TO "postgres";
CREATE TABLE "interested" (
  "id" int4 NOT NULL DEFAULT nextval('interested_id_seq'::regclass),
  "name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "last_name" varchar(100) COLLATE "pg_catalog"."default",
  "e_mail" varchar(100) COLLATE "pg_catalog"."default",
  "phone" varchar(15) COLLATE "pg_catalog"."default" NOT NULL,
  "type_person" int2 NOT NULL
)
;ALTER TABLE "interested" OWNER TO "postgres";
CREATE TABLE "log" (
  "id" int4 NOT NULL DEFAULT nextval('log_id_seq'::regclass),
  "id_user" int4 NOT NULL,
  "description" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "date_time" timestamptz(6) NOT NULL
)
;ALTER TABLE "log" OWNER TO "postgres";
CREATE TABLE "modules" (
  "id" int4 NOT NULL DEFAULT nextval('modules_id_seq'::regclass),
  "name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "status" int2 NOT NULL
)
;ALTER TABLE "modules" OWNER TO "postgres";
CREATE TABLE "permission" (
  "id" int4 NOT NULL DEFAULT nextval('permission_id_seq'::regclass),
  "id_user" int4 NOT NULL,
  "id_module" int4 NOT NULL,
  "read_permission" int2,
  "write_permission" int2,
  "delete_permission" int2,
  "update_permission" int2
)
;ALTER TABLE "permission" OWNER TO "postgres";
CREATE TABLE "request" (
  "id" int4 NOT NULL DEFAULT nextval('request_id_seq'::regclass),
  "folio" varchar(50) COLLATE "pg_catalog"."default",
  "id_user" int4 NOT NULL,
  "date_creation" date NOT NULL,
  "observations" varchar(2000) COLLATE "pg_catalog"."default",
  "id_status" int4 NOT NULL,
  "id_address" int4 NOT NULL,
  "id_interested" int4 NOT NULL,
  "id_transact" int4 NOT NULL,
  "id_vulnerable_group" int2 NOT NULL,
  "request_turn" varchar(100) COLLATE "pg_catalog"."default",
  "type_work" varchar(100) COLLATE "pg_catalog"."default",
  "means_request" int2 NOT NULL,
  "delivery" int2,
  "date_commitment" date,
  "flag" int2,
  "id_modality" int4,
  "prevent_date" date,
  "citizen_prevent_date" date,
  "correct_date" date,
  "cancelation_description" varchar(2000) COLLATE "pg_catalog"."default",
  "last_status" int4,
  "settlement_date" date,
  "settlement_number" varchar(50) COLLATE "pg_catalog"."default"
)
;ALTER TABLE "request" OWNER TO "postgres";
CREATE TABLE "request_audit" (
  "id" int4 NOT NULL,
  "id_user" int4 NOT NULL,
  "accion" varchar COLLATE "pg_catalog"."default" NOT NULL,
  "id_request" int4 NOT NULL
)
;ALTER TABLE "request_audit" OWNER TO "postgres";
CREATE TABLE "request_requirement" (
  "id_request" int4,
  "id_requirement" int4
)
;ALTER TABLE "request_requirement" OWNER TO "postgres";
CREATE TABLE "role" (
  "id" int4 NOT NULL DEFAULT nextval('"ROLE_id_seq"'::regclass),
  "description" varchar(255) COLLATE "pg_catalog"."default" NOT NULL
)
;ALTER TABLE "role" OWNER TO "postgres";
CREATE TABLE "tracing_dictum" (
  "id" int4 NOT NULL DEFAULT nextval('tracing_dictum_id_seq'::regclass),
  "date_receives_dictum" date,
  "folio_dictum" varchar(50) COLLATE "pg_catalog"."default",
  "answer_dictum" int4,
  "citizen_dictum" date,
  "id_request" int4 NOT NULL
)
;ALTER TABLE "tracing_dictum" OWNER TO "postgres";
CREATE TABLE "tracing_turn" (
  "id" int4 NOT NULL DEFAULT nextval('tracing_turn_id_seq'::regclass),
  "id_request" int4 NOT NULL,
  "date_turn" date NOT NULL,
  "review_area" int4 NOT NULL,
  "observations" varchar(2000) COLLATE "pg_catalog"."default"
)
;ALTER TABLE "tracing_turn" OWNER TO "postgres";
CREATE TABLE "user_audit" (
  "id" int4 NOT NULL,
  "id_user" int4 NOT NULL,
  "accion" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "id_user_modification" int4 NOT NULL
)
;ALTER TABLE "user_audit" OWNER TO "postgres";
CREATE TABLE "users" (
  "id" int4 NOT NULL DEFAULT nextval('"USERS_Id_seq"'::regclass),
  "user_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "employee_number" int4 NOT NULL,
  "description" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "rol" int4 NOT NULL,
  "user_register" int4 NOT NULL,
  "date_register" timestamptz(6) NOT NULL,
  "status" int4 NOT NULL,
  "password" varchar(300) COLLATE "pg_catalog"."default" NOT NULL
)
;ALTER TABLE "users" OWNER TO "postgres";
CREATE TABLE "vulnerable_group" (
  "id" int4 NOT NULL DEFAULT nextval('vulnerable_group_id_seq'::regclass),
  "description" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "status" int2 NOT NULL
)
;ALTER TABLE "vulnerable_group" OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."user_auth"("_user_name" varchar, "_password" varchar)
  RETURNS SETOF "public"."users" AS $BODY$ 
		SELECT
			* 
		FROM
			public."users" 
		WHERE
			user_name = _user_name 
			AND password = _password;
	$BODY$
  LANGUAGE sql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."user_auth"("_user_name" varchar, "_password" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_activate_request"("_id_request" int4, "_status" int4, "_last_status" int4)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN

UPDATE 
	public.request
SET 
	id_status = _last_status,
	last_status = _status
WHERE 
	id = _id_request;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_activate_request"("_id_request" int4, "_status" int4, "_last_status" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_area_record"()
  RETURNS SETOF "public"."cat_area" AS $BODY$
BEGIN

    RETURN QUERY SELECT * from cat_area order by id asc;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_area_record"() OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_create_dictum_request"("_date_receives_dictum" date, "_folio_dictum" varchar, "_answer_dictum" int4, "_citizen_dictum" date, "_id_request" int4)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN
INSERT INTO public.tracing_dictum(
			date_receives_dictum, 
			folio_dictum, 
			answer_dictum, 
			citizen_dictum, 
			id_request)
VALUES (_date_receives_dictum,
		_folio_dictum,
		_answer_dictum,
		_citizen_dictum,
		_id_request);

UPDATE public.request
SET 
	id_status = 8
WHERE 
	id = _id_request;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_create_dictum_request"("_date_receives_dictum" date, "_folio_dictum" varchar, "_answer_dictum" int4, "_citizen_dictum" date, "_id_request" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_create_modality"(OUT "code_modality" int4, IN "_code" int4, IN "_description" varchar, IN "_legal_foundation" varchar, IN "_id_transact" int4, IN "_days" int4, IN "_type_days" bpchar)
  RETURNS SETOF "pg_catalog"."int4" AS $BODY$
DECLARE code_inserted_modality integer;
BEGIN 
	INSERT INTO public.cat_modality(
	code, 
	description, 
	legal_foundation, 
	id_transact,
	status,
	days,
	type_days)
	VALUES (_code, 
			_description, 
			_legal_foundation, 
			_id_transact,
			1,
			_days,
			_type_days)
	RETURNING ID into code_inserted_modality;

	RETURN QUERY SELECT code from public.cat_modality where id = code_inserted_modality;
END 
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_create_modality"(OUT "code_modality" int4, "_code" int4, "_description" varchar, "_legal_foundation" varchar, "_id_transact" int4, "_days" int4, "_type_days" bpchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_create_request_requeriment"("_folio" varchar, "_id_requeriment" int4)
  RETURNS "pg_catalog"."void" AS $BODY$
DECLARE _id_request integer;
BEGIN

_id_request = (SELECT 
	id 
from 
	public.request as a
where 
	a.folio = _folio);

INSERT INTO public.request_requirement(
	id_request, 
	id_requirement)
VALUES (_id_request,_id_requeriment);

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_create_request_requeriment"("_folio" varchar, "_id_requeriment" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_create_requirement"("_description" varchar)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN
	INSERT INTO public.cat_requirement(description,status)
	VALUES (_description,1);
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_create_requirement"("_description" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_create_settlement"("_id_request" int4, "_settlement_date" date, "_settlement_number" varchar)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN

UPDATE public.request
SET settlement_date= _settlement_date,
	settlement_number= _settlement_number,
	id_status = 6
WHERE id = _id_request;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_create_settlement"("_id_request" int4, "_settlement_date" date, "_settlement_number" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_create_turn_request"("_id_request" int4, "_date_turn" date, "_review_area" int4, "_observations" varchar, "_id_status" int4)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN
INSERT INTO public.tracing_turn(
	id_request, 
	date_turn, 
	review_area, 
	observations)
VALUES (_id_request,
		_date_turn,
		_review_area,
		_observations);

UPDATE 
	public.request
SET 
	id_status = 5,
	last_status = _id_status
WHERE 
	id = _id_request;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_create_turn_request"("_id_request" int4, "_date_turn" date, "_review_area" int4, "_observations" varchar, "_id_status" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_data_vulnerable_group"()
  RETURNS SETOF "public"."vulnerable_group" AS $BODY$
BEGIN

RETURN QUERY SELECT * FROM vulnerable_group where status = 1 order by id asc;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_data_vulnerable_group"() OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_delete_modality"("_id" int4)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN

UPDATE public.cat_modality
	SET   status = 0
	WHERE id = _id;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_delete_modality"("_id" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_delete_requirement"("_id" int4)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN
	UPDATE public.cat_requirement
	SET  status = 0
	WHERE id = _id;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_delete_requirement"("_id" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_delete_transact"("_id" int4)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN 
	UPDATE public.cat_transact
	SET status= 0
	WHERE id = _id;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_delete_transact"("_id" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_delete_user"("_id_user" int4)
  RETURNS "pg_catalog"."bool" AS $BODY$

BEGIN

UPDATE public."users" SET status = 0 WHERE "id" = _id_user;
return true;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_delete_user"("_id_user" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_eliminate_relation_modality"("_id" int4)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN
DELETE FROM cat_modality_requirement WHERE id_modality = _id;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_eliminate_relation_modality"("_id" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_eliminate_relation_transact"("_id_transact" int4)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN
	DELETE FROM cat_transact_requirement where id_transact = _id_transact;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_eliminate_relation_transact"("_id_transact" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_get_last_folio"()
  RETURNS SETOF "pg_catalog"."varchar" AS $BODY$
BEGIN
	
	RETURN QUERY select folio from request  order by id desc limit 1;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_get_last_folio"() OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_get_modality_requirements"(IN "_modality_code" int4, OUT "id" int4, OUT "description" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN

RETURN QUERY SELECT 
				c.id,
				c.description 
			FROM 
				public.cat_modality as a
			LEFT JOIN
				public.cat_modality_requirement as b
			on 
				a.code = b.id_modality
			LEFT JOIN 
				public.cat_requirement as c
			on 
				c.id = b.id_requirement
			WHERE
				a.code =  _modality_code
				order by id asc;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_get_modality_requirements"("_modality_code" int4, OUT "id" int4, OUT "description" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_get_request_requirement"(IN "_code" int4, IN "_type_request" int4, OUT "id" int4, OUT "description" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN

IF (_type_request = 2) THEN

RETURN QUERY SELECT 
	c.id,
	c.description
FROM
	public.cat_modality as a
left join
	public.cat_modality_requirement as b
on
	b.id_modality = a.code
left join
	public.cat_requirement as c
on
	b.id_requirement = c.id
	where a.code = _code
	order by c.id asc;

ELSE
	RETURN QUERY SELECT 
		c.id,
		c.description
	FROM 
		public.cat_transact as a
	left join
		public.cat_transact_requirement as b
	on
		a.code = b.id_transact
	left join
		public.cat_requirement as c
	on
		b.id_requirement = c.id
	where a.code = _code
	order by c.id asc;

END IF;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_get_request_requirement"("_code" int4, "_type_request" int4, OUT "id" int4, OUT "description" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_get_transact_requirements"(IN "_transact_code" int4, OUT "id" int4, OUT "description" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN

RETURN QUERY SELECT 
				c.id,
				c.description 
			FROM 
				public.cat_transact as a
			LEFT JOIN
				public.cat_transact_requirement as b
			on 
				a.code = b.id_transact
			LEFT JOIN 
				public.cat_requirement as c
			on 
				c.id = b.id_requirement
			WHERE
				a.code = _transact_code
				order by c.id asc;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_get_transact_requirements"("_transact_code" int4, OUT "id" int4, OUT "description" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_insert_transact"(OUT "_id_code" int4, IN "_code" int4, IN "_description" varchar, IN "_legal_foundation" varchar, IN "_days_transact" int4, IN "_type_days_transact" bpchar)
  RETURNS SETOF "pg_catalog"."int4" AS $BODY$
DECLARE _id_transact integer;
BEGIN

	INSERT INTO public.cat_transact(
		code, 
		description, 
		legal_foundation, 
		status,
		days,
		type_days)
	VALUES (_code,
			_description,
			_legal_foundation,
			1,
			_days_transact,
			_type_days_transact)
	
	RETURNING ID into _id_transact;

	RETURN QUERY SELECT code from public.cat_transact where id = _id_transact;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_insert_transact"(OUT "_id_code" int4, "_code" int4, "_description" varchar, "_legal_foundation" varchar, "_days_transact" int4, "_type_days_transact" bpchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_insert_user"("_user_name" varchar, "_employee_number" int4, "_description" varchar, "_rol" int4, "_user_register" int4, "_password" varchar)
  RETURNS "pg_catalog"."void" AS $BODY$ 
	
		INSERT INTO 
	public."users"
		("user_name",
		"employee_number",
		"description",
		"rol", 
		"user_register",
		"date_register",
		"status",
		"password") 
	VALUES (_user_name,
		_employee_number,
		_description,
		_rol,
		_user_register,
		'now()',
		1,
		_password);
		
	$BODY$
  LANGUAGE sql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_insert_user"("_user_name" varchar, "_employee_number" int4, "_description" varchar, "_rol" int4, "_user_register" int4, "_password" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_insert_user_permission"("_id_user" int4, "_id_module" int4, "_read_permission" int4, "_write_permission" int4, "_delete_permission" int4, "_update_permission" int4)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN

INSERT INTO public.permission(
	id_user,
	id_module, 
	read_permission, 
	write_permission, 
	delete_permission, 
	update_permission)
VALUES (
	_id_user, 
	_id_module, 
	_read_permission, 
	_write_permission, 
	_delete_permission, 
	_update_permission);

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_insert_user_permission"("_id_user" int4, "_id_module" int4, "_read_permission" int4, "_write_permission" int4, "_delete_permission" int4, "_update_permission" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_log_insert"("_id_user" int4, "_description" varchar)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN
	INSERT INTO 
	public."log"(id_user, description,date_time)
	VALUES (_id_user,_description ,NOW());
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_log_insert"("_id_user" int4, "_description" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_modality_data"(IN "_code_modality" int4, OUT "id" int4, OUT "description" varchar, OUT "legal_foundation" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN 
	RETURN QUERY select 
		a.id,
		a.description,
		c.legal_foundation

	from 
		public.cat_requirement as a 
	LEFT JOIN
		public.cat_modality_requirement as b
	on 
		a.id = b.id_requirement
	LEFT join 
		public.cat_modality as c
	on
		b.id_modality = c.code
	where c.code = _code_modality ;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_modality_data"("_code_modality" int4, OUT "id" int4, OUT "description" varchar, OUT "legal_foundation" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_modality_days"(IN "_code" int4, OUT "days" int4, OUT "type_days" bpchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
RETURN QUERY SELECT
                a.days,
                a.type_days
            FROM
                cat_modality as a
            where
                a.code = _code;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_modality_days"("_code" int4, OUT "days" int4, OUT "type_days" bpchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_modality_record"(OUT "id" int4, OUT "code" int4, OUT "description" varchar, OUT "legal_foundation" varchar, OUT "id_transact" int4, OUT "code_transact" int4, OUT "description_transact" varchar, OUT "days" int4, OUT "type_days" bpchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
	RETURN QUERY select 
	a.id,
	a.code,
	a.description,
	a.legal_foundation,
	b.id,
	b.code,
	b.description,
	a.days,
	a.type_days
from 
	cat_modality as a
LEFT JOIN 
	cat_transact as b
on
	a.id_transact = b.code
WHERE 
	a.status = 1
order by a.id asc;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_modality_record"(OUT "id" int4, OUT "code" int4, OUT "description" varchar, OUT "legal_foundation" varchar, OUT "id_transact" int4, OUT "code_transact" int4, OUT "description_transact" varchar, OUT "days" int4, OUT "type_days" bpchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_record_area_revisora"(OUT "id" int4, OUT "description" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
	RETURN QUERY SELECT a.id, a.description from public.cat_area as a order by a.id asc;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_record_area_revisora"(OUT "id" int4, OUT "description" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_record_report"(OUT "folio" varchar, OUT "clave_tramite" int4, OUT "tramite" varchar, OUT "clave_modalidad" int4, OUT "modalidad" varchar, OUT "fecha_creacion" text, OUT "fecha_compromiso" text)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN

RETURN QUERY SELECT 
    a.folio,
    b.code as clave_tramite,
    b.description as tramite,
    c.code as clave_modalidad,
    c.description as modalidad,
    to_char(a.date_creation,'dd-mm-yyyy' ) as fecha_creacion,
    to_char(a.date_commitment,'dd-mm-yyyy') as fecha_compromiso
FROM
    public.request as a
left join
    public.cat_transact as b
on
    a.id_transact = b.code
left join 
    public.cat_modality as c
on 
    a.id_modality = c.code
left join 
    public.address as d
on
    d.id = a.id_address
left join
    public.tracing_turn as e
on 
    e.id_request = a.id
left join
    public.tracing_dictum as f
on 
    f.id_request = a.id
left join
    public.interested as g
on g.id = a.id_interested
left join
    public.cat_status h
on 
    h.id = a.id_status
left join
    public.vulnerable_group i
on 
    i.id = a.id_vulnerable_group 

WHERE a.flag = 1
order by a.id asc;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_record_report"(OUT "folio" varchar, OUT "clave_tramite" int4, OUT "tramite" varchar, OUT "clave_modalidad" int4, OUT "modalidad" varchar, OUT "fecha_creacion" text, OUT "fecha_compromiso" text) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_record_request_requirement"(IN "_code" int4, IN "_type_request" int4, OUT "id" int4, OUT "description" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN

IF (_type_request = 2) THEN

RETURN QUERY SELECT 
	c.id,
	c.description
FROM
	public.cat_modality as a
left join
	public.cat_modality_requirement as b
on
	b.id_modality = a.code
left join
	public.cat_requirement as c
on
	b.id_requirement = c.id
	where a.code = _code
	order by c.id asc;

ELSE
	RETURN QUERY SELECT 
		c.id,
		c.description
	FROM 
		public.cat_transact as a
	left join
		public.cat_transact_requirement as b
	on
		a.code = b.id_transact
	left join
		public.cat_requirement as c
	on
		b.id_requirement = c.id
	where a.code = _code
	order by c.id asc;

END IF;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_record_request_requirement"("_code" int4, "_type_request" int4, OUT "id" int4, OUT "description" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_record_request_requirement"(IN "_id_request" int4, OUT "id" int4, OUT "description" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN

	RETURN QUERY SELECT 
	c.id,
	c.description
	FROM
		public.request  as a
	LEFT JOIN
		public.request_requirement as b
	ON
		b.id_request = a.id
	LEFT JOIN
		public.cat_requirement as c
	ON 
		b.id_requirement = c.id
	WHERE a.id =_id_request
	order by c.id asc;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_record_request_requirement"("_id_request" int4, OUT "id" int4, OUT "description" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_record_requirement"()
  RETURNS SETOF "public"."cat_requirement" AS $BODY$
BEGIN 
	RETURN QUERY select * from public.cat_requirement where status = 1 order by id asc;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_record_requirement"() OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_record_transact"(OUT "id" int4, OUT "code" int4, OUT "description" varchar, OUT "legal_foundation" varchar, OUT "total_modality" int8, OUT "id_transact" int4, OUT "days" int4, OUT "type_days" bpchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$

	SELECT
		a.id,
		a.code,
		a.description,
		a.legal_foundation,
		count(b.id_transact) as total_modality,
		b.id_transact,
		a.days,	
		a.type_days
	FROM
		cat_transact  as a
	left JOIN 
		cat_modality as b
	on  a.code = b.id_transact
	WHERE
		a.status = 1
	GROUP BY b.id_transact,a.id,a.code,a.description,a.legal_foundation,a.days,a.type_days
	order by a.id asc;

$BODY$
  LANGUAGE sql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_record_transact"(OUT "id" int4, OUT "code" int4, OUT "description" varchar, OUT "legal_foundation" varchar, OUT "total_modality" int8, OUT "id_transact" int4, OUT "days" int4, OUT "type_days" bpchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_record_vulnerable_group"(OUT "id" int4, OUT "description" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
	RETURN QUERY SELECT a.id, a.description from public.vulnerable_group as a order by a.id asc;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_record_vulnerable_group"(OUT "id" int4, OUT "description" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_relation_modality_requirement"("_id_modality" int4, "_id_requirement" int4)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN

INSERT INTO public.cat_modality_requirement(id_modality,id_requirement)
VALUES (_id_modality, _id_requirement);

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_relation_modality_requirement"("_id_modality" int4, "_id_requirement" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_relation_transaction_requirement"("_id_transact" int4, "_id_requirement" int4)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN

INSERT INTO public.cat_transact_requirement(
	id_transact, 
	id_requirement)
VALUES (_id_transact,_id_requirement);

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_relation_transaction_requirement"("_id_transact" int4, "_id_requirement" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_request_address"("_postal_code" varchar, "_colony" varchar, "_ext_number" varchar, "_int_number" varchar, "_street" varchar, "_delegation" varchar)
  RETURNS SETOF "pg_catalog"."int4" AS $BODY$
DECLARE _id_request_address integer;
BEGIN

INSERT INTO public.address(
		postal_code, 
		colony, 
		ext_number, 
		int_number, 
		street,
		delegation)
	
VALUES (_postal_code,
		_colony,
		_ext_number,
		_int_number,
		_street,
		_delegation)

RETURNING ID into _id_request_address;
RETURN QUERY SELECT _id_request_address;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_request_address"("_postal_code" varchar, "_colony" varchar, "_ext_number" varchar, "_int_number" varchar, "_street" varchar, "_delegation" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_request_data"("_id_user" int4, "_folio" varchar, "_observations" varchar, "_id_status" int4, "_id_address" int4, "_id_interested" int4, "_id_transact" int4, "_id_vulnerable_group" int4, "_request_turn" varchar, "_type_work" varchar, "_means_request" int4, "_delivery" int4, "_date_commitment" date, "_id_modality" int4)
  RETURNS SETOF "pg_catalog"."varchar" AS $BODY$
DECLARE id_request_created integer;
BEGIN

INSERT INTO public.request( id_user,
							folio,
							date_creation, 
							observations, 
							id_status, 
							id_address, 
							id_interested, 
							id_transact, 
							id_vulnerable_group, 
							request_turn, 
							type_work, 
							means_request, 
							delivery,
							date_commitment,
							flag,
							id_modality)
VALUES (_id_user,
		_folio, 
		NOW(), 
		_observations, 
		_id_status, 
		_id_address, 
		_id_interested, 
		_id_transact, 
		_id_vulnerable_group, 
		_request_turn, 
		_type_work, 
		_means_request, 
		_delivery,
		_date_commitment,
		1,
		_id_modality)
RETURNING ID into id_request_created;

	RETURN QUERY select folio from public.request where id = id_request_created;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_request_data"("_id_user" int4, "_folio" varchar, "_observations" varchar, "_id_status" int4, "_id_address" int4, "_id_interested" int4, "_id_transact" int4, "_id_vulnerable_group" int4, "_request_turn" varchar, "_type_work" varchar, "_means_request" int4, "_delivery" int4, "_date_commitment" date, "_id_modality" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_request_interested"("_name" varchar, "_last_name" varchar, "_e_mail" varchar, "_phone" varchar, "_type_person" int4)
  RETURNS SETOF "pg_catalog"."int4" AS $BODY$
DECLARE id_request_interested integer;
BEGIN
INSERT INTO public.interested(
	name, 
	last_name, 
	e_mail, 
	phone,
	type_person)
	VALUES (_name, 
			_last_name, 
			_e_mail, 
			_phone,
			_type_person)

RETURNING ID into id_request_interested;
RETURN QUERY SELECT id_request_interested;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_request_interested"("_name" varchar, "_last_name" varchar, "_e_mail" varchar, "_phone" varchar, "_type_person" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_request_record"(OUT "id" int4, OUT "folio" varchar, OUT "id_user" int4, OUT "id_status" int4, OUT "description_status" varchar, OUT "date_creation" text, OUT "date_commitment" text, OUT "code" int4, OUT "description" varchar, OUT "modality_desc" varchar, OUT "id_turn" int4, OUT "id_dictum" int4, OUT "last_status" int4, OUT "settlement_number" varchar, OUT "id_address" int4, OUT "id_interested" int4, OUT "citizen_dictum" date, OUT "folio_dictum" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN

RETURN QUERY SELECT 
	a.id,
	a.folio,
	a.id_user,
	d.id as id_status,
	d.description as description_status,
	to_char(a.date_creation,'dd-mm-yyyy' ),
	to_char(a.date_commitment,'dd-mm-yyyy' ),
	b.code,
	b.description,
	c.description as modality_desc,
	e.id as id_turn,
	f.id as id_dictum,
	a.last_status,
	a.settlement_number,
	a.id_address,
	a.id_interested,
	f.citizen_dictum,
	f.folio_dictum

FROM 
	public.request as a
left join 
	public.cat_transact as b
on
	a.id_transact = b.code
left join 
	public.cat_modality as c
on 
	a.id_modality = c.code

left join 
	public.cat_status as d
on
	d.id = a.id_status
left join 
	public.tracing_turn as e
on
	e.id_request = a.id
left join
	public.tracing_dictum as f
on
	f.id_request = a.id
WHERE
	flag = 1
order by id desc;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_request_record"(OUT "id" int4, OUT "folio" varchar, OUT "id_user" int4, OUT "id_status" int4, OUT "description_status" varchar, OUT "date_creation" text, OUT "date_commitment" text, OUT "code" int4, OUT "description" varchar, OUT "modality_desc" varchar, OUT "id_turn" int4, OUT "id_dictum" int4, OUT "last_status" int4, OUT "settlement_number" varchar, OUT "id_address" int4, OUT "id_interested" int4, OUT "citizen_dictum" date, OUT "folio_dictum" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_role"()
  RETURNS SETOF "public"."role" AS $BODY$ 
		SELECT
			*
		FROM
			public. "role";
	$BODY$
  LANGUAGE sql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_role"() OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_status_list"()
  RETURNS SETOF "public"."cat_status" AS $BODY$
BEGIN
RETURN QUERY select 
				* 
			from 
				public.cat_status;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_status_list"() OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_transact_days"(IN "_code" int4, OUT "days" int4, OUT "type_days" bpchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
RETURN QUERY SELECT 
				a.days,
				a.type_days
			FROM 
				cat_transact as a
			where
				a.code = _code;
				
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_transact_days"("_code" int4, OUT "days" int4, OUT "type_days" bpchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_transact_record"(OUT "code" int4, OUT "description" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
	RETURN QUERY SELECT a.code,
						a.description
				FROM public.cat_transact as a 
				WHERE status = 1 
				order by id desc;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_transact_record"(OUT "code" int4, OUT "description" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_transact_record_modality"("_code" int4)
  RETURNS SETOF "public"."cat_modality" AS $BODY$
BEGIN
	RETURN QUERY SELECT * FROM public.cat_modality where id_transact = _code order by id asc;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_transact_record_modality"("_code" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_transact_requeriment"(IN "_id_code" int4, OUT "id" int4, OUT "description" varchar, OUT "legal_foundation" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
	
	RETURN QUERY select 
		a.id,
		a.description ,
		c.legal_foundation
	from 
		public.cat_requirement as a
	inner join public.cat_transact_requirement as b
	on a.id = b.id_requirement
	inner join public.cat_transact as c
	on c.code = b.id_transact
	where c.code = _id_code;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_transact_requeriment"("_id_code" int4, OUT "id" int4, OUT "description" varchar, OUT "legal_foundation" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_update_citizen_dictum"("_id_request" int4, "_date_citizen_dictum" date)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN

UPDATE tracing_dictum
SET
	citizen_dictum = _date_citizen_dictum
WHERE
	_id_request = id_request;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_update_citizen_dictum"("_id_request" int4, "_date_citizen_dictum" date) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_update_correct"("_id_request" int4, "_correct_date" date)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN
UPDATE public.request
	
SET 	
	correct_date = _correct_date,
	id_status = 9
WHERE 
	id = _id_request;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_update_correct"("_id_request" int4, "_correct_date" date) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_update_data_address"("_id_address" int4, "_postal_code" varchar, "_colony" varchar, "_ext_number" varchar, "_int_number" varchar, "_street" varchar, "_delegation" varchar)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN

	UPDATE public.address
	SET postal_code=_postal_code, 
		colony=_colony, 
		ext_number=_ext_number, 
		int_number=_int_number, 
		street=_street, 
		delegation=_delegation
    WHERE id = _id_address;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_update_data_address"("_id_address" int4, "_postal_code" varchar, "_colony" varchar, "_ext_number" varchar, "_int_number" varchar, "_street" varchar, "_delegation" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_update_data_interested"("_id_interested" int4, "_name" varchar, "_last_name" varchar, "_e_mail" varchar, "_phone" varchar, "_type_person" int4)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN

	UPDATE 
		public.interested
    SET name=_name, 
    	last_name=_last_name, 
    	e_mail=_e_mail, 
    	phone=_phone, 
    	type_person=_type_person
    WHERE 
    	id =_id_interested;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_update_data_interested"("_id_interested" int4, "_name" varchar, "_last_name" varchar, "_e_mail" varchar, "_phone" varchar, "_type_person" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_update_data_request"("_id_request" int4, "_observations" varchar, "_id_status" int4, "_id_transact" int4, "_id_vulnerable_group" int4, "_request_turn" varchar, "_type_work" varchar, "_means_request" int4, "_delivery" int4, "_id_modality" int4)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN
	UPDATE public.request
	SET 
	observations=_observations, 
	id_status=_id_status, 
	id_transact=_id_transact, 
	id_vulnerable_group=_id_vulnerable_group, 
	request_turn=_request_turn, 
	type_work=_type_work, 
	means_request=_means_request, 
	delivery=_delivery, 
	id_modality=_id_modality 	
WHERE id = _id_request;

DELETE FROM public.request_requirement where id_request = _id_request;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_update_data_request"("_id_request" int4, "_observations" varchar, "_id_status" int4, "_id_transact" int4, "_id_vulnerable_group" int4, "_request_turn" varchar, "_type_work" varchar, "_means_request" int4, "_delivery" int4, "_id_modality" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_update_modality"("_id" int4, "_description" varchar, "_legal_foundation" varchar, "_days" int4, "_type_days" bpchar)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN

UPDATE public.cat_modality
	SET   description=_description, legal_foundation=_legal_foundation, days=_days, type_days = _type_days
	WHERE id = _id;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_update_modality"("_id" int4, "_description" varchar, "_legal_foundation" varchar, "_days" int4, "_type_days" bpchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_update_prevention"("_id_request" int4, "_prevent_date" date, "_citizen_prevent_date" date)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN
UPDATE public.request
	
SET 
	prevent_date = _prevent_date, 
	citizen_prevent_date = _citizen_prevent_date,
	id_status = 7
WHERE 
	id = _id_request;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_update_prevention"("_id_request" int4, "_prevent_date" date, "_citizen_prevent_date" date) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_update_request_info"(IN "_id_request" int4, OUT "id" int4, OUT "observations" varchar, OUT "id_status" int4, OUT "id_transact" int4, OUT "id_vulnerable_group" int2, OUT "means_request" int2, OUT "delivery" int2, OUT "id_modality" int4, OUT "request_turn" varchar, OUT "type_work" varchar, OUT "postal_code" varchar, OUT "colony" varchar, OUT "ext_number" varchar, OUT "int_number" varchar, OUT "street" varchar, OUT "delegation" varchar, OUT "name" varchar, OUT "last_name" varchar, OUT "e_mail" varchar, OUT "phone" varchar, OUT "type_person" int2, OUT "description_modality" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
	
RETURN QUERY SELECT 
		a.id,
		a.observations,
		a.id_status,
		a.id_transact,
		a.id_vulnerable_group,
		a.means_request,
		a.delivery,
		a.id_modality,
		a.request_turn,
		a.type_work,
		b.postal_code,
		b.colony,
		b.ext_number,
		b.int_number,
		b.street,
		b.delegation,
		c.name,
		c.last_name,
		c.e_mail,
		c.phone,
		c.type_person,
		d.description as description_modality
FROM
	public.request AS a
LEFT JOIN
	public.address AS b
ON
	a.id_address = b.id
LEFT JOIN
	public.interested AS c
ON
	a.id_interested = c.id
LEFT JOIN
	public.cat_modality as d
on	
	d.code = a.id_modality
WHERE a.id = _id_request;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_update_request_info"("_id_request" int4, OUT "id" int4, OUT "observations" varchar, OUT "id_status" int4, OUT "id_transact" int4, OUT "id_vulnerable_group" int2, OUT "means_request" int2, OUT "delivery" int2, OUT "id_modality" int4, OUT "request_turn" varchar, OUT "type_work" varchar, OUT "postal_code" varchar, OUT "colony" varchar, OUT "ext_number" varchar, OUT "int_number" varchar, OUT "street" varchar, OUT "delegation" varchar, OUT "name" varchar, OUT "last_name" varchar, OUT "e_mail" varchar, OUT "phone" varchar, OUT "type_person" int2, OUT "description_modality" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_update_request_requeriment"("_id_request" int4, "_id_requeriment" int4)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN

INSERT INTO public.request_requirement(
	id_request, 
	id_requirement)
VALUES (_id_request,_id_requeriment);

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_update_request_requeriment"("_id_request" int4, "_id_requeriment" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_update_requirement"("_id" int4, "_description" varchar)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN

	UPDATE public.cat_requirement
	SET  description=_description
	WHERE id = _id;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_update_requirement"("_id" int4, "_description" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_update_status_request"("_id_request" int4, "_status" int4, "_last_status" int4, "_cancelation_description" varchar)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN

UPDATE 
	public.request
SET 
	id_status = _status,
	last_status = _last_status,
	cancelation_description = _cancelation_description
WHERE 
	id = _id_request;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_update_status_request"("_id_request" int4, "_status" int4, "_last_status" int4, "_cancelation_description" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_update_transact"("_id" int4, "_description" varchar, "_legal_foundation" varchar, "_days" int4, "_type_days" bpchar)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN 
	UPDATE public.cat_transact
	SET  description=_description, legal_foundation=_legal_foundation, days = _days, type_days = _type_days
	WHERE id = _id;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_update_transact"("_id" int4, "_description" varchar, "_legal_foundation" varchar, "_days" int4, "_type_days" bpchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_update_user"("_id_user" int4, "_user_name" varchar, "_employee_number" int4, "_description" varchar, "_rol" int4, "_user_register" int4, "_password" varchar)
  RETURNS "pg_catalog"."bool" AS $BODY$ 
	BEGIN
	IF (_password = '') THEN
		update 
		public."users"
	SET		
		user_name = _user_name,
		employee_number = _employee_number,
		description = _description,
		rol = _rol, 
		user_register = _user_register
	WHERE 
		id = _id_user;
	ELSE 
		update 
		public."users"
	SET		
		user_name = _user_name,
		employee_number = _employee_number,
		description = _description,
		rol = _rol, 
		user_register = _user_register,
		password = _password
	WHERE 
		id = _id_user;
	END IF;
	
	RETURN true;
	END
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;ALTER FUNCTION ""."vud_update_user"("_id_user" int4, "_user_name" varchar, "_employee_number" int4, "_description" varchar, "_rol" int4, "_user_register" int4, "_password" varchar) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_users_permisions"(IN "_id_user" int4, OUT "id" int4, OUT "name" varchar, OUT "id_module" int4, OUT "id_user" int4, OUT "read_permission" int2, OUT "write_permission" int2, OUT "delete_permission" int2, OUT "update_permission" int2)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
RETURN QUERY SELECT 
	a.id,
	a.name,
	b.id_module,
	b.id_user,
	b.read_permission,
	b.write_permission,
	b.delete_permission,
	b.update_permission
FROM
	PUBLIC.modules AS A 
	LEFT JOIN PUBLIC.permission AS b ON a.id = b.id_module

WHERE
	b.id_user = _id_user
order by a.id;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_users_permisions"("_id_user" int4, OUT "id" int4, OUT "name" varchar, OUT "id_module" int4, OUT "id_user" int4, OUT "read_permission" int2, OUT "write_permission" int2, OUT "delete_permission" int2, OUT "update_permission" int2) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_users_record"(OUT "id" int4, OUT "user_name" varchar, OUT "employee_number" int4, OUT "user_register" int4, OUT "rol" varchar, OUT "description" varchar, OUT "id_rol" int4)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ 
		SELECT
			a.id,
			a.user_name,
			a.employee_number,
			a.user_register,
			b.description,
			a.description,
			b.id
		FROM
			public. "users" as a
		INNER JOIN public. "role" as b
		ON a.rol = b.id
		where a.status = 1 order by a.id asc;
	$BODY$
  LANGUAGE sql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_users_record"(OUT "id" int4, OUT "user_name" varchar, OUT "employee_number" int4, OUT "user_register" int4, OUT "rol" varchar, OUT "description" varchar, OUT "id_rol" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_view_data_request"(IN "_id_request" int4, OUT "id" int4, OUT "folio" varchar, OUT "id_user" int4, OUT "user_name" varchar, OUT "means_request" int2, OUT "prevent_date" text, OUT "citizen_prevent_date" text, OUT "correct_date" text, OUT "observation" varchar, OUT "request_turn" varchar, OUT "type_work" varchar, OUT "name" varchar, OUT "last_name" varchar, OUT "phone" varchar, OUT "delegation" varchar, OUT "colony" varchar, OUT "postal_code" varchar, OUT "street" varchar, OUT "ext_number" varchar, OUT "int_number" varchar, OUT "id_status" int4, OUT "description_status" varchar, OUT "date_creation" text, OUT "date_commitment" text, OUT "code" int4, OUT "description" varchar, OUT "modality_desc" varchar, OUT "id_turn" int4, OUT "id_dictum" int4, OUT "folio_dictum" varchar, OUT "date_receives_dictum" text, OUT "answer_dictum" int4, OUT "citizen_dictum" text)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN

RETURN QUERY SELECT 
	a.id,
	a.folio,
	a.id_user,
	i.user_name,
	a.means_request,
	to_char(a.prevent_date,'yyyy-mm-dd'),
	to_char(a.citizen_prevent_date,'yyyy-mm-dd'),
	to_char(a.correct_date,'yyyy-mm-dd'),
	a.observations,
	a.request_turn,
	a.type_work,
	g.name,
	g.last_name,
	g.phone,
	h.delegation,
	h.colony,
	h.postal_code,
	h.street,
	h.ext_number,
	h.int_number,
	d.id as id_status,
	d.description as description_status,
	to_char(a.date_creation,'yyyy-mm-dd' ),
	to_char(a.date_commitment,'yyyy-mm-dd' ),
	b.code,
	b.description,
	c.description as modality_desc,
	e.id as id_turn,
	f.id as id_dictum,
	f.folio_dictum,
	to_char(f.date_receives_dictum,'yyyy-mm-dd'),
	f.answer_dictum,
	to_char(f.citizen_dictum,'yyyy-mm-dd')
FROM 
	public.request as a
left join 
	public.cat_transact as b
on
	a.id_transact = b.code
left join 
	public.cat_modality as c
on 
	a.id_modality = c.code

left join 
	public.cat_status as d
on
	d.id = a.id_status
left join 
	public.tracing_turn as e
on
	e.id_request = a.id
left join
	public.tracing_dictum as f
on
	f.id_request = a.id
left join
	public.interested as g
on 
	a.id_interested = g.id
left join
	public.address as h
on
	a.id_address = h.id
left join 
	public.users as i
on 
	i.id = a.id_user
WHERE
	flag = 1 
AND a.id = _id_request
order by id asc;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_view_data_request"("_id_request" int4, OUT "id" int4, OUT "folio" varchar, OUT "id_user" int4, OUT "user_name" varchar, OUT "means_request" int2, OUT "prevent_date" text, OUT "citizen_prevent_date" text, OUT "correct_date" text, OUT "observation" varchar, OUT "request_turn" varchar, OUT "type_work" varchar, OUT "name" varchar, OUT "last_name" varchar, OUT "phone" varchar, OUT "delegation" varchar, OUT "colony" varchar, OUT "postal_code" varchar, OUT "street" varchar, OUT "ext_number" varchar, OUT "int_number" varchar, OUT "id_status" int4, OUT "description_status" varchar, OUT "date_creation" text, OUT "date_commitment" text, OUT "code" int4, OUT "description" varchar, OUT "modality_desc" varchar, OUT "id_turn" int4, OUT "id_dictum" int4, OUT "folio_dictum" varchar, OUT "date_receives_dictum" text, OUT "answer_dictum" int4, OUT "citizen_dictum" text) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "public"."vud_year_report"(IN "_year" varchar, OUT "clave_tramite" int4, OUT "descripcion_tramite" varchar, OUT "clave_modalidad" int4, OUT "descripcion_modalidad" varchar, OUT "enero" int8, OUT "febrero" int8, OUT "marzo" int8, OUT "abril" int8, OUT "mayo" int8, OUT "junio" int8, OUT "julio" int8, OUT "agosto" int8, OUT "septiembre" int8, OUT "octubre" int8, OUT "noviembre" int8, OUT "diciembre" int8, OUT "total_de_tramites" int8, OUT "total_de_modalidades" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
RETURN QUERY select  
	a.code AS CLAVE_TRAMITE,
	a.description AS DESCRIPCION_TRAMITE,
	c.code AS CLAVE_MODALIDAD,
	c.description AS descripcion_modalidad,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '01' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Enero,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '02' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Febrero,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '03' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Marzo,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '04' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Abril,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '05' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Mayo,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '06' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Junio,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '07' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Julio,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '08' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Agosto,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '09' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Septiembre,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '10' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Octubre,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '11' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as noviembre,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '12' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as diciembre,
	count(CASE TO_CHAR(b.date_creation, 'YYYY') WHEN _year THEN  b.id_transact END) AS TOTAL_DE_TRAMITES,
	count(CASE TO_CHAR(b.date_creation, 'YYYY') WHEN _year THEN  c.code END ) AS TOTAL_DE_MODALIDADES
from 
	public.cat_transact as a 

left join 
	public.request as b
on
	a.code = b.id_transact

left join 
	public.cat_modality as c
on
	c.id_transact = a.code 
group by 
	a.code,
	a.description,
	c.code,
	c.description
order by a.code asc;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;ALTER FUNCTION ""."vud_year_report"("_year" varchar, OUT "clave_tramite" int4, OUT "descripcion_tramite" varchar, OUT "clave_modalidad" int4, OUT "descripcion_modalidad" varchar, OUT "enero" int8, OUT "febrero" int8, OUT "marzo" int8, OUT "abril" int8, OUT "mayo" int8, OUT "junio" int8, OUT "julio" int8, OUT "agosto" int8, OUT "septiembre" int8, OUT "octubre" int8, OUT "noviembre" int8, OUT "diciembre" int8, OUT "total_de_tramites" int8, OUT "total_de_modalidades" int8) OWNER TO "postgres";
BEGIN;
LOCK TABLE "public"."address" IN SHARE MODE;
DELETE FROM "public"."address";
INSERT INTO "public"."address" ("id","postal_code","colony","ext_number","int_number","street","delegation") VALUES (250, '52900', 'Ciudad Adolfo López Mateos', '', '', 'SILENCIO', 'Atizapán de Zaragoza'),(251, '52900', 'Ciudad Adolfo López Mateos', '', '', 'ASDFSADGA', 'Atizapán de Zaragoza'),(252, '52900', 'Ciudad Adolfo López Mateos', 'MZ 14 LOTE 15', 'MZ 8899 LT678F', 'AV OCEANO', 'Atizapán de Zaragoza'),(253, '52947', 'Lomas Lindas I Sección', '', '', 'AV. SILENCIO', 'Atizapán de Zaragoza'),(254, '52947', 'Lomas Lindas I Sección', '', '', 'AV. SILENCIO', 'Atizapán de Zaragoza'),(255, '52947', 'Lomas Lindas I Sección', '', '', 'AV. SILENCIO', 'Atizapán de Zaragoza'),(256, '52947', 'Lomas Lindas I Sección', '', '', 'SILENCIO MAGICO', 'Atizapán de Zaragoza'),(217, '52900', 'Ciudad Adolfo López Mateos', '89', 'N/A ', 'AV LOPEZ MATEOS', 'Atizapán de Zaragoza'),(215, '52947', 'Lomas Lindas II Sección', '5467', 'N/A', 'AV. OCEANO PACIFICO', 'Atizapán de Zaragoza'),(216, '52947', 'Lomas Lindas II Sección', '5467', 'N/A', 'AV. OCEANO PACIFICO', 'Atizapán de Zaragoza'),(218, '52987', 'Conjunto Habitacional Villa Sol', '', '', 'Av. del sol', 'Atizapán de Zaragoza'),(219, '52947', 'Lomas Lindas I Sección', '67', '', 'AV. DEL CIELO', 'Atizapán de Zaragoza'),(220, '52900', 'Ciudad Adolfo López Mateos', '', '', 'AV DEL MAR MUERTO', 'Atizapán de Zaragoza'),(221, '52947', 'Lomas Lindas I Sección', '', '', 'Av. Oceano Pacifico', 'Atizapán de Zaragoza'),(222, '52900', 'Ciudad Adolfo López Mateos', '', '', 'aV. OCEANO INDICO', 'Atizapán de Zaragoza'),(223, '52947', 'Lomas Lindas I Sección', '', '10', 'av. oceano pacifico', 'Atizapán de Zaragoza'),(224, '52947', 'Lomas Lindas I Sección', '', '', 'Av. Oceano Pacifico', 'Atizapán de Zaragoza'),(225, '52947', 'Lomas Lindas I Sección', '', '', 'AV. OCEANO PACIFICO', 'Atizapán de Zaragoza'),(226, '52947', 'Lomas Lindas I Sección', '', '', 'AV. OCEANO PACIFICO', 'Atizapán de Zaragoza'),(227, '52947', 'Lomas Lindas I Sección', '', '', 'AV. OCEANO PACIFICO', 'Atizapán de Zaragoza'),(228, '52947', 'Lomas Lindas I Sección', '', '', ' calle', 'Atizapán de Zaragoza'),(229, '52947', 'Lomas Lindas I Sección', '', '', 'av. oceano pacifico', 'Atizapán de Zaragoza'),(230, '52947', 'Lomas Lindas I Sección', '', '', 'av. oceano pacifico', 'Atizapán de Zaragoza'),(232, '52947', 'Lomas Lindas I Sección', '', '', 'AV. OCEANO PACIFICO', 'Atizapán de Zaragoza'),(233, '52947', 'Lomas Lindas I Sección', '', '', 'AV. OCEANO PACIFICO', 'Atizapán de Zaragoza'),(235, '52947', 'Lomas Lindas I Sección', '', '', 'AV. OCEANO PACIFICO', 'Atizapán de Zaragoza'),(236, '52947', 'Lomas Lindas I Sección', '', '', 'AV. OCEANO PACIFICO', 'Atizapán de Zaragoza'),(237, '52900', 'Ciudad Adolfo López Mateos', '', '', 'AADFSKAJDF', 'Atizapán de Zaragoza'),(238, '52947', 'Lomas Lindas I Sección', '', '', 'AV. OCEANO PACIFICO', 'Atizapán de Zaragoza'),(239, '52947', 'Lomas Lindas I Sección', '', '', 'AV. OCEANO PACIFICO', 'Atizapán de Zaragoza'),(240, '52947', 'Lomas Lindas I Sección', '10', '', 'AV. OCEANO PACIFICO', 'Atizapán de Zaragoza'),(231, '52947', 'Lomas Lindas I Sección', '', '', 'AV. OCEANO PACIFICO', 'Atizapán de Zaragoza'),(234, '52947', 'Lomas Lindas I Sección', '', '', 'AV. OCEANO PACIFICO', 'Atizapán de Zaragoza'),(241, '02000', 'Centro de Azcapotzalco', '654', '75', 'hfhj', 'Azcapotzalco'),(242, '02000', 'Centro de Azcapotzalco', '654', '75', 'hfhj', 'Azcapotzalco'),(243, '52900', 'Ciudad Adolfo López Mateos', '', '', 'DEL SILENCIO', 'Atizapán de Zaragoza'),(244, '52947', 'Lomas Lindas I Sección', '', '', 'silencio', 'Atizapán de Zaragoza'),(245, '52900', 'Ciudad Adolfo López Mateos', '', '', 'SILENCIO', 'Atizapán de Zaragoza'),(246, '52900', 'Ciudad Adolfo López Mateos', '', '', 'SILENCIO', 'Atizapán de Zaragoza'),(247, '52900', 'Ciudad Adolfo López Mateos', '', '', 'SILENCIO', 'Atizapán de Zaragoza'),(248, '52900', 'Ciudad Adolfo López Mateos', '', '', 'SILENCIO', 'Atizapán de Zaragoza'),(249, '52900', 'Ciudad Adolfo López Mateos', '', '', 'SILENCIO', 'Atizapán de Zaragoza');
COMMIT;
BEGIN;
LOCK TABLE "public"."cat_area" IN SHARE MODE;
DELETE FROM "public"."cat_area";
INSERT INTO "public"."cat_area" ("id","description") VALUES (1, 'DIRECCIÓN GENERAL DE OBRAS Y DESARROLLO URBANO'),(2, ' DIRECCIÓN GENERAL JURÍDICA Y GOBIERNO'),(3, 'DIRECCIÓN GENERAL DE SERVICIOS URBANOS'),(4, 'DIRECCIÓN GENERAL DE ADMINISTRACIÓN'),(5, 'DIRECCIÓN GENERAL DE PROTECCIÓN CIVIL.');
COMMIT;
BEGIN;
LOCK TABLE "public"."cat_modality" IN SHARE MODE;
DELETE FROM "public"."cat_modality";
INSERT INTO "public"."cat_modality" ("id","code","description","legal_foundation","id_transact","status","days","type_days") VALUES (1, 318, 'Expedicion ', 'Artículos 6, 7 y 9 del Acuerdo por el que se reforman y adicionan diversas disposiciones del similar por el que se establece el Sistema de Empadronamiento  para  comerciantes  de  los  Mercados  Públicos  de  la Ciudad de México y los formatos oficiales de Cédula de Empadronamiento, refrendo y autorización hasta por 90 días para que una persona distinta al empadronado   pueda   ejercer   el   comercio   en   puestos   permanentes   o temporales en Mercados Públicos, por cuenta del empadronado,Artículos  5,  6  y 7  del  Acuerdo  por  el  que  se  establece  el  Sistema  de Empadronamiento  para  comerciantes  de  los  Mercados  Públicos  de  la Ciudad de México y los formatos oficiales de Cédula de Empadronamiento, refrendo y autorización hasta por 90 días para que una persona distinta al empadronado   pueda   ejercer   el   comercio   en   puestos   permanentes   o temporales en Mercados Públicos, por cuenta del empadronado,Artículos 1, 3, 6, 8 Fracciones I y II, 9 y 11 del Catálogo de Giros para el,Desarrollo de Actividades Comerciales en Mercados Públicos de la Ciudad de México,Artículos 51 fracción V y 124 fracción XVII del Reglamento Interior de la,Administración Pública del Distrito Federal,Artículos 7 bis, 32, 33, 34, 35, 40,  41, 42,  44, 49  y 89  de la  Ley de Procedimiento Administrativo del Distrito Federal,Artículos 26, 27, 28, 29, 30 y 32 del Reglamento de Mercados para el Distrito Federal,Numerales Décimo Quinto Fracción  I, Décimo Sexto,  Décimo Séptimo, Décimo Noveno, Vigésimo, Vigésimo  Primero, Vigésimo Segundo, Vigésimo  Tercero  y  Vigésimo  Cuarto  de  los  Lineamientos   para  la Operación y Funcionamiento de los Mercados Públicos del Distrito Federal', 857, 1, 15, 'H'),(11, 650, 'Expedición de licencia para: Excavaciones o cortes cuya profundidad sea mayor de un metro; Tapiales que invadan la acera en una medida superior a 0.5 m; Obras o instalaciones temporales en propiedad privada y de la vía pública para ferias, aparatos mecánicos, circos, carpas, graderías desmontables y otros similares; e Instalaciones o modificaciones en edificaciones existentes, de ascensores para personas, montacargas, escaleras mecánicas o cualquier otro mecanismo de transporte electro-mecánico, equipos contra incendio y tanques de almacenamiento y/o instalación de maquinaria, con o sin plataformas.', 'Ley de Desarrollo Urbano del Distrito Federal.- Artículos 7 fracciones VII y XVIII, 58, 59 y 87 fracción VI.,Reglamento  de  Construcciones  para  el  Distrito  Federal.-   Artículos  3 fracción IV, 10, 36, 38, 55, 57, 58, 59, 60, 61, 64, 65, 66, 70 fracción II, 236 y 238.,Acuerdo   por   el   que   se   establece   el   Subcomité   de   Instalaciones,Código Fiscal de la Ciudad de México.- Artículos 20, 181, 182, 300, 301 y,302.,Subterráneas del Centro Histórico de la Ciudad de México. Gaceta Oficial del 24 de abril de 2009.- Acuerdos Segundo, Quinto, Sexto, Séptimo y Décimo Segundo.', 905, 1, 1, 'H'),(2, 319, 'Reexpedición', 'Artículos 6, 7 y 9 del Acuerdo por el que se reforman y adicionan diversas disposiciones del similar por el que se establece el Sistema de Empadronamiento  para  comerciantes  de  los  Mercados  Públicos  de  la Ciudad de México y los formatos oficiales de Cédula de Empadronamiento, refrendo y autorización hasta por 90 días para que una persona distinta al empadronado   pueda   ejercer   el   comercio   en   puestos   permanentes   o temporales en Mercados Públicos, por cuenta del empadronado,Artículos  5,  6  y 7  del  Acuerdo  por  el  que  se  establece  el  Sistema  de Empadronamiento  para  comerciantes  de  los  Mercados  Públicos  de  la Ciudad de México y los formatos oficiales de Cédula de Empadronamiento, refrendo y autorización hasta por 90 días para que una persona distinta al empadronado   pueda   ejercer   el   comercio   en   puestos   permanentes   o temporales en Mercados Públicos, por cuenta del empadronado,Artículos 1, 3, 6, 8 Fracciones I y II, 9 y 11 del Catálogo de Giros para el,Desarrollo de Actividades Comerciales en Mercados Públicos de la Ciudad de México,Artículos 51 fracción V y 124 fracción XVII del Reglamento Interior de la,Administración Pública del Distrito Federal,Artículos 7 bis, 32, 33, 34, 35, 40,  41, 42,  44, 49  y 89  de la  Ley de Procedimiento Administrativo del Distrito Federal,Artículos 26, 27, 28, 29, 30 y 32 del Reglamento de Mercados para el Distrito Federal,Numerales Décimo Quinto Fracción  I, Décimo Sexto,  Décimo Séptimo, Décimo Noveno, Vigésimo, Vigésimo  Primero, Vigésimo Segundo, Vigésimo  Tercero  y  Vigésimo  Cuarto  de  los  Lineamientos   para  la Operación y Funcionamiento de los Mercados Públicos del Distrito Federal', 857, 1, 15, 'H'),(3, 619, 'Registro de manifestación de construcción.', 'Ley Orgánica de la Administración  Pública del Distrito Federal.- Artículo 39 fracción II.,Reglamento  Interior de la Administración  Pública del Distrito Federal.- Artículo 50 A fracción XXIX.,Reglamento de Construcciones  para el Distrito Federal.- Artículos 3 fracciones IV y VIII, 36, 38, 47, 48, 51 fracciones II y III, 53, 54 fracción III, 61, 64, 65 y 70.,Ley de Desarrollo  Urbano  del Distrito  Federal.-  Artículos  7 fracciòn  VII,  XVIII, 8 fraccion IV, 47 Quater fracción XVI, inciso c) y 87 fracción VI.,Código Fiscal de la Ciudad de México.- Artículos 20, 181, 182, 300, 301 y 302.', 903, 1, 0, '0'),(4, 620, 'Prorroga de Registro de manifestación de construcción.', 'Reglamento de Construcciones para el Distrito Federal,Artículos 54 fracción III y 64,Artículo 185, último párrafo del Código Fiscal de la Ciudad de México', 903, 1, 40, 'H'),(5, 621, 'Aviso de Terminación de Obra de Registro de manifestación de construcción tipo B', 'Reglamento de Construcciones para el Distrito Federal. Artículos 65 y 70.', 903, 1, 5, 'H'),(6, 645, 'Areas o zonas de conservacion del patrimonio historico, artistico, arqueologico y Cultural de la Federación o cuando se trate de inmuebles parte patrimonio cultural urbano y/o ubicados dentro del area de conservación patrimonial del distrito federal, según corresponda.', 'Ley de Desarrollo Urbano del Distrito Federal.- Artículos 7 fracciones VII y XVIII, 58, 59 y 87 fracción VI.,Reglamento  de  Construcciones  para  el  Distrito  Federal.-   Artículos  3 fracción IV, 10, 36, 38, 55, 57, 58, 59, 60, 61, 64, 65, 66, 70 fracción II, 236 y 238.,Acuerdo   por   el   que   se   establece   el   Subcomité   de   Instalaciones,Código Fiscal de la Ciudad de México.- Artículos 20, 181, 182, 300, 301 y,302.,Subterráneas del Centro Histórico de la Ciudad de México. Gaceta Oficial del 24 de abril de 2009.- Acuerdos Segundo, Quinto, Sexto, Séptimo y Décimo Segundo.', 905, 1, 30, 'H'),(7, 646, 'Edificaciones en suelo de conservación.', 'Ley de Desarrollo Urbano del Distrito Federal.- Artículos 7 fracciones VII y XVIII, 58, 59 y 87 fracción VI.,Reglamento  de  Construcciones  para  el  Distrito  Federal.-   Artículos  3 fracción IV, 10, 36, 38, 55, 57, 58, 59, 60, 61, 64, 65, 66, 70 fracción II, 236 y 238.,Acuerdo   por   el   que   se   establece   el   Subcomité   de   Instalaciones,Código Fiscal de la Ciudad de México.- Artículos 20, 181, 182, 300, 301 y,302.,Subterráneas del Centro Histórico de la Ciudad de México. Gaceta Oficial del 24 de abril de 2009.- Acuerdos Segundo, Quinto, Sexto, Séptimo y Décimo Segundo.', 905, 1, 1, 'H'),(8, 647, 'Instalaciones subterráneas, aéreas o sobre superficie, de demolición del pavimento o cortes en las banquetas y guarniciones en la vía pública.', 'Ley de Desarrollo Urbano del Distrito Federal.- Artículos 7 fracciones VII y XVIII, 58, 59 y 87 fracción VI.,Reglamento  de  Construcciones  para  el  Distrito  Federal.-   Artículos  3 fracción IV, 10, 36, 38, 55, 57, 58, 59, 60, 61, 64, 65, 66, 70 fracción II, 236 y 238.,Acuerdo   por   el   que   se   establece   el   Subcomité   de   Instalaciones,Código Fiscal de la Ciudad de México.- Artículos 20, 181, 182, 300, 301 y,302.,Subterráneas del Centro Histórico de la Ciudad de México. Gaceta Oficial del 24 de abril de 2009.- Acuerdos Segundo, Quinto, Sexto, Séptimo y Décimo Segundo.', 905, 1, 1, 'H'),(9, 648, 'Estaciones repetidoras de comunicación celular y/o inalámbrica.', 'Ley de Desarrollo Urbano del Distrito Federal.- Artículos 7 fracciones VII y XVIII, 58, 59 y 87 fracción VI.,Reglamento  de  Construcciones  para  el  Distrito  Federal.-   Artículos  3 fracción IV, 10, 36, 38, 55, 57, 58, 59, 60, 61, 64, 65, 66, 70 fracción II, 236 y 238.,Acuerdo   por   el   que   se   establece   el   Subcomité   de   Instalaciones,Código Fiscal de la Ciudad de México.- Artículos 20, 181, 182, 300, 301 y,302.,Subterráneas del Centro Histórico de la Ciudad de México. Gaceta Oficial del 24 de abril de 2009.- Acuerdos Segundo, Quinto, Sexto, Séptimo y Décimo Segundo.', 905, 1, 1, 'H'),(10, 649, 'Demoliciones mayores de 60m2.', 'Ley de Desarrollo Urbano del Distrito Federal.- Artículos 7 fracciones VII y XVIII, 58, 59 y 87 fracción VI.,Reglamento  de  Construcciones  para  el  Distrito  Federal.-   Artículos  3 fracción IV, 10, 36, 38, 55, 57, 58, 59, 60, 61, 64, 65, 66, 70 fracción II, 236 y 238.,Acuerdo   por   el   que   se   establece   el   Subcomité   de   Instalaciones,Código Fiscal de la Ciudad de México.- Artículos 20, 181, 182, 300, 301 y,302.,Subterráneas del Centro Histórico de la Ciudad de México. Gaceta Oficial del 24 de abril de 2009.- Acuerdos Segundo, Quinto, Sexto, Séptimo y Décimo Segundo.', 905, 1, 1, 'H'),(12, 651, 'Para instalaciones o modificaciones en edificaciones existentes, de ascensores para personas, monta cargas, escaleras mecanicas o cualquier otro tipo de mecanismo de transporte electromecanico. ', 'Ley de Desarrollo Urbano del Distrito Federal.- Artículos 7 fracciones VII y XVIII, 58, 59 y 87 fracción VI.,Reglamento  de  Construcciones  para  el  Distrito  Federal.-   Artículos  3 fracción IV, 10, 36, 38, 55, 57, 58, 59, 60, 61, 64, 65, 66, 70 fracción II, 236 y 238.,Acuerdo   por   el   que   se   establece   el   Subcomité   de   Instalaciones,Código Fiscal de la Ciudad de México.- Artículos 20, 181, 182, 300, 301 y,302.,Subterráneas del Centro Histórico de la Ciudad de México. Gaceta Oficial del 24 de abril de 2009.- Acuerdos Segundo, Quinto, Sexto, Séptimo y Décimo Segundo.', 905, 1, 1, 'H'),(13, 652, 'Prorroga de licencia de Construcción Especial.', 'Ley de Desarrollo Urbano del Distrito Federal.- Artículos 7 fracciones VII y XVIII, 58, 59 y 87 fracción VI.,Reglamento  de  Construcciones  para  el  Distrito  Federal.-   Artículos  3 fracción IV, 10, 36, 38, 55, 57, 58, 59, 60, 61, 64, 65, 66, 70 fracción II, 236 y 238.,Acuerdo   por   el   que   se   establece   el   Subcomité   de   Instalaciones,Código Fiscal de la Ciudad de México.- Artículos 20, 181, 182, 300, 301 y,302.,Subterráneas del Centro Histórico de la Ciudad de México. Gaceta Oficial del 24 de abril de 2009.- Acuerdos Segundo, Quinto, Sexto, Séptimo y Décimo Segundo.', 905, 1, 1, 'H'),(14, 653, 'Aviso de Terminación de Obra de licencia de Construcción Especial', 'Reglamento de Construcciones para el Distrito Federal.- Artículos 65 y 70', 905, 1, 5, 'H'),(15, 695, 'Registro de manifestación de construcción tipo A.', 'Ley de Desarrollo Urbano del Distrito Federal.- Artículos 7º fracciones VII,,XVIII, 8 fracciones III, IV y 87 fracción VI.,Reglamento de Construcciones para el Distrito Federal.- Artículos 3º fracción IV, 47, 48, 50, 51 fracción I, 52, 54 fracciones I, II, 64, 65 párrafo tercero, 70 fracción II, 128, 244 y 245.,Reglamento  Interior  de  la  Administración  Pública  del  Distrito  Federal.-,Artículo 50 A fracción XXIX.,Ley Orgánica de la Administración Pública del Distrito Federal Artículo 39 fracción II.', 906, 1, 0, '0'),(16, 696, 'Prorroga de Registro de manifestación de construcción tipo A.', 'Reglamento de Construcciones para el Distrito Federal.- Artículos 54 fracciones I, II y 64', 906, 1, 3, 'H'),(17, 697, 'Aviso de Terminación de Obra de Registro de manifestación de construcción tipo A.', 'Reglamento de Construcciones para el Distrito Federal.- Artículos  65 y 70', 906, 1, 0, '0'),(18, 639, ' Aviso de visto bueno de seguridad y operación', 'Reglamento  de  Construcciones  para  el  Distrito  Federal.-  Artículos  35 fracción II párrafo tercero, 38 fracción III inciso c), 68, 69 y 70.', 907, 1, 0, '0'),(19, 640, 'Renovación visto bueno de seguridad y operación', 'Reglamento  de  Construcciones  para  el  Distrito  Federal.-  Artículos  35 fracción II párrafo tercero, 38 fracción III inciso c), 68, 69 y 70.', 907, 1, 0, '0'),(20, 643, ' Registro de constancia de seguridad estructural ', 'Ley de Desarrollo Urbano del Distrito Federal.- Artículo 7 fracción VII.,Reglamento  de  Construcciones  para  el  Distrito  Federal.-  Artículos  38 fracción I, inciso e), 71, 139 fracciones I y II y 170, 233.', 908, 1, 0, '0'),(21, 644, 'Renovación de constancia de seguridad estructural.', 'Ley de Desarrollo Urbano del Distrito Federal.- Artículo 7 fracción VII.,Reglamento  de  Construcciones  para  el  Distrito  Federal.-  Artículos  38 fracción I, inciso e), 71, 139 fracciones I y II y 170, 233.', 908, 1, 0, '0'),(22, 2668, 'a) Inmuebles destinados a vivienda plurifamiliar, conjuntos y unidades habitacionales', 'Ley del Sistema de Protección Civil del Distrito Federal. Artículos 73, 78, 89, 90, 91, Transitorio 23.,Ley  Orgánica  de  la  Administración  Pública  del Distrito  Federal.  Articulo  39 fracciones LXVIII Y LXIX,Reglamento de la Ley de Protección Civil para el Distrito Federal.  Artículos 7, 23, 24, 26.,Términos  de  Referencia  para  la  elaboración  de  Programas  Internos  de Protección Civil, TR-SPC-001-PIPC-2016, Publicados en la Gaceta Oficial de la Ciudad de México el 22 de Febrero de 2016. Aplica en su totalidad', 1980, 1, 30, 'H'),(23, 26681, ' b) Inmuebles destinados al servicio público', 'Ley del Sistema de Protección Civil del Distrito Federal. Artículos 73, 78, 89, 90, 91, Transitorio 23.,Ley  Orgánica  de  la  Administración  Pública  del Distrito  Federal.  Articulo  39 fracciones LXVIII Y LXIX,Reglamento de la Ley de Protección Civil para el Distrito Federal.  Artículos 7, 23, 24, 26.,Términos  de  Referencia  para  la  elaboración  de  Programas  Internos  de Protección Civil, TR-SPC-001-PIPC-2016, Publicados en la Gaceta Oficial de la Ciudad de México el 22 de Febrero de 2016. Aplica en su totalidad', 1980, 1, 30, 'H'),(24, 26682, ' c) Inmuebles de aforo mayor a cincuenta personas, o de aforo menor, en los casos en los que de acuerdo al cuestionario de autodiagnóstico, resulten de alto riesgo', 'Ley del Sistema de Protección Civil del Distrito Federal. Artículos 73, 78, 89, 90, 91, Transitorio 23.,Ley  Orgánica  de  la  Administración  Pública  del Distrito  Federal.  Articulo  39 fracciones LXVIII Y LXIX,Reglamento de la Ley de Protección Civil para el Distrito Federal.  Artículos 7, 23, 24, 26.,Términos  de  Referencia  para  la  elaboración  de  Programas  Internos  de Protección Civil, TR-SPC-001-PIPC-2016, Publicados en la Gaceta Oficial de la Ciudad de México el 22 de Febrero de 2016. Aplica en su totalidad', 1980, 1, 30, 'H'),(25, 26683, ' d) Establecimientos mercantiles de mediano y alto riesgo', 'Ley del Sistema de Protección Civil del Distrito Federal. Artículos 73, 78, 89, 90, 91, Transitorio 23.,Ley  Orgánica  de  la  Administración  Pública  del Distrito  Federal.  Articulo  39 fracciones LXVIII Y LXIX,Reglamento de la Ley de Protección Civil para el Distrito Federal.  Artículos 7, 23, 24, 26.,Términos  de  Referencia  para  la  elaboración  de  Programas  Internos  de Protección Civil, TR-SPC-001-PIPC-2016, Publicados en la Gaceta Oficial de la Ciudad de México el 22 de Febrero de 2016. Aplica en su totalidad', 1980, 1, 30, 'H'),(26, 26684, 'e) Centros comerciales', 'Ley del Sistema de Protección Civil del Distrito Federal. Artículos 73, 78, 89, 90, 91, Transitorio 23.,Ley  Orgánica  de  la  Administración  Pública  del Distrito  Federal.  Articulo  39 fracciones LXVIII Y LXIX,Reglamento de la Ley de Protección Civil para el Distrito Federal.  Artículos 7, 23, 24, 26.,Términos  de  Referencia  para  la  elaboración  de  Programas  Internos  de Protección Civil, TR-SPC-001-PIPC-2016, Publicados en la Gaceta Oficial de la Ciudad de México el 22 de Febrero de 2016. Aplica en su totalidad', 1980, 1, 30, 'H'),(27, 26685, 'f) Industrias', 'Ley del Sistema de Protección Civil del Distrito Federal. Artículos 73, 78, 89, 90, 91, Transitorio 23.,Ley  Orgánica  de  la  Administración  Pública  del Distrito  Federal.  Articulo  39 fracciones LXVIII Y LXIX,Reglamento de la Ley de Protección Civil para el Distrito Federal.  Artículos 7, 23, 24, 26.,Términos  de  Referencia  para  la  elaboración  de  Programas  Internos  de Protección Civil, TR-SPC-001-PIPC-2016, Publicados en la Gaceta Oficial de la Ciudad de México el 22 de Febrero de 2016. Aplica en su totalidad', 1980, 1, 30, 'H'),(28, 2670, 'Autorización del Programa Interno de Protección civil para Unidades Hospitalarias', 'Ley del Sistema de Protección Civil del Distrito Federal. Artículos 73, 78, 89, 90, 91, Transitorio 23.,Ley Orgánica de la Administración Pública del Distrito Federal. Articulo 39 fracciones LXVIII Y LXIX.,Términos  de  Referencia  para  la  elaboración  de  Programas  Internos  de Protección Civil, TR-SPC-001-PIPC-2016. Aplica en su totalidad.,Reglamento de la Ley de Protección Civil para el Distrito Federal.  Artículos,23, 24, 26.,Términos de Referencia para la Elaboración de Programas Internos de Protección Civil en Unidades Hospitalarias, TR-SPC-004-PIPC- Unidades Hospitalarias-2017,  Publicados en la Gaceta Oficial de la Ciudad de México el 2 de Marzo de 2017. Aplica en su totalidad.', 1980, 1, 30, 'H'),(29, 2669, 'Autorización del Programa Interno de Protección Civil para Obras en Proceso de Construcción, Remodelación y Demolición.', 'Ley del Sistema de Protección  Civil del Distrito Federal.  Artículos  73, 78, 89, 90, 91, Transitorio 23. Reglamento de la Ley de Protección Civil para el Distrito Federal.  Artículos 23, 24,26. Ley Orgánica  de la Administración  Pública del Distrito Federal. Articulo 39 fracciones LXVIII Y LXIX. Términos de Referencia  para la elaboración  de Programas  Internos de Protección Civil 003-PIPC-OBRAS-2017, Publicados en la Gaceta Oficial de la Ciudad de México el para obras en proceso de construcción,  remodelación  y demolición TR-SPC-2 de Marzo de 2017. Aplica en su totalidad.', 1980, 1, 30, 'H'),(30, 1, 'Aviso para la presentación de espectáculos públicos.', 'Ley para la celebración de Espectáculos Públicos en el Distrito Federal. Artículo 4, fracción II, III, X, 8, 9, 12, 19, 20, 23, 24, 25, 26, 27, 28, 29 y 30, 55, 55 BIS, 55 TER. 54 QUÁTER, 58, 59 y 60.,Reglamento de la Ley para la celebración de Espectáculos Públicos del Distrito Federal en materia de Espectáculos Masivos y Deportivos. Artículos 3, 6, 7, 8, 9, 10 y 11.,Código Fiscal de la CDMX. Artículos 20, 256, 257 y 258.,Reglamento interior de la Administración Pública del Distrito Federal. Artículo 104 fracción II.,', 3, 1, 0, '0'),(31, 2, 'Permiso para la presentación de Espectáculos Públicos', 'Ley para la celebración de Espectáculos Públicos en el Distrito Federal. Artículo 4, fracción II, III, X, 8, 9, 12, 19, 20, 23, 24, 25, 26, 27, 28, 29 y 30, 55, 55 BIS, 55 TER. 54 QUÁTER, 58, 59 y 60.,Reglamento de la Ley para la celebración de Espectáculos Públicos del Distrito Federal en materia de Espectáculos Masivos y Deportivos. Artículos 3, 6, 7, 8, 9, 10 y 11.,Código Fiscal de la CDMX. Artículos 20, 256, 257 y 258.,Reglamento interior de la Administración Pública del Distrito Federal. Artículo 104 fracción II.,', 3, 1, 0, '0'),(58, 6699, 'modalidad de prueba', 'fundamento juridico de una modalidad de prueba', 55555, 0, 30, 'H'),(33, 32, ' (Programa Especial).', 'Ley del Sistema de Protección Civil del Distrito Federal. Artículos 24, 76 y 94.,Términos de Referencia para la elaboración de Programas Internos de Protección Civil, TR-SPC-002-PEPC-2016. Sección IV, Capítulo I - Cuestionario de Autodiagnóstico.', 1, 0, 30, 'H'),(59, 7777788, 'descipcion para modalidad de prueba actualizacion ', 'fundamento juridico de modalidad actualizacion', 99894, 0, 90, 'H'),(32, 31, ' (Programa Interno).', 'Ley del Sistema de Protección Civil del Distrito Federal. Artículos 24, 76.,Términos de Referencia para la elaboración de Programas Internos de Protección Civil, TR-SPC-001-PIPC-2016. Sección IV, Capítulo I - Cuestionario de Autodiagnóstico', 1, 0, 30, 'H'),(60, 98765, 'descripción de nueva modalidad con log', 'fundamento juridico de pruebaaaaaaa', 55555, 0, 15, 'H'),(61, 9966, 'DESCRIPCION DE PRUEBA', 'PRUEBA DE FUNDAMENTO JURIDICO', 905, 1, 0, '0');
COMMIT;
BEGIN;
LOCK TABLE "public"."cat_modality_requirement" IN SHARE MODE;
DELETE FROM "public"."cat_modality_requirement";
INSERT INTO "public"."cat_modality_requirement" ("id_modality","id_requirement") VALUES (31, 1),(32, 1),(318, 58),(318, 60),(318, 59),(318, 56),(318, 31),(318, 8),(318, 1),(318, 6),(319, 31),(319, 4),(319, 56),(319, 62),(319, 61),(319, 8),(319, 1),(319, 6),(619, 307),(619, 306),(619, 218),(619, 240),(619, 318),(619, 317),(619, 8),(619, 221),(619, 309),(619, 310),(619, 320),(619, 313),(619, 1),(619, 6),(619, 311),(619, 308),(619, 316),(619, 314),(619, 228),(619, 246),(619, 312),(619, 315),(620, 321),(620, 8),(620, 1),(620, 6),(621, 1),(621, 6),(621, 8),(621, 322),(621, 323),(639, 324),(639, 8),(639, 6),(639, 325),(639, 1),(640, 326),(640, 6),(640, 8),(640, 1),(643, 211),(643, 8),(643, 1),(643, 6),(644, 211),(644, 8),(644, 1),(644, 6),(646, 219),(646, 240),(646, 227),(646, 178),(646, 309),(646, 224),(646, 221),(646, 310),(646, 225),(646, 222),(646, 308),(646, 314),(646, 228),(646, 223),(646, 220),(646, 315),(647, 229),(647, 178),(647, 233),(647, 225),(647, 231),(647, 230),(647, 247),(647, 232),(648, 219),(648, 235),(648, 240),(648, 178),(648, 236),(648, 238),(648, 225),(648, 237),(648, 247),(649, 240),(649, 245),(649, 239),(649, 225),(649, 243),(649, 242),(649, 246),(649, 244),(649, 247),(650, 248),(650, 247),(652, 249),(652, 8),(652, 1),(652, 6),(653, 252),(653, 250),(653, 251),(653, 1),(653, 6),(653, 253),(695, 306),(695, 218),(695, 175),(695, 178),(695, 8),(695, 1),(695, 6),(695, 180),(695, 316),(695, 176),(695, 177),(696, 321),(696, 8),(696, 1),(696, 6),(697, 182),(697, 8),(697, 322),(697, 1),(697, 6),(2668, 340),(2668, 270),(2668, 359),(2668, 362),(2668, 344),(2668, 254),(2668, 360),(2668, 271),(2668, 274),(2668, 357),(2668, 266),(2668, 260),(2668, 264),(2668, 259),(2668, 261),(2668, 258),(2668, 257),(2668, 256),(2668, 268),(2668, 155),(2668, 269),(2668, 117),(2668, 116),(2668, 8),(2668, 255),(2668, 337),(2668, 273),(2668, 1),(2668, 368),(2668, 364),(2668, 336),(2668, 372),(2668, 352),(2668, 262),(2668, 363),(2668, 343),(2668, 348),(2668, 267),(2669, 125),(2669, 85),(2669, 129),(2669, 84),(2669, 379),(2669, 380),(2669, 130),(2669, 114),(2669, 112),(2669, 133),(2669, 381),(2669, 135),(2669, 172),(2669, 132),(2669, 138),(2669, 382),(2669, 119),(2669, 106),(2669, 147),(2669, 101),(2669, 120),(2669, 102),(2669, 108),(2669, 383),(2669, 93),(2669, 94),(2669, 1),(2669, 86),(2669, 92),(2669, 153),(2669, 384),(2669, 385),(2669, 109),(2669, 111),(2669, 122),(2669, 95),(2669, 98),(2669, 104),(2669, 96),(2669, 103),(2669, 386),(2669, 387),(2669, 99),(2669, 107),(2669, 110),(2669, 478),(2669, 97),(2669, 388),(2669, 389),(2669, 144),(2669, 390),(2669, 391),(2669, 392),(2669, 393),(2669, 394),(2669, 395),(2669, 396),(2669, 131),(2669, 397),(2669, 398),(2669, 137),(2669, 113),(2669, 399),(2669, 400),(2669, 401),(2669, 145),(2669, 149),(2669, 404),(2669, 121),(2669, 405),(2669, 406),(2669, 407),(2669, 408),(2669, 409),(2669, 150),(2669, 152),(2669, 118),(2669, 79),(2669, 410),(2669, 411),(2669, 127),(2669, 123),(2669, 412),(2669, 128),(2669, 124),(2669, 413),(2669, 414),(2669, 126),(2669, 154),(2669, 415),(2669, 416),(2669, 156),(2669, 417),(2669, 418),(2669, 419),(2669, 420),(2669, 421),(2669, 422),(2669, 423),(2669, 424),(2669, 140),(2669, 425),(2669, 426),(2669, 148),(2669, 427),(2669, 141),(2669, 428),(2669, 87),(2669, 429),(2669, 430),(2669, 431),(2669, 432),(2669, 433),(2669, 142),(2669, 434),(2669, 435),(2669, 436),(2669, 437),(2669, 438),(2669, 439),(2669, 440),(2669, 441),(2669, 143),(2669, 442),(2669, 443),(2669, 444),(2669, 445),(2669, 446),(2669, 263),(2670, 340),(2670, 349),(2670, 359),(2670, 362),(2670, 334),(2670, 344),(2670, 365),(2670, 360),(2670, 355),(2670, 135),(2670, 357),(2670, 346),(2670, 347),(2670, 342),(2670, 261),(2670, 8),(2670, 337),(2670, 358),(2670, 1),(2670, 354),(2670, 338),(2670, 368),(2670, 364),(2670, 336),(2670, 353),(2670, 372),(2670, 352),(2670, 361),(2670, 335),(2670, 350),(2670, 341),(2670, 339),(2670, 356),(2670, 345),(2670, 363),(2670, 343),(2670, 348),(2670, 351),(26681, 368),(26681, 340),(26681, 270),(26681, 359),(26681, 362),(26681, 344),(26681, 254),(26681, 259),(26681, 360),(26681, 271),(26681, 274),(26681, 357),(26681, 266),(26681, 264),(26681, 261),(26681, 258),(26681, 155),(26681, 268),(26681, 257),(26681, 256),(26681, 260),(26681, 269),(26681, 117),(26681, 116),(26681, 8),(26681, 255),(26681, 337),(26681, 273),(26681, 1),(26681, 364),(26681, 336),(26681, 372),(26681, 352),(26681, 262),(26681, 363),(26681, 343),(26681, 348),(26681, 267),(26682, 368),(26682, 340),(26682, 270),(26682, 359),(26682, 362),(26682, 344),(26682, 254),(26682, 259),(26682, 360),(26682, 260),(26682, 271),(26682, 274),(26682, 357),(26682, 266),(26682, 264),(26682, 261),(26682, 258),(26682, 257),(26682, 256),(26682, 155),(26682, 269),(26682, 117),(26682, 116),(26682, 8),(26682, 255),(26682, 337),(26682, 273),(26682, 1),(26682, 364),(26682, 268),(26682, 336),(26682, 372),(26682, 352),(26682, 262),(26682, 363),(26682, 343),(26682, 348),(26682, 267),(26683, 368),(26683, 340),(26683, 270),(26683, 359),(26683, 362),(26683, 344),(26683, 254),(26683, 360),(26683, 259),(26683, 271),(26683, 274),(26683, 357),(26683, 266),(26683, 260),(26683, 264),(26683, 261),(26683, 258),(26683, 155),(26683, 257),(26683, 256),(26683, 269),(26683, 117),(26683, 116),(2669, 476),(2669, 476),(2669, 477),(2669, 478),(26683, 268),(26683, 8),(26683, 255),(26683, 337),(26683, 273),(26683, 1),(26683, 364),(26683, 336),(26683, 372),(26683, 352),(26683, 262),(26683, 363),(26683, 343),(26683, 348),(26683, 267),(26684, 368),(26684, 340),(26684, 270),(26684, 359),(26684, 362),(26684, 344),(26684, 254),(26684, 360),(26684, 271),(26684, 274),(26684, 357),(26684, 266),(26684, 260),(26684, 264),(26684, 268),(26684, 259),(26684, 261),(26684, 258),(26684, 257),(26684, 256),(26684, 269),(26684, 117),(26684, 155),(26684, 116),(26684, 8),(26684, 255),(26684, 337),(26684, 273),(26684, 1),(26684, 364),(26684, 336),(26684, 372),(26684, 352),(26684, 262),(26684, 363),(26684, 343),(26684, 348),(26684, 267),(26685, 368),(26685, 340),(26685, 270),(26685, 359),(26685, 362),(26685, 344),(26685, 254),(26685, 155),(26685, 360),(26685, 271),(26685, 274),(26685, 357),(26685, 259),(26685, 266),(26685, 268),(26685, 260),(26685, 264),(26685, 261),(26685, 258),(26685, 257),(26685, 256),(26685, 269),(26685, 117),(26685, 116),(26685, 8),(26685, 255),(26685, 337),(26685, 273),(26685, 1),(26685, 364),(26685, 336),(26685, 372),(26685, 352),(26685, 262),(26685, 363),(26685, 343),(26685, 348),(26685, 267),(1, 9),(1, 10),(1, 1),(1, 6),(1, 8),(1, 373),(1, 20),(1, 374),(2, 26),(2, 11),(2, 12),(2, 13),(2, 14),(2, 15),(2, 16),(2, 17),(2, 18),(2, 21),(2, 6),(2, 8),(2, 1),(2, 9),(2, 20),(2, 24),(2, 374),(2, 375),(7777788, 1),(7777788, 2),(6699, 1),(6699, 2),(6699, 3),(6699, 4),(6699, 5),(6699, 6),(6699, 7),(6699, 8),(6699, 9),(98765, 1),(98765, 2),(9966, 1),(9966, 2),(9966, 3),(9966, 4),(9966, 5),(2670, 116),(2670, 116),(2669, 477);
COMMIT;
BEGIN;
LOCK TABLE "public"."cat_requirement" IN SHARE MODE;
DELETE FROM "public"."cat_requirement";
INSERT INTO "public"."cat_requirement" ("id","description","status") VALUES (300, 'Copia de la bitácora de la supervisión  de campo efectuada por el personal operativo,de la Dirección General del H. Cuerpo de Bomberos', 1),(301, 'Anexos complementarios referentes a las activaciones  de patrocinios  (carpas V.I.P., botargas, inflables, activaciones de radio, fiestas, globos tipo aerostato, etc.)', 1),(302, 'Croquis de ubicación en el inmueble o área de los dispositivos siguientes: a.- Equipo para la prevención  y el combate  de incendios.  b.- Rutas de evacuación, salidas de emergencia,    puntos    de   reunión,    accesos    internos    y   externos,    zonas   de estacionamiento.   c.-   Dispositivo   de   seguridad.   d.-   Estructuras   temporales   y equipamiento  complementario. e.- Servicios generales y de atención a emergencias.', 1),(303, 'Programa   de  mantenimiento   y  supervisión   integral  a  estructuras  temporales   y equipamiento  complementario', 1),(304, 'Contrato vigente y carta responsiva de la operación de sanitarios portátiles', 1),(305, 'Presentar  cualquier  otra responsiva  que no se contemple  en este  apartado  y que solicite las autoridades competentes', 1),(306, 'Comprobante de pago de derechos de acuerdo al Código Fiscal. Original y copia', 1),(307, 'Certificado  Único  de  Zonificación  de  Uso  del  Suelo  o Certificado  Único  de Zonificación  del Suelo Digital o Certificado  de Acreditación  de Uso del Suelo por Derechos Adquiridos, los cuales deberán ser verificados y firmados por el Director Responsable  de Obra y/o Corresponsable  en Diseño Urbano y Arquitectónico,  en su caso. Original y copia', 1),(308, 'Memoria Descriptiva del proyecto, la cual contendrá como mínimo: el listado de locales construidos y las áreas libres, superficie y número de ocupantes o usuarios de cada  uno;  el análisis  del  cumplimiento  de los Programas  Delegacionales  o Parcial, incluyendo  coeficientes  de ocupación  y utilización del suelo; cumpliendo con los requerimientos  del Reglamento, sus Normas Técnicas Complementarias  y demás disposiciones  referentes a: accesibilidad para personas con discapacidad, cantidad   de  estacionamientos   y  su   funcionalidad,   patios   de  iluminación   y ventilación,   niveles  de  iluminación  y  ventilación   en  cada  local,  circulaciones horizontales y verticales, salidas y muebles hidrosanitarios,  visibilidad en salas de espectáculos,  resistencia  de los materiales  al fuego,  circulaciones  y  salidas  de emergencia, equipos de extinción de fuego y otras que se requieran; y en su caso, de  las  restricciones  o afectaciones  del  predio.  Estos  documentos  deben  estar firmados por el propietario o poseedor, por el proyectista indicando su número de cédula profesional,  por el Director Responsable  de Obra y el Corresponsable  en Instalaciones, en su caso. Original y copia', 1),(309, 'Dos tantos de los proyectos de las instalaciones hidráulicas incluyendo el uso de sistemas para calentamiento de agua por medio del aprovechamiento de la energía solar  conforme  a los  artículos  82, 83 y 89 del  Reglamento  de  Construcciones,  sanitarias,  eléctricas,  gas e instalaciones  especiales  y otras que se requieran,  en los que se debe incluir como mínimo:  plantas,  cortes e isométricos  en su caso, mostrando  las  trayectorias  de  tuberías,  alimentaciones,   así  como  el  diseño  y memorias   correspondientes,   que   incluyan   la  descripción   de  los  dispositivos conforme  a los requerimientos  establecidos  por el Reglamento  y sus Normas  en cuanto a salidas y muebles hidráulicos y sanitarios, equipos de extinción de fuego, sistema de captación y aprovechamiento  de aguas pluviales en azotea y otras que considere el proyecto. Estos documentos deben estar firmados por el propietario o poseedor,  por el proyectista  indicando  su número  de cédula  profesional,  por el Director Responsable de Obra y el Corresponsable en Instalaciones, en su caso', 1),(483, 'prueba 2', 0),(482, 'Prueba 111', 0),(1, 'Formato de solicitud correspondiente al trámite, debidamente requisitado', 1),(2, 'Identificación oficial (persona física o moral)', 1),(3, 'Fotografía', 1),(4, 'Cédula de empadronamiento. Original y copia', 1),(5, 'Comprobantes de pago de derechos por uso de suelo y utilización de locales en Mercados Públicos. Original y copia', 1),(6, 'Identificación oficial con fotografía (carta de naturalización  o cartilla de servicio militar o cédula profesional  o pasaporte o certificado  de nacionalidad mexicana o credencial para votar o licencia para conducir) original y copia', 1),(7, 'Identificación oficial de la persona que ejercerá la actividad comercial en nombre del títular. Original y copia', 1),(8, 'Documento con el que acredite la personalidad del representante legal. Original y copia', 1),(9, 'Comprobante de pago de derechos contemplando el art. 256 inciso b, del código fiscal. Original', 1),(10, 'Comprobante de pago de derechos contemplando el art. 257 del código fiscal. Original', 1),(11, 'Vo.Bo de seguridad y operación suscrito por el director responsable de la obra. Original y copia', 1),(12, 'Certificado Único de Acreditación de uso de suelo por Derechos Adquiridos. Original y copia', 1),(13, 'Responsiva de un correspondiente de Seguridad Estructural en los términos del Reglamento de Construcciones. Original y copia', 1),(14, 'Documento que acredite el vinculo legal entre el titular responsable y los participantes. Original y copia', 1),(15, 'Constancia de adeudos en el pago de contribuciones previstas en el art. 20 del código fiscal. Original y copia', 1),(16, 'Oficio de la Secretaria Pública de México que emita opinión de la celebración del espectáculo. Original y copia', 1),(17, 'Comprobante de pagos al que se refiere el art. 190 del código fiscal. Original', 1),(18, 'Comprobante de pagos al que se refiere el art. 191 fracción IV del código fiscal. Original', 1),(19, 'Vo.Bo para la celebración de espectaculos en lo relativo a extintores y señaletica de evacuación para siniestros. Original y copia', 1),(20, 'Comprobante de pagos al que se refiere el art. 256 inciso a, fracción II del código fiscal. Original', 1),(21, 'Comprobante de pagos al que se refiere el art. 257 del código fiscal. Original', 1),(22, 'Documento que demuestre la designación de la comunidad para realizar el evento. Original y copia', 1),(479, 'EN ESPECTÁCULOS TRADICIONALES: Copia del permiso de Transporte de sustancias peligrosas otorgado por la SCT', 0),(480, ' EN CASO DE PIROTÉCNIA EN EXTERIORES: Constancias de capacitación del personal técnico pirotecnico expedidas por institución o tercer acreditado', 0),(310, 'Dos tantos del proyecto estructural de la obra en planos debidamente acotados, con especificaciones  que contengan una descripción  completa y detallada de las características  de la estructura  incluyendo  su cimentación.  Se  especificarán  en ellos  los datos  esenciales  del diseño  como  las cargas  vivas  y los coeficientes sísmicos   considerados    y   las   calidades   de   materiales.   Se   indicarán   los procedimientos   de  construcción  recomendados,   cuando  éstos  difieran  de  los tradicionales.  Deberán mostrarse  en planos los detalles de conexiones,  cambios de nivel y aberturas  para ductos. En particular,  para estructuras  de concreto  se indicarán  mediante  dibujos  acotados  los  detalles  de  colocación  y  traslapes  de refuerzo  de  las  conexiones   entre  miembros  estructurales.  En  los  planos  de estructuras de acero se mostrarán todas las conexiones entre miembros, así como la manera en que deben unirse entre sí los diversos  elementos  que integran un miembro  estructural.  Cuando  se  utilicen  remaches  o  tornillos  se  indicará  su diámetro,  número, colocación y calidad, y cuando las conexiones  sean soldadas se mostrarán  las  características  completas  de la soldadura;  éstas  se indicarán utilizando una simbología  apropiada  y, cuando sea necesario,  se complementará la descripción  con dibujos acotados  y a escala. En el caso de que la estructura esté formada por elementos  prefabricados  o de patente, los planos estructurales deberán   indicar  las  condiciones   que  éstos  deben  cumplir   en  cuanto   a  su resistencia y otros requisitos de comportamiento. Deben especificarse los herrajes y  dispositivos  de  anclaje,  las  tolerancias  dimensionales   y  procedimientos   de montaje.  Deberán  indicarse,  asimismo,  los  procedimientos   de  apuntalamiento, erección  de elementos prefabricados  y conexiones  de una estructura  nueva con otra existente. En los planos de fabricación y en los de montaje de estructuras de acero o de concreto prefabricado,  se proporcionará  la información necesaria para que la estructura  se fabrique y monte de manera que se cumplan  los requisitos indicados en los planos estructurales', 1),(484, 'Nuevo requicito de prueba con el log222222', 0),(23, 'Autorización para pirotécnia y efectos especiales (en caso que lo requiera). Original y copia', 1),(24, 'Póliza de seguro de responsabilidad civil. Original', 1),(25, 'Programa especial de protección civil. Original y copia', 1),(26, 'Autorización por parte de la Secretaria de Gobernación (extranjeros). Original y copia', 1),(27, 'Vo.Bo de seguridad y operación sobre juegos mecánicos. Original y copia', 1),(28, 'Recibo por el que acredita servicio de energía eléctrica. Original y copia', 1),(29, 'Escrito de responsabilidad en caso de averías en las instalaciones y/o equipos eléctricos. Original', 1),(30, 'Responsiva suscrita por Corresponsable en instalaciones. Original', 1),(31, '3 Fotografías tamaño credencial', 1),(32, 'CURP del interesado. Copia', 1),(33, 'Acta de nacimiento del interesado. Original y copia', 1),(34, 'Acta de defunción del titular de la cédula de empadronamiento. Original y copia', 1),(35, 'Comprobante de domicilio del interesado, no mayor a 3 meses de antigüedad. Original y copia', 1),(36, 'Comprobantes de pago de derechos por uso y autorización de locales de Mercados Públicos correspondiente al año en que se realiza la solicitud y los cuatro años anteriores. Copia', 1),(37, 'EN CASO DE PIROTÉCNIA EN EXTERIORES: Documentos de acreditación de personalidad jurídica (carta poder firmada ante dos testigos con ratificación de las firmas ante Notario Público, Carta Poder firmada ante dos testigos e identificación oficial del interesado y de quien realiza el trámite. Poder notarial e identificación oficial del representante o apoderado, acta constitutiva, poder notarial e identificación oficial del representante o apoderado). Original y copia', 1),(38, 'Croquis de los artificios pirotécnicos', 1),(39, 'EN CASO DE PIROTÉCNIA EN EXTERIORES: Croquis y analisis de riesgo en un rango de 500 m', 1),(40, 'EN CASO DE PIROTÉCNIA EN EXTERIORES: Croquis y analisis de riesgo del área en que se detonarán', 1),(41, 'EN CASO DE PIROTÉCNIA EN INTERIORES: Programa de quema', 1),(42, 'EN CASO DE PIROTÉCNIA EN INTERIORES: Copia del Permiso General otorgado por la Secretaria de la Defensa Nacional', 1),(43, 'EN CASO DE PIROTÉCNIA EN EXTERIORES: Copia del permiso de Transporte de sustancias peligrosas otorgado por la SCT', 1),(44, 'EN CASO DE PIROTÉCNIA EN INTERIORES: Copia del Permiso de Autorización de Quema expedido por la delegación correspondiente en los términos de la Ley del Sistema de Protección Civil', 1),(45, 'EN CASO DE PIROTÉCNIA EN INTERIORES: Copia de Póliza de Seguro que ampare la Responsabilidad Civil y daños a terceros', 1),(46, 'EN CASO DE PIROTÉCNIA EN INTERIORES: Constancias de capacitación del personal técnico pirotecnico expedidas por institución o tercer acreditado', 1),(47, 'EN CASO DE PIROTÉCNIA EN INTERIORES: Copia del contrato de prestación de servicios entre el organizador y el permisionario', 1),(48, 'EN CASO DE PIROTÉCNIA EN EXTERIORES: Relación de artificios pirotécnicos especificando cantidad y potencia, así como gráfica de altura y expansión', 1),(49, 'EN CASO DE PIROTÉCNIA EN INTERIORES: Relación del personal pirotécnico, adjuntando documentación que identifique a cada uno', 1),(50, 'EN CASO DE PIROTÉCNIA EN EXTERIORES: Propuesta de distancias mínimas de seguridad y, en su caso, medidas de seguridad adicionales previstas', 1),(51, 'EN CASO DE PIROTÉCNIA EN EXTERIORES: Procedimientos de emergencias responsivas', 1),(52, 'CURP del titular (cedente). Copia', 1),(53, 'Acta de nacimiento del cesionario. Original y copia', 1),(54, 'Comprobante de domicilio del cesionario, no mayor a tres meses de antigüedad. Original y copia', 1),(55, 'CURP del cesioario. Copia', 1),(56, 'CURP. Copia', 1),(57, 'Comprobante de pago de derechos por el uso y autorización de Locales de Mercados Públicos del año de la solicitud y cuatro años anteriores. Copia', 1),(58, 'Acta de Nacimiento. Original y copia', 1),(59, 'Comprobante de domicilio no mayor a tres meses de antigüedad. Original y copia', 1),(60, 'Autorización sanitaria expedida por la Secretaría de Salud (si el comercio lo requiere). Original y copia', 1),(61, 'Documento expedido por autoridad competente para el caso de robo. Original y copia', 1),(62, 'Documento expedido por autoridad competente para el caso de extravio. Original y copia', 1),(63, 'Identificación oficial de la persona que ejercerá la actividad comercial en nombre del títular de la cédula de empadronamiento. Copia', 1),(64, 'Comprobante de pago de la contribución que proceda conforme a lo establecido en el Código Fiscal para el permiso correspondiente. Copia', 1),(65, 'Opinión favorable de la Dirección General de Obras y Desarrollo Urbano, emitida por el Órgano político Correspondiente. Original y copia', 1),(66, 'Opinión favorable de la Dirección General de Protección Civil, emitida por el Órgano Político Correspondiente. Original y copia', 1),(67, 'Dictamen técnico de la Dirección de Patrimonio Cultural Urbano de la Secretaria de Desarrollo Urbano y Vivienda, en su caso de que el inmueble esté catalogado con valor patrimonial. Original y copia', 1),(68, 'Autorización del Instituto Nacional de Antropología e Historia (en caso de que el inmueble esté catalogado con valor historico). Original y copia', 1),(69, 'Autorización del Instituto Nacional de Bellas Artes (en caso de que el inmueble esté catalogado con valor artístico). Original y copia', 1),(70, 'Comprobante de no adeudo al Fideicomiso del Mercado, en su caso, (tratandose de los Mercados Públicos en Auto Administración) correspondiente al año en que se realiza la solicitud y cuatro años anteriores. Copia', 1),(71, 'Copia de la identificación oficial del interesado y/o representante legal', 1),(72, 'Perspectiva y render del inmueble con los anuncios y su entorno', 1),(73, 'Escrito en el que conste la enuencia del propietario del estacionamiento público o lote baldío correspondiente para la instalación de los anuncios en vallas, o en su caso, copia del contrato de arrendamiento entre el poseedor del inmueble y el publicista', 1),(74, 'Aprobación de la Autoridad del Espacio Público del Distrito Federal, del proyecto de incremento de eficiencia del predio para alojar vehículos, cuando se pretenda vallas en un estacionamiento público', 1),(75, 'Copia simple de la credencial para votar, pasaporte, cédula profesional del solicitante, y en su caso, del representante si el solicitante fuera una persona moral', 1),(76, 'Copia simple de la credencial para votar, pasaporte o cédula profesional del propietario y poseedor del inmueble donde se pretenden colocar los anuncios', 1),(77, 'Los documentos previstos para este trámite deberán entregarse tanto en versión digital como impresa, y contener la firma del solicitante', 1),(78, 'Original del comprobante de pago de derechos', 1),(79, 'REMODELACIÓN: Manifestación de construcción Especial', 1),(80, 'Personas Morales:,- Acta Constitutiva, poder notarial e identificación oficial del representante legal o apoderado. Original y copia,Persona Física:,- Poder Notarial e identificación oficial del representante legal o apoderado,- Carta poder firmada ante dos testigos con ratificación en las firmas ante notario público. Original y copia,- Carta poder firmada ante dos testigos e identificación oficial del interesado y de quien realiza el trámite. Original y copia', 1),(81, 'Declaración de Valor Catastral y pago del Impuesto Predial, junto con el comprobante de pago', 1),(82, 'Croquis en dos tantos respecto a la edificación que exista en el predio (2 niveles)', 1),(83, 'Dictamen de seguridad estructural suscrito por un Director Responsable de Obra o Responsable en Seguridad Estructural, según corresponda al Reglamento de Construcciones del DF (más de dos niveles)', 1),(84, 'CONSTRUCCIÓN: Carta compromiso de responsabilidad  firmada por el representante legal', 1),(85, 'CONSTITUCIÓN: Acta constitutiva de la empresa', 1),(86, 'CONSTRUCCIÓN: Manifestación de construcción', 1),(87, 'DEMOLICIÓN: Dictamen del Estudio de Impacto Ambiental, emitido por la Secretaría de Medio ambiente. Copia', 1),(88, 'Dictamen  del Estudio  de Impacto  Urbano  emitido  por la  Secretaría  de Desarrollo Urbano y Vivienda. Copia', 1),(89, 'Póliza de seguros de la empresa y daños contra terceros. Copia', 1),(90, 'Carta responsiva de carga vigente de los extintores a utilizar', 1),(91, 'Copia de la autorización de las autoridades del Trabajo cuando  en  la obra  existan  recipientes  sujetos  a presión,  generador  de  vapor  o caldera', 1),(92, 'CONSTRUCCIÓN: El proyecto ejecutivo deberá estar firmado por el D.R.O y los corresponsables', 1),(93, 'CONSTRUCCIÓN: El proyecto  deberá  integrar  la descripción  general,  ubicación,  entorno  inmediato, superficie total de construcción, además de las correspondientes memorias descriptivas y planos: arquitectónicos,  estructurales,  instalaciones hidráulicas,  sanitarias, eléctricas y de gas', 1),(94, 'CONSTRUCCIÓN: Estratigrafía del terreno con las características de los materiales', 1),(95, 'CONSTRUCCIÓN: Procedimiento   de  excavación,  método  de  estabilización  de  los  taludes  y  la protección a colindancias que señala las precauciones  que deben tomarse para que no resulten afectadas las construcciones, de los predios vecinos o los servicios públicos', 1),(96, 'CONSTRUCCIÓN: Procedimiento  constructivo  de  las  cimentaciones,  que  garantice  la  seguridad durante  y  después  de  la  construcción.   Dicho  procedimiento   debe  evitar  daños  a estructuras   e  instalaciones   vecinas   y  los  servicios   públicos   por   vibraciones   o desplazamiento vertical u horizontal del suelo', 1),(97, 'CONSTRUCCIÓN: Procedimiento de canalización de aguas para mitigar posibles riesgos, en caso de que el Nivel de aguas freáticas se encuentre por encima de la excavación', 1),(98, 'CONSTRUCCIÓN: Procedimiento   de  protección   a  servicios  cercanos   a  la  excavación:  líneas energizadas, tuberías, ductos y demás redes de infraestructura', 1),(99, 'CONSTRUCCIÓN: Riesgos externos, incluyendo  los fenómenos  naturales que podrían afectar a la obra', 1),(100, 'CONSTRUCCIÓN: Croquis donde se identifiquen  las brigadas existentes en la obra en proceso de construcción, remodelación o demolición', 1),(101, 'CONSTRUCCIÓN: Descripción de las medidas de prevención y control en cada fase de la obra de construcción', 1),(102, 'CONSTRUCCIÓN: Descripción de medidas de seguridad para instalaciones  provisionales', 1),(103, 'CONSTRUCCIÓN: Planos  de  conjunto  y  por  niveles,  de  ubicación  de  equipo  contra  incendio, primeros auxilios, rutas de evacuación,  escaleras y salidas de emergencia,  puntos de  reunión,  así  como  vialidades   para  un  rápido  acceso  de  los  cuerpos  de emergencia', 1),(104, 'CONSTRUCCIÓN: Procedimiento   de  resguardo,  protección  y  confinamiento   de  sustancias  y/o materiales  peligrosos,  durante  el proceso  constructivo,  en  base  a la  NOM-006- STPS-2000', 1),(105, 'Planos de localización de zonas de mayor riesgo interno, zonas de menor riesgo y rutas de evacuación', 1),(106, 'CONSTRUCCIÓN: Descripción   del  equipo  de  protección  personal  y  responsabilidades   de  los trabajadores', 1),(107, 'CONSTRUCCIÓN: Procedimiento de seguridad de estructuras de andamios, grúas telescópicas que se utilizaran durante la edificación', 1),(108, 'CONSTRUCCIÓN: Directorio telefónico para requerir servicios de auxilio de bomberos, hospitales, policía y rescate', 1),(109, 'CONSTRUCCIÓN: Planes, manuales y procedimientos  de actuación por tipo de riesgo a que está expuesto  en obra en proceso  de construcción,  remodelación  o  demolición  en el predio', 1),(110, 'CONSTRUCCIÓN: Procedimiento para la evaluación de daños', 1),(111, 'CONSTRUCCIÓN: Planes, manuales y procedimientos  de coordinación,  para el restablecimiento en la obra', 1),(112, 'CONSTRUCCIÓN: Copia  del Dictamen  del Estudio  Impacto  Ambiental,  emitido  por la  Secretaría  de Medio Ambiente', 1),(113, 'REMODELACIÓN: Copia vigente del carnet del Director Responsable de Obra y de los corresponsables  en estructura e instalaciones, así como sus respectivas cartas de responsabilidad', 1),(114, 'CONSTRUCCIÓN: Copia  del  Dictamen  del  Estudio  de Impacto  Urbano  emitido  por la  Secretaría  de Desarrollo Urbano y Vivienda', 1),(115, 'CONSTRUCCIÓN: Cuando  en  la obra  existan  recipientes  sujetos  a presión,  generador  de  vapor  o caldera, se requerirá anexar copia de la autorización de las autoridades del Trabajo', 1),(116, 'Dictamen técnico de instalaciones  eléctricas emitido por una unidad verificadora y/o por corresponsables  de instalaciones', 1),(117, 'Dictamen  técnico de instalaciones  de gas L.P. emitido por una unidad verificadora y/o por corresponsables  de instalaciones', 1),(118, 'REMODELACIÓN: Integrar la descripción general del proyecto, ubicación, entorno inmediato, superficie total de construcción, además de las correspondientes  memorias descriptivas y planos', 1),(119, 'CONSTRUCCIÓN: Cronograma o programa de obra elaborado por meses y partidas', 1),(120, 'CONSTRUCCIÓN: Descripción de los riesgos internos, así como los trabajos de riesgo que se realizan en cada fase de la obra como: demolición,  construcción,  instalaciones  y acabados, se incluyen trabajos en espacios confinados y en alturas', 1),(121, 'REMODELACIÓN: Descripción  de las medidas de prevención,  control en cada fase de la obra de remodelación', 1),(122, 'CONSTRUCCIÓN: Plano de localización de zonas de mayor riesgo interno', 1),(123, 'REMODELACIÓN: Plano de localización de las zonas de menor riesgo', 1),(124, 'REMODELACIÓN: Procedimiento de seguridad de estructuras de andamios', 1),(125, 'CONSTRUCCIÓN: Acciones para la atención de emergencias  en la obra, manual de primeros auxilios,  que incluya  las instrucciones  operativas  para el control  y manejo  de  las emergencias detectadas', 1),(126, 'REMODELACIÓN: Simulacros el cual deberá contener el procedimiento  e instructivo para (incendio, sismo,   inundación,   artefacto   explosivo)   que   incluya   cronograma,   bitácora   y evaluación', 1),(127, 'REMODELACIÓN: Planes, manuales  y procedimientos  de actuación por tipo de riesgo a que está expuesto el predio adecuándolos al mismo', 1),(128, 'REMODELACIÓN: Procedimiento  de coordinación,  para el restablecimiento  en la obra, en caso de emergencia siniestro o desastre', 1),(129, 'CONSTRUCCIÓN: Aplicación  del seguro  para indemnizar  a los afectados;  deberá  contar  con póliza  de  seguro  de  la  empresa  y  daños  contra  terceros  (personas  y  bienes materiales)', 1),(130, 'CONSTRUCCIÓN: Continuidad de operaciones; en caso de suspensión de trabajos por siniestros antrópicos o naturales, el D.R.O. y corresponsables  en coordinación con la Unidad de  Protección   Civil   Delegacional,   definirán   los  rabajos   necesarios   para  no incrementar el nivel de riesgo al estar inconclusos', 1),(131, 'REMODELACIÓN: Copia del Acta Constitutiva de la Empresa', 1),(132, 'CONSTRUCCIÓN: Copia del certificado único de zonificación de uso de suelo especifico, vigente', 1),(133, 'CONSTRUCCIÓN: Copia de constancia de número oficial y alineamiento vigente', 1),(134, 'Copia del Dictamen del Estudio de Impacto Ambiental, emitido por la Secretaría de Medio Ambiente', 1),(135, 'CONSTRUCCIÓN: Copia de la póliza de seguros de la empresa y daños contra terceros', 1),(136, 'Copia de la factura del servicio de recarga de los extintores (anual)', 1),(137, 'REMODELACIÓN: Copia del visto bueno del INBA y/o la licencia del INAH cuando se trate de inmuebles del Patrimonio Histórico, Artístico y Arqueológico de la Federación', 1),(138, 'CONSTRUCCIÓN: Copia del cuestionario  para la clasificación  del grado de riesgo de las  empresas, industrias  o  establecimientos   a  que  se  refiere  el  Capítulo  IV  de  los  Términos  de Referencia para la elaboración de programas Internos de protección civil para obras en proceso de construcción,  remodelación  y demolición', 1),(139, 'Integrar  la  descripción  general  del  proyecto  de  demolición,   ubicación,  entorno inmediato, superficie total de construcción  a demoler, además de las correspondientes memorias descriptivas del procedimiento  de demolición de elementos arquitectónicos  y estructurales,  además  de  la  desinstalación  de  los  suministros  y servicios  de  agua potable, energía eléctrica, drenaje y gas, así como sus respectivos planos', 1),(140, 'DEMOLICIÓN: Cronograma o programa de obra elaborado por meses y partidas', 1),(141, 'DEMOLICIÓN: Descripción de los riesgos internos, así como los trabajos de riesgo que se realizan en cada fase de la demolición, incluyendo trabajos en espacios confinados y en alturas', 1),(142, 'DEMOLICIÓN: Evaluación general', 1),(143, 'DEMOLICIÓN: Procedimiento  de coordinación,  para el restablecimiento  en la demolición, en caso de emergencia siniestro o desastre', 1),(144, 'REMODELACIÓN: Aplicación  del seguro  para indemnizar  a los afectados', 1),(145, 'REMODELACIÓN: Croquis donde se identifiquen las brigadas existentes en el predio', 1),(146, 'Cronograma  y bitácora  del  Programa  de  Capacitación,  deberán  contener  las constancias vigentes', 1),(147, 'CONSTRUCCIÓN: Descripción  de la supervisión  en materia  de Protección  Civil, que  realizará el Director Responsable de Obra y la supervisión de la seguridad en la obra conforme a la NOM-031-STPS-2011', 1),(148, 'DEMOLICIÓN: Descripción   de  las  medidas  de  prevención   y  control  en  cada  fase  de  la demolición', 1),(149, 'REMODELACIÓN: Descripción  de  medidas  de  seguridad  para  instalaciones  provisionales: oficinas,     almacenes,     talleres,     comedores,     servicio     médico,     patios     de almacenamiento,    áreas   de   lavado   de   equipo,   baños,   accesos,   protección perimetral,  instalación  de líneas eléctricas, alumbrado,  conexiones  a tierra, según aplique', 1),(311, 'Memoria  de  Cálculo  Estructural,  será  expedida  en  papel  membretado  de  la Empresa  o del proyectista,  en donde  conste  su número  de cédula  profesional  y firma,  así  como,  la  descripción  del  proyecto,  localización,  número  de  niveles subterráneos  y uso conforme  a lo establecido  en el artículo 53 inciso e), séptimo párrafo  del  Reglamento  de  Construcciones  para  el  Distrito  Federal. Original y copia', 1),(150, 'REMODELACIÓN: Elaboración  de  planos  de  conjunto  y por  niveles,  de  equipo  contra  incendio, botiquín   de  primeros   auxilios,   rutas   de  evacuación,   escaleras   y  salidas   de emergencia   y  de  los  equipos  contra  incendio,  puntos  de  reunión,  así  como vialidades para un rápido acceso de los cuerpos de emergencia. Señalamientos  en materia   de  protección   civil   durante   el   proceso   constructivo   en  base   a  la NOM/003/SEGOB-2011, eñales  y avisos para protección  civil. Colores, formas y símbolos  a  utilizar  y  la  NOM-  002-  STPS-  2012,  Condiciones   de  Seguridad- Prevención y Protección contra incendios en los centros de trabajo', 1),(151, 'Plano de localización de zonas de mayor riesgo interno y plano de localización de las zonas de menor riesgo', 1),(152, 'REMODELACIÓN: Equipo de protección personal y responsabilidades  en materia de protección civil de los trabajadores', 1),(153, 'CONSTRUCCIÓN: Medidas de seguridad en la carga y descarga de material y residuos de obra', 1),(154, 'DEMOLICIÓN: Acciones  para la atención  de emergencias  en el proceso  de  demolición, manual de primeros auxilios, que incluya las instrucciones operativas para el control y manejo de las emergencias detectadas', 1),(155, 'Equipo de primeros auxilios y las brigadas de atención', 1),(156, 'DEMOLICIÓN: Capacitación   referente   al  control   y  manejo   de  emergencias,   que  incluya cronograma, constancias, bitácora y evaluación', 1),(157, 'Planos acotados y a escala de plantas y alzados, estructurales, instalación eléctrica e iluminación; deberán incluir diseño, materiales estructurales, acabados, color, texturas, dimensiones y demás especificaciones, así como uns fotografía del inmueble. A su vez, los pies del plano correspondientes contendrán croquis de ubicación del anuncio, escala gráfica, fecha, nombre del plano y su número, nombres y firmas del solicitante, Director Responsable de Obra, y en su caso, correspondiente', 1),(158, 'Cálculos estructurales y memoria estructural, tratandose de autosoportados', 1),(159, 'Perspectiva y render de la edificación, en la que se considere también el anuncio de que se trate', 1),(160, 'Opinión técnica favorable de la Secretaria de Protección Civil de que el anuncio no representa un riesgo para la integridad física o patrimonial de las personas', 1),(161, 'Copia simple de los recibos de pago del impuesto predial y del derecho de suministro de agua, respectivamente, del inmueble que se trate, correspondientes al bimestre inmediato anterior a la fecha de la solicitud', 1),(162, 'Declaración bajo protesta de decir la verdad del responsable de la obra, relativa a la instalación y demás circunstancias que deriven de la instalación del anuncio de que se trate, donde señale que no se afectan árboles con motivo de las obras que se puedan llevar a cabo ni en las instalaciones de los anuncios', 1),(163, 'Planos acotados y a escala: a). De plantas, alzados y cortes de los anuncios; b). Estructurales, en su caso; c). De instalación eléctrica, de iluminación, y en su caso, del sistema electrónico, y d). De diseño gráfico de la placa de identificación y su ubicación en el tapial; Los planos deberán incluir diseño, dimensiones, materiales estructurales, acabados, color y texturas y en su caso, el tipo, material y dimensiones de la estructura de soporte de los anuncios. A su vez, los pies de plano correspondientes deben contener croquis de ubicación del anuncio, escala gráfica, fecha, nombre del plano y su número, nombre y firma del solicitante', 1),(164, 'Perspectiva o render: a) del anuncio en tapial individualmente considerado, y b) del tapial con los anuncios instalados', 1),(165, 'Copia simple del recibo de pago de impuestos predial del inmueble de que se trate, correspondiente al bimestre inmediato anterior a la fecha de solicitud, o en su caso al ejercicio fiscal en curso, o en su caso la constancia de adeudo', 1),(166, 'Copia simple del recibo de pago del derecho de suministro de agua del inmueble de que se trate, correspondiente al bimestre inmediato anterior a la fecha de la solicitud, o en su caso constancia de adeudo', 1),(167, 'Copia simple del contrato de arrendamiento entre el poseedor o propietario del inmueble y el solicitante, en su caso', 1),(168, 'Declaración bajo protesta de decir la verdad del responsable de la obra, relativa a la instalación del anuncio de que se trate, donde señale que no se afectarán árboles con motivo de las obras que se puedan llevar a cabo ni en las instalaciones de los anuncios', 1),(169, 'Copia simple de la manifestación de construcción, registrada por la delegación correspondiente', 1),(170, 'Cuando el solicitante sea una persona moral, copia certificada de la escritura pública que acredite su constitución y copia certificada de la escritura pública que acredite la designación de su representante', 1),(171, 'Copia simple de la identificación del propietario y poseedor del inmueble donde se pretender colocar los anuncios', 1),(172, 'CONSTRUCCIÓN: Copia del carnet del Director Responsable de Obra y en su caso de cada corresponsable', 1),(173, 'En su caso: I. Proyecto de propuesta de recuperación y mantenimiento de áreas verdes y/o espacios públicos. II. Manifestación de voluntad para la futura suscripción del instrumento jurídico', 1),(174, 'Opinión técnica favorable de la Secretaria de Protección Civil, cuando el anuncio forme parte de una lona, manta, malla u otro material flexible que se pretenda instalar sobre la edificación en proceso de construcción o remodelación, o en andamios que la circunden, con una altura y longitud homólogas a las de la edificación', 1),(175, 'Constancia de alineamiento y número oficial vigente, excepto para apertura de claros de 1.5 m como máximo en construcciones hasta de dos niveles, si no se afectan elementos estructurales y no se cambia total o parcialmente el uso o destino del inmueble; e instalación o construcción de cisternas, fosas sépticas o albañales. Original y copia', 1),(176, 'Plano o croquis que contenga la ubicación, superficie del predio, metros cuadrados por construir, distribución y dimensiones de los espacios, área libre, y en su caso, N° de cajones de estacionamiento. Original y copia', 1),(177, 'Registro de intervención registrado por la Secretaria de Desarrollo Urbano y de Vivienda, cuando el inmueble se encuentre en área de conservación patrimonial del Distrito Federal. Original y copia', 1),(178, 'Dictamen ténico favorable de la Secretaria de Desarrollo Urbano y Vivienda cuando se trate de áreas de conservación patrimonial y/o inmuebles afectados al patrimonio cultural urbano o sus colindantes; y/o Vo.Bo del Instituto Nacional de Bellas Artes y/o la licencia del Instituto Nacional de Antropología e Historia para el caso de un monumento histórico, artístico o arqueológico, según sea su ámbito de competencia de acuerdo con lo establecido en la Ley Federal en materia. Original y copia', 1),(179, 'Constrancia de adeudos de predial y agua emitida por la Administración Tributaria y de Sistema de Aguas de la Ciudad de México en las que se acredite que se encuentran al corriente de sus obligaciones. Original y copia', 1),(180, 'Para el caso de ampliación de una vivienda unifamiliar, cuya edificación original cuente con licencia de construcción, registro de obra ejecutada o registro de manifestación de construcción, siempre y cuando no se rebasen; el área total de 120m2 de construcción, incluyendo la ampliación; dos niveles, 5.5m de altura y claros libres de 4m, presentar licencia de construcción o registro de obra ejecutada de la edificación original, o en su caso, el registro de manifestación de construcción, asó como indicar en el plano o croquis, la edificación original y el área de ampliación. Original y copia', 1),(181, 'Comptrobante de pago de derechos por la prórroga. Original y copia', 1),(182, 'Comprobante de pago de derechos, cuando se trate de modificación equivalente al 20% de los derechos causadospor el registro, análisis y estudio de manifestación de construcción. Original y copia', 1),(183, 'Boleta predial del último bimestre de cada inmueble involucrado. Original y copia', 1),(184, 'Escritura de propiedad del o de los inmuebles. Copia certificada y copia simple', 1),(185, 'Croquis en original y dos tantos que contengan en la parte superior, la situación actual del o de los inmuebles, consignando las calles colindantes, la superficie y linderos reales del predio y el anteproyecto consignando también las calles colindantes, la superficie y linderos del predio o predios resultantes', 1),(186, 'Avalúo vigente de los terrenos, para el cálculo de los derechos. Original y copia', 1),(187, 'Comprobante de pago del 1% del valor del avalúo para la licencia de Relotificación en original y copia, el cual debe presentar posterior al ingreso de la solicitud una vez que la autoridad informe al interesado el monto a pagar', 1),(188, 'En  el  caso  de  que  requiera  estudio  de  impacto urbano  o  urbano-ambiental, dictamen aprobatorio de la Secretaría. Original y copia', 1),(189, 'Registros de declaración de apertura o licencias de funcionamiento, en su caso. Original y copia', 1),(190, 'Registro de manifestación de construcción, en su caso. Original y copia', 1),(191, 'Licencia de Construcción Especial, en su caso. Original y copia', 1),(192, 'Croquis de localización del polígono a relotificar, a escala de 1:500 a 1:5000, según sea su dimensión. Original y copia', 1),(193, 'Memoria descriptiva, impresa y en medio magnéticosea su dimensión. Original y copia', 1),(194, 'Relación de propietarios e interés, con expresión de la naturaleza y cuantía de su derecho; impresa y en medio magnético. Original y copia', 1),(195, 'Propuesta de adjudicación de inmuebles resultantes, con determinación de su uso  y  designación nominal  de  los  adjudicatarios; impresa  y  en  medio  magnético. Original y copia', 1),(196, 'Avalúo de los inmuebles que se adjudicarán; impreso y en medio magnético. Original y copia', 1),(197, 'Avalúo de los derechos, edificaciones, construcciones o plantaciones que deben extinguirse o destruirse para la ejecución del proyecto de relotificación, impreso y en medio magnético. Original y copia', 1),(198, 'Cuenta de liquidación provisional, impresa y en medio magnético. Original y copia', 1),(199, 'Planos catastrales con división de predios, impreso y en medio magnético. Original y copia', 1),(200, 'Plano de situación y relación con el  entorno urbano, impreso y en medio magnético. Original y copia', 1),(201, 'Plano  de  delimitación del  polígono  a  relotificar, en  el  que  se  expresen su superficie en metros cuadrados, los límites del polígono, los linderos de los terrenos afectados, construcciones y demás elementos existentes sobre el terreno, impreso y en medio magnético. Original y copia', 1),(202, 'Planos de zonificación que contengan la expresión gráfica de las normas de ordenación a que se refieren los Programas, impreso y en medio magnético. Original y copia', 1),(203, 'Plano de clasificación y avalúo de las superficies adjudicadas,  impreso y en medio magnético. Original y copia', 1),(204, 'Proyecto de Relotificación. Original y copia', 1),(205, 'Planos impresos que se entregarán en una escala comprendida entre 1:500 y 1:5000, con la calidad suficiente para que puedan percibirse los linderos y la simbología utilizada, impreso y en medio magnético', 1),(206, 'Comprobante de Pago del 10% de los derechos causados por su expedición para la prórroga de la Licencia de Relotificación. Original y copia', 1),(207, 'Escritura  de  propiedad  del  o  de  los  inmuebles  que  pretende  subdividir. Copia certificada y copia simple', 1),(208, 'Comprobante de pago de los derechos de la Licencia de Subdivisión o Fusión en original y copia, el cual debe presentar posterior al ingreso de la solicitud una vez que la autoridad informe al interesado el monto a pagar', 1),(209, 'Comprobante de pago de derechos para la prórroga de la Licencia de Subdivisión o Fusión. Original y copia', 1),(210, 'Licencia de Subdivisión o fusión anterior. Original y copia', 1),(211, 'Constancia de Seguridad Estructural anterior. Original', 1),(212, 'Para el caso de supresiones, Constancia de Adeudos con la leyenda "NO REGISTRA" correspondiente al bimestre en que se está solicitando la supresión, este trámite no tiene costo.', 1),(213, 'Refrendo correspondiente al año en que se realiza la solicitud. Original y copia', 1),(214, 'Comprobante de pago de derechos, de acuerdo al tipo de obra, el cual debe  presentarse  posterior  al  ingreso  de  la  solicitud  una  vez  que  la autoridad informe al interesado el monto a pagar. Original y copia', 1),(215, 'Avalúo emitido por un valuador registrado ante la Secretaría de Finanzas. Original y copia', 1),(216, 'Comprobante de pago de la sanción equivalente del 5 al 10% del valor de las  construcciones  en  proceso  o  terminadas,  el  cual  debe  presentar posterior al  ingreso de la solicitud  una  vez que  la  autoridad  informe  al interesado el monto a pagar', 1),(217, 'Demás  documentos  que  el  Reglamento  de  Construcciones  para  el Distrito Federal y otras disposiciones exijan para el registro de manifestación de construcción o para expedición de licencia de construcción especial, con las responsivas de un Director de Obra, y de los Corresponsables (se encuentran dentro de este formato), en su caso. De acuerdo al artículo 72 del Reglamento de Construcciones para el Distrito Federal', 1),(312, 'Proyecto de protección a colindancias firmados por el proyectista indicando su número  de cédula  profesional,  así como  el Director  Responsable  de Obra  y el Corresponsable en Seguridad Estructural, en su caso. Original y copia', 1),(313, 'Estudio de mecánica  de suelos del predio de acuerdo  con los alcances  y lo establecido en las Normas Técnicas Complementarias  para Diseño y Construcción de Cimentaciones  del Reglamento,  incluyendo los procedimientos constructivos de la excavación, muros de contención y cimentación,  así como las recomendaciones de  protección  a colindancias.  El  estudio  debe  estar  firmado  por  el especialista indicando su número de cédula profesional,  así como por el Director Responsable de  Obra  y  por  el  Corresponsable   en  Seguridad  Estructural,  en  su  caso.  (por duplicado)', 1),(218, 'Constancia de Adeudos de Predial y Agua emitida por la Administración Tributaria y el Sistema de Aguas de la Ciudad de México en la que se acredite que se encuentran al corriente de sus obligaciones. (original y copia) En caso, de que la Licencia de Construcción Especial se solicite para que la  obra  se  realice en la vía pública no  serán necesarias  presentar las Constancias de Adeudos', 1),(219, 'Certificado único de zonificación de uso de suelo o certificado único de zonificación del suelo digital o certificado de acreditación de uso del suelo por derechos adquiridos, los cuales deberán ser verificados y firmados por el Director Responsable de Obra y/o Corresponsable en Diseño Urbano y Arquitectónico, en su caso. Original y copia', 1),(220, 'Proyecto alternativo de captación y aprovechamiento de aguas pluviales y de tratamiento de aguas residuales aprobados por el Sistema de Aguas de la Ciudad de México. Original y copia', 1),(221, 'Dos tantos del proyecto arquitectónico de la obra en planos a escala, debidamente acotados y con las especificaciones de los materiales, acabados y equipos a utilizar, en los que se debe incluir, como mínimo: croquis  de  localización  del  predio,  levantamiento  del  estado  actual, indicando las construcciones y árboles existentes; planta de conjunto, mostrando los límites del predio y la localización y uso de las diferentes partes edificadas y áreas exteriores; plantas arquitectónicas, indicando el uso de los distintos locales y las circulaciones, con el mobiliario fijo que se requiera; cortes y fachadas; cortes por fachada, cuando colinden en vía pública y detalles arquitectónicos interiores y de obra exterior. Deberán estar firmados por el propietario o poseedor, por el proyectista indicando su número de cédula profesional, por el Director Responsable de Obra y el Corresponsable en Diseño Urbano y Arquitectónico, en su caso', 1),(222, 'Memoria de cálculo en la cual se describirán con el nivel de detalle suficiente para que puedan ser evaluados por un especialista externo al proyecto, debiéndose respetar los contenidos señalados en lo dispuesto en la memoria estructural consignada en el artículo 53 fracción I, inciso e) del Reglamento. Original y copia', 1),(223, 'Proyecto  de  protección  a   colindancias   y   estar  firmados   por  el proyectista  indicando  su  número  de  cédula  profesional,  así  como  el Director Responsable de Obra y el Corresponsable en Seguridad Estructural, en su caso. Original y copia', 1),(224, 'Dos tantos del estudio de mecánica de suelos del predio de acuerdo con  los  alcances  y  lo  establecido  en  las  Normas  Técnicas Complementarias para Diseño y Construcción de Cimentaciones del Reglamento, incluyendo los procedimientos constructivos de la excavación, muros de contención y cimentación, así como las recomendaciones de protección  a  colindancias.  Deberá  estar  firmado  por  el  especialista indicando su número de cédula profesional, así como por el Director Responsable de Obra y por el Corresponsable en Seguridad Estructural, en su caso', 1),(225, 'Libro de bitácora de obra foliado, para ser sellado por la Secretaría de Desarrollo Urbano y Vivienda o la Delegación, el cual debe conservarse en la  obra,  realizando  su  apertura  en  el  sitio  con  la  presencia  de  los autorizados para usarla, quienes lo firmarán en ese momento. Original y copia', 1),(226, 'Responsiva del Director Responsable de Obra del proyecto de la obra, así como de los Corresponsables en Seguridad Estructural, en Diseño Urbano  y  Arquitectónico  e  Instalaciones,  las  cuales  se  encuentran incluidas en este formato', 1),(227, 'Dictamen  favorable del estudio  de  impacto  ambiental,  en  su  caso. Original y copia', 1),(228, 'Póliza vigente del seguro de responsabilidad civil por daños a terceros en las obras clasificadas en el Grupo A y Subgrupo B1, según el artículo 139 de este Reglamento. Por un monto asegurado no menor del 10% del costo total de la obra construida por el tiempo de vigencia de la licencia de construcción especial. Original y copia', 1),(229, 'Cinco   tantos   de   los   planos   arquitectónicos,   estructurales   y   de instalaciones y cinco tantos en versión digital de los mismos', 1),(230, 'Memorias de cálculo respectivas, signados por el Director Responsable de Obra y del Corresponsable en Instalaciones, cuando se trate de obras para la conducción de fluidos eléctricos, gas natural, petroquímicos y petrolíferos. El proyecto deberá ser formulado de conformidad con las Normas y demás disposiciones aplicables en la materia y autorizado por la Secretaría de Obras y Servicios. Original y copia', 1),(231, 'Memoria descriptiva y de instalaciones signadas por el Director Responsable de Obra y del Corresponsable en Instalaciones cuando se trate de obras para la conducción de fluidos eléctricos, gas natural, petroquímicos y petrolíferos. El proyecto deberá ser formulado de conformidad  con  las  Normas  y  demás  disposiciones  aplicables  en  la materia y autorizado por la Secretaría de Obras y Servicios. Origianl y copia', 1),(232, 'Vo.Bo  de las  áreas  involucradas  de  la  Administración  Pública Federal y/o local, de conformidad con las disposiciones aplicables. Original y copia (STC metro, PEMEX, CFE, AGU, Secretaría de Obras y Servicios de la Ciudad de México)', 1),(233, 'En caso de que la obra se realice dentro del perímetro del Centro Histórico deberá presentar Visto Bueno del Subcomite de Instalaciones subterráneas del Centro Histórico de la Ciudad de México. Original y copia', 1),(234, 'Certificado único de zonificación de uso de suelo o certificado único de zonificación del suelo digital o certificado de zonificación de usos del suelo específico  o  certificado  de acreditación  de  uso  del  suelo  por  derechos adquiridos. Original y copia', 1),(235, 'Cinco tantos y cinco versiones en archivo electrónico de los planos arquitectónicos, estructurales, de instalaciones', 1),(314, 'Para el caso de las edificaciones  que pertenezcan  al grupo A o subgrupo B1, según el artículo 139 del Reglamento,  o para las edificaciones  del subgrupo B2, acuse de ingreso de la orden de revisión  del proyecto  estructural  emitido por el Instituto  para  la  Seguridad  de  las  Construcciones  de  la  Ciudad  de  México. Original y copia', 1),(315, 'Responsiva  del Director  Responsable  de Obra del proyecto  de la obra,  así como de los Corresponsables  en los supuestos  señalados  en el  artículo  36 del Reglamento. (se encuentra en este formato de solicitud)', 1),(236, 'Memoria de cálculo signado por el Director Responsable de Obra y el o los Corresponsables en su caso. El proyecto debe ser formulado de conformidad  con  las  Normas  y  demás  disposiciones  aplicables  en  la materia. Original y copia', 1),(237, 'Memorias descriptivas, signados por el Director Responsable de Obra y el o los Corresponsables en su caso. (El proyecto debe ser formulado de conformidad con las Normas y demás disposiciones aplicables en la materia. Original y copia', 1),(238, 'Dictámenes de la Secretaría de Desarrollo Urbano y Vivienda y de las demás dependencias, órganos o entidades de la Administración Pública Federal y/o local que señalen las disposiciones en la materia. Original y copia', 1),(239, 'Documento que acredite la propiedad del inmueble. Original y copia', 1),(240, 'Constancia de alineamiento y número oficial vigente. Original y copia', 1),(241, 'Libro de bitácora de obra foliado para ser sellado por la Secretaría de Desarrollo Urbano y Vivienda o la Delegación correspondiente,  el cual debe conservarse en la obra, realizando su apertura en el sitio con la presencia de los autorizados para usarla, quienes lo firmaran en ese momento', 1),(242, 'Memoria descriptiva del procedimiento que se vaya a emplear y la indicación del sitio de disposición donde se va a depositar el material producto de la demolición, documentos que deberán estar firmados por el Director Responsable de Obra y el Corresponsable en Seguridad Estructural, en su caso. Original y copia', 1),(243, 'Medidas de protección a colindancias. Original y copia', 1),(244, 'Programa en el que se indicará el orden en que se realizará cada una de las etapas de los trabajos, el volumen estimado y fechas aproximadas en que se demolerán los elementos de la edificación. En caso de prever el uso de explosivos, el programa señalará con toda precisión él o los días y la hora o las horas en que se realizarán las explosiones, que estarán sujetas a la aprobación de la Delegación. Original y copia', 1),(245, 'Cualquier demolición en zonas declaradas de Monumentos Históricos, Artísticos y Arqueológicos de la Federación o cuando se trate de inmuebles afectos al patrimonio cultural urbano y/o ubicados dentro del Área de Conservación Patrimonial de la Ciudad de México requerirá, previo a la licencia de construcción especial para demolición, la autorización por parte de las autoridades federales que correspondan y el dictamen técnico favorable  de  la  Secretaría  de  Desarrollo  Urbano  y  Vivienda,  debiendo contar en todos los casos, con responsiva de un Director Responsable de Obra y de los Corresponsables. Original y copia', 1),(246, 'Presentar  acuse  de  recibo  de  la  Declaratoria  Ambiental  ante  la Secretaría del Medio Ambiente. Original y copia', 1),(247, 'Responsiva del Director Responsable de Obra y los Corresponsables, en su caso, las cuales se encuentran incluidas en este formato', 1),(248, 'Para el caso, de instalaciones o modificaciones en edificaciones existentes, de ascensores para personas, montacargas, escaleras mecánicas o cualquier otro mecanismo de transporte electromecánico, se deberá acompañar con los datos referentes a la ubicación del edificio y el tipo de servicios a que se destinará, así como dos juegos completos de planos,  especificaciones  y bitácora proporcionados  por la  empresa que fabrique el aparato, y de una memoria donde se detallen los cálculos que hayan sido necesarios', 1),(249, 'Comprobante de Pago de Derechos para la Prórroga de la Licencia de Construcción Especial. Original y copia', 1),(250, 'Dos copias legibles de los planos y archivo electrónico de los mismos, en caso, de que los trabajos amparados con licencia de construcción especial en la vía pública contengan modificaciones', 1),(251, 'En su caso, Vo.Bo de Seguridad y Operación con la responsiva de un Director Responsable de Obra y del Corresponsable en Instalaciones. Original y copia', 1),(252, 'Documento que acredite la personalidad del representante legal, de resultar aplicable. (Acta Constitutiva, Poder Notarial, Carta Poder). Original y copia', 1),(253, 'Pago de los derechos correspondientes por los metros cuadrados de construcción adicional, de acuerdo con el Código Fiscal de la Ciudad de México. Original y copia', 1),(254, 'Comprobante de pago de derechos correspondientes. Original y copia', 1),(255, 'En su caso, carta de corresponsabilidad del tercer acreditado. Que contenga lo siguiente: I. Nombre, domicilio y número de registro vigente del Tercer Acreditado que la expide; II. Vigencia de la carta de corresponsabilidad, la cual no podrá ser inferior a un año; III. Actividades que ampara la carta de corresponsabilidad; IV. Firma original de otorgamiento, y  V. Manifestación expresa de la responsabilidad solidaria que tiene el Tercero Acreditado con el obligado y el periodo que comprenda', 1),(256, 'Croquis y/o planos especificando la ubicación del inmueble y sus alrededores', 1),(257, 'Croquis y/o planos de la descripción de las áreas existentes en el inmueble, señalando los riesgos internos', 1),(258, 'Croquis señalando las rutas de evacuación, salidas de emergencia y zonas de menor riesgo, identificación de los sistemas de alertamiento', 1),(316, 'Para el caso de construcciones  que requieran la instalación de tomas de agua y conexión a la red de drenaje, la solicitud y comprobante  del pago de derechos. Original y copia', 1),(317, 'Dictamen de Factibilidad de Servicios Hidráulicos. Original y copia', 1),(318, 'Dictamen   favorable   del  estudio  del  impacto   urbano  o  impacto   urbano- ambiental, en su caso. Original y copia', 1),(319, 'Presentar acuse de recibo de la Declaratoria  Ambiental  ante la Secretaría del Medio  Ambiente,  cuando  se  trate  de  proyectos  habitacionales  de  más  de  20 viviendas. Original y copia', 1),(320, 'En  zonas   de  conservación   patrimonial   con   valor   histórico,   artístico   o arqueológico, licencia del Instituto Nacional de Antropología e Historia, visto bueno del Instituto  Nacional  de Bellas Artes o dictamen  de la Secretaría  de Desarrollo Urbano y Vivienda, en su caso. Original y copia', 1),(321, 'Comprobante de pago de derechos por la prórroga', 1),(259, 'Croquis señalando la distribución de equipo contra incendios y señalización', 1),(260, 'Cronograma y bitácora del programa de capacitación, deberán contener las constancias', 1),(261, 'Croquis donde se identifiquen las brigadas existentes en el inmueble', 1),(262, 'Registro del mantenimiento y control del equipo de prevención y combate de incendios. ', 1),(263, 'DEMOLICIÓN: Última factura de recarga de extintores', 1),(322, 'En caso de existir diferencias  entre la obra ejecutada y los planos registrados, se deberá anexar dos copias de los planos que contengan dichas modificaciones; siempre  y  cuando  no  se  afecten  las  condiciones   de  seguridad,   estabilidad, destino,  uso,  servicios,  habitabilidad   e  higiene,  se  respeten  las  restricciones indicadas en el Certificado Único de Zonificación de Uso del Suelo, la Constancia de Alineamiento y las características  de la manifestación  registrada, así como, las tolerancias que fija el Reglamento y sus Normas', 1),(323, 'En caso de modificaciones, comprobante de pago de derechos equivalente al 20% de los derechos causados por el registro, análisis y estudio de la manifestación de construcción. Original y copia', 1),(324, 'Constancia de Seguridad Estructural sólo cuando el inmueble pertenezca al Grupo A o Subgrupo B1, de conformidad con el Artículo 139 fracciones I y II  inciso  a)  del  Reglamento  de  Construcciones  para  el  Distrito  Federal', 1),(325, 'En su caso, los resultados de las pruebas a las que se refieren los artículos 185 y 186 del Reglamento de Construcciones para el Distrito Federal, cuando sea necesario comprobar la seguridad de una estructura por medio de pruebas de carga en los siguientes casos:,*En las obras provisionales o de recreación que puedan albergar a más de 100  personas;  determinado  por  el  dictamen  técnico  de  estabilidad  o seguridad estructural expedido por un Corresponsable en Seguridad Estructural.,*Cuando no exista suficiente evidencia teórica o experimental para juzgar en forma confiable la seguridad de la estructura en cuestión, y,*Cuando la Delegación previa opinión de la Secretaría de Obras y Servicios lo determine conveniente en razón de duda en la calidad y resistencia de los materiales o en cuanto al proyecto estructural y a los procedimientos constructivos', 1),(326, 'Aviso de Visto bueno de seguridad y operación anterior', 1),(327, 'Documento con el que el solicitante acredite su interés legítimo', 1),(328, 'Documento del que se solicita la certificación', 1),(329, 'Comptrobante de pago de derechos', 1),(330, 'En caso de que los comprobantes de domicilio no se encuentren a nombre del interesado, se deberá presentar una manifestación por escrito del titular del inmueble y copia de su identificación oficial, de que el solicitante reside en el domicilio señalado desde hace más de 6 meses; o dos cartas testimoniales de dos vecinos y sus respectivas identificaciones oficiales y comprobantes de domicilio a nombre de los mismos, manifestando bajo protesta de decir verdad que conocen y que el solicitante reside en el domicilio señalado o cualquier otra prueba que lo acredite', 1),(331, 'En caso de menor de edad su acta de nacimiento; identificación oficial y comprobante de domicilio del padre o tutor', 1),(332, 'Comprobante de pago de la constancia de alineamiento y/o N° oficial en original y copia, el cual debe presentar posterior al ingreso de la solicitud una vez que la autoridad informe al interesado el monto a pagar', 1),(333, 'Documento que acredite la propiedad o posesión del predio. Original y copia', 1),(334, 'Carta de responsabilidad expedida por el obligado a realizar el Programa Interno del Inmueble o corresponsabilidad del tercer acreditado. Carta de responsabilidad del tercer acreditado que contenga lo siguiente:,I. Nombre, domicilio y N° de registro vigente del tercer acreditado que la expide,II. Vigencia de la carta de corresponsabilidad, la cual no podrá ser inferior a un año,III. Actividades que ampara la carta de corresponsabilidad,IV. Firma original del otorgamiento,V. Manifestación expresa de la responsabilidad solidaria que tiene el Tercero Acreditado con el obligado y el periodo que comprenda', 1),(335, 'Planos legibles de ubicación de las señalizaciones de obligación, precaución y prohibitivas o restrictivas', 1),(336, 'Organigrama del Comité Interno de Protección Civil', 1),(337, 'Evaluación y análisis de riesgos', 1),(338, 'Identificación de los sistemas de alertamiento', 1),(339, 'Planos legibles señalando las rutas de evacuación, salidas de emergencia y zonas de menor riesgo, puntos de reunión', 1),(340, 'Acta Constitutiva (Documento de Integración del Comité Interno de Protección Civil)', 1),(341, 'Planos legibles señalando la distribución de equipo contra incendios y señalización', 1),(342, 'Cronograma y bitácora del programa de capacitación de los integrantes del Comité Interno y/o brigadas del inmueble, deberán contener las constancias vigentes', 1),(343, 'Tabla del código de colores para la identificación de las brigadas', 1),(344, 'Carta responsiva emitida por la empresa que da el servicio especificando que es lo que proporciona, recarga y mantenimiento de extintores', 1),(345, 'Resgistro del mantenimiento y control del equipo de prevención y conbate de incendios. Bitácoras con fecha y firmas del responsable del mantenimiento y responsable del inmueble', 1),(346, 'Cronograma y bitácora de mantenimiento en general, y registro del mantenimiento preventivo y correctivo mediante bitácoras, reforzandolo con reporte fotográfico, fecha y firma del responsable', 1),(347, 'Cronograma y bitácora de simulacros, estos deberán de estar sustentados con reporte fotográfico con formato de evaluación del ejercicio y fecha', 1),(348, 'Última factura de recarga de extintores', 1),(349, 'Bitácora de incidentes dentro de las instalaciones de la unidad hospitalaria', 1),(350, 'Planos legibles de ubicación de los equipos de primeros auxilios', 1),(351, 'Vo.Bo de Seguridad y Operación y/o dictamen estructural, deberá estar firmado por el DRO y por corresponsable', 1),(352, 'Planes, manuales y procedimientos de restablecimiento', 1),(353, 'Para los recipientes sujetos  a presión, generador de vapor o caldera, se requerirá anexar copia de la autorización de las autoridadesdel Trabajo.', 1),(354, 'Formatos  de  Inspección  rápida  visual  y  física  para  evaluar  daños  del inmueble después de una emergencia, siniestro o desastre.', 1),(355, 'Copia  del  cuestionario  para  la  clasificación  del  grado  de  riesgo  de  las empresas, industrias o establecimientos a que se refiere el Capítulo III de los Términos de Referencia, con la carta firmada en original.', 1),(356, 'Protocolos  y  bitácoras  de  transporte,  identificación  y  almacenamiento  de sustancias químicas peligrosas y residuos peligrosos biológicos infecciosos (R.P.B.I.).', 1),(357, 'Copia del estudio de impacto ambiental en el caso de las empresas que de conformidad a la Ley Ambiental estén obligadas a ello.', 1),(358, 'Factura  instalación  del  sistema  de  alertamiento  sísmico  y/o  evidencia fotográfica de su instalación y mantenimiento general.', 1),(359, 'Bitácoras de mantenimiento de instalaciones eléctricas, sanitarias, hidráulicas y especiales, del último mes', 1),(360, 'Control ecológico de plagas vigente', 1),(361, 'Planos legibles de la descripción e identificación de las áreas existentes en el inmueble, señalando cada uno de los riesgos internos', 1),(362, 'Calendario de capacitación ejercicio (conforme al año corriente)', 1),(363, 'Responsiva  de  aplicación  de  mica  antiestallante  o  factura  de  vidrios templados', 1),(364, 'Oficio de no modificación o cambios estructurales o licencia de construcción especial', 1),(365, 'Comprobante de pago de los derechos correspondientes', 1),(366, 'Comprobante de no adeudo al Fideicomiso del Mercado. Correspondiente al año que se realiza la solicitud y cuatro años anteriores', 1),(367, 'Identificación oficial del titular (carta de naturalización  o cartilla de servicio militar o cédula profesional  o pasaporte o certificado  de nacionalidad mexicana o credencial para votar o licencia para conducir) original y copia', 1),(368, 'Identificación oficial del solicitante (carta de naturalización  o cartilla de servicio militar o cédula profesional  o pasaporte o certificado  de nacionalidad mexicana o credencial para votar o licencia para conducir). Original y copia', 1),(369, 'Contrato y registro vigente del particular a cargo de los servicios de seguridad privada', 1),(370, 'REMODELACIÓN: Procedimiento   de  resguardo,  protección  y  confinamiento   de  sustancias  y/o materiales peligrosos, durante el proceso de remodelación,  en base a la NOM-006- STPS-2000', 1),(371, 'DEMOLICIÓN: Procedimiento   de  resguardo,  protección  y  confinamiento   de  sustancias  y/o materiales peligrosos, durante el proceso de remodelación,  en base a la NOM-006- STPS-2000', 1),(372, 'Planes, manuales y procedimientos de actuación por tipo de riesgo a que está expuesto el inmueble adecuándolos al mismo', 1),(373, 'Permiso o aviso expedido por la Secretaría de Desarrollo Económico que ampare el fiuncionamiento del establecimiento mercantil y/o revalidación correspondiente. Copia', 1),(374, 'Vo.Bo para la celebración de espectáculos públicos masivos en lo relativo a extintores, señalización para el caso de incendio y sismos, rutas de evacuación y salidas de emergencia. Original y copia', 1),(375, 'Cuando la naturaleza y clase del espectáculo público lo requiera la autorización de la asociación o sociedad de autores o compositores que corresponda, para los efectos de los derechos de autor o del propio titular. Original y copia', 1),(376, 'Documento que acredite que el solicitante fue designado por la comunidad para llevar la organización del festejo. Original y copia', 1),(377, 'Construcciones de vivienda unifamiliar hasta 2 niveles, presentar croquis en dos tantos respecto a la edificación que existe en el predio, señalando superficie de construcción a regularizar', 1),(378, 'Construcciones de vivienda plurifamiliar que tengan más de dos niveles, deberán presentar dictamen de seguridad estructural suscrito por un Director Responsable de Obra o por un Corresponsable de Seguridad Estructural, según corresponda de conformidad con el reglamento de construcciones del Distrito Federal. Lo anterior acompañado de dos juegos de los planos arquitectónicos (plantas, cortes y fachadas, en su caso)', 1),(379, 'CONSTRUCCIÓN: Carta responsiva emitida por la empresa que da el servicio especificando  que es lo que   proporciona,   recarga   y  mantenimiento   de   extintores', 1),(380, 'CONSTRUCCIÓN: Comprobante de pago de los derechos correspondientes', 1),(381, 'CONSTRUCCIÓN: Acta Constitutiva  (Documento  de Integración  del Comité Interno de Protección Civil)', 1),(382, 'CONSTRUCCIÓN: Cronograma  y bitácora  de  simulacros.  Estos  deberán  estar  sustentados  con reporte fotográfico con formato de fecha registrado firmado por el responsable', 1),(383, 'CONSTRUCCIÓN: Documento con el que acredite la personalidad en caso de actuar con caracter de representante legal, mandatario o apoderado, en original y una copia simple. Original y copia', 1),(384, 'CONSTRACCIÓN: Cronograma  y bitácora  del  Programa  de Capacitación,  deberán  contener  las constancias vigentes', 1),(385, 'CONSTRUCCIÓN: Organigrama del Comité Interno de Protección Civil', 1),(386, 'CONSTRUCCIÓN: Tabla del código de colores para la identificación de las brigadas', 1),(387, 'CONSTRUCCIÓN: Identificación oficial del solicitante (carta de naturalización  o cartilla de servicio militar o cédula profesional  o pasaporte o certificado  de nacionalidad mexicana o credencial para votar o licencia para conducir). Original y copia', 1),(388, 'REMODELACIÓN: Acciones para la atención de emergencias  en la obra, manual de primeros auxilios,  que incluya  las instrucciones  operativas  para el control  y manejo  de las emergencias', 1),(389, 'REMODELACIÓN: Acta Constitutiva  (Documento  de Integración  del Comité Interno de Protección Civil)', 1),(390, 'REMODELACIÓN: Carta compromiso de responsabilidad  firmada por el representante legal', 1),(391, 'REMODELACIÓN: Carta responsiva emitida por la empresa que da el servicio especificando  que es lo que   proporciona,   recarga   y  mantenimiento   de   extintores', 1),(392, 'REMODELACIÓN: Continuidad de operaciones; en caso de suspensión de trabajos por siniestros antrópicos o naturales', 1),(393, 'REMODELACIÓN: Copia  del  Dictamen  del  Estudio  de Impacto  Urbano  emitido  por la  Secretaría  de Desarrollo Urbano y Vivienda', 1),(394, 'REMODELACIÓN: Copia  del Dictamen  del Estudio  Impacto  Ambiental,  emitido  por la  Secretaría  de Medio Ambiente', 1),(395, 'REMODELACIÓN: Copia de constancia de número oficial y alineamiento vigente', 1),(396, 'REMODELACIÓN: Copia de la póliza de seguros de la empresa y daños contra terceros', 1),(397, 'REMODELACIÓN: Copia del certificado único de zonificación de uso de suelo especifico', 1),(398, 'REMODELACIÓN: Copia del cuestionario  para la clasificación  del grado de riesgo de las  empresas, industrias  o  establecimientos   a  que  se  refiere  el  Capítulo  IV  de  los  Términos  de Referencia para la elaboración de programas Internos de protección civil para obras en proceso de construcción,  remodelación  y demolición', 1),(399, 'REMODELACIÓN: Cronograma  y bitácora  de  simulacros.  Estos  deberán  estar  sustentados  con reporte fotográfico con formato de fecha registrado firmado por el responsable', 1),(400, 'REMODELACIÓN: Cronograma  y bitácora  del  Programa  de Capacitación,  deberán  contener  las constancias vigentes', 1),(401, 'REMODELACIÓN: Cronograma o programa de obra elaborado por meses y partidas', 1),(402, 'REMODELACIÓN: Croquis donde se identifiquen  las brigadas existentes en la obra en proceso de construcción, remodelación o demolición', 1),(403, 'REMODELACIÓN: Cuando  en  la obra  existan  recipientes  sujetos  a presión,  generador  de  vapor  o caldera, se requerirá anexar copia de la autorización de las autoridades del Trabajo', 1),(404, 'REMODELACIÓN: Descripción  de la supervisión  en materia  de Protección  Civil, que  realizará el Director Responsable de Obra y la supervisión de la seguridad en la obra conforme a la NOM-031-STPS-2011', 1),(405, 'REMODELACIÓN: Descripción de los riesgos internos, así como los trabajos de riesgo que se realizan en cada fase de la obra como: demolición,  construcción,  instalaciones  y acabados, se incluyen trabajos en espacios confinados y en alturas', 1),(406, 'REMODELACIÓN: Dictamen  técnico de instalaciones  de gas L.P. emitido por una unidad verificadora y/o por corresponsables  de instalaciones', 1),(407, 'REMODELACIÓN: Dictamen técnico de instalaciones  eléctricas emitido por una unidad verificadora y/o por corresponsables  de instalaciones', 1),(408, 'REMODELACIÓN: Directorio telefónico para requerir servicios de auxilio de bomberos, hospitales, policía y rescate', 1),(409, 'REMODELACIÓN: El proyecto ejecutivo deberá estar firmado por el D.R.O y los corresponsables', 1),(410, 'REMODELACIÓN: Medidas de seguridad en la carga y descarga de material y residuos de obra', 1),(411, 'REMODELACIÓN: Organigrama del Comité Interno de Protección Civil', 1),(412, 'REMODELACIÓN: Plano de localización de zonas de mayor riesgo interno', 1),(413, 'REMODELACIÓN: Tabla del código de colores para la identificación de las brigadas', 1),(414, 'REMODELACIÓN: Procedimiento para la evaluación de daños', 1),(415, 'DEMOLICIÓN: Acta Constitutiva  (Documento  de Integración  del Comité Interno de Protección Civil)', 1),(416, 'DEMOLICIÓN: Aplicación  del seguro  para indemnizar  a los afectados', 1),(417, 'DEMOLICIÓN: Carta compromiso de responsabilidad  firmada por el representante legal', 1),(418, 'DEMOLICIÓN: Continuidad de operaciones; en caso de suspensión de trabajos por siniestros antrópicos o naturales', 1),(419, 'DEMOLICIÓN: Copia de constancia de número oficial y alineamiento vigente', 1),(420, 'DEMOLICIÓN: Copia del Acta Constitutiva de la Empresa', 1),(421, 'DEMOLICIÓN: Copia del cuestionario  para la clasificación  del grado de riesgo de las  empresas, industrias  o  establecimientos   a  que  se  refiere  el  Capítulo  IV  de  los  Términos  de Referencia para la elaboración de programas Internos de protección civil para obras en proceso de construcción,  remodelación  y demolición', 1),(422, 'DEMOLICIÓN: Copia del visto bueno del INBA y/o la licencia del INAH cuando se trate de inmuebles del Patrimonio Histórico, Artístico y Arqueológico de la Federación', 1),(423, 'DEMOLICIÓN: Copia vigente del carnet del Director Responsable de Obra y de los corresponsables  en estructura e instalaciones, así como sus respectivas cartas de responsabilidad', 1),(424, 'DEMOLICIÓN: Cronograma  y bitácora  de  simulacros.  Estos  deberán  estar  sustentados  con reporte fotográfico con formato de fecha registrado firmado por el responsable', 1),(425, 'DEMOLICIÓN: Cronograma y bitácora del programa de capacitación, deberán contener las constancias', 1),(426, 'DEMOLICIÓN: Croquis donde se identifiquen las brigadas existentes en el predio', 1),(427, 'DEMOLICIÓN: Descripción  de la supervisión  en materia  de Protección  Civil, que  realizará el Director Responsable de Obra y la supervisión de la seguridad en la obra conforme a la NOM-031-STPS-2011', 1),(428, 'DEMOLICIÓN: Descripción de medidas de seguridad para instalaciones  provisionales', 1),(429, 'DEMOLICIÓN: Directorio telefónico para requerir servicios de auxilio de bomberos, hospitales, policía y rescate', 1),(430, 'DEMOLICIÓN: El proyecto ejecutivo deberá estar firmado por el D.R.O y los corresponsables', 1),(431, 'DEMOLICIÓN: Elaboración  de  planos  de  conjunto  y por  niveles,  de  equipo  contra  incendio, botiquín   de  primeros   auxilios,   rutas   de  evacuación,   escaleras   y  salidas   de emergencia   y  de  los  equipos  contra  incendio,  puntos  de  reunión,  así  como vialidades para un rápido acceso de los cuerpos de emergencia. Señalamientos  en materia   de  protección   civil   durante   el   proceso   constructivo   en  base   a  la NOM/003/SEGOB-2011, eñales  y avisos para protección  civil. Colores, formas y símbolos  a  utilizar  y  la  NOM-  002-  STPS-  2012,  Condiciones   de  Seguridad- Prevención y Protección contra incendios en los centros de trabajo', 1),(432, 'DEMOLICIÓN: Equipo de primeros auxilios y las brigadas de atención', 1),(433, 'DEMOLICIÓN: Equipo de protección personal y responsabilidades  en materia de protección civil de los trabajadores', 1),(434, 'DEMOLICIÓN: Integrar la descripción general del proyecto, ubicación, entorno inmediato, superficie total de construcción, además de las correspondientes  memorias descriptivas y planos', 1),(435, 'DEMOLICIÓN: Manifestación de construcción Especial', 1),(436, 'DEMOLICIÓN: Medidas de seguridad en la carga y descarga de material y residuos de obra', 1),(437, 'DEMOLICIÓN: Organigrama del Comité Interno de Protección Civil', 1),(438, 'DEMOLICIÓN: Planes, manuales  y procedimientos  de actuación por tipo de riesgo a que está expuesto el predio adecuándolos al mismo', 1),(439, 'DEMOLICIÓN: Plano de localización de las zonas de menor riesgo', 1),(440, 'DEMOLICIÓN: Plano de localización de zonas de mayor riesgo interno', 1),(441, 'DEMOLICIÓN: Póliza de seguros de la empresa y daños contra terceros. Copia', 1),(442, 'DEMOLICIÓN: Procedimiento de seguridad de estructuras de andamios', 1),(443, 'DEMOLICIÓN: Copia del certificado único de zonificación de uso de suelo especifico, vigente', 1),(444, 'DEMOLICIÓN: Procedimiento para la evaluación de daños', 1),(445, 'DEMOLICIÓN: Simulacros el cual deberá contener el procedimiento  e instructivo para (incendio, sismo,   inundación,   artefacto   explosivo)   que   incluya   cronograma,   bitácora   y evaluación', 1),(446, 'DEMOLICIÓN: Tabla del código de colores para la identificación de las brigadas', 1),(447, 'EN ESPECTÁCULOS TRADICIONALES: Carta responsiva de carga vigente de los extintores a utilizar', 1),(448, 'EN CASO DE PIROTÉCNIA EN EXTERIORES: Carta de corresponsabilidad del Tercer Acreditado', 1),(449, 'EN CASO DE PIROTÉCNIA EN INTERIORES: Formato de solicitud TDGAM_AIQ_1, por duplicado debidamente requisitado con firmas autógrafas', 1),(450, 'EN CASO DE PIROTÉCNIA EN INTERIORES: Documentos de acreditación de personalidad jurídica (carta poder firmada ante dos testigos con ratificación de las firmas ante Notario Público, Carta Poder firmada ante dos testigos e identificación oficial del interesado y de quien realiza el trámite. Poder notarial e identificación oficial del representante o apoderado, acta constitutiva, poder notarial e identificación oficial del representante o apoderado). Original y copia', 1),(451, 'EN CASO DE PIROTÉCNIA EN INTERIORES: Identificación oficial con fotografía (carta de naturalización  o cartilla de servicio militar o cédula profesional  o pasaporte o certificado  de nacionalidad mexicana o credencial para votar o licencia para conducir) original y copia', 1),(452, 'EN CASO DE PIROTÉCNIA EN INTERIORES: Carta responsiva de carga vigente de los extintores a utilizar', 1),(453, 'EN CASO DE PIROTÉCNIA EN EXTERIORES: Copia del Permiso General otorgado por la Secretaria de la Defensa Nacional', 1),(454, 'EN CASO DE PIROTÉCNIA EN EXTERIORES: Copia del Permiso de Autorización de Quema expedido por la delegación correspondiente en los términos de la Ley del Sistema de Protección Civil', 1),(455, 'EN CASO DE PIROTÉCNIA EN EXTERIORES: Copia de Póliza de Seguro que ampare la Responsabilidad Civil y daños a terceros', 1),(456, 'EN CASO DE PIROTÉCNIA EN EXTERIORES: Carta responsiva de carga vigente de los extintores a utilizar', 1),(457, 'EN CASO DE PIROTÉCNIA EN EXTERIORES: Identificación oficial con fotografía (carta de naturalización  o cartilla de servicio militar o cédula profesional  o pasaporte o certificado  de nacionalidad mexicana o credencial para votar o licencia para conducir) original y copia', 1),(458, 'EN CASO DE PIROTÉCNIA EN EXTERIORES: Formato de solicitud TDGAM_AIQ_1, por duplicado debidamente requisitado con firmas autógrafas', 1),(459, 'EN ESPECTÁCULOS TRADICIONALES: Documentos de acreditación de personalidad jurídica (carta poder firmada ante dos testigos con ratificación de las firmas ante Notario Público, Carta Poder firmada ante dos testigos e identificación oficial del interesado y de quien realiza el trámite. Poder notarial e identificación oficial del representante o apoderado, acta constitutiva, poder notarial e identificación oficial del representante o apoderado). Original y copia', 1),(460, 'EN ESPECTÁCULOS TRADICIONALES: Copia del contrato de prestación de servicios entre el organizador y el permisionario', 1),(461, 'EN ESPECTÁCULOS TRADICIONALES: Formato de solicitud TDGAM_AIQ_1, por duplicado debidamente requisitado con firmas autógrafas', 1),(462, 'EN ESPECTÁCULOS TRADICIONALES: Croquis y analisis de riesgo en un rango de 500 m', 1),(463, 'EN CASO DE PIROTÉCNIA EN EXTERIORES: Programa de quema', 1),(464, 'EN CASO DE PIROTÉCNIA EN EXTERIORES: Relación del personal pirotécnico, adjuntando documentación que identifique a cada uno', 1),(465, 'EN ESPECTÁCULOS TRADICIONALES: Identificación oficial con fotografía (carta de naturalización  o cartilla de servicio militar o cédula profesional  o pasaporte o certificado  de nacionalidad mexicana o credencial para votar o licencia para conducir) original y copia', 1),(466, 'EN ESPECTÁCULOS TRADICIONALES: Copia del Permiso General otorgado por la Secretaria de la Defensa Nacional', 1),(467, 'EN ESPECTÁCULOS TRADICIONALES: Copia del permiso de Transporte de sustancias peligrosas otorgado por la SCT', 1),(468, 'EN ESPECTÁCULOS TRADICIONALES: Copia del Permiso de Autorización de Quema expedido por la delegación correspondiente en los términos de la Ley del Sistema de Protección Civil', 1),(469, 'EN ESPECTÁCULOS TRADICIONALES: Copia de Póliza de Seguro que ampare la Responsabilidad Civil y daños a terceros', 1),(470, 'EN ESPECTÁCULOS TRADICIONALES: Constancias de capacitación del personal técnico pirotecnico expedidas por institución o tercer acreditado', 1),(471, 'EN CASO DE PIROTÉCNIA EN EXTERIORES: Copia del contrato de prestación de servicios entre el organizador y el permisionario', 1),(472, 'EN ESPECTÁCULOS TRADICIONALES: Relación de artificios pirotécnicos especificando cantidad y potencia, así como gráfica de altura y expansión', 1),(473, 'EN ESPECTÁCULOS TRADICIONALES: Relación del personal pirotécnico, adjuntando documentación que identifique a cada uno', 1),(474, 'EN ESPECTÁCULOS TRADICIONALES: Propuesta de distancias mínimas de seguridad y, en su caso, medidas de seguridad adicionales previstas', 1),(475, 'EN ESPECTÁCULOS TRADICIONALES: Procedimientos de emergencias responsivas', 1),(476, 'REMODELACIÓN: Croquis donde se identifiquen  las brigadas existentes en la obra en proceso de construcción, remodelación o demolición', 1),(477, 'REMODELACIÓN: Cuando  en  la obra  existan  recipientes  sujetos  a presión,  generador  de  vapor  o caldera, se requerirá anexar copia de la autorización de las autoridades del Trabajo', 1),(478, 'DEMOLICIÓN: Procedimiento   de  resguardo,  protección  y  confinamiento   de  sustancias  y/o materiales peligrosos, durante el proceso de remodelación,  en base a la NOM-006- STPS-2000', 1),(264, 'Cronograma  y  bitácora  de  simulacros.  Estos  deberán  estar  sustentados  con reporte fotográfico con formato de fecha registrado firmado por el responsable', 1),(265, 'Croquis  de  ubicación  de  los  equipos  de  primeros  auxilios', 1),(266, 'Cronograma   y   bitácora   de   mantenimiento   en   general,   y   registro   del mantenimiento preventivo y correctivo. Mediante bitácoras. Reforzándolo con reporte fotográfico', 1),(267, 'Vo.Bo de Seguridad y Operación (Cuando así lo estipulen la normatividad aplicable por tipo de inmueble opor el giro de la empresa, industria o establecimiento). Deberá estar firmado por el DRO, responsable del inmueble y por la Delegación a la que pertenece', 1),(268, 'Copia de la póliza de seguro (cuando la actividad o giro de la empresa sea de mediano o alto riesgo, conforme al capítulo III de los Términos de Referencia)', 1),(269, 'Cuando en la empresa, industria o establecimiento existan recipientes sujetos a presión,  generador  de  vapor  o  caldera,  se  requerirá  anexar  copia  de  la autorización de las autoridades del Trabajo', 1),(270, 'Auto  calificación  de  riesgo  en  materia  de  Protección  Civil,  con  el  formato TGAM_API_CUESTIONARIO  DE  AUTODIAGNÓSTICO   a  que  se  refiere  la sección VI del Capítulo I de los Términos de Referencia, firmada en original. (Cuestionario de Autodiagnóstico, Capítulo 1 de los Términos de Referencia', 1),(271, 'Copia  del  cuestionario  para  la  clasificación  del  grado  de  riesgo  de  las empresas, industrias o establecimientos a que se refiere el Capítulo IV de los Términos de Referencia, con la carta firmada en original', 1),(272, 'Dictamen técnico de instalaciones eléctricas emitido por una unidad verificadora y/o por corresponsables de instalaciones', 1),(273, 'Factura instalación del sistema de alertamiento sísmico y evidencia fotográfica', 1),(274, 'Copia de declaración de apertura. (Permiso o Aviso expedido por la Secretaría de Desarrollo Económico que ampare el funcionamiento del establecimiento mercantil)', 1),(275, 'Documentos  de acreditación  de personalidad  jurídica (Carta Poder firmada ante dos testigos con ratificación de las firmas ante Notario Público, Carta Poder firmada ante dos testigos e identificación  oficial del interesado y de quien realiza el trámite, Poder Notarial  e Identificación  Oficial  del representante  o  apoderado,  Acta  Constitutiva, Poder  Notarial  e Identificación  Oficial  del  representante   o apoderado)', 1),(276, 'Carta de Corresponsabilidad de Tercer Acreditado', 1),(277, 'Generalidades:,- Objetivo del Subprograma  de Prevención en su fase de Gestión Prospectiva ,- Nombre del evento,- Información general del evento,- Plano de ubicación y colindancias del inmueble en que se celebrará el evento,- Promotor responsable del evento,- Descripción del evento,- Aforo esperado en el evento,- Definición del grado de riesgo del evento,- Minuto a minuto de las operaciones de montaje, operación general y desmontaje del equipamiento  a instalarse,- Programación  minuto a minuto del espectáculo,- Riesgos por Agentes Perturbadores,- Riesgos Internos,- Riesgos Externos,- Determinación  de zonas de riesgo,- Determinación  de zonas de menor riesgo,- Diseño de rutas de evacuación,- Condiciones de seguridad en salidas de emergencia,- Condiciones de accesibilidad  para las personas con capacidades diferentes,- Mensajes en materia de protección civil,- Medidas para el manejo transporte y almacenamiento de combustibles', 1),(278, 'Características del evento:,- Marco de referencia para la clasificación del grado de riesgo,- Tipo de evento,- Área o inmueble donde se pretende realizar el evento,- Estructuras temporales y equipamiento  complementario a instalarse,- Activación de artificios pirotécnicos,- Instalaciones  de aprovechamiento de Gas L.P,- Equipamiento  para la prevención y el combate de incendio,- Dispositivo de Seguridad,- Colindancias  del área o inmueble,- Afluencia estimada de asistentes al evento,- Estacionamientos', 1),(279, 'Orden de operación:,- Directorio del organizador,- Directorio de responsables  por área,- Directorio de proveedores,- Cronograma  de montaje y desmontaje,- Minuto a minuto del evento,- Aforo de acuerdo a la capacidad de la superficie libre de estructuras temporales y demás equipamiento  a instalarse,- Mecanismos  de entrega, venta y distribución de boletaje', 1),(280, 'Comité Interno de Protección Civil: ,- Comité Interno de Protección Civil aplicable al evento,- Objetivo,- Obligatoriedad,- Formación del Comité Interno de Protección Civil,- Integración del Comité Interno de Protección Civil,- Documentos  de Integración,- Funciones del Coordinador General y suplente,Jefe de edificio,- Jefe de piso o área', 1),(281, 'Programa de seguridad:,- Objetivo,- Dispositivo de seguridad para el evento,- Distribución y consigna de los elementos de seguridad', 1),(282, 'Formación de Brigadas:,- Integración de Brigadas,- Características que deben tener los brigadistas,- Colores para la identificación  de los brigadistas,- Funciones generales de los brigadistas,- Funciones y actividades de la Brigada de Evacuación,- Funciones y actividades de la Brigada de Primeros Auxilios,- Funciones y actividades de la Brigada de Prevención y Combate de Incendios,- Funciones de la Brigada de Comunicación,- Capacitación', 1),(283, 'Plan de Acción Correctiva', 1),(284, 'Programa de Mantenimiento  Preventivo y Correctivo', 1),(285, 'Acciones de Auxilio:,- Objetivos,- Fase de Alerta,- Mensajes en materia de protección civil,- Accionamiento  de la Coordinación  de Brigadas de Protección Civil,- Accionamiento  del plan de evacuación del inmueble o área de afluencia masiva,- Procedimiento  de evacuación y repliegue,- Procedimiento  en caso de riña,- Procedimiento  en caso de robo,- Procedimiento  en caso de amenaza de bomba,- Procedimiento  en caso de persona lesionada,- Procedimiento  en caso de persona extraviada,- Procedimiento  en caso de sismo,- Procedimiento  en caso de incendio,- Procedimiento  de atención integral para las personas con capacidades diferentes,- Procedimiento  de actuación ente la suspensión o cancelación del evento por causas de fuerza mayor', 1),(286, 'Atención a emergencias  mayores:,- Directorio de hospitales y servicios de atención a emergencias,- Acciones de coordinación  y enlace con los cuerpos gubernamentales de atención médica, seguridad pública y atención a emergencias  mayores,- Acciones de coordinación  y enlace con autoridades en materia de protección civil', 1),(287, 'Objetivo del Subprograma  de Recuperación:,- Evaluación de Daños,- Inspección Visual,- Inspección Física,- Inspección Técnica,- Reinicio de Actividades,- Vuelta a la normalidad,- Suspensión del evento', 1),(288, 'Carta responsiva,  diseño, memoria de cálculo y características  de los materiales  de estructuras temporales a instalarse', 1),(289, 'Carta responsiva de plantas generadoras  de energía', 1),(290, 'Carta responsiva de la instalación de carpas', 1),(291, 'Carta responsiva de la instalación de panelería y mamparería.  Carta responsiva de la aplicación de material retardante al fuego', 1),(292, 'Carta responsiva de la aplicación de material retardante al fuego', 1),(293, 'Contrato y registro vigente del particular a cargo de los servicios de atención médica prehospitalaria', 1),(294, 'Certificación del personal médico y del personal Técnico en Urgencias Médicas', 1),(295, 'Carta responsiva del equipo para la prevención y el combate de incendio', 1),(296, 'Permiso   emitido   por  parte  de  la  autoridad   Delegacional   competente   para   la activación y quema de artificios pirotécnicos', 1),(297, 'Copia  del permiso  general  expedido  por la Secretaría  de la Defensa  Nacional  al proveedor responsable de la activación de artificios pirotécnicos', 1),(298, 'Copia de la autorización  para la trasportación  y quema de los artificios pirotécnicos  emitidos  al proveedor  correspondiente  por  parte  de  la  Secretaría de  la Defensa Nacional', 1),(299, 'Copia  del contrato  de servicio  en el cual deberá  especificarse  la potencia,  tipo y cantidad de artificios a activarse', 1);
COMMIT;
BEGIN;
LOCK TABLE "public"."cat_status" IN SHARE MODE;
DELETE FROM "public"."cat_status";
INSERT INTO "public"."cat_status" ("id","description") VALUES (2, 'Art. 49'),(3, 'Cancelada'),(4, 'Prevenida'),(1, 'Completa'),(5, 'Turnada'),(6, 'Finiquitada'),(7, 'Con Seguimiento'),(8, 'Dictaminada'),(9, 'Subsanada');
COMMIT;
BEGIN;
LOCK TABLE "public"."cat_transact" IN SHARE MODE;
DELETE FROM "public"."cat_transact";
INSERT INTO "public"."cat_transact" ("id","code","description","legal_foundation","status","days","type_days") VALUES (2, 3, 'Presentación de espectáculos públicos', 'Ley para la celebración de Espectáculos Públicos en el Distrito Federal. Artículo 4, fracción II, III, X, 8, 9, 12, 19, 20, 23, 24, 25, 26, 27, 28, 29 y 30, 55, 55 BIS, 55 TER. 54 QUÁTER, 58, 59 y 60.,Reglamento de la Ley para la celebración de Espectáculos Públicos del Distrito Federal en materia de Espectáculos Masivos y Deportivos. Artículos 3, 6, 7, 8, 9, 10 y 11.,Código Fiscal de la CDMX. Artículos 20, 256, 257 y 258.,Reglamento interior de la Administración Pública del Distrito Federal. Artículo 104 fracción II.', 1, 0, '0'),(4, 856, 'Cambio del nombre del titular de la cédula de empadronamiento de locales en Mercados Públicos por fallecimiento del empadronado', 'Artículos 32, 40, 41, 42, 43, 44 y 82 del Reglamento de Mercados para el Distrito Federal.,Artículos 7 bis, 32, 33, 34, 35, 40,  41, 42,  44, 49  y 89  de la  Ley de Procedimiento Administrativo del Distrito Federal.,Artículos 6, 7 y 9 del Acuerdo por el que se reforman y adicionan diversas disposiciones del similar por el que se establece el Sistema de Empadronamiento  para  comerciantes  de  los  Mercados  Públicos  de  la Ciudad de México y los formatos oficiales de Cédula de Empadronamiento, refrendo y autorización hasta por 90 días para que una persona distinta al empadronado   pueda   ejercer   el   comercio   en   puestos   permanentes   o temporales en Mercados Públicos, por cuenta del empadronado.,Artículos  5,  6  y 7  del  Acuerdo  por  el  que  se  establece  el  Sistema  de Empadronamiento  para  comerciantes  de  los  Mercados  Públicos  de  la Ciudad de México y los formatos oficiales de Cédula de Empadronamiento, refrendo y autorización hasta por 90 días para que una persona distinta al empadronado   pueda   ejercer   el   comercio   en   puestos   permanentes   o temporales en Mercados Públicos, por cuenta del empadronado.,Artículos 51, fracción V y 124, fracción XVII del Reglamento Interior de la Administración Pública del Distrito Federal.,Numerales Décimo Quinto Fracción V, Décimo Sexto, Décimo Séptimo, Décimo   Octavo,   Trigésimo   Primero,   Trigésimo   Segundo,   Trigésimo Tercero y Trigésimo Cuarto de los Lineamientos para la operación y funcionamiento de los Mercados Públicos del Distrito Federal.', 1, 40, 'H'),(5, 859, 'Refrendo de empadronamiento para ejercer actividades comerciales en Mercados Públicos', 'Artículo 31 del Reglamento de Mercados para el Distrito Federal,Artículo 264 del Código Fiscal del Distrito Federal,Artículos 51 fracción V y 124 fracción XVII del Reglamento Interior de la Administración Pública del Distrito Federal,Artículos  7bis,  32,  33,  34,  35,  40,  41,  42,  44  y  49  de  la  Ley  de Procedimiento Administrativo del Distrito Federal,Artículos  5,  6  y 7  del  Acuerdo  por  el  que  se  establece  el  Sistema  de Empadronamiento  para  comerciantes  de  los  Mercados  Públicos  de  la Ciudad de México y los formatos oficiales de Cédula de Empadronamiento, refrendo y autorización hasta por 90 días para que una persona distinta al empadronado   pueda   ejercer   el   comercio   en   puestos   permanentes   o temporales en Mercados Públicos, por cuenta del empadronado,Artículos 6, 7 y 9 del Acuerdo por el que se reforman y adicionan diversas disposiciones del similar por el que se establece el Sistema de Empadronamiento  para  comerciantes  de  los  Mercados  Públicos  de  la Ciudad de México y los formatos oficiales de Cédula de Empadronamiento, refrendo y autorización hasta por 90 días para que una persona distinta al empadronado   pueda   ejercer   el   comercio   en   puestos   permanentes   o temporales en Mercados Públicos, por cuenta del empadronado,Numerales Décimo Quinto Fracción II, Décimo Sexto, Décimo Séptimo, Vigésimo   Quinto,   Vigésimo   Sexto   y   Vigésimo   Séptimo   de   los Lineamientos  para  la  Operación  y  Funcionamiento  de  los  Mercados Públicos del Distrito Federal', 1, 15, 'H'),(6, 1986, 'Autorización para la instalación, quema de pirotécnia y efectos especiales', 'Ley del Sistema del Sistema de Protección  Civil para el Distrito Federal. Artículo 95.,Reglamento de la Ley De Protección Civil para el Distrito Federal. Artículo 32.,Norma Técnica Complementaria  Ntcpc-010-Pirotecnia-  2017.- Instalación y Quema de,Artificios  Pirotécnicos   en  Espectáculos   Públicos  y  Tradicionales   en  la  Ciudad  de,México. Aplican en su totalidad.,Ley de Procedimiento Administrativo del Distrito Federal. Artículo 89.', 1, 7, 'H'),(12, 1951, 'Autorización para romper el pavimento o hacer cortes en las banquetas y guarniciones en la vía pública para llevar a cabo su mantenimiento', 'Ley de Desarrollo Urbano del Distrito Federal.- Artículos 7 fracciones VII.,Reglamento de Construcciones para el Distrito Federal.- Artículos 7, 9, 10 fracción III,y 191.,Ley de Procedimiento Administrativo del Distrito Federal.- Artículos 32, 44 y 89.', 1, 40, 'H'),(13, 1450, 'Licencia de anuncios en Vallas en Vías Secundarias', 'Ley de Publicidad Exterior del Distrito Federal. Artículos 3 fracciones I, II, XXVI, XXXV, XXXVI, XXXIX y XLI; 7 fracción II; 17;  49 fracción III; 50, 51; 69 y 73.,Reglamento de la Ley de Publicidad Exterior del Distrito Federal. Artículos 2 fracciones VI, XIV, XVIII; 5; 13 fracción II; 42 fracción III; 44; 49; 66 fracción I, II, III y V ; 68; 70 fracción I inciso b); 82; 83 fracción V; 84; 86 y 86 Bis. ,Código Fiscal del Distrito Federal. Artículos 19 y 20.,Reglamento de Construcciones para el Distrito Federal. Artículo 34 fracción I.', 1, 30, 'H'),(3, 2, 'Autorización para la celebración de espectáculos', 'Ley para la celebración de Espectáculos Públicos en el Distrito Federal. Artículo 4, fracción II, III, X, 8, 9, 12, 19, 20, 23, 24, 25, 26, 27, 28, 29 y 30, 55, 55 BIS, 55 TER. 54 QUÁTER, 58, 59 y 60.,Reglamento de la Ley para la celebración de Espectáculos Públicos del Distrito Federal en materia de Espectáculos Masivos y Deportivos. Artículos 3, 6, 7, 8, 9, 10 y 11.,Código Fiscal de la CDMX. Artículos 20, 256, 257 y 258.,Reglamento interior de la Administración Pública del Distrito Federal. Artículo 104 fracción II.', 1, 5, 'H'),(7, 855, 'Autorización para el traspaso de derechos de Cédula de Empadronamiento del local en Mercado Público', 'Artículos 32, 35, 36, 37, 39 y 40  del Reglamento de Mercados para el Distrito Federal,Artículo 264 del Código Fiscal del Distrito Federal,Numerales Décimo Quinto Fracción IV, Décimo Sexto, Décimo Séptimo, Décimo Octavo y Trigésimo de los Lineamientos para la Operación y Funcionamiento de los Mercados Públicos del Distrito Federal,Artículos 51 fracción V y 124, fracción XVII del Reglamento Interior de la Administración Pública del Distrito Federal,Artículos  5,  6  y 7  del  Acuerdo  por  el  que  se  establece  el  Sistema  de Empadronamiento  para  comerciantes  de  los  Mercados  Públicos  de  la Ciudad de México y los formatos oficiales de Cédula de Empadronamiento, refrendo y autorización hasta por 90 días para que una persona distinta al empadronado   pueda   ejercer   el   comercio   en   puestos   permanentes   o temporales en Mercados Públicos, por cuenta del empadronado,Artículos 6, 7 y 9 del Acuerdo por el que se reforman y adicionan diversas disposiciones del Similar por el que se establece el Sistema de Empadronamiento  para  comerciantes  de  los  Mercados  Públicos  de  la Ciudad de México y los formatos oficiales de Cédula de Empadronamiento, refrendo y autorización hasta por 90 días para que una persona distinta al empadronado   pueda   ejercer   el   comercio   en   puestos   permanentes   o temporales en Mercados Públicos, por cuenta del empadronado', 1, 15, 'H'),(8, 852, 'Autorización de cambio de giro de local en Mercado Público', 'Artículos 32, 35, 38, 39 y 40 del Reglamento de Mercados para el Distrito,Federal,Artículos  7  bis,  32,  33,  34,  35,  40,  41,  42,  44  y  49  de  la  Ley  de,Procedimiento Administrativo del Distrito Federal,Numerales Décimo Quinto Fracción III, Décimo Sexto, Décimo Séptimo,,Décimo Octavo, Vigésimo Octavo y Vigésimo Noveno de los Lineamientos para la Operación y Funcionamiento de los Mercados Públicos del Distrito Federal,Artículos 51 fracción V y 124 fracción XVII del Reglamento Interior de la,Administración Pública del Distrito Federal,Artículos Artículos 1, 3, 6, 8 Fracciones I y II, 9 y 11 del Catálogo de Giros para el Desarrollo de Actividades Comerciales en Mercados Públicos de la Ciudad de México,Artículo 264 del Código Fiscal del Distrito Federal,Artículos 6, 7 y 9 del Acuerdo por el que se reforman y adicionan diversas,disposiciones del similar por el que se establece el Sistema de Empadronamiento  para  comerciantes  de  los  Mercados  Públicos  de  la Ciudad de México y los formatos oficiales de Cédula de Empadronamiento, refrendo y autorización hasta por 90 días para que una persona distinta al empadronado   pueda   ejercer   el   comercio   en   puestos   permanentes   o temporales en Mercados Públicos, por cuenta del empadronado,Artículos  5,  6  y 7  del  Acuerdo  por  el  que  se  establece  el  Sistema  de Empadronamiento  para  comerciantes  de  los  Mercados  Públicos  de  la Ciudad de México y los formatos oficiales de Cédula de Empadronamiento, refrendo y autorización hasta por 90 días para que una persona distinta al empadronado   pueda   ejercer   el   comercio   en   puestos   permanentes   o temporales en Mercados Públicos, por cuenta del empadronado', 1, 15, 'H'),(9, 857, 'Cédula de empadronamiento para ejercer actividades comerciales en mercados públicos o su reexpedición', 'Artículos 6, 7 y 9 del Acuerdo por el que se reforman y adicionan diversas disposiciones del similar por el que se establece el Sistema de Empadronamiento  para  comerciantes  de  los  Mercados  Públicos  de  la Ciudad de México y los formatos oficiales de Cédula de Empadronamiento, refrendo y autorización hasta por 90 días para que una persona distinta al empadronado   pueda   ejercer   el   comercio   en   puestos   permanentes   o temporales en Mercados Públicos, por cuenta del empadronado,Artículos  5,  6  y 7  del  Acuerdo  por  el  que  se  establece  el  Sistema  de Empadronamiento  para  comerciantes  de  los  Mercados  Públicos  de  la Ciudad de México y los formatos oficiales de Cédula de Empadronamiento, refrendo y autorización hasta por 90 días para que una persona distinta al empadronado   pueda   ejercer   el   comercio   en   puestos   permanentes   o temporales en Mercados Públicos, por cuenta del empadronado,Artículos 1, 3, 6, 8 Fracciones I y II, 9 y 11 del Catálogo de Giros para el,Desarrollo de Actividades Comerciales en Mercados Públicos de la Ciudad de México,Artículos 51 fracción V y 124 fracción XVII del Reglamento Interior de la,Administración Pública del Distrito Federal,Artículos 7 bis, 32, 33, 34, 35, 40,  41, 42,  44, 49  y 89  de la  Ley de Procedimiento Administrativo del Distrito Federal,Artículos 26, 27, 28, 29, 30 y 32 del Reglamento de Mercados para el Distrito Federal,Numerales Décimo Quinto Fracción  I, Décimo Sexto,  Décimo Séptimo, Décimo Noveno, Vigésimo, Vigésimo  Primero, Vigésimo Segundo, Vigésimo  Tercero  y  Vigésimo  Cuarto  de  los  Lineamientos   para  la Operación y Funcionamiento de los Mercados Públicos del Distrito Federal', 1, 15, 'H'),(10, 858, 'Permiso para ejercer actividades comerciales con Romerías', 'Artículos 51 fracción V y 124 fracción XVII del Reglamento Interior de la Administración Pública del Distrito Federal,Artículos  7  bis,  32,  33,  34,  35,  40,  41,  42,  44  y  49  de  la  Ley  de,Procedimiento Administrativo del Distrito Federal,Artículos 1, 2 fracción IX, 3, 4, 6, 7, 12, 13, 14, 15 y 16 de las Normas para las Romerías de Mercados Públicos de la Ciudad de México,Numerales Décimo Quinto Fracción VII, Décimo Sexto, Décimo Séptimo, Trigésimo Noveno, Cuadragésimo, Cuadragésimo Primero y Cuadragésimo Segundo de los Lineamientos para la Operación y Funcionamiento de los Mercados Públicos del Distrito Federal.,Artículo 264 del Código Fiscal del Distrito Federal,Aviso por el que se da a conocer el Listado de los Mercados Públicos de la Ciudad de México.', 1, 15, 'H'),(11, 853, 'Autorización de remodelación de local', 'Artículo 264 Código Fiscal del Distrito Federal.,Artículo 28 del Reglamento de Construcciones para el Distrito Federal.,Artículos 51 fracción V y 124 fracción XVII del Reglamento Interior de la,Administración Pública del Distrito Federal.,Artículos  7  bis,  32,  33,  34,  35,  40,  41,  42,  44  y  89  de  la  Ley  de,Procedimiento Administrativo del Distrito Federal.,Numerales Décimo Quinto fracción VIII, Décimo Sexto,  Décimo Séptimo, Cuadragésimo Tercero, Cuadragésimo Cuarto, Cuadragésimo Quinto, Cuadragésimo Sexto y Cuadragésimo Séptimo de los Lineamientos para la Operación y Funcionamiento de los Mercados Públicos del Distrito Federal.,Artículos 6, 7 y 9 del Acuerdo por el que se reforman y adicionan diversas,disposiciones del similar por el que se establece el Sistema de Empadronamiento  para  Comerciantes  de  los  Mercados  Públicos  de  la Ciudad de México y los Formatos oficiales de cédula de empadronamiento, refrendo y autorización hasta por 90 días para que una persona distinta al empadronado   pueda   ejercer   el   comercio   en   puestos   permanentes   o temporales en Mercados Públicos, por cuenta del empadronado.,Aviso por el que se da a conocer el Listado de los Mercados Públicos de la,Ciudad de México.', 1, 15, 'H'),(29, 4, 'Regularización de construcción de inmuebles dedicados a la vivienda.', 'Fundamento en el Código Fiscal del Distrito Federal, a constinuación en su caso se calcula: por concepto de derechos Art. 185, por concepto de autorización para el uso de las redes de agua y drenaje Art. 182 fracción I y II', 1, 40, 'H'),(14, 1452, 'Instalación, reconstrucción, cambio de diametro y supresión de tomas de agua potable, tomas de agua residual y descargas domiliciarias, armado de cuadro e instalación de medidores', 'Ley de Aguas del Distrito Federal, artículos 16 fracción II, 18 fracciones II, III y IV, 50, 51, 56, 57, 58, 63, 66, 71, 72, 74,75 y 76.,Reglamento de Construcciones del Distrito Federal, artículo 128.,Código Fiscal del Distrito Federal, artículos  181, 182, 302, 430, 431 y 432.', 1, 8, 'H'),(15, 908, 'Registro de constacia de Seguridad Estructural y su Renovación', 'Ley de Desarrollo Urbano del Distrito Federal.- Artículo 7 fracción VII.,Reglamento  de  Construcciones  para  el  Distrito  Federal.-  Artículos  38 fracción I, inciso e), 71, 139 fracciones I y II y 170, 233.', 1, 0, '0'),(16, 1451, 'Licencia de anuncios denominativos en inmuebles ubicados en vías secundarias', 'Ley de Publicidad Exterior del Distrito Federal. Artículos 3 fracciones I, II, V, XV, XXVI, XXXV, XLI, 7 fracción II, 12, 22, 23, 24, 25, 26, 27, 28, 29, 30, 49 fracción I, 50, 51, 72 y 73.,Reglamento de la Ley de Publicidad Exterior del Distrito Federal. Artículos 2 fracción V, 5, 13 fracción I, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 66, 68, 70 fracción I inciso a), 82, 83 fracción I, 85 y 88 fracción II.,Código Fiscal del Distrito Federal. Artículos 19 y 20.,Reglamento de Construcciones para el Distrito Federal. Artículo 34 fracción I.', 1, 30, 'H'),(17, 1449, 'Autorización temporal para anuncios en tapiales en vías secundarias', 'Ley de Publicidad Exterior del Distrito Federal. Artículos 3 fracciones I, II, XII, XIX, XXXV, XLI, 7 fracción II, 12, 17, 49 fracción II, 50, 51, 76 fracción I, 77 y 79.,Reglamento de la Ley de Publicidad Exterior del Distrito Federal. Artículos 2 fracción XX, 5, 13 fracción II, 44, 52 fracciones I, III, IV y V, 70 fracción II, 89, 90 fracción I, 91 fracciones I, II, III, IV, V, VI, VII, X, XI, XII y XIII, 91 Bis y 92 fracción I.,Código Fiscal del Distrito Federal. Artículos 19 y 20.', 1, 30, 'H'),(18, 906, 'Registro de manifestación de construcción Tipo A', 'Ley de Desarrollo Urbano del Distrito Federal.- Artículos 7º fracciones VII,,XVIII, 8 fracciones III, IV y 87 fracción VI.,Reglamento de Construcciones para el Distrito Federal.- Artículos 3º fracción IV, 47, 48, 50, 51 fracción I, 52, 54 fracciones I, II, 64, 65 párrafo tercero, 70 fracción II, 128, 244 y 245.,Reglamento  Interior  de  la  Administración  Pública  del  Distrito  Federal.-,Artículo 50 A fracción XXIX.,Ley Orgánica de la Administración Pública del Distrito Federal Artículo 39 fracción II.', 1, 0, '0'),(19, 861, 'Expedición de licencia de subdivisión, fusión y prórroga', 'Ley de Desarrollo Urbano del Distrito Federal.- Artículos 4 fracciones III y IV, 7 fracciones,I, VII, 8 fracción III y 87 fracciones VII y VIII.,Reglamento  de la Ley de Desarrollo  Urbano del Distrito Federal.- Artículos 75, 127,134 y 135 fracciones I y II.,Código Fiscal del Distrito Federal.- Artículo 20.', 1, 2, 'H'),(20, 909, 'Registro de Obra Ejecutada', 'Ley de Desarrollo Urbano del Distrito Federal.- Artículos 7 fracciones VII, 87 fracción VI y 99 fracción I.,Ley Orgánica de la Administración Pública del Distrito Federal.- Artículo 39 fracción II.,Reglamento  de  Construcciones  para  el  Distrito  Federal.-  Artículos  3 fracciones IV y XIII  72 y 253.,Código Fiscal de la Ciudad de México.- Artículos 20, 181,182, 185, 300, 301 y 302.,Artículos 185 o 186 del Código Fiscal de la Ciudad de México de conformidad con el artículo 72 del,Reglamento de Construcciones para el Distrito Federal.', 1, 30, 'H'),(21, 905, 'Licencia de construcción especial', 'Ley de Desarrollo Urbano del Distrito Federal.- Artículos 7 fracciones VII y XVIII, 58, 59 y 87 fracción VI.,Reglamento  de  Construcciones  para  el  Distrito  Federal.-   Artículos  3 fracción IV, 10, 36, 38, 55, 57, 58, 59, 60, 61, 64, 65, 66, 70 fracción II, 236 y 238.,Acuerdo   por   el   que   se   establece   el   Subcomité   de   Instalaciones,Código Fiscal de la Ciudad de México.- Artículos 20, 181, 182, 300, 301 y,302.,Subterráneas del Centro Histórico de la Ciudad de México. Gaceta Oficial del 24 de abril de 2009.- Acuerdos Segundo, Quinto, Sexto, Séptimo y Décimo Segundo.', 1, 1, 'H'),(22, 1987, 'Autorización del programa especial de Protección Civil', 'Ley del Sistema de Protección Civil del Distrito Federal. Artículos de la ley o reglamento: Artículos 71, 74, 76, 77, 78, 79, 94, 95, 96, 97 y 180.,Términos  de referencia  para la elaboración  de programas  especiales  de protección civil, TR-SPC-002-PEPC-2016. Aplican en su totalidad.,Reglamento de la Ley de Protección Civil para el Distrito Federal. Artículos 5, 29, 30.,Norma  Técnica  Complementaria  NTCPC-001-IT-2016.- Instalaciones  de Gas L.P. Temporales. Aplican en su totalidad.,Norma   Técnica   Complementaria   NTCPC-002-IT-   2016.-   Instalaciones Eléctricas Temporales. Aplica en su totalidad.', 1, 40, 'H'),(23, 903, 'Registro de manifestación de construcción Tipo B o C', 'Ley Orgánica de la Administración  Pública del Distrito Federal.- Artículo 39 fracción II.,Reglamento  Interior de la Administración  Pública del Distrito Federal.- Artículo 50 A fracción XXIX.,Reglamento de Construcciones  para el Distrito Federal.- Artículos 3 fracciones IV y VIII, 36, 38, 47, 48, 51 fracciones II y III, 53, 54 fracción III, 61, 64, 65 y 70.,Ley de Desarrollo  Urbano  del Distrito  Federal.-  Artículos  7 fracciòn  VII,  XVIII, 8 fraccion IV, 47 Quater fracción XVI, inciso c) y 87 fracción VI.,Código Fiscal de la Ciudad de México.- Artículos 20, 181, 182, 300, 301 y 302.', 1, 40, 'H'),(24, 907, 'Aviso Visto Bueno de seguridad y operación, y renovación', 'Reglamento  de  Construcciones  para  el  Distrito  Federal.-  Artículos  35 fracción II párrafo tercero, 38 fracción III inciso c), 68, 69 y 70.', 1, 0, '0'),(25, 5, 'Expedición de copias certificadas de los documentos que obren en los archivos de la delegación', 'Ley Orgánica de la Administración Pública del Distrito Federal, artículo 39 fracción I,Ley de Procedimiento Administrativo del Distrito Federal. - Artículos 35 párrafos 3o. y 44,Código Fiscal del Distrito Federal.- Artículo 248 fracción I.,Reglamento Interior de la Administración Pública del Distrito Federal, artículo 123 fracción II.,Manual Específico de Operación de Ventanillas Únicas.', 1, 40, 'H'),(26, 1434, 'Expedición de certificado de residencia', 'Ley Orgánica de la Administración Pública del Distrito Federal, artículo 39 fracción IX,Reglamento Interior de la Administración Pública del Distrito Federal, artículo 124 fracción VII.', 1, 40, 'H'),(27, 904, 'Expedición de constancia de alineamiento y/o número oficial', 'Ley de Desarrollo Urbano del Distrito Federal.- Artículo 87 fracción I.,Reglamento de la Ley de Desarrollo Urbano del Distrito Federal.- Artículo 43.,Reglamento de Construcciones para el Distrito Federal.- Artículos 8, 22, 24 y 25.', 1, 6, 'H'),(28, 1, 'Cuestionario de Autodiagnóstico en Materia de Protección Civil', 'Ley del Sistema de Protección Civil del Distrito Federal. Artículos 24, 76.,Términos de Referencia para la elaboración de Programas Internos de Protección Civil, TR-SPC-001-PIPC-2016. Sección IV, Capítulo I - Cuestionario de Autodiagnóstico', 1, 40, 'H'),(30, 1980, 'Programa interno de Protección Civil', 'Ley del Sistema de Protección Civil del Distrito Federal. Artículos 73, 78, 89, 90, 91, Transitorio 23.,Ley  Orgánica  de  la  Administración  Pública  del Distrito  Federal.  Articulo  39 fracciones LXVIII Y LXIX,Reglamento de la Ley de Protección Civil para el Distrito Federal.  Artículos 7, 23, 24, 26.,Términos  de  Referencia  para  la  elaboración  de  Programas  Internos  de Protección Civil, TR-SPC-001-PIPC-2016, Publicados en la Gaceta Oficial de la Ciudad de México el 22 de Febrero de 2016. Aplica en su totalidad', 1, 30, 'N'),(37, 69, 'esto es una prueba', 'prueba de fundamento', 1, 0, '0'),(31, 6666, 'Descripcion de clave', 'fundamento juridico de esta cosa', 1, 0, '0'),(32, 7777, 'PRUEBA', 'PRUEBA', 1, 0, '0'),(34, 55555, 'Tramite de prueba en actualizacion', 'Fundamento juridico de un tramite de prueba en actualizacion', 1, 50, 'N'),(35, 99894, 'descripcion de prueba con las fechas', '', 0, 15, 'H'),(39, 666777, 'descripcion de prueba para tramite con log', 'fundamento juridico de prueba ', 0, 30, 'N'),(38, 77, 'prueba', 'fundamento de prueba', 0, 0, '0'),(1, 854, 'Autorización hasta por 90 días para que una persona distinta del empadronado pueda ejercer el comercio en puestos permanentes o temporales en mercados públicos por cuenta del empadronado', 'Artículo 15 del Reglamento de Mercados para el Distrito Federal.,Artículo 264 del Código Fiscal del Distrito Federal.,Artículos 5, 6 y 7 del acuerdo por el que se establece el sistema de Empadronamiento para comerciantes de los Mercados Públicos de la Ciudad de México y los formatos oficiales de Cédula de Empadronamiento, refrendo y autorización hasta por 90 días para que una empresa distinta al empadronado pueda ejercer el comercio en puestos permanentes o temporales en mercados públicos por cuenta del empadronado.,Artículos 6, 7 y 9 del acuerdo por el que se reforman y adicionan diversas disposiciones del similar por el que se establece el sistema de Empadronamiento para comerciantes de los Mercados Públicos de la Ciudad de México y en los formatos oficiales de Cédula de Empadronamiento, refrendo y autorización hasta por 90 días para que una persona distinta al empadronamiento pueda ejercer el comercio en puestos permanentes o temporales en Mercados Públicos, por cuenta del empadronado.,Numerales Décimo Quinto Fracción VI, Décimo Sexto, Décimo Séptimo, Trigésimo Quinto, Trigesimo Sexto, Trigésimo Septimo y Trigésimo Octavo de los Lineamientos para la Operación y funcionamiento de los Mercados Públicos del Distrito Federal.,Artículos 7 BIS, 32, 33, 34, 35, 40, 41, 42, 44 y 49 de la Ley de Procedimiento Administrativo del Distrito Federal.,Artículos 51 fracción V y 124 fracción XVII del Reglamento Interior de la Administración Pública del Distrito Federal.', 1, 15, 'H');
COMMIT;
BEGIN;
LOCK TABLE "public"."cat_transact_requirement" IN SHARE MODE;
DELETE FROM "public"."cat_transact_requirement";
INSERT INTO "public"."cat_transact_requirement" ("id_transact","id_requirement") VALUES (5, 327),(5, 328),(5, 329),(852, 1),(852, 4),(852, 31),(852, 56),(852, 6),(852, 366),(852, 36),(852, 8),(852, 60),(853, 1),(853, 4),(853, 367),(853, 56),(853, 65),(853, 66),(853, 67),(853, 68),(853, 69),(853, 70),(853, 36),(853, 8),(854, 1),(854, 56),(854, 4),(854, 5),(854, 6),(854, 366),(854, 8),(854, 7),(854, 60),(855, 1),(855, 4),(855, 52),(855, 31),(855, 367),(855, 6),(855, 366),(855, 36),(855, 8),(855, 60),(855, 53),(855, 54),(855, 55),(856, 1),(856, 31),(856, 4),(856, 32),(856, 6),(856, 60),(856, 33),(856, 34),(856, 35),(856, 36),(856, 8),(856, 366),(858, 1),(858, 6),(858, 4),(858, 63),(858, 64),(858, 366),(858, 36),(858, 8),(858, 56),(858, 213),(859, 1),(859, 56),(859, 4),(859, 36),(859, 6),(859, 8),(859, 366),(859, 60),(861, 1),(861, 8),(861, 6),(861, 183),(861, 219),(861, 240),(861, 207),(861, 186),(861, 218),(861, 208),(861, 209),(861, 210),(904, 1),(904, 6),(904, 8),(904, 332),(904, 333),(1987, 285),(909, 1),(909, 214),(909, 240),(909, 316),(909, 218),(909, 215),(909, 216),(909, 6),(909, 8),(909, 217),(1434, 78),(1434, 35),(1434, 26),(1434, 6),(1434, 3),(1434, 1),(1434, 330),(1434, 331),(1449, 1),(1449, 159),(1449, 24),(1449, 165),(1449, 166),(1449, 167),(1449, 162),(1449, 169),(1449, 75),(1449, 170),(1449, 171),(1449, 172),(1449, 173),(1449, 174),(1449, 78),(1450, 1),(1450, 72),(1450, 24),(1450, 73),(1450, 74),(1450, 170),(1450, 75),(1450, 76),(1450, 173),(1450, 78),(1450, 166),(1451, 1),(1451, 158),(1451, 159),(1451, 160),(1451, 24),(1451, 166),(1451, 167),(1451, 162),(1451, 172),(1451, 76),(1451, 170),(1451, 75),(1451, 78),(1452, 1),(1452, 240),(1452, 306),(1452, 6),(1452, 79),(1452, 80),(1452, 212),(1951, 1),(1951, 71),(1951, 8),(861, 185),(1987, 301),(1986, 447),(1987, 286),(1986, 448),(1451, 157),(1449, 157),(1986, 449),(1986, 450),(1986, 451),(1986, 44),(1986, 42),(1986, 45),(1986, 452),(1986, 47),(1986, 40),(1986, 41),(1986, 49),(1986, 37),(1986, 453),(1986, 454),(1986, 455),(1986, 456),(1986, 457),(1986, 458),(1986, 459),(1986, 460),(1986, 461),(1986, 39),(1986, 462),(1986, 48),(1986, 463),(1986, 50),(1986, 51),(1987, 278),(1986, 464),(1986, 465),(1986, 466),(1986, 468),(1986, 469),(1986, 470),(1986, 471),(1986, 472),(1986, 473),(1986, 474),(1986, 51),(1987, 276),(1987, 292),(1987, 290),(1987, 291),(1987, 289),(1987, 295),(1987, 288),(1987, 294),(1987, 280),(1987, 304),(1987, 293),(1987, 369),(1987, 299),(1987, 297),(1987, 298),(1987, 300),(1987, 302),(1987, 37),(1987, 282),(1987, 1),(1987, 277),(1987, 6),(1987, 287),(1987, 279),(1987, 296),(1987, 283),(1987, 24),(1987, 305),(1987, 303),(1987, 284),(1987, 281),(2, 376),(2, 296),(2, 25),(2, 24),(2, 26),(2, 27),(2, 28),(2, 29),(2, 30),(4, 1),(4, 81),(4, 377),(4, 378),(5, 6),(6666, 1),(6666, 2),(6666, 3),(6666, 4),(6666, 1),(6666, 2),(6666, 3),(6666, 4),(6666, 5),(6666, 6),(69, 1),(69, 2),(69, 3),(69, 4),(69, 5),(69, 6),(69, 7),(69, 15),(69, 475),(69, 476),(69, 477),(69, 478),(77, 1),(77, 2),(77, 5),(666777, 1),(666777, 2),(666777, 3),(666777, 4),(666777, 5),(1986, 43),(1986, 46),(1986, 480),(1986, 479);
COMMIT;
BEGIN;
LOCK TABLE "public"."interested" IN SHARE MODE;
DELETE FROM "public"."interested";
INSERT INTO "public"."interested" ("id","name","last_name","e_mail","phone","type_person") VALUES (183, 'JOSE FERNANDO', 'FONSECA PEDRAZA', 'ffonseca@abcdigital.mx', '6677847432', 2),(181, 'FERNANDO', 'FONSECA PEDRAZA', 'ffonseca@abcdigital.mx', '5566778899', 1),(182, 'FERNANDO', 'FONSECA PEDRAZA', 'ffonseca@abcdigital.mx', '5566778899', 1),(184, 'DANIEL ', 'CHAVEZ PEREZ', 'chavez@gmail.com', '6677889977', 2),(185, 'FERNANDO', 'FONSECA PEDRAZA', 'ffonseca@abcdigital.mx', '5536568741', 2),(186, 'Fernando', ' fonseca pedraza', '', '666555444667', 2),(187, 'FERNANDO', 'FONSECA PEDRAZA', '', '5536568741', 2),(188, 'FERNANDO', 'FONSECA PEDRAZA', '', '5536568741', 2),(189, 'FERNANDO', 'FONSECA PEDRAZA', 'ffonseca@abcdigital.mx', '5566778899', 2),(190, 'FERNANDO', 'FONSECA PEDRAZA', 'ffonseca@abcdigital.mx', '5566778899', 2),(191, 'fonseca', 'pedraza fernando', 'ffonseca@abcdigital.mx', '5546568741', 1),(192, 'fernando', 'fonseca pedraza', 'ffonseca@abcdigital.mx', '5536568741', 1),(193, 'fernando', 'fonseca pedraza', 'ffonseca@abcdigital.mx', '5536568741', 1),(195, 'FERNANDO', 'FONSECA PEDRAZA', 'ffonseca@abcdigital.mx', '5566778899', 2),(196, 'CHAVEZ', 'PEREZ ROJAS', 'ffonseca@abcdigital.mx', '5526568741', 2),(198, 'CHAVEZ', 'PEREZ ROJAS', 'ffonseca@abcdigital.mx', '5526568741', 2),(199, 'CHAVEZ', 'PEREZ ROJAS', 'ffonseca@abcdigital.mx', '5526568741', 2),(200, 'sadfkjasf', 'lkjsdfklsjdf lkjsdflkjsf', 'ffonseca@abcdigital.mx', '5536568741', 1),(201, 'FERNANDO', 'FONSECA PEDRAZA', 'ffonseca@abcdigital.mx', '5536568741', 2),(202, 'FERNANDO', 'FONSECA PEDRAZA', 'ffonseca@abcdigital.mx', '5566778899', 2),(194, 'FERNANDO', 'FONSECA PEDRAZA', 'ffonseca@abcdigital.mx', '5566778899', 2),(197, 'CHAVEZ LOCO', 'PEREZ ROJAS', 'ffonseca@abcdigital.mx', '5526568741', 2),(203, 'pau´s', 'hola pruebas', 'fernando', '5566778899', 2),(204, 'SADCSDC', 'SDCVSDV SDVSDV', 'fernando', '5566778899', 2),(205, 'DFSDV', 'SDCVSDV SDSDV', 'ffonseca@abcdigital.mx', '2345678765', 2),(206, 'DFSDV', 'SDCVSDV SDSDV', 'ffonseca@abcdigital.mx', '2345678765', 2),(207, 'DFSDV', 'SDCVSDV SDSDV', 'ffonseca@abcdigital.mx', '2345678765', 2),(208, 'DFSDV', 'SDCVSDV SDSDV', 'ffonseca@abcdigital.mx', '2345678765', 2),(209, 'DFSDV', 'SDCVSDV SDSDV', 'ffonseca@abcdigital.mx', '2345678765', 2),(210, 'DFSDV', 'SDCVSDV SDSDV', 'ffonseca@abcdigital.mx', '2345678765', 2),(211, 'adsfadf', 'asdfasdf ADFSASDF', '', '123456', 2),(212, 'FERNANDO', 'FONSECA PEDRAZA', 'ffonseca@abcdigital.mx', '4567876556', 2),(213, 'FERNANDO', 'FONSECA PEDRAZA', 'ffonseca@abcdigital.mx', '5678987654', 2),(214, 'FERNANDO', 'FONSECA PEDRAZA', 'ffonseca@abcdigital.mx', '5678987654', 2),(215, 'FERNANDO', 'FONSECA PEDRAZA', 'ffonseca@abcdigital.mx', '5678987654', 2),(216, 'ROGELIO ', 'PEDRO CHAVEZ', 'ffonseca@abcdigital.mx', '456787654677', 2);
COMMIT;
BEGIN;
LOCK TABLE "public"."log" IN SHARE MODE;
DELETE FROM "public"."log";
INSERT INTO "public"."log" ("id","id_user","description","date_time") VALUES (107, 1, 'Un usuario se ingreso de manera correcta', '2018-09-16 21:36:06.174982-05'),(108, 1, 'El usuario se actualizo de manera correcta.', '2018-09-16 21:36:32.632496-05'),(109, 1, 'El usuario se elimino de manera correcta', '2018-09-16 21:36:42.355052-05'),(110, 1, 'El tramite se guardo de manera correcta', '2018-09-16 21:39:49.767771-05'),(111, 1, 'Se ejecunto de manera corrercta la actualizacion', '2018-09-16 21:40:07.664795-05'),(112, 1, 'Se elimino de manera correcta el tramite.', '2018-09-16 21:41:16.528734-05'),(113, 1, 'Se ingreso la modalidad de manera correcta', '2018-09-16 21:47:52.531384-05'),(114, 1, 'Se actualizaron los datos de manera correcta', '2018-09-16 21:48:33.331717-05'),(115, 1, 'Se actualizaron los datos de manera correcta', '2018-09-16 21:48:48.448582-05'),(116, 1, 'El registro se elimino de manera correcta.', '2018-09-16 21:48:56.407037-05'),(117, 1, 'Se guardaron de manera correcta los nuevos requisitos.', '2018-09-16 21:52:25.465995-05'),(118, 1, 'Se actualizo de manera correcta el requisito.', '2018-09-16 21:52:39.515798-05'),(119, 1, 'El registro se elimino de manera correcta.', '2018-09-16 21:52:51.626491-05'),(120, 1, 'El usuario cerro sesion', '2018-09-16 21:55:41.210191-05'),(121, 1, 'El usuario fernando ingreso al sistema de manera correcta.', '2018-09-16 21:55:48.213591-05'),(122, 1, 'Se ingreso finiquito de manera correcta correcta la solicitud 166', '2018-09-16 22:00:36.520081-05'),(123, 1, 'El usuario cerro sesion', '2018-09-16 22:01:45.987055-05'),(124, 1, 'El usuario fernando ingreso al sistema de manera correcta.', '2018-09-17 10:07:51.728769-05'),(125, 1, 'El usuario fernando ingreso al sistema de manera correcta.', '2018-09-17 10:32:18.79168-05'),(126, 1, 'El usuario fernando ingreso al sistema de manera correcta.', '2018-09-17 10:32:29.295281-05'),(127, 1, 'Se elimino de manera correcta el tramite.', '2018-09-17 10:32:42.891058-05'),(128, 1, 'Se ingreso la modalidad de manera correcta', '2018-09-17 11:05:31.352648-05'),(129, 1, 'El usuario cerro sesion', '2018-09-17 11:15:34.866167-05'),(130, 1, 'El usuario fernando ingreso al sistema de manera correcta.', '2018-09-17 12:06:14.871045-05'),(131, 1, 'El usuario fernando ingreso al sistema de manera correcta.', '2018-09-17 12:08:43.813564-05'),(132, 1, 'El usuario fernando ingreso al sistema de manera correcta.', '2018-09-18 10:31:58.902654-05'),(133, 1, 'El usuario fernando ingreso al sistema de manera correcta.', '2018-09-18 18:54:43.370963-05'),(134, 1, 'El usuario fernando ingreso al sistema de manera correcta.', '2018-09-19 11:31:19.34329-05'),(135, 1, 'Se ingreso finiquito de manera correcta correcta la solicitud 174', '2018-09-19 12:36:47.510968-05'),(136, 1, 'Se ingreso finiquito de manera correcta correcta la solicitud 175', '2018-09-19 12:52:11.256803-05'),(137, 1, 'El usuario fernando ingreso al sistema de manera correcta.', '2018-09-21 10:26:13.398513-05'),(138, 1, 'El usuario fernando ingreso al sistema de manera correcta.', '2018-09-21 10:37:22.930419-05'),(139, 1, 'El usuario fernando ingreso al sistema de manera correcta.', '2018-09-21 11:48:38.637975-05'),(140, 1, 'El usuario fernando ingreso al sistema de manera correcta.', '2018-09-21 12:49:20.382271-05'),(141, 1, 'El usuario fernando ingreso al sistema de manera correcta.', '2018-09-21 12:49:59.70752-05'),(142, 1, 'El usuario fernando ingreso al sistema de manera correcta.', '2018-09-21 13:21:21.956179-05'),(143, 1, 'Se ingreso finiquito de manera correcta correcta la solicitud 180', '2018-09-21 14:36:09.166832-05'),(144, 1, 'El usuario fernando ingreso al sistema de manera correcta.', '2018-09-21 17:39:40.989673-05');
COMMIT;
BEGIN;
LOCK TABLE "public"."modules" IN SHARE MODE;
DELETE FROM "public"."modules";
INSERT INTO "public"."modules" ("id","name","status") VALUES (1, 'solicitud', 1),(2, 'usuarios', 1),(3, 'tramites', 1),(4, 'modalidades', 1),(5, 'requisitos', 1),(6, 'seguimiento', 1);
COMMIT;
BEGIN;
LOCK TABLE "public"."permission" IN SHARE MODE;
DELETE FROM "public"."permission";
INSERT INTO "public"."permission" ("id","id_user","id_module","read_permission","write_permission","delete_permission","update_permission") VALUES (52, 1, 1, 1, 1, 1, 1),(53, 1, 2, NULL, NULL, 1, NULL);
COMMIT;
BEGIN;
LOCK TABLE "public"."request" IN SHARE MODE;
DELETE FROM "public"."request";
INSERT INTO "public"."request" ("id","folio","id_user","date_creation","observations","id_status","id_address","id_interested","id_transact","id_vulnerable_group","request_turn","type_work","means_request","delivery","date_commitment","flag","id_modality","prevent_date","citizen_prevent_date","correct_date","cancelation_description","last_status","settlement_date","settlement_number") VALUES (179, '20/2018', 1, '2018-09-18', 'observaciones', 1, 240, 202, 907, 1, '', '', 2, 1, '2018-09-18', 1, 640, NULL, NULL, NULL, NULL, NULL, NULL, NULL),(184, '25/2018', 1, '2018-09-21', 'holaaa', 1, 252, 212, 1, 1, '', '', 1, NULL, '2018-11-02', 1, 31, NULL, NULL, NULL, NULL, NULL, NULL, NULL),(166, '7/2018', 1, '2019-01-01', 'la solcitud esta co,mpleta', 6, 223, 189, 5, 1, '', '', 2, 1, '2018-11-09', 1, NULL, NULL, NULL, NULL, 'se cancela por motivos de no se', 3, '2018-09-07', '12345sdf'),(176, '17/2018', 1, '2018-09-17', 'ASDASGDFBGADFBV', 9, 236, 199, 5, 1, '', '', 2, 1, '2018-11-12', 1, NULL, '2018-09-12', '2018-09-13', '2018-09-20', NULL, NULL, NULL, NULL),(181, '22/2018', 1, '2018-09-19', 'ASFSFBFB', 8, 244, 204, 5, 1, '', '', 2, 1, '2018-11-14', 1, NULL, '2018-09-05', '2018-09-12', '2018-09-25', NULL, 2, NULL, NULL),(185, '26/2018', 1, '2018-09-21', 'observaciones de prueba creando lel registro de dictamen', 1, 253, 213, 5, 1, '', '', 1, NULL, '2018-11-16', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),(187, '28/2018', 1, '2018-09-21', 'observaciones de prueba creando lel registro de dictamen', 8, 255, 215, 5, 1, '', '', 1, NULL, '2018-11-16', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),(186, '27/2018', 1, '2018-09-21', 'observaciones de prueba creando lel registro de dictamen', 8, 254, 214, 5, 1, '', '', 1, NULL, '2018-11-16', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),(164, '5/2018', 1, '2018-09-12', 'El usuario no cumplió con los requisitos de manera correcta.', 2, 221, 187, 1434, 1, '', '', 2, 1, '2018-11-07', 1, NULL, NULL, NULL, NULL, 'Cancelacion debido a que se capturaron de manera errónea los datos', 3, NULL, NULL),(165, '6/2018', 1, '2018-09-12', 'el usuario tiene de manera correcta todos los documentos', 1, 222, 188, 857, 1, '', '', 2, 1, '2018-10-03', 1, 319, NULL, NULL, NULL, NULL, NULL, NULL, NULL),(180, '21/2018', 1, '2018-09-19', 'observaciones', 6, 243, 203, 5, 1, '', '', 2, 1, '2018-11-14', 1, NULL, '2018-09-07', '2018-09-20', '2018-09-13', NULL, 2, '2018-09-17', '6666666-999-FOPRTO'),(171, '12/2018', 1, '2018-09-17', 'observaciones !!!1', 1, 231, 194, 5, 1, '', '', 2, 1, '2018-11-12', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),(188, '29/2018', 1, '2018-09-21', 'observaciones de prueba con el nuevo dictamen', 8, 256, 216, 1, 1, '', '', 1, NULL, '2018-11-02', 1, 31, NULL, NULL, NULL, 'dgtfhy', 3, NULL, NULL),(167, '8/2018', 1, '2018-09-17', 'sin observaciones', 1, 224, 190, 5, 1, '', '', 2, 1, '2018-11-12', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),(168, '9/2018', 1, '2018-09-17', 'observaciones !!!', 1, 228, 191, 5, 1, '', '', 2, 1, '2018-11-12', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),(169, '10/2018', 1, '2018-09-17', 'observaciones', 1, 229, 192, 5, 1, '', '', 2, 1, '2018-11-12', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),(170, '11/2018', 1, '2018-09-17', 'observaciones', 1, 230, 193, 5, 1, '', '', 2, 1, '2018-11-12', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),(172, '13/2018', 1, '2018-09-17', 'observaciones !!!1', 1, 232, 195, 5, 1, '', '', 2, 1, '2018-11-12', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),(173, '14/2018', 1, '2018-09-17', 'ASDASGDFBGADFBV', 2, 233, 196, 5, 1, '', '', 2, 1, '2018-11-12', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),(177, '18/2018', 1, '2018-09-17', 'asdfsgfadsfg', 1, 237, 200, 5, 2, '', '', 2, 1, '2018-11-12', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),(163, '4/2018', 1, '2018-09-12', 'ACTUALIZACION DE OBSERVACIONES', 3, 220, 186, 907, 2, '', '', 2, 1, '2018-11-07', 1, 640, '2018-09-05', NULL, '2018-09-10', 'Cancelada', 7, NULL, NULL),(162, '3/2018', 1, '2018-09-11', 'solicitud completa', 3, 218, 184, 1, 1, '', '', 2, 1, '2018-10-23', 1, 32, NULL, NULL, NULL, 'Cancelada', 7, NULL, NULL),(161, '2/2018', 1, '2018-09-11', 'observaciones esto esta incompleto', 3, 217, 183, 1434, 1, 'GIRO O USO', 'TIPO DE OBRAA', 1, 2, '2018-11-06', 1, NULL, '2018-09-12', NULL, '2018-09-11', 'Cancelada', 7, NULL, NULL),(160, '1/2018', 1, '2018-09-11', 'OBSERVACIONES DE PRUEBA PARA GUARDAR LOS REQUSITOS DE LA SOLICITUD', 1, 216, 182, 5, 1, 'GIRO O USO', 'TIPO DE OBRA', 2, 1, '2018-11-06', 1, NULL, NULL, NULL, NULL, 'motivo de cancelacion', 3, NULL, NULL),(178, '19/2018', 1, '2018-09-18', '', 1, 239, 201, 907, 1, '', '', 2, 1, '2018-09-18', 1, 639, NULL, NULL, NULL, NULL, NULL, NULL, NULL),(174, '15/2018', 1, '2018-09-17', 'ASDASGDFBGADFBV', 6, 234, 197, 5, 1, '', '', 2, 1, '2018-11-12', 1, NULL, '2018-09-05', '2018-09-06', '2018-09-06', 'motivo de cancelacion', 2, '2018-09-07', '6666666-999'),(175, '16/2018', 1, '2018-09-17', 'ASDASGDFBGADFBV', 6, 235, 198, 5, 1, '', '', 2, 1, '2018-11-12', 1, NULL, '2018-09-19', '2018-09-20', '2018-09-22', NULL, NULL, '2018-09-05', 'adsf'),(182, '23/2018', 1, '2018-09-19', 'CZKJCNH', 1, 250, 210, 1, 1, '', '', 1, NULL, '2018-10-31', 1, 31, NULL, NULL, NULL, NULL, NULL, NULL, NULL),(183, '24/2018', 1, '2018-09-19', 'asdfasdasdg', 1, 251, 211, 1, 3, '', '', 2, NULL, '2018-10-31', 1, 31, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;
BEGIN;
LOCK TABLE "public"."request_audit" IN SHARE MODE;
DELETE FROM "public"."request_audit";
COMMIT;
BEGIN;
LOCK TABLE "public"."request_requirement" IN SHARE MODE;
DELETE FROM "public"."request_requirement";
INSERT INTO "public"."request_requirement" ("id_request","id_requirement") VALUES (161, 3),(160, 327),(160, 328),(160, 329),(161, 6),(162, 1),(163, 1),(163, 6),(163, 8),(163, 326),(164, 3),(164, 6),(164, 26),(164, 35),(164, 78),(165, 1),(165, 4),(165, 6),(165, 8),(165, 31),(165, 56),(165, 61),(165, 62),(166, 327),(166, 328),(166, 329),(167, 6),(167, 327),(167, 328),(167, 329),(168, 6),(168, 327),(168, 328),(168, 329),(172, 6),(172, 327),(172, 328),(177, 6),(177, 327),(177, 328),(177, 329),(178, 324),(178, 8),(178, 6),(178, 325),(179, 326),(179, 6),(179, 8),(179, 1),(171, 6),(171, 327),(171, 328),(174, 6),(174, 327),(174, 328),(174, 329),(180, 6),(180, 327),(180, 328),(181, 6),(181, 327),(181, 328),(181, 329),(182, 1),(183, 1),(184, 1),(185, 6),(185, 327),(185, 328),(185, 329),(186, 6),(186, 327),(186, 328),(186, 329),(187, 6),(187, 327),(187, 328),(187, 329),(188, 1);
COMMIT;
BEGIN;
LOCK TABLE "public"."role" IN SHARE MODE;
DELETE FROM "public"."role";
INSERT INTO "public"."role" ("id","description") VALUES (2, 'Operador'),(1, 'Administrador'),(3, 'Responsable');
COMMIT;
BEGIN;
LOCK TABLE "public"."tracing_dictum" IN SHARE MODE;
DELETE FROM "public"."tracing_dictum";
INSERT INTO "public"."tracing_dictum" ("id","date_receives_dictum","folio_dictum","answer_dictum","citizen_dictum","id_request") VALUES (12, '2018-09-05', '9988/COF/012018', 2, '2018-09-10', 161),(13, '2018-09-05', '9988/COF/012018', 1, '2018-09-13', 162),(14, '2018-09-06', '9988/COF/012018', 1, '2018-09-16', 163),(15, '2018-09-12', 'q2345rt43wdef', 2, '2018-09-04', 166),(16, '2018-09-12', 'gjtu9876', 1, '2018-09-19', 175),(17, '2018-09-19', '7UIHD', 2, '2018-09-19', 174),(22, '2018-09-05', '9988/COF/012018', 1, NULL, 187),(23, '2018-09-01', '9988/COF/012018', 1, '2018-09-04', 186),(18, '2018-09-06', '9988/COF/012018', 1, '2018-09-03', 181),(24, '2018-09-06', '9988/COF/012018', 1, '2018-09-19', 180),(25, '2018-09-06', '9988/COF/012018', 1, '2018-09-29', 188);
COMMIT;
BEGIN;
LOCK TABLE "public"."tracing_turn" IN SHARE MODE;
DELETE FROM "public"."tracing_turn";
INSERT INTO "public"."tracing_turn" ("id","id_request","date_turn","review_area","observations") VALUES (29, 160, '2018-09-05', 3, 'adfsfgadfg'),(30, 162, '2018-09-17', 2, ''),(31, 163, '2018-09-13', 2, 'observaciones al turnar'),(32, 166, '2018-09-19', 5, 'observaciones de turnado'),(33, 174, '2018-09-12', 2, 'adfsdg'),(34, 181, '2018-09-19', 5, 'observaciones a la hora de turnar'),(35, 187, '2018-09-12', 5, 'observaciuones para dictamen'),(36, 188, '2018-09-21', 5, 'observaciones de turnado'),(37, 180, '2018-09-04', 3, 'sdasfbg');
COMMIT;
BEGIN;
LOCK TABLE "public"."user_audit" IN SHARE MODE;
DELETE FROM "public"."user_audit";
COMMIT;
BEGIN;
LOCK TABLE "public"."users" IN SHARE MODE;
DELETE FROM "public"."users";
INSERT INTO "public"."users" ("id","user_name","employee_number","description","rol","user_register","date_register","status","password") VALUES (41, 'chavez', 4, 'operador', 2, 1, '2018-09-05 19:40:15.950239-05', 0, 'd41d8cd98f00b204e9800998ecf8427e'),(1, 'fernando', 1, 'super usuario 666 ', 1, 1, '2018-08-15 18:35:51.888068-05', 1, '4c882dcb24bcb1bc225391a602feca7c'),(38, 'lucia', 2, 'proyect manager', 1, 1, '2018-09-05 19:38:29.064125-05', 1, 'ee2ec3cc66427bb422894495068222a8'),(40, 'sergio', 3, 'developer', 2, 1, '2018-09-05 19:39:23.391233-05', 0, '5e8edd851d2fdfbd7415232c67367cc3'),(45, 'silviaa', 5, 'descripcion de prueba para empleado', 2, 1, '2018-09-16 21:36:06.165982-05', 0, '4c882dcb24bcb1bc225391a602feca7c');
COMMIT;
BEGIN;
LOCK TABLE "public"."vulnerable_group" IN SHARE MODE;
DELETE FROM "public"."vulnerable_group";
INSERT INTO "public"."vulnerable_group" ("id","description","status") VALUES (1, 'No pertenece al grupo vulnerable', 1),(2, 'Adultos mayores', 1),(3, 'Personas con discapacidades', 1),(4, 'Mujeres embarazadas y jefas de familia', 1),(5, 'Niños Y Niñas', 1),(6, 'Pueblos Indígenas', 1),(7, 'Personas con enfermedades mentales', 1),(8, 'Personas con VIH/SIDA', 1),(9, 'Trabajadores migrantes', 1),(10, 'Minorías sexuales', 1),(11, 'Personas detenidas', 1),(12, 'Personas en situación de calle', 1);
COMMIT;
ALTER TABLE "address" ADD CONSTRAINT "address_pkey" PRIMARY KEY ("id");
ALTER TABLE "cat_area" ADD CONSTRAINT "cat_area_pkey" PRIMARY KEY ("id");
ALTER TABLE "cat_modality" ADD CONSTRAINT "cat_modality_pkey" PRIMARY KEY ("id");
ALTER TABLE "cat_requirement" ADD CONSTRAINT "cat_requeriment_pkey" PRIMARY KEY ("id");
ALTER TABLE "cat_status" ADD CONSTRAINT "cat_status_pkey" PRIMARY KEY ("id");
ALTER TABLE "cat_transact" ADD CONSTRAINT "cat_transact_pkey" PRIMARY KEY ("id");
ALTER TABLE "interested" ADD CONSTRAINT "interested_pkey" PRIMARY KEY ("id");
ALTER TABLE "log" ADD CONSTRAINT "log_pkey" PRIMARY KEY ("id");
ALTER TABLE "modules" ADD CONSTRAINT "modules_pkey" PRIMARY KEY ("id");
ALTER TABLE "permission" ADD CONSTRAINT "permission_pkey" PRIMARY KEY ("id");
ALTER TABLE "request" ADD CONSTRAINT "request_pkey" PRIMARY KEY ("id");
ALTER TABLE "request_audit" ADD CONSTRAINT "request_audit_pkey" PRIMARY KEY ("id");
ALTER TABLE "role" ADD CONSTRAINT "ROLE_pkey" PRIMARY KEY ("id");
ALTER TABLE "tracing_dictum" ADD CONSTRAINT "tracing_dictum_pkey" PRIMARY KEY ("id");
ALTER TABLE "tracing_turn" ADD CONSTRAINT "tracing_turn_pkey" PRIMARY KEY ("id");
ALTER TABLE "user_audit" ADD CONSTRAINT "user_audit_pkey" PRIMARY KEY ("id");
ALTER TABLE "users" ADD CONSTRAINT "USERS_pkey" PRIMARY KEY ("id");
ALTER TABLE "vulnerable_group" ADD CONSTRAINT "vulnerable_group_pkey" PRIMARY KEY ("id");
ALTER TABLE "cat_modality" ADD CONSTRAINT "cat_modality_code_key" UNIQUE ("code");
ALTER TABLE "cat_modality" ADD CONSTRAINT "cat_modality_id_transact_fkey" FOREIGN KEY ("id_transact") REFERENCES "public"."cat_transact" ("code") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "cat_modality_requirement" ADD CONSTRAINT "cat_modality_requirement_id_modality_fkey" FOREIGN KEY ("id_modality") REFERENCES "public"."cat_modality" ("code") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "cat_modality_requirement" ADD CONSTRAINT "cat_modality_requirement_id_requirement_fkey" FOREIGN KEY ("id_requirement") REFERENCES "public"."cat_requirement" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "cat_transact" ADD CONSTRAINT "cat_transact_code_key" UNIQUE ("code");
ALTER TABLE "cat_transact_requirement" ADD CONSTRAINT "cat_transact_requirement_id_requirement_fkey" FOREIGN KEY ("id_requirement") REFERENCES "public"."cat_requirement" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "cat_transact_requirement" ADD CONSTRAINT "cat_transact_requirement_id_transact_fkey" FOREIGN KEY ("id_transact") REFERENCES "public"."cat_transact" ("code") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "tracing_turn" ADD CONSTRAINT "review_area_fk" FOREIGN KEY ("review_area") REFERENCES "public"."cat_area" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "users" ADD CONSTRAINT "users_employee_number_key" UNIQUE ("employee_number");
ALTER TABLE "users" ADD CONSTRAINT "users_id_fkey" FOREIGN KEY ("rol") REFERENCES "public"."role" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER SEQUENCE "ROLE_id_seq"
OWNED BY "role"."id";
SELECT setval('"ROLE_id_seq"', 4, true);
ALTER SEQUENCE "ROLE_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "USERS_Id_seq"
OWNED BY "users"."id";
SELECT setval('"USERS_Id_seq"', 46, true);
ALTER SEQUENCE "USERS_Id_seq" OWNER TO "postgres";
ALTER SEQUENCE "address_id_seq"
OWNED BY "address"."id";
SELECT setval('"address_id_seq"', 257, true);
ALTER SEQUENCE "address_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "cat_area_id_seq"
OWNED BY "cat_area"."id";
SELECT setval('"cat_area_id_seq"', 2, false);
ALTER SEQUENCE "cat_area_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "cat_modality_id_seq"
OWNED BY "cat_modality"."id";
SELECT setval('"cat_modality_id_seq"', 62, true);
ALTER SEQUENCE "cat_modality_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "cat_requeriment_id_seq"
OWNED BY "cat_requirement"."id";
SELECT setval('"cat_requeriment_id_seq"', 485, true);
ALTER SEQUENCE "cat_requeriment_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "cat_status_id_seq"
OWNED BY "cat_status"."id";
SELECT setval('"cat_status_id_seq"', 2, true);
ALTER SEQUENCE "cat_status_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "cat_transact_id_seq"
OWNED BY "cat_transact"."id";
SELECT setval('"cat_transact_id_seq"', 40, true);
ALTER SEQUENCE "cat_transact_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "interested_id_seq"
OWNED BY "interested"."id";
SELECT setval('"interested_id_seq"', 217, true);
ALTER SEQUENCE "interested_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "log_id_seq"
OWNED BY "log"."id";
SELECT setval('"log_id_seq"', 145, true);
ALTER SEQUENCE "log_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "modules_id_seq"
OWNED BY "modules"."id";
SELECT setval('"modules_id_seq"', 2, false);
ALTER SEQUENCE "modules_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "permission_id_seq"
OWNED BY "permission"."id";
SELECT setval('"permission_id_seq"', 54, true);
ALTER SEQUENCE "permission_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "request_id_seq"
OWNED BY "request"."id";
SELECT setval('"request_id_seq"', 189, true);
ALTER SEQUENCE "request_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "tracing_dictum_id_seq"
OWNED BY "tracing_dictum"."id";
SELECT setval('"tracing_dictum_id_seq"', 26, true);
ALTER SEQUENCE "tracing_dictum_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "tracing_turn_id_seq"
OWNED BY "tracing_turn"."id";
SELECT setval('"tracing_turn_id_seq"', 38, true);
ALTER SEQUENCE "tracing_turn_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "vulnerable_group_id_seq"
OWNED BY "vulnerable_group"."id";
SELECT setval('"vulnerable_group_id_seq"', 2, false);
ALTER SEQUENCE "vulnerable_group_id_seq" OWNER TO "postgres";
