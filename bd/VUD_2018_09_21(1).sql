PGDMP     /    2                v           prueba    10.4    10.4 �               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false                       1262    16393    prueba    DATABASE     �   CREATE DATABASE prueba WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Mexico.1252' LC_CTYPE = 'Spanish_Mexico.1252';
    DROP DATABASE prueba;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false                       0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            	           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16402    users    TABLE     {  CREATE TABLE public.users (
    id integer NOT NULL,
    user_name character varying(255) NOT NULL,
    employee_number integer NOT NULL,
    description character varying(255) NOT NULL,
    rol integer NOT NULL,
    user_register integer NOT NULL,
    date_register timestamp with time zone NOT NULL,
    status integer NOT NULL,
    password character varying(300) NOT NULL
);
    DROP TABLE public.users;
       public         postgres    false    3                       1255    25007 /   user_auth(character varying, character varying)    FUNCTION     	  CREATE FUNCTION public.user_auth(_user_name character varying, _password character varying) RETURNS SETOF public.users
    LANGUAGE sql
    AS $$ 
		SELECT
			* 
		FROM
			public."users" 
		WHERE
			user_name = _user_name 
			AND password = _password;
	$$;
 [   DROP FUNCTION public.user_auth(_user_name character varying, _password character varying);
       public       postgres    false    3    197                       1255    33511 /   vud_activate_request(integer, integer, integer)    FUNCTION       CREATE FUNCTION public.vud_activate_request(_id_request integer, _status integer, _last_status integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE 
	public.request
SET 
	id_status = _last_status,
	last_status = _status
WHERE 
	id = _id_request;
END
$$;
 g   DROP FUNCTION public.vud_activate_request(_id_request integer, _status integer, _last_status integer);
       public       postgres    false    1    3            �            1259    33388    cat_area    TABLE     k   CREATE TABLE public.cat_area (
    id integer NOT NULL,
    description character varying(100) NOT NULL
);
    DROP TABLE public.cat_area;
       public         postgres    false    3            �            1255    33447    vud_area_record()    FUNCTION     �   CREATE FUNCTION public.vud_area_record() RETURNS SETOF public.cat_area
    LANGUAGE plpgsql
    AS $$
BEGIN

    RETURN QUERY SELECT * from cat_area order by id asc;

END
$$;
 (   DROP FUNCTION public.vud_area_record();
       public       postgres    false    225    1    3            �            1255    33460 J   vud_create_dictum_request(date, character varying, integer, date, integer)    FUNCTION     0  CREATE FUNCTION public.vud_create_dictum_request(_date_receives_dictum date, _folio_dictum character varying, _answer_dictum integer, _citizen_dictum date, _id_request integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
INSERT INTO public.tracing_dictum(
			date_receives_dictum, 
			folio_dictum, 
			answer_dictum, 
			citizen_dictum, 
			id_request)
VALUES (_date_receives_dictum,
		_folio_dictum,
		_answer_dictum,
		_citizen_dictum,
		_id_request);

UPDATE public.request
SET 
	id_status = 8
WHERE 
	id = _id_request;

END
$$;
 �   DROP FUNCTION public.vud_create_dictum_request(_date_receives_dictum date, _folio_dictum character varying, _answer_dictum integer, _citizen_dictum date, _id_request integer);
       public       postgres    false    1    3                        1255    33537 _   vud_create_modality(integer, character varying, character varying, integer, integer, character)    FUNCTION     �  CREATE FUNCTION public.vud_create_modality(OUT code_modality integer, _code integer, _description character varying, _legal_foundation character varying, _id_transact integer, _days integer, _type_days character) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
DECLARE code_inserted_modality integer;
BEGIN 
	INSERT INTO public.cat_modality(
	code, 
	description, 
	legal_foundation, 
	id_transact,
	status,
	days,
	type_days)
	VALUES (_code, 
			_description, 
			_legal_foundation, 
			_id_transact,
			1,
			_days,
			_type_days)
	RETURNING ID into code_inserted_modality;

	RETURN QUERY SELECT code from public.cat_modality where id = code_inserted_modality;
END 
$$;
 �   DROP FUNCTION public.vud_create_modality(OUT code_modality integer, _code integer, _description character varying, _legal_foundation character varying, _id_transact integer, _days integer, _type_days character);
       public       postgres    false    3    1                       1255    33582 :   vud_create_request_requeriment(character varying, integer)    FUNCTION     �  CREATE FUNCTION public.vud_create_request_requeriment(_folio character varying, _id_requeriment integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE _id_request integer;
BEGIN

_id_request = (SELECT 
	id 
from 
	public.request as a
where 
	a.folio = _folio);

INSERT INTO public.request_requirement(
	id_request, 
	id_requirement)
VALUES (_id_request,_id_requeriment);

END
$$;
 h   DROP FUNCTION public.vud_create_request_requeriment(_folio character varying, _id_requeriment integer);
       public       postgres    false    3    1            �            1255    16735 )   vud_create_requirement(character varying)    FUNCTION     �   CREATE FUNCTION public.vud_create_requirement(_description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO public.cat_requirement(description,status)
	VALUES (_description,1);
END
$$;
 M   DROP FUNCTION public.vud_create_requirement(_description character varying);
       public       postgres    false    1    3            $           1255    33519 7   vud_create_settlement(integer, date, character varying)    FUNCTION     Q  CREATE FUNCTION public.vud_create_settlement(_id_request integer, _settlement_date date, _settlement_number character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE public.request
SET settlement_date= _settlement_date,
	settlement_number= _settlement_number,
	id_status = 6
WHERE id = _id_request;

END
$$;
 ~   DROP FUNCTION public.vud_create_settlement(_id_request integer, _settlement_date date, _settlement_number character varying);
       public       postgres    false    1    3            �            1255    33525 K   vud_create_turn_request(integer, date, integer, character varying, integer)    FUNCTION     �  CREATE FUNCTION public.vud_create_turn_request(_id_request integer, _date_turn date, _review_area integer, _observations character varying, _id_status integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
INSERT INTO public.tracing_turn(
	id_request, 
	date_turn, 
	review_area, 
	observations)
VALUES (_id_request,
		_date_turn,
		_review_area,
		_observations);

UPDATE 
	public.request
SET 
	id_status = 5,
	last_status = _id_status
WHERE 
	id = _id_request;

END
$$;
 �   DROP FUNCTION public.vud_create_turn_request(_id_request integer, _date_turn date, _review_area integer, _observations character varying, _id_status integer);
       public       postgres    false    1    3            �            1259    25106    vulnerable_group    TABLE     �   CREATE TABLE public.vulnerable_group (
    id integer NOT NULL,
    description character varying(255) NOT NULL,
    status smallint NOT NULL
);
 $   DROP TABLE public.vulnerable_group;
       public         postgres    false    3                       1255    33306    vud_data_vulnerable_group()    FUNCTION     �   CREATE FUNCTION public.vud_data_vulnerable_group() RETURNS SETOF public.vulnerable_group
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY SELECT * FROM vulnerable_group where status = 1 order by id asc;

END
$$;
 2   DROP FUNCTION public.vud_data_vulnerable_group();
       public       postgres    false    3    1    223            
           1255    16755    vud_delete_modality(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_modality(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE public.cat_modality
	SET   status = 0
	WHERE id = _id;

END
$$;
 7   DROP FUNCTION public.vud_delete_modality(_id integer);
       public       postgres    false    3    1            �            1255    16737    vud_delete_requirement(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_requirement(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	UPDATE public.cat_requirement
	SET  status = 0
	WHERE id = _id;
END
$$;
 :   DROP FUNCTION public.vud_delete_requirement(_id integer);
       public       postgres    false    3    1            �            1255    16710    vud_delete_transact(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_transact(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
	UPDATE public.cat_transact
	SET status= 0
	WHERE id = _id;
END
$$;
 7   DROP FUNCTION public.vud_delete_transact(_id integer);
       public       postgres    false    1    3                       1255    25027    vud_delete_user(integer)    FUNCTION     �   CREATE FUNCTION public.vud_delete_user(_id_user integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$

BEGIN

UPDATE public."users" SET status = 0 WHERE "id" = _id_user;
return true;

END
$$;
 8   DROP FUNCTION public.vud_delete_user(_id_user integer);
       public       postgres    false    1    3                       1255    16754 (   vud_eliminate_relation_modality(integer)    FUNCTION     �   CREATE FUNCTION public.vud_eliminate_relation_modality(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
DELETE FROM cat_modality_requirement WHERE id_modality = _id;
END
$$;
 C   DROP FUNCTION public.vud_eliminate_relation_modality(_id integer);
       public       postgres    false    3    1            �            1255    16774 (   vud_eliminate_relation_transact(integer)    FUNCTION     �   CREATE FUNCTION public.vud_eliminate_relation_transact(_id_transact integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	DELETE FROM cat_transact_requirement where id_transact = _id_transact;
END
$$;
 L   DROP FUNCTION public.vud_eliminate_relation_transact(_id_transact integer);
       public       postgres    false    3    1            �            1255    33550    vud_get_last_folio()    FUNCTION     �   CREATE FUNCTION public.vud_get_last_folio() RETURNS SETOF character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
	
	RETURN QUERY select folio from request  order by id desc limit 1;

END
$$;
 +   DROP FUNCTION public.vud_get_last_folio();
       public       postgres    false    1    3            3           1255    33605 &   vud_get_modality_requirements(integer)    FUNCTION       CREATE FUNCTION public.vud_get_modality_requirements(_modality_code integer, OUT id integer, OUT description character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY SELECT 
				c.id,
				c.description 
			FROM 
				public.cat_modality as a
			LEFT JOIN
				public.cat_modality_requirement as b
			on 
				a.code = b.id_modality
			LEFT JOIN 
				public.cat_requirement as c
			on 
				c.id = b.id_requirement
			WHERE
				a.code =  _modality_code
				order by id asc;
END
$$;
    DROP FUNCTION public.vud_get_modality_requirements(_modality_code integer, OUT id integer, OUT description character varying);
       public       postgres    false    1    3                       1255    33588 -   vud_get_request_requirement(integer, integer)    FUNCTION     7  CREATE FUNCTION public.vud_get_request_requirement(_code integer, _type_request integer, OUT id integer, OUT description character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN

IF (_type_request = 2) THEN

RETURN QUERY SELECT 
	c.id,
	c.description
FROM
	public.cat_modality as a
left join
	public.cat_modality_requirement as b
on
	b.id_modality = a.code
left join
	public.cat_requirement as c
on
	b.id_requirement = c.id
	where a.code = _code
	order by c.id asc;

ELSE
	RETURN QUERY SELECT 
		c.id,
		c.description
	FROM 
		public.cat_transact as a
	left join
		public.cat_transact_requirement as b
	on
		a.code = b.id_transact
	left join
		public.cat_requirement as c
	on
		b.id_requirement = c.id
	where a.code = _code
	order by c.id asc;

END IF;

END
$$;
 �   DROP FUNCTION public.vud_get_request_requirement(_code integer, _type_request integer, OUT id integer, OUT description character varying);
       public       postgres    false    3    1                       1255    33603 &   vud_get_transact_requirements(integer)    FUNCTION       CREATE FUNCTION public.vud_get_transact_requirements(_transact_code integer, OUT id integer, OUT description character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY SELECT 
				c.id,
				c.description 
			FROM 
				public.cat_transact as a
			LEFT JOIN
				public.cat_transact_requirement as b
			on 
				a.code = b.id_transact
			LEFT JOIN 
				public.cat_requirement as c
			on 
				c.id = b.id_requirement
			WHERE
				a.code = _transact_code
				order by c.id asc;
END
$$;
    DROP FUNCTION public.vud_get_transact_requirements(_transact_code integer, OUT id integer, OUT description character varying);
       public       postgres    false    3    1            �            1255    33534 V   vud_insert_transact(integer, character varying, character varying, integer, character)    FUNCTION     �  CREATE FUNCTION public.vud_insert_transact(OUT _id_code integer, _code integer, _description character varying, _legal_foundation character varying, _days_transact integer, _type_days_transact character) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
DECLARE _id_transact integer;
BEGIN

	INSERT INTO public.cat_transact(
		code, 
		description, 
		legal_foundation, 
		status,
		days,
		type_days)
	VALUES (_code,
			_description,
			_legal_foundation,
			1,
			_days_transact,
			_type_days_transact)
	
	RETURNING ID into _id_transact;

	RETURN QUERY SELECT code from public.cat_transact where id = _id_transact;

END
$$;
 �   DROP FUNCTION public.vud_insert_transact(OUT _id_code integer, _code integer, _description character varying, _legal_foundation character varying, _days_transact integer, _type_days_transact character);
       public       postgres    false    1    3            #           1255    25018 c   vud_insert_user(character varying, integer, character varying, integer, integer, character varying)    FUNCTION       CREATE FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying) RETURNS void
    LANGUAGE sql
    AS $$ 
	
		INSERT INTO 
	public."users"
		("user_name",
		"employee_number",
		"description",
		"rol", 
		"user_register",
		"date_register",
		"status",
		"password") 
	VALUES (_user_name,
		_employee_number,
		_description,
		_rol,
		_user_register,
		'now()',
		1,
		_password);
		
	$$;
 �   DROP FUNCTION public.vud_insert_user(_user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying);
       public       postgres    false    3                       1255    25125 P   vud_insert_user_permission(integer, integer, integer, integer, integer, integer)    FUNCTION       CREATE FUNCTION public.vud_insert_user_permission(_id_user integer, _id_module integer, _read_permission integer, _write_permission integer, _delete_permission integer, _update_permission integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

INSERT INTO public.permission(
	id_user,
	id_module, 
	read_permission, 
	write_permission, 
	delete_permission, 
	update_permission)
VALUES (
	_id_user, 
	_id_module, 
	_read_permission, 
	_write_permission, 
	_delete_permission, 
	_update_permission);

END
$$;
 �   DROP FUNCTION public.vud_insert_user_permission(_id_user integer, _id_module integer, _read_permission integer, _write_permission integer, _delete_permission integer, _update_permission integer);
       public       postgres    false    1    3            �            1255    25005 *   vud_log_insert(integer, character varying)    FUNCTION     �   CREATE FUNCTION public.vud_log_insert(_id_user integer, _description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO 
	public."log"(id_user, description,date_time)
	VALUES (_id_user,_description ,NOW());
END
$$;
 W   DROP FUNCTION public.vud_log_insert(_id_user integer, _description character varying);
       public       postgres    false    1    3            +           1255    25103    vud_modality_data(integer)    FUNCTION        CREATE FUNCTION public.vud_modality_data(_code_modality integer, OUT id integer, OUT description character varying, OUT legal_foundation character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN 
	RETURN QUERY select 
		a.id,
		a.description,
		c.legal_foundation

	from 
		public.cat_requirement as a 
	LEFT JOIN
		public.cat_modality_requirement as b
	on 
		a.id = b.id_requirement
	LEFT join 
		public.cat_modality as c
	on
		b.id_modality = c.code
	where c.code = _code_modality ;
END
$$;
 �   DROP FUNCTION public.vud_modality_data(_code_modality integer, OUT id integer, OUT description character varying, OUT legal_foundation character varying);
       public       postgres    false    1    3            �            1255    33589    vud_modality_days(integer)    FUNCTION     ^  CREATE FUNCTION public.vud_modality_days(_code integer, OUT days integer, OUT type_days character) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY SELECT
                a.days,
                a.type_days
            FROM
                cat_modality as a
            where
                a.code = _code;

END
$$;
 b   DROP FUNCTION public.vud_modality_days(_code integer, OUT days integer, OUT type_days character);
       public       postgres    false    3    1            "           1255    33549    vud_modality_record()    FUNCTION     x  CREATE FUNCTION public.vud_modality_record(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT id_transact integer, OUT code_transact integer, OUT description_transact character varying, OUT days integer, OUT type_days character) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY select 
	a.id,
	a.code,
	a.description,
	a.legal_foundation,
	b.id,
	b.code,
	b.description,
	a.days,
	a.type_days
from 
	cat_modality as a
LEFT JOIN 
	cat_transact as b
on
	a.id_transact = b.code
WHERE 
	a.status = 1
order by a.id asc;

END
$$;
 "  DROP FUNCTION public.vud_modality_record(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT id_transact integer, OUT code_transact integer, OUT description_transact character varying, OUT days integer, OUT type_days character);
       public       postgres    false    3    1                       1255    33607    vud_record_area_revisora()    FUNCTION       CREATE FUNCTION public.vud_record_area_revisora(OUT id integer, OUT description character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY SELECT a.id, a.description from public.cat_area as a order by a.id asc;

END
$$;
 b   DROP FUNCTION public.vud_record_area_revisora(OUT id integer, OUT description character varying);
       public       postgres    false    1    3            (           1255    33611    vud_record_report()    FUNCTION     �  CREATE FUNCTION public.vud_record_report(OUT folio character varying, OUT clave_tramite integer, OUT tramite character varying, OUT clave_modalidad integer, OUT modalidad character varying, OUT fecha_creacion text, OUT fecha_compromiso text) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY SELECT 
    a.folio,
    b.code as clave_tramite,
    b.description as tramite,
    c.code as clave_modalidad,
    c.description as modalidad,
    to_char(a.date_creation,'dd-mm-yyyy' ) as fecha_creacion,
    to_char(a.date_commitment,'dd-mm-yyyy') as fecha_compromiso
FROM
    public.request as a
left join
    public.cat_transact as b
on
    a.id_transact = b.code
left join 
    public.cat_modality as c
on 
    a.id_modality = c.code
left join 
    public.address as d
on
    d.id = a.id_address
left join
    public.tracing_turn as e
on 
    e.id_request = a.id
left join
    public.tracing_dictum as f
on 
    f.id_request = a.id
left join
    public.interested as g
on g.id = a.id_interested
left join
    public.cat_status h
on 
    h.id = a.id_status
left join
    public.vulnerable_group i
on 
    i.id = a.id_vulnerable_group 

WHERE a.flag = 1
order by a.id asc;

END
$$;
 �   DROP FUNCTION public.vud_record_report(OUT folio character varying, OUT clave_tramite integer, OUT tramite character varying, OUT clave_modalidad integer, OUT modalidad character varying, OUT fecha_creacion text, OUT fecha_compromiso text);
       public       postgres    false    3    1                       1255    33585 '   vud_record_request_requirement(integer)    FUNCTION     �  CREATE FUNCTION public.vud_record_request_requirement(_id_request integer, OUT id integer, OUT description character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN

	RETURN QUERY SELECT 
	c.id,
	c.description
	FROM
		public.request  as a
	LEFT JOIN
		public.request_requirement as b
	ON
		b.id_request = a.id
	LEFT JOIN
		public.cat_requirement as c
	ON 
		b.id_requirement = c.id
	WHERE a.id =_id_request
	order by c.id asc;

END
$$;
 }   DROP FUNCTION public.vud_record_request_requirement(_id_request integer, OUT id integer, OUT description character varying);
       public       postgres    false    1    3                       1255    33587 0   vud_record_request_requirement(integer, integer)    FUNCTION     :  CREATE FUNCTION public.vud_record_request_requirement(_code integer, _type_request integer, OUT id integer, OUT description character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN

IF (_type_request = 2) THEN

RETURN QUERY SELECT 
	c.id,
	c.description
FROM
	public.cat_modality as a
left join
	public.cat_modality_requirement as b
on
	b.id_modality = a.code
left join
	public.cat_requirement as c
on
	b.id_requirement = c.id
	where a.code = _code
	order by c.id asc;

ELSE
	RETURN QUERY SELECT 
		c.id,
		c.description
	FROM 
		public.cat_transact as a
	left join
		public.cat_transact_requirement as b
	on
		a.code = b.id_transact
	left join
		public.cat_requirement as c
	on
		b.id_requirement = c.id
	where a.code = _code
	order by c.id asc;

END IF;

END
$$;
 �   DROP FUNCTION public.vud_record_request_requirement(_code integer, _type_request integer, OUT id integer, OUT description character varying);
       public       postgres    false    3    1            �            1259    16726    cat_requirement    TABLE     �   CREATE TABLE public.cat_requirement (
    id integer NOT NULL,
    description character varying(5000) NOT NULL,
    status integer
);
 #   DROP TABLE public.cat_requirement;
       public         postgres    false    3                       1255    16734    vud_record_requirement()    FUNCTION     �   CREATE FUNCTION public.vud_record_requirement() RETURNS SETOF public.cat_requirement
    LANGUAGE plpgsql
    AS $$
BEGIN 
	RETURN QUERY select * from public.cat_requirement where status = 1 order by id asc;
END
$$;
 /   DROP FUNCTION public.vud_record_requirement();
       public       postgres    false    205    3    1            '           1255    33535    vud_record_transact()    FUNCTION     �  CREATE FUNCTION public.vud_record_transact(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT total_modality bigint, OUT id_transact integer, OUT days integer, OUT type_days character) RETURNS SETOF record
    LANGUAGE sql
    AS $$

	SELECT
		a.id,
		a.code,
		a.description,
		a.legal_foundation,
		count(b.id_transact) as total_modality,
		b.id_transact,
		a.days,	
		a.type_days
	FROM
		cat_transact  as a
	left JOIN 
		cat_modality as b
	on  a.code = b.id_transact
	WHERE
		a.status = 1
	GROUP BY b.id_transact,a.id,a.code,a.description,a.legal_foundation,a.days,a.type_days
	order by a.id asc;

$$;
 �   DROP FUNCTION public.vud_record_transact(OUT id integer, OUT code integer, OUT description character varying, OUT legal_foundation character varying, OUT total_modality bigint, OUT id_transact integer, OUT days integer, OUT type_days character);
       public       postgres    false    3            �            1255    33608    vud_record_vulnerable_group()    FUNCTION       CREATE FUNCTION public.vud_record_vulnerable_group(OUT id integer, OUT description character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY SELECT a.id, a.description from public.vulnerable_group as a order by a.id asc;

END
$$;
 e   DROP FUNCTION public.vud_record_vulnerable_group(OUT id integer, OUT description character varying);
       public       postgres    false    1    3                       1255    16752 3   vud_relation_modality_requirement(integer, integer)    FUNCTION       CREATE FUNCTION public.vud_relation_modality_requirement(_id_modality integer, _id_requirement integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

INSERT INTO public.cat_modality_requirement(id_modality,id_requirement)
VALUES (_id_modality, _id_requirement);

END
$$;
 g   DROP FUNCTION public.vud_relation_modality_requirement(_id_modality integer, _id_requirement integer);
       public       postgres    false    1    3            ,           1255    16765 6   vud_relation_transaction_requirement(integer, integer)    FUNCTION       CREATE FUNCTION public.vud_relation_transaction_requirement(_id_transact integer, _id_requirement integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

INSERT INTO public.cat_transact_requirement(
	id_transact, 
	id_requirement)
VALUES (_id_transact,_id_requirement);

END
$$;
 j   DROP FUNCTION public.vud_relation_transaction_requirement(_id_transact integer, _id_requirement integer);
       public       postgres    false    1    3            *           1255    33561 �   vud_request_address(character varying, character varying, character varying, character varying, character varying, character varying)    FUNCTION     x  CREATE FUNCTION public.vud_request_address(_postal_code character varying, _colony character varying, _ext_number character varying, _int_number character varying, _street character varying, _delegation character varying) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
DECLARE _id_request_address integer;
BEGIN

INSERT INTO public.address(
		postal_code, 
		colony, 
		ext_number, 
		int_number, 
		street,
		delegation)
	
VALUES (_postal_code,
		_colony,
		_ext_number,
		_int_number,
		_street,
		_delegation)

RETURNING ID into _id_request_address;
RETURN QUERY SELECT _id_request_address;

END
$$;
 �   DROP FUNCTION public.vud_request_address(_postal_code character varying, _colony character varying, _ext_number character varying, _int_number character varying, _street character varying, _delegation character varying);
       public       postgres    false    1    3                       1255    33581 �   vud_request_data(integer, character varying, character varying, integer, integer, integer, integer, integer, character varying, character varying, integer, integer, date, integer)    FUNCTION     �  CREATE FUNCTION public.vud_request_data(_id_user integer, _folio character varying, _observations character varying, _id_status integer, _id_address integer, _id_interested integer, _id_transact integer, _id_vulnerable_group integer, _request_turn character varying, _type_work character varying, _means_request integer, _delivery integer, _date_commitment date, _id_modality integer) RETURNS SETOF character varying
    LANGUAGE plpgsql
    AS $$
DECLARE id_request_created integer;
BEGIN

INSERT INTO public.request( id_user,
							folio,
							date_creation, 
							observations, 
							id_status, 
							id_address, 
							id_interested, 
							id_transact, 
							id_vulnerable_group, 
							request_turn, 
							type_work, 
							means_request, 
							delivery,
							date_commitment,
							flag,
							id_modality)
VALUES (_id_user,
		_folio, 
		NOW(), 
		_observations, 
		_id_status, 
		_id_address, 
		_id_interested, 
		_id_transact, 
		_id_vulnerable_group, 
		_request_turn, 
		_type_work, 
		_means_request, 
		_delivery,
		_date_commitment,
		1,
		_id_modality)
RETURNING ID into id_request_created;

	RETURN QUERY select folio from public.request where id = id_request_created;

END
$$;
 �  DROP FUNCTION public.vud_request_data(_id_user integer, _folio character varying, _observations character varying, _id_status integer, _id_address integer, _id_interested integer, _id_transact integer, _id_vulnerable_group integer, _request_turn character varying, _type_work character varying, _means_request integer, _delivery integer, _date_commitment date, _id_modality integer);
       public       postgres    false    3    1            �            1255    33328 k   vud_request_interested(character varying, character varying, character varying, character varying, integer)    FUNCTION     !  CREATE FUNCTION public.vud_request_interested(_name character varying, _last_name character varying, _e_mail character varying, _phone character varying, _type_person integer) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
DECLARE id_request_interested integer;
BEGIN
INSERT INTO public.interested(
	name, 
	last_name, 
	e_mail, 
	phone,
	type_person)
	VALUES (_name, 
			_last_name, 
			_e_mail, 
			_phone,
			_type_person)

RETURNING ID into id_request_interested;
RETURN QUERY SELECT id_request_interested;

END
$$;
 �   DROP FUNCTION public.vud_request_interested(_name character varying, _last_name character varying, _e_mail character varying, _phone character varying, _type_person integer);
       public       postgres    false    3    1            -           1255    33618    vud_request_record()    FUNCTION     �  CREATE FUNCTION public.vud_request_record(OUT id integer, OUT folio character varying, OUT id_user integer, OUT id_status integer, OUT description_status character varying, OUT date_creation text, OUT date_commitment text, OUT code integer, OUT description character varying, OUT modality_desc character varying, OUT id_turn integer, OUT id_dictum integer, OUT last_status integer, OUT settlement_number character varying, OUT id_address integer, OUT id_interested integer, OUT citizen_dictum date, OUT folio_dictum character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY SELECT 
	a.id,
	a.folio,
	a.id_user,
	d.id as id_status,
	d.description as description_status,
	to_char(a.date_creation,'dd-mm-yyyy' ),
	to_char(a.date_commitment,'dd-mm-yyyy' ),
	b.code,
	b.description,
	c.description as modality_desc,
	e.id as id_turn,
	f.id as id_dictum,
	a.last_status,
	a.settlement_number,
	a.id_address,
	a.id_interested,
	f.citizen_dictum,
	f.folio_dictum

FROM 
	public.request as a
left join 
	public.cat_transact as b
on
	a.id_transact = b.code
left join 
	public.cat_modality as c
on 
	a.id_modality = c.code

left join 
	public.cat_status as d
on
	d.id = a.id_status
left join 
	public.tracing_turn as e
on
	e.id_request = a.id
left join
	public.tracing_dictum as f
on
	f.id_request = a.id
WHERE
	flag = 1
order by id desc;

END
$$;
   DROP FUNCTION public.vud_request_record(OUT id integer, OUT folio character varying, OUT id_user integer, OUT id_status integer, OUT description_status character varying, OUT date_creation text, OUT date_commitment text, OUT code integer, OUT description character varying, OUT modality_desc character varying, OUT id_turn integer, OUT id_dictum integer, OUT last_status integer, OUT settlement_number character varying, OUT id_address integer, OUT id_interested integer, OUT citizen_dictum date, OUT folio_dictum character varying);
       public       postgres    false    3    1            �            1259    16413    role    TABLE     g   CREATE TABLE public.role (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);
    DROP TABLE public.role;
       public         postgres    false    3            	           1255    25015 
   vud_role()    FUNCTION     �   CREATE FUNCTION public.vud_role() RETURNS SETOF public.role
    LANGUAGE sql
    AS $$ 
		SELECT
			*
		FROM
			public. "role";
	$$;
 !   DROP FUNCTION public.vud_role();
       public       postgres    false    3    199            �            1259    25079 
   cat_status    TABLE     m   CREATE TABLE public.cat_status (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);
    DROP TABLE public.cat_status;
       public         postgres    false    3            2           1255    33428    vud_status_list()    FUNCTION     �   CREATE FUNCTION public.vud_status_list() RETURNS SETOF public.cat_status
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY select 
				* 
			from 
				public.cat_status;
END
$$;
 (   DROP FUNCTION public.vud_status_list();
       public       postgres    false    1    3    217            .           1255    33347    vud_transact_days(integer)    FUNCTION     "  CREATE FUNCTION public.vud_transact_days(_code integer, OUT days integer, OUT type_days character) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY SELECT 
				a.days,
				a.type_days
			FROM 
				cat_transact as a
			where
				a.code = _code;
				
END
$$;
 b   DROP FUNCTION public.vud_transact_days(_code integer, OUT days integer, OUT type_days character);
       public       postgres    false    1    3                       1255    33332    vud_transact_record()    FUNCTION     *  CREATE FUNCTION public.vud_transact_record(OUT code integer, OUT description character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY SELECT a.code,
						a.description
				FROM public.cat_transact as a 
				WHERE status = 1 
				order by id desc;
END
$$;
 _   DROP FUNCTION public.vud_transact_record(OUT code integer, OUT description character varying);
       public       postgres    false    3    1            �            1259    16694    cat_modality    TABLE     &  CREATE TABLE public.cat_modality (
    id integer NOT NULL,
    code integer NOT NULL,
    description character varying(2000) NOT NULL,
    legal_foundation character varying(3000),
    id_transact integer NOT NULL,
    status integer NOT NULL,
    days integer,
    type_days character(1)
);
     DROP TABLE public.cat_modality;
       public         postgres    false    3            1           1255    16821 %   vud_transact_record_modality(integer)    FUNCTION     �   CREATE FUNCTION public.vud_transact_record_modality(_code integer) RETURNS SETOF public.cat_modality
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY SELECT * FROM public.cat_modality where id_transact = _code order by id asc;
END
$$;
 B   DROP FUNCTION public.vud_transact_record_modality(_code integer);
       public       postgres    false    1    203    3            �            1255    25101 !   vud_transact_requeriment(integer)    FUNCTION     �  CREATE FUNCTION public.vud_transact_requeriment(_id_code integer, OUT id integer, OUT description character varying, OUT legal_foundation character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
	
	RETURN QUERY select 
		a.id,
		a.description ,
		c.legal_foundation
	from 
		public.cat_requirement as a
	inner join public.cat_transact_requirement as b
	on a.id = b.id_requirement
	inner join public.cat_transact as c
	on c.code = b.id_transact
	where c.code = _id_code;

END
$$;
 �   DROP FUNCTION public.vud_transact_requeriment(_id_code integer, OUT id integer, OUT description character varying, OUT legal_foundation character varying);
       public       postgres    false    1    3            �            1255    33619 (   vud_update_citizen_dictum(integer, date)    FUNCTION       CREATE FUNCTION public.vud_update_citizen_dictum(_id_request integer, _date_citizen_dictum date) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE tracing_dictum
SET
	citizen_dictum = _date_citizen_dictum
WHERE
	_id_request = id_request;
END
$$;
 `   DROP FUNCTION public.vud_update_citizen_dictum(_id_request integer, _date_citizen_dictum date);
       public       postgres    false    1    3            /           1255    33481 !   vud_update_correct(integer, date)    FUNCTION     �   CREATE FUNCTION public.vud_update_correct(_id_request integer, _correct_date date) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
UPDATE public.request
	
SET 	
	correct_date = _correct_date,
	id_status = 9
WHERE 
	id = _id_request;
END
$$;
 R   DROP FUNCTION public.vud_update_correct(_id_request integer, _correct_date date);
       public       postgres    false    1    3            �            1255    33592 �   vud_update_data_address(integer, character varying, character varying, character varying, character varying, character varying, character varying)    FUNCTION       CREATE FUNCTION public.vud_update_data_address(_id_address integer, _postal_code character varying, _colony character varying, _ext_number character varying, _int_number character varying, _street character varying, _delegation character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

	UPDATE public.address
	SET postal_code=_postal_code, 
		colony=_colony, 
		ext_number=_ext_number, 
		int_number=_int_number, 
		street=_street, 
		delegation=_delegation
    WHERE id = _id_address;

END
$$;
 �   DROP FUNCTION public.vud_update_data_address(_id_address integer, _postal_code character varying, _colony character varying, _ext_number character varying, _int_number character varying, _street character varying, _delegation character varying);
       public       postgres    false    1    3                       1255    33599 x   vud_update_data_interested(integer, character varying, character varying, character varying, character varying, integer)    FUNCTION     �  CREATE FUNCTION public.vud_update_data_interested(_id_interested integer, _name character varying, _last_name character varying, _e_mail character varying, _phone character varying, _type_person integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

	UPDATE 
		public.interested
    SET name=_name, 
    	last_name=_last_name, 
    	e_mail=_e_mail, 
    	phone=_phone, 
    	type_person=_type_person
    WHERE 
    	id =_id_interested;

END
$$;
 �   DROP FUNCTION public.vud_update_data_interested(_id_interested integer, _name character varying, _last_name character varying, _e_mail character varying, _phone character varying, _type_person integer);
       public       postgres    false    3    1            &           1255    33600 �   vud_update_data_request(integer, character varying, integer, integer, integer, character varying, character varying, integer, integer, integer)    FUNCTION     �  CREATE FUNCTION public.vud_update_data_request(_id_request integer, _observations character varying, _id_status integer, _id_transact integer, _id_vulnerable_group integer, _request_turn character varying, _type_work character varying, _means_request integer, _delivery integer, _id_modality integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	UPDATE public.request
	SET 
	observations=_observations, 
	id_status=_id_status, 
	id_transact=_id_transact, 
	id_vulnerable_group=_id_vulnerable_group, 
	request_turn=_request_turn, 
	type_work=_type_work, 
	means_request=_means_request, 
	delivery=_delivery, 
	id_modality=_id_modality 	
WHERE id = _id_request;

DELETE FROM public.request_requirement where id_request = _id_request;

END
$$;
 ,  DROP FUNCTION public.vud_update_data_request(_id_request integer, _observations character varying, _id_status integer, _id_transact integer, _id_vulnerable_group integer, _request_turn character varying, _type_work character varying, _means_request integer, _delivery integer, _id_modality integer);
       public       postgres    false    3    1                       1255    33547 V   vud_update_modality(integer, character varying, character varying, integer, character)    FUNCTION     z  CREATE FUNCTION public.vud_update_modality(_id integer, _description character varying, _legal_foundation character varying, _days integer, _type_days character) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE public.cat_modality
	SET   description=_description, legal_foundation=_legal_foundation, days=_days, type_days = _type_days
	WHERE id = _id;

END
$$;
 �   DROP FUNCTION public.vud_update_modality(_id integer, _description character varying, _legal_foundation character varying, _days integer, _type_days character);
       public       postgres    false    1    3            %           1255    33480 *   vud_update_prevention(integer, date, date)    FUNCTION     L  CREATE FUNCTION public.vud_update_prevention(_id_request integer, _prevent_date date, _citizen_prevent_date date) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
UPDATE public.request
	
SET 
	prevent_date = _prevent_date, 
	citizen_prevent_date = _citizen_prevent_date,
	id_status = 7
WHERE 
	id = _id_request;
END
$$;
 q   DROP FUNCTION public.vud_update_prevention(_id_request integer, _prevent_date date, _citizen_prevent_date date);
       public       postgres    false    1    3                       1255    33590     vud_update_request_info(integer)    FUNCTION     �  CREATE FUNCTION public.vud_update_request_info(_id_request integer, OUT id integer, OUT observations character varying, OUT id_status integer, OUT id_transact integer, OUT id_vulnerable_group smallint, OUT means_request smallint, OUT delivery smallint, OUT id_modality integer, OUT request_turn character varying, OUT type_work character varying, OUT postal_code character varying, OUT colony character varying, OUT ext_number character varying, OUT int_number character varying, OUT street character varying, OUT delegation character varying, OUT name character varying, OUT last_name character varying, OUT e_mail character varying, OUT phone character varying, OUT type_person smallint, OUT description_modality character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
	
RETURN QUERY SELECT 
		a.id,
		a.observations,
		a.id_status,
		a.id_transact,
		a.id_vulnerable_group,
		a.means_request,
		a.delivery,
		a.id_modality,
		a.request_turn,
		a.type_work,
		b.postal_code,
		b.colony,
		b.ext_number,
		b.int_number,
		b.street,
		b.delegation,
		c.name,
		c.last_name,
		c.e_mail,
		c.phone,
		c.type_person,
		d.description as description_modality
FROM
	public.request AS a
LEFT JOIN
	public.address AS b
ON
	a.id_address = b.id
LEFT JOIN
	public.interested AS c
ON
	a.id_interested = c.id
LEFT JOIN
	public.cat_modality as d
on	
	d.code = a.id_modality
WHERE a.id = _id_request;

END
$$;
 �  DROP FUNCTION public.vud_update_request_info(_id_request integer, OUT id integer, OUT observations character varying, OUT id_status integer, OUT id_transact integer, OUT id_vulnerable_group smallint, OUT means_request smallint, OUT delivery smallint, OUT id_modality integer, OUT request_turn character varying, OUT type_work character varying, OUT postal_code character varying, OUT colony character varying, OUT ext_number character varying, OUT int_number character varying, OUT street character varying, OUT delegation character varying, OUT name character varying, OUT last_name character varying, OUT e_mail character varying, OUT phone character varying, OUT type_person smallint, OUT description_modality character varying);
       public       postgres    false    1    3                       1255    33601 0   vud_update_request_requeriment(integer, integer)    FUNCTION       CREATE FUNCTION public.vud_update_request_requeriment(_id_request integer, _id_requeriment integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

INSERT INTO public.request_requirement(
	id_request, 
	id_requirement)
VALUES (_id_request,_id_requeriment);

END
$$;
 c   DROP FUNCTION public.vud_update_request_requeriment(_id_request integer, _id_requeriment integer);
       public       postgres    false    3    1                       1255    16736 2   vud_update_requirement(integer, character varying)    FUNCTION     �   CREATE FUNCTION public.vud_update_requirement(_id integer, _description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

	UPDATE public.cat_requirement
	SET  description=_description
	WHERE id = _id;
END
$$;
 Z   DROP FUNCTION public.vud_update_requirement(_id integer, _description character varying);
       public       postgres    false    1    3                       1255    33509 G   vud_update_status_request(integer, integer, integer, character varying)    FUNCTION     ~  CREATE FUNCTION public.vud_update_status_request(_id_request integer, _status integer, _last_status integer, _cancelation_description character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE 
	public.request
SET 
	id_status = _status,
	last_status = _last_status,
	cancelation_description = _cancelation_description
WHERE 
	id = _id_request;
END
$$;
 �   DROP FUNCTION public.vud_update_status_request(_id_request integer, _status integer, _last_status integer, _cancelation_description character varying);
       public       postgres    false    3    1                       1255    33536 V   vud_update_transact(integer, character varying, character varying, integer, character)    FUNCTION     y  CREATE FUNCTION public.vud_update_transact(_id integer, _description character varying, _legal_foundation character varying, _days integer, _type_days character) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
	UPDATE public.cat_transact
	SET  description=_description, legal_foundation=_legal_foundation, days = _days, type_days = _type_days
	WHERE id = _id;
END
$$;
 �   DROP FUNCTION public.vud_update_transact(_id integer, _description character varying, _legal_foundation character varying, _days integer, _type_days character);
       public       postgres    false    3    1                       1255    25020 l   vud_update_user(integer, character varying, integer, character varying, integer, integer, character varying)    FUNCTION       CREATE FUNCTION public.vud_update_user(_id_user integer, _user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$ 
	BEGIN
	IF (_password = '') THEN
		update 
		public."users"
	SET		
		user_name = _user_name,
		employee_number = _employee_number,
		description = _description,
		rol = _rol, 
		user_register = _user_register
	WHERE 
		id = _id_user;
	ELSE 
		update 
		public."users"
	SET		
		user_name = _user_name,
		employee_number = _employee_number,
		description = _description,
		rol = _rol, 
		user_register = _user_register,
		password = _password
	WHERE 
		id = _id_user;
	END IF;
	
	RETURN true;
	END
	$$;
 �   DROP FUNCTION public.vud_update_user(_id_user integer, _user_name character varying, _employee_number integer, _description character varying, _rol integer, _user_register integer, _password character varying);
       public       postgres    false    3    1                       1255    25122    vud_users_permisions(integer)    FUNCTION     z  CREATE FUNCTION public.vud_users_permisions(_id_user integer, OUT id integer, OUT name character varying, OUT id_module integer, OUT id_user integer, OUT read_permission smallint, OUT write_permission smallint, OUT delete_permission smallint, OUT update_permission smallint) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY SELECT 
	a.id,
	a.name,
	b.id_module,
	b.id_user,
	b.read_permission,
	b.write_permission,
	b.delete_permission,
	b.update_permission
FROM
	PUBLIC.modules AS A 
	LEFT JOIN PUBLIC.permission AS b ON a.id = b.id_module

WHERE
	b.id_user = _id_user
order by a.id;
END
$$;
   DROP FUNCTION public.vud_users_permisions(_id_user integer, OUT id integer, OUT name character varying, OUT id_module integer, OUT id_user integer, OUT read_permission smallint, OUT write_permission smallint, OUT delete_permission smallint, OUT update_permission smallint);
       public       postgres    false    1    3            �            1255    33472    vud_users_record()    FUNCTION       CREATE FUNCTION public.vud_users_record(OUT id integer, OUT user_name character varying, OUT employee_number integer, OUT user_register integer, OUT rol character varying, OUT description character varying, OUT id_rol integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$ 
		SELECT
			a.id,
			a.user_name,
			a.employee_number,
			a.user_register,
			b.description,
			a.description,
			b.id
		FROM
			public. "users" as a
		INNER JOIN public. "role" as b
		ON a.rol = b.id
		where a.status = 1 order by a.id asc;
	$$;
 �   DROP FUNCTION public.vud_users_record(OUT id integer, OUT user_name character varying, OUT employee_number integer, OUT user_register integer, OUT rol character varying, OUT description character varying, OUT id_rol integer);
       public       postgres    false    3            �            1255    33563    vud_view_data_request(integer)    FUNCTION     �	  CREATE FUNCTION public.vud_view_data_request(_id_request integer, OUT id integer, OUT folio character varying, OUT id_user integer, OUT user_name character varying, OUT means_request smallint, OUT prevent_date text, OUT citizen_prevent_date text, OUT correct_date text, OUT observation character varying, OUT request_turn character varying, OUT type_work character varying, OUT name character varying, OUT last_name character varying, OUT phone character varying, OUT delegation character varying, OUT colony character varying, OUT postal_code character varying, OUT street character varying, OUT ext_number character varying, OUT int_number character varying, OUT id_status integer, OUT description_status character varying, OUT date_creation text, OUT date_commitment text, OUT code integer, OUT description character varying, OUT modality_desc character varying, OUT id_turn integer, OUT id_dictum integer, OUT folio_dictum character varying, OUT date_receives_dictum text, OUT answer_dictum integer, OUT citizen_dictum text) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY SELECT 
	a.id,
	a.folio,
	a.id_user,
	i.user_name,
	a.means_request,
	to_char(a.prevent_date,'yyyy-mm-dd'),
	to_char(a.citizen_prevent_date,'yyyy-mm-dd'),
	to_char(a.correct_date,'yyyy-mm-dd'),
	a.observations,
	a.request_turn,
	a.type_work,
	g.name,
	g.last_name,
	g.phone,
	h.delegation,
	h.colony,
	h.postal_code,
	h.street,
	h.ext_number,
	h.int_number,
	d.id as id_status,
	d.description as description_status,
	to_char(a.date_creation,'yyyy-mm-dd' ),
	to_char(a.date_commitment,'yyyy-mm-dd' ),
	b.code,
	b.description,
	c.description as modality_desc,
	e.id as id_turn,
	f.id as id_dictum,
	f.folio_dictum,
	to_char(f.date_receives_dictum,'yyyy-mm-dd'),
	f.answer_dictum,
	to_char(f.citizen_dictum,'yyyy-mm-dd')
FROM 
	public.request as a
left join 
	public.cat_transact as b
on
	a.id_transact = b.code
left join 
	public.cat_modality as c
on 
	a.id_modality = c.code

left join 
	public.cat_status as d
on
	d.id = a.id_status
left join 
	public.tracing_turn as e
on
	e.id_request = a.id
left join
	public.tracing_dictum as f
on
	f.id_request = a.id
left join
	public.interested as g
on 
	a.id_interested = g.id
left join
	public.address as h
on
	a.id_address = h.id
left join 
	public.users as i
on 
	i.id = a.id_user
WHERE
	flag = 1 
AND a.id = _id_request
order by id asc;

END
$$;
   DROP FUNCTION public.vud_view_data_request(_id_request integer, OUT id integer, OUT folio character varying, OUT id_user integer, OUT user_name character varying, OUT means_request smallint, OUT prevent_date text, OUT citizen_prevent_date text, OUT correct_date text, OUT observation character varying, OUT request_turn character varying, OUT type_work character varying, OUT name character varying, OUT last_name character varying, OUT phone character varying, OUT delegation character varying, OUT colony character varying, OUT postal_code character varying, OUT street character varying, OUT ext_number character varying, OUT int_number character varying, OUT id_status integer, OUT description_status character varying, OUT date_creation text, OUT date_commitment text, OUT code integer, OUT description character varying, OUT modality_desc character varying, OUT id_turn integer, OUT id_dictum integer, OUT folio_dictum character varying, OUT date_receives_dictum text, OUT answer_dictum integer, OUT citizen_dictum text);
       public       postgres    false    1    3                        1255    33616 "   vud_year_report(character varying)    FUNCTION     4  CREATE FUNCTION public.vud_year_report(_year character varying, OUT clave_tramite integer, OUT descripcion_tramite character varying, OUT clave_modalidad integer, OUT descripcion_modalidad character varying, OUT enero bigint, OUT febrero bigint, OUT marzo bigint, OUT abril bigint, OUT mayo bigint, OUT junio bigint, OUT julio bigint, OUT agosto bigint, OUT septiembre bigint, OUT octubre bigint, OUT noviembre bigint, OUT diciembre bigint, OUT total_de_tramites bigint, OUT total_de_modalidades bigint) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY select  
	a.code AS CLAVE_TRAMITE,
	a.description AS DESCRIPCION_TRAMITE,
	c.code AS CLAVE_MODALIDAD,
	c.description AS descripcion_modalidad,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '01' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Enero,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '02' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Febrero,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '03' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Marzo,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '04' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Abril,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '05' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Mayo,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '06' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Junio,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '07' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Julio,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '08' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Agosto,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '09' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Septiembre,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '10' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Octubre,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '11' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as noviembre,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '12' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as diciembre,
	count(CASE TO_CHAR(b.date_creation, 'YYYY') WHEN _year THEN  b.id_transact END) AS TOTAL_DE_TRAMITES,
	count(CASE TO_CHAR(b.date_creation, 'YYYY') WHEN _year THEN  c.code END ) AS TOTAL_DE_MODALIDADES
from 
	public.cat_transact as a 

left join 
	public.request as b
on
	a.code = b.id_transact

left join 
	public.cat_modality as c
on
	c.id_transact = a.code 
group by 
	a.code,
	a.description,
	c.code,
	c.description
order by a.code asc;
END
$$;
 �  DROP FUNCTION public.vud_year_report(_year character varying, OUT clave_tramite integer, OUT descripcion_tramite character varying, OUT clave_modalidad integer, OUT descripcion_modalidad character varying, OUT enero bigint, OUT febrero bigint, OUT marzo bigint, OUT abril bigint, OUT mayo bigint, OUT junio bigint, OUT julio bigint, OUT agosto bigint, OUT septiembre bigint, OUT octubre bigint, OUT noviembre bigint, OUT diciembre bigint, OUT total_de_tramites bigint, OUT total_de_modalidades bigint);
       public       postgres    false    1    3            �            1259    16411    ROLE_id_seq    SEQUENCE     �   CREATE SEQUENCE public."ROLE_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public."ROLE_id_seq";
       public       postgres    false    199    3            
           0    0    ROLE_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public."ROLE_id_seq" OWNED BY public.role.id;
            public       postgres    false    198            �            1259    16400    USERS_Id_seq    SEQUENCE     �   CREATE SEQUENCE public."USERS_Id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public."USERS_Id_seq";
       public       postgres    false    3    197                       0    0    USERS_Id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public."USERS_Id_seq" OWNED BY public.users.id;
            public       postgres    false    196            �            1259    25034    address    TABLE     ;  CREATE TABLE public.address (
    id integer NOT NULL,
    postal_code character varying(7) NOT NULL,
    colony character varying(50) NOT NULL,
    ext_number character varying(50),
    int_number character varying(50),
    street character varying(255) NOT NULL,
    delegation character varying(255) NOT NULL
);
    DROP TABLE public.address;
       public         postgres    false    3            �            1259    25032    address_id_seq    SEQUENCE     �   CREATE SEQUENCE public.address_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.address_id_seq;
       public       postgres    false    209    3                       0    0    address_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.address_id_seq OWNED BY public.address.id;
            public       postgres    false    208            �            1259    33386    cat_area_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_area_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.cat_area_id_seq;
       public       postgres    false    225    3                       0    0    cat_area_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.cat_area_id_seq OWNED BY public.cat_area.id;
            public       postgres    false    224            �            1259    16692    cat_modality_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_modality_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.cat_modality_id_seq;
       public       postgres    false    203    3                       0    0    cat_modality_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.cat_modality_id_seq OWNED BY public.cat_modality.id;
            public       postgres    false    202            �            1259    16746    cat_modality_requirement    TABLE     f   CREATE TABLE public.cat_modality_requirement (
    id_modality integer,
    id_requirement integer
);
 ,   DROP TABLE public.cat_modality_requirement;
       public         postgres    false    3            �            1259    16724    cat_requeriment_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_requeriment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.cat_requeriment_id_seq;
       public       postgres    false    3    205                       0    0    cat_requeriment_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.cat_requeriment_id_seq OWNED BY public.cat_requirement.id;
            public       postgres    false    204            �            1259    25077    cat_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.cat_status_id_seq;
       public       postgres    false    217    3                       0    0    cat_status_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.cat_status_id_seq OWNED BY public.cat_status.id;
            public       postgres    false    216            �            1259    16674    cat_transact    TABLE       CREATE TABLE public.cat_transact (
    id integer NOT NULL,
    code integer NOT NULL,
    description character varying(2000) NOT NULL,
    legal_foundation character varying(3000),
    status integer NOT NULL,
    days integer,
    type_days character(1)
);
     DROP TABLE public.cat_transact;
       public         postgres    false    3            �            1259    16672    cat_transact_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cat_transact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.cat_transact_id_seq;
       public       postgres    false    201    3                       0    0    cat_transact_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.cat_transact_id_seq OWNED BY public.cat_transact.id;
            public       postgres    false    200            �            1259    16757    cat_transact_requirement    TABLE     x   CREATE TABLE public.cat_transact_requirement (
    id_transact integer NOT NULL,
    id_requirement integer NOT NULL
);
 ,   DROP TABLE public.cat_transact_requirement;
       public         postgres    false    3            �            1259    25053 
   interested    TABLE     �   CREATE TABLE public.interested (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    last_name character varying(100),
    e_mail character varying(100),
    phone character varying(15) NOT NULL,
    type_person smallint NOT NULL
);
    DROP TABLE public.interested;
       public         postgres    false    3            �            1259    25051    interested_id_seq    SEQUENCE     �   CREATE SEQUENCE public.interested_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.interested_id_seq;
       public       postgres    false    3    213                       0    0    interested_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.interested_id_seq OWNED BY public.interested.id;
            public       postgres    false    212            �            1259    33466    log    TABLE     �   CREATE TABLE public.log (
    id integer NOT NULL,
    id_user integer NOT NULL,
    description character varying(255) NOT NULL,
    date_time timestamp with time zone NOT NULL
);
    DROP TABLE public.log;
       public         postgres    false    3            �            1259    33464 
   log_id_seq    SEQUENCE     �   CREATE SEQUENCE public.log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 !   DROP SEQUENCE public.log_id_seq;
       public       postgres    false    3    231                       0    0 
   log_id_seq    SEQUENCE OWNED BY     9   ALTER SEQUENCE public.log_id_seq OWNED BY public.log.id;
            public       postgres    false    230            �            1259    25095    modules    TABLE     �   CREATE TABLE public.modules (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    status smallint NOT NULL
);
    DROP TABLE public.modules;
       public         postgres    false    3            �            1259    25093    modules_id_seq    SEQUENCE     �   CREATE SEQUENCE public.modules_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.modules_id_seq;
       public       postgres    false    221    3                       0    0    modules_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.modules_id_seq OWNED BY public.modules.id;
            public       postgres    false    220            �            1259    25087 
   permission    TABLE     �   CREATE TABLE public.permission (
    id integer NOT NULL,
    id_user integer NOT NULL,
    id_module integer NOT NULL,
    read_permission smallint,
    write_permission smallint,
    delete_permission smallint,
    update_permission smallint
);
    DROP TABLE public.permission;
       public         postgres    false    3            �            1259    25085    permission_id_seq    SEQUENCE     �   CREATE SEQUENCE public.permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.permission_id_seq;
       public       postgres    false    3    219                       0    0    permission_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.permission_id_seq OWNED BY public.permission.id;
            public       postgres    false    218            �            1259    25042    request    TABLE     (  CREATE TABLE public.request (
    id integer NOT NULL,
    folio character varying(50),
    id_user integer NOT NULL,
    date_creation date NOT NULL,
    observations character varying(2000),
    id_status integer NOT NULL,
    id_address integer NOT NULL,
    id_interested integer NOT NULL,
    id_transact integer NOT NULL,
    id_vulnerable_group smallint NOT NULL,
    request_turn character varying(100),
    type_work character varying(100),
    means_request smallint NOT NULL,
    delivery smallint,
    date_commitment date,
    flag smallint,
    id_modality integer,
    prevent_date date,
    citizen_prevent_date date,
    correct_date date,
    cancelation_description character varying(2000),
    last_status integer,
    settlement_date date,
    settlement_number character varying(50)
);
    DROP TABLE public.request;
       public         postgres    false    3            �            1259    25064    request_audit    TABLE     �   CREATE TABLE public.request_audit (
    id integer NOT NULL,
    id_user integer NOT NULL,
    accion character varying NOT NULL,
    id_request integer NOT NULL
);
 !   DROP TABLE public.request_audit;
       public         postgres    false    3            �            1259    25040    request_id_seq    SEQUENCE     �   CREATE SEQUENCE public.request_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.request_id_seq;
       public       postgres    false    3    211                       0    0    request_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.request_id_seq OWNED BY public.request.id;
            public       postgres    false    210            �            1259    33567    request_requirement    TABLE     `   CREATE TABLE public.request_requirement (
    id_request integer,
    id_requirement integer
);
 '   DROP TABLE public.request_requirement;
       public         postgres    false    3            �            1259    33453    tracing_dictum    TABLE     �   CREATE TABLE public.tracing_dictum (
    id integer NOT NULL,
    date_receives_dictum date,
    folio_dictum character varying(50),
    answer_dictum integer,
    citizen_dictum date,
    id_request integer NOT NULL
);
 "   DROP TABLE public.tracing_dictum;
       public         postgres    false    3            �            1259    33451    tracing_dictum_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tracing_dictum_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.tracing_dictum_id_seq;
       public       postgres    false    3    229                       0    0    tracing_dictum_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.tracing_dictum_id_seq OWNED BY public.tracing_dictum.id;
            public       postgres    false    228            �            1259    33431    tracing_turn    TABLE     �   CREATE TABLE public.tracing_turn (
    id integer NOT NULL,
    id_request integer NOT NULL,
    date_turn date NOT NULL,
    review_area integer NOT NULL,
    observations character varying(2000)
);
     DROP TABLE public.tracing_turn;
       public         postgres    false    3            �            1259    33429    tracing_turn_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tracing_turn_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.tracing_turn_id_seq;
       public       postgres    false    227    3                       0    0    tracing_turn_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.tracing_turn_id_seq OWNED BY public.tracing_turn.id;
            public       postgres    false    226            �            1259    25059 
   user_audit    TABLE     �   CREATE TABLE public.user_audit (
    id integer NOT NULL,
    id_user integer NOT NULL,
    accion character varying(255) NOT NULL,
    id_user_modification integer NOT NULL
);
    DROP TABLE public.user_audit;
       public         postgres    false    3            �            1259    25104    vulnerable_group_id_seq    SEQUENCE     �   CREATE SEQUENCE public.vulnerable_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.vulnerable_group_id_seq;
       public       postgres    false    223    3                       0    0    vulnerable_group_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.vulnerable_group_id_seq OWNED BY public.vulnerable_group.id;
            public       postgres    false    222            (           2604    25037 
   address id    DEFAULT     h   ALTER TABLE ONLY public.address ALTER COLUMN id SET DEFAULT nextval('public.address_id_seq'::regclass);
 9   ALTER TABLE public.address ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    209    208    209            /           2604    33391    cat_area id    DEFAULT     j   ALTER TABLE ONLY public.cat_area ALTER COLUMN id SET DEFAULT nextval('public.cat_area_id_seq'::regclass);
 :   ALTER TABLE public.cat_area ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    224    225    225            &           2604    16697    cat_modality id    DEFAULT     r   ALTER TABLE ONLY public.cat_modality ALTER COLUMN id SET DEFAULT nextval('public.cat_modality_id_seq'::regclass);
 >   ALTER TABLE public.cat_modality ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    202    203    203            '           2604    16729    cat_requirement id    DEFAULT     x   ALTER TABLE ONLY public.cat_requirement ALTER COLUMN id SET DEFAULT nextval('public.cat_requeriment_id_seq'::regclass);
 A   ALTER TABLE public.cat_requirement ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    205    204    205            +           2604    25082    cat_status id    DEFAULT     n   ALTER TABLE ONLY public.cat_status ALTER COLUMN id SET DEFAULT nextval('public.cat_status_id_seq'::regclass);
 <   ALTER TABLE public.cat_status ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    217    216    217            %           2604    16677    cat_transact id    DEFAULT     r   ALTER TABLE ONLY public.cat_transact ALTER COLUMN id SET DEFAULT nextval('public.cat_transact_id_seq'::regclass);
 >   ALTER TABLE public.cat_transact ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    200    201    201            *           2604    25056    interested id    DEFAULT     n   ALTER TABLE ONLY public.interested ALTER COLUMN id SET DEFAULT nextval('public.interested_id_seq'::regclass);
 <   ALTER TABLE public.interested ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    212    213    213            2           2604    33469    log id    DEFAULT     `   ALTER TABLE ONLY public.log ALTER COLUMN id SET DEFAULT nextval('public.log_id_seq'::regclass);
 5   ALTER TABLE public.log ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    230    231    231            -           2604    25098 
   modules id    DEFAULT     h   ALTER TABLE ONLY public.modules ALTER COLUMN id SET DEFAULT nextval('public.modules_id_seq'::regclass);
 9   ALTER TABLE public.modules ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    220    221    221            ,           2604    25090    permission id    DEFAULT     n   ALTER TABLE ONLY public.permission ALTER COLUMN id SET DEFAULT nextval('public.permission_id_seq'::regclass);
 <   ALTER TABLE public.permission ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    218    219    219            )           2604    25045 
   request id    DEFAULT     h   ALTER TABLE ONLY public.request ALTER COLUMN id SET DEFAULT nextval('public.request_id_seq'::regclass);
 9   ALTER TABLE public.request ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    210    211    211            $           2604    16416    role id    DEFAULT     d   ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public."ROLE_id_seq"'::regclass);
 6   ALTER TABLE public.role ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    198    199    199            1           2604    33456    tracing_dictum id    DEFAULT     v   ALTER TABLE ONLY public.tracing_dictum ALTER COLUMN id SET DEFAULT nextval('public.tracing_dictum_id_seq'::regclass);
 @   ALTER TABLE public.tracing_dictum ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    229    228    229            0           2604    33434    tracing_turn id    DEFAULT     r   ALTER TABLE ONLY public.tracing_turn ALTER COLUMN id SET DEFAULT nextval('public.tracing_turn_id_seq'::regclass);
 >   ALTER TABLE public.tracing_turn ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    227    226    227            #           2604    16405    users id    DEFAULT     f   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public."USERS_Id_seq"'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    196    197    197            .           2604    25109    vulnerable_group id    DEFAULT     z   ALTER TABLE ONLY public.vulnerable_group ALTER COLUMN id SET DEFAULT nextval('public.vulnerable_group_id_seq'::regclass);
 B   ALTER TABLE public.vulnerable_group ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    222    223    223            �          0    25034    address 
   TABLE DATA               f   COPY public.address (id, postal_code, colony, ext_number, int_number, street, delegation) FROM stdin;
    public       postgres    false    209   5|      �          0    33388    cat_area 
   TABLE DATA               3   COPY public.cat_area (id, description) FROM stdin;
    public       postgres    false    225   W~      �          0    16694    cat_modality 
   TABLE DATA               u   COPY public.cat_modality (id, code, description, legal_foundation, id_transact, status, days, type_days) FROM stdin;
    public       postgres    false    203   �~      �          0    16746    cat_modality_requirement 
   TABLE DATA               O   COPY public.cat_modality_requirement (id_modality, id_requirement) FROM stdin;
    public       postgres    false    206   ^�      �          0    16726    cat_requirement 
   TABLE DATA               B   COPY public.cat_requirement (id, description, status) FROM stdin;
    public       postgres    false    205   ��      �          0    25079 
   cat_status 
   TABLE DATA               5   COPY public.cat_status (id, description) FROM stdin;
    public       postgres    false    217   
�      �          0    16674    cat_transact 
   TABLE DATA               h   COPY public.cat_transact (id, code, description, legal_foundation, status, days, type_days) FROM stdin;
    public       postgres    false    201   ��      �          0    16757    cat_transact_requirement 
   TABLE DATA               O   COPY public.cat_transact_requirement (id_transact, id_requirement) FROM stdin;
    public       postgres    false    207   1�      �          0    25053 
   interested 
   TABLE DATA               U   COPY public.interested (id, name, last_name, e_mail, phone, type_person) FROM stdin;
    public       postgres    false    213   ��                 0    33466    log 
   TABLE DATA               B   COPY public.log (id, id_user, description, date_time) FROM stdin;
    public       postgres    false    231   ��      �          0    25095    modules 
   TABLE DATA               3   COPY public.modules (id, name, status) FROM stdin;
    public       postgres    false    221   �      �          0    25087 
   permission 
   TABLE DATA               �   COPY public.permission (id, id_user, id_module, read_permission, write_permission, delete_permission, update_permission) FROM stdin;
    public       postgres    false    219   �      �          0    25042    request 
   TABLE DATA               l  COPY public.request (id, folio, id_user, date_creation, observations, id_status, id_address, id_interested, id_transact, id_vulnerable_group, request_turn, type_work, means_request, delivery, date_commitment, flag, id_modality, prevent_date, citizen_prevent_date, correct_date, cancelation_description, last_status, settlement_date, settlement_number) FROM stdin;
    public       postgres    false    211   !      �          0    25064    request_audit 
   TABLE DATA               H   COPY public.request_audit (id, id_user, accion, id_request) FROM stdin;
    public       postgres    false    215   S                0    33567    request_requirement 
   TABLE DATA               I   COPY public.request_requirement (id_request, id_requirement) FROM stdin;
    public       postgres    false    232   p      �          0    16413    role 
   TABLE DATA               /   COPY public.role (id, description) FROM stdin;
    public       postgres    false    199   E      �          0    33453    tracing_dictum 
   TABLE DATA               {   COPY public.tracing_dictum (id, date_receives_dictum, folio_dictum, answer_dictum, citizen_dictum, id_request) FROM stdin;
    public       postgres    false    229   �      �          0    33431    tracing_turn 
   TABLE DATA               \   COPY public.tracing_turn (id, id_request, date_turn, review_area, observations) FROM stdin;
    public       postgres    false    227   :      �          0    25059 
   user_audit 
   TABLE DATA               O   COPY public.user_audit (id, id_user, accion, id_user_modification) FROM stdin;
    public       postgres    false    214   �      �          0    16402    users 
   TABLE DATA               �   COPY public.users (id, user_name, employee_number, description, rol, user_register, date_register, status, password) FROM stdin;
    public       postgres    false    197   	      �          0    25106    vulnerable_group 
   TABLE DATA               C   COPY public.vulnerable_group (id, description, status) FROM stdin;
    public       postgres    false    223   [
                 0    0    ROLE_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public."ROLE_id_seq"', 3, true);
            public       postgres    false    198                       0    0    USERS_Id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public."USERS_Id_seq"', 45, true);
            public       postgres    false    196                       0    0    address_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.address_id_seq', 256, true);
            public       postgres    false    208                       0    0    cat_area_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.cat_area_id_seq', 1, false);
            public       postgres    false    224                       0    0    cat_modality_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.cat_modality_id_seq', 61, true);
            public       postgres    false    202                       0    0    cat_requeriment_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.cat_requeriment_id_seq', 484, true);
            public       postgres    false    204                        0    0    cat_status_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.cat_status_id_seq', 1, true);
            public       postgres    false    216            !           0    0    cat_transact_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.cat_transact_id_seq', 39, true);
            public       postgres    false    200            "           0    0    interested_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.interested_id_seq', 216, true);
            public       postgres    false    212            #           0    0 
   log_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.log_id_seq', 144, true);
            public       postgres    false    230            $           0    0    modules_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.modules_id_seq', 1, false);
            public       postgres    false    220            %           0    0    permission_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.permission_id_seq', 53, true);
            public       postgres    false    218            &           0    0    request_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.request_id_seq', 188, true);
            public       postgres    false    210            '           0    0    tracing_dictum_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.tracing_dictum_id_seq', 25, true);
            public       postgres    false    228            (           0    0    tracing_turn_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.tracing_turn_id_seq', 37, true);
            public       postgres    false    226            )           0    0    vulnerable_group_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.vulnerable_group_id_seq', 1, false);
            public       postgres    false    222            8           2606    16418    role ROLE_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.role
    ADD CONSTRAINT "ROLE_pkey" PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.role DROP CONSTRAINT "ROLE_pkey";
       public         postgres    false    199            4           2606    16410    users USERS_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.users
    ADD CONSTRAINT "USERS_pkey" PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.users DROP CONSTRAINT "USERS_pkey";
       public         postgres    false    197            D           2606    25039    address address_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.address DROP CONSTRAINT address_pkey;
       public         postgres    false    209            V           2606    33393    cat_area cat_area_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.cat_area
    ADD CONSTRAINT cat_area_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.cat_area DROP CONSTRAINT cat_area_pkey;
       public         postgres    false    225            >           2606    16791 "   cat_modality cat_modality_code_key 
   CONSTRAINT     ]   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_code_key UNIQUE (code);
 L   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_code_key;
       public         postgres    false    203            @           2606    16789    cat_modality cat_modality_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_pkey;
       public         postgres    false    203            B           2606    16731 $   cat_requirement cat_requeriment_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.cat_requirement
    ADD CONSTRAINT cat_requeriment_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.cat_requirement DROP CONSTRAINT cat_requeriment_pkey;
       public         postgres    false    205            N           2606    25084    cat_status cat_status_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.cat_status
    ADD CONSTRAINT cat_status_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.cat_status DROP CONSTRAINT cat_status_pkey;
       public         postgres    false    217            :           2606    16690 "   cat_transact cat_transact_code_key 
   CONSTRAINT     ]   ALTER TABLE ONLY public.cat_transact
    ADD CONSTRAINT cat_transact_code_key UNIQUE (code);
 L   ALTER TABLE ONLY public.cat_transact DROP CONSTRAINT cat_transact_code_key;
       public         postgres    false    201            <           2606    16688    cat_transact cat_transact_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.cat_transact
    ADD CONSTRAINT cat_transact_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.cat_transact DROP CONSTRAINT cat_transact_pkey;
       public         postgres    false    201            H           2606    25058    interested interested_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.interested
    ADD CONSTRAINT interested_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.interested DROP CONSTRAINT interested_pkey;
       public         postgres    false    213            \           2606    33471    log log_pkey 
   CONSTRAINT     J   ALTER TABLE ONLY public.log
    ADD CONSTRAINT log_pkey PRIMARY KEY (id);
 6   ALTER TABLE ONLY public.log DROP CONSTRAINT log_pkey;
       public         postgres    false    231            R           2606    25100    modules modules_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.modules
    ADD CONSTRAINT modules_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.modules DROP CONSTRAINT modules_pkey;
       public         postgres    false    221            P           2606    25092    permission permission_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.permission
    ADD CONSTRAINT permission_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.permission DROP CONSTRAINT permission_pkey;
       public         postgres    false    219            L           2606    25071     request_audit request_audit_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.request_audit
    ADD CONSTRAINT request_audit_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.request_audit DROP CONSTRAINT request_audit_pkey;
       public         postgres    false    215            F           2606    25050    request request_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.request
    ADD CONSTRAINT request_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.request DROP CONSTRAINT request_pkey;
       public         postgres    false    211            Z           2606    33458 "   tracing_dictum tracing_dictum_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.tracing_dictum
    ADD CONSTRAINT tracing_dictum_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.tracing_dictum DROP CONSTRAINT tracing_dictum_pkey;
       public         postgres    false    229            X           2606    33439    tracing_turn tracing_turn_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.tracing_turn
    ADD CONSTRAINT tracing_turn_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.tracing_turn DROP CONSTRAINT tracing_turn_pkey;
       public         postgres    false    227            J           2606    25063    user_audit user_audit_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.user_audit
    ADD CONSTRAINT user_audit_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.user_audit DROP CONSTRAINT user_audit_pkey;
       public         postgres    false    214            6           2606    33531    users users_employee_number_key 
   CONSTRAINT     e   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_employee_number_key UNIQUE (employee_number);
 I   ALTER TABLE ONLY public.users DROP CONSTRAINT users_employee_number_key;
       public         postgres    false    197            T           2606    25111 &   vulnerable_group vulnerable_group_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.vulnerable_group
    ADD CONSTRAINT vulnerable_group_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.vulnerable_group DROP CONSTRAINT vulnerable_group_pkey;
       public         postgres    false    223            ^           2606    16815 *   cat_modality cat_modality_id_transact_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality
    ADD CONSTRAINT cat_modality_id_transact_fkey FOREIGN KEY (id_transact) REFERENCES public.cat_transact(code);
 T   ALTER TABLE ONLY public.cat_modality DROP CONSTRAINT cat_modality_id_transact_fkey;
       public       postgres    false    201    203    2874            `           2606    16838 B   cat_modality_requirement cat_modality_requirement_id_modality_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality_requirement
    ADD CONSTRAINT cat_modality_requirement_id_modality_fkey FOREIGN KEY (id_modality) REFERENCES public.cat_modality(code);
 l   ALTER TABLE ONLY public.cat_modality_requirement DROP CONSTRAINT cat_modality_requirement_id_modality_fkey;
       public       postgres    false    206    203    2878            _           2606    16797 E   cat_modality_requirement cat_modality_requirement_id_requirement_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_modality_requirement
    ADD CONSTRAINT cat_modality_requirement_id_requirement_fkey FOREIGN KEY (id_requirement) REFERENCES public.cat_requirement(id);
 o   ALTER TABLE ONLY public.cat_modality_requirement DROP CONSTRAINT cat_modality_requirement_id_requirement_fkey;
       public       postgres    false    2882    205    206            a           2606    16776 E   cat_transact_requirement cat_transact_requirement_id_requirement_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_transact_requirement
    ADD CONSTRAINT cat_transact_requirement_id_requirement_fkey FOREIGN KEY (id_requirement) REFERENCES public.cat_requirement(id) ON UPDATE CASCADE ON DELETE CASCADE;
 o   ALTER TABLE ONLY public.cat_transact_requirement DROP CONSTRAINT cat_transact_requirement_id_requirement_fkey;
       public       postgres    false    2882    207    205            b           2606    16832 B   cat_transact_requirement cat_transact_requirement_id_transact_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cat_transact_requirement
    ADD CONSTRAINT cat_transact_requirement_id_transact_fkey FOREIGN KEY (id_transact) REFERENCES public.cat_transact(code);
 l   ALTER TABLE ONLY public.cat_transact_requirement DROP CONSTRAINT cat_transact_requirement_id_transact_fkey;
       public       postgres    false    2874    201    207            c           2606    33440    tracing_turn review_area_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.tracing_turn
    ADD CONSTRAINT review_area_fk FOREIGN KEY (review_area) REFERENCES public.cat_area(id);
 E   ALTER TABLE ONLY public.tracing_turn DROP CONSTRAINT review_area_fk;
       public       postgres    false    2902    225    227            ]           2606    25021    users users_id_fkey    FK CONSTRAINT     m   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_id_fkey FOREIGN KEY (rol) REFERENCES public.role(id);
 =   ALTER TABLE ONLY public.users DROP CONSTRAINT users_id_fkey;
       public       postgres    false    197    199    2872            �     x���Or�0���)�	\���)-Op��t�nȨ��.�Mϐ#�b���4�(�V��я��{O8pP��A����H�ىCy}:�3Tt��!�e�M��i����k���#}���LW8p�0��6�n���8�\�z���U'	��0�7��Ns�UK��'����@9��x��m;\�F)�mžL`j1P��"U�����8A��a��.?
�>�%��"_��0���b_��I��{J߮�����J�x�8N>�o�Dہ���a`�BÙ4��Z�ep+-��">����"/���YP�#U�[���{5��7���n{:r؉�OC˕4��"D��Pl�w\�ڐ��B�w��3H�j�Bk�qX��%��*-�Ҡ���J�. h���c��m�g6WLiV[̳�b�ɈD"��g�)�(A��Z"K�jh����h�:������ôV9ؑ������K2��̧�2��P\v� ݝ����b'�睞Q�a�����ø��W"+�`?��1���$60_׫��7�z�      �      x�u��	�@D�V���_Z�0
�U�lC -唒ܘ��ܲ�a�9Y]�u{W���S�,h�)��b���b�x�jp��r]|{eej�dI�z꜇����πs��|Ӫ1;}&�t�����YW-#<G ��>�      �   h  x��\Ms�6>3��͌�H���'WVZ�4�k'��L/	���CIO�����ч^6��?���iSvb��l��QL� �~<��Z�pb�߯E(�&�:R�淠�Ҍ�6fWl�B���*L�:U_-�S�"U1OЌ�p�K�2��"[���-2�K&cqu���|�@нs��"�h���
}�R$y�ؚ+�X��B�'9z�v��e/q��tu����d�VO9��"�!}{��~�G �^���H/@&�4�l������q�DR��S%?�@n>%�g.4cS������&�x,<�L�	%�	�򈉪w��
�=&~'B�U�jv��HL��Hf�%d/�X� 7���@�Z�8�<(��'u4��I��Ч���83ݼ��GSGܑ
:��+-�j|r5��1O�Ń��|�����p����|�1JW)I�;�ҒuQ��"�J�Q����P�����I0�Х�b�!��D�.�mΨq����B�z&V�5�����E��2�~T�Ӓ#�cz ��
H���1[JH�u����>���{A�#�.pw4%�LY�!�A\��Jx��[r ��K�p�1g���)(�1���䞤Y	�����_�0�:�\2Nُ��N^�f�D��\�'S�~�\�s\l[�J/E��o�js��G�%�WI�׺w.V����6��$ټ=+�"gQ;�d"J��V���~�)}�^��B��I-h�	��r�&��ZC����ph��]�F�:��� \��6�K^ZT
Wd�Aq�*� �d"������@�HX,r�~�^�1�-2��!�'�'K"�e�ڀ�3��c�7�d�8*8��"��;@�Z
"a�$�ϮJ�^³�ueZ�������cl>&��R��\���J���v�pq�h�4'�Qz�}�,�Ib��4�p��*�7�S� 8 ry�g):3��.�� iE_D�oJE��OP���B�e��c��d�Z�و �j��!^C��&hA��PR��y�i�ƣ��w���FJ��%�P_�)��M�s�Wvh�~��cߨ%O����k9���W'�x��$�؃��ȿO�M��8$_�p!��f)�VE�Qi^����&�mt�x;`C8J��%�8�qE����>ܧ�g>e�v�P�>�v܉&��:L�a©N<t��L=̳�b�h+��53̵H}�6�B�H�Bjv����>��?�6qi���ڎ�C#)@Hp�df�1p�=����(g�5�HBH?1I�~я@�TR�����2ɶ��8�A�+W)n��nzhMm��|���j�	�ur}��'�}���Rc������m��Z���+j��4.�����.�s&�0I܉Z�ԳD�M(ލ�f���i++:��������g4�fGMC{�x�5"�ծL��4�m���l��Vy�PQ�8l54n��Y�7jId��$)J�5ŷ��*�nK���6#_�0�B|K#UI��&O���1l�C	m'5�ق�0�x�#�~�$�Q����m�~6�|Ƕ����J��|����3M~Ce ��jf�_7�#���7�����/�Q��Ȇ�z�|h]����F�e���_)�cȾ}�P�ګpZ�K�Ʒ����[�����Ve� ��b�g�*=G���D�\"GI�M^4+��P��5�=)��'�S`�~CZ��������UW��!��Ki\jh�7D"���M;�VwTNi�J@F@���|Nh�s�u�����3�?{F���}k~s}*+��{[��Qڣ�)�m& ��j��fM6�eD�ҥf���zI5q���\.�U��-h-:cK�ԙsZ�]!%O��*n�#�����S���0��:�ZWb-r��ʥ�"������5H�Tϣ��d�����4�ڀ��:.]�V���ULߎ��=4����{C����Y�e�U\������4o���[� ������S؏�qZ�p���
�e����;��O���޿pz?��PzбRZҧwdy�?���$c���~���&5 �ު�b���Ʈ�G��[�S�W]9���)�Ө�"ڦʢI�Ԟ�"\��ur4"ywW�XY����E��E����Q_Rg���uG�x��
������Pj�=��;4��(%�.�Ď�����p�0�Z��w��I���[B�%
��O^��.�Z{F�V��Z��0w�j��V�`=0Z�s�[�a��$�
 ����VA/�
[Ƅ��Ԍ�|YۋvW��դ�0K9�aL����M3�+elS^⒀&���Ш��/����MD�������3�rc(h'��%r�ʥI��aA]�XF�+�o��B���bERn<��/�Y�M-�[;�sQ�>C�Q�D[�_0�	��)X����4��TsM�R+5"їF����Z��-�t�����Ż�4#n���vwQ��2)��<>~����5Őj�ݙ��$~۽7�/ӚOj"V���wM^Pup�8�����tv`�Ã�.{����в"<�íD�{+ �Ǵ=
<.�0�z�v�Z������?�N'vU�s\�̡Ŗ��1���
]U�H{���{8�Fr����6��H�##��f��v�,m4C���$���J.Rs��ڕSfpЮSr��+zT�i(�*�|*��(�Q.t������V=�{��uOC�c����Y����<�e	~:�w�����8��5�F�x^.�e��>q챵��[�uA1<,H\��T�= 5�ƶu�:�Dڪ�����K���j��}��%�;��{-�n�x�)���c�o3�.;̤- ��[������&���N��Y7���hH��y�eT�;W*��|ۤ�䥧0��R̉{���we7V��x y�)z��#o�Y�g{w_�t�����/�h�Xۤw���5��ͷ,�ː;ٶmנ��۳���{@SS�VYo!�!�	v#���qKSz(�]�2�JBkL�i�6xAU�|���Z�*��z+"���y��1:��sg�`�P킢.��z\*/⃿�������G}���>�o������!�~|��7�{|�n���lvV�{LG���~�՗<��z��XО$��
��H������s4���V'ZhIʣ���7�b[�}H�~C�i���\z,�:p�-�:���]��!�{=����7�ݧ��4�Zb�-����_��V�ѿ���ܣ�ԸS�r���N�.���_�I�ƴ�i:�Ha�38����9��:s=�Z�f�_o~�I�v�f7V��n�j�kQySkL�&�_ ���&�Čy�����]Jھ�joM���Ȣ��$����J)<���D��&{�g����}O�F�rJ�B\65D�O��]
1�3�jS����!������|v�'��񜝞��{d�?t�śW�G/�^���9[/f'�/2��={�?��J�      �   %  x�e�k������¯ s��G���VK�H'e�q� �v�e�k���wø��w`l�r"��&�3�]��:���Ի�c�����O�v{�}�}v�z֭W}=�ѮU��D�N�5�(vN�,�7���{涍��=#ݶmm�{��YG��r��Ѿ�8W��>6���㚉���w��^Q�3�g�:��s�;�X\��F{ލ����F;~S|5z��:S���wϯuϼ�6���uD�{>��>�O_��ޮb��}���-�h�k�x:~a�E�`|��3�q4f��S�ķq]��o�G��\YS��ƻ�6�Q1sׯ�=?q�����s��=�H{����κ�q�щ���"��o|���������1�s=�چ?m�|��1�Ɏ���L���i�2<��ɣ���ӆ��{ʪ���f��&K�F(���2Tx��G�.׆�%�r���u����ڑ�iG�w��K��P�ɣ�1�������F��[	]��Zu��w������I?-,��a��V%ƾ����Uh/�T�M�!�O�q>��4�t���N�E�*�[��p��#�d��`���,A]����V�(�"+��n�!����K�7D��&L���­��jױ<5E���"F%S��T��Jݱ��29�n��0�aI����TfΕ$�)�M�g5�D��M=d�'���G3C���G3C[Pr"��E�G[���-|���������(���]ꎑ1J��W{Λf{�2�t��.���;)��c��ΡЁ���)��	J�6�_��LP8ǅ6�Q�Km�h�F�4Z��+�fitK�����F�4z��<��i4K�[=�h�F5z�у��itO�}��ˡ��F�����2�{���F5:��BM=�uxwxwxwxwxwxwxwxwxw�pppppppp�v�vTq�v�v���C�b�a�/��:`'�K�;�;�;�;�;�;��������l
llll����

���x_�o�-��d����R�R\��9]�ihhhhhh�h�h�h�h�h�h�h�h�(�(�(�(�(�(���(�����h�h�h�h���<y�rhN�'��e��3�2����������������������������℅������*�*�������u�VRq��y��8��2������������ڼ�����t���G�՘Vk�9��͹뵾���z���k}�^�+�Zߛ���g�
^q˵.��-罆��d��k�Y���KQ���<��G�}�@����{��(;��7�/�}�>�l���7�������μI'      �      x��}Koɖ��W��t���^�$��~�eW�Ѹ@#�L��H2ٙ�`֮���]�j�EӨ�Ŭ�l�+"NDf��쪹5=t_K"�y��wƃA�\�I3S$�6_n~�����^-��!��ͯs���&�Ei��l�\%ibee�����r���%����k��9�q�W�tJ#|����T����UV�h�o��mV�uo���`�;�g���L�٢�f�|�T9�\�����&����Oa�d��s���H�U9���i�l�T��W�}s[�8�I�7���Hn��#TI��}s�g�?z_��0�2��&��e���g���sZ��w^�����۫�|��^�9L>��2�ɔf�s���xi^/�:Ǔ�M�߯r���|c.a8�n�T�M����lΣ�5�
s�,3�r>�fsXs}`�-|�f��?d��/�o��S��,��q��o��j��Ӣ���j�_����4���9,������'��>��}�#��i&3�Q	�2SX�2p;�s�����e����>uY/��VE�-3�6 �]�9��l"�80��u��Ϥ�\�dpZr��:����pܻ���*�%��Y���v&�p��8a<ܳ_�#�L�O�Η�hJ��#���r�	�ټ���H������y./f����aV�v��4L�J
XE�,a"=���Uf�%�,m�KR%ҭ����#-q#�ɺ,�t�?�ղ�����fK"a��65[T�-$?�{z�)<��%"�gV��p��_�>�*��Iq`�U�}����z�;Ϫe~O����y>-������硫Q��k����UV��K���Ʒ���b�\-�*|�lZei�T�|���/�^�R8~8*|-���B�� ���<�ӯ�CV��H�wy5�
%
�mn��yѬ�n�B���U��w/.��Gi���J���u,a X3�O��W@mu�q�'�7���xVO�|�dj�_�k\S	w�4OaKH��m~�7T��yM�D�oJۇ���y�HQ�!k��m�����I�/b��<[���	I�Ҭ�� �M ��j^�48e2���浐�t��޴�a���CY��3��[��&Q����5썞pFk���8G���i� zx)���q��� ��+�U�d�E�������mY��>l>O���_皃಑��6?;�b���/���9J��%��l���4�m���p��%}��R��k���Vs>1��A	L�����|	���J��󇬐s�>3�O"V�c"!��4����J��NA-��?��%R��omXÇ����02w{@�.�B�e��'�3�D��b&���h����/�n�ݗ��ѡ�BbALb��$k>wL���o"箙	gD9�ϓ�K�0���qٸ�e�O���dQeS�<��0�� t����g�o���7����3:8���β����������I���a��O7���6��'R��~�?���$����Y�i���6,�h��2׃�l~^�S����"|��Đ����Ȼ_��<ã�kH`���Sp.���o~IP�/�w�
P&�B��/D�Ɯ���d�;9��\�_�9�ک=�:�����B?��l�h�����h��	.H��4ħ�W�D0�Ν�ky|�.g�ϸܓ��h�7�@��7�\�(���F��m�$���+��BfG��K7dXSKR)3w�&���M'ȃ
z�$	-[�7GoF�����_P�ğ%|}ok���8@���f�tI�T��팥o	����*4�YZ��.VL
���r�%M�\�Q�S� � 3�c��<�ô��c4��\�lf���d��LNƽEW��Qo ?�@���!�b�{���o\T�U�ʏ%��45d�p�3��4�/8(�mԻJQ>zŵD���L$����� ����"!{ְD���l�9#H�����Z�����V��ײCR�Z�����dk�כ����{Թ]T_�����_�~�	�d~b8�[^�vk1��TX��%��"�4�Ќ�F��<�
��OH�x�h �p/X<�<l�u�p�+P�������=������.������� `z)�x��r�5�K�h�z�' �����q��m=5����X�H��I��q�;-Ck�&"Y������0�sۧ	�b2�E&#�1<u��C{?��2q[��?o���,-�I-��B6��<p��S�[WlՆ݉'��:���)ƽ�����7X���ۤw�V�/7�+Pى�����K�(B(F{����I	&\�@Y�D��a�Ywç=��4l��2pz(��l���}u�H�6��v%���P�؏1��R����������6I���x��7�ϟ���-�GgO>w�8J��2,�]�>�q�3�Q�8��.AM��n�z��A���+��a���Ĭ�3ڹl<a�(�,���=�r๑�z���r���51ZJod��H�mV�)��Զ�vm{4|�\;��h��4��ƷD2����J*�<����GPe$>����C)���.ߚ��ח�6�z���w�͇�����wo�^_�al�������P�k�����m£ˊ��4˲�G.����Ү=s���g..���ͻ��q���}��.o�����s7.����zI.z�d7L�W���O`J�p��0$��,I�Zƅ���Oc�?�i��`�*�Be R1�"֊�LK�L��d��.�V�h��l~��j�D�	{��	��@m-�y"�-(B�%��T/ɜ���[�	�^��Z�
?0�ȌZ92�C�1DA�DLփ����_l��j�����>-r�jO�p��MCga��(�m8�H2�R��1�gk.u�F�n�;��S'��(A��&5O�:ȧNǲ�,�����`o.1�d���^o/[�U�^�v:.�#V�azP%ŀ�&�[�q���3L�PA�\"���D!ٗ?�	e�jz%���~nW�� 4hm\�F9$T��m�"Yȟ�_����� ���ȟ�|���R����#���rH���^(���lCN��,��ٴ��dv�>�� ��g	��:�G�a
l����
\+;aKy�¯�I���n΅d����fDX�d�#��`��<����rWC�/��(�@V�����n`mɿ���٢S�aa�`{§�/��5�e#�G��a[/�$uLIb��
�-���J-�,�(@?�o�	�2�l��h������͚����z*]�
�r0�ǀ�1aY��28���A7�e�[��.��$$cJ����(�e@�>��n�S~�|��Wr��}�!�����=_���Ngr��K	��(1�t����W�����cVU�GҎB��R���o=me�\mʎ6������ m�s�.���fy�,j8j���2?�6��`X.��7��! &�)��u@�dԄ"m��H�B��њ���E�*:�x\�������Fe�c�
����ˊD����D�<�}�"h�-Bc�ִZ��1Q��a��V�.R�;�2��&��p\%y��]'.�������ƽ3�	��UlR�H�"���וC��6��FK�P~ޮ�Ozכ_��8�da#��Dp��C�>�o�����+����=�7�E���]���ҷ��3�I�}����]�v+����^���U��lt��4�u>e\'��l�>�>"(���h9l{���O���`��z�݌z�ю	�0{n�c���a���Q�N��g	��3��ο��&Ŕr\���>�m��zj݉��i�O��&�iFaRknX�X\��c31-A/���W��ĎY��sK�H�f�c�����Q�M�G��xג���E�Fõ���;Ջ�\29E����M��/Ը����W�u���$��a�2����4�L�Y^�p�H_*��@�{�-L��n[���+�r���=��9���wjV�OX�>�=!'��#�|d�@&��2���u�лW@g@D� ���7��!}9�/��S�Ot�RE�~RJ�r�fʹ��{҃����ѫ<��6Wx�s~���    ��||���83�?0�X7��4�-�\���8�ƺ�3�=��3�❱���K �0�d:0d�V!8�xߣ��N!�w����o=fe���h�?n�T�H_��Y�_��:[�d�}��Zi�u���|�T���
��9���HP�L��������h��Oz}�S�RI��/٬I�"�b
HJs8�ֳ6�IA�9��'=�d����1)����l��d)�@�=q|�N��IA����N��������թ[ҋ��ӏhz��l�^�D~��Df����������f�(w�Ĺ!LRJ B9�g���kpI�8g�$m��:5�V�4���"�l�
�2{�5��N�q�Y�j��O��w��~��x��*�����q�?�?�_�F���A�(���*���Z�s/9�o��wx���;��M7B��d�|�G��,t�U��ł�-��I;|V�Џa�RٿY��s4T�@4�|l��-�_��ݷ�����%�����ƿ}"�֔%�����L0��,��S���.��%q�0�ݝ�Ul"UA-��Άz�R	��3���������(l�w�Bǹ�����6���	��y����:z��L�J�����E���+����M�rO�|Up|M����3���9�uc�GK�7֕<�5�&ˤ(IE{�~����Vёptҋ�̂R��t*�M�(�a��N������?SΖG,�G���ì::�{e�f�g�����P�*�:�s���q��^��Ž�2%����zL�YS�E��7p��,�=�X����=;E��0���מ�sٖd��z`�PRz���9੾[t��)c��#V���o�c���\�������$k&���L��>�,�t1����M�t�K����s.$�Όh����2��i�����ĩ�6�4<V�W�XT�&�3";�q�$ ~��F{�{n�H:xx�2�^}{P�O)��\v�D���R>8<��	�(	�*H���D�D|]�:b�s*�)ז���y���LH*gaJO(��Vn�W0�J(*ce>���B���Ў�͘J���s2%�"7��j!.��:�_S��]�=s�5��D��}�|�g�:��ԗ���Ƨ�� ��+� �������G�UA�o��n.߼��|}v~����/�PV��8L{��0	���P���o|�5��R�Ͽ������)pZY�y�)β�i��*���O��J�C{|��񵏳}c�N����m��`cd�"Q9w?��>��"?,Z�y`��Đ���e��%N�˦aa�ϖ�8���
o��j��_|��I�d��ld��0��d�U��M�S����|{)@��m͉��T��i� ��Y�,:^�g\��W���ʻ��?�|./������-�h��Q�۵��C����2�Y���1c������A�;���y}%�P���t&�D%x�g�[b�3!�g1����'�qeO��x&ѳX���a'	f�e*�IO�a�:<?�ve}��^�(����T~Sv��I�&6ߦ�1B����J��>���|,]V[F�@��(O�s��-|�W3J� |�Z
:���,NJ
�*�]�Uqy���.uz��l��D^�a}���9�9x�uL���x�nd��E:&�-� ؑGt��6R��*�U��JyY.�D
� �T3��9+,4�Eh���F�3^�N�mS͞N��-u�Q��*�c�pOn���A�"�VՐ�%C�Z
��rHӸG>�Gܭ����4yp�u���װ�E��/)sh�.4)�e�9!��9<Q��ņ��$;MV:������T5�x3G��^K�u�Z�[�T����R:ޔ@%������i���h����,ʨ����@�Dg�4�"E/�0Q�r:�T��Xm>�v�(���p:@]������*����D��&�)��A$�/&���0�?�S�)�_�]Ғ��'k�تn�R�����o?Ub��$�G��F�u�p�h0�T\ɁZ.��\o)�}����cʙ�`����$��8s�S;.�d������1Nѯ��,	_����f��<#k1��1Ѕ�@��r���@��f���J��bO�����(D�7�7~��7����&/�7	Z..�����>�;T�5h�?6�o�6���[�g鑺�>�E��x�u�¨�@��z"�3������a��-5����F�a�	�#P��Z��)O�AC\�S��Ĉ������Mߤ�r���Y�RX��<O|������
��4�Y����5X嘬�j�'���U+H'�f���B,�"�Ra�o�NvU t�������Hɑ)�PŠtD���4AB���(�3x�k.�ϝc�?�Rȭ��#l�IyǺٝ���B�k��-R���ݛo��o������F{�.��R�4�c�&$&
�Ij�@>�՟��?�[o�&kȵ��d���`n.0��7��CJ,@mEuSz7�Wb���K~C<��j�7LDr�n~]K��U��i��UZJ��[3/i�;��l��T���z���?o�B�^�K��ȜP9C�/<��7���%󕀣�I�8B���VB<K]����%����G⋹j[����9�jk�jl|?�G�7�+V��Y)��z�#��e�ZW=S�
G��_Y˩a�I�&��V[�����m���+_���2�Щ�s{k���<[�j�b��T��M��Eg� f��pȈ��ɺ�KAko�7��E�Z񴔆�r���}���秤rQ�����hi�L@�bA��z42�ÿ7�"�֠"�����y}p}�,�$~W�֏M�{I�!ʹ�o���k��0�iF��\�<����{2.�۲�nNaYs9`JF�p�`��t��|���-uk�w��8�����!,"BY�`����Μm�W�UN	IMѶRMvJ�c�����{�죽b��lR����`N�=�Uk��&����&�P�yf]�VH{��4̫SB��e>��� ��d\U���;�YWk��%)�X�Q9P�*�l)^��Q|4��"�UT��8�ց�tV;�fAfWH�Z�3o�R�Z�a:4<�&�c��:�5����
����y�^�J�踍jU諩�>$�o����¨���?ZO�5G�c԰���{�_�Gx`W��Ho5��٢1@������愅 �^�,�ʧ�4!xmf!��d0�w�A�ʉ7͚�60`��8Q�˴{�9�4�$0��}�RD�O�>%��ټ�o�1T�n���Ǌl �f:�M�9�ȷ���k���~n�$�IMa8�d�G�U����4�b�[�$�%ӂq+��IgI���|��'U3/7��'oĸ��_���p�ap������Ո~
`g���%��e��i8m\�g`�X4^#0�<s~T5�����s�T7ul��.�>�����2�U%Y����Jk�О3������t(��ܮ��ľ�~{F�)�,����/*mC)�����O|�"��m~���\v�a�ެ,6����GN2r<~�p6(�C�,��C��!��+�u�Im��'�n��4���
"n,�g�U5y��� ϓ%����r�}p�2��Ƭu�h�Sj}i�콲��iK/��D٭.��7�6��w|ڻҁB�)t��>�89����cU��ҘY�Ԡb��U�,�Ƥ��>�<����B�hCX!���o�^I�%��"�G%��U&�M
ԁ��kC8�~W�Nc�0��Km��0�g��,���T�屖�d��R�ф��c��c_��x��6i�,OV�h����Y�X
<�����/8y��������u�\ߩ�n¶��5K�@7o���n�i���dQ ��&����-�*�;���,2�Ͻ�+�/�H8Z1~#��ᐶu�剩8����T�����q��1Y>��I#;�q��~w�zn���`=l�p�x��d�	>1e2Ye�Z
j��N���f��)c�k�u��xL���	���a����O!����o�A@�0�\�-9#���]tY�HK��r�x��z[�4.].�wG�C�m�����0 �<    ;<��\���"Y��!�R���JI�d� ;��g��m��>�H_��D�m�A��M��HN�X�_�n1�8AƒrEDQ:(@�Ȕ�2n��0{'������?������a�-Y�/^��m~{�� ~C�Z}�w�cn	1w��o�%НK����n#	P�`A�Y��C\�HI֌�/��eE��N��Vqi:Y�����m�E���R���&!�8�iDQEVAd�I����T Y�H�7�y0����w�%�R��4�-��&�3��yY0�c�5���H��[$�F��I���D�
<��Rp���c�d��ܒ�d�󧘖���ѓ�a��Y�o��VW���`��#�ƅ��mqE����U.?l�޴咰�f�yM�����ū�����NB1~2��7�z�c�>���[��_�t+oq�,v�b��nO��aO_U��U�Tu��oEFI��������t|+֝�lͯ�5,�[�nb�B��V���D���!�h�ܹ��I?qX&��+�W^^zm���`оN����� ��3x�}"æ���2����jKl5��x]t`�Px�~��\\6��ֶ�}4�co�GE�\OԷ�i��Gb���.l��J�v�xEAE�6==�׊6A�ҳJW�l�ze���MXd���$B��O��㴙�p_��H,1��U>A��|�\k8���W-K�9i�/�e���e;��i��.If3��ŋ�"N��-�B`�J�'��:lֺQ�b�iĭ��8�����H2R�E��9U��WI_�`��2�z�'� 
���["�h�r��l��Z3����Fa���H���I�?d�oІ�߷�މi���N=��y̹���
��K;i��l�qTk<v�њ��_���R�@�˞Fn,���I�@)������vH�4�w�3o��/"#^d����a��,�C�4���	v.yi�<`ߦ�v͹��>m�ާ.b,�$�B>ó~l��ĵl���4�	���y]*��.���#
�l%J�K�{Y(ᄻS"'�Q:��Jc�/�C�t�.Mp��Z�X���I��K��H������,B} �0)�\aw#}����Í"u!I�%G�Z�Bx�b��c֎e�%�;���؟�}au:Ȅ�6��=�*���EE��6ܨ�n��a�[�^3�hW�[:nn�1��e} �����=�����M_ c���nb洭�~���w~O8�h"΃`+�hG� �w
�;�������]d��8%h�W]j8����ji�A ܪ��v֢�5=�FPu���%������y4ոw�>��\���P-�&[y��u�F> �h|^�ƫ����Ѭ+~(ĥK��}���R�?j6�٭i�[��O��@pXꍳW�aA#E@��X��`ñ��{���>�Jw��n+A|���D�I�h��d��g��Yc"�"�^,�'����W����֛�i���N2Y7�P$z���las��w��������Ƒ?B6�Aڙ�cb��/���Hu�!�ت���hc&w�U�rYe�֎��|J��&R���NpK�W��#�Rv�c�5j
~��M���-Y:�O�}�Kщ*��� p"y<�x��5Z���>1zس�fK�O�/x�Ԧ9�����ɼ��j�!|�&�{7,V$W��9u�����m��_{</�d��3��lI�m3;�	�ӱ�� ���m�	miD�H��٫O7r�oCP:.�hj��V</�R���%��@�~/P�ք�ǡ�����V��W�Q�Jy$1|K:W�EQ� ��O\����,iD��G�'��r����q����F�9�։C����C����|��4B���Ϩ��I�<_IJ�m"'�H���ϖ�� ����ݗ ��mm�(��{��L�9V 	��@�'�c�����k|I��2O:�'w���@���S��sJm3�Y��*�(��%�[p�X��`6jI%�`Tß��P,�XH��a���/�Y�X%��>�h��94E�;�D�,�G�YRo~e�l��H�&Q�����ح:�Z�x�]س�� L���c��.�~�}Jk_����M��h�_�x��E�/�}���n�����)��f��8�͌{ߖ��M;96+(��,x��;���Q���k���u�k��,^�X��b��F=�pO��S�͎/+դ���A�C���D��X��Єs}��s��m}���还8�͕��+5k����K��� ţgRS�>�AG��ur�;�����a����-K�Y3av�w�sa�����;ɩ!K��fO7YQF hqG�~PA����ֽ�*�}es!4�&���'k�xT����g0�E�J�!#ONД�4�[8�Mv�C�=L�[.I�a�3��ħ��V�K���;T�@��8�S����
`לa��Fso*�<}��g����z'��9ް�Z]�y �,j�Ì�fa�D����%b���M��U@z�;r$���X�C�����]ΙJ��[4F6of�:g��|�U�}Ǟ� �	D ~H��϶��'[;,�c+m~JDCYJe��o���ev���`y�������"1�r� Y�.�=f�rF�;����_ͤ�div/߳�;�WfBo���|��sx�i5Y������G�,E�&�+�)ɞ��p���C�d�B��ҖNz�+
a����0�}������F+�R|���=�d{��G��O�R�M^x_^F�"RW�@���f��v�2�Y��D�%�cS&��U��fF��?-&t�p,FF\�:��7�2���z�0�������ݞv\#-ִ�*_9&�����4/%��4�l$�TM��A�E�Dj{j�
��NǑU�+�[!��'M<�i�y�2u|��Ȅ�7�Y�Q���%	E�Ŕ�4��ȵ��R�)�ba��ѲG����������H�s�U_WI�L�wB�:���k�tn���2u%�������"=_w�}�z����g�!����nC6�>~K���4!RPG7
}�]��j��Z����h�h�Ҵ������_�p����;9,��c7Z�[�/�N2`&UfSR��G(����:�/oߙ�����9�/[c��-�8+��djV��r��K�+���ey@K��zW�?�im:γ��W��;6)�T�k*�d�b�S2��ZZ��`���������>��;vw����P�-�{U$��EUӯ`��OIG[�a�o��5u2o���q�v��zk�خ�A��؜뒐P�g�Gv�u��B>��H��ۃW'�~v�C�����󨏪k��9�Zk����1}L檼qoKڈ�DK,ҿ������-Ȯj?f8�����qq�0<��)>��e\��,����E�}M��h+��=��`��kԋ2��z��5S[�v&'��:-[ø�:!"Ȉ�i����2���ý1bO�畞ͬ]�C��S��'���)�۴E�j����π$���_��B0ב��P�	���&nPR] ���\��s�Y(6\f��U�Iq}4�<�@4�#�%F!�D�֮�[��������pO���5u�"�H�q��f���y�'����+�2�p�tE%�i����I�]�@mڅ�f̊G��J��Y��>7�s�^lU��.G�X�2e�+0~_m������KC���b�-CO�G���O���N@�}�K�'@�`\��1��ҖF�+i���#"�
w��� �gӕ<$��A!�Ŗ�g���la���$��`b'O�^�$����i�Xz;�Z�����X=���r����f��S_�z��ѱ|l��B^��/�ڐq��������4g(�2�6ąOVC��>���T��V}[-�R�f6˾o���ek��%&�}����1�4$���sw��GdԌˌἾ��M�}X�K������u1C5� yJ���:�lHm�`�k�4)�-�2��#����������?�o/]u\x��#��|2k��5�ռ���    R�^��̬�R"e�T�!�ziK@ֈ��#���
�W~��,E3���ǒØ��5��7�4��Z�����ݡ�9�
[�T����^�tދ+��w�aK�G*h��R)�b9��[�.��sXdr��<oȀ31kkJʋ �SF�C@����yT���aֿ���V�-�&{G0�h-�~.ڝ��K��h����{ �&;x�س�6Uτ��U�v��GD�(�ԩ ma���b2<���f/�F�X8dm��.�iMBn�|�kn�F�@l�-F��l�؄�=,��DLT�qa�̆/��6���SO�+����������-���mS�])�S7;a��f��k��WD.:��Z�V�G�������9� Z&��(��SCƱ�c��}����jD��2�|�6D#� �Ɖ�z7��֎��㴮8�_x�_f�]F��n$����U��.=�t�У��XoK��ڵ���y�E{6�I4d}`���k��A��+m�� ?4V���GD��A����Xa��V�Ҝ���W���÷C�����y´��fI+^GZf�ɡ���T��}~�¸�������\S�-���AU�vv�*��`J�Q���f"ա���J X��1B;2X_F�ZږZ��}���^T����i�����H:�&'���%o�I�^��Tɫ;|������N�.0:�C3Ry��0"�e�e��Yv 7�w�*��&i`8�\`>t5'��<����e��]��m��[X�v��2�X�I@Խ�xS���2G)F��$���}�N_�ƒyxc��_X����t^�J��2v��VՉ�s`����.2�"�&M�g�?�s6K�\_���Ǿ9u�7g�}�߹�.�	�8�T{���Yk�S$!	j�� t����/Z��w��@���:D_�X�I3�]��&�+m<�����7݅m���ĵ�!��Q���Mw#�ߪ�^����cǉ�T�c�����A�MD��ƚ��Ⱥ�R/�}�y�k&��=y�t�GQ[�)��I:�����:��K�n�Ɨa���nU����R�+S�3POS>P�Y-� 6�Z��8�j9�<������C�a�*v{L.܅ֲw��օY�,$�v}�ʨ.ǌ�� ����(m��Y�Q�kH����Te���<�`�]�Zǳ=�z���~����Ɵ=n�_�xul�+<7?�2����ym��C�^`��ು0�O�{,B	�fOm��nѮ��fm2}�m��.l�E !���imMO*�l����7�o��P����a�B����9;����C���CX��N�$�q�Y�Z��&��5B�d��K*���Ki��4��D������� ��V�B�����Y�[���Wv�"�)TW�JǷ)�1���夥~��Y|7r��P�2J�UG��"�)���N�7�f,�rr^�������w�(q!=�%��L�.E<m����v��X�Y�áꋰ�fIfY�D�S�P6�-�O�1Sۏ%�;���Ϻ�T$��Ӳ@��_�#�@WHW��'�qk�YSPPM�).Ƃ�R��P�[�^+c�֝<j�򰧬zy����e����G@��⌣ͿJ��y pC�$z�6�T�����s^1�
z76�'��S��w,�2[j�o2�g�6z�SO�n���k7���@��K,7"�(mMAb����y[;lF�Mﰆ�h��|� s�X+�N�-zg�6�^u����Q��u��4{�s\�=�a��V���$�˚��_�mu�А��b\���f�j���7!�㺯@�]���G*��!}2���#�vjF�c���_%@
�H�H�_*��&K��u9�]o˖#��h�I%���
z��P��|\e�wC!/���i���u����S��l�Uۣ	�Kn�܂�.�,%C���,;uA"����gq�A�AC�w�"�h�H������zK=A{Y�c8Pl�~&�����I��[}�'w�O|f�����0B�V�`���|�U�.h#C���w�.J;Z�w�K��ǳ�����sm39=��|
�oE<�R�Zy���F���.e�mIan��9�+��>�X߸w�V��@�����#/5�2al{Y����!n~Ms��cH>ʿx��ޢ�쎁���μù���9� BM�C YE���H�ro	xm�E�k��nS�u��Or�&��a�
�4{I0r?���-�ԝ9�����;�|	T��H�>�x�()D�3������%«��W�\�%ݻj~�Dx\:iw;:�RJ�ŵpA�;�R����ߦ"G_C��V���
}���|�	)/������\ߤ��3�w`tx�6�5	�nZ�l]�]�v��ҷv�M0��oѤ'nR�i�YI��C�m��@����-���x�dQn  ���|$W�Q�����u�Ծ9���(��vp���O8&�M�J�IH״~X�����.��=t�pU� |�U/�?��K��h%���C)�4�{�3Cm�A��4G��otv���S�`;ϺG�%Sq�jݩY;պo�MG9lՑ�n]�p�?�;��º�}O���aiu�h@�&L����2�L�'�`�3��шj�����n�f�Lwp��\�m�G�.�n"w�F S�PN�J������4�Ѭ���Q����:�o*�JJ�����e'<jG�{_���2\j�e��uml��YT�ݫ�ϭsrȩ=�=��w�ҍ]u/W�Ǖ�s��ƴ	��E)���Tv�N[ke�k֡�,�Eꭁ�A�v���/��z���#���3���G�5���xF J�����qh�1��|�2�~�� �ҷu15�"ʼs+��[�󬂸��[+�������g��q�s`�,0=�x��5\jC��[v1,ѫ��A��I̪�EVL8$DDd���ex*�u`kn96:��d����}ېQ}l��l�t-!6�,�rsF8Q�,���ؼp�`���5�K��
Y~��(1�x���c�'�����Z�'q8tl�uU�h��[8"I�k�[D���ƳBv����و�������_���RgG�'�R 86�T$p�{�-����q^1jpoK�uJ����Jר������@i������N�b'1�DRd9GR�%��	8[���������k$��i�����~�}CM�I��r�h!��ar:�&nfDt7=3��������d�tŅ
ƅ۰�� ��Z�0~6"�5#����]�$}zzÕ3��!P��٭���4�ڠo+/�pq��ߢ���g��\�@'j��G��(t<VVY-k<:�	*��g�7�/2f�sZ0~�ÕF#Lu\��:�S��=�|�JûCm��##{*��tN�%*!u�m���J3��~+.Mu�]���1�#n��Ұ?���F�aG�0�� ��4g�hС>���N|^(�w��:�����aGgi�!
�7*�O9�(��qdK44� 7�.a�#-�������`N��w��aihO�#w���3���p�����B��x���/��c����'�ƴ��ؽ�Z���g�J�]��J���+�T�����v����QG �^E���a��V�j�λJ��F�U��V�v���������x� �tX)p��Z����͢(�*����� ���L>b��5��G������r�P�x�,=No��	}��w集���,$'v��OzW����O��[xl�_3= �֌�O�F��<�*Ͻ����ܞ����;�"��x2z���^	�����+�
���v���=Y�BT1�>��-͊$�Eo������R����,�V���DG�S�׬S$�bp�2O��|���\M-�B�ӄ����+6o�}ko��܂-�[�����ߕ7o<9ꦝh ���Q�\k,�xF��s��Qs�q�)�r�MR���ɢ��N����5�n;�5m�x�F�3�����
�$��1TS���"�m���અ2߬��'	x����c����޷z��:I3y�ū��u�@�f�"�^R���4�Β����4ΰ�+m �    ������u��ΛwR��hD6>d�Yn�9_��٩�1I���)���ew!���Jy�����=�yH�ull���-���Yg�3��mi#���ל�q@k��^1-I��j��ɺ97?/��a섰������1"?,��r��:�+������F�Ο�=_���9�5�N��c�Btd��=�?|_�� �*����ԉ3�
e�&х��_�8'0�ѹʡ�����&ዿɬ�_ܢt`"K�!û��:x�(y�³$^���B �H ��b��\�q�g*KDg\�ZEͿ�/T���?os��:1z���n��=�:x�K��3#tu�O�^�B&9}ɯ<�$q�͑�p"���UbɁ����I�0�0&�$`��j���;#G!��Sֹ2ϡx!��Q̲n��4��͍1�J0�\�͏.b��;���8k��W�1�>�����^q�"��Z}����̈́�f#��	)�1�r�Z�!�G�,b�"�6��N�%�3�����H�=G]�zHΈY�� ��f��pP'�S@	�a����w|4��|ǔ�j�n<%��+�~��s4��^��q�}�Ŋ@<�'�
.2�BR	����dþOw����ȥ��/�B�F�u�h��3g3K�G�*�M�E��Ϊ�رEt�j�5�u��Q�(�K8�������Ce���	�P���\�W��'�qӬ�;֞�?���SԮW%�۽6�E�O�I�yZ�u��wЃz����xл�|������ݯ�!�y^߃��>�Qgm8��E!}^	Cb�*�0�Y�Ɩ%��
��"���l,�޾{��`p�y����7��`@����=��^���\pQ�[���'��C`V��>i�H��J����;�.��<A�nk�%��W��iG�r�M���5?x3�&�esc����SqaAק�N�q����nU%;�~@1^�j����MZz��y[�yZ�)���>�E�����m���~|��!ŰP����o���!ͬڍ��N��R�ɟ&��J,�����"KQ*�b�i�2�u>-��ʢ�-uy�["4w���j
i�����q_t*�GG�n,�>��cG ������l�f)͇G����L}�;��N`R>4H�]�~g[*
f����q9�<^F�6N�mcQ�p!��TH�/h\��$~��Jm�sg��o-�JH;X��"�ب7|dqL�3i����tZ�����rUA�nŜ0����Sg%��]c���_˱y6M��4�E�/4�R*�L���矦Cco�p���];�M�}ouTw�߱h�p�*|�,�����йٳ"���jw���B�b�f�,r|���u��Nt�d>�L*W��<;VRZ�hf�7�´�ʛ�S���ѥ�[ڴ�ԓ�l�l��5�Rqu�a���o�p����=5�ur���"؆3�?��sr� gA�*/4P���.������3CdL��8��:��%�zuO=����q�E�N�{���i�lc���"�h���K�q������J��Qc�pc�|e��$�ӯK��.���+Ĥ^c�?NN�j��{�Tr�zܜ���/T�BA��!j^��[�na�\]�c*h)�-K	W��Vq�a=w�$, ����:�K6��^�a���u�\KPE��+\K��H�ܢ����]C{«��l��ɂ>m025z3���KK(�#)ޤ��F1��z	�������G��)����·TLS�J��TƵ�,�-�j��+~�S���v�v�+��a�&A���=n2h���M	����O^�!\÷,��|OtB�>�*�d��[k����E'a�tQFd14ٰ�����|�	���`	�ł�j0بh0m���/t�HF%?�����Y��XO`E�o��;P�=�<�Y�(-^]��9��x���4�C�VC6\ą�-�� �]�$x��M�,�SL��NƮ��84��Q,I�>�51��_e6�d	s��m3,�@��IC�]4�&!J��_\D�]~�o�#w��uٙ�Ԑ~��T��7XRCdZ
E�aVdw��Arľ�0�!:>���f��c�`�?�׏*�ɒsC�\��*�C�RWn�Ia�a�c�`�
T,~V6���6=ȁ���a�4~��?�ȝ|�uK[�Nd�k�[i�/�`�_�b��2��8u%H���8CE���6E��1r���ƞ<�9e�K9���M���}=�o���п�5����0�,Z.��%����aח����e��d4�:�:��o��4�`ޞ�e;R~��C��ExH��O�@��z�m��Qw`��li� HK�6�`L�#i"ᨵ���JJ���V>�V����F�����p��Qc�'c#p��F����
��d˦P��)3"�2�J��#���i}�H@|}�o<�gP��^�mӒ2b5.4�����g)��,�wЇM/s�4�,#.o��ޒ� ����p��j質�F �E��,�����-�	[�aN1��x.��X>ie�%�*IF�Y�9*�D�~Cr
��3.I����u8Ks^��!̯�σ����/�{�-=��alm61KƂI�ib�U)8��΢��Q�n9�f�]7�S�_�1���?�D������� �7��W��hNҨ!���TMP+��H7�җ�X%
�n���Aj�=�_��X�B��R���V�$�(��E��#	(
|�H��db+U�������'�9�6T��V��T�%�]�Oh�0C�|d �h�<|������o��#�uM�q$�jp�#��H~kf�����o�.�3�m2>m�����m-.�a&�}��iCO�Ƨ�=|��d2ږ'�G�)唟�<���9iU��<��׶�d�m�;-�I��;]7(c�rVm���l��DM�̋<��CY
��<��q������?7b��04&����_S�9:��h?ѳ�Z.��q��|}y�a��߿~��|�9��y޽={}��%��Z`oqf��Ϊ��4�	Nr~�����4�W7�>l���۫3�s��˛�w7~��e� J��n���UK��⻳7�|v��<�~���4d��v۔`Y�Ro)�LV`d���i�\ʅ�|���pry>���/�e�>(/�9w8�0�{[Ø�\�]4~�pK�L��p�ެW�Y*
L_�X�*�6^��:���ûc��u�&��4��]|���V&���we󰭌�sr��3���Lcr8��l������)N:���D�cp�f����]dwp TI�N�</z��a�Esq�}�����K])�|�'ux]��:?j�Z�x�k��3�D����
I��19<z�(�"
9�s���u_��;��ӝ����Xiy4ح_z��ֺ��/���{�g�(q��jX���+v�wN�����h�	Xht��$B�A~���d�:��``f4��Z����}{_1v��:)1�l9I@��1)8c{z��R�E.U�U�<�������o�(5G{X�nƅWg>��7i����:�@S�<a���n����N���R�
G���r~�%g�o�y�Q����ͽk��њ�YC������ǻ�fb��~Ju���T�� 7�r)#>,�q+8���i�pJ-l���}u�z<�9?8�I13w�3��2�g��x��j ��6臢��K��ǻy�u��YW{x[��k&��_�E=nf���rQ�c����~�M������0B,�YZ���2͘k�U�\�� ʍ�x�񛍂j��`��Ka�ә�w;�S��v�yHe���[:�V�Z�<%b�3X��8�ϟaC}� @� J���ܢ���J�j�S�qs�ì�	�'��(�X5��2E��������0��
�m�%K�PQ�h�05���0[��aI���|�#t���m/��ٞ��9�g9��>~;�f~#ЯΨ1F��§ha����ĝ��"w�6�����W�|����(�@/2�2���?���:������/��R )�G&��֮��v\[�ߕJm���~������j��9��� N   r?�<�]r��Qo�����8L+;(��;X�p��e׃��; �h��g?��ث�����#Ŝ}5��G �uR��~��cP{�s��Ξ���㆖��}&杌���Cƪ���)��2�S�s�B��#�~��m��@�?sp�:�Ƽ�1{�w�v���tz[/�P�;��<������@JOɿ�"��4��\ڇR�Pa�m�C��7;#I�Ƭ�����X���4�?���9ق�,g����=�_n沇���W�j3���P��~`oe�C���؃��#��q���xp̭�3w
t�Br��
B�$�ҍT�!o:��H�'-�w e���)��o/?��^��8�ո�>�ݟ��C��a��[Ф�C:0�R�Z��Z�F閮����]�AA�hJ��P-��.ďl<o�ҳ��� +��꛽�QU���݂��%�v��Fq�0O��
�$�T�FD�u�9��A�M��kJ��k���@O]Ab?�K�I�@�e%�n!�sonr�T��$�~ꍰۍe9U\Y�`6ڧ����bCYf�{f!N9y���|�ao�YT쏎��MՉZ6U��֨�L�OI��5I�p�80����U)�Ty���.�嚧�{���&Pň�в�>JǛ)��Mr�	���I�Xw��DKCy�b����"�n9Y�I��.��ƶ�g"��=6�D*�a���e����4j�t��R���*�>��؟��&s��K�}3U�SzB���PC�#ɢX�u��*�����s��҂��7�;p5����>�b�`��H�n`���?',^��Y�~���XY.�2�5�2�de(�hM�^p�[qɾh=m�g!�X 0��-�b\�SZ'/9��ٚiW�����Y�z�Ϯ�k�a>�6��X����k��;�ߣ�������W�B�I����8Td�+�)�웰�B��I@�oB���3�/��r�s�P䭠c���nB��3������T�H���󭪂����
���4��,�wL|�	EB/M�6�}�i�=
6X����Z?�*y�|��c����ϠK$aw%Ve�S�MSp�=���'��=�A9ڻ>*2�s� d�1�x���2��f��4	q_TM!�i d�~"l.K4wJ�O	��#9Ꝺ'!`}��͡���א��kN�6?�i�8��.��VS�]��[��	��̢9f����k��T9:����Eio��@G{���k�ɦ�X�K.(��t��������+n����V��/7����2����*+@_K\��������ߒm���i	��J��2&:���A�;Z9�AO�-��VAi��nO)5[@�U*H��
��������E�g�k^p�����Y�	[dW��؏�?���U�F(��TZ��4M�1����RF]Ky�@㯅!�x�hF�F���y�|���Mvj�_�~tx�߻7����9l;�R�1�*w������qd.��zU��1��32L�o�Ȝ�Ôs��b�����M�r�2�bo�a��.��e��/ۥ��0^����0��Ȍ<:�ٺ�K����{u��F�V�E�,���>c���&�$�:�@L%U5�3�8Ƽ��B�9�3f~�׃?��O�b��       �   n   x�ʻ
�@@�:�
�@��b���Mt��莙�~ݽpRh��Q^c-��+}	s�x����	���W6����%t��b�UѨ�%�V��o���<��>L==ƈxMK$:      �      x��\�r�F]�_��TQ�E�ʖ�U~(���Te�[
R$�H�5����e�E6�]���s��@7J)9�G��D6�~�{�o�s|���<IW�gl"�(�"Y�~M�Ӽd���i���Vܰ9/8�r����;��w��;LdLL٫�\�"go�D|z�^��o�0z���F�.��豨��s=\���q�������c^�n��.�rt*���>9da�~�q�/|F��A��⧌�q5�3 �u`i�,sұHZ��/D���W��2����J��bA_��K�
�5�x�u�Y�~��W+-�f~��݅Չ$ZH�
�x���e�M2/�^^LfiFk��]��w.���,V:��w��'
����\v��q!��E�XN�:Y}���l�'E��YZ�&�R���;Q$|b� Td�|:IZ�2m���I]�* q}�� $�<���z4	�.*h�C�8-{r$�����Q�a1&>Č�oE	@|<.�DL�b�V�y�1�z��/��(&�$��c)X)X!.�b�3<�'i��X�zz-���i9�K�,�KL�Xf�"�����N131S�o�Qѐ%�DM9�Wʅ3�mCi���.Н.'|"�����0azI�~��%�)�g�(��У"�P|�ȋ����X�\X�g��o�b9�q��h�~�(Hg�B��b��1�V"
�4e�r�	�W>VҔu	�E�3��o�yON���[� �؀6d���#9=Z��`�l�l|�����5��9Y�2��Q[w����z�3��j�	�5g?,S��M3�^}�T|$��_W��|hZ��,�5Z;+ҫէR5)Zo?WK0��~&�B�7���ж�do�LTX)k���Q��o��2��δ1e�q�$�[�w�¾�Ήc��Rv��O��S� (��g#L��|wӴ%xN��hͅ��e��:�]m(���ҴJ�*�kc]�y%�'-l�|�q�ɺ>�֯B�>�HO��ҪQz�Y;7,��չe�>OI��ULqo�dU0 ����Z���nu`�v��冰\Ǎ����b���iFOUc�ث�:O�|2dko��DPKq>E�X�
_L�[`��]�����u:�V�e����{޾c�k<�v&{t���SAq����E2O�n��8�C�A�;<<��k���Pѫ�5)ɝ�ٚ|Rn%��Ԁ%�7��h�3��D%����ces2�=V.�"Ǥ�٨�sti�,���A��n�
�OH�7�ש�W�A"���BI��c�3�k�)r.ӪT�g�P2:�+xN�5�+g	��iɬZNA`��E�O1��aa�
ە3#Ǉ3�uG9�],���f��S9f�dD�Í���Yd�&s2�׮��;n���i"�B�@�LjI�s>%��'��OEߛ��/�)��c��݊�Ť�����|E�}q�~�/������y�	E���>���E��,�����(�7�o���߳Q���h��j��ߚl���b�s`���'&�p7��-�@'i���7�Y��2�:�w����2-���kt��ަ�趀75��$��A��6X�v��JDm���/�z��I;g�-�L��!y؅2J9���R�����oXNCf��fA;�L�������O���O�ܷqsQ�;�r[�僙ny�O�e�\�c�ĞR _U��)�ӧ���F0mWtH�F%�B�Vo<n���F��C� o�y2� YO�u���V�-R&ԗmN�,Jm`�����Ⱦ03s/+#���И�z�rѢ��*��Ĉ���֔����M�+I�� fvMh�{/�-����$����3z��۰�^�����a}l�6��:G��z����f��L�B|�{P�y�����ۓ��Z�� ��t7�������_v�1	;;�P
�ߩ���[>n�d���=��ܺ�i�됌�t�[FsU��({_���}��E�\��9ǐK�5���&y�N�;��!� �>~�k�#kc�B�@'��n\��!�k��P2��\�eCoK
�V;��mU�`�q�D ����Ra}Ղ����t|�(}�Br�@�Z������q�<Q{�o�.��>�NN��B"��<S!f9f�4�4�c�h�M�ZD�v��/²u�2���wW�^����%��2%�feJڭ�Ҩ-�Ti�&������/:�s�p���NO~󙖯ד?|+K6*Y/v�Y��Z�Ui6[���p&G�p�����*yQZL�l��{jMče5�*�Jh���]�)d����g��/�-h��!s�������^��v.
1Q`�WK�ʛ U����\;��@�y�Ȫ�ÄL���-�I
�,��/��B����|ƛ)�s)��V3=?Y��i�����!�$0Te:�
	�1���D�3pn�u�!{�λ���xo�V��YuIu%m���"\P^�Po(%e��!��0�������c�0����@gXG7riE���6:�滴�{$(1<t�~D�B}嵈��ܸBGɉ/��d�|��Z1a�;��kCy�Գ-�UښtXݽѿ�=��Ȯ��
��7�q�[\�A>�P����آ; �p�k�& ��0H���R+h����|�*:Y��*�{;굪�Th�y�1�d�]`׈����u�Y���*��h
EX�Q��n�$���w��q���&j��`G�]^���ֹC`6�[��6�g��s��u���z�~]�	T�t���@b��rH��0~|d^\t@sԓٴ�[[/��X鼍<���u�nɪn�����WՉ��T++��/S��-c�7`k`E��n�[L/E���l��s�V{�b���V5�,�PV�vkrG��y��m��������fKs�a����B6_�Z��<�*�[��+՘,����M�]���8�	��FH����a���`?��J�?W�_3��;�g�0[�k�;��u^7{r�
��1"ôT��岬��y������.h��r�sD-���� Ԡ%0���4ʘ�=��TL��L\?\wn�LgM) ��>�?����2����o{��M�^[�I�ŝzmo<Z�m#�1�S�=�gʿ�B3������k
d CI�ߧ_��PoN:���"�hН=�Nx(eOiOi/7��P���v���\�.�\p���sg	?�21u��Fe`t"C��_ՑG�ɂ������6%UPڶ�O�B<��)�=p�	�RBr\����r� �Y�>�S�u�@+�}��CV�S�τ*� W@�E���PA߃���;d��D,8��r�m/��ܸH�D�~?��)ʔ�.�N�V��:�zcdv�2EX��p�p-��6��3^��M:N+v��8�x��h1E_9+jx~kޕ�x��=���<R��@S����j7jB��V� �Y�-ĥ(�\�yd1���<�^{Ɍ3��N�����cg'��G��wp���;��D�F���%��*r���Ά6�C�O��ɩ��35�[X䢿!���:�z������F���hx&O������z�>���X���t�������/�#g#��V��m�{�v�7|�q�6�Xs��Ѫ�1��Pyvd�|#̨���@�q��=M��ܗ��D��[Ǫ�������u6.������D�=5��*ZnK�c���*./����_bz�V�χtC�k�ߎ.j⩌�:��ב�:a;nJ�y
YG�:<�R�L�d9Sۀ���BHD�x��,S������2r�{��f�ۖ�Jw8�n�Vd�܁;�BA��<P틽�wk�w<��y�s��e��{��vN6!K�9�տ�GeJi��Fjp�ʮ��D�����ţ0�˨��
`� r�N�O�rv�e�?d�K�����6�:ێlJ�����@�D�1#�9�m�B���;I�U����;}לA~,wד~�ẇ��N��?U#V��^���<��=8ծ�0�#>_�� &���F$�\�}G�D��V&鱈6��ӯ�Д=�B0��)�.�u��0*`�,��/�e��[�A���C�V�٩o��z��.ra'�F.u-�1P��� �  ��U{z�����y�_)��5�Z+G����ʣ���A�P��J� )�b�������*o��ǁJ�t.=;ҕS~-��i�yM�)�x ڴ����9>�����Ou/pB�q��Y�*&���*x����at��%�pˌN����넒��Q8{A�k��"�y)��y�`���ď�
����$L�$d��&�T��kŔĩ8`�R?Id����A+�p��V�NU���p���tU�YԮ�8�sˁ �_6�l�2��
>;k�>��!��I�J��ͥdr�֑�;j���� ���%d� wVvs��p=:���K9��׶/�w�ܪ��~u�:sKM�4�2�9:�k4Vg�'������c;����ux��O���S���
�̇=��͟�={�?��      �   j  x��UK��0['��r��.s�s4 䚚��B���8�K�#�_܅��:��p
jE@1r�FjI�J�J佸��1�
9۠S��2��q�c�q,
�0�������j�Gg� �ɴ,�-�j9)=T@�;[���c1�8PW�WP�{��]���^�uڅ�������A��A�y��C��˦�]h�l)��/T�᝷�������pJ<#�����̭8Uz�q��(��:���q s�~K��op<��i�&}��A��u���:`JaN1)��Y냂��Z�ޡ�QUap�ki�5�3���+iX�� ���l���R�,1[xg�w3�z߉�f��2��xw~~�|0g�7�u�;##�;~v�y���{2�u�Nקy2Af��dc[
�*�����@� 8���z�N��x�%��
�`�8���;wv���y&_�ne-Τ�هed3�|����<���+���f�m�1�^�܁;��7���?P�����Oם�{�r�Әe�	+����+�����O|$�J����Z�E�h����SP��g,`&H�JЄ�z��zy���_��0��+�bRŢ�}�������������x_�      �   �  x����n�0��'O�'�������*
H�����&%I�����'蓭��T��%�@����{��(���LQ�s5Or��y��
ݦI��
�f��ꕾ�w�j}������8"����G.0f�����($j~���?�"]y�.a���ԯW�[�n���F�v��!o����!�;����z���~�`�q���d���|��������<��/ ���Q3�r@�%����O.~�= ��T|�<�����F������]*]�$��U��]��3�m���勵7�2�.�~��W��>P_��Y�n����:xط������Ey��J��e��2Y 3�uV� ����p�����2."���r���FZ�Ł������h��@��T�d �۸�vL3 ����eO��P� ��������A/��p}a ����k:�L�!�gV          �  x����n�0���S�*��(J��	��v1��ب�찧m9ْ�h��}�I����O?��8�������uhƾ�5ա���04۩~B���g�B"��7 .|���m�?��N�z��ygny����3/��}{h���94�l9��a�Pک�a����Ê�E#^D �@Y�Uͯf{�Ġ�j_����m�]A�MV��N"g(��CԟN�s� �7�A�eeک�����T̮�}���h��sw�h�jߏծ���O�!"�䇁.�����;4��8���&!�7Ί�U�d}�Gq��c�����54o�vlU�5�1!�9�
.C���g�;D���%�L��Į8�ѻ�����Ͱ0�[3�����Y�<��]�i���X�QE6��i<W����K[��]�Y�m�9���TWc�o��t�U���-���q��cmXo�;�g�;lb˹��
%@*�G�\Ѿ�1�$�0A0�g#�/L�h0�XZ�*_q'�N���*6|q�)��D���~��{���=�L�|�d��M=�(^���)*�T���LЦ�y��Nڶ�D��W<���v�z=e|���siU=DC�0���G��%�,'�\c��#_���E�:�lf�))�)��Ψ.�B����CI<IB4�����>/$A�M��{�j���*&���2�B-|Ƴn�V������NI^ك��;zܞ�^�▿��kh�P#�xY�v�s�����l6��o      �   Q   x��K
�0�uz��N�&� Z�Mr�ˁ�d����PN3�w4X�;W�~X�6�a��Q�'`���dz*��6|L)�4o�      �       x�35�4D@.Sc e��B�@�+F��� ij�      �   "  x��W�r�:}_���V��z4����)�>t��`%��S�d�|W?�?ֽE690�B0��u��wx$�`��1����Eu���E�������D���㜄p�ۭ���'"8v!8�^m�R�] ��h-s|��|��[����$��COp�Q��6���T7�������F���D��X��������t�ʵ{�UM���x��kZV���{��"<?h��B�#��I�I�]Of��޾��G�+S�}�qq�xp+�Q����<K�<�f�و��G��].��K�����y��BGo{���=�[}���*�n����hں��y�nՓ. AH2���C��.����O��
 ?�pfA�������e	��L7t�lU]T���R��	T)�@^�ۢ)�ޟT�k�Q]k��# @@:Xr�{�M��(��G�8ػ/ WEl55{n���ͭ�+]׿~�Z�`rq ����d���.�#p�V9�36+�Y�hY��1��#u�l�<#A��@��s}<��[��E]�who���p�3�Rg��b���@Wl	���wuu�b+@Y�t��x�i Gy	���Нr�_�>��.��-��c����"b��l��Z�K@�%�8�0&V��RpH��]�̱�od�_���~������A�D����w>��.w���@���r@{����A��ãI ��@�u�#v��$��*��M����N�4e��s9�p� ��,���أ���?z�kn�H�� v�8�m�حK��uQ��b���]EF���=Op�ᩏXΪ݆V���+����G��|�Ҕ���,�w�P�$Aˇr���Խ�����{]@ow��t�лd���U��$z�ft1�������Ih������j��P��aO$�o���m�(����<�_�z���Jq����ީaA��e�f�6�FFg���=��|tgx�p�7� 8�e�}wppT�����5k�۝P�Qa3H������e�W\�=�����f ��V	i4~�e$(@lC&�s��8~��      �      x������ � �         �   x�=Q��0;���@٥��Q,T_��bsk��?m�C�&[���,q
����Z}�K��qst�Etb�g	�j^'I�+���Rp>��Ԑ
��
�(5�Q��S:�#n�f��"����� �A�G�K,O�'#J��U�	�Q�U@� � 'ح�W���|���ߩ*[([([0	(	(	(	 �������X�      �   1   x�3��/H-JL�/�2�tL����,.���9�R����rR�b���� b��      �   �   x��й1���X�s95Aݖh�OR%U����J*�����l�]��N�=B7�h���x�X�c�6Qz���x���]�G.;~��o��O3!F�'�z:��^y��@�`����A��JD[W_ʵ�k�U2���u\+7����&�\+      �   �   x�}�1�0Eg�\�*���]�(%U=]*��E�{��M�R`��s��v}�y$%m���HZ�t��M>����S�S��q�d�����u�Ǻ�D�mq��x�$H��Ws8�8{���D�^�e�Oֺi/�c����H��.}�
|�]!�K�S�x�!�N�[l      �      x������ � �      �   5  x���Mj�0���)|�	��9g�Ɩ�i ��hO_�@�n�����(~Ow�RNm��T��P�Bt1�b��8:3��7h'eTqP�K�ɘ��I$cb$�ҁ���i-[�gc��8S�7B�������F�G=�@	(�DX8�˜!3��R08	��;Kj9yN-�^�O�}Kk�J����4b�Mp���Aa�B���C��\�uΨC�uޔUE�<6�cG�}ˆ�>��BR
y(8�)���G�f�9��y��))���y�y[u��Sr�{�I�m_����FmM�!�H���;�[�u�7nF~�      �   �   x�UPKj�@]�O��u��2�E�H�
��ٌ�ј���wʢ��X���xz�j��0QH����!���g�L[K���j��M>�ãۨ��#t�A��ᄝѨ��F��H�r-�F-�#����=:c
�V���#��,����!SklXϧ�������=G%qB[�.io���K�ˣz�V8�^z�3C@NET_��a��"}���>�i�GIǂ��81D����yiס-��XUU��yx1     