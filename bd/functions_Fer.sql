/**
*	PROCEDIMIENTOS PARA LOG IN
*
**/
DROP FUNCTION user_auth ( VARCHAR, VARCHAR );
	
	CREATE OR REPLACE FUNCTION user_auth 
		(_user_name VARCHAR,
		_password VARCHAR ) 
		RETURNS SETOF "users" 
	AS $$ 
		SELECT
			* 
		FROM
			public."users" 
		WHERE
			user_name = _user_name 
			AND password = _password;
	$$ LANGUAGE SQL 

/**
*PROCEDIMIENTOS PARA MODULO DE USUARIOS
*
**/
--SELECT prueba_usuarios ( 'fernando', 'contraseña' );

DROP FUNCTION vud_users_record();
		
		CREATE OR REPLACE FUNCTION vud_users_record
			(out id integer,
			out user_name varchar,
			out employee_number integer,
			out user_register integer,
			out rol varchar,
			out description varchar,
			out id_rol integer)
			RETURNS SETOF record 
	AS $$ 
		SELECT
			a.id,
			a.user_name,
			a.employee_number,
			a.user_register,
			b.description,
			a.description,
			b.id
		FROM
			public. "users" as a
		INNER JOIN public. "role" as b
		ON a.rol = b.id
		where a.status = 1 order by a.id asc;
	$$ LANGUAGE SQL
--SELECT * from vud_users_record()

--procedimiento para obtener todos los ROLES

DROP FUNCTION vud_role()
	
	CREATE OR REPLACE FUNCTION vud_role()
		RETURNS SETOF "role"
	AS $$ 
		SELECT
			*
		FROM
			public. "role";
	$$ LANGUAGE SQL

select * from vud_role()


--insertar un usuario

DROP FUNCTION vud_insert_user( character varying, integer, character varying, integer, integer, character varying);

CREATE OR REPLACE FUNCTION vud_insert_user
		(_user_name VARCHAR,
			_employee_number INTEGER,
			_description VARCHAR,
			_rol INTEGER,
			_user_register INTEGER,
		_password VARCHAR ) 
		RETURNS void
	AS $$ 
	
		INSERT INTO 
	public."users"
		("user_name",
		"employee_number",
		"description",
		"rol", 
		"user_register",
		"date_register",
		"status",
		"password") 
	VALUES (_user_name,
		_employee_number,
		_description,
		_rol,
		_user_register,
		'now()',
		1,
		_password);
		
	$$ LANGUAGE SQL 

-- UPDATE USER

DROP FUNCTION vud_update_user(integer, varchar,integer,varchar,integer,integer,varchar);

select vud_update_user(9,'vianey2',111111,'descripcion vianey',2,666666,'prueba');

SELECT vud_update_user(6,'PEDRO perez',999888,'descripcion de pedro',1,9876,'');

CREATE OR REPLACE FUNCTION vud_update_user
		(_id_user INTEGER,
		_user_name VARCHAR,
		_employee_number INTEGER,
		_description VARCHAR,
		_rol INTEGER,
		_user_register INTEGER,
		_password VARCHAR) 
		RETURNS boolean 
	AS $$ 
	BEGIN
	IF (_password = '') THEN
		update 
		public."users"
	SET		
		user_name = _user_name,
		employee_number = _employee_number,
		description = _description,
		rol = _rol, 
		user_register = _user_register
	WHERE 
		id = _id_user;
	ELSE 
		update 
		public."users"
	SET		
		user_name = _user_name,
		employee_number = _employee_number,
		description = _description,
		rol = _rol, 
		user_register = _user_register,
		password = _password
	WHERE 
		id = _id_user;
	END IF;
	
	RETURN true;
	END
	$$ LANGUAGE 'plpgsql'; 

--ELIMINAR USUARIO
DROP FUNCTION vud_delete_user(integer);

CREATE OR REPLACE FUNCTION vud_delete_user(
	_id_user integer)
RETURNS boolean
AS $$

BEGIN

UPDATE public."users" SET status = 0 WHERE "id" = _id_user;
return true;

END
$$ LANGUAGE 'plpgsql';


/**
*	PROCEDIMIENTOS PARA MODULO DE LOG
*
**/

/**
*funcion para insertar en el log
**/
DROP FUNCTION vud_log_insert(integer, character varying)

CREATE OR REPLACE FUNCTION vud_log_insert(
	_id_user integer,
	_description character varying)
RETURNS void
AS $$
BEGIN
	INSERT INTO 
	public."log"(id_user, description,date_time)
	VALUES (_id_user,_description ,NOW());
END
$$
LANGUAGE 'plpgsql'
	

/**
*	PROCEDIMIENTOS PARA MODULO DE CATALO DE TRAMITES
*
**/

--consulta para consultar toda la informacion del catalogo tramites
DROP FUNCTION vud_record_transact();
CREATE OR REPLACE FUNCTION vud_record_transact(
	out id integer,
	out code integer,
	out description varchar,
	out legal_foundation varchar,
	out total_modality bigint,
	out id_transact integer,
	out days integer,
	out type_days char)
RETURNS SETOF RECORD
AS $$

	SELECT
		a.id,
		a.code,
		a.description,
		a.legal_foundation,
		count(b.id_transact) as total_modality,
		b.id_transact,
		a.days,	
		a.type_days
	FROM
		cat_transact  as a
	left JOIN 
		cat_modality as b
	on  a.code = b.id_transact
	WHERE
		a.status = 1
	GROUP BY b.id_transact,a.id,a.code,a.description,a.legal_foundation
	order by a.id asc;

$$ LANGUAGE SQL

SELECT * FROM vud_record_transact(); 

--procedimiento almacenado para insertar nuevo tramite
DROP FUNCTION vud_insert_transact(integer, character varying, character varying);

CREATE OR REPLACE FUNCTION vud_insert_transact(
	out _id_code integer,
	_code integer,
	_description character varying,
	_legal_foundation character varying,
	_days_transact integer,
	_type_days_transact char)

RETURNS SETOF integer
AS $$
DECLARE _id_transact integer;
BEGIN

	INSERT INTO public.cat_transact(
		code, 
		description, 
		legal_foundation, 
		status,
		days,
		type_days)
	VALUES (_code,
			_description,
			_legal_foundation,
			1,
			_days_transact,
			_type_days_transact)
	
	RETURNING ID into _id_transact;

	RETURN QUERY SELECT code from public.cat_transact where id = _id_transact;

END
$$
LANGUAGE 'plpgsql';
--funcion par actualizar el tramite
DROP FUNCTION vud_update_transact(integer, character varying, character varying, integer, char);

CREATE OR REPLACE FUNCTION vud_update_transact(
	_id integer,
	_description character varying,
	_legal_foundation character varying,
	_days integer,
	_type_days char)
RETURNS VOID
AS $$
BEGIN 
	UPDATE public.cat_transact
	SET  description=_description, legal_foundation=_legal_foundation, days = _days, type_days = _type_days
	WHERE id = _id;
END
$$
LANGUAGE 'plpgsql';

--funcion para actualizar el tramite y las relaciones
DROP FUNCTION vud_eliminate_relation_transact(integer)

CREATE OR REPLACE FUNCTION vud_eliminate_relation_transact(
	_id_transact integer)
RETURNS VOID
AS $$
BEGIN
	DELETE FROM cat_transact_requirement where id_transact = _id_transact;
END
$$
LANGUAGE 'plpgsql'

SELECT vud_eliminate_relation_transact();

-- crear relacion tramite-requisito

CREATE OR REPLACE FUNCTION vud_relation_transaction_requirement(
	_id_transact integer,
	_id_requirement integer)
RETURNS void
AS $$
BEGIN

INSERT INTO public.cat_transact_requirement(
	id_transact, 
	id_requirement)
VALUES (_id_transact,_id_requirement);

END
$$
LANGUAGE 'plpgsql'

SELECT vud_relation_transaction_requirement()

--eliminar tramite
DROP FUNCTION vud_delete_transact(integer)
CREATE OR REPLACE FUNCTION vud_delete_transact(
	_id integer)

RETURNS VOID
AS $$
BEGIN 
	UPDATE public.cat_transact
	SET status = 0
	WHERE id = _id;

	UPDATE public.cat_modality
	SET status =0
	where id_transact = _id;
END
$$
LANGUAGE 'plpgsql';

--Se muestra la informacion  de los requisitos

CREATE OR REPLACE FUNCTION vud_record_requirement()
	RETURNS SETOF cat_requirement
AS $$
BEGIN 
	RETURN QUERY select * from public.cat_requirement where status = 1 order by id asc;
END
$$
LANGUAGE 'plpgsql'

select * from vud_record_requirement();

--Se crean los nuevos requisitos
DROP FUNCTION vud_create_requirement();

CREATE OR REPLACE FUNCTION vud_create_requirement(_description character varying)
	RETURNS void
AS $$
BEGIN
	INSERT INTO public.cat_requirement(description,status)
	VALUES (_description,1);
END
$$
LANGUAGE 'plpgsql'

--funcion para actualizar requisito

CREATE OR REPLACE FUNCTION vud_update_requirement(_id integer, _description character varying)
	RETURNS void
AS $$
BEGIN

	UPDATE public.cat_requirement
	SET  description=_description
	WHERE id = _id;
END
$$
LANGUAGE 'plpgsql'

--funcion para eliminar requisito
CREATE OR REPLACE FUNCTION vud_delete_requirement(_id integer)
	RETURNS void
AS $$
BEGIN
	UPDATE public.cat_requirement
	SET  status = 0
	WHERE id = _id;
END
$$
LANGUAGE 'plpgsql'


-- funcion para traer todas las modalidades asignadas a un tramite

drop function vud_modality_record();

CREATE OR REPLACE FUNCTION vud_modality_record(
	out id integer,  
	out code integer,  
	out description character varying,  
	out legal_foundation	character varying,  
	out id_transact integer,  
	out code_transact integer, 
	out description_transact character varying,
	out days integer,
	out type_days char)
	RETURNS SETOF record
	
AS $$
BEGIN
	RETURN QUERY select 
	a.id,
	a.code,
	a.description,
	a.legal_foundation,
	b.id,
	b.code,
	b.description,
	a.days,
	a.type_days
from 
	cat_modality as a
LEFT JOIN 
	cat_transact as b
on
	a.id_transact = b.code
WHERE 
	a.status = 1
order by a.id asc;

END
$$
LANGUAGE 'plpgsql'

-- query para agregar la modalidad
DROP FUNCTION vud_create_modality(integer, character varying, character varying, integer,integer, char);

CREATE OR REPLACE FUNCTION vud_create_modality(out code_modality integer,
	_code integer,
	_description character varying,
	_legal_foundation character varying,
	_id_transact integer,
	_days integer,
	_type_days char)
RETURNS SETOF integer
AS $$
DECLARE code_inserted_modality integer;
BEGIN 
	INSERT INTO public.cat_modality(
	code, 
	description, 
	legal_foundation, 
	id_transact,
	status,
	days,
	type_days)
	VALUES (_code, 
			_description, 
			_legal_foundation, 
			_id_transact,
			1,
			_days,
			_type_days)
	RETURNING ID into code_inserted_modality;

	RETURN QUERY SELECT code from public.cat_modality where id = code_inserted_modality;
END 
$$
LANGUAGE 'plpgsql'

select * from vud_create_modality(1444,'descripcion de la modalidad fn', 'legal foundation fn',13)

--funcion para crear la relacion de requisitos y modalidades
CREATE OR REPLACE FUNCTION vud_relation_modality_requirement(
	_id_modality integer,
	_id_requirement integer)
RETURNS void
AS $$
BEGIN

INSERT INTO public.cat_modality_requirement(id_modality,id_requirement)
VALUES (_id_modality, _id_requirement);

END
$$
LANGUAGE 'plpgsql'

--funcion para actualizar las modalidades
DROP FUNCTION vud_update_modality(integer, character varying, character varying)

CREATE OR REPLACE FUNCTION vud_update_modality(
	_id integer,
	_description character varying,
	_legal_foundation character varying,
	_days integer,
	_type_days char)

RETURNS void
AS $$
BEGIN

UPDATE public.cat_modality
	SET   description=_description, legal_foundation=_legal_foundation, days=_days, type_days = _type_days
	WHERE id = _id;

END
$$
LANGUAGE 'plpgsql'

--funcion para eliminar las relaciones existentes de la modalidad

CREATE OR REPLACE FUNCTION vud_eliminate_relation_modality(
	_id integer)
RETURNS void
AS $$
BEGIN
DELETE FROM cat_modality_requirement WHERE id_modality = _id;
END
$$
LANGUAGE 'plpgsql'

SELECT vud_eliminate_relation_modality(22)


--funcion para eliminar la modalidad de manera logica

CREATE OR REPLACE FUNCTION vud_delete_modality(
	_id integer)

RETURNS void
AS $$
BEGIN

UPDATE public.cat_modality
	SET   status = 0
	WHERE id = _id;

END
$$
LANGUAGE 'plpgsql'

/**
*
*Funciones para el controlador de las solicitudes.
*
*
*
**/

--FUNCION PARA TRAER TODA LA INOFMRACION DE LA TABLA CAT_TRANSACT
DROP FUNCTION vud_transact_record()

CREATE FUNCTION vud_transact_record(out code integer, out description varchar)
RETURNS SETOF record
AS $$
BEGIN
	RETURN QUERY SELECT a.code,
						a.description
				FROM public.cat_transact as a 
				WHERE status = 1 
				order by id desc;
END
$$
LANGUAGE 'plpgsql'




CREATE FUNCTION vud_transact_record(out code integer, out description varchar)
RETURNS SETOF record
AS $$
BEGIN
	RETURN QUERY SELECT a.code,
					CONCAT(a.code,', ',a.description) as description
				FROM public.cat_transact as a 
				WHERE status = 1 
				order by id desc;
END
$$
LANGUAGE 'plpgsql'



SELECT * from  vud_transact_record();

--mostrar las modalidades de acuerdo a un tramite
DROP FUNCTION vud_transact_record_modality(integer);

CREATE FUNCTION vud_transact_record_modality(
	_code integer)
RETURNS SETOF cat_modality
AS $$
BEGIN
	RETURN QUERY SELECT * FROM public.cat_modality where id_transact = _code order by id asc;
END
$$
LANGUAGE 'plpgsql'

SELECT * FROM vud_transact_record_modality();

--mostrar los requisitos de un tramite

DROP FUNCTION vud_transact_requeriment(integer, integer, character varying, character varying);

CREATE FUNCTION vud_transact_requeriment(
	_id_code integer, 
	out id integer, 
	out description character varying,
	out legal_foundation character varying)
RETURNS SETOF record
AS $$
BEGIN
	
	RETURN QUERY select 
		a.id,
		a.description ,
		c.legal_foundation
	from 
		public.cat_requirement as a
	inner join public.cat_transact_requirement as b
	on a.id = b.id_requirement
	inner join public.cat_transact as c
	on c.code = b.id_transact
	where c.code = _id_code;

END
$$
LANGUAGE 'plpgsql'

SELECT * FROM vud_transact_requeriment(integer);

--mostrar los requisitos de una modalidad de acuerdo a la clave

DROP FUNCTION vud_modality_data(integer);

CREATE OR REPLACE FUNCTION vud_modality_data(
	_code_modality integer
	out id integer,
	out description character varying,
	out legal_foundation character varying)
RETURNS SETOF cat_requirement
AS $$
BEGIN 
	RETURN QUERY select 
		a.id,
		a.description,
		c.legal_foundation

	from 
		public.cat_requirement as a 
	LEFT JOIN
		public.cat_modality_requirement as b
	on 
		a.id = b.id_requirement
	LEFT join 
		public.cat_modality as c
	on
		b.id_modality = c.code
	where c.code = _code_modality ;
END
$$
LANGUAGE 'plpgsql'

-- consulta para ver permisos por usuario

DROP FUNCTION vud_users_permisions(integer, character varying, integer, smallint,smallint,smallint,smallint,integer)

CREATE OR REPLACE FUNCTION vud_users_permisions(
	_id_user integer,
	out id integer,
	out name character varying,
	out id_module integer,
	out id_user integer,
	out read_permission smallint,
	out write_permission smallint,
	out delete_permission smallint,
	out update_permission smallint
)
RETURNS SETOF RECORD
AS $$
BEGIN
RETURN QUERY SELECT 
	a.id,
	a.name,
	b.id_module,
	b.id_user,
	b.read_permission,
	b.write_permission,
	b.delete_permission,
	b.update_permission
FROM
	PUBLIC.modules AS A 
	LEFT JOIN PUBLIC.permission AS b ON a.id = b.id_module

WHERE
	b.id_user = _id_user
order by a.id;
END
$$
LANGUAGE 'plpgsql'

END
$$
LANGUAGE 'plpgsql'

SELECT * FROM vud_users_permisions(1)

-- agregar permisos por usuario

CREATE OR REPLACE FUNCTION vud_insert_user_permission(
	_id_user integer,
	_id_module integer,
	_read_permission smallint,
	_write_permission smallint,
	_delete_permission smallint,
	_update_permission smallint)
RETURNS void
AS $$
BEGIN

INSERT INTO public.permission(
	id_user,
	id_module, 
	read_permission, 
	write_permission, 
	delete_permission, 
	update_permission)
VALUES (
	_id_user, 
	_id_module, 
	_read_permission, 
	_write_permission, 
	_delete_permission, 
	_update_permission);

END
$$
LANGUAGE 'plpgsql'

--

CREATE OR REPLACE FUNCTION vud_data_vulnerable_group()
RETURNS SETOF vulnerable_group
AS $$
BEGIN

RETURN QUERY SELECT * FROM vulnerable_group where status = 1 order by id asc;

END
$$
LANGUAGE 'plpgsql'

select * from vud_data_vulnerable_group();



-- FUNCION PARA INSERTAR LA INFORMACION DEL LAS SOLICITUDES ENTRANTES

--funcion para insertar la direccion del interesado
DROP FUNCTION vud_request_address(character varying, character varying, integer, integer, character varying,character varying)

CREATE OR REPLACE FUNCTION vud_request_address(
	_postal_code character varying,
	_colony character varying,
	_ext_number character varying,
	_int_number character varying,
	_street character varying,
	_delegation character varying)

RETURNS SETOF integer
AS $$
DECLARE _id_request_address integer;
BEGIN

INSERT INTO public.address(
		postal_code, 
		colony, 
		ext_number, 
		int_number, 
		street,
		delegation)
	
VALUES (_postal_code,
		_colony,
		_ext_number,
		_int_number,
		_street,
		_delegation)

RETURNING ID into _id_request_address;
RETURN QUERY SELECT _id_request_address;

END
$$
LANGUAGE 'plpgsql'


--funcion para insertar la informacion del interesado
DROP FUNCTION vud_request_interested(character varying,character varying,character varying,character varying,integer)

CREATE OR REPLACE FUNCTION vud_request_interested(
	_name character varying, 
	_last_name character varying, 
	_e_mail character varying, 
	_phone character varying,
	_type_person integer)
RETURNS SETOF integer
AS $$
DECLARE id_request_interested integer;
BEGIN
INSERT INTO public.interested(
	name, 
	last_name, 
	e_mail, 
	phone,
	type_person)
	VALUES (_name, 
			_last_name, 
			_e_mail, 
			_phone,
			_type_person)

RETURNING ID into id_request_interested;
RETURN QUERY SELECT id_request_interested;

END
$$
LANGUAGE 'plpgsql'



-- FUNCION PARA INSERTAR LA SOLICITUD

DROP FUNCTION vud_request_data(
integer,
character varying,
character varying,
integer,
integer,
integer,
integer,
integer,
character varying,
character varying,
integer,
integer,
date,
integer)

CREATE OR REPLACE FUNCTION vud_request_data(
	_id_user integer, 
	_folio character varying,
	_observations character varying, 
	_id_status integer,
	_id_address integer, 
	_id_interested integer, 
	_id_transact integer, 
	_id_vulnerable_group integer, 
	_request_turn character varying, 
	_type_work character varying, 
	_means_request integer, 
	_delivery integer,
	_date_commitment date,
	_id_modality integer)

RETURNS SETOF character varying
AS $$
DECLARE id_request_created integer;
BEGIN

INSERT INTO public.request( id_user,
							folio,
							date_creation, 
							observations, 
							id_status, 
							id_address, 
							id_interested, 
							id_transact, 
							id_vulnerable_group, 
							request_turn, 
							type_work, 
							means_request, 
							delivery,
							date_commitment,
							flag,
							id_modality)
VALUES (_id_user,
		_folio, 
		NOW(), 
		_observations, 
		_id_status, 
		_id_address, 
		_id_interested, 
		_id_transact, 
		_id_vulnerable_group, 
		_request_turn, 
		_type_work, 
		_means_request, 
		_delivery,
		_date_commitment,
		1,
		_id_modality)
RETURNING ID into id_request_created;

	RETURN QUERY select folio from public.request where id = id_request_created;

END
$$
LANGUAGE 'plpgsql'


--funcion para traer el numero de dias naturales o habiles de la fecha limite

CREATE OR REPLACE FUNCTION vud_transact_days(
	_code integer,
	out days integer,
	out type_days char)
	
RETURNS SETOF RECORD
AS $$
BEGIN
RETURN QUERY SELECT 
				a.days,
				a.type_days
			FROM 
				cat_transact as a
			where
				a.code = _code;
				
END
$$
LANGUAGE 'plpgsql'




--funcion para traer el numero de dias naturales o habiles de la fehca limite

DROP FUNCTION vud_modality_days(smallint)

CREATE OR REPLACE FUNCTION vud_modality_days(
    _code integer,
    out days integer,
    out type_days char)
RETURNS SETOF RECORD
AS $$
BEGIN
RETURN QUERY SELECT
                a.days,
                a.type_days
            FROM
                cat_modality as a
            where
                a.code = _code;

END
$$
LANGUAGE 'plpgsql'

--funcion para mostrar todo el listado de las solicitudes creadas

DROP FUNCTION vud_request_record()

CREATE OR REPLACE FUNCTION vud_request_record(
out id integer,
out folio character varying,
out id_user integer,
out id_status integer,
out description_status character varying,
out date_creation text,
out date_commitment text,
out code integer,
out description character varying,
out modality_desc character varying,
out id_turn integer,
out id_dictum integer,
out last_status integer,
out settlement_number character varying,
out id_address integer,
out id_interested integer,
out citizen_dictum date,
out folio_dictum character varying)

RETURNS SETOF record
AS $$
BEGIN

RETURN QUERY SELECT 
	a.id,
	a.folio,
	a.id_user,
	d.id as id_status,
	d.description as description_status,
	to_char(a.date_creation,'dd-mm-yyyy' ),
	to_char(a.date_commitment,'dd-mm-yyyy' ),
	b.code,
	b.description,
	c.description as modality_desc,
	e.id as id_turn,
	f.id as id_dictum,
	a.last_status,
	a.settlement_number,
	a.id_address,
	a.id_interested,
	f.citizen_dictum,
	f.folio_dictum

FROM 
	public.request as a
left join 
	public.cat_transact as b
on
	a.id_transact = b.code
left join 
	public.cat_modality as c
on 
	a.id_modality = c.code

left join 
	public.cat_status as d
on
	d.id = a.id_status
left join 
	public.tracing_turn as e
on
	e.id_request = a.id
left join
	public.tracing_dictum as f
on
	f.id_request = a.id
WHERE
	flag = 1
order by id desc;

END
$$
LANGUAGE 'plpgsql'


select * from vud_request_record()



--funcion para traer los estatus

CREATE OR REPLACE FUNCTION vud_status_list()
RETURNS SETOF public.cat_status
AS $$
BEGIN
RETURN QUERY select 
				* 
			from 
				public.cat_status;
END
$$
LANGUAGE 'plpgsql'

SELECT * FROM vud_status_list()


--funcion para sertar el turno de la solicitud
DROP FUNCTION vud_create_turn_request (integer, date, integer, character varying,integer)

CREATE OR REPLACE FUNCTION  vud_create_turn_request(
	_id_request integer, 
	_date_turn date, 
	_review_area integer, 
	_observations character varying,
	_id_status integer)

RETURNS void
AS $$
BEGIN
INSERT INTO public.tracing_turn(
	id_request, 
	date_turn, 
	review_area, 
	observations)
VALUES (_id_request,
		_date_turn,
		_review_area,
		_observations);

UPDATE 
	public.request
SET 
	id_status = 5
	last_status = _id_status;
WHERE 
	id = _id_request;

END
$$
LANGUAGE 'plpgsql'


--funcion para traer todas las areas
CREATE OR REPLACE FUNCTION  vud_area_record()

RETURNS SETOF cat_area
AS $$
BEGIN

	RETURN QUERY SELECT * from cat_area order by id asc;

END
$$
LANGUAGE 'plpgsql'


--funcion para capturar el dictamen de una solicitud

DROP FUNCTION vud_create_dictum_request(date,character varying, integer, date,integer);

CREATE OR REPLACE FUNCTION vud_create_dictum_request(
	_date_receives_dictum date, 
	_folio_dictum character varying, 
	_answer_dictum integer, 
	_citizen_dictum date, 
	_id_request integer)

RETURNS void
AS $$
BEGIN
INSERT INTO public.tracing_dictum(
			date_receives_dictum, 
			folio_dictum, 
			answer_dictum, 
			citizen_dictum, 
			id_request)
VALUES (_date_receives_dictum,
		_folio_dictum,
		_answer_dictum,
		_citizen_dictum,
		_id_request);

UPDATE public.request
SET 
	id_status = 8
WHERE 
	id = _id_request;

END
$$
LANGUAGE 'plpgsql'




--informacion que se debe de mostrar de las solicitudes, sin editar.

DROP FUNCTION vud_view_data_request(integer)

CREATE OR REPLACE FUNCTION vud_view_data_request(
_id_request integer,
out id integer,
out folio character varying,
out id_user integer,
out user_name character varying,
out means_request smallint,
out prevent_date text,
out citizen_prevent_date text,
out correct_date text,
out observation character varying,
out request_turn character varying,
out type_work character varying,
out name character varying, 
out last_name character varying, 
out phone character varying, 
out delegation character varying, 
out colony character varying, 
out postal_code character varying, 
out street character varying, 
out ext_number character varying,
out int_number character varying,
out id_status integer,
out description_status character varying,
out date_creation text,
out date_commitment text,
out code integer,
out description character varying,
out modality_desc character varying,
out id_turn integer,
out id_dictum integer,
out folio_dictum character varying,
out date_receives_dictum text,
out answer_dictum integer,
out citizen_dictum text)

RETURNS SETOF record
AS $$
BEGIN

RETURN QUERY SELECT 
	a.id,
	a.folio,
	a.id_user,
	i.user_name,
	a.means_request,
	to_char(a.prevent_date,'yyyy-mm-dd'),
	to_char(a.citizen_prevent_date,'yyyy-mm-dd'),
	to_char(a.correct_date,'yyyy-mm-dd'),
	a.observations,
	a.request_turn,
	a.type_work,
	g.name,
	g.last_name,
	g.phone,
	h.delegation,
	h.colony,
	h.postal_code,
	h.street,
	h.ext_number,
	h.int_number,
	d.id as id_status,
	d.description as description_status,
	to_char(a.date_creation,'yyyy-mm-dd' ),
	to_char(a.date_commitment,'yyyy-mm-dd' ),
	b.code,
	b.description,
	c.description as modality_desc,
	e.id as id_turn,
	f.id as id_dictum,
	f.folio_dictum,
	to_char(f.date_receives_dictum,'yyyy-mm-dd'),
	f.answer_dictum,
	to_char(f.citizen_dictum,'yyyy-mm-dd')
FROM 
	public.request as a
left join 
	public.cat_transact as b
on
	a.id_transact = b.code
left join 
	public.cat_modality as c
on 
	a.id_modality = c.code

left join 
	public.cat_status as d
on
	d.id = a.id_status
left join 
	public.tracing_turn as e
on
	e.id_request = a.id
left join
	public.tracing_dictum as f
on
	f.id_request = a.id
left join
	public.interested as g
on 
	a.id_interested = g.id
left join
	public.address as h
on
	a.id_address = h.id
left join 
	public.users as i
on 
	i.id = a.id_user
WHERE
	flag = 1 
AND a.id = _id_request
order by id asc;

END
$$
LANGUAGE 'plpgsql'


select * from vud_view_data_request()



--FUNCION PARA ACTUALIZAR LOS DATOS DE LA PREVENCION Y SUBSANACION

DROP FUNCTION vud_update_prevention(integer, date,date);

CREATE OR REPLACE FUNCTION vud_update_prevention(
	_id_request integer,
	_prevent_date date,
	_citizen_prevent_date date)

RETURNS void
AS $$
BEGIN
UPDATE public.request
	
SET 
	prevent_date = _prevent_date, 
	citizen_prevent_date = _citizen_prevent_date,
	id_status = 7
WHERE 
	id = _id_request;
END
$$
LANGUAGE 'plpgsql'


CREATE OR REPLACE FUNCTION vud_update_correct(
	_id_request integer,
	_correct_date date)

RETURNS void
AS $$
BEGIN
UPDATE public.request
	
SET 	
	correct_date = _correct_date,
	id_status = 9
WHERE 
	id = _id_request;
END
$$
LANGUAGE 'plpgsql'

-- FUNCION PARA CAMBIAR LOS ESTATUS DE LAS SOLICITUDES 
DROP FUNCTION vud_update_status_request(integer,integer,integer,character varying);

CREATE OR REPLACE FUNCTION vud_update_status_request(
	_id_request integer,
	_status integer,
	_last_status integer,
	_cancelation_description character varying)
RETURNS VOID
AS $$
BEGIN

UPDATE 
	public.request
SET 
	id_status = _status,
	last_status = _last_status,
	cancelation_description = _cancelation_description
WHERE 
	id = _id_request;
END
$$
LANGUAGE 'plpgsql'


DROP FUNCTION vud_activate_request(integer,integer,integer);

CREATE OR REPLACE FUNCTION vud_activate_request(
	_id_request integer,
	_status integer,
	_last_status integer)
RETURNS VOID
AS $$
BEGIN

UPDATE 
	public.request
SET 
	id_status = _last_status,
	last_status = _status
WHERE 
	id = _id_request;
END
$$
LANGUAGE 'plpgsql'


-- funcion para finiquitar la solicitud
DROP FUNCTION vud_create_settlement(integer,date,character varying)

CREATE OR REPLACE FUNCTION vud_create_settlement(
		_id_request integer,
		_settlement_date date,
		_settlement_number character varying)
RETURNS VOID
AS $$
BEGIN

UPDATE public.request
SET settlement_date= _settlement_date,
	settlement_number= _settlement_number,
	id_status = 6
WHERE id = _id_request;

END
$$
LANGUAGE 'plpgsql'

--FUNCION PARA GUARDAR TODOS LOS REQUSITOS DE UNA SOLICITUD

DROP FUNCTION vud_create_request_requeriment(character varying, integer)

CREATE OR REPLACE FUNCTION vud_create_request_requeriment(

	_folio character varying, 
	_id_requeriment integer)

RETURNS void
AS $$
DECLARE _id_request integer;
BEGIN

_id_request = (SELECT 
	id 
from 
	public.request as a
where 
	a.folio = _folio);

INSERT INTO public.request_requirement(
	id_request, 
	id_requirement)
VALUES (_id_request,_id_requeriment);

END
$$
LANGUAGE 'plpgsql'

select * from vud_create_request_requeriment('1/2018',1)

-- funcion para porder traer la informacion de la solicitud

DROP FUNCTION vud_update_request_info(INTEGER)


CREATE OR REPLACE FUNCTION vud_update_request_info(
	_id_request integer,
	out id integer,
	out observations character varying,
	out id_status integer,
	out id_transact integer,
	out id_vulnerable_group smallint,
	out means_request smallint,
	out delivery smallint,
	out id_modality integer,
	out request_turn character varying,
	out type_work character varying,
	out postal_code character varying,
	out colony character varying,
	out ext_number character varying,
	out int_number character varying,
	out street character varying,
	out delegation character varying,
	out name character varying,
	out last_name character varying,
	out e_mail character varying,
	out phone character varying,
	out type_person smallint,
	out description_modality character varying)

RETURNS SETOF RECORD
AS $$
BEGIN
	
RETURN QUERY SELECT 
		a.id,
		a.observations,
		a.id_status,
		a.id_transact,
		a.id_vulnerable_group,
		a.means_request,
		a.delivery,
		a.id_modality,
		a.request_turn,
		a.type_work,
		b.postal_code,
		b.colony,
		b.ext_number,
		b.int_number,
		b.street,
		b.delegation,
		c.name,
		c.last_name,
		c.e_mail,
		c.phone,
		c.type_person,
		d.description as description_modality
FROM
	public.request AS a
LEFT JOIN
	public.address AS b
ON
	a.id_address = b.id
LEFT JOIN
	public.interested AS c
ON
	a.id_interested = c.id
LEFT JOIN
	public.cat_modality as d
on	
	d.code = a.id_modality
WHERE a.id = _id_request;

END
$$
LANGUAGE 'plpgsql'

select * from vud_update_request_info(150)
	
SELECT * FROM vud_update_request_info(154)


--funcion para traer los requisitos llenados en la solicitud

CREATE OR REPLACE FUNCTION vud_record_request_requirement(
_id_request integer,
out id integer,
out description character varying)
RETURNS SETOF RECORD
AS $$
BEGIN

	RETURN QUERY SELECT 
	c.id,
	c.description
	FROM
		public.request  as a
	LEFT JOIN
		public.request_requirement as b
	ON
		b.id_request = a.id
	LEFT JOIN
		public.cat_requirement as c
	ON 
		b.id_requirement = c.id
	WHERE a.id =_id_request
	order by c.id asc;

END
$$
LANGUAGE 'plpgsql'



--funcion para traer los requisitos que se compararan contra los guardados para la solicitud

CREATE OR REPLACE FUNCTION vud_get_request_requirement(
_code integer,
_type_request integer,
out id integer,
out description character varying)

RETURNS SETOF RECORD
AS $$
BEGIN

IF (_type_request = 2) THEN

RETURN QUERY SELECT 
	c.id,
	c.description
FROM
	public.cat_modality as a
left join
	public.cat_modality_requirement as b
on
	b.id_modality = a.code
left join
	public.cat_requirement as c
on
	b.id_requirement = c.id
	where a.code = _code
	order by c.id asc;

ELSE
	RETURN QUERY SELECT 
		c.id,
		c.description
	FROM 
		public.cat_transact as a
	left join
		public.cat_transact_requirement as b
	on
		a.code = b.id_transact
	left join
		public.cat_requirement as c
	on
		b.id_requirement = c.id
	where a.code = _code
	order by c.id asc;

END IF;

END
$$
LANGUAGE 'plpgsql'


/**********************************************************
**
**funciones para acutalizar la informacion de la solicitud
**
***********************************************************/
DROP FUNCTION vud_update_data_address(integer, 
									character varying,
									character varying,
									character varying,
									character varying,
									character varying,
									character varying); 

CREATE OR REPLACE FUNCTION vud_update_data_address(
		_id_address integer,
		_postal_code character varying, 
		_colony character varying, 
		_ext_number character varying, 
		_int_number character varying, 
		_street character varying, 
		_delegation character varying)

RETURNS VOID
AS $$
BEGIN

	UPDATE public.address
	SET postal_code=_postal_code, 
		colony=_colony, 
		ext_number=_ext_number, 
		int_number=_int_number, 
		street=_street, 
		delegation=_delegation
    WHERE id = _id_address;

END
$$
LANGUAGE 'plpgsql'

--FUNCION PARA ACTUALIZAR LOS DATOS DEL INTERESADO.

CREATE OR REPLACE FUNCTION vud_update_data_interested(
	_id_interested integer,
	_name character varying,
	_last_name character varying,
	_e_mail character varying, 
	_phone character varying,
	_type_person integer)
		
RETURNS VOID
AS $$
BEGIN

	UPDATE 
		public.interested
    SET name=_name, 
    	last_name=_last_name, 
    	e_mail=_e_mail, 
    	phone=_phone, 
    	type_person=_type_person
    WHERE 
    	id =_id_interested;

END
$$
LANGUAGE 'plpgsql'


--FUNCION PARA ACTUALIZAR LOS DATOS DE LA SOLICITUD
CREATE OR REPLACE FUNCTION vud_update_data_request(
	_id_request integer,
	_observations character varying,
	_id_status integer,
	_id_transact integer,
	_id_vulnerable_group integer,
	_request_turn character varying,
	_type_work character varying,
	_means_request integer,
	_delivery integer,
	_id_modality integer)
RETURNS VOID
AS $$
BEGIN
	UPDATE public.request
	SET 
	observations=_observations, 
	id_status=_id_status, 
	id_transact=_id_transact, 
	id_vulnerable_group=_id_vulnerable_group, 
	request_turn=_request_turn, 
	type_work=_type_work, 
	means_request=_means_request, 
	delivery=_delivery, 
	id_modality=_id_modality 	
WHERE id = _id_request;

DELETE FROM public.request_requirement where id_request = _id_request;

END
$$
LANGUAGE 'plpgsql'

--actualizar la relacion de requisitos con solicitud

CREATE OR REPLACE FUNCTION vud_update_request_requeriment(
	_id_request integer, 
	_id_requeriment integer)

RETURNS void
AS $$
BEGIN

INSERT INTO public.request_requirement(
	id_request, 
	id_requirement)
VALUES (_id_request,_id_requeriment);

END
$$
LANGUAGE 'plpgsql'

--FUNCION PARA MOSTRAR LOS REQUISITOS MARCADOS DE UN TRAMITE

CREATE OR REPLACE FUNCTION vud_get_transact_requirements(
		_transact_code integer,
		out id integer,
		out description character varying)
RETURNS SETOF RECORD
AS $$
BEGIN

RETURN QUERY SELECT 
				c.id,
				c.description 
			FROM 
				public.cat_transact as a
			LEFT JOIN
				public.cat_transact_requirement as b
			on 
				a.code = b.id_transact
			LEFT JOIN 
				public.cat_requirement as c
			on 
				c.id = b.id_requirement
			WHERE
				a.code = _transact_code;
END
$$
LANGUAGE 'plpgsql'

--FUNCION PARA MOSTRAR LOS REQUISITOS MARCADOS DE UNA MODALIDAD

CREATE OR REPLACE FUNCTION vud_get_modality_requirements(
		_modality_code integer,
		out id integer,
		out description character varying)
RETURNS SETOF RECORD
AS $$
BEGIN

RETURN QUERY SELECT 
				c.id,
				c.description 
			FROM 
				public.cat_modality as a
			LEFT JOIN
				public.cat_modality_requirement as b
			on 
				a.code = b.id_modality
			LEFT JOIN 
				public.cat_requirement as c
			on 
				c.id = b.id_requirement
			WHERE
				a.code =  _modality_code
				order by id asc;
END
$$
LANGUAGE 'plpgsql'

--FUNCION PARA CONSULTAR LA INFORMACION DEL REPORTE

SELECT 
	a.folio,
	b.code,
	b.description,
	c.code,
	c.description,
	to_char(a.date_creation,'dd-mm-yyyy' ),
	to_char(a.date_commitment,'dd-mm-yyyy' )
FROM
	public.request as a
left join
	public.cat_transact as b
on
	a.id_transact = b.code
left join 
	public.cat_modality as c
on 
	a.id_modality = c.code
order by a.id desc



--function para traer las areas revisoras
CREATE OR REPLACE FUNCTION vud_record_area_revisora(
	out id integer,
	out description character varying)
RETURNS SETOF RECORD
AS $$
BEGIN
	RETURN QUERY SELECT id, description from public.cat_area order by id asc;

END
$$
LANGUAGE 'plpgsql'


--function para traer los grupos vulnerables
CREATE OR REPLACE FUNCTION vud_record_vulnerable_group(
	out id integer,
	out description character varying)
RETURNS SETOF RECORD
AS $$
BEGIN
	RETURN QUERY SELECT id, description from public.vulnerable_group order by id asc;

END
$$
LANGUAGE 'plpgsql'

--funcion para traer todas las delegaciones
CREATE OR REPLACE FUNCTION vud_record_delegation(
	out description character varying)
RETURNS SETOF RECORD
AS $$
BEGIN
	RETURN QUERY SELECT 
					cast(distinct(a.delegation) as character varying)
				FROM
					public.address AS a 
				ORDER BY a.delegation AS

END
$$
LANGUAGE 'plpgsql'

--

DROP FUNCTION vud_record_report()
CREATE OR REPLACE FUNCTION vud_record_report(
	out folio character varying,
	out clave_tramite integer,
	out tramite character varying,
	out clave_modalidad integer,
	out modalidad character varying,
	out fecha_creacion text,
	out fecha_compromiso text)
RETURNS SETOF RECORD
AS $$
BEGIN

RETURN QUERY SELECT 
    a.folio,
    b.code as clave_tramite,
    b.description as tramite,
    c.code as clave_modalidad,
    c.description as modalidad,
    to_char(a.date_creation,'dd-mm-yyyy' ) as fecha_creacion,
    to_char(a.date_commitment,'dd-mm-yyyy') as fecha_compromiso
FROM
    public.request as a
left join
    public.cat_transact as b
on
    a.id_transact = b.code
left join 
    public.cat_modality as c
on 
    a.id_modality = c.code
left join 
    public.address as d
on
    d.id = a.id_address
left join
    public.tracing_turn as e
on 
    e.id_request = a.id
left join
    public.tracing_dictum as f
on 
    f.id_request = a.id
left join
    public.interested as g
on g.id = a.id_interested
left join
    public.cat_status h
on 
    h.id = a.id_status
left join
    public.vulnerable_group i
on 
    i.id = a.id_vulnerable_group 

WHERE a.flag = 1
order by a.id asc;

END
$$
LANGUAGE 'plpgsql'




--funcion para mostrar los el reporte anual

--nombre de la funcion
CREATE OR REPLACE FUNCTION vud_year_report(
	_year character varying,
	out clave_tramite integer,
	out descripcion_tramite character varying,
	out clave_modalidad integer,
	out descripcion_modalidad character varying,
	out enero bigint,
	out febrero bigint,
	out marzo bigint,
	out abril bigint,
	out mayo bigint,
	out junio bigint,
	out julio bigint,
	out agosto bigint,
	out septiembre bigint,
	out octubre bigint,
	out noviembre bigint,
	out diciembre bigint,
	out total_de_tramites bigint,
	out total_de_modalidades bigint	)

RETURNS SETOF RECORD
AS $$
BEGIN
RETURN QUERY select  
	a.code AS CLAVE_TRAMITE,
	a.description AS DESCRIPCION_TRAMITE,
	c.code AS CLAVE_MODALIDAD,
	c.description AS descripcion_modalidad,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '01' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Enero,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '02' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Febrero,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '03' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Marzo,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '04' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Abril,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '05' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Mayo,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '06' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Junio,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '07' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Julio,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '08' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Agosto,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '09' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Septiembre,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '10' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as Octubre,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '11' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as noviembre,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '12' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as diciembre,
	count(CASE TO_CHAR(b.date_creation, 'YYYY') WHEN _year THEN  b.id_transact END) AS TOTAL_DE_TRAMITES,
	count(CASE TO_CHAR(b.date_creation, 'YYYY') WHEN _year THEN  c.code END ) AS TOTAL_DE_MODALIDADES
from 
	public.cat_transact as a 

left join 
	public.request as b
on
	a.code = b.id_transact

left join 
	public.cat_modality as c
on
	c.id_transact = a.code 
and
	b.id_modality = c.code
group by 
	a.code,
	a.description,
	c.code,
	c.description
order by a.code asc;
END
$$
LANGUAGE 'plpgsql'

select * from vud_year_report('2019')
DROP FUNCTION vud_year_report(integer)


CREATE OR REPLACE FUNCTION vud_update_citizen_dictum(
	_id_request integer,
	_date_citizen_dictum date)
RETURNS VOID
AS $$
BEGIN

UPDATE tracing_dictum
SET
	citizen_dictum = _date_citizen_dictum
WHERE
	_id_request = id_request;
END
$$
LANGUAGE 'plpgsql'


--FUNCION PARA GENERAR LA SEGUNDA GRAFICA

CREATE OR REPLACE FUNCTION vud_total_request_month(
_year character varying,
out enero bigint,
out febrero bigint,
out marzo bigint,
out abril bigint,
out mayo bigint,
out junio bigint,
out julio bigint,
out agosto bigint,
out septiembre bigint,
out octubre bigint,
out noviembre bigint,
out diciembre bigint)
RETURNS SETOF RECORD
AS $$
BEGIN

RETURN QUERY select  
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '01' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as enero,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '02' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as febrero,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '03' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as marzo,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '04' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as abril,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '05' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as mayo,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '06' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as junio,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '07' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as julio,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '08' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as agosto,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '09' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as septiembre,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '10' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as octubre,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '11' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as noviembre,
	Count(CASE when (TO_CHAR(b.date_creation, 'MM') = '12' and  TO_CHAR(b.date_creation, 'YYYY') = _year) THEN b.id_transact END) as diciembre
from 
	public.cat_transact as a 

left join 
	public.request as b
on
	a.code = b.id_transact

left join 
	public.cat_modality as c
on
	c.id_transact = a.code 
and
	b.id_modality = c.code;

END
$$
LANGUAGE 'plpgsql'


--FUNCION PARA GRAFICA DE DONA

CREATE OR REPLACE FUNCTION vud_chart_status(
	_year character varying,
	out id integer,
	out description character varying,
	out total_stado bigint)
RETURNS SETOF RECORD
AS $$
BEGIN

RETURN QUERY SELECT a.id,
				a.description,
				COUNT(b.id_status) as total_stado
	FROM 
		public.cat_status as a 
left join 
	public.request as b
on
	a.id = b.id_status
where to_char(b.date_creation, 'YYYY') = _year
	GROUP BY b.id_status,a.description,a.id
	order by a.id asc;

END
$$
LANGUAGE 'plpgsql'


drop function vud_chart_status()
select * from vud_chart_status()


-- FUNCION PARA GUARDAR LOS MOVIMIENTOS DE CUANDO SE CREA UNA SOLICITUD
CREATE OR REPLACE FUNCTION vud_request_audit_insert(
	
	_id_user integer,
	_accion character varying,
	_folio character varying)
RETURNS VOID
AS $$
DECLARE _id_request integer;
BEGIN 

	_id_request into select id from request where folio = _folio;

	INSERT INTO public.request_audit(
	 id_user, 
	 accion, 
	 id_request)
	VALUES (_id_user, 
			_accion, 
			_id_request);
	
RETURN QUERY 

END
$$
LANGUAGE 'plpgsql'

--